package com.binarium.chipredes.billets.ui.validate

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.billets.domain.usecases.DecodeBilletQrUseCase
import com.binarium.chipredes.billets.domain.usecases.GetBilletsTask
import com.binarium.chipredes.wolke.models.BilletData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ValidateBilletsViewModel @Inject constructor(
    private val decodeBilletQrUseCase: DecodeBilletQrUseCase,
    private val getBilletsTask: GetBilletsTask,
) : ViewModel() {

    private val _readBillets = MutableLiveData<List<BilletData>>()
    val readBillets: LiveData<List<BilletData>> = _readBillets
    private val readBilletsList = mutableListOf<BilletData>()

    var isFetching = MutableLiveData<Boolean>()

    init {
        isFetching.value = false
        startGetBilletsTask()
    }

    private fun startGetBilletsTask() {
        viewModelScope.launch {
            getBilletsTask.start().collect { billetData ->
                billetData?.let { onBilletFetched(it) }
                isFetching.postValue(false)
            }
        }
    }

    private fun onBilletFetched(billetData: BilletData) {
        readBilletsList.add(billetData)
        _readBillets.postValue(readBilletsList.toList())
    }

    fun onQrRead(rawValue: String) {
        when (val result = decodeBilletQrUseCase(rawValue)) {
            is DecodeBilletQrUseCase.DecodeBilletQrResult.DecodeSuccess -> {
                getBilletData(result.billetId)
            }
            is DecodeBilletQrUseCase.DecodeBilletQrResult.DecodeError -> Timber.e(result.message)
        }
    }

    private fun getBilletData(billetId: String) {
        if (getBilletsTask.addBilletToFetch(billetId)) isFetching.postValue(true)
    }
}
