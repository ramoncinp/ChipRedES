package com.binarium.chipredes.billets.ui.reservations

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.billets.domain.usecases.CancelBilletReservationUseCase
import com.binarium.chipredes.wolke2.models.ReservacionSaldo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CancelReservationViewModel @Inject constructor(
    private val cancelBilletReservationUseCase: CancelBilletReservationUseCase,
) : ViewModel() {

    private lateinit var selectedReservation: ReservacionSaldo

    private val _loading = MutableLiveData(false)
    val loading: LiveData<Boolean>
        get() = _loading

    private val _onSuccess = MutableLiveData(false)
    val onSuccess: LiveData<Boolean>
        get() = _onSuccess

    private val _onError = MutableLiveData<String>()
    val onError: LiveData<String>
        get() = _onError

    fun onReservation(reservation: ReservacionSaldo) {
        selectedReservation = reservation
    }

    fun cancelBilletReservation() {
        viewModelScope.launch(Dispatchers.IO) {
            _loading.postValue(true)
            try {
                selectedReservation.id?.let {
                    cancelBilletReservationUseCase(it)
                }
                _onSuccess.postValue(true)
            } catch (e: Exception) {
                e.printStackTrace()
                _onError.postValue("Error al cancelar reservación de saldo")
            }
            _loading.postValue(false)
        }
    }
}
