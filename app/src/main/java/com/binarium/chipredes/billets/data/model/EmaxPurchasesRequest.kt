package com.binarium.chipredes.billets.data.model

import java.util.*

data class EmaxPurchasesRequest(
    val clientId: String,
    val startDate: Date,
    val endDate: Date,
)
