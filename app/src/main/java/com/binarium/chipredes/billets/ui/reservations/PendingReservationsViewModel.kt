package com.binarium.chipredes.billets.ui.reservations

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.utils.DateUtils
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke2.domain.usecases.GetPendingReservationsUseCase
import com.binarium.chipredes.wolke2.models.ReservacionSaldo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class PendingReservationsViewModel @Inject constructor(
    private val getPendingReservationsUseCase: GetPendingReservationsUseCase,
) : ViewModel() {

    var startDate: Date? = DateUtils.getDeltaDate(Date(), -7)
    var endDate: Date? = Date()

    private val _clientData = MutableLiveData<LocalClientData>()
    val clientData: LiveData<LocalClientData>
        get() = _clientData
    private val selectedClient: LocalClientData?
        get() = _clientData.value
    private val clientChipRedId: String
        get() = selectedClient?.id!!

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _pendingReservations = MutableLiveData<List<ReservacionSaldo>>()
    val pendingReservations: LiveData<List<ReservacionSaldo>>
        get() = _pendingReservations

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    private fun canFetchPurchases() = selectedClient != null && startDate != null && endDate != null

    fun onNewClientSelected(queriedClient: LocalClientData) {
        _clientData.value = queriedClient
        fetchReservations()
    }

    fun onStartDateSelected(selectedStartDateString: String) {
        val startDateString = "$selectedStartDateString 00:00:00"
        startDate = DateUtils.dateStringToObject(startDateString)
        fetchReservations()
    }

    fun onEndDateSelected(selectedEndDateString: String) {
        val endDateString = "$selectedEndDateString 23:59:59"
        endDate = DateUtils.dateStringToObject(endDateString)
        fetchReservations()
    }

    fun fetchReservations() {
        if (canFetchPurchases().not()) return
        viewModelScope.launch(Dispatchers.IO) {
            getReservations()
        }
    }

    private suspend fun getReservations() {
        _loading.postValue(true)
        if (startDate != null && endDate != null) {
            try {
                val reservations = getPendingReservationsUseCase(
                    clientChipRedId,
                    startDate!!, endDate!!
                )
                _pendingReservations.postValue(reservations.toList())
            } catch (e: Exception) {
                e.printStackTrace()
                _errorMessage.postValue("Error al buscar reservaciones de cliente")
            }
        }
        _loading.postValue(false)
    }
}
