package com.binarium.chipredes.billets.domain.usecases

import com.binarium.chipredes.billets.data.model.AdjustablePurchase
import javax.inject.Inject

class CompareAdjustablePurchasesUseCase @Inject constructor() {

    operator fun invoke(
        emaxPurchases: List<AdjustablePurchase>,
        chipRedPurchases: List<AdjustablePurchase>,
    ): List<AdjustablePurchase> {

        val difference = emaxPurchases.filterNot { emaxPurchase ->
            hasPurchaseWithTicket(chipRedPurchases, emaxPurchase.ticket)
        }

        return difference.map { missingPurchases -> missingPurchases.copy(toAdjust = true) }
    }

    private fun hasPurchaseWithTicket(
        list: List<AdjustablePurchase>,
        ticketNumber: String,
    ): Boolean {
        for (purchase in list) {
            if (purchase.ticket == ticketNumber) return true
        }
        return false
    }
}
