package com.binarium.chipredes.billets.ui.reservations

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.binarium.chipredes.R
import com.binarium.chipredes.billets.ui.reservations.adapter.PendingReservationsAdapter
import com.binarium.chipredes.databinding.FragmentPendingReservationsBinding
import com.binarium.chipredes.searchclient.SelectedClientViewModel
import com.binarium.chipredes.utils.DateUtils
import com.binarium.chipredes.utils.extensions.addSeparators
import com.binarium.chipredes.utils.extensions.setDatePicker
import com.binarium.chipredes.utils.extensions.setVisible
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke2.models.ReservacionSaldo
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class PendingReservationsFragment : Fragment() {

    private val selectedClientViewModel: SelectedClientViewModel by activityViewModels()
    private val viewModel: PendingReservationsViewModel by viewModels()

    private lateinit var pendingReservationsAdapter: PendingReservationsAdapter

    private var _binding: FragmentPendingReservationsBinding? = null
    private val binding
        get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedClientViewModel.selectedClient.observe(this) {
            viewModel.onNewClientSelected(it)
        }
        if (selectedClientViewModel.selectedClient.value == null) {
            navigateToSearchFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentPendingReservationsBinding.inflate(layoutInflater, container, false)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.movimientos_cuenta_menu, menu)
    }

    override fun onResume() {
        super.onResume()

        requireActivity().onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    requireActivity().finish()
                }
            })

        activity?.title = "Reservaciones pendientes"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.searchLocalClientFragment) {
            navigateToSearchFragment()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initAdapters()
        initObservers()
        viewModel.fetchReservations()
    }

    private fun initViews() {
        binding.startDate.setDatePicker(requireActivity().supportFragmentManager) {
            viewModel.onStartDateSelected(it)
        }
        binding.endDate.setDatePicker(requireActivity().supportFragmentManager) {
            viewModel.onEndDateSelected(it)
        }
        bindInitialDates()
    }

    private fun bindInitialDates() {
        val startDate = DateUtils.getDayNameMonthYear(viewModel.startDate)
        val endDate = DateUtils.getDayNameMonthYear(viewModel.endDate)
        binding.startDate.setText(startDate)
        binding.endDate.setText(endDate)
    }

    private fun initAdapters() {
        pendingReservationsAdapter = PendingReservationsAdapter {
            findNavController().navigate(
                PendingReservationsFragmentDirections.pendingReservationsToCancelReservation(it)
            )
        }
        binding.pendingReservationsList.apply {
            adapter = pendingReservationsAdapter
            addSeparators()
        }
    }

    private fun initObservers() {
        viewModel.clientData.observe(viewLifecycleOwner) {
            setClientData(it)
        }
        viewModel.loading.observe(viewLifecycleOwner) {
            binding.progressBar.setVisible(it)
        }
        viewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        }
        viewModel.pendingReservations.observe(viewLifecycleOwner) {
            setReservationsData(it)
        }
    }

    private fun setClientData(localClientData: LocalClientData) {
        binding.clientEt.setText(localClientData.nombre)
    }

    private fun setReservationsData(reservations: List<ReservacionSaldo>) {
        Timber.d("Reservaciones pendientes -> $reservations")
        pendingReservationsAdapter.submitList(reservations)
        binding.pendingReservationsCv.setVisible(reservations.isNotEmpty())
        binding.noReservationsText.setVisible(reservations.isEmpty())
    }

    private fun navigateToSearchFragment() {
        findNavController().navigate(
            PendingReservationsFragmentDirections.actionPendingReservationsFragmentToSearchLocalClientFragment()
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
