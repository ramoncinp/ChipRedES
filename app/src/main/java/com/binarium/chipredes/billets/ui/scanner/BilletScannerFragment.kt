package com.binarium.chipredes.billets.ui.scanner

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.GenericDialog
import com.binarium.chipredes.R
import com.binarium.chipredes.barcode.BarcodeTrackerFactory
import com.binarium.chipredes.billets.BilletTransactionPlates
import com.binarium.chipredes.billets.data.model.ReadBilletsData
import com.binarium.chipredes.billets.ui.BilletsAdapter
import com.binarium.chipredes.billets.ui.readbillets.ReadBilletsBottomSheet
import com.binarium.chipredes.databinding.FragmentBilletScannerBinding
import com.binarium.chipredes.ocr.GraphicOverlay
import com.binarium.chipredes.ocr.OcrDetectorProcessor
import com.binarium.chipredes.ocr.OcrGraphic
import com.binarium.chipredes.utils.LoadingDialogFragment
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.MultiDetector
import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.google.android.gms.vision.text.TextBlock
import com.google.android.gms.vision.text.TextRecognizer
import com.rengwuxian.materialedittext.MaterialEditText
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.io.IOException

private const val RC_HANDLE_CAMERA_PERM = 2
private const val RC_HANDLE_GMS = 9001

@AndroidEntryPoint
class BilletScannerFragment : Fragment() {

    private var cameraSource: CameraSource? = null
    private var billetMultiDetector: MultiDetector? = null
    private var ocrMultiDetector: MultiDetector? = null

    private val viewModel: BilletScannerViewModel by viewModels()

    private lateinit var binding: FragmentBilletScannerBinding
    private lateinit var billetsAdapter: BilletsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val arguments = requireActivity().intent?.extras?.let {
            BilletScannerFragmentArgs.fromBundle(it)
        }
        requireActivity().intent?.extras?.clear()

        if (arguments != null) {
            viewModel.loadingPosition = arguments.selectedLoadingPosition
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        setHasOptionsMenu(true)

        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_billet_scanner, container, false)
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
        setCameraSource()
        //launchBottomSheet()
    }

    private fun initViews() {
        binding.takePlatesIb.setOnClickListener {
            cameraSource?.release()
            setOcrViews()
            setOcrDetector()
            startCameraSource()
        }

        binding.returnToScanBilletsCv.setOnClickListener {
            cameraSource?.release()
            setBilletViews()
            setBilletDetector()
            startCameraSource()
        }

        binding.useBilletsButton.setOnClickListener {
            if (viewModel.canSendData()) {
                onUserBilletsClicked()
            }
        }

        binding.takePlatesButton.setOnClickListener {
            onCapturePlates()
        }
    }

    override fun onResume() {
        super.onResume()
        startCameraSource()
        viewModel.resumeGetBilletsTask()
    }

    override fun onPause() {
        super.onPause()
        binding.preview.release()
        binding.preview.stop()
        viewModel.pauseGetBilletsTask()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraSource?.release()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_read_billets, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.delete_icon) {
            billetsAdapter.setRemovableData(!billetsAdapter.canRemove)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun startCameraSource() {
        val code =
            GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(requireContext())
        if (code != ConnectionResult.SUCCESS) {
            val dlg = GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS)
            dlg?.show()
        }
        try {
            binding.preview.start(cameraSource, binding.faceOverlay)
        } catch (e: IOException) {
            Timber.e("Unable to start camera source -> %s", e.toString())
            cameraSource?.release()
            cameraSource = null
        }
    }

    private fun setCameraSource() {
        val rc = ActivityCompat.checkSelfPermission(
            requireActivity().applicationContext,
            Manifest.permission.CAMERA
        )
        if (rc == PackageManager.PERMISSION_GRANTED) {
            setBilletDetector()
        } else {
            requestCameraPermission()
        }
    }

    private fun setBilletDetector() {
        if (billetMultiDetector == null) {
            createBilletCameraSource()
        } else {
            setCameraMultiDetector(billetMultiDetector!!)
        }
    }

    private fun setOcrDetector() {
        if (ocrMultiDetector == null) {
            createOcrCameraSource()
        } else {
            setCameraMultiDetector(ocrMultiDetector!!)
        }
    }

    private fun createBilletCameraSource() {
        val bardCodeDetector = BarcodeDetector.Builder(requireContext()).build()
        val barcodeFactory = BarcodeTrackerFactory(requireContext(), binding.faceOverlay)
        barcodeFactory.setmInterface { barcode ->
            requireActivity().runOnUiThread {
                viewModel.onQrRead(barcode.rawValue)
            }
        }
        bardCodeDetector.setProcessor(MultiProcessor.Builder(barcodeFactory).build())

        billetMultiDetector = MultiDetector.Builder()
            .add(bardCodeDetector)
            .build()

        if (billetMultiDetector?.isOperational?.not() == true) {
            val lowStorageFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW)
            val hasLowStorage = requireActivity().registerReceiver(null, lowStorageFilter) != null

            if (hasLowStorage) {
                Toast.makeText(requireContext(), R.string.low_storage_error, Toast.LENGTH_LONG)
                    .show()
                Timber.w(getString(R.string.low_storage_error))
            }
        }

        billetMultiDetector?.let {
            setCameraMultiDetector(it)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun createOcrCameraSource() {
        val graphicOverlay = binding.graphicOverlay as GraphicOverlay<OcrGraphic>
        val textRecognizer = TextRecognizer.Builder(requireContext()).build()
        val plateMaskLayout = binding.plateMaskLayout

        val ocrDetectorProcessor = OcrDetectorProcessor(graphicOverlay)
        plateMaskLayout.setLayoutListener {
            ocrDetectorProcessor.setRectFilter(plateMaskLayout.getmRect())
        }
        ocrDetectorProcessor.setRectFilter(plateMaskLayout.getmRect())
        ocrDetectorProcessor.setOcrInterface {
            Timber.d("Scanned plates!! ${it.value}")
            requireActivity().runOnUiThread {
                binding.scannedPlatesTv.text = it.value
            }
        }
        textRecognizer.setProcessor(ocrDetectorProcessor as Detector.Processor<TextBlock>)

        ocrMultiDetector = MultiDetector.Builder()
            .add(textRecognizer)
            .build()

        if (ocrMultiDetector?.isOperational?.not() == true) {
            val lowStorageFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW)
            val hasLowStorage = requireActivity().registerReceiver(null, lowStorageFilter) != null

            if (hasLowStorage) {
                Toast.makeText(requireContext(), R.string.low_storage_error, Toast.LENGTH_LONG)
                    .show()
                Timber.w(getString(R.string.low_storage_error))
            }
        }

        ocrMultiDetector?.let {
            setCameraMultiDetector(it)
        }
    }

    private fun setCameraMultiDetector(multiDetector: MultiDetector) {
        cameraSource = CameraSource.Builder(requireContext(), multiDetector)
            .setFacing(CameraSource.CAMERA_FACING_BACK)
            .setRequestedPreviewSize(1280, 720)
            .setRequestedFps(2.0f)
            .setAutoFocusEnabled(true)
            .build()
    }

    private fun requestCameraPermission() {
        Timber.w("Camera permission is not granted. Requesting permission")
        val permissions = arrayOf(Manifest.permission.CAMERA)
        if (!ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.CAMERA
            )
        ) {
            ActivityCompat.requestPermissions(requireActivity(), permissions, RC_HANDLE_CAMERA_PERM)
        }
    }

    private fun initObservers() {
        billetsAdapter = BilletsAdapter(BilletsAdapter.BilletListener {
            if (billetsAdapter.canRemove) {
                viewModel.removeBillet(it)
            }
        })

        binding.readCodes.layoutManager = LinearLayoutManager(requireContext())
        binding.readCodes.adapter = billetsAdapter
        binding.readCodes.setHasFixedSize(true)

        viewModel.readBillets.observe(viewLifecycleOwner) {
            if (it.isEmpty()) billetsAdapter.canRemove = false
            else binding.readCodes.smoothScrollToPosition(it.size - 1)

            setTotals()
            billetsAdapter.submitList(it)
        }

        viewModel.isFetching.observe(viewLifecycleOwner) {
            binding.progressBar.isVisible = it
        }

        viewModel.clientData.observe(viewLifecycleOwner) {
            binding.billetsClientTv.text = it.email
            setTotals()
        }

        viewModel.clientError.observe(viewLifecycleOwner) {
            val dialog = GenericDialog(
                "Error", it,
                { activity?.finish() }, null, requireContext()
            )
            dialog.setCancelable(false)
            dialog.show()
        }

        viewModel.clientWarning.observe(viewLifecycleOwner) {
            GenericDialog(
                "Aviso", it, { }, null, requireContext()
            ).show()
        }

        viewModel.onReady.observe(viewLifecycleOwner) {}

        viewModel.processingSalesMessage.observe(viewLifecycleOwner) {
            if (it) showProcessingDialog()
            else hideProcessingDialog()
        }
    }

    private fun setTotals() {
        binding.useBilletsButton.alpha =
            (if (viewModel.saleAmount > 0 && viewModel.clientData.value != null) 1f else 0.5f)
        binding.useBilletsText.alpha =
            (if (viewModel.saleAmount > 0 && viewModel.clientData.value != null) 1f else 0.5f)

        val totalText = "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(viewModel.saleAmount)
        binding.readBilletsTotal.text = totalText
    }

    private fun onUserBilletsClicked() {
        if (binding.useBilletsButton.alpha == 1.0f) {
            with(viewModel) {
                launchBottomSheet(
                    ReadBilletsData(
                        readBillets.value!!,
                        clientData.value!!,
                        BilletTransactionPlates(capturedPlates, savedCapturePath),
                        loadingPosition
                    )
                )
            }
        }
    }

    private fun setOcrViews() {
        binding.plateMaskLayout.visibility = View.VISIBLE
        binding.takePlatesButton.visibility = View.VISIBLE
        binding.returnToScanBilletsCv.visibility = View.VISIBLE
        binding.readPlatesCv.visibility = View.VISIBLE
        binding.readCodes.visibility = View.GONE
        binding.clientCardView.visibility = View.GONE
        binding.totalBilletsCv.visibility = View.GONE
        binding.useBilletsButton.visibility = View.GONE
        binding.takePlatesIb.visibility = View.GONE
    }

    private fun setBilletViews() {
        binding.plateMaskLayout.visibility = View.GONE
        binding.takePlatesButton.visibility = View.GONE
        binding.returnToScanBilletsCv.visibility = View.GONE
        binding.readPlatesCv.visibility = View.GONE
        binding.readCodes.visibility = View.VISIBLE
        binding.clientCardView.visibility = View.VISIBLE
        binding.totalBilletsCv.visibility = View.VISIBLE
        binding.useBilletsButton.visibility = View.VISIBLE
        binding.takePlatesIb.visibility = View.VISIBLE
        if (viewModel.capturedPlates.isNotEmpty()) {
            binding.platesCardView.visibility = View.VISIBLE
            binding.readPlatesTv.text = viewModel.capturedPlates
        } else {
            binding.platesCardView.visibility = View.GONE
        }
    }

    private fun onCapturePlates() {
        var plates = binding.scannedPlatesTv.text.toString()

        if (plates.contains("-")) {
            plates = plates.replace("-", "")
        }

        if (plates.contains(" ")) {
            plates = plates.replace(" ", "")
        }

        cameraSource?.takePicture(null) {
            viewModel.savePicture(it, requireActivity().filesDir.path)
            Timber.d("Foto almacenada con path ${viewModel.savedCapturePath}")
        }

        confirmPlates(plates)
    }

    private fun confirmPlates(plates: String) {
        val builder = AlertDialog.Builder(requireContext())
        val content = layoutInflater.inflate(R.layout.dialog_confirm_read_plates, null)
        val readPlates: MaterialEditText = content.findViewById(R.id.plates_et)
        readPlates.setText(plates)
        builder.setView(content)
        builder.setPositiveButton("Ok") { dialog, _ ->
            viewModel.capturedPlates = readPlates.text.toString()
            cameraSource?.release()
            setBilletViews()
            setBilletDetector()
            startCameraSource()
            dialog.dismiss()
        }
        builder.setNegativeButton("Regresar") { dialog, _ -> dialog.dismiss() }
        builder.setCancelable(true)
        builder.show()
    }

    private fun showProcessingDialog() {
        val fragment = getLoadingFragment()
        if (fragment == null) {
            LoadingDialogFragment().apply {
                isCancelable = false
            }.show(
                requireActivity().supportFragmentManager,
                LoadingDialogFragment.TAG
            )
        }
    }

    private fun hideProcessingDialog() {
        val fragment = getLoadingFragment() as LoadingDialogFragment?
        fragment?.dismiss()
    }

    private fun getLoadingFragment() = requireActivity().supportFragmentManager
        .findFragmentByTag(LoadingDialogFragment.TAG)

    private fun launchBottomSheet(readBilletsData: ReadBilletsData) {
        val bottomSheet = ReadBilletsBottomSheet.createInstance(readBilletsData)
        bottomSheet.show(requireActivity().supportFragmentManager, ReadBilletsBottomSheet.TAG)
    }
}
