package com.binarium.chipredes.billets.data.model

data class MissingPurchasesResult(
    val quantity: Int,
    val amountSum: String,
)
