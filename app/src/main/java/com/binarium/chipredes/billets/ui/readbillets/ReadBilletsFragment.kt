package com.binarium.chipredes.billets.ui.readbillets

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.GenericDialog
import com.binarium.chipredes.R
import com.binarium.chipredes.billets.ui.BilletsAdapter
import com.binarium.chipredes.databinding.FragmentReadBilletsBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class ReadBilletsFragment : Fragment() {

    lateinit var binding: FragmentReadBilletsBinding
    val viewModel: ReadBilletsViewModelOld by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getArguments(ReadBilletsFragmentArgs.fromBundle(requireArguments()))
        viewModel.createBilletTransaction()
    }

    private fun getArguments(arguments: ReadBilletsFragmentArgs) {
        with(viewModel) {
            readBillets = arguments.readBillets.toList()
            localClientData = arguments.client
            loadingPosition = arguments.selectedLoadingPosition
            plates = arguments.plates
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_read_billets,
            container,
            false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
    }

    private fun initViews() {
        binding.cancelButton.setOnClickListener {
            viewModel.cancelReservation()
        }

        binding.startButton.setOnClickListener {
            viewModel.startSale()
        }
    }

    private fun initObservers() {
        val adapter = BilletsAdapter(BilletsAdapter.BilletListener {
            Timber.d("Billet selected ${it.monto}")
        }, BilletsAdapter.BilletAdapterViewType.READ_BILLET_VIEW_TYPE.ordinal)

        binding.billetsList.adapter = adapter

        viewModel.reservedBillets.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        viewModel.isLoading.observe(viewLifecycleOwner) {
            binding.loadingLayout.visibility = if (it) View.VISIBLE else View.GONE
            binding.billetsListLayout.visibility = if (!it) View.VISIBLE else View.GONE
            binding.startButton.visibility = if (!it) View.VISIBLE else View.GONE
            binding.cancelButton.visibility = if (!it) View.VISIBLE else View.GONE
            binding.loadingText.text = viewModel.loadingMessage
        }

        viewModel.billetTransaction.observe(viewLifecycleOwner) {
            val amountText = "$ ${ChipREDConstants.MX_AMOUNT_FORMAT.format(it.cantidad)}"
            binding.readBilletsTotal.text = amountText
        }

        viewModel.reservationSubmitted.observe(viewLifecycleOwner) {
            if (it) {
                closeScreen("Reservación enviada a dispensario")
                viewModel.onReservationSubmitted()
            }
        }

        viewModel.reservationCancelled.observe(viewLifecycleOwner) {
            if (it) {
                closeScreen("Venta cancelada")
                viewModel.onReservationCancelled()
            }
        }

        viewModel.errorMessage.observe(viewLifecycleOwner) {
            GenericDialog(
                "Error",
                it,
                { requireActivity().finish() },
                null,
                requireContext()
            ).show()
        }

        viewModel.startSaleErrorMessage.observe(viewLifecycleOwner) {
            GenericDialog(
                "Error al iniciar venta",
                it,
                { viewModel.startSale() },
                { requireActivity().finish() },
                requireContext()
            ).apply {
                setPositiveText("Reintentar")
                setNegativeText("Salir")
                show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        requireActivity().onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    requireActivity().finish()
                }
            })
    }

    private fun setActivityResult(result: Int) {
        val resultIntent = Intent()
        requireActivity().setResult(result, resultIntent)
    }

    private fun closeScreen(message: String) {
        Toast.makeText(
            requireContext(),
            message,
            Toast.LENGTH_LONG
        ).show()
        setActivityResult(RESULT_OK)
        requireActivity().finish()
    }
}
