package com.binarium.chipredes.billets.ui.reservations.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.databinding.PendingReservationItemBinding
import com.binarium.chipredes.utils.DateUtils
import com.binarium.chipredes.wolke2.models.ReservacionSaldo

class PendingReservationsAdapter(val onClickListener: (ReservacionSaldo) -> Unit) :
    ListAdapter<ReservacionSaldo, PendingReservationsAdapter.ViewHolder>(
        PendingReservationDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            PendingReservationItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val purchase = getItem(position)
        holder.bind(purchase, onClickListener)
    }

    class ViewHolder(private val binding: PendingReservationItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ReservacionSaldo, clickListener: (ReservacionSaldo) -> Unit) {
            val amountText = "\$${ChipREDConstants.MX_AMOUNT_FORMAT.format(item.cantidad)}"
            val pcText = "Posición de carga ${item.posicionCarga}"
            with(binding) {
                amountTv.text = amountText
                loadingPositionTv.text = pcText
                dateTv.text = formatDate(item.fechaHora)
                cancelButton.setOnClickListener {
                    clickListener(item)
                }
            }
        }

        private fun formatDate(date: String): String = try {
            val dateObj = DateUtils.formatoFechaMovimientosCuenta(date, true)
            "${DateUtils.getDayMonthYear(dateObj)} ${DateUtils.getHourWithSeconds(dateObj)}"
        } catch (e: Exception) {
            date
        }
    }

    class PendingReservationDiffCallback : DiffUtil.ItemCallback<ReservacionSaldo>() {
        override fun areItemsTheSame(
            oldItem: ReservacionSaldo,
            newItem: ReservacionSaldo,
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: ReservacionSaldo,
            newItem: ReservacionSaldo,
        ): Boolean = oldItem == oldItem
    }
}
