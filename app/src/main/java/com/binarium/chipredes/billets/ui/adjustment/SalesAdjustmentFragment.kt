package com.binarium.chipredes.billets.ui.adjustment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.airbnb.lottie.LottieDrawable
import com.binarium.chipredes.billets.ui.adjustment.adapters.AdjustmentResultsAdapter
import com.binarium.chipredes.databinding.SalesAdjustmentFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class SalesAdjustmentFragment : Fragment() {

    private val viewModel: SalesAdjustmentViewModel by viewModels()
    private lateinit var adjustmentResultsAdapter: AdjustmentResultsAdapter
    private var _binding: SalesAdjustmentFragmentBinding? = null
    private val binding
        get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getMissingSales()
    }

    private fun getMissingSales() {
        val arguments = SalesAdjustmentFragmentArgs.fromBundle(requireArguments())
        val missingSales = arguments.missingPurchases.reversed().toList()
        viewModel.missingSales = missingSales
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = SalesAdjustmentFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
        viewModel.adjustSales()
    }

    private fun initViews() {
        initLogList()
        playAnimation()
        setMissingSalesInfo()
        binding.returnButton.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun initObservers() {
        viewModel.adjustmentProgress.observe(viewLifecycleOwner) { progress ->
            Timber.d("New progress is $progress")
            binding.progressIndicator.progress = progress
        }

        viewModel.adjustmentFinished.observe(viewLifecycleOwner) { finished ->
            if (finished) {
                binding.returnButton.visibility = View.VISIBLE
                binding.adjustingViewsAnimation.cancelAnimation()
            }
        }

        viewModel.results.observe(viewLifecycleOwner) { results ->
            adjustmentResultsAdapter.submitList(results)
        }
    }

    private fun initLogList() {
        adjustmentResultsAdapter = AdjustmentResultsAdapter()
        binding.resultsList.adapter = adjustmentResultsAdapter
    }

    private fun playAnimation() {
        binding.adjustingViewsAnimation.repeatCount = LottieDrawable.INFINITE
        binding.adjustingViewsAnimation.playAnimation()
    }

    private fun setMissingSalesInfo() {
        val text = "Sincronizando ${viewModel.missingSales.size} consumos"
        binding.missingSalesTv.text = text
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
