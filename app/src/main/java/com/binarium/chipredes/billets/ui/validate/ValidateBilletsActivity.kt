package com.binarium.chipredes.billets.ui.validate

import android.Manifest
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.binarium.chipredes.R
import com.binarium.chipredes.barcode.BarcodeTrackerFactory
import com.binarium.chipredes.billets.ui.BilletsAdapter
import com.binarium.chipredes.databinding.ActivityValidateBilletsBinding
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.MultiDetector
import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.barcode.BarcodeDetector
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.io.IOException

private const val HANDLE_CAMERA_PERM = 2
private const val HANDLE_GMS = 9001

@AndroidEntryPoint
class ValidateBilletsActivity : AppCompatActivity() {
    private var cameraSource: CameraSource? = null
    private var billetMultiDetector: MultiDetector? = null

    private lateinit var binding: ActivityValidateBilletsBinding
    private lateinit var billetsAdapter: BilletsAdapter

    private val viewModel: ValidateBilletsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Validar billetes"

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding = ActivityValidateBilletsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setCameraSource()
        initViews()
        initObservers()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        startCameraSource()
    }

    override fun onPause() {
        super.onPause()
        binding.preview.release()
        binding.preview.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraSource?.release()
    }

    private fun initViews() {
        billetsAdapter = BilletsAdapter(BilletsAdapter.BilletListener {})
        binding.readCodes.adapter = billetsAdapter
    }

    private fun initObservers() {
        viewModel.isFetching.observe(this) { isFetching ->
            binding.progressBar.visibility = if (isFetching) View.VISIBLE else View.GONE
        }

        viewModel.readBillets.observe(this) {
            if (it.isNotEmpty()) binding.readCodes.smoothScrollToPosition(it.size - 1)
            billetsAdapter.submitList(it)
        }
    }

    private fun startCameraSource() {
        val code =
            GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        if (code != ConnectionResult.SUCCESS) {
            val dlg = GoogleApiAvailability.getInstance().getErrorDialog(this, code, HANDLE_GMS)
            dlg?.show()
        }
        try {
            binding.preview.start(cameraSource, binding.faceOverlay)
        } catch (e: IOException) {
            Timber.e("Unable to start camera source -> %s", e.toString())
            cameraSource?.release()
            cameraSource = null
        }
    }

    private fun setCameraSource() {
        val rc = ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA)
        if (rc == PackageManager.PERMISSION_GRANTED) {
            setBilletDetector()
        } else {
            requestCameraPermission()
        }
    }

    private fun requestCameraPermission() {
        Timber.w("Camera permission is not granted. Requesting permission")
        val permissions = arrayOf(Manifest.permission.CAMERA)
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)
        ) {
            ActivityCompat.requestPermissions(this, permissions, HANDLE_CAMERA_PERM)
        }
    }

    private fun setBilletDetector() {
        if (billetMultiDetector == null) {
            createBilletCameraSource()
        } else {
            setCameraMultiDetector(billetMultiDetector!!)
        }
    }

    private fun createBilletCameraSource() {
        val bardCodeDetector = BarcodeDetector.Builder(this).build()
        val barcodeFactory = BarcodeTrackerFactory(this, binding.faceOverlay)
        barcodeFactory.setmInterface { barcode ->
            runOnUiThread {
                viewModel.onQrRead(barcode.rawValue)
            }
        }
        bardCodeDetector.setProcessor(MultiProcessor.Builder(barcodeFactory).build())

        billetMultiDetector = MultiDetector.Builder()
            .add(bardCodeDetector)
            .build()

        if (billetMultiDetector?.isOperational?.not() == true) {
            val lowStorageFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW)
            val hasLowStorage = registerReceiver(null, lowStorageFilter) != null

            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG)
                    .show()
                Timber.w(getString(R.string.low_storage_error))
            }
        }

        billetMultiDetector?.let {
            setCameraMultiDetector(it)
        }
    }

    private fun setCameraMultiDetector(multiDetector: MultiDetector) {
        cameraSource = CameraSource.Builder(this, multiDetector)
            .setFacing(CameraSource.CAMERA_FACING_BACK)
            .setRequestedPreviewSize(1280, 720)
            .setRequestedFps(2.0f)
            .setAutoFocusEnabled(true)
            .build()
    }
}
