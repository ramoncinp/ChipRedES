package com.binarium.chipredes.billets.domain.usecases

import com.binarium.chipredes.emax.EmaxDBManager
import com.binarium.chipredes.local.data.LocalServerClient
import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.wolke2.domain.usecases.GetPendingReservationsUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import javax.inject.Inject

class ProcessPendingReservationsUseCase @Inject constructor(
    private val getPendingReservationsUseCase: GetPendingReservationsUseCase,
    private val getSaleRequestUseCase: GetSaleRequestUseCase,
    private val emaxDBManager: EmaxDBManager,
    private val cancelBilletReservationUseCase: CancelBilletReservationUseCase,
    private val localServiceClient: LocalServerClient,
    private val logger: Logger
) {

    suspend operator fun invoke(clientId: String): Flow<ProcessingReservationsResult> = flow {
        try {
            val reservations = getPendingReservationsUseCase(
                clientId = clientId,
                startDate = null,
                endDate = null
            )

            if (reservations.isEmpty()) {
                emit(ProcessingReservationsResult.Done)
                return@flow
            } else {
                logger.postMessage(
                    message = "${reservations.size} reservaciones pendientes",
                    level = LogLevel.WARNING,
                    client = clientId
                )
            }

            emit(ProcessingReservationsResult.Processing)
            reservations.forEach { reservation ->
                reservation.id?.let {
                    val saleRequest = getSaleRequestUseCase(it)
                    if (saleRequest == null) {
                        logger.postMessage(
                            message = "Cancelar reservacion por solicitud de venta nula",
                            level = LogLevel.INFO,
                            client = clientId
                        )
                        cancelBilletReservationUseCase(it)
                    } else {
                        searchPendingSaleInEmax(saleRequest.preautorizacion, clientId)
                    }
                }
            }
            emit(ProcessingReservationsResult.Adjusted)
        } catch (e: Exception) {
            e.printStackTrace()
            Timber.e(e)
            logger.postMessage(
                message = "Error en proceso de reservaciones pendientes: $e",
                level = LogLevel.ERROR,
                client = clientId
            )
            emit(ProcessingReservationsResult.Error)
        }
    }

    private suspend fun searchPendingSaleInEmax(preAuth: String, clientId: String) {
        val finishedSale = emaxDBManager.getPurchaseFromPreAuth(preAuth)
        finishedSale?.let {
            logger.postMessage(
                message = "Registrando venta pendiente de emax, preAuth: $preAuth",
                level = LogLevel.INFO,
                client = clientId
            )
            localServiceClient.registerDispenserSale(it)
            return
        }

        val canceledSale = emaxDBManager.isPreAuthCanceled(preAuth)
        if (canceledSale) {
            logger.postMessage(
                message = "Cancelando reservacion pendiente de emax, preAuth: $preAuth",
                level = LogLevel.INFO,
                client = clientId
            )
            localServiceClient.cancelSale(preAuth)
            return
        }
    }
}

sealed class ProcessingReservationsResult {
    object Processing : ProcessingReservationsResult()
    object Done : ProcessingReservationsResult()
    object Adjusted : ProcessingReservationsResult()
    object Error : ProcessingReservationsResult()
}
