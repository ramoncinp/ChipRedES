package com.binarium.chipredes.billets.domain.usecases

import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.wolke.domain.usecases.GetStationMongoIdUseCase
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke2.data.Wolke2Repository
import com.binarium.chipredes.wolke2.domain.usecases.GetClientStationAccountUseCase
import com.binarium.chipredes.wolke2.models.ReserveTransactionResult
import com.binarium.chipredes.wolke2.services.ReserveBilletTransaction
import javax.inject.Inject

class CreateBilletReservationUseCase @Inject constructor(
    private val wolke2Repository: Wolke2Repository,
    private val stationMongoIdUseCase: GetStationMongoIdUseCase,
    private val getClientStationAccountUseCase: GetClientStationAccountUseCase,
    private val logger: Logger,
) {

    suspend operator fun invoke(
        client: LocalClientData,
        billetsIds: List<String>,
        loadingPosition: Int,
        urlPlates: String = "",
        placas: String = "",
    ): CreateReservationResult {

        val request = ReserveBilletTransaction(
            clientId = client.id.orEmpty(),
            stationId = stationMongoIdUseCase(),
            billetIds = billetsIds,
            loadingPosition = loadingPosition.toString(),
            urlPlates = urlPlates,
            placas = placas
        )

        return try {
            val response = wolke2Repository.createBilletReservation(request)
            if (response.isSuccessful) {
                val getReservationResult = response.body()
                getReservationResult?.let {
                    postClientDataEvent(client)
                    CreateReservationResult.Success(it)
                } ?: CreateReservationResult.Error("Error al obtener datos de reservación")
            } else {
                val errorToDisplay = "Error en respuesta para crear reservación de saldo"
                val errorMessage = response.errorBody()?.string()
                    ?: "$errorToDisplay, Codigo ${response.code()}"
                postErrorMessage(errorMessage, client)
                CreateReservationResult.Error(errorToDisplay)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            val errorToDisplay = "Error al crear reservacion de saldo"
            val error = "$errorToDisplay: ${e.message}"
            postErrorMessage(error, client)
            CreateReservationResult.Error(errorToDisplay)
        }
    }

    private fun postErrorMessage(errorMessage: String, client: LocalClientData) {
        logger.postMessage(
            message = errorMessage,
            level = LogLevel.ERROR,
            client = client.id.orEmpty(),
            balance = client.saldo ?: -1.0
        )
    }

    private suspend fun postClientDataEvent(client: LocalClientData) {
        client.id?.let {
            getClientStationAccountUseCase.invoke(it)?.let { account ->
                val balance = account.saldo
                logger.postMessage(
                    message = "Reservacion de saldo creada",
                    level = LogLevel.INFO,
                    client = account.cliente.id,
                    balance = balance
                )
            }
        }
    }
}

sealed class CreateReservationResult {
    data class Success(val data: ReserveTransactionResult) : CreateReservationResult()
    data class Error(val message: String) : CreateReservationResult()
}
