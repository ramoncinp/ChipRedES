package com.binarium.chipredes.billets

class BilletStatus {
    companion object {
        const val STATUS_ACTIVO = "AC"
        const val STATUS_CANCELADO = "CA"
        const val STATUS_USADO = "US"
    }
}