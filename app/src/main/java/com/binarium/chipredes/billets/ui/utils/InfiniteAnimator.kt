package com.binarium.chipredes.billets.ui.utils

import android.animation.Animator
import android.animation.Animator.AnimatorListener
import com.airbnb.lottie.LottieAnimationView

class InfiniteAnimator(
    private val animationView: LottieAnimationView,
) : AnimatorListener {
    override fun onAnimationStart(p0: Animator) {}
    override fun onAnimationEnd(p0: Animator) {
        animationView.playAnimation()
    }
    override fun onAnimationCancel(p0: Animator) {}
    override fun onAnimationRepeat(p0: Animator) {}
}
