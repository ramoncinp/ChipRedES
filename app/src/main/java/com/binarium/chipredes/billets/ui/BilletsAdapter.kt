package com.binarium.chipredes.billets.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.R
import com.binarium.chipredes.billets.BilletStatus
import com.binarium.chipredes.databinding.BilletLayoutBinding
import com.binarium.chipredes.databinding.TransparentBilletLayoutBinding
import com.binarium.chipredes.wolke.models.BilletData

class BilletsAdapter(
        private val onClickListener: BilletListener,
        private val viewType: Int = BilletAdapterViewType.TRANSPARENT_VIEW_TYPE.ordinal) :
        ListAdapter<BilletData, RecyclerView.ViewHolder>(DiffCallback) {

    enum class BilletAdapterViewType {
        READ_BILLET_VIEW_TYPE,
        TRANSPARENT_VIEW_TYPE
    }

    var canRemove = false

    fun setRemovableData(newVal: Boolean) {
        if (currentList.isNotEmpty()) {
            canRemove = newVal
            notifyDataSetChanged()
        }
    }

    class TransparentBilletViewHolder(private var binding: TransparentBilletLayoutBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(clickListener: BilletListener, billet: BilletData, position: Int, canRemove: Boolean) {
            val context = binding.root.context
            val statusColor = when (billet.estatus) {
                BilletStatus.STATUS_USADO -> ContextCompat.getColor(context, R.color.canjeado_low_alpha)
                BilletStatus.STATUS_CANCELADO -> ContextCompat.getColor(context, R.color.cancelado_low_alpha)
                else -> ContextCompat.getColor(context, R.color.activo_low_alpha)
            }
            val amountText = "$ " + ChipREDConstants.MX_AMOUNT_FORMAT.format(billet.monto)

            (binding.root as CardView).setCardBackgroundColor(statusColor)
            binding.amount.text = amountText
            binding.billetNumber.text = billet.folio
            binding.idxText.text = (position + 1).toString()
            binding.billet = billet
            binding.clickListener = clickListener
            binding.removeButton.visibility = if (canRemove) View.VISIBLE else View.GONE
            binding.idxCv.visibility = if (canRemove) View.GONE else View.VISIBLE
        }

        companion object {
            fun from(parent: ViewGroup): RecyclerView.ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = TransparentBilletLayoutBinding.inflate(layoutInflater, parent, false)
                return TransparentBilletViewHolder(binding)
            }
        }
    }

    class ReadBilletViewHolder(private val binding: BilletLayoutBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(billet: BilletData) {
            val amountText = "$ " + ChipREDConstants.MX_AMOUNT_FORMAT.format(billet.monto)
            binding.amount.text = amountText
            binding.billetNumber.text = billet.folio
        }

        companion object {
            fun from(parent: ViewGroup): RecyclerView.ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = BilletLayoutBinding.inflate(layoutInflater, parent, false)
                return ReadBilletViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            BilletAdapterViewType.TRANSPARENT_VIEW_TYPE.ordinal -> TransparentBilletViewHolder.from(parent)
            BilletAdapterViewType.READ_BILLET_VIEW_TYPE.ordinal -> ReadBilletViewHolder.from(parent)
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val billet = getItem(position)
        when (holder) {
            is TransparentBilletViewHolder -> {
                holder.bind(onClickListener, billet, position, canRemove)
            }
            is ReadBilletViewHolder -> {
                holder.bind(billet)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    class BilletListener(val clickListener: (billet: BilletData) -> Unit) {
        fun onClick(billet: BilletData) = clickListener(billet)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<BilletData>() {
        override fun areItemsTheSame(oldItem: BilletData, newItem: BilletData): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: BilletData, newItem: BilletData): Boolean {
            return oldItem == newItem
        }
    }
}
