package com.binarium.chipredes.billets.domain.usecases

import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.BilletData
import com.binarium.chipredes.wolke.models.GetBilletDataPost
import java.lang.Exception
import javax.inject.Inject

class GetBilletDataUseCase @Inject constructor(
    private val wolkeApiService: WolkeApiService,
) {

    suspend operator fun invoke(billetId: String): BilletData? {
        val request = createRequest(billetId)
        return try {
            val response = getBilletData(request)
            response.data
        } catch (e: Exception) {
            null
        }
    }

    private fun createRequest(billetId: String) = GetBilletDataPost(idBillete = billetId)

    private suspend fun getBilletData(request: GetBilletDataPost) =
        wolkeApiService.getBilletData(request)
}
