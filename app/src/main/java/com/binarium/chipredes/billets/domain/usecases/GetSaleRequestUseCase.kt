package com.binarium.chipredes.billets.domain.usecases

import com.binarium.chipredes.local.LocalApiService
import com.binarium.chipredes.local.models.GetSaleRequestPost
import com.binarium.chipredes.local.models.SaleRequestData
import javax.inject.Inject

class GetSaleRequestUseCase @Inject constructor(
    private val localApiService: LocalApiService,
) {
    suspend operator fun invoke(reservationId: String): SaleRequestData? {
        val request = localApiService.getSaleRequests(
            GetSaleRequestPost(reservationId = reservationId)
        )

        val saleRequestData = if (request.isSuccessful) {
            request.body()?.data
        } else {
            null
        }

        return saleRequestData
    }
}
