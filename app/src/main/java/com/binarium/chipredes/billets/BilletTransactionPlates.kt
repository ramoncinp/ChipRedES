package com.binarium.chipredes.billets

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BilletTransactionPlates(
        val plates: String,
        val photoPath: String
) : Parcelable
