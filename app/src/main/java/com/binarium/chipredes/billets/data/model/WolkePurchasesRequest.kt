package com.binarium.chipredes.billets.data.model

import java.util.*

data class WolkePurchasesRequest(
    val clientId: String,
    val accountId: String,
    val startDate: Date,
    val endDate: Date,
)
