package com.binarium.chipredes.billets.ui.adjustment.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.binarium.chipredes.R
import com.binarium.chipredes.billets.data.model.AdjustablePurchase
import com.binarium.chipredes.databinding.AdjustedPurchaseResultLayoutBinding
import com.binarium.chipredes.wolke2.models.AdjustSaleResult

class AdjustmentResultsAdapter : ListAdapter<AdjustSaleResult, AdjustmentResultsAdapter.ViewHolder>(
    AdjustablePurchaseDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            AdjustedPurchaseResultLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = getItem(position)
        holder.bind(result)
    }

    class ViewHolder(private val binding: AdjustedPurchaseResultLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: AdjustSaleResult) {
            var saleData: AdjustablePurchase? = null
            var icon: Int? = null
            var color: Int? = null
            if (item is AdjustSaleResult.Success) {
                saleData = item.sale
                icon = R.drawable.ic_check
                color = R.color.green_money
            } else if (item is AdjustSaleResult.Error) {
                saleData = item.sale
                icon = R.drawable.ic_error
                color = R.color.red
            }

            val amountText = "\$${saleData?.amount}"
            val quantityText = "${saleData?.quantity}L"
            val ticketText = "Ticket ${saleData?.ticket}"
            binding.amountTv.text = amountText
            binding.quantityTv.text = quantityText
            binding.ticketTv.text = ticketText
            setResultImage(icon!!, color!!)
        }

        private fun setResultImage(drawableRes: Int, color: Int) {
            val context = binding.root.context
            binding.resultImageCv.setCardBackgroundColor(ContextCompat.getColor(context, color))
            binding.resultImageView.setImageResource(drawableRes)
        }
    }

    class AdjustablePurchaseDiffCallback : DiffUtil.ItemCallback<AdjustSaleResult>() {
        override fun areItemsTheSame(
            oldItem: AdjustSaleResult,
            newItem: AdjustSaleResult,
        ): Boolean = oldItem == newItem

        override fun areContentsTheSame(
            oldItem: AdjustSaleResult,
            newItem: AdjustSaleResult,
        ): Boolean = oldItem == oldItem
    }
}
