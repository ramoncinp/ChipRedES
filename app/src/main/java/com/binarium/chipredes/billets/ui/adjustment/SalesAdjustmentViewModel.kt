package com.binarium.chipredes.billets.ui.adjustment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.billets.data.model.AdjustablePurchase
import com.binarium.chipredes.wolke2.domain.usecases.AdjustSaleUseCase
import com.binarium.chipredes.wolke2.models.AdjustSaleResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SalesAdjustmentViewModel @Inject constructor(
    private val adjustSaleUseCase: AdjustSaleUseCase,
) : ViewModel() {

    lateinit var missingSales: List<AdjustablePurchase>

    private val _adjustmentProgress = MutableLiveData<Int>()
    val adjustmentProgress: LiveData<Int>
        get() = _adjustmentProgress

    private var adjustmentStarted = false
    private val _adjustmentFinished = MutableLiveData<Boolean>()
    val adjustmentFinished: LiveData<Boolean>
        get() = _adjustmentFinished

    private val resultsList = mutableListOf<AdjustSaleResult>()
    private val _results = MutableLiveData<List<AdjustSaleResult>>()
    val results: LiveData<List<AdjustSaleResult>>
        get() = _results

    private var adjustedSalesCounter = 0

    fun adjustSales() {
        if (adjustmentStarted) return
        Timber.d("Adjusting!!!")
        viewModelScope.launch(Dispatchers.IO) {
            adjustmentStarted = true
            for (adjustableSale in missingSales) {
                val result = adjustSaleUseCase.invoke(adjustableSale)
                Timber.d("Adjusting $adjustableSale, result was $result")
                delay(500)
                addResult(result)
                incrementProgress()
            }
            _adjustmentFinished.postValue(true)
        }
    }

    private fun incrementProgress() {
        adjustedSalesCounter++
        val currentProgress = adjustedSalesCounter * 100 / missingSales.size
        _adjustmentProgress.postValue(currentProgress)
    }

    private fun addResult(result: AdjustSaleResult) {
        resultsList.add(0, result)
        _results.postValue(resultsList.toList())
    }
}
