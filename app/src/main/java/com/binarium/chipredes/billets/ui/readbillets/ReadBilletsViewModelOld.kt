package com.binarium.chipredes.billets.ui.readbillets

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.billets.BilletTransactionPlates
import com.binarium.chipredes.billets.domain.usecases.CancelBilletReservationUseCase
import com.binarium.chipredes.emax.EmaxApiService
import com.binarium.chipredes.emax.EmaxDBManager
import com.binarium.chipredes.local.LocalApiService
import com.binarium.chipredes.local.services.StartBilletSalePost
import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.wolke.models.BilletData
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke2.Wolke2ApiService
import com.binarium.chipredes.wolke2.domain.usecases.GetClientStationAccountUseCase
import com.binarium.chipredes.wolke2.models.ReserveTransactionResult
import com.binarium.chipredes.wolke2.services.ReserveBilletTransaction
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import org.json.JSONObject
import timber.log.Timber
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class ReadBilletsViewModelOld @Inject constructor(
    private val wolke2ApiService: Wolke2ApiService,
    private val localApiService: LocalApiService,
    sharedPreferences: SharedPreferences,
    private val emaxApiService: EmaxApiService,
    private val emaxDBManager: EmaxDBManager,
    private val cancelBilletReservationUseCase: CancelBilletReservationUseCase,
    private val getClientStationAccountUseCase: GetClientStationAccountUseCase,
    private val logger: Logger,
) : ViewModel() {

    var loadingPosition = 0
    var readBillets: List<BilletData>? = null
    var localClientData: LocalClientData? = null
    var plates: BilletTransactionPlates? = null
    private val stationId = sharedPreferences.getString(ChipREDConstants.STATION_MONGO_ID, "")

    private val _reservationSubmitted = MutableLiveData<Boolean>()
    val reservationSubmitted: LiveData<Boolean>
        get() = _reservationSubmitted

    private val _reservationCancelled = MutableLiveData<Boolean>()
    val reservationCancelled: LiveData<Boolean>
        get() = _reservationCancelled

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _billetTransaction = MutableLiveData<ReserveTransactionResult>()
    val billetTransaction: LiveData<ReserveTransactionResult>
        get() = _billetTransaction

    private val _reservedBillets = MutableLiveData<List<BilletData>>()
    val reservedBillets: LiveData<List<BilletData>>
        get() = _reservedBillets

    var loadingMessage = "Creando reservación de saldo"
    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    private val _startSaleErrorMessage = MutableLiveData<String>()
    val startSaleErrorMessage: LiveData<String>
        get() = _startSaleErrorMessage


    init {
        _reservationSubmitted.value = false
    }

    fun createBilletTransaction() {
        viewModelScope.launch {
            _isLoading.value = true

            val isSideAvailable = isLoadingPositionAvailable()
            if (isSideAvailable != 0) {
                _errorMessage.value = "Posición de carga en estado incorrecto"
                return@launch
            }

            // Obtener url de foto
            var urlPlates = ""
            if (plates?.photoPath?.isNotEmpty()!!) {
                Timber.d("Subiendo foto de placas")
                urlPlates = uploadPlatesPhotoFile()
                Timber.d("Foto subida $urlPlates")
            }

            // Filter just active billets
            readBillets = readBillets?.filter { it.estatus == "AC" }

            // Map billets objects to billet id's list
            val billetIds = readBillets!!.map {
                it.id
            }.toList()

            try {
                // Run service and get result
                val getReservationResponse = wolke2ApiService.createBilletTransaction(
                    ReserveBilletTransaction(
                        localClientData?.id!!,
                        stationId!!,
                        billetIds,
                        loadingPosition.toString(),
                        urlPlates,
                        plates?.plates!!
                    )
                )

                if (getReservationResponse.isSuccessful) {
                    // Obtener payload
                    val getReservationResult = getReservationResponse.body()

                    // Create list for add reserved billets
                    val reservedBillets = mutableListOf<BilletData>()

                    // Add to the list the received reserved billets
                    for (billetData in readBillets!!) {
                        if (getReservationResult?.foliosBilletes?.contains(billetData.folio) == true) {
                            reservedBillets.add(billetData)
                        }
                    }

                    // Set list to observable live data
                    _reservedBillets.value = reservedBillets.toList()

                    // Set billet transaction object
                    _billetTransaction.value = getReservationResult!!
                    postClientDataEvent()
                } else {
                    // Obtener mensaje de error!!
                    val errorMessage = getReservationResponse.errorBody()?.string()
                    val errorBody = JSONObject(errorMessage!!)
                    val message = errorBody.getString("message")
                    _errorMessage.value = message
                    logger.postMessage(
                        message = "Error al crear reservacion de saldo (Wolke2): $message",
                        level = LogLevel.WARNING,
                        client = localClientData?.id.orEmpty(),
                        balance = localClientData?.saldo ?: -1.0
                    )
                }

                // Report fetched data
                _isLoading.value = false
            } catch (e: Exception) {
                Timber.e("Error al crear reservacion de saldo")
                logger.postMessage(
                    message = "Error al crear reservacion de saldo: ${e.message}",
                    level = LogLevel.ERROR,
                    client = localClientData?.id.orEmpty(),
                    balance = localClientData?.saldo ?: -1.0
                )
                _errorMessage.value = "Error al crear reservación de saldo"
            }
        }
    }

    fun cancelReservation() {
        viewModelScope.launch(Dispatchers.IO) {
            _billetTransaction.value?.id?.let {
                try {
                    val cancelResult = cancelBilletReservationUseCase(it.id)
                    logger.postMessage(
                        message = "Resultado de cancelacion de reservacion de saldo: ${cancelResult.message}",
                        level = LogLevel.INFO,
                        client = localClientData?.id.orEmpty()
                    )
                    Timber.d("Cancelling reservation result -> ${cancelResult.message}")
                } catch (e: Exception) {
                    logger.postMessage(
                        message = "Error al cancelar reservacion de saldo: ${e.message}",
                        level = LogLevel.ERROR,
                        client = localClientData?.id.orEmpty()
                    )
                    Timber.e("Error al cancelar reservación de saldo $e")
                }
                _reservationCancelled.postValue(true)
            }
        }
    }

    fun startSale() {
        viewModelScope.launch(Dispatchers.IO) {
            loadingMessage = "Iniciando venta..."
            _isLoading.postValue(true)

            try {
                if (!emaxApiService.testConnection()) {
                    _startSaleErrorMessage.postValue("No responde servidor EMAX")
                    return@launch
                }
            } catch (e: Exception) {
                _startSaleErrorMessage.postValue("Error al probar conexión con EMAX")
                logger.postMessage("Error al probar conexion con Emax", LogLevel.ERROR)
                return@launch
            }

            // Get auth from emax
            val emaxAuthResult = emaxApiService.getAuth(localClientData!!)
            if (emaxAuthResult == null) {
                _startSaleErrorMessage.postValue("Error al obtener autorización de emax")
                logger.postMessage("Error al obtener autorización de emax", LogLevel.ERROR)
                return@launch
            } else if (emaxAuthResult.error.isNotEmpty()) {
                _startSaleErrorMessage.postValue(emaxAuthResult.error)
                logger.postMessage(
                    "Error al obtener autorización de emax (Emax): ${emaxAuthResult.error}",
                    LogLevel.ERROR
                )
                return@launch
            }

            // Create sale data for start
            val billetSaleData = _billetTransaction.value
            val startBilletSalePost = StartBilletSalePost(
                reservationId = billetSaleData?.id?.id!!,
                clientAccountId = billetSaleData.cuenta?.id!!,
                clientId = billetSaleData.cliente?.id!!,
                loadingPosition = billetSaleData.posicionCarga?.toInt()!!,
                cantidad = billetSaleData.cantidad!!,
                autorizacion = emaxAuthResult.authNumber
            )

            // Request billet sale start
            // LocalService.host = localUrl
            try {
                val startSaleResponse = localApiService.startBilletSale(startBilletSalePost)
                val startSaleJsonResponse = responseToJson(startSaleResponse)

                // Evaluate result
                if (startSaleJsonResponse.has("ok")) {
                    // it worked!
                    _reservationSubmitted.postValue(true)
                    logger.postMessage(
                        "Reservacion enviada al dispensario",
                        LogLevel.INFO,
                        client = localClientData?.id.orEmpty()
                    )
                } else {
                    // Not worked, get error message
                    val errorMessage: String = if (startSaleJsonResponse.has("aviso")) {
                        startSaleJsonResponse.getString("aviso")
                    } else {
                        startSaleJsonResponse.getString("error")
                    }
                    // Report error message
                    _startSaleErrorMessage.postValue(errorMessage)
                    logger.postMessage(
                        message = "Error al enviar preset(Local): $errorMessage",
                        level = LogLevel.ERROR,
                        client = localClientData?.id.orEmpty()
                    )
                }
            } catch (e: Exception) {
                _startSaleErrorMessage.postValue("Error al enviar preset")
                logger.postMessage(
                    message = "Error al enviar preset: ${e.message}",
                    level = LogLevel.ERROR,
                    client = localClientData?.id.orEmpty()
                )
            }
        }
    }

    private suspend fun responseToJson(responseBody: ResponseBody) = withContext(Dispatchers.IO) {
        val responseBodyStr = responseBody.string()
        return@withContext JSONObject(responseBodyStr)
    }

    private suspend fun isLoadingPositionAvailable() =
        withContext(Dispatchers.IO) {
            return@withContext emaxDBManager.getLoadingPositionState(loadingPosition)
        }

    private suspend fun uploadPlatesPhotoFile(): String = withContext(Dispatchers.IO) {
        var photoUrl = ""
        val photoDateFormat = SimpleDateFormat("yyyy-MM-dd_HHmmss", Locale.ROOT)
        val timeStamp = photoDateFormat.format(Date())

        val storageReference = FirebaseStorage.getInstance().reference.child(
            "${localClientData?.id!!}/fotos_placas/$timeStamp.jpg"
        )

        val imageFile = File(plates?.photoPath!!)
        val size = imageFile.length().toInt()
        val data = ByteArray(size)

        try {
            val buf = BufferedInputStream(FileInputStream(imageFile))
            buf.read(data, 0, data.size)
            buf.close()

            storageReference.putBytes(data).await()
            val downloadUri = storageReference.downloadUrl.await()
            photoUrl = downloadUri.toString()

        } catch (e: Exception) {
            e.printStackTrace()
            Timber.e(e, "Error al subir archivo")
        }

        return@withContext photoUrl
    }

    fun onReservationSubmitted() {
        _reservationSubmitted.postValue(false)
    }

    fun onReservationCancelled() {
        _reservationCancelled.postValue(false)
    }

    private fun postClientDataEvent() {
        viewModelScope.launch(Dispatchers.IO) {
            localClientData?.id?.let {
                getClientStationAccountUseCase.invoke(it)?.let { account ->
                    val balance = account.saldo
                    logger.postMessage(
                        message = "Reservacion de saldo creada",
                        level = LogLevel.INFO,
                        client = account.cliente.id,
                        balance = balance
                    )
                }
            }
        }
    }
}
