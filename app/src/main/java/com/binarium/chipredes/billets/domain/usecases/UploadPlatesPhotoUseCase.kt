package com.binarium.chipredes.billets.domain.usecases

import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject

class UploadPlatesPhotoUseCase @Inject constructor(
    private val firebaseStorage: FirebaseStorage,
) {

    suspend operator fun invoke(
        photoPath: String,
        clientId: String,
    ): String = withContext(Dispatchers.IO) {
        var photoUrl = ""
        val photoDateFormat = SimpleDateFormat("yyyy-MM-dd_HHmmss", Locale.ROOT)
        val timeStamp = photoDateFormat.format(Date())

        val storageReference = firebaseStorage.reference.child(
            "${clientId.ifEmpty { "unknown" }}/fotos_placas/$timeStamp.jpg"
        )

        val imageFile = File(photoPath)
        val size = imageFile.length().toInt()
        val data = ByteArray(size)

        try {
            val buf = BufferedInputStream(FileInputStream(imageFile))
            buf.read(data, 0, data.size)
            buf.close()

            storageReference.putBytes(data).await()
            val downloadUri = storageReference.downloadUrl.await()
            photoUrl = downloadUri.toString()

        } catch (e: Exception) {
            e.printStackTrace()
            Timber.e(e, "Error al subir archivo")
        }

        return@withContext photoUrl
    }
}
