package com.binarium.chipredes.billets.ui.adjustment

import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.binarium.chipredes.GenericDialog
import com.binarium.chipredes.R
import com.binarium.chipredes.billets.ui.adjustment.adapters.AdjustableSalesAdapter
import com.binarium.chipredes.databinding.FragmentMissingSalesBinding
import com.binarium.chipredes.searchclient.SelectedClientViewModel
import com.binarium.chipredes.utils.extensions.addSeparators
import com.binarium.chipredes.utils.extensions.setDatePicker
import com.binarium.chipredes.utils.extensions.setVisible
import com.binarium.chipredes.wolke.models.LocalClientData
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class MissingSalesFragment : Fragment() {

    private val selectedClientViewModel: SelectedClientViewModel by activityViewModels()
    private val viewModelMissing: MissingSalesViewModel by viewModels()

    private lateinit var binding: FragmentMissingSalesBinding
    private lateinit var emaxPurchasesAdapter: AdjustableSalesAdapter
    private lateinit var chipRedPurchasesAdapter: AdjustableSalesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        selectedClientViewModel.selectedClient.observe(this) {
            Timber.i("Nuevo cliente seleccionado ${it.email}")
            viewModelMissing.onNewClientSelected(it)
        }

        if (selectedClientViewModel.selectedClient.value == null) {
            Timber.i("Navegar a SearchViewFragment")
            navigateToSearchFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_missing_sales,
            container,
            false
        )

        Timber.d("onCreate View!!")
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.movimientos_cuenta_menu, menu)
    }

    override fun onResume() {
        super.onResume()

        requireActivity().onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    requireActivity().finish()
                }
            })

        activity?.title = "Conciliación de ventas"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.searchLocalClientFragment) {
            navigateToSearchFragment()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initAdapters()
        initObservers()
        viewModelMissing.reloadPurchases()
    }

    private fun initViews() {
        binding.startDate.setDatePicker(requireActivity().supportFragmentManager) {
            viewModelMissing.onStartDateSelected(it)
        }
        binding.endDate.setDatePicker(requireActivity().supportFragmentManager) {
            viewModelMissing.onEndDateSelected(it)
        }
        binding.adjustCv.setOnClickListener {
            viewModelMissing.missingSales?.let { sales ->
                findNavController().navigate(
                    MissingSalesFragmentDirections.missingSalesToAdjust(sales.toTypedArray())
                )
            }
        }
    }

    private fun initAdapters() {
        emaxPurchasesAdapter = AdjustableSalesAdapter()
        chipRedPurchasesAdapter = AdjustableSalesAdapter()
        binding.emaxPurchasesList.apply {
            adapter = emaxPurchasesAdapter
            addSeparators()
        }
        binding.chipRedPurchasesList.apply {
            adapter = chipRedPurchasesAdapter
            addSeparators()
        }
    }

    private fun initObservers() {
        viewModelMissing.loading.observe(viewLifecycleOwner) {
            binding.progressBar.setVisible(it)
            binding.chipRedPurchasesCardView.setVisible(it.not())
            binding.emaxPurchasesCardView.setVisible(it.not())
        }

        viewModelMissing.clientData.observe(viewLifecycleOwner) {
            setClientData(it)
        }

        viewModelMissing.chipRedPurchases.observe(viewLifecycleOwner) {
            chipRedPurchasesAdapter.submitList(it)
        }

        viewModelMissing.emaxPurchases.observe(viewLifecycleOwner) {
            emaxPurchasesAdapter.submitList(it)
        }

        viewModelMissing.missingPurchasesResult.observe(viewLifecycleOwner) { missingPurchasesResult ->
            val noMissingPurchases = missingPurchasesResult.quantity == 0
            val resultText = "${missingPurchasesResult.quantity} " +
                    "consumos\n${missingPurchasesResult.amountSum}"
            binding.missingPurchasesCv.visibility = View.VISIBLE
            binding.missingPurchasesTv.text = resultText
            binding.missingPurchasesCv.setCardBackgroundColor(
                if (noMissingPurchases) ContextCompat.getColor(requireContext(),
                    R.color.green_money)
                else
                    ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)
            )
            binding.adjustCv.visibility = if (noMissingPurchases) View.GONE else View.VISIBLE
        }

        viewModelMissing.errorMessage.observe(viewLifecycleOwner) { error ->
            if (error.isNotEmpty()) {
                GenericDialog(
                    "Error",
                    error,
                    {},
                    null,
                    requireContext()
                ).show()
            }
        }
    }

    private fun setClientData(localClientData: LocalClientData) {
        binding.clientEt.setText(localClientData.nombre)
    }

    private fun navigateToSearchFragment() {
        findNavController().navigate(
            MissingSalesFragmentDirections.missingSalesToSearchClient()
        )
    }
}
