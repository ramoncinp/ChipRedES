package com.binarium.chipredes.billets.ui.adjustment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.wolke2.domain.usecases.GetLocalClientUseCase
import com.binarium.chipredes.billets.data.model.AdjustablePurchase
import com.binarium.chipredes.billets.data.model.EmaxPurchasesRequest
import com.binarium.chipredes.billets.data.model.MissingPurchasesResult
import com.binarium.chipredes.billets.data.model.WolkePurchasesRequest
import com.binarium.chipredes.billets.domain.usecases.CompareAdjustablePurchasesUseCase
import com.binarium.chipredes.emax.domain.usecases.GetAdjustableEmaxPurchasesUseCase
import com.binarium.chipredes.utils.DateUtils
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke2.domain.usecases.GetAdjustablePurchasesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*
import javax.inject.Inject

private const val MAX_SIZE = 100
private const val MAX_DATE_RANGE = 2678400000L // 31 days in milliseconds

@HiltViewModel
class MissingSalesViewModel @Inject constructor(
    private val getLocalClientUseCase: GetLocalClientUseCase,
    private val getAdjustableEmaxPurchasesUseCase: GetAdjustableEmaxPurchasesUseCase,
    private val getAdjustableChipRedPurchasesUseCase: GetAdjustablePurchasesUseCase,
    private val compareAdjustablePurchasesUseCase: CompareAdjustablePurchasesUseCase,
) : ViewModel() {

    var startDate: Date? = null
    var endDate: Date? = null
    var missingSales: List<AdjustablePurchase>? = null
    private val clientChipRedId: String
        get() = selectedClient?.id!!

    private val _clientData = MutableLiveData<LocalClientData>()
    val clientData: LiveData<LocalClientData>
        get() = _clientData
    private val selectedClient: LocalClientData?
        get() = _clientData.value

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    private val _chipRedPurchases = MutableLiveData<List<AdjustablePurchase>>()
    val chipRedPurchases: LiveData<List<AdjustablePurchase>>
        get() = _chipRedPurchases

    private val _emaxPurchases = MutableLiveData<List<AdjustablePurchase>>()
    val emaxPurchases: LiveData<List<AdjustablePurchase>>
        get() = _emaxPurchases

    private val _missingPurchasesResult = MutableLiveData<MissingPurchasesResult>()
    val missingPurchasesResult: LiveData<MissingPurchasesResult>
        get() = _missingPurchasesResult

    private fun canFetchPurchases() = selectedClient != null && startDate != null && endDate != null

    fun onNewClientSelected(queriedClient: LocalClientData) {
        viewModelScope.launch {
            getClientAccountData(queriedClient)
        }
    }

    fun reloadPurchases() {
        if (canFetchPurchases().not()) return
        viewModelScope.launch {
            getAndComparePurchases()
        }
    }

    private fun onNewDateSelected() {
        if (canFetchPurchases().not()) return
        val dateRangeError = validateDateRange()
        dateRangeError?.let {
            clearPurchases()
            _errorMessage.value = it
            return
        }
        viewModelScope.launch {
            getAndComparePurchases()
        }
    }

    private fun validateDateRange(): String? = if (startDate != null && endDate != null) {
        val dateRange = endDate!!.time - startDate!!.time
        if (dateRange > MAX_DATE_RANGE) "El rango de fechas no puede ser mayor a 31 días" else null
    } else {
        null
    }

    private fun clearPurchases() {
        _emaxPurchases.value = listOf()
        _chipRedPurchases.value = listOf()
        _missingPurchasesResult.value =
            MissingPurchasesResult(0, getSumOfMissingPurchases(listOf()))
    }

    private suspend fun getClientAccountData(queriedClient: LocalClientData) {
        _clientData.value = getLocalClientUseCase.invoke(queriedClient.id!!)
    }

    private suspend fun getEmaxPurchases(): List<AdjustablePurchase> {
        try {
            val clientId = selectedClient?.idClienteLocal
            if (clientId != null && startDate != null && endDate != null) {
                val emaxPurchasesRequest = EmaxPurchasesRequest(
                    clientId,
                    startDate!!,
                    endDate!!
                )
                return getAdjustableEmaxPurchasesUseCase(clientChipRedId, emaxPurchasesRequest)
            }
        } catch (e: Exception) {
            Timber.e("Error getting the emax purchases")
        }

        return emptyList()
    }

    private suspend fun getChipRedPurchases(): List<AdjustablePurchase> {
        try {
            val clientId = selectedClient?.id
            val accountId = selectedClient?.idCuenta
            if (clientId != null && accountId != null && startDate != null && endDate != null) {
                val purchasesRequest = WolkePurchasesRequest(
                    clientId,
                    accountId,
                    startDate!!,
                    endDate!!
                )
                return getAdjustableChipRedPurchasesUseCase(clientChipRedId, purchasesRequest)
            }
        } catch (e: Exception) {
            Timber.e("Error al obtener consumos de ChipRED")
        }

        return emptyList()
    }

    private suspend fun getAndComparePurchases() {
        _loading.value = true
        val emaxPurchases = getEmaxPurchases()
        Timber.d("EmaxPurchases: $emaxPurchases")

        if (isPurchasesLimitReached(emaxPurchases.size).not()) {
            val chipRedPurchases = getChipRedPurchases()
            Timber.d("ChipRedPurchases: $chipRedPurchases")

            val missingPurchases = compareAdjustablePurchasesUseCase(emaxPurchases, chipRedPurchases)
            Timber.d("MissingPurchases: $missingPurchases")

            val finalChipRedPurchases = mutableListOf<AdjustablePurchase>()
            finalChipRedPurchases.addAll(missingPurchases)
            finalChipRedPurchases.addAll(chipRedPurchases)
            _emaxPurchases.value = emaxPurchases
            _chipRedPurchases.value = finalChipRedPurchases
            _missingPurchasesResult.value = MissingPurchasesResult(
                missingPurchases.size,
                getSumOfMissingPurchases(missingPurchases)
            )
            missingSales = missingPurchases
        }

        _loading.value = false
    }

    private fun getSumOfMissingPurchases(missingPurchases: List<AdjustablePurchase>): String {
        var amount = 0.0
        missingPurchases.forEach {
            amount += it.amount
        }
        return "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(amount)
    }

    fun onStartDateSelected(selectedStartDateString: String) {
        val startDateString = "$selectedStartDateString 00:00:00"
        startDate = DateUtils.dateStringToObject(startDateString)
        onNewDateSelected()
    }

    fun onEndDateSelected(selectedEndDateString: String) {
        val endDateString = "$selectedEndDateString 23:59:59"
        endDate = DateUtils.dateStringToObject(endDateString)
        onNewDateSelected()
    }

    private fun isPurchasesLimitReached(listSize: Int): Boolean {
        return if (listSize > MAX_SIZE) {
            val message = "La cantidad de consumos a conciliar " +
                    "es mayor al límite (100), intente con un rango de fechas menor"
            _errorMessage.value = message
            true
        } else {
            _errorMessage.value = ""
            false
        }
    }
}
