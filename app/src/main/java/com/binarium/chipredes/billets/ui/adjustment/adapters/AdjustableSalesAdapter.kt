package com.binarium.chipredes.billets.ui.adjustment.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.binarium.chipredes.R
import com.binarium.chipredes.billets.data.model.AdjustablePurchase
import com.binarium.chipredes.databinding.PurchasesToAdjustLayoutBinding
import com.binarium.chipredes.utils.DateUtils

class AdjustableSalesAdapter : ListAdapter<AdjustablePurchase, AdjustableSalesAdapter.ViewHolder>(
    AdjustablePurchaseDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            PurchasesToAdjustLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val purchase = getItem(position)
        holder.bind(purchase)
    }

    class ViewHolder(private val binding: PurchasesToAdjustLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: AdjustablePurchase) {
            val amountText = "\$${item.amount}"
            val quantityText = "${item.quantity}L"
            val ticketText = "Ticket ${item.ticket}"
            binding.amountTv.text = amountText
            binding.quantityTv.text = quantityText
            binding.ticketTv.text = ticketText
            binding.dateTv.text = formatDate(item.date)
            setBackground(item)
        }

        private fun formatDate(date: String): String = try {
            val dateObj = DateUtils.formatoFechaMovimientosCuenta(date, false)
            "${DateUtils.getDayMonthYear(dateObj)} ${DateUtils.getHourWithSeconds(dateObj)}"
        } catch (e: Exception) {
            emaxFormattedDate(date)
        }

        private fun emaxFormattedDate(date: String): String = try {
            val dateObj = DateUtils.formatoFechaEmax(date)
            "${DateUtils.getDayMonthYear(dateObj)} ${DateUtils.getHourWithSeconds(dateObj)}"
        } catch (e: Exception) {
            date
        }

        private fun setBackground(item: AdjustablePurchase) {
            val context = binding.root.context
            if (item.toAdjust) {
                binding.root.setBackgroundColor(ContextCompat.getColor(context,
                    R.color.cancelado_low_alpha))
            } else {
                binding.root.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
            }
        }
    }

    class AdjustablePurchaseDiffCallback : DiffUtil.ItemCallback<AdjustablePurchase>() {
        override fun areItemsTheSame(
            oldItem: AdjustablePurchase,
            newItem: AdjustablePurchase,
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: AdjustablePurchase,
            newItem: AdjustablePurchase,
        ): Boolean = oldItem == oldItem
    }
}
