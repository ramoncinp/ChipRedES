package com.binarium.chipredes.billets.domain.usecases

import com.binarium.chipredes.wolke2.data.Wolke2Repository
import com.binarium.chipredes.wolke2.models.CancelBilletReservationResult
import javax.inject.Inject

class CancelBilletReservationUseCase @Inject constructor(
    private val wolke2Repository: Wolke2Repository
) {

    suspend operator fun invoke(reservationId: String): CancelBilletReservationResult {
        return wolke2Repository.cancelBilletReservation(reservationId)
    }
}
