package com.binarium.chipredes.billets.ui.readbillets

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.billets.data.model.ReadBilletsData
import com.binarium.chipredes.billets.data.model.getActiveBilletsQuantity
import com.binarium.chipredes.billets.data.model.getTotalAmount
import com.binarium.chipredes.billets.ui.utils.InfiniteAnimator
import com.binarium.chipredes.databinding.BottomSheetFragmentReadBilletsBinding
import com.binarium.chipredes.utils.extensions.parcelable
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReadBilletsBottomSheet : BottomSheetDialogFragment() {

    private var _binding: BottomSheetFragmentReadBilletsBinding? = null
    private val viewModel: ReadBilletsViewModel by viewModels()
    private val binding
        get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getBilletData()
    }

    private fun getBilletData() {
        val readBilletsData: ReadBilletsData? = arguments?.parcelable(READ_BILLETS_DATA_PARAM)
        readBilletsData?.let { viewModel.readBilletsData = it }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        _binding = BottomSheetFragmentReadBilletsBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.readBilletsData?.let {
            initViews(it)
            initObservers()
        }
    }

    private fun initViews(readBilletsData: ReadBilletsData) {
        with(binding) {
            val amountText = "$ ${
                ChipREDConstants.MX_AMOUNT_FORMAT.format(
                    readBilletsData.getTotalAmount()
                )
            }"
            binding.presetAmountTv.text = amountText
            billetsQuantityTv.text = readBilletsData.getActiveBilletsQuantity().toString()

            cancelButton.setOnClickListener { dismiss() }
            startButton.setOnClickListener { sendPreset() }
            cancelErrorButton.setOnClickListener { dismiss() }
            retryButton.setOnClickListener { sendPreset(isRetry = true) }
            cancelSendErrorButton.setOnClickListener { dismiss() }
            closeButton.setOnClickListener { closeBottomSheet() }
        }
    }

    private fun sendPreset(isRetry: Boolean = false) {
        showSendPresetAnimation()
        if (isRetry) {
            viewModel.retrySendPreset()
        } else {
            viewModel.sendPreset()
        }
    }

    private fun showSendPresetAnimation() {
        with(binding) {
            sendingPresetAnimation.apply {
                addAnimatorListener(InfiniteAnimator(this))
                playAnimation()
            }
            showAnimationView()
        }
    }

    private fun initObservers() {
        with(viewModel) {
            onError.observe(viewLifecycleOwner) {
                it?.let { message ->
                    binding.errorMessage.text = message
                    binding.closeableErrorGroup.isVisible = true
                    binding.readBilletsGroup.isVisible = false
                    binding.sendingPresetGroup.isVisible = false
                    binding.sendErrorGroup.isVisible = false
                    onErrorObserved()
                }
            }
            onSendError.observe(viewLifecycleOwner) {
                it?.let { message ->
                    binding.sendErrorMessage.text = message
                    binding.closeableErrorGroup.isVisible = false
                    binding.readBilletsGroup.isVisible = false
                    binding.sendingPresetGroup.isVisible = false
                    binding.sendErrorGroup.isVisible = true
                    onSendErrorObserved()
                }
            }
            loadingMessage.observe(viewLifecycleOwner) {
                binding.sendingPresetTv.text = it
            }
            onSuccess.observe(viewLifecycleOwner) { message ->
                if (message.isNotEmpty()) {
                    binding.successText.text = message
                    binding.closeableErrorGroup.isVisible = false
                    binding.readBilletsGroup.isVisible = false
                    binding.sendingPresetGroup.isVisible = false
                    binding.sendErrorGroup.isVisible = false
                    binding.successGroup.isVisible = true
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun closeBottomSheet() {
        val resultIntent = Intent()
        requireActivity().setResult(RESULT_OK, resultIntent)
        dismiss()
        requireActivity().finish()
    }

    private fun BottomSheetFragmentReadBilletsBinding.showAnimationView() {
        sendingPresetGroup.isVisible = true
        readBilletsGroup.isVisible = false
        closeableErrorGroup.isVisible = false
        sendErrorGroup.isVisible = false
    }

    companion object {
        const val TAG = "ReadBilletsBottomSheet"
        const val READ_BILLETS_DATA_PARAM = "ReadBilletsData"

        fun createInstance(params: ReadBilletsData): ReadBilletsBottomSheet {
            val args = bundleOf(READ_BILLETS_DATA_PARAM to params)
            return ReadBilletsBottomSheet().apply {
                arguments = args
                isCancelable = false
            }
        }
    }
}
