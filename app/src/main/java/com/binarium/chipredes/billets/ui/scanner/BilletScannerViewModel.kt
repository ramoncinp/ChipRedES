package com.binarium.chipredes.billets.ui.scanner

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.billets.BilletStatus
import com.binarium.chipredes.billets.domain.usecases.DecodeBilletQrUseCase
import com.binarium.chipredes.billets.domain.usecases.GetBilletsTask
import com.binarium.chipredes.billets.domain.usecases.ProcessPendingReservationsUseCase
import com.binarium.chipredes.billets.domain.usecases.ProcessingReservationsResult
import com.binarium.chipredes.billets.domain.usecases.ValidateClientBalanceResult
import com.binarium.chipredes.billets.domain.usecases.ValidateClientBalanceUseCase
import com.binarium.chipredes.emax.EmaxDBManager
import com.binarium.chipredes.ocr.domain.SavePictureUseCase
import com.binarium.chipredes.wolke.domain.usecases.GetStationMongoIdUseCase
import com.binarium.chipredes.wolke.models.BilletData
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke2.domain.usecases.GetLocalClientUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class BilletScannerViewModel @Inject constructor(
    private val decodeBilletQrUseCase: DecodeBilletQrUseCase,
    private val getBilletsTask: GetBilletsTask,
    private val getLocalClientUseCase: GetLocalClientUseCase,
    private val validateClientBalanceUseCase: ValidateClientBalanceUseCase,
    private val getStationMongoIdUseCase: GetStationMongoIdUseCase,
    private val savePictureUseCase: SavePictureUseCase,
    private val processPendingReservationsUseCase: ProcessPendingReservationsUseCase,
    private val emaxDBManager: EmaxDBManager
) : ViewModel() {

    var loadingPosition = 0
    var saleAmount = 0.0
    var capturedPlates = ""
    var savedCapturePath = ""
    private var clientId: String? = null
    private lateinit var stationId: String

    private val readBilletsList = mutableSetOf<BilletData>()

    var isFetching = MutableLiveData<Boolean>()

    private val _readBillets = MutableLiveData<List<BilletData>>()
    val readBillets: LiveData<List<BilletData>> = _readBillets

    private val _clientData = MutableLiveData<LocalClientData>()
    val clientData: LiveData<LocalClientData> = _clientData

    private val _clientError = MutableLiveData<String>()
    val clientError: LiveData<String> = _clientError

    private val _clientWarning = MutableLiveData<String>()
    val clientWarning: LiveData<String> = _clientWarning

    private val _onReady = MutableLiveData<Boolean>()
    val onReady: LiveData<Boolean> = _onReady

    private val _processingSalesMessage = MutableLiveData<Boolean>()
    val processingSalesMessage: LiveData<Boolean> = _processingSalesMessage

    init {
        isFetching.value = false
        getStationId()
        checkLoadingPositionAvailable { startGetBilletsTask() }
    }

    private fun startGetBilletsTask() {
        viewModelScope.launch {
            getBilletsTask.start().collect { billetData ->
                billetData?.let { onBilletFetched(it) }
                isFetching.postValue(false)
            }
        }
    }

    private fun getStationId() {
        stationId = getStationMongoIdUseCase()
    }

    fun onQrRead(rawValue: String) {
        when (val result = decodeBilletQrUseCase(rawValue)) {
            is DecodeBilletQrUseCase.DecodeBilletQrResult.DecodeSuccess -> {
                val clientId = result.clientId
                val billetId = result.billetId

                if (this.clientId == null) {
                    this.clientId = clientId
                    getAndValidateClientData()
                }

                if (this.clientId?.equals(clientId) == false) {
                    if (_clientError.value == null)
                        _clientError.postValue("Sólo se pueden leer billetes de un cliente")
                } else {
                    getBilletData(billetId)
                }
            }

            is DecodeBilletQrUseCase.DecodeBilletQrResult.DecodeError -> Timber.e(result.message)
        }
    }

    private fun getBilletData(billetId: String) {
        if (getBilletsTask.addBilletToFetch(billetId)) isFetching.postValue(true)
    }

    private fun onBilletFetched(billetData: BilletData) {
        Timber.d("Billet fetched!")
        if (billetData.estatus == BilletStatus.STATUS_ACTIVO && !readBilletsList.contains(billetData)) {
            saleAmount += billetData.monto!!
        }
        readBilletsList.add(billetData)
        _readBillets.postValue(readBilletsList.toList())
    }

    private fun getAndValidateClientData() {
        viewModelScope.launch(Dispatchers.IO) {
            isFetching.postValue(true)
            _processingSalesMessage.postValue(true)
            val client = getClientData()
            val result = validateClientBalance(client)
            if (result is ValidateClientBalanceResult.ClientNonAvailable)
                return@launch

            clientId?.let { processPendingReservations(it) }
            isFetching.postValue(false)
            _processingSalesMessage.postValue(false)
        }
    }

    private suspend fun processPendingReservations(client: String) {
        var validateAgain = true

        getBilletsTask.pause()
        processPendingReservationsUseCase.invoke(client).collect {
            when (it) {
                ProcessingReservationsResult.Error,
                ProcessingReservationsResult.Adjusted -> clearBilletsList()
                ProcessingReservationsResult.Processing -> _processingSalesMessage.postValue(true)
                ProcessingReservationsResult.Done -> validateAgain = false
            }
        }

        if (validateAgain) delay(2000L)
        val secondValidationResult = validateClientBalance(shouldLogError = validateAgain)
        if (secondValidationResult is ValidateClientBalanceResult.Mismatch) {
            _clientError.postValue(secondValidationResult.message)
        }

        _processingSalesMessage.postValue(false)
        getBilletsTask.resume()
    }

    private fun clearBilletsList() {
        getBilletsTask.clear()
        readBilletsList.clear()
        _readBillets.postValue(readBilletsList.toList())
        saleAmount = 0.0
    }

    private suspend fun getClientData(): LocalClientData? {
        clientId?.let { id ->
            val clientData = getLocalClientUseCase(id)
            clientData?.let { data ->
                _clientData.postValue(data)
                return data
            } ?: {
                if (_clientError.value == null) {
                    _clientError.postValue("Cliente no encontrado")
                }
                clientId = null
            }
        }

        return null
    }

    fun removeBillet(billet: BilletData) {
        if (billet.estatus == BilletStatus.STATUS_ACTIVO) saleAmount -= billet.monto!!
        readBilletsList.remove(billet)
        getBilletsTask.removeBillet(billet.id)
        _readBillets.value = readBilletsList.toList()
    }

    private suspend fun validateClientBalance(
        client: LocalClientData? = clientData.value,
        shouldLogError: Boolean = true
    ): ValidateClientBalanceResult {
        return client?.let {
            validateClientBalanceUseCase(it, shouldLogError).also { result ->
                if (result is ValidateClientBalanceResult.Error) {
                    _clientError.postValue(result.message)
                } else if (result is ValidateClientBalanceResult.Success) {
                    _onReady.postValue(true)
                }
            }
        } ?: run {
            _clientError.postValue("Error al obtener datos de cliente")
            ValidateClientBalanceResult.ClientNonAvailable
        }
    }

    fun savePicture(data: ByteArray, baseDir: String) {
        viewModelScope.launch {
            savedCapturePath = savePictureUseCase(data, baseDir)
        }
    }

    fun canSendData(): Boolean {
        val mustCapturePlates = clientData.value?.capturaPlacas
        mustCapturePlates?.let {
            if (it && capturedPlates.isEmpty()) {
                _clientWarning.value = "La captura de placas para este cliente es obligatoria"
                return false
            }
        }

        return true
    }

    private fun checkLoadingPositionAvailable(onSuccess: () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            val state = try {
                emaxDBManager.getLoadingPositionState(loadingPosition)
            } catch (e: Exception) {
                null
            }

            if (state != 0) {
                _clientError.postValue("Posicion de carga en estado incorrecto")
            } else {
                onSuccess.invoke()
            }
        }
    }

    fun resumeGetBilletsTask() {
        getBilletsTask.resume()
    }

    fun pauseGetBilletsTask() {
        getBilletsTask.pause()
    }

    override fun onCleared() {
        super.onCleared()
        getBilletsTask.stop()
    }
}
