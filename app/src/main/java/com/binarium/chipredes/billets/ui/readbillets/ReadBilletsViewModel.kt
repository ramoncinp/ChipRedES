package com.binarium.chipredes.billets.ui.readbillets

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.billets.BilletStatus
import com.binarium.chipredes.billets.data.model.ReadBilletsData
import com.binarium.chipredes.billets.domain.usecases.CreateBilletReservationUseCase
import com.binarium.chipredes.billets.domain.usecases.CreateReservationResult
import com.binarium.chipredes.billets.domain.usecases.UploadPlatesPhotoUseCase
import com.binarium.chipredes.emax.domain.usecases.EmaxAuthResult
import com.binarium.chipredes.emax.domain.usecases.GetEmaxAuthUseCase
import com.binarium.chipredes.emax.domain.usecases.GetLoadingPositionStateUseCase
import com.binarium.chipredes.emax.model.LoadingPositionState
import com.binarium.chipredes.local.domain.StartBilletSaleResult
import com.binarium.chipredes.local.domain.StartBilletSaleUseCase
import com.binarium.chipredes.local.domain.TestLocalConnectionUseCase
import com.binarium.chipredes.local.domain.TestLocalResult
import com.binarium.chipredes.wolke2.models.ReserveTransactionResult
import com.binarium.chipredes.wolke2.models.toResultText
import com.binarium.chipredes.wolke2.models.toStartBilletSalePost
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReadBilletsViewModel @Inject constructor(
    private val getLoadingPositionStateUseCase: GetLoadingPositionStateUseCase,
    private val createBilletReservationUseCase: CreateBilletReservationUseCase,
    private val uploadPlatesPhotoUseCase: UploadPlatesPhotoUseCase,
    private val getEmaxAuthUseCase: GetEmaxAuthUseCase,
    private val startBilletSaleUseCase: StartBilletSaleUseCase,
    private val testLocalConnectionUseCase: TestLocalConnectionUseCase,
) : ViewModel() {

    var readBilletsData: ReadBilletsData? = null
    private var billetReservationData: ReserveTransactionResult? = null

    private val _onError = MutableLiveData<String?>()
    val onError: LiveData<String?>
        get() = _onError

    private val _onSendError = MutableLiveData<String?>()
    val onSendError: LiveData<String?>
        get() = _onSendError

    private val _loadingMessage = MutableLiveData<String>()
    val loadingMessage: LiveData<String>
        get() = _loadingMessage

    private val _onSuccess = MutableLiveData<String>()
    val onSuccess: LiveData<String>
        get() = _onSuccess


    fun sendPreset() {
        viewModelScope.launch(Dispatchers.IO) {

            if (!testConnection()) return@launch

            _loadingMessage.postValue("Validando posicion de carga")
            if (!isSideAvailable()) {
                _onError.postValue("Posición de carga en estado incorrecto")
                return@launch
            }

            readBilletsData?.let { data ->
                val urlPlates = getPlatesUrl(data)

                _loadingMessage.postValue("Creando reservación")
                val createReservationResult = createBilletReservationUseCase(
                    data.client,
                    getActiveBilletsIds(),
                    data.loadingPosition,
                    urlPlates,
                    data.billetTransactionPlates.plates
                )

                if (createReservationResult is CreateReservationResult.Error) {
                    _onError.postValue(createReservationResult.message)
                    return@launch
                }

                // Create reservation result is Success
                billetReservationData =
                    (createReservationResult as CreateReservationResult.Success).data
                billetReservationData?.authAndSendPreset(data)
            }
        }
    }

    fun retrySendPreset() {
        viewModelScope.launch(Dispatchers.IO) {
            if (!testConnection()) return@launch
            readBilletsData?.let { data ->
                billetReservationData?.authAndSendPreset(data)
            }
        }
    }

    private suspend fun ReserveTransactionResult.authAndSendPreset(data: ReadBilletsData) {
        _loadingMessage.postValue("Obteniendo autorización")
        val getEmaxAuth = getEmaxAuthUseCase.invoke(data.client)
        if (getEmaxAuth is EmaxAuthResult.Error) {
            _onSendError.postValue(getEmaxAuth.message)
            return
        }

        val emaxAuth = (getEmaxAuth as EmaxAuthResult.Success).data
        val startSalePost = this.toStartBilletSalePost(emaxAuth)

        _loadingMessage.postValue("Enviando preset")
        val startBilletSaleResult = startBilletSaleUseCase(startSalePost)
        if (startBilletSaleResult is StartBilletSaleResult.Error) {
            _onSendError.postValue(startBilletSaleResult.message)
        } else {
            _onSuccess.postValue(this.toResultText())
        }
    }

    private suspend fun getPlatesUrl(data: ReadBilletsData): String {
        var urlPlates = ""
        val platesPhotoPath = data.billetTransactionPlates.photoPath
        if (platesPhotoPath.isNotEmpty()) {
            urlPlates = uploadPlatesPhotoUseCase.invoke(
                platesPhotoPath, data.client.id.orEmpty()
            )
        }
        return urlPlates
    }

    private suspend fun isSideAvailable(): Boolean {
        return readBilletsData?.loadingPosition?.let {
            val state = getLoadingPositionStateUseCase.invoke(it)
            state == LoadingPositionState.Idle
        } ?: false
    }

    private fun getActiveBilletsIds(): List<String> {
        val activeBillets = readBilletsData?.billets?.filter {
            it.estatus == BilletStatus.STATUS_ACTIVO
        }

        return activeBillets?.map { it.id } ?: emptyList()
    }

    suspend fun testConnection(): Boolean {
        _loadingMessage.postValue("Probando conexión")
        val connectionState = testLocalConnectionUseCase.invoke()
        if (connectionState is TestLocalResult.Error) {
            _onError.postValue(connectionState.message)
            return false
        }
        return true
    }

    fun onErrorObserved() {
        _onError.value = null
    }

    fun onSendErrorObserved() {
        _onSendError.value = null
    }
}
