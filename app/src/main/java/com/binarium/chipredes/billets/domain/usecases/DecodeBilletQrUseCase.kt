package com.binarium.chipredes.billets.domain.usecases

import javax.inject.Inject

class DecodeBilletQrUseCase @Inject constructor() {

    operator fun invoke(billetQr: String): DecodeBilletQrResult {
        return if (billetQr.contains("|") && billetQr.length >= 3) {
            val pipeIdx = billetQr.indexOf("|")
            val billetId = billetQr.substring(pipeIdx + 1)
            val clientId = billetQr.substring(0, pipeIdx)
            DecodeBilletQrResult.DecodeSuccess(billetId, clientId)
        } else {
            DecodeBilletQrResult.DecodeError("QR leído no es un billete electrónico")
        }
    }

    sealed class DecodeBilletQrResult {
        class DecodeError(val message: String) : DecodeBilletQrResult()
        class DecodeSuccess(val billetId: String, val clientId: String) : DecodeBilletQrResult()
    }
}
