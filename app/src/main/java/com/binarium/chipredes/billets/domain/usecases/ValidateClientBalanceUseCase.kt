package com.binarium.chipredes.billets.domain.usecases

import com.binarium.chipredes.emax.domain.usecases.GetClientBalanceUseCase
import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke2.domain.usecases.GetClientStationAccountUseCase
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.abs

class ValidateClientBalanceUseCase @Inject constructor(
    private val getClientBalanceUseCase: GetClientBalanceUseCase,
    private val getClientStationAccountUseCase: GetClientStationAccountUseCase,
    private val logger: Logger
) {

    suspend operator fun invoke(localClientData: LocalClientData, shouldLog: Boolean = true): ValidateClientBalanceResult {
        try {
            val clientEmaxId = localClientData.idClienteLocal.toString()
            val clientEmaxBalance = getClientBalanceUseCase(clientEmaxId)
            val clientStationAccount = getClientStationAccountUseCase(localClientData.id.toString())
            val chipredBalance = clientStationAccount?.saldo

            if (clientEmaxBalance != null && chipredBalance != null) {
                val emaxBalInt = clientEmaxBalance.toInt()
                val chipRedBalInt = chipredBalance.toInt()

                if (abs(chipRedBalInt - emaxBalInt) > 1) {
                    if (shouldLog) {
                        logger.postMessage(
                            message = "Saldo diferente, ChipRED: $chipredBalance, Emax: $clientEmaxBalance",
                            level = LogLevel.ERROR,
                            client = localClientData.id.orEmpty(),
                            balance = chipredBalance
                        )
                    }

                    return ValidateClientBalanceResult.Mismatch(
                        "El saldo del cliente es diferente al registrado en la estación. " +
                            "Por favor, comuníquelo con el supervisor."
                    )
                }
            } else {
                logger.postMessage(
                    message = "Alguno de los saldos es nulo, ChipRED: $chipredBalance, Emax: $clientEmaxBalance",
                    level = LogLevel.ERROR,
                    client = localClientData.id.orEmpty(),
                    balance = chipredBalance ?: -1.0
                )
                return ValidateClientBalanceResult.Error("No se pudo validar el saldo del cliente. Por favor, intente de nuevo")
            }
        } catch (e: java.lang.Exception) {
            Timber.e(e, "Error al validar datos de cliente: ${e.message}")
            logger.postMessage(
                message = "Error al validar saldo de cliente: ${e.message}",
                level = LogLevel.INFO,
                client = localClientData.id.orEmpty(),
                balance = localClientData.saldo ?: -1.0
            )
            return ValidateClientBalanceResult.Error("No se pudo validar el saldo del cliente. Por favor, intente de nuevo")
        }

        if (shouldLog) {
            logger.postMessage(
                message = "Saldo ok",
                level = LogLevel.INFO,
                client = localClientData.id.orEmpty(),
                balance = localClientData.saldo ?: -1.0
            )
        }
        return ValidateClientBalanceResult.Success
    }
}

sealed class ValidateClientBalanceResult {
    object Success : ValidateClientBalanceResult()
    object ClientNonAvailable : ValidateClientBalanceResult()
    class Error(val message: String) : ValidateClientBalanceResult()
    class Mismatch(val message: String) : ValidateClientBalanceResult()
}
