package com.binarium.chipredes.billets.data.model

import android.os.Parcelable
import com.binarium.chipredes.billets.BilletStatus
import com.binarium.chipredes.billets.BilletTransactionPlates
import com.binarium.chipredes.wolke.models.BilletData
import com.binarium.chipredes.wolke.models.LocalClientData
import kotlinx.parcelize.Parcelize

@Parcelize
data class ReadBilletsData(
    val billets: List<BilletData>,
    val client: LocalClientData,
    val billetTransactionPlates: BilletTransactionPlates,
    val loadingPosition: Int,
) : Parcelable

fun ReadBilletsData.getTotalAmount(): Double {
    var total = 0.0
    billets.filter {
        it.estatus == BilletStatus.STATUS_ACTIVO
    }.forEach { billet ->
        billet.monto?.let { total += it }
    }
    return total
}

fun ReadBilletsData.getActiveBilletsQuantity(): Int {
    return billets.filter { it.estatus == BilletStatus.STATUS_ACTIVO }.size
}
