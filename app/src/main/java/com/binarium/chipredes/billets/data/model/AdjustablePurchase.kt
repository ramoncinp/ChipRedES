package com.binarium.chipredes.billets.data.model

import android.os.Parcelable
import com.binarium.chipredes.emax.model.EmaxPurchase
import com.binarium.chipredes.wolke2.models.MovimientoCuenta
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class AdjustablePurchase(
    val id: String,
    @Json(name = "num_ticket") val ticket: String,
    @Json(name = "costo") val amount: Double,
    @Json(name = "cantidad") val quantity: Double,
    @Json(name = "fecha_hora") val date: String,
    @Json(name = "producto") val product: String,
    @Json(name = "precio_unitario") val unitPrice: Double,
    @Json(name = "tag_despachador") val pumperTag: String,
    @Json(name = "id_cliente") val clientId: String,
    @Json(name = "id_estacion") val stationId: String,
    @Json(name = "tipo_venta") val saleType: String = "1",
    @Json(name = "tag_cliente") val clientTag: String = "no_disponible",
    @Json(name = "numero_estacion") val stationNumber: String,
    @Json(name = "posicion_carga") val loadingPosition: String,
    @Json(name = "id_despachador_emax") val pumperId: String,
    @Json(name = "tipo_cliente") val clientType: String = "1",
    @Json(name = "numero_dispensario") val dispenserNumber: String,
    @Json(name = "ids_billetes") val billets: List<String> = listOf(),
    @Json(name = "placas") val plates: String = "",
    @Json(name = "url_placas") val plates_url: String = "",
    var toAdjust: Boolean = false,
) : Parcelable

@Parcelize
data class StationClientInfo(
    val clientId: String,
    val stationId: String,
    val stationNumber: String,
) : Parcelable

fun MovimientoCuenta.toAdjustablePurchase(
    stationClientInfo: StationClientInfo,
): AdjustablePurchase? {
    val purchase = consumo?.let {
        AdjustablePurchase(
            it.numTicket,
            it.numTicket,
            it.costo,
            it.cantidad,
            it.fechaHora,
            it.producto.id,
            it.precioUnitario,
            it.idDespachadorEmax,
            clientId = stationClientInfo.clientId,
            stationId = stationClientInfo.stationId,
            stationNumber = stationClientInfo.stationNumber,
            loadingPosition = it.posicionCarga,
            pumperId = it.idDespachadorEmax,
            dispenserNumber = it.numeroDispensario
        )
    }
    return purchase
}

fun EmaxPurchase.toAdjustablePurchase(
    stationClientInfo: StationClientInfo,
): AdjustablePurchase = AdjustablePurchase(
    id.toString(),
    ticket,
    amount,
    liters,
    dateTime,
    product,
    unitPrice,
    pumperId,
    clientId = stationClientInfo.clientId,
    stationId = stationClientInfo.stationId,
    stationNumber = stationClientInfo.stationNumber,
    loadingPosition = loadingPosition,
    pumperId = pumperId,
    dispenserNumber = dispenser
)
