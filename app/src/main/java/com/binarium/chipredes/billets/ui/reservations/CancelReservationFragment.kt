package com.binarium.chipredes.billets.ui.reservations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.databinding.FragmentCancelReservationBinding
import com.binarium.chipredes.utils.extensions.setVisible
import com.binarium.chipredes.wolke2.models.ReservacionSaldo
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CancelReservationFragment : Fragment() {

    private val viewModel: CancelReservationViewModel by viewModels()

    private var _binding: FragmentCancelReservationBinding? = null
    private val binding
        get() = _binding!!

    private val args by navArgs<CancelReservationFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentCancelReservationBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initReservationData()
        initViews()
        initObservers()
    }

    private fun initViews() {
        binding.cancelButton.setOnClickListener {
            viewModel.cancelBilletReservation()
        }
    }

    private fun initReservationData() {
        with(args.reservation) {
            viewModel.onReservation(this)
            bindReservation(this)
        }
    }

    private fun initObservers() {
        viewModel.onSuccess.observe(viewLifecycleOwner) {
            if (it) findNavController().navigateUp()
        }

        viewModel.onError.observe(viewLifecycleOwner) {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            findNavController().navigateUp()
        }

        viewModel.loading.observe(viewLifecycleOwner) {
            with(binding) {
                progressBar.setVisible(it)
                cancelReservationMessage.setVisible(it.not())
                reservationAmountTv.setVisible(it.not())
                cancelButton.setVisible(it.not())
            }
        }
    }

    private fun bindReservation(reservation: ReservacionSaldo) {
        val amountText = "\$${ChipREDConstants.MX_AMOUNT_FORMAT.format(reservation.cantidad)}"
        binding.reservationAmountTv.text = amountText
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
