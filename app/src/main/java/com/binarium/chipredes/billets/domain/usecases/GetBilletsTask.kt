package com.binarium.chipredes.billets.domain.usecases

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber
import javax.inject.Inject

private const val INITIAL_ELEMENT_IDX = 0
private const val DEBOUNCE_TIME_MS = 250L

class GetBilletsTask @Inject constructor(
    private val getBilletDataUseCase: GetBilletDataUseCase,
) {
    private var isActive = true
    private var isPaused = false
    private val billetsToFetch = mutableListOf<String>()
    private val readCodes = mutableSetOf<String>()

    fun start() = flow {
        while (isActive) {
            if (!isPaused) {
                delay(DEBOUNCE_TIME_MS)
                Timber.d("Getting billets")
                if (billetsToFetch.isNotEmpty()) {
                    val billetId = billetsToFetch[INITIAL_ELEMENT_IDX]
                    val billetData = getBilletDataUseCase(billetId)
                    if (billetData == null) readCodes.remove(billetId)

                    emit(billetData)
                    billetsToFetch.removeAt(INITIAL_ELEMENT_IDX)
                }
            }
        }
        Timber.d("Billet task finished")
    }.flowOn(Dispatchers.IO)

    fun pause() {
        isPaused = true
    }

    fun resume() {
        isPaused = false
    }

    fun stop() {
        isActive = false
    }

    fun addBilletToFetch(billetId: String): Boolean {
        return if (readCodes.contains(billetId).not() && !isPaused) {
            readCodes.add(billetId)
            billetsToFetch.add(billetId)
            true
        } else {
            false
        }
    }

    fun removeBillet(billetId: String) {
        readCodes.remove(billetId)
    }

    fun clear() {
        readCodes.clear()
    }
}
