package com.binarium.chipredes.internationalization;

import android.content.Context;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class LanguageManager
{
    //Valores que se pueden buscar
    public static final String CODIGO_FISCAL = "codigo_fiscal";
    public static final String DESPACHADOR = "despachador";
    public static final String EFECTIVO = "efectivo";

    private static JSONObject getStringsDictionary(Context context)
    {
        JSONObject mStrings;
        try
        {
            InputStream is = context.getAssets().open("strings.json");
            int size = is.available();
            byte[] buffer = new byte[size];

            //Verificar que se pueda leer el archivo
            if (is.read(buffer) == -1)
            {
                is.close();
                return null;
            }
            is.close();

            //Crear objeto JSON
            mStrings = new JSONObject(new String(buffer, StandardCharsets.UTF_8));

            //Obtener el nombre del país
            String mCountry = SharedPreferencesManager.getString(context,
                    ChipREDConstants.COUNTRY, "mexico");

            //Obtener diccionario del país, validando que existe
            if (mStrings.has(mCountry))
            {
                return mStrings.getJSONObject(mCountry);
            }
        }
        catch (JSONException | IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static String getString(String key, String defaultValue, Context context)
    {
        try
        {
            JSONObject countryDictionary = getStringsDictionary(context);
            if (countryDictionary == null)
            {
                return defaultValue;
            }
            else
            {
                return countryDictionary.getString(key);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return defaultValue;
    }
}
