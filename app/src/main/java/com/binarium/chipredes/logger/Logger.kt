package com.binarium.chipredes.logger

import com.binarium.chipredes.logger.model.LogLevel

interface Logger {

    fun postMessage(
        message: String,
        level: LogLevel,
        client: String = "",
        balance: Double = -1.0
    )
}
