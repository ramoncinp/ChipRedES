package com.binarium.chipredes.logger

import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.wolke2.domain.usecases.LogEventUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoggerImpl(
    private val logEventUseCase: LogEventUseCase,
) : Logger {
    override fun postMessage(
        message: String,
        level: LogLevel,
        client: String,
        balance: Double,
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            logEventUseCase.invoke(
                client, message, level.name, balance
            )
        }
    }
}
