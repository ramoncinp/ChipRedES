package com.binarium.chipredes.logger.model

enum class LogLevel {
    INFO,
    WARNING,
    ERROR
}
