/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.binarium.chipredes.barcode;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.binarium.chipredes.R;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;

/**
 * Factory for creating a tracker and associated graphic to be associated with a new barcode.  The
 * multi-processor uses this factory to create barcode trackers as needed -- one for each barcode.
 */
public class BarcodeTrackerFactory implements MultiProcessor.Factory<Barcode>
{
    private BarcodeTrackerInterface mInterface;
    private Context context;
    private GraphicOverlay mGraphicOverlay;

    public BarcodeTrackerFactory(Context context, GraphicOverlay graphicOverlay)
    {
        mGraphicOverlay = graphicOverlay;
        this.context = context;
    }

    @Override
    public Tracker<Barcode> create(Barcode barcode)
    {
        BarcodeGraphic graphic = new BarcodeGraphic(context, mGraphicOverlay);
        mInterface.onReadBarcode(barcode);
        return new GraphicTracker<>(mGraphicOverlay, graphic);
    }

    public void setmInterface(BarcodeTrackerInterface mInterface)
    {
        this.mInterface = mInterface;
    }

    public interface BarcodeTrackerInterface
    {
        void onReadBarcode(Barcode barcode);
    }
}

/**
 * Graphic instance for rendering barcode position, size, and ID within an associated graphic
 * overlay view.
 */
class BarcodeGraphic extends TrackedGraphic<Barcode>
{
    private Paint mRectPaint;
    private Paint mTextPaint;
    private volatile Barcode mBarcode;

    BarcodeGraphic(Context context, GraphicOverlay overlay)
    {
        super(overlay);

        final int selectedColor = context.getResources().getColor(R.color.activo_low_alpha);

        mRectPaint = new Paint();
        mRectPaint.setColor(selectedColor);
        mRectPaint.setStyle(Paint.Style.FILL);
        mRectPaint.setStrokeWidth(4.0f);

        mTextPaint = new Paint();
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTextSize(36.0f);
    }

    /**
     * Updates the barcode instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateItem(Barcode barcode)
    {
        mBarcode = barcode;
        postInvalidate();
    }

    /**
     * Draws the barcode annotations for position, size, and raw value on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas)
    {
        Barcode barcode = mBarcode;
        if (barcode == null)
        {
            return;
        }

        final int margin = 32;

        // Draws the bounding box around the barcode.
        RectF rect = new RectF(barcode.getBoundingBox());
        rect.left = translateX(rect.left - margin);
        rect.top = translateY(rect.top - margin);
        rect.right = translateX(rect.right + margin);
        rect.bottom = translateY(rect.bottom + margin);
        canvas.drawRect(rect, mRectPaint);

        float rectHeight = rect.bottom - rect.top;
        float rectWidth = rect.right - rect.left;

        // Draws a label at the bottom of the barcode indicate the barcode value that was detected.
        //canvas.drawText(barcode.rawValue, rect.left + (rectWidth / 2), rect.bottom -
        // (rectHeight / 2), mTextPaint);
    }
}