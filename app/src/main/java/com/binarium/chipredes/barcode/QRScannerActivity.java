/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.binarium.chipredes.barcode;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.AudioPlayer;
import com.binarium.chipredes.R;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class QRScannerActivity extends AppCompatActivity
{
    private static final int RequestCameraPermissionID = 1001;

    private SurfaceView surfaceView;
    private AudioPlayer audioPlayer;
    private CameraSource cameraSource;

    private boolean detected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (!checkCameraPermission()) finish();
        setContentView(R.layout.activity_qrscanner);

        //Buscar parámetros
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            if (extras.containsKey("text"))
            {
                //Obtener string
                String textToShow = extras.getString("text");
                TextView mText = findViewById(R.id.optional_text);
                mText.setText(textToShow);
                mText.setVisibility(View.VISIBLE);
            }
        }

        surfaceView = findViewById(R.id.camera_preview);
        audioPlayer = new AudioPlayer();

        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this).setBarcodeFormats
                (Barcode.QR_CODE).build();

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>()
        {
            @Override
            public void release()
            {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections)
            {
                final SparseArray<Barcode> qrcodes = detections.getDetectedItems();
                if (qrcodes.size() != 0 && !detected)
                {
                    detected = true;
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            audioPlayer.playSuccesSound1(QRScannerActivity.this);
                            returnQrData(qrcodes.valueAt(0).displayValue);
                            //Retornar info del código
                            /*((PaymentMethod) activity).setScannerResult(qrcodes.valueAt(0)
                            .displayValue);
                            ((PaymentMethod) activity).setGetBilletDataFragment();*/
                        }
                    });
                }
            }
        });

        cameraSource = new CameraSource.Builder(this, barcodeDetector).setRequestedPreviewSize
                (640, 480).build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback()
        {
            @Override
            public void surfaceCreated(SurfaceHolder holder)
            {
                if (ActivityCompat.checkSelfPermission(QRScannerActivity.this, Manifest
                        .permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(QRScannerActivity.this, new
                            String[]{Manifest.permission.CAMERA}, RequestCameraPermissionID);
                }
                try
                {
                    cameraSource.start(surfaceView.getHolder());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
            {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder)
            {
                cameraSource.stop();
            }
        });
    }

    public void returnQrData(String stringData)
    {
        Intent qrData = new Intent();
        qrData.setData(Uri.parse(stringData));
        setResult(RESULT_OK, qrData);

        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case RequestCameraPermissionID:
            {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
                            PackageManager.PERMISSION_GRANTED)
                    {
                        return;
                    }
                    else
                    {
                        try
                        {
                            cameraSource.start(surfaceView.getHolder());
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private boolean checkCameraPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager
                    .PERMISSION_GRANTED)
            {
                Toast.makeText(this, "No se tienen permisos para " + "utilizar la cámara", Toast
                        .LENGTH_SHORT).show();
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
}
