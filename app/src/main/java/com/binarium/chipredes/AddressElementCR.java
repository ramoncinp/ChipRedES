package com.binarium.chipredes;

public class AddressElementCR
{
    public static final int PROVINCIA = 30;
    public static final int CANTON = 31;
    public static final int DISTRITO = 32;
    public static final int BARRIO = 33;

    private String code;
    private String name;
    private String parentCode = null;

    public AddressElementCR(String name, String code)
    {
        this.code = code;
        this.name = name.toUpperCase();
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getParentCode()
    {
        return parentCode;
    }

    public void setParentCode(String parentCode)
    {
        this.parentCode = parentCode;
    }

    public void setCodeFormat()
    {
        if (code.length() == 1)
        {
            code = "0" + code;
        }
    }

    @Override
    public String toString()
    {
        return name + " - " + code;
    }
}
