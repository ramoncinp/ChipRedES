package com.binarium.chipredes.pendingsales;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.binarium.chipredes.R;

import org.json.JSONObject;

public class PendingSaleActivity extends AppCompatActivity
{
    //Constantes
    public final static int SALE_FROM_LAST_SALE_SERVICE = 0;
    public final static int SALE_FROM_PENDING_SALES_SERVICE = 1;
    public final static String SALE_SERVICE = "saleService";

    //Variables
    private int pump;

    //Views
    private CardView lastSaleCardView;
    private CardView pendingSalesCardView;
    private ConstraintLayout noInternetLayout;
    private FrameLayout frameLayout;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_sale);

        //Referencias views
        lastSaleCardView = findViewById(R.id.last_sale_card_view);
        pendingSalesCardView = findViewById(R.id.pending_sales_card_view);
        noInternetLayout = findViewById(R.id.no_internet_layout);
        progressBar = findViewById(R.id.progress_bar);
        frameLayout = findViewById(R.id.pending_sales_fragment_container);

        //Obtener posicion de carga
        pump = getIntent().getIntExtra("pump", 0);

        lastSaleCardView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (validatePump())
                {
                    setLastSaleFragment(PendingSaleActivity.SALE_FROM_LAST_SALE_SERVICE, null);
                    noInternetLayout.setVisibility(View.GONE);
                }
            }
        });

        pendingSalesCardView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (validatePump())
                {
                    GetPendingSalesFragment getPendingSalesFragment = new GetPendingSalesFragment();
                    getPendingSalesFragment.setGetPendingSalesInterface(new GetPendingSalesFragment.GetPendingSalesInterface()
                    {
                        @Override
                        public int getLoadingPosition()
                        {
                            return pump;
                        }

                        @Override
                        public void setSelectedSale(JSONObject sale)
                        {
                            setLastSaleFragment(PendingSaleActivity
                                    .SALE_FROM_PENDING_SALES_SERVICE, sale);
                        }

                        @Override
                        public void showProgressBar()
                        {
                            frameLayout.setVisibility(View.GONE);
                            progressBar.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void hideProgressBar()
                        {
                            progressBar.setVisibility(View.GONE);
                            frameLayout.setVisibility(View.VISIBLE);
                        }
                    });

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id
                            .pending_sales_fragment_container, getPendingSalesFragment).commit();

                    noInternetLayout.setVisibility(View.GONE);
                }
            }
        });

        Button closeButton = findViewById(R.id.negative_button);
        closeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });
    }

    private void setLastSaleFragment(int service, final JSONObject sale)
    {
        Bundle bundle = new Bundle();
        bundle.putInt(PendingSaleActivity.SALE_SERVICE, service);

        AddTaxCodeFragment addTaxCodeFragment = new AddTaxCodeFragment();
        addTaxCodeFragment.setArguments(bundle);
        addTaxCodeFragment.setAddTaxCodeInterface(new AddTaxCodeFragment.AddTaxCodeInterface()
        {
            @Override
            public JSONObject getSale()
            {
                return sale;
            }

            @Override
            public int getLoadingPosition()
            {
                return pump;
            }

            @Override
            public void showProgressBar()
            {
                frameLayout.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void hideProgressBar()
            {
                progressBar.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.pending_sales_fragment_container,
                addTaxCodeFragment).commit();


    }

    private boolean validatePump()
    {
        if (pump == 0)
        {
            Toast.makeText(this, "Error en posicion de carga seleccionada, intente otra " +
                    "vez", Toast.LENGTH_LONG).show();
            return false;
        }
        else
        {
            return true;
        }
    }
}
