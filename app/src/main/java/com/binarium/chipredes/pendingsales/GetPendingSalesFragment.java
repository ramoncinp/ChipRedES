package com.binarium.chipredes.pendingsales;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetPendingSalesFragment extends Fragment
{
    private final static String TAG = "ConsumosPendientes";

    //Variables
    private boolean fetching = false;
    private boolean allPendingSales = false;
    private int loadingPosition;
    private int offset = 0;

    private ChipRedManager chipRedManager;
    private GetPendingSalesInterface getPendingSalesInterface;
    private ArrayList<PendingSale> pendingSales;

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private TextView noPendingSales;

    private PendingSaleArrayAdapter pendingSaleArrayAdapter;
    private LinearLayoutManager linearLayoutManager;

    public GetPendingSalesFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState)

    {
        View content = inflater.inflate(R.layout.fragment_get_pending_sales, container, false);
        noPendingSales = content.findViewById(R.id.no_pending_sales);
        recyclerView = content.findViewById(R.id.pending_sales_list);
        progressBar = content.findViewById(R.id.progress_bar);

        //Inicializar lista
        pendingSales = new ArrayList<>();

        // Inflate the layout for this fragment
        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle("Consumos pendientes");
        getPendingSalesInterface.showProgressBar();

        //Obtener posicion de carga
        loadingPosition = getPendingSalesInterface.getLoadingPosition();

        //Inicializar lista de consumos
        initListAdapter();

        //Pedir los primeros consumos
        initChipRedManager();
    }

    public void setGetPendingSalesInterface(GetPendingSalesInterface getPendingSalesInterface)
    {
        this.getPendingSalesInterface = getPendingSalesInterface;
    }

    private void initChipRedManager()
    {
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        GetPendingSalesFragment.this))
                    return;

                progressBar.setVisibility(View.GONE);
                fetching = false;

                if (!pendingSales.isEmpty())
                {
                    allPendingSales = true;
                    Log.d("PendingSales", "No hay más ventas que pedir");
                }

                getPendingSalesInterface.hideProgressBar();
                Log.v(TAG, crMessage);
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        GetPendingSalesFragment.this))
                    return;

                progressBar.setVisibility(View.GONE);
                fetching = false;

                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                Log.e(TAG, errorMessage);
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        GetPendingSalesFragment.this))
                    return;

                progressBar.setVisibility(View.GONE);
                fetching = false;

                try
                {
                    Log.d(TAG, response.toString(2));
                    parsePendingSales(response);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                getPendingSalesInterface.hideProgressBar();
            }
        }, getContext());

        //Obtener consumos pendientes
        chipRedManager.getPendingLocalSales(loadingPosition, offset);
    }

    private void initListAdapter()
    {
        pendingSaleArrayAdapter = new PendingSaleArrayAdapter(pendingSales, getContext());
        pendingSaleArrayAdapter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getPendingSalesInterface.setSelectedSale(pendingSales.get(recyclerView
                        .getChildAdapterPosition(v)).getJsonObject());
            }
        });

        linearLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setAdapter(pendingSaleArrayAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = pendingSales.size();
                int firstVisibleItemPosition =
                        linearLayoutManager.findFirstVisibleItemPosition();

                if (totalItemCount <= visibleItemCount)
                {
                    allPendingSales = true;
                }

                if (visibleItemCount + firstVisibleItemPosition == totalItemCount && !fetching && !allPendingSales)
                {
                    progressBar.setVisibility(View.VISIBLE);

                    Log.d("PendingSalesList", "Pedir más consumos");
                    //Incrementar el offset
                    offset += 10;

                    //Pedir consumos locales
                    chipRedManager.getPendingLocalSales(loadingPosition, offset);
                    fetching = true;
                }
                Log.d("PendingSalesList", "Visible item count -> " + visibleItemCount);
                Log.d("PendingSalesList", "Total item count -> " + totalItemCount);
                Log.d("PendingSalesList",
                        "First visible item position -> " + firstVisibleItemPosition);
            }
        });
    }

    private void parsePendingSales(JSONObject response)
    {
        try
        {
            //Obtener consumos
            JSONArray pendingSalesArray = response.getJSONArray("data");

            for (int i = 0; i < pendingSalesArray.length(); i++)
            {
                //Obtener consumo
                JSONObject pendingSaleJson = pendingSalesArray.getJSONObject(i);

                PendingSale pendingSale = new PendingSale();
                pendingSale.setAmount(pendingSaleJson.getDouble("costo"));
                pendingSale.setVolume(pendingSaleJson.getDouble("cantidad"));
                pendingSale.setUnitPrice(pendingSaleJson.getDouble("precio_unitario"));
                pendingSale.setDateHour(pendingSaleJson.getString("fecha_hora"));
                pendingSale.setProductDesc(pendingSaleJson.getString("descri_prod").toUpperCase());
                pendingSale.setJsonObject(pendingSaleJson);

                pendingSales.add(pendingSale);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        if (pendingSales.isEmpty())
        {
            noPendingSales.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            pendingSaleArrayAdapter.notifyDataSetChanged();
        }
    }

    public interface GetPendingSalesInterface
    {
        int getLoadingPosition();

        void setSelectedSale(JSONObject sale);

        void showProgressBar();

        void hideProgressBar();
    }
}
