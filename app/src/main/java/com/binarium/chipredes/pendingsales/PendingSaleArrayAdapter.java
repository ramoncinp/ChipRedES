package com.binarium.chipredes.pendingsales;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class PendingSaleArrayAdapter extends RecyclerView.Adapter<PendingSaleArrayAdapter
        .PendingSaleViewHolder> implements View.OnClickListener
{
    private final ArrayList<PendingSale> pendingSales;
    private Context context;
    private View.OnClickListener listener;

    public PendingSaleArrayAdapter(ArrayList<PendingSale> pendingSales, Context context)
    {
        this.pendingSales = pendingSales;
        this.context = context;
    }

    @Override
    public int getItemCount()
    {
        return pendingSales.size();
    }

    @Override
    public PendingSaleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                .pending_sale_layout, viewGroup, false);

        PendingSaleViewHolder pendingSaleViewHolder = new PendingSaleViewHolder(v);
        v.setOnClickListener(this);
        return pendingSaleViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PendingSaleViewHolder pendingSaleViewHolder, int i)
    {
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
        String country = sharedPreferencesManager.getStringFromSP(ChipREDConstants.COUNTRY,
                "mexico");

        DecimalFormat volumeFormat = new DecimalFormat("0.000");
        PendingSale pendingSale = pendingSales.get(i);
        String amount;

        if (country.equals("mexico"))
        {
            amount = ChipREDConstants.MX_AMOUNT_FORMAT.format(pendingSale.getAmount()) + " MXN";
        }
        else
        {
            amount = ChipREDConstants.CR_AMOUNT_FORMAT.format(pendingSale.getAmount()) + " CRC";
        }

        pendingSaleViewHolder.amount.setText(amount);
        pendingSaleViewHolder.volume.setText(volumeFormat.format(pendingSale.getVolume()) + " L");

        String dateHour = pendingSale.getDateHour();
        int token = dateHour.indexOf(' ');
        if (token != -1)
        {
            pendingSaleViewHolder.time.setText(dateHour.substring(token + 1));
            pendingSaleViewHolder.date.setText(dateHour.substring(0, token));
        }
        pendingSaleViewHolder.product.setText(pendingSale.getProductDesc());

        if (pendingSale.getJsonObject().has("medio_pago") && pendingSale.getJsonObject().has
                ("cedula"))
        {
            try
            {
                JSONObject pendingSaleJson = pendingSale.getJsonObject();
                pendingSaleViewHolder.taxCode.append(pendingSaleJson.getString("cedula"));
                pendingSaleViewHolder.taxCode.append("\n");
                pendingSaleViewHolder.taxCode.append("Cédula");
                pendingSaleViewHolder.paymentMethod.append(ChipREDConstants.getPaymentMethodName
                        (pendingSaleJson.getString("medio_pago")));
                pendingSaleViewHolder.paymentMethod.append("\n");
                pendingSaleViewHolder.paymentMethod.append("Medio de pago");

                pendingSaleViewHolder.extras.setVisibility(View.VISIBLE);

                ((CardView) pendingSaleViewHolder.itemView).setCardBackgroundColor(ContextCompat
                        .getColor(context, R.color.green_money));
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    static class PendingSaleViewHolder extends RecyclerView.ViewHolder
    {
        private TextView amount;
        private TextView volume;
        private TextView date;
        private TextView time;
        private TextView product;

        private TextView taxCode;
        private TextView paymentMethod;

        private LinearLayout extras;

        PendingSaleViewHolder(View itemView)
        {
            super(itemView);

            amount = itemView.findViewById(R.id.pending_sale_amount);
            date = itemView.findViewById(R.id.pending_sale_date);
            time = itemView.findViewById(R.id.pending_sale_time);
            product = itemView.findViewById(R.id.pending_sale_product);
            volume = itemView.findViewById(R.id.pending_sale_volume);

            paymentMethod = itemView.findViewById(R.id.pending_sale_payment_method);
            taxCode = itemView.findViewById(R.id.pending_sale_tax_code);

            extras = itemView.findViewById(R.id.pending_sale_extras);
        }
    }
}

