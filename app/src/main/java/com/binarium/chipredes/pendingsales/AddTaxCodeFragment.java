package com.binarium.chipredes.pendingsales;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;

import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.PaymentMethodObject;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddTaxCodeFragment extends Fragment
{
    //Variables
    private int saleService;
    private Integer selectedPaymentMethodIdx = null;
    private String country;

    //Objetos
    private AddTaxCodeInterface addTaxCodeInterface;
    private ChipRedManager chipRedManager;
    private JSONObject lastSale;

    //Views
    private Group pendingSalesGroup;
    private MaterialEditText taxCodeEditText;
    private MaterialBetterSpinner paymentMethodSpinner;
    private TextView amountTv;
    private TextView volumeTv;
    private TextView unitPriceTv;
    private TextView productTv;

    public AddTaxCodeFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState)

    {
        // Inflate the layout for this fragment
        View content = inflater.inflate(R.layout.fragment_add_tax_code, container, false);

        taxCodeEditText = content.findViewById(R.id.tax_code_edit_text);
        paymentMethodSpinner = content.findViewById(R.id.payment_method_spinner);
        amountTv = content.findViewById(R.id.last_sale_amount_tv);
        volumeTv = content.findViewById(R.id.last_sale_volume_tv);
        unitPriceTv = content.findViewById(R.id.last_sale_unit_price_tv);
        productTv = content.findViewById(R.id.last_sale_product_desc_tv);
        pendingSalesGroup = content.findViewById(R.id.pending_sales_group);

        taxCodeEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        Button assignTaxCode = content.findViewById(R.id.save);
        assignTaxCode.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                validateAndAssignTaxCode();
            }
        });

        Button close = content.findViewById(R.id.cancel);
        close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (getActivity() != null) getActivity().finish();
            }
        });

        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        //Saber cual servicio se utilizó para obtener la venta
        if (getArguments() != null)
        {
            saleService = getArguments().getInt(PendingSaleActivity.SALE_SERVICE);
        }

        //Obtener país de la estación
        country = SharedPreferencesManager.getString(getContext(), ChipREDConstants.COUNTRY);

        initChipRedManager();
        initPaymentMethodSpinner();

        //Obtener la última venta
        if (saleService == PendingSaleActivity.SALE_FROM_PENDING_SALES_SERVICE)
        {
            lastSale = addTaxCodeInterface.getSale();
            if (lastSale != null)
            {
                showSaleInfo(lastSale);
            }
        }
        else if (saleService == PendingSaleActivity.SALE_FROM_LAST_SALE_SERVICE)
        {
            addTaxCodeInterface.showProgressBar();
            chipRedManager.getPendingSale(addTaxCodeInterface.getLoadingPosition());
        }
    }

    public void setAddTaxCodeInterface(AddTaxCodeInterface addTaxCodeInterface)
    {
        this.addTaxCodeInterface = addTaxCodeInterface;
    }

    public void showSaleInfo(JSONObject sale)
    {
        try
        {
            //Saber si el JSON es solo la venta o es la repsuesta completa
            //Obtener los datos de la venta
            if (sale.has("data"))
            {
                lastSale = sale.getJSONObject("data");
            }
            else
            {
                lastSale = sale;
            }

            String amount = "";
            String volume = ChipREDConstants.VOLUME_FORMAT.format(lastSale.getDouble("cantidad"))
                    + "" + " L";
            String unitPrice = "";

            if (country.equals("mexico"))
            {
                amount = "$ " + ChipREDConstants.MX_AMOUNT_FORMAT.format(lastSale.getDouble
                        ("costo"));
                unitPrice = ChipREDConstants.MX_AMOUNT_FORMAT.format(lastSale.getDouble
                        ("precio_unitario")) + " MXN";
            }
            else if (country.equals("costa rica"))
            {
                amount = "₡ " + ChipREDConstants.CR_AMOUNT_FORMAT.format(lastSale.getDouble
                        ("costo"));
                unitPrice = ChipREDConstants.MX_AMOUNT_FORMAT.format(lastSale.getDouble
                        ("precio_unitario")) + " CRC";
            }

            amountTv.setText(amount);
            unitPriceTv.setText(unitPrice);
            volumeTv.setText(volume);
            productTv.setText(lastSale.getString("descri_prod").toUpperCase());

            if (lastSale.has("medio_pago") && lastSale.has("cedula"))
            {
                try
                {
                    taxCodeEditText.setText(lastSale.getString("cedula"));
                    paymentMethodSpinner.setListSelection(ChipREDConstants.getPaymentMethodIdx
                            (lastSale.getString("medio_pago")));
                    paymentMethodSpinner.setText(ChipREDConstants.getPaymentMethodName(lastSale
                            .getString("medio_pago")));
                    selectedPaymentMethodIdx = ChipREDConstants.getPaymentMethodIdx(lastSale
                            .getString("medio_pago"));
                    paymentMethodSpinner.clearFocus();
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }
        catch (JSONException | NullPointerException e)
        {
            e.printStackTrace();
            Toast.makeText(getContext(), "Error al procesar la información", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void validateAndAssignTaxCode()
    {
        boolean isValid = taxCodeEditText.validateWith(new METValidator("Ingrese una cedula válida")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                if (isEmpty)
                {
                    return false;
                }
                else
                {
                    return (text.length() >= 9 && text.length() <= 12);
                }
            }
        });

        //Obtener el método de pago seleccionado
        if (selectedPaymentMethodIdx == null)
        {
            paymentMethodSpinner.setError("Seleccione método de pago");
        }

        if (!isValid || selectedPaymentMethodIdx == null) return;

        try
        {
            String taxCode = taxCodeEditText.getText().toString();
            lastSale.put("cedula", taxCode);
            lastSale.put("medio_pago", ChipREDConstants.PAYMENT_METHODS().get
                    (selectedPaymentMethodIdx).getCode());

            addTaxCodeInterface.showProgressBar();

            //Servicio nuevo para asignar número de cédula
            chipRedManager.addTaxCodeToSale(lastSale);
        }
        catch (NullPointerException | JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void initPaymentMethodSpinner()
    {
        //Obtener lista de medios de pago
        ArrayList<String> paymentMethodsDesc = new ArrayList<>();

        for (PaymentMethodObject paymentMethod : ChipREDConstants.PAYMENT_METHODS())
        {
            paymentMethodsDesc.add(paymentMethod.getName());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout
                .simple_dropdown_item_1line, paymentMethodsDesc);
        paymentMethodSpinner.setAdapter(arrayAdapter);
        paymentMethodSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                selectedPaymentMethodIdx = position;
                paymentMethodSpinner.setText(ChipREDConstants.PAYMENT_METHODS().get(position)
                        .getName());
                paymentMethodSpinner.clearFocus();
            }
        });
    }

    private void initChipRedManager()
    {
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        AddTaxCodeFragment.this))
                    return;

                showMessageInDialog(crMessage);
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        AddTaxCodeFragment.this))
                    return;

                showMessageInDialog(errorMessage);
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        AddTaxCodeFragment.this))
                    return;

                try
                {
                    Log.d("ConsumosPendiente", response.toString(2));
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                showSaleInfo(response);
                addTaxCodeInterface.hideProgressBar();
            }
        }, getContext());
    }

    private void showMessageInDialog(String message)
    {
        GenericDialog genericDialog = new GenericDialog("Asignar cédula", message, new
                Runnable()
                {
                    @Override
                    public void run()
                    {
                        getActivity().finish();
                    }
                }, null, getContext());

        genericDialog.setCancelable(false);
        genericDialog.show();
    }

    public interface AddTaxCodeInterface
    {
        JSONObject getSale();

        int getLoadingPosition();

        void showProgressBar();

        void hideProgressBar();
    }
}
