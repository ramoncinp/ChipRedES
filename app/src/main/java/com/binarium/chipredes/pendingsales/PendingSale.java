package com.binarium.chipredes.pendingsales;

import org.json.JSONObject;

public class PendingSale
{
    private double amount;
    private double volume;
    private double unitPrice;
    private String productDesc;
    private String dateHour;

    private JSONObject jsonObject;

    public PendingSale()
    {

    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    public double getVolume()
    {
        return volume;
    }

    public void setVolume(double volume)
    {
        this.volume = volume;
    }

    public double getUnitPrice()
    {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice)
    {
        this.unitPrice = unitPrice;
    }

    public String getProductDesc()
    {
        return productDesc;
    }

    public void setProductDesc(String productDesc)
    {
        this.productDesc = productDesc;
    }

    public String getDateHour()
    {
        return dateHour;
    }

    public void setDateHour(String dateHour)
    {
        this.dateHour = dateHour;
    }

    public JSONObject getJsonObject()
    {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject)
    {
        this.jsonObject = jsonObject;
    }
}
