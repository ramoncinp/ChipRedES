package com.binarium.chipredes;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class TcpClient extends AsyncTask<String, Void, String>
{
    private static final String TAG = TcpClient.class.getSimpleName();
    private static final int TIMEOUT = 2000; // 2 segundos de timeout

    // Variables
    private int port;
    private String host;

    // Objetos
    private TcpInterface tcpInterface;

    public TcpClient(String host, int port)
    {
        this.host = host;
        this.port = port;
    }

    public void setTcpInterface(TcpInterface tcpInterface)
    {
        this.tcpInterface = tcpInterface;
    }

    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings)
    {
        String result = null;

        // Obtener mensaje
        String message = strings[0];

        try
        {
            // Crear socket
            Socket socket = new Socket(host, port);
            socket.setSoTimeout(TIMEOUT);
            Log.d(TAG, "Socket conectado con host " + host);

            try
            {
                // Envia mensaje a servidor
                PrintWriter bufferOut =
                        new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

                // Recibe respuesta de servidor
                BufferedReader bufferIn =
                        new BufferedReader(new InputStreamReader(socket.getInputStream()));

                // Enviar mensaje
                bufferOut.print(message);
                bufferOut.flush();
                Log.d(TAG, "Enviando mensaje -> " + message);

                // Obtener respuesta
                result = bufferIn.readLine();
                Log.d(TAG, "Mensaje recibido " + result);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Log.e(TAG, "Error al enviar mensaje");
            }
            finally
            {
                // Cerrar socket
                socket.close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG, "Error al abrir socket");
        }

        return result;
    }

    @Override
    protected void onPostExecute(String s)
    {
        super.onPostExecute(s);
        if (s != null)
        {
            tcpInterface.onResponse(s);
        }
        else
        {
            tcpInterface.onError();
        }
    }

    public interface TcpInterface
    {
        void onResponse(String message);

        void onError();
    }
}
