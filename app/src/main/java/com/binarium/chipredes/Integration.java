package com.binarium.chipredes;

import android.content.Context;

public class Integration
{
    private boolean enabled;
    private Context context;
    private String imageResource;
    private String name;
    private String constantName;

    public Integration(String name, String constantName, Context context)
    {
        this.context = context;
        this.name = name;
        this.constantName = constantName;

        // El habilitado por default es falso cuando es la integración de enviar preset
        enabled = SharedPreferencesManager.getBoolean(context, constantName, !constantName.equals(
                "sendPresetIntegration"));
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
        SharedPreferencesManager.putBoolean(context, constantName, enabled);
    }

    public String getImageResource()
    {
        return imageResource;
    }

    public void setImageResource(String imageResource)
    {
        this.imageResource = imageResource;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getConstantName()
    {
        return constantName;
    }
}
