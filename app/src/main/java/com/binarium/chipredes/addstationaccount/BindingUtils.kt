package com.binarium.chipredes.addstationaccount

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.binarium.chipredes.R
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke.models.QueriedClient

@BindingAdapter("localClientsList")
fun bindLocalClientsRecyclerView(recyclerView: RecyclerView, data: List<LocalClientData>?) {
    val adapter = recyclerView.adapter as LocalClientAdapter
    adapter.submitList(data)
}

@BindingAdapter("wolkeClientsList")
fun bindWolkeClientsRecyclerView(recyclerView: RecyclerView, data: List<QueriedClient>?) {
    val adapter = recyclerView.adapter as WolkeClientAdapter
    adapter.submitList(data)
}

@BindingAdapter("accountDialogStatus")
fun bindAccountStatusProgress(progressBar: ProgressBar, status: ClientStationAccountStatus) {
    when (status) {
        ClientStationAccountStatus.LOADING -> progressBar.visibility = View.VISIBLE
        else -> progressBar.visibility = View.GONE
    }
}

@BindingAdapter("accountResult")
fun bindAccountResult(layout: RelativeLayout, status: ClientStationAccountStatus) {
    if (status == ClientStationAccountStatus.DONE || status == ClientStationAccountStatus.ERROR) {
        layout.visibility = View.VISIBLE
        val imageView = layout.findViewById<ImageView>(R.id.result_image)
        val cardView = layout.findViewById<CardView>(R.id.result_status_cv)
        if (status == ClientStationAccountStatus.DONE) {
            cardView.setCardBackgroundColor(Color.GREEN)
            imageView.setImageResource(R.drawable.ic_check)
        } else {
            cardView.setCardBackgroundColor(Color.RED)
            imageView.setImageResource(R.drawable.ic_error)
        }
    } else {
        layout.visibility = View.GONE
    }
}