package com.binarium.chipredes.addstationaccount

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.binarium.chipredes.R
import com.binarium.chipredes.databinding.DialogFragmentClientStationAccountBinding
import com.binarium.chipredes.wolke.models.QueriedClient
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ClientStationAccountDialog(private val queriedClient: QueriedClient) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)

        val binding = DialogFragmentClientStationAccountBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val application = requireNotNull(activity).application
        val viewModel: ClientStationAccountViewModel by viewModels()
        viewModel.selectedClient = queriedClient
        viewModel.getClientAccounts()

        binding.viewModel = viewModel

        viewModel.status.observe(viewLifecycleOwner, { currentStatus ->
            when (currentStatus) {
                ClientStationAccountStatus.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.resultLayout.visibility = View.GONE
                }
                ClientStationAccountStatus.DONE -> {
                    binding.progressBar.visibility = View.GONE
                    binding.resultLayout.visibility = View.VISIBLE
                    binding.resultImage.setImageResource(R.drawable.ic_check)
                    binding.resultStatusCv.setCardBackgroundColor(ContextCompat.getColor(application, R.color.green_ok))
                    binding.dialogActionButton.visibility = View.GONE
                }
                ClientStationAccountStatus.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    binding.resultLayout.visibility = View.VISIBLE
                    binding.resultImage.setImageResource(R.drawable.ic_error)
                    binding.resultStatusCv.setCardBackgroundColor(ContextCompat.getColor(application, R.color.red))
                    binding.dialogActionButton.visibility = View.GONE
                }
                ClientStationAccountStatus.CAPTURE -> {
                    binding.progressBar.visibility = View.GONE
                    binding.resultLayout.visibility = View.VISIBLE
                    binding.resultImage.setImageResource(R.drawable.ic_account_white)
                    binding.resultStatusCv.setCardBackgroundColor(ContextCompat.getColor(application, R.color.red))
                    binding.dialogActionButton.visibility = View.VISIBLE
                }
                else -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })

        binding.dialogActionButton.setOnClickListener { viewModel.createStationAccount() }
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
    }
}
