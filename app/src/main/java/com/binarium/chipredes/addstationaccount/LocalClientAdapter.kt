package com.binarium.chipredes.addstationaccount

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.binarium.chipredes.databinding.LocalClientViewItemBinding
import com.binarium.chipredes.wolke.models.LocalClientData

class LocalClientAdapter(private val onClickListener: OnClickListener) :
    ListAdapter<LocalClientData, LocalClientAdapter.QueriedClientViewHolder>(LocalClientDiffCallback) {

    class QueriedClientViewHolder(private var binding: LocalClientViewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(queriedClient: LocalClientData) {
            binding.client = queriedClient
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QueriedClientViewHolder {
        return QueriedClientViewHolder(LocalClientViewItemBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun onBindViewHolder(holder: QueriedClientViewHolder, position: Int) {
        val queriedClient = getItem(position)
        holder.itemView.setOnClickListener { onClickListener.onClick(queriedClient) }
        holder.bind(queriedClient)
    }

    class OnClickListener(val clickListener: (queriedClient: LocalClientData) -> Unit) {
        fun onClick(queriedClient: LocalClientData) = clickListener(queriedClient)
    }

    companion object LocalClientDiffCallback : DiffUtil.ItemCallback<LocalClientData>() {
        override fun areItemsTheSame(oldItem: LocalClientData, newItem: LocalClientData): Boolean {
            return oldItem.id === newItem.id
        }

        override fun areContentsTheSame(
            oldItem: LocalClientData,
            newItem: LocalClientData,
        ): Boolean {
            return oldItem == newItem
        }
    }
}
