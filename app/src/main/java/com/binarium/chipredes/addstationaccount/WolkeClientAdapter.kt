package com.binarium.chipredes.addstationaccount

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.binarium.chipredes.databinding.WolkeClientViewItemBinding
import com.binarium.chipredes.wolke.models.QueriedClient

class WolkeClientAdapter(private val onClickListener: OnClickListener) :
    ListAdapter<QueriedClient, WolkeClientAdapter.QueriedClientViewHolder>(LocalClientDiffCallback) {

    class QueriedClientViewHolder(private var binding: WolkeClientViewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(queriedClient: QueriedClient) {
            binding.client = queriedClient
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QueriedClientViewHolder {
        return QueriedClientViewHolder(WolkeClientViewItemBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun onBindViewHolder(holder: QueriedClientViewHolder, position: Int) {
        val queriedClient = getItem(position)
        holder.itemView.setOnClickListener { onClickListener.onClick(queriedClient) }
        holder.bind(queriedClient)
    }

    class OnClickListener(val clickListener: (queriedClient: QueriedClient) -> Unit) {
        fun onClick(queriedClient: QueriedClient) = clickListener(queriedClient)
    }

    companion object LocalClientDiffCallback : DiffUtil.ItemCallback<QueriedClient>() {
        override fun areItemsTheSame(oldItem: QueriedClient, newItem: QueriedClient): Boolean {
            return oldItem.id === newItem.id
        }

        override fun areContentsTheSame(
            oldItem: QueriedClient,
            newItem: QueriedClient,
        ): Boolean {
            return oldItem == newItem
        }
    }
}
