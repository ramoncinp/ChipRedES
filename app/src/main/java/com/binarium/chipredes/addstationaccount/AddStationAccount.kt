@file:OptIn(ObsoleteCoroutinesApi::class)

package com.binarium.chipredes.addstationaccount

import android.os.Bundle
import android.view.MenuItem
import android.widget.SearchView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.binarium.chipredes.R
import com.binarium.chipredes.databinding.ActivityAddStationAccountBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.ObsoleteCoroutinesApi

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class AddStationAccount : AppCompatActivity() {

    val viewModel: AddStationAccountViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_station_account)

        title = "Crear cuenta"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val binding = DataBindingUtil.setContentView<ActivityAddStationAccountBinding>(
            this,
            R.layout.activity_add_station_account
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.queriedClientsList.adapter = WolkeClientAdapter(
            WolkeClientAdapter.OnClickListener {
                val clientAccountDialog = ClientStationAccountDialog(it)
                clientAccountDialog.show(supportFragmentManager, "clientAccountDialog")
            }
        )
        binding.queriedClientsList.layoutManager = LinearLayoutManager(this)
        binding.searchClient.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                viewModel.queryChannel.trySend(newText).isSuccess
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean = true
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()

        return super.onOptionsItemSelected(item)
    }
}
