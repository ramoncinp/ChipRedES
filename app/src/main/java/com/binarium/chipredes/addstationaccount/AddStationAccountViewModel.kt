package com.binarium.chipredes.addstationaccount

import androidx.lifecycle.*
import com.binarium.chipredes.wolke.domain.usecases.SearchWolkeClientUseCase
import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.QueriedClient
import com.binarium.chipredes.wolke.models.TestConnectionPost
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

private const val SEARCH_DELAY_MILLIS = 500L

@FlowPreview
@ExperimentalCoroutinesApi
@HiltViewModel
class AddStationAccountViewModel @Inject constructor(
    private val searchWolkeClientUseCase: SearchWolkeClientUseCase,
    private val wolkeApiService: WolkeApiService,
) : ViewModel() {

    private val _searchingClient = MutableLiveData<Boolean>()
    val searchingClient: LiveData<Boolean>
        get() = _searchingClient

    val queryChannel = BroadcastChannel<String>(Channel.CONFLATED)
    private val _clients = queryChannel
        .asFlow()
        .debounce(SEARCH_DELAY_MILLIS)
        .mapLatest {
            if (it.isEmpty()) emptyList()
            else searchClient(it)
        }
        .catch {
            Timber.e("Error searching clients")
            _searchingClient.value = false
        }
    val queriedClients = _clients.asLiveData()

    init {
        _searchingClient.value = false
        testWolkeConnection()
    }

    suspend fun searchClient(query: String): List<QueriedClient> {
        Timber.i("Querying client -> $query")
        _searchingClient.value = true
        val clients = viewModelScope.async { searchWolkeClientUseCase(query) }
        val result = clients.await()
        _searchingClient.value = false

        return result
    }

    private fun testWolkeConnection() {
        viewModelScope.launch {
            try {
                val bodyRequest = TestConnectionPost()
                val chipRedResponse = wolkeApiService.testConnection(bodyRequest)

                Timber.i("Response status -> ${chipRedResponse.response}")
                Timber.i("Response message -> ${chipRedResponse.message}")
                Timber.i("Response data -> ${chipRedResponse.data}")
            } catch (e: Exception) {
                Timber.e(e.toString())
            }
        }
    }
}