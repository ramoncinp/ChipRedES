package com.binarium.chipredes.addstationaccount

import android.content.SharedPreferences
import androidx.lifecycle.*
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.emax.EmaxApiService
import com.binarium.chipredes.wolke.models.QueriedClient
import com.binarium.chipredes.wolke2.Wolke2Api
import com.binarium.chipredes.wolke2.models.ClientStationAccount
import com.binarium.chipredes.wolke2.services.CreateStationAccount
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

enum class ClientStationAccountStatus { LOADING, ERROR, DONE, CAPTURE }

@HiltViewModel
class ClientStationAccountViewModel @Inject constructor(
        private val emaxApiService: EmaxApiService,
        private val sharedPreferences: SharedPreferences
) : ViewModel() {

    var selectedClient: QueriedClient? = null

    private val _clientAccounts = MutableLiveData<List<ClientStationAccount>>()

    private val _status = MutableLiveData<ClientStationAccountStatus>()
    val status: LiveData<ClientStationAccountStatus>
        get() = _status

    val resultText = MutableLiveData<String>()


    fun getClientAccounts() {
        viewModelScope.launch {
            try {
                _status.value = ClientStationAccountStatus.LOADING

                // Obtener lista de cuentas desde wolke2
                val clientAccounts: List<ClientStationAccount> = Wolke2Api.retrofitService
                        .getStationAccounts(selectedClient?.id!!).data

                // Analizar cuentas
                var hasThisStationAccount = false
                clientAccounts.forEach { account ->
                    if (account.estacion.id == getThisStationId()) {
                        hasThisStationAccount = true
                    }
                }

                // Reportar status
                if (hasThisStationAccount) {
                    _status.value = ClientStationAccountStatus.DONE
                    resultText.value = "El cliente ya tiene una cuenta en esta estación"
                } else {
                    _status.value = ClientStationAccountStatus.CAPTURE
                    resultText.value = "El cliente no tiene una cuenta para esta estación"
                }

            } catch (e: Exception) {
                resultText.value = "Error al obtener datos de cliente"

                _clientAccounts.value = ArrayList()
                _status.value = ClientStationAccountStatus.ERROR
            }
        }
    }

    private fun getThisStationId(): String {
        return sharedPreferences.getString(
                ChipREDConstants.STATION_MONGO_ID,
                ""
        ).toString()
    }

    fun createStationAccount() {
        viewModelScope.launch {
            _status.value = ClientStationAccountStatus.LOADING

            // Probar conexión hacia EMAX
            val success = emaxApiService.testConnection()

            // Evaluar resultado
            if (success) {
                // Crear cliente en emax
                val addEmaxClientResult = emaxApiService.addClient(selectedClient!!)

                // Evaluar respuesta
                if (addEmaxClientResult["result"] == "error") {
                    _status.value = ClientStationAccountStatus.ERROR
                    resultText.value = "Error al crear cuenta en ChipRED"
                } else {
                    // Obtener id de cliente emax
                    val emaxClientId: String = addEmaxClientResult["emax_client_id"] as String

                    // Crear cuenta en wolke2
                    val createStationAccount = CreateStationAccount(emaxClientId, getThisStationId())

                    try {
                        val clientAccount = Wolke2Api.retrofitService.createStationAccount(
                                selectedClient?.id!!,
                                createStationAccount)

                        if (emaxApiService.addVehicle(clientAccount)) {
                            _status.value = ClientStationAccountStatus.DONE
                            resultText.value = "Registro de cuenta exitoso"
                        } else {
                            _status.value = ClientStationAccountStatus.ERROR
                            resultText.value = "Error al crear vehículo en EMAX"
                        }
                    } catch (e: Exception) {
                        _status.value = ClientStationAccountStatus.ERROR
                        resultText.value = "Error al crear cuenta en ChipRED"
                    }
                }
            } else {
                Timber.d("emax no respondio")
            }
        }
    }
}