package com.binarium.chipredes;

import org.json.JSONException;
import org.json.JSONObject;

public class Product
{
    private String id;
    private String description;
    private String color;
    private double literPrice;

    public Product ()
    {

    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public void setLiterPrice(double literPrice)
    {
        this.literPrice = literPrice;
    }

    public String getId()
    {
        return id;
    }

    public String getDescription()
    {
        return description;
    }

    public String getColor()
    {
        return color;
    }

    public double getLiterPrice()
    {
        return literPrice;
    }

    public JSONObject toJson()
    {
        try
        {
            JSONObject product = new JSONObject();
            product.put("id_producto", id);
            product.put("descripcion", description);
            product.put("precio_litro", literPrice);
            product.put("color", color);

            return product;
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
