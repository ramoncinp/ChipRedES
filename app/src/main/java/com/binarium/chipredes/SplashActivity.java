package com.binarium.chipredes;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.binarium.chipredes.config.ConfigApp;
import com.binarium.chipredes.config.domain.FetchRemoteConfigValues;
import com.binarium.chipredes.main.MainActivity;

import javax.inject.Inject;

public class SplashActivity extends AppCompatActivity
{
    private ImageView img;
    private final Handler handler = new Handler();
    private Class classToExecute;
    private int fragmentIdx = 0;

    private final Runnable startActivityRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            if (classToExecute != null)
            {
                Intent intent = new Intent();
                intent.setClass(SplashActivity.this, classToExecute);
                intent.putExtra("setFragment", fragmentIdx);

                if (fragmentIdx != 0)
                {
                    intent.putExtra("completeProcess", true);
                }

                startActivity(intent);
                finish();
            }
        }
    };

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        img = findViewById(R.id.splash_icon);

        initSpManager();
    }

    private void initSpManager()
    {
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(this);

        String pin = sharedPreferencesManager.getStringFromSP(ChipREDConstants.APP_PIN, "");
        String localIp = sharedPreferencesManager.getStringFromSP(ChipREDConstants.CR_LOCAL_IP, "");
        String wolkeIp = sharedPreferencesManager.getStringFromSP(ChipREDConstants.CR_WOLKE_IP, "");
        String webSocketIp =
                sharedPreferencesManager.getStringFromSP(ChipREDConstants.CR_WEB_SOCKET_IP, "");
        String stationMongoId =
                sharedPreferencesManager.getStringFromSP(ChipREDConstants.STATION_MONGO_ID, "");

        fragmentIdx = 0;

        //Definir el fragment que se debe mostrar según sea el caso
        if (pin.isEmpty() && localIp.isEmpty() && wolkeIp.isEmpty() && webSocketIp.isEmpty())
        {
            classToExecute = ConfigApp.class;
            //El valor inicial del fragment es el correcto
        }
        else if (pin.isEmpty())
        {
            //Mostrar fragment de ingresar PIN
            classToExecute = ConfigApp.class;
            fragmentIdx = 1;
        }
        else if (localIp.isEmpty() || wolkeIp.isEmpty() || webSocketIp.isEmpty() || stationMongoId.isEmpty())
        {
            //Mostrar fragment de ingresar direcciones de servidores
            classToExecute = ConfigApp.class;
            fragmentIdx = 2;
        }/*
        else if ()
        {
            //Mostrar fragment de obtener datos de
            classToExecute = ConfigApp.class;
            fragmentIdx = 3;
        }*/
        else
        {
            classToExecute = MainActivity.class;
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        AnimationSet set = new AnimationSet(true);

        Animation fadeIn = FadeIn();
        fadeIn.setStartOffset(0);
        set.addAnimation(fadeIn);

        img.startAnimation(set);

        handler.postDelayed(startActivityRunnable, 2000);
    }

    public void onPause()
    {
        super.onPause();
        handler.removeCallbacks(startActivityRunnable);
    }

    private Animation FadeIn()
    {
        Animation fade;
        fade = new AlphaAnimation(0.0f, 1.0f);
        fade.setDuration(1000);
        fade.setInterpolator(new AccelerateInterpolator());
        return fade;
    }
}
