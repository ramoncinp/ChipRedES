package com.binarium.chipredes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.adapters.SimpleImageTextRow;
import com.binarium.chipredes.barcode.QRScannerActivity;
import com.binarium.chipredes.billeteapi.AddClientPipeline;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import timber.log.Timber;

public class AddClient extends AppCompatActivity {
    private final static int ADD_CLIENT = 1;
    private final static int MODIFY_CLIENT = 2;
    private final static int COMPLETE_CLIENT_INFO = 3;
    private static final String[] filterOptions = {"Por email", "Por nombre", "Por cédula"};

    private ChipRedManager chipRedManager;
    private ProgressDialog progressDialog;
    private Dialog crAddressesDialog;

    private boolean infoAlreadyShown = false;
    private boolean clientSearchedByQR = false;
    private boolean clientSearchedByID = false;
    private boolean searchingClientByTaxCode = false;
    private int activityAction;
    private int currentFocusedView;
    private int selectedFilterIdx = ChipRedManager.BYEMAIL;
    private String idClientToEdit;
    private String clientStatus;
    private Country selectedCountry;

    //Elementos de direccion para Costa Rica
    private AddressElementCR provincia;
    private AddressElementCR canton;
    private AddressElementCR distrito;
    private AddressElementCR barrio;

    private CardView searchViewCard;
    private LinearLayout mailingListLayout;

    private MaterialEditText nameEd;
    private MaterialEditText phoneEd;
    private MaterialEditText emailEd;
    private MaterialBetterSpinner countrySelector;

    private Button button;
    private Button cancelButton;

    private SearchView searchView;
    private ScrollView scrollView;

    private JSONObject oldInfoFromWebService;

    private SimpleCursorAdapter suggestionsAdapter;

    private final ArrayList<MaterialEditText> mailingList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_modify_account);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Obtener acción de la actividad, agregar o modificar cliente
        activityAction = getIntent().getIntExtra("activityAction", ADD_CLIENT);

        //Inicializar Views
        initViews();

        if (activityAction == MODIFY_CLIENT) {
            //Definicion de views y listeners para editar cliente o completar registro
            setEditActivityViews();
            setEditActivityListeners();
        } else if (activityAction == COMPLETE_CLIENT_INFO) {
            //Definicion de views para completar la información del cliente
            setCompleteClientInfoViews();
            setCompleteClientInfoListeners();
        } else if (activityAction == ADD_CLIENT) {
            //Definir listeners para cuando la actividad se dedica a dar de alta clientes
            setAddClientActivityListeners();
            selectStationCountry();
            setTaxCodeForm();
        }

        //Validar que haya una solicitud de buscar cliente
        String clientID = getIntent().getStringExtra(ChipREDConstants.CLIENT_TO_MODIFY);
        if (clientID != null) {
            clientSearchedByID = true;
            chipRedManager.searchClient(ChipRedManager.BYID, clientID);
        }
    }

    private void initViews() {
        //Definir titulo default
        setTitle("Nueva Cuenta");

        //Botones
        button = findViewById(R.id.submit_account);
        cancelButton = findViewById(R.id.cancel_modify_account);
        //Definir listener para el boton de cancelar
        cancelButton.setOnClickListener(v -> finish());

        //EditTexts
        nameEd = findViewById(R.id.new_account_name);
        phoneEd = findViewById(R.id.new_account_phone_number);
        emailEd = findViewById(R.id.new_account_email);

        //Filtros para solo mayúsculas
        nameEd.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        //Selector de país
        countrySelector = findViewById(R.id.country_selector);

        //SearchView para obtener clientes
        searchView = findViewById(R.id.sv_editar_cliente);
        searchViewCard = findViewById(R.id.card_view_search_client);

        //ScrollView del formulario
        scrollView = findViewById(R.id.add_account_scroll_view);

        //Obtener el layout de la lista de difusión
        mailingListLayout = findViewById(R.id.mailing_list_layout);

        //Agregar listener para agregar correo a lista de difusión
        CardView addEmail = findViewById(R.id.add_mail_to_list);
        addEmail.setOnClickListener(view -> addMailToList(""));
    }

    private void addMailToList(String email) {
        //Obtener el view de la forma correspondiente
        View mailListLayout = getLayoutInflater().inflate(R.layout.mailing_list_element, null);
        mailingListLayout.addView(mailListLayout);

        //Obtener MaterialEditText
        MaterialEditText newMail = mailListLayout.findViewById(R.id.new_account_email);
        //Obtener el índice que le toca al texto
        final int mailIdx = mailingList.size();
        //Modificar hint
        String newHint = "Correo alternativo " + (mailIdx + 1);
        newMail.setHint(newHint);
        newMail.setFloatingLabelText(newHint);
        newMail.setText(email);

        mailingList.add(newMail);
        onMalingListModified();
    }

    private void onMalingListModified() {
        //Obtener cantidad de editText agregados
        int mailCount = mailingListLayout.getChildCount();
        //Redefinir listeners para remover cada uno
        for (int i = 0; i < mailCount; i++) {
            View mMailLayout = mailingListLayout.getChildAt(i);
            //Obtener MaterialEditText
            MaterialEditText newMail = mMailLayout.findViewById(R.id.new_account_email);

            final int mailNumber = i;
            //Modificar hint
            String newHint = "Correo alternativo " + (mailNumber + 1);
            newMail.setHint(newHint);
            newMail.setFloatingLabelText(newHint);

            //Definir botón para remover
            CardView removeMail = mMailLayout.findViewById(R.id.remove_mail_from_list);
            removeMail.setOnClickListener(view -> {
                //Obtener Linenar layout que contiene EditText y CardView
                LinearLayout parentLayout = (LinearLayout) view.getParent();
                //Obtener Linear layout donde se encuentra el padre
                LinearLayout principalLayout = (LinearLayout) parentLayout.getParent();
                principalLayout.removeView(parentLayout);

                //Eliminar último elemento
                mailingList.remove(mailNumber);

                //Actualizar listeners
                onMalingListModified();
            });
        }
    }

    private void resetMailList() {
        //Remover views
        while (mailingListLayout.getChildCount() > 0) {
            mailingListLayout.removeView(mailingListLayout.getChildAt(0));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (activityAction == MODIFY_CLIENT || activityAction == COMPLETE_CLIENT_INFO) {
            getMenuInflater().inflate(R.menu.read_qr_and_filter_menu, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.read_client_qr) {
            startActivityForResult(new Intent(AddClient.this, QRScannerActivity.class),
                    ChipREDConstants.READ_QR_REQUEST);
        } else if (itemId == R.id.select_filter_button) {
            searchClientFilterDialog();
        } else if (itemId == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setCountrySpinner() {
        //Enlistar países
        ArrayList<Country> countries = new ArrayList<>();
        countries.add(new Country("México", R.drawable.ic_mexico, ChipREDConstants.COUNTRIES[0]));
        countries.add(new Country("Costa Rica", R.drawable.ic_costa_rica, ChipREDConstants
                .COUNTRIES[1]));

        //Definir su adaptador
        CountryArrayAdapter countryArrayAdapter = new CountryArrayAdapter(this, countries);
        countrySelector.setAdapter(countryArrayAdapter);
        countrySelector.setOnItemClickListener((parent, view, position, id) -> {
            Country country = (Country) parent.getItemAtPosition(position);
            countrySelector.setText(country.getName());

            //Asignar a variable global
            selectedCountry = country;

            //Mostrar layout correspondiente
            setForm(country);
            setTaxCodeForm();
            countrySelector.clearFocus();
        });
    }

    private void selectStationCountry() {
        //Obtener el país de la estacion
        String stationCountry = SharedPreferencesManager.getString(this, ChipREDConstants
                .COUNTRY, "");

        //Definir un país por default
        Country stationCountryObject;
        int countryIdx = -1;
        if (stationCountry.equals("mexico")) {
            countryIdx = 0;
        } else if (stationCountry.equals("costa rica")) {
            countryIdx = 1;
        }

        //Si existe un país valido, definirlo
        if (countryIdx != -1) {
            countrySelector.setListSelection(countryIdx);
            stationCountryObject = (Country) countrySelector.getAdapter().getItem(countryIdx);
            setForm(stationCountryObject);
            countrySelector.setText(stationCountryObject.getName());
            countrySelector.clearFocus();

            selectedCountry = stationCountryObject;
        }
    }

    private void setAddClientActivityListeners() {
        setCountrySpinner();
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                if (AddClient.this.isFinishing()) return;

                searchingClientByTaxCode = false;
                if (progressDialog != null) progressDialog.dismiss();

                if (webServiceN == ChipRedManager.SEARCH_CLIENT_WS) return;

                if (crMessage.contains("Registrado")) {
                    showWelcomeDialog();
                } else if (crMessage.equals("Modificado")) {
                    Toast.makeText(AddClient.this, "Información de cliente modificada", Toast
                            .LENGTH_SHORT).show();

                    finish();
                } else {
                    GenericDialog genericDialog = new GenericDialog("Aviso", crMessage, () -> {
                        if (activityAction == MODIFY_CLIENT) {
                            finish();
                        }
                    }, null, AddClient.this);

                    genericDialog.setCancelable(false);
                    genericDialog.show();
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                if (AddClient.this.isFinishing()) return;

                searchingClientByTaxCode = false;
                if (progressDialog != null) progressDialog.dismiss();

                GenericDialog genericDialog = new GenericDialog("Error", errorMessage, () -> {
                    if (activityAction == MODIFY_CLIENT) {
                        finish();
                    }
                }, null, AddClient.this);

                genericDialog.show();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                if (AddClient.this.isFinishing()) return;

                Timber.d(response.toString());
                if (webServiceN != ChipRedManager.SEARCH_CLIENT_WS) {
                    showFoundAddressElements(response);
                } else {
                    if (searchingClientByTaxCode) {
                        compareTaxCodes(response);
                    } else {
                        setOldInfo(response);
                    }
                }
            }
        }, this);

        button.setOnClickListener(v -> {
            hideSoftKeyboard(AddClient.this, v);

            final JSONObject request = validateAndGetUserData();
            if (request != null) {
                try {
                    confirmEmailAddress(request.getString("email"), () -> {
                        progressDialog = ProgressDialog.show(AddClient.this, "",
                                "Registrando cliente...", true, false);

                        addAccount(request);
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setEditActivityViews() {
        setTitle("Modificar cuenta");
        button.setText("Modificar");

        countrySelector.setEnabled(false);
        countrySelector.setClickable(false);
        CountryArrayAdapter countryArrayAdapter = new CountryArrayAdapter(this, new ArrayList<>());
        countrySelector.setAdapter(countryArrayAdapter);
        countrySelector.setOnItemClickListener((parent, view, position, id) -> {
            //Nadafse
        });

        button.setVisibility(View.GONE);
        scrollView.setVisibility(View.INVISIBLE);
        searchViewCard.setVisibility(View.VISIBLE);
        cancelButton.setVisibility(View.VISIBLE);

        //Adaptador para los suggestions
        final String[] from = new String[]{"nombreCliente"};
        final int[] to = new int[]{android.R.id.text1};

        suggestionsAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1,
                null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        searchView.setSuggestionsAdapter(suggestionsAdapter);
    }

    private void setEditActivityListeners() {
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                if (AddClient.this.isFinishing()) return;

                //Hacer falsa la variable en caso de que haya habido un error
                clientSearchedByQR = false;
                clientSearchedByID = false;

                if (progressDialog != null) progressDialog.dismiss();

                if (webServiceN != ChipRedManager.SEARCH_CLIENT_WS) {
                    if (crMessage.contains("Registrado")) {
                        showWelcomeDialog();
                    } else if (crMessage.equals("Modificado")) {
                        String messageToShow = "Información de cliente modificada";
                        Toast.makeText(AddClient.this, messageToShow, Toast.LENGTH_SHORT)
                                .show();
                        finish();
                    } else {
                        GenericDialog genericDialog = new GenericDialog("Aviso", crMessage,
                                () -> {
                                    if (activityAction == MODIFY_CLIENT) {
                                        finish();
                                    }
                                }, null, AddClient.this);

                        genericDialog.setCancelable(false);
                        genericDialog.show();
                    }
                } else {
                    Timber.d(crMessage);
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                if (AddClient.this.isFinishing()) return;

                //Hacer falsa la variable en caso de que haya habido un error
                clientSearchedByQR = false;
                clientSearchedByID = false;

                if (progressDialog != null) progressDialog.dismiss();

                if (webServiceN != ChipRedManager.SEARCH_CLIENT_WS) {
                    GenericDialog genericDialog = new GenericDialog("Error", errorMessage, () -> {
                        //nada...
                    }, null, AddClient.this);

                    genericDialog.show();
                }
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                if (AddClient.this.isFinishing()) return;

                Timber.d(response.toString());
                if (webServiceN == ChipRedManager.SEARCH_CLIENT_WS) {
                    if (clientSearchedByQR) {
                        clientSearchedByQR = false;
                        try {
                            setOldInfo(response.getJSONArray("data").getJSONObject(0));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (clientSearchedByID) {
                        clientSearchedByID = false;

                        try {
                            setOldInfo(response.getJSONArray("data").getJSONObject(0));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        getSuggestions(response);
                    }
                } else if (webServiceN == ChipRedManager.COMPLETE_CLIENT_SIGNUP) {
                    showWelcomeDialog();
                } else //Busqueda de direcciones por default
                {
                    showFoundAddressElements(response);
                }
            }
        }, this);

        button.setOnClickListener(v -> {
            hideSoftKeyboard(AddClient.this, v);

            final JSONObject data = validateAndGetUserData();

            if (data != null) {
                //Definir la accion
                Runnable modifyClient = () -> {
                    progressDialog = ProgressDialog.show(AddClient.this, "",
                            "Procesando información", true, false);

                    //Evaluar si se va a editar o completar
                    if (activityAction == MODIFY_CLIENT) {
                        //Enviar información para que se modifique el cliente
                        chipRedManager.addOrModifyClient(data, false, idClientToEdit);
                    } else if (activityAction == COMPLETE_CLIENT_INFO) {
                        chipRedManager.completeClientSignUp(data, idClientToEdit);
                    }
                };

                //Si se dio opcion a modificar el email
                if (emailEd.isEnabled()) {
                    try {
                        confirmEmailAddress(data.getString("email"), modifyClient);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    modifyClient.run();
                }
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                chipRedManager.searchClient(selectedFilterIdx, newText);
                return false;
            }
        });
    }

    private void setCompleteClientInfoViews() {
        setTitle("Completar registro");
        button.setText("Registrar");

        setCountrySpinner();
        selectStationCountry();

        button.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.VISIBLE);
        searchViewCard.setVisibility(View.GONE);
        cancelButton.setVisibility(View.VISIBLE);

        //Adaptador para los suggestions
        final String[] from = new String[]{"nombreCliente"};
        final int[] to = new int[]{android.R.id.text1};

        suggestionsAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1,
                null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        searchView.setSuggestionsAdapter(suggestionsAdapter);
    }

    private void setCompleteClientInfoListeners() {
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                if (AddClient.this.isFinishing()) return;

                if (progressDialog != null) progressDialog.dismiss();
                GenericDialog genericDialog = new GenericDialog("Aviso", crMessage, () -> {
                    //Nada :)
                }, null, AddClient.this);
                genericDialog.show();
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                if (AddClient.this.isFinishing()) return;

                if (progressDialog != null) progressDialog.dismiss();
                GenericDialog genericDialog = new GenericDialog("Error", errorMessage, () -> {
                    //Regresar al fragment de asignar cliente y reiniciarlo
                    Intent resultIntent = new Intent();
                    //Salio bien el registro
                    setResult(1, resultIntent);
                    finish();
                }, null, AddClient.this);
                genericDialog.show();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                if (AddClient.this.isFinishing()) return;

                Timber.d(response.toString());
                if (webServiceN == ChipRedManager.SEARCH_CLIENT_WS && clientSearchedByID) {
                    clientSearchedByID = false;
                    try {
                        setGottenInfoToComplete(response.getJSONArray("data").getJSONObject(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (webServiceN == ChipRedManager.COMPLETE_CLIENT_SIGNUP) {
                    showWelcomeDialog();
                } else if (webServiceN == ChipRedManager.GET_PROVINCIAS || webServiceN == ChipRedManager.GET_BARRIOS || webServiceN == ChipRedManager.GET_CANTONES || webServiceN == ChipRedManager.GET_DISTRITOS) {
                    showFoundAddressElements(response);
                }
            }
        }, this);

        button.setOnClickListener(v -> {
            hideSoftKeyboard(AddClient.this, v);
            final JSONObject data = validateAndGetUserData();
            if (data != null) {
                //Definir la accion
                Runnable modifyClient = new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = ProgressDialog.show(AddClient.this, "",
                                "Procesando" + " información", true, false);

                        //Enviar información para que se complete el registro del cliente
                        chipRedManager.completeClientSignUp(data, idClientToEdit);
                    }
                };
                Handler handler = new Handler();
                handler.postDelayed(modifyClient, 250);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void showWelcomeDialog() {
        LayoutInflater inflater = getLayoutInflater();
        View alertDialog = inflater.inflate(R.layout.welcome_layout, null);

        if (selectedCountry.getIdentifier().equals("costa rica")) {
            TextView textView = alertDialog.findViewById(R.id.text);
            textView.setVisibility(View.GONE);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertDialog);
        builder.setPositiveButton("OK", (dialog, which) -> {
            dialog.dismiss();
            //Regresar al fragment de asignar cliente y reiniciarlo
            Intent resultIntent = new Intent();
            //Salio bien el registro
            setResult(0, resultIntent);
            finish();
        });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    public JSONObject validateAndGetUserData() {
        boolean val1 = nameEd.validateWith(new METValidator("El nombre debe ser de más de 3 " +
                "caracteres") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                int size = text.length();
                return size > 3 && !isEmpty;
            }
        });

        boolean val2 = validateTaxCode();

        boolean val3 = phoneEd.validateWith(new METValidator("Número de teléfono inválido") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                if (selectedCountry.getIdentifier().contains("costa rica")) {
                    if (isEmpty) {
                        return true;
                    } else {
                        return text.length() == 8;
                    }
                } else {
                    if (isEmpty) {
                        return true;
                    } else {
                        return text.length() == 10;
                    }
                }
            }
        });

        //Crear validador para correo electrónico
        METValidator emailValidator = new METValidator("Dirección de correo inválida") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                String mText = text.toString();
                if (isEmpty || !mText.contains("@")) {
                    return false;
                } else {
                    //Contador para las '@'
                    int numberOfChars = 0;

                    //Validar que solo tenga un @
                    for (int i = 0; i < mText.length(); i++) {
                        char mChar = mText.charAt(i);
                        if (mChar == '@') numberOfChars++;
                    }

                    return numberOfChars == 1;
                }
            }
        };

        //Validar correo electrónico principal
        boolean val4 = emailEd.validateWith(emailValidator);

        //Validar los correos de lista de difusión
        for (MaterialEditText materialEditText : mailingList) {
            val4 &= materialEditText.validateWith(emailValidator);
        }

        boolean val5 = validateSelectedAddressForm();

        boolean val6 = true;
        // Evaluar nombre de cliente si es Mexico
        if (selectedCountry.getIdentifier().equals("mexico")) {
            // Evaluar que no exista el caracter "&"
            val6 = nameEd.validateWith(new METValidator("No esta admitido el caracter \"&\"") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    return !text.toString().contains("&");
                }
            });
        }

        if (val1 && val2 && val3 && val4 && val5 && val6) {
            JSONObject request = new JSONObject();
            try {
                request.put("nombre", nameEd.getText().toString().trim());
                request.put("clave_fiscal", getTaxCode().toUpperCase());
                request.put("email", emailEd.getText().toString().trim());
                request.put("estatus", clientStatus);

                JSONArray mailingListArray = new JSONArray();
                for (MaterialEditText materialEditText : mailingList) {
                    if (materialEditText.getText() != null)
                        mailingListArray.put(materialEditText.getText().toString().trim());
                }

                request.put("contactos", mailingListArray);
            } catch (JSONException e) {
                Toast.makeText(this, "Error al procesar los datos", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            return setPhoneAndAddress(selectedCountry, request);
        } else {
            return null;
        }
    }

    private JSONObject setPhoneAndAddress(Country selectedCountry, JSONObject request) {
        if (selectedCountry.getIdentifier().contains("mexico")) {
            MaterialEditText calle = findViewById(R.id.mexico_addr_calle);
            MaterialEditText numeroExterior = findViewById(R.id.mexico_addr_numero_exterior);
            MaterialEditText numeroInterior = findViewById(R.id.mexico_addr_numero_interior);
            MaterialEditText colonia = findViewById(R.id.mexico_addr_colonia);
            MaterialEditText municipio = findViewById(R.id.mexico_addr_municipio);
            MaterialEditText estado = findViewById(R.id.mexico_addr_estado);
            MaterialEditText codigoPostal = findViewById(R.id.mexico_addr_codigo_postal);

            try {
                JSONObject address = new JSONObject();
                address.put("pais", "mexico");
                address.put("calle", calle.getText().toString());
                address.put("numero_ext", numeroExterior.getText().toString());
                address.put("numero_int", numeroInterior.getText().toString());
                address.put("colonia", colonia.getText().toString());
                address.put("municipio", municipio.getText().toString());
                address.put("estado", estado.getText().toString());
                address.put("codigo_postal", codigoPostal.getText().toString());

                JSONObject phone = new JSONObject();
                phone.put("numero", phoneEd.getText().toString());
                phone.put("codigo", "52");

                request.put("telefono", phone);
                request.put("direccion", address);

                //Regresar objeto JSON con la dirección ingresada
                return request;
            } catch (JSONException e) {
                //Devolver el objeto como ingresó al método
                e.printStackTrace();
                return request;
            }
        } else {
            //Dirección opcional!
            MaterialEditText otrasSenias = findViewById(R.id.costa_rica_addr_otras_senias);
            try {
                JSONObject address = new JSONObject();

                if (provincia != null) {
                    address.put("provincia", AddClient.this.provincia.getCode());
                    if (canton != null) {
                        address.put("canton", AddClient.this.canton.getCode());
                        if (distrito != null) {
                            address.put("distrito", AddClient.this.distrito.getCode());
                            if (AddClient.this.barrio != null) {
                                address.put("barrio", AddClient.this.barrio.getCode());
                            }
                        }
                    }
                }

                if (!otrasSenias.getText().toString().isEmpty()) {
                    address.put("otras_senas", otrasSenias.getText().toString());
                }

                address.put("pais", "costa rica");
                request.put("direccion", address);

                //Capturar teléfono
                JSONObject phone = new JSONObject();
                if (phoneEd.length() != 0) {
                    try {
                        phone.put("numero", phoneEd.getText().toString());
                        phone.put("codigo", "506");
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                request.put("telefono", phone);

                //Regresar objeto JSON con la dirección ingresada
                return request;
            } catch (JSONException e) {
                //Devolver el objeto como ingresó al método
                e.printStackTrace();
                return request;
            }
        }
    }

    private boolean validateSelectedAddressForm() {
        boolean isValid = true;

        // Campos de dirección son opcionales, menos código postal
        isValid &= selectedCountry != null;

        return isValid;
    }

    private void confirmEmailAddress(String email, final Runnable action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Correo Electrónico");

        View view = getLayoutInflater().inflate(R.layout.dialog_confirm_email_address, null);
        TextView confirmText = view.findViewById(R.id.confirm_text);
        TextView emailToConfirm = view.findViewById(R.id.email_to_validate);

        confirmText.setText("La dirección de correo proporcionada se utilizará para el envío de "
                + "documentos fiscales, ¿Está seguro?");

        emailToConfirm.setText(email);
        builder.setPositiveButton("Sí", (dialog, which) -> action.run());
        builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
        builder.setView(view);

        Dialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context
                .INPUT_METHOD_SERVICE);
        if (imm != null) imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    public void setOldInfo(JSONObject data) {
        //Remover elementos de lista de difusión
        resetMailList();

        oldInfoFromWebService = new JSONObject();
        try {
            idClientToEdit = data.getString("id");
            oldInfoFromWebService.put("nombre", data.getString("nombre"));
            oldInfoFromWebService.put("email", data.getString("email"));
            oldInfoFromWebService.put("clave_fiscal", data.getString("clave_fiscal"));

            if (data.has("telefono"))
                oldInfoFromWebService.put("telefono", data.getJSONObject("telefono"));

            if (data.has("direccion")) {
                oldInfoFromWebService.put("direccion", data.getJSONObject("direccion"));

                //Dar opcion para seleccionar país si no se tiene uno asignado
                if (oldInfoFromWebService.getJSONObject("direccion").getString("pais").isEmpty()) {
                    setCountrySpinner();
                }
            }

            //Habilitar editText de correo cuando el cliente no tiene uno
            if (oldInfoFromWebService.getString("email").isEmpty()) {
                emailEd.setEnabled(true);
                emailEd.requestFocus();

                //No hay un correo registrado, debe completar el registro
                activityAction = COMPLETE_CLIENT_INFO;
            }

            // Obtener estatus
            if (data.has("estatus")) {
                clientStatus = data.getString("estatus");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String nombre = "", email = "", claveFiscal = "", telefono = "";
        try {
            nombre = oldInfoFromWebService.getString("nombre");
            email = oldInfoFromWebService.getString("email");
            claveFiscal = oldInfoFromWebService.getString("clave_fiscal");

            //Rellenar los demás campos dependiendo del país
            if (oldInfoFromWebService.has("telefono"))
                telefono = oldInfoFromWebService.getJSONObject("telefono").getString("numero");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        searchView.setQuery(nombre, false);
        nameEd.setText(nombre);
        emailEd.setText(email);
        phoneEd.setText(telefono);

        //Obtener contactos
        try {
            if (data.has("contactos")) {
                JSONArray contacts = data.getJSONArray("contactos");
                //Agregar al JSON con info para modificar
                oldInfoFromWebService.put("contactos", contacts);
                //Mostrar contactos
                for (int i = 0; i < contacts.length(); i++) {
                    addMailToList(contacts.getString(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setOldAddressInfo(oldInfoFromWebService);
        setTaxCode(claveFiscal);

        if (!infoAlreadyShown) {
            scrollView.setVisibility(View.VISIBLE);
            scrollView.setAlpha(0.0f);
            scrollView.animate().alpha(1.0f).setListener(null);

            button.setVisibility(View.VISIBLE);
            cancelButton.setVisibility(View.VISIBLE);

            infoAlreadyShown = true;
        }

        searchView.clearFocus();
    }

    public void setGottenInfoToComplete(JSONObject response) {
        try {
            idClientToEdit = response.getString("id");
            nameEd.setText(response.getString("nombre"));
            setTaxCodeForm();
            setTaxCode(response.getString("clave_fiscal"));
            emailEd.requestFocus();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isInfoChanged(JSONObject data) {
        try {
            if (!data.getString("nombre").equals(oldInfoFromWebService.getString("nombre"))) {
                return true;
            }

            if (!data.getString("email").equals(oldInfoFromWebService.getString("email"))) {
                return true;
            }

            if (!data.getString("clave_fiscal").equals(oldInfoFromWebService.getString
                    ("clave_fiscal"))) {
                return true;
            }

            if (!data.getString("telefono").equals(oldInfoFromWebService.getString("telefono"))) {
                return true;
            }

            if (!data.getString("direccion").equals(oldInfoFromWebService.getString("direccion"))) {
                return true;
            }

            if (data.has("contactos") && oldInfoFromWebService.has("contactos")) {
                if (data.getJSONArray("contactos").length() != oldInfoFromWebService.getJSONArray("contactos").length()) {
                    return true;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void getSuggestions(JSONObject response) {
        //Crear suggestions de los clientes encontrados
        final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "nombreCliente"});

        final ArrayList<JSONObject> suggestionsValues = new ArrayList<>();

        try {
            JSONArray arrClientes = response.getJSONArray("data");
            for (int i = 0; i < arrClientes.length(); i++) {
                try {
                    //Obtener cliente
                    JSONObject client = arrClientes.getJSONObject(i);
                    String email = "";

                    if (client.has("email")) email = client.getString("email");
                    suggestionsValues.add(client);
                    c.addRow(new Object[]{i, arrClientes.getJSONObject(i).get("nombre").toString
                            () + " - " + email});
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            suggestionsAdapter.changeCursor(c);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                setOldInfo(suggestionsValues.get(position));
                return false;
            }
        });
    }

    public void hideKeyboard() {
        try {
            View kb = this.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) (this.getSystemService(Context
                    .INPUT_METHOD_SERVICE));
            if (imm != null && kb != null) {
                imm.hideSoftInputFromWindow(kb.getWindowToken(), 0);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ChipREDConstants.READ_QR_REQUEST) {
            if (resultCode == RESULT_OK) {
                //Usar el código
                if (data != null) {
                    clientSearchedByQR = true;

                    String qrCode = data.getData().toString();
                    chipRedManager.searchClient(ChipRedManager.BYID, qrCode);
                    Timber.d("Read QR: " + qrCode);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideKeyboard();
    }

    private void setForm(Country country) {
        //Definir imagen del pais
        ImageView countryImage = findViewById(R.id.country_image);
        countryImage.setImageResource(country.getImageResource());

        //Obtener el layout del formulario
        LinearLayout linearLayout = findViewById(R.id.client_info_form_layout);

        //Analizar si ya existe un view
        if (linearLayout.getChildCount() != 0) {
            linearLayout.removeAllViews();
        }

        //Obtener el view de la forma correspondiente
        View addressForm;
        if (country.getIdentifier().contains("mexico")) {
            addressForm = getLayoutInflater().inflate(R.layout.client_form_mexico_address, null);
            linearLayout.addView(addressForm);
        } else {
            addressForm = getLayoutInflater().inflate(R.layout.client_form_costa_rica_address,
                    null);

            // Ocultar views de estacion
            MaterialEditText codigoActividad = addressForm.findViewById(R.id.activity_code);
            MaterialEditText margenDistribucion =
                    addressForm.findViewById(R.id.distribution_margin);
            if (codigoActividad != null) codigoActividad.setVisibility(View.GONE);
            if (margenDistribucion != null) margenDistribucion.setVisibility(View.GONE);

            linearLayout.addView(addressForm);
            defineAddressListeners();
        }
    }

    public void setOldAddressInfo(JSONObject response) {
        String countryIdentifier;

        //Obtener el país
        try {
            countryIdentifier = response.getJSONObject("direccion").getString("pais");
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error al procesar la información", Toast.LENGTH_SHORT).show();
            return;
        }

        //Crear el objeto country para definir el layout
        Country country;
        if (countryIdentifier.equals("mexico")) {
            country = new Country("México", R.drawable.ic_mexico, countryIdentifier);
        } else {
            country = new Country("Costa Rica", R.drawable.ic_costa_rica, countryIdentifier);
        }

        countrySelector.setText(country.getName());
        selectedCountry = country;
        setForm(country);
        setTaxCodeForm();

        if (countryIdentifier.contains("mexico")) {
            MaterialEditText calle = findViewById(R.id.mexico_addr_calle);
            MaterialEditText numeroExterior = findViewById(R.id.mexico_addr_numero_exterior);
            MaterialEditText numeroInterior = findViewById(R.id.mexico_addr_numero_interior);
            MaterialEditText colonia = findViewById(R.id.mexico_addr_colonia);
            MaterialEditText municipio = findViewById(R.id.mexico_addr_municipio);
            MaterialEditText estado = findViewById(R.id.mexico_addr_estado);
            MaterialEditText codigoPostal = findViewById(R.id.mexico_addr_codigo_postal);

            try {
                JSONObject address = response.getJSONObject("direccion");
                calle.setText(address.getString("calle"));
                numeroExterior.setText(address.getString("numero_ext"));
                numeroInterior.setText(address.getString("numero_int"));
                colonia.setText(address.getString("colonia"));
                municipio.setText(address.getString("municipio"));
                estado.setText(address.getString("estado"));
                codigoPostal.setText(address.getString("codigo_postal"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            MaterialBetterSpinner provincia = findViewById(R.id.costa_rica_addr_provincia);
            MaterialBetterSpinner canton = findViewById(R.id.costa_rica_addr_canton);
            MaterialBetterSpinner distrito = findViewById(R.id.costa_rica_addr_distrito);
            MaterialBetterSpinner barrio = findViewById(R.id.costa_rica_addr_barrio);
            MaterialEditText otrasSenias = findViewById(R.id.costa_rica_addr_otras_senias);

            String provinciaString = "";
            String cantonString = "";
            String distritoString = "";
            String barrioString = "";
            String otrasSenasString = "";

            try {
                JSONObject address = response.getJSONObject("direccion");
                if (address.has("provincia")) provinciaString = address.getString("provincia");
                if (address.has("canton")) cantonString = address.getString("canton");
                if (address.has("distrito")) distritoString = address.getString("distrito");
                if (address.has("otras_senas")) otrasSenasString = address.getString("otras_senas");
                if (address.has("barrio"))
                    barrioString = address.getString("barrio"); //Puede no venir este campo
            } catch (JSONException e) {
                e.printStackTrace();
            }

            provincia.setText(provinciaString);
            canton.setText(cantonString);
            distrito.setText(distritoString);
            barrio.setText(barrioString);
            otrasSenias.setText(otrasSenasString);

            if (!provinciaString.isEmpty()) {
                AddClient.this.provincia = new AddressElementCR("", provinciaString);
                provincia.setEnabled(true);
            }

            if (!cantonString.isEmpty()) {
                AddClient.this.canton = new AddressElementCR("", cantonString);
                canton.setEnabled(true);
            }

            if (!distritoString.isEmpty()) {
                AddClient.this.distrito = new AddressElementCR("", distritoString);
                distrito.setEnabled(true);
            }

            if (!barrioString.isEmpty()) {
                AddClient.this.barrio = new AddressElementCR("", barrioString);
                barrio.setEnabled(true);
            }
        }
    }

    private void defineAddressListeners() {
        View.OnFocusChangeListener focusListener = (v, hasFocus) -> {
            if (hasFocus) currentFocusedView = v.getId();
        };

        View.OnClickListener onClickListener = v -> {
            final int addressType;
            final ArrayList<String> parentCodes = new ArrayList<>();

            if (currentFocusedView == R.id.costa_rica_addr_provincia) {
                addressType = AddressElementCR.PROVINCIA;
            } else if (currentFocusedView == R.id.costa_rica_addr_canton) {
                addressType = AddressElementCR.CANTON;
                parentCodes.add(provincia.getCode());
            } else if (currentFocusedView == R.id.costa_rica_addr_distrito) {
                addressType = AddressElementCR.DISTRITO;
                parentCodes.add(provincia.getCode());
                parentCodes.add(canton.getCode());
            } else if (currentFocusedView == R.id.costa_rica_addr_barrio) {
                addressType = AddressElementCR.BARRIO;
                parentCodes.add(provincia.getCode());
                parentCodes.add(canton.getCode());
                parentCodes.add(distrito.getCode());
            } else {
                addressType = AddressElementCR.PROVINCIA;
            }

            //Enviar peticion para buscar lo seleccionado
            chipRedManager.getCrAddressElement(addressType, parentCodes);
            createDialogForAddressElements(addressType);
        };

        MaterialBetterSpinner provincia = findViewById(R.id.costa_rica_addr_provincia);
        MaterialBetterSpinner canton = findViewById(R.id.costa_rica_addr_canton);
        MaterialBetterSpinner distrito = findViewById(R.id.costa_rica_addr_distrito);
        MaterialBetterSpinner barrio = findViewById(R.id.costa_rica_addr_barrio);

        ArrayAdapter<String> dummyAdapter = new ArrayAdapter<>(this, android.R.layout
                .simple_list_item_1, new String[]{});
        provincia.setAdapter(dummyAdapter);
        canton.setAdapter(dummyAdapter);
        distrito.setAdapter(dummyAdapter);
        barrio.setAdapter(dummyAdapter);

        provincia.setOnFocusChangeListener(focusListener);
        canton.setOnFocusChangeListener(focusListener);
        distrito.setOnFocusChangeListener(focusListener);
        barrio.setOnFocusChangeListener(focusListener);

        provincia.setOnClickListener(onClickListener);
        canton.setOnClickListener(onClickListener);
        distrito.setOnClickListener(onClickListener);
        barrio.setOnClickListener(onClickListener);
    }

    private void createDialogForAddressElements(int addressType) {
        //Crear diálogo
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String title;

        switch (addressType) {
            case AddressElementCR.PROVINCIA:
                title = "Provincias";
                break;

            case AddressElementCR.CANTON:
                title = "Cantónes";
                break;

            case AddressElementCR.DISTRITO:
                title = "Distritos";
                break;

            case AddressElementCR.BARRIO:
                title = "Barrios";
                break;

            default:
                title = "Direcciones";
        }

        //Definir titulo según el valor encontrado
        builder.setTitle(title);

        //Crear view
        View content = getLayoutInflater().inflate(R.layout.dialog_simple_recycler_view, null);
        builder.setView(content);

        //Definir boton de cerrar diálogo
        builder.setNegativeButton("Cerrar", (dialog, which) -> dialog.dismiss());

        crAddressesDialog = builder.create();
        crAddressesDialog.setCancelable(true);
        crAddressesDialog.show();
    }

    private void showFoundAddressElements(JSONObject response) {
        //Obtener progressbar
        ProgressBar progressBar = crAddressesDialog.findViewById(R.id.progress_bar);

        //Obtener textView de error
        TextView noValue = crAddressesDialog.findViewById(R.id.no_values);

        //Obtener su recycler view
        final RecyclerView addressElementsList = crAddressesDialog.findViewById(R.id.servers_list);

        //Obtener arreglo de direcciones
        final ArrayList<AddressElementCR> addressElementCRS = new ArrayList<>();

        try {
            JSONArray array = response.getJSONArray("data");
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String name = object.getString("nombre");
                String codigo = object.getString("codigo");

                AddressElementCR addressElementCR = new AddressElementCR(name, codigo);
                addressElementCRS.add(addressElementCR);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            noValue.setVisibility(View.VISIBLE);
        }

        if (!addressElementCRS.isEmpty()) {
            SimpleImageTextRow simpleImageTextRow = new SimpleImageTextRow(addressElementCRS);
            simpleImageTextRow.setListener(v -> {
                int position = addressElementsList.getChildAdapterPosition(v);
                defineSelectedAddressElement(addressElementCRS.get(position));
                crAddressesDialog.dismiss();
            });
            addressElementsList.setAdapter(simpleImageTextRow);
            addressElementsList.setLayoutManager(new LinearLayoutManager(this));
            addressElementsList.setHasFixedSize(true);
            addressElementsList.setVisibility(View.VISIBLE);
        } else {
            noValue.setVisibility(View.VISIBLE);
        }
        progressBar.setVisibility(View.GONE);
    }

    private void defineSelectedAddressElement(AddressElementCR addressElementCR) {
        MaterialBetterSpinner provinciaSpinner = findViewById(R.id.costa_rica_addr_provincia);
        MaterialBetterSpinner cantonSpinner = findViewById(R.id.costa_rica_addr_canton);
        MaterialBetterSpinner distritoSpinner = findViewById(R.id.costa_rica_addr_distrito);
        MaterialBetterSpinner barrioSpinner = findViewById(R.id.costa_rica_addr_barrio);

        if (currentFocusedView == R.id.costa_rica_addr_provincia) {
            provincia = addressElementCR;
            provinciaSpinner.setText(provincia.toString());

            canton = null;
            cantonSpinner.setText("");

            distrito = null;
            distritoSpinner.setText("");

            barrio = null;
            barrioSpinner.setText("");

            cantonSpinner.setEnabled(true);
            distritoSpinner.setEnabled(false);
            barrioSpinner.setEnabled(false);
        } else if (currentFocusedView == R.id.costa_rica_addr_canton) {
            canton = addressElementCR;
            cantonSpinner.setText(canton.toString());

            distrito = null;
            distritoSpinner.setText("");

            barrio = null;
            barrioSpinner.setText("");

            distritoSpinner.setEnabled(true);
            barrioSpinner.setEnabled(false);
        } else if (currentFocusedView == R.id.costa_rica_addr_distrito) {
            distrito = addressElementCR;
            distritoSpinner.setText(distrito.toString());

            barrio = null;
            barrioSpinner.setText("");

            barrioSpinner.setEnabled(true);
        } else if (currentFocusedView == R.id.costa_rica_addr_barrio) {
            barrio = addressElementCR;
            barrioSpinner.setText(barrio.toString());
        }
    }

    private void compareTaxCodes(JSONObject response) {
        searchingClientByTaxCode = false;

        //Obtener cedula
        String foundTaxCode;

        try {
            //Evaluar longitud del arreglo
            JSONArray array = response.getJSONArray("data");
            if (array.length() == 0) return;

            //Comparar el codigo ingresado con el encontrado
            foundTaxCode = array.getJSONObject(0).getString("clave_fiscal");
            //Si son diferentes... sal del método
            if (!getTaxCode().equals(foundTaxCode)) return;
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
            return;
        }

        GenericDialog dialog = new GenericDialog("Agregar cliente", "La cédula ya esta " +
                "registrada", () -> {
            //Nada
        }, null, this);

        dialog.setPositiveText("Ok");
        dialog.show();

        setTaxtCodeError();
    }

    private void searchClientFilterDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout
                .simple_list_item_1);
        arrayAdapter.add(filterOptions[0]);
        arrayAdapter.add(filterOptions[1]);
        arrayAdapter.add(filterOptions[2]);

        builder.setAdapter(arrayAdapter, (dialog, which) -> {
            String selectedOption = arrayAdapter.getItem(which);

            if (selectedOption != null) {
                selecFilterOption(selectedOption);
            } else {
                Toast.makeText(AddClient.this, "Error al elegir el filtro", Toast
                        .LENGTH_SHORT).show();
            }
            dialog.dismiss();
        });

        builder.setTitle("Filtro de búsqueda");

        Dialog dialog = builder.create();
        dialog.show();
    }

    private void selecFilterOption(String itemName) {
        if (itemName.equals(filterOptions[0])) {
            searchView.setQueryHint("Buscar cliente (email)");
            selectedFilterIdx = ChipRedManager.BYEMAIL;
        } else if (itemName.equals(filterOptions[1])) {
            searchView.setQueryHint("Buscar cliente (nombre)");
            selectedFilterIdx = ChipRedManager.BYNAME;
        } else if (itemName.equals(filterOptions[2])) {
            searchView.setQueryHint("Buscar cliente (cédula)");
            selectedFilterIdx = ChipRedManager.BYTAXCODE;
        }
        searchView.setQuery("", false);
    }

    private void addAccount(JSONObject client) {
        if (selectedCountry.getIdentifier().equals("mexico")) {
            // Crear proceso para registrar cliente en emax y chipred
            AddClientPipeline addClientPipeline =
                    new AddClientPipeline(new Handler(message -> {
                        if (message.obj != null) {
                            // Obtener JSON
                            JSONObject response = (JSONObject) message.obj;
                            String result = "";
                            String messageStr = "Error desconocido";

                            // Obtener datos
                            try {
                                result = response.getString("result");
                                messageStr = response.getString("message");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (result.equals("ok")) {
                                showWelcomeDialog();
                            } else {
                                showResult(
                                        messageStr
                                );
                            }

                            if (progressDialog != null) progressDialog.dismiss();
                        }

                        return false;
                    }));

            // Iniciar proceso
            addClientPipeline.addClient(this, client);
            addClientPipeline.start();
        } else {
            chipRedManager.addOrModifyClient(client, true, null);
        }
    }

    private void showResult(String text) {
        GenericDialog genericDialog = new GenericDialog(
                "Error",
                text,
                new Runnable() {
                    @Override
                    public void run() {
                        if ("Error".equals("ok")) {
                            finish();
                        }
                    }
                },
                null,
                this
        );

        genericDialog.setCancelable(false);
        genericDialog.show();
    }

    private void setTaxCodeForm() {
        LinearLayout taxCodeLayout = findViewById(R.id.client_taxcode_layout);

        //Analizar si ya existe un view
        if (taxCodeLayout.getChildCount() != 0) {
            taxCodeLayout.removeAllViews();
        }

        //Agregar listener para campo de cédula
        View.OnFocusChangeListener focusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                //Si su focus cambio de verdadero a falso, pedir comparación a wolke
                if (!b) {
                    //Cambió su focus a falso
                    try {
                        String text = getTaxCode();
                        chipRedManager.searchClient(ChipRedManager.BYTAXCODE, text);
                        searchingClientByTaxCode = true;
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        //Obtener el view de la forma correspondiente
        View taxCodeForm;
        if (selectedCountry.getIdentifier().contains("mexico")) {
            // Dibujar formulario de RFC para clientes de México
            taxCodeForm = getLayoutInflater().inflate(R.layout.client_mexico_tax_code, null);

            // Obtener editText's
            MaterialEditText rfcEt1 = taxCodeForm.findViewById(R.id.tax_code_1);
            MaterialEditText rfcEt2 = taxCodeForm.findViewById(R.id.tax_code_2);
            MaterialEditText rfcEt3 = taxCodeForm.findViewById(R.id.tax_code_3);

            // Definir filtro de mayúsculas
            rfcEt1.setFilters(new InputFilter[]{new InputFilter.AllCaps(),
                    new InputFilter.LengthFilter(4)});
            rfcEt2.setFilters(new InputFilter[]{new InputFilter.AllCaps(),
                    new InputFilter.LengthFilter(6)});
            rfcEt3.setFilters(new InputFilter[]{new InputFilter.AllCaps(),
                    new InputFilter.LengthFilter(3)});

            // Definir focusChangeListener
            rfcEt3.setOnFocusChangeListener(focusChangeListener);

            if (activityAction == MODIFY_CLIENT || activityAction == COMPLETE_CLIENT_INFO) {
                rfcEt1.setEnabled(false);
                rfcEt2.setEnabled(false);
                rfcEt3.setEnabled(false);
            }
        } else {
            // Dibujar editText para cédula de clientes de Costa Rica
            taxCodeForm = getLayoutInflater().inflate(R.layout.client_costa_rica_tax_code,
                    null);

            // Obtener editText
            MaterialEditText rfcEt = taxCodeForm.findViewById(R.id.tax_code);

            // Definir filtro de mayúsculas
            rfcEt.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

            //Agregar listener para campo de cédula
            rfcEt.setOnFocusChangeListener(focusChangeListener);

            if (activityAction == MODIFY_CLIENT || activityAction == COMPLETE_CLIENT_INFO) {
                rfcEt.setEnabled(false);
            }
        }

        taxCodeLayout.addView(taxCodeForm);
    }

    private boolean validateTaxCode() {
        boolean valid;

        if (selectedCountry.getIdentifier().equals("mexico")) {
            // Obtener editTexts
            MaterialEditText rfcEt1 = findViewById(R.id.tax_code_1);
            MaterialEditText rfcEt2 = findViewById(R.id.tax_code_2);
            MaterialEditText rfcEt3 = findViewById(R.id.tax_code_3);

            valid = rfcEt1.validateWith(new METValidator("Longitud no válida") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    return text.length() == 3 || text.length() == 4;
                }
            });

            valid &= rfcEt2.validateWith(new METValidator("Longitud no válida") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    return text.length() == 6;
                }
            });

            valid &= rfcEt3.validateWith(new METValidator("Longitud no válida") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    return text.length() == 3;
                }
            });
        } else {
            // Obtener editText
            MaterialEditText cedulaEt = findViewById(R.id.tax_code);

            // Validar
            valid = cedulaEt.validateWith(new METValidator("Campo obligatorio") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    return !isEmpty;
                }
            });
        }

        return valid;
    }

    private String getTaxCode() {
        String taxCode = "";

        if (selectedCountry.getIdentifier().equals("mexico")) {
            // Obtener editTexts
            MaterialEditText rfcEt1 = findViewById(R.id.tax_code_1);
            MaterialEditText rfcEt2 = findViewById(R.id.tax_code_2);
            MaterialEditText rfcEt3 = findViewById(R.id.tax_code_3);

            taxCode += rfcEt1.getText().toString();
            taxCode += "-";
            taxCode += rfcEt2.getText().toString();
            taxCode += "-";
            taxCode += rfcEt3.getText().toString();
        } else {
            // Obtener editText
            MaterialEditText cedulaEt = findViewById(R.id.tax_code);
            taxCode = cedulaEt.getText().toString();
        }

        return taxCode.trim();
    }

    private void setTaxCode(String taxCode) {
        if (selectedCountry.getIdentifier().equals("mexico")) {
            // Obtener editTexts
            MaterialEditText rfcEt1 = findViewById(R.id.tax_code_1);
            MaterialEditText rfcEt2 = findViewById(R.id.tax_code_2);
            MaterialEditText rfcEt3 = findViewById(R.id.tax_code_3);

            String[] segments = taxCode.split("-");
            if (segments.length == 3) {
                rfcEt1.setText(segments[0]);
                rfcEt2.setText(segments[1]);
                rfcEt3.setText(segments[2]);
            } else {
                rfcEt1.setText(taxCode);
            }
        } else {
            // Obtener editText
            MaterialEditText cedulaEt = findViewById(R.id.tax_code);
            cedulaEt.setText(taxCode);
        }
    }

    private void setTaxtCodeError() {
        String message = "Clave fiscal ya registrada";
        if (selectedCountry.getIdentifier().equals("mexico")) {
            // Obtener editTexts
            MaterialEditText rfcEt1 = findViewById(R.id.tax_code_1);
            MaterialEditText rfcEt2 = findViewById(R.id.tax_code_2);
            MaterialEditText rfcEt3 = findViewById(R.id.tax_code_3);

            rfcEt1.setError(message);
            rfcEt2.setError(message);
            rfcEt3.setError(message);
        } else {
            // Obtener editText
            MaterialEditText cedulaEt = findViewById(R.id.tax_code);
            cedulaEt.setError(message);
        }
    }
}
