package com.binarium.chipredes.billeteapi;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.binarium.chipredes.Pumper;
import com.binarium.chipredes.emax.EmaxProvider;
import com.binarium.chipredes.emax.EmaxResponseParser;
import com.binarium.chipredes.emax.WsChipredManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;

import timber.log.Timber;

public class AddBalancePipeline extends Thread {
    // Constantes
    private static final String TAG = AddBalancePipeline.class.getSimpleName();
    private static final String OK = "ok";
    private static final String ERROR = "error";

    // Objetos
    private Context context;
    private final Handler handler;
    private JSONObject paymentData;
    private JSONObject paymentResultData;
    private Pumper selectedPumper;

    // Objetos para enviar peticiones
    private BEManager beManager;

    // Constructor
    public AddBalancePipeline(Handler handler) {
        this.handler = handler;
    }

    public void addBalance(Context context, JSONObject paymentData, Pumper selectedPumper) {
        this.context = context;
        this.paymentData = paymentData;
        this.selectedPumper = selectedPumper;
    }

    /**
     * Proceso:
     * <p>
     * 1.- Test connection a EMAX
     * 2.- Obtener datos de depósito
     * 3.- Realizar depósito en ChipRED
     * 4.- Realizar depósito en EMAX
     **/

    private void initChipredManager() {
        beManager = new BEManager();
        beManager.init(context, new BEManager.OnMessageReceived() {
            @Override
            public void onResponse(JSONObject response, int service) {
                // Abono hecho correctamente, hacer abono a EMAX
                Timber.d("Depóstio realizado correctamente %s", response.toString());
                paymentResultData = response;

                Timber.d("Depósito realizado en ChipRED");
                returnMessage(OK, "Depósito realizado correctamente");
            }

            @Override
            public void onError(String message, int service) {
                Timber.e("Error al realizar depósito en ChipRED %s", message);
                returnMessage(ERROR, message);
            }
        });
    }

    private void addBalanceToEmax() {
        Timber.d("Iniciando depósito en EMAX");
        WsChipredManager.registerDeposit(context, paymentData,
                new EmaxProvider.SimecobConnectionInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            Timber.d("Respuesta de EMAX al realizar depósito");

                            if (response == null || response.isEmpty()) {
                                Timber.d("No se recibió respuesta de EMAX, verifique si se realizó el abono a la cuenta.");
                                addBalanceToChipRed();
                                return;
                            }

                            JSONObject responseData =
                                    EmaxResponseParser.parseEmaxResponse(new ByteArrayInputStream(response.getBytes()));

                            if (responseData.getString("iderror").equals("0")) {
                                addBalanceToChipRed();
                            } else {
                                String message = responseData.getString("msgerror");
                                Timber.e("Error al hacer depósito en EMAX -> %s", message);
                                returnMessage(ERROR, message);
                            }
                        } catch (Exception e) {
                            Timber.e("Error al procesar respuesta de depósito en EMAX");
                            returnMessage(ERROR, "Error al obtener respuesta de EMAX");
                        }
                    }

                    @Override
                    public void onError(String response) {
                        Timber.e("Response Error: ");
                        Timber.e(response);
                        returnMessage(ERROR, "Error al abonar saldo en Emax");
                    }
                });
    }

    private void addBalanceToChipRed() {
        Timber.d("Iniciando depósito en ChipRED");
        try {
            double amount = paymentData.getDouble("cantidad");
            String accountId = paymentData.getString("id_cuenta");

            beManager.addToBalance(accountId, amount, selectedPumper);
        } catch (JSONException e) {
            e.printStackTrace();
            Timber.e("Error al obtener datos para abonar en ChipRED");
            returnMessage(ERROR, "Error al obtener datos para abonar en ChipRED");
        }
    }

    @Override
    public void run() {
        // Crear instancia de BEManager
        Timber.d("Inicializando ChipRedManager...");
        initChipredManager();

        // Ejecutar servicio para probar conexión con EMAX
        Timber.d("Probando conexión con EMAX");
        WsChipredManager.testConnection(context, new EmaxProvider.SimecobConnectionInterface() {
            @Override
            public void onSuccess(String response) {
                // Agregar saldo en chipRED
                Timber.d("Test connection a EMAX exitoso");
                Timber.d("Agregando saldo en EMAX");
                addBalanceToEmax();
            }

            @Override
            public void onError(String response) {
                Timber.e("Errar al probar conexión con EMAX");
                returnMessage(ERROR, "No responde servidor EMAX");
            }
        });
    }

    private void returnMessage(String status, String messageVal) {
        Message message = new Message();
        JSONObject response = new JSONObject();

        try {
            response.put("result", status);
            response.put("message", messageVal);
            response.put("data", paymentResultData);

            message.obj = response;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        handler.sendMessage(message);
    }
}
