package com.binarium.chipredes.billeteapi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.binarium.chipredes.R
import com.binarium.chipredes.databinding.DialogFragmentResendEmailBinding
import com.rengwuxian.materialedittext.validation.METValidator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ResendConfirmationEmailDialog : DialogFragment() {
    lateinit var binding: DialogFragmentResendEmailBinding
    val viewModel: ResendConfirmationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DialogFragmentResendEmailBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val application = requireNotNull(activity).application

        binding.viewModel = viewModel
        binding.submitButton.setOnClickListener {
            getEmailToConfirm()
        }
        binding.returnButton.setOnClickListener {
            dialog?.dismiss()
        }

        viewModel.status.observe(this, { currentStatus ->
            when (currentStatus) {
                ResendConfirmationDialogStatus.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.resultLayout.visibility = View.GONE
                    binding.confirmationEmailLayout.visibility = View.GONE
                    binding.submitButton.visibility = View.GONE
                    binding.returnButton.visibility = View.GONE
                }
                ResendConfirmationDialogStatus.DONE -> {
                    binding.progressBar.visibility = View.GONE
                    binding.confirmationEmailLayout.visibility = View.GONE
                    binding.resultLayout.visibility = View.VISIBLE
                    binding.resultImage.setImageResource(R.drawable.ic_check)
                    binding.resultStatusCv.setCardBackgroundColor(ContextCompat.getColor(application,
                        R.color.green_ok))
                    binding.submitButton.visibility = View.GONE
                    binding.returnButton.visibility = View.GONE
                }
                ResendConfirmationDialogStatus.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    binding.confirmationEmailLayout.visibility = View.GONE
                    binding.resultLayout.visibility = View.VISIBLE
                    binding.resultImage.setImageResource(R.drawable.ic_error)
                    binding.resultStatusCv.setCardBackgroundColor(ContextCompat.getColor(application,
                        R.color.red))
                    binding.submitButton.visibility = View.GONE
                    binding.returnButton.visibility = View.GONE
                }
                else -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun getEmailToConfirm() {
        val isValid =
            binding.emailToConfirmate.validateWith(object : METValidator("Dirección no válida") {
                override fun isValid(text: CharSequence, isEmpty: Boolean): Boolean {
                    return text.contains("@")
                }
            })

        if (isValid) {
            val email = binding.emailToConfirmate.text?.toString()
            if (email != null) {
                viewModel.resendEmail(email)
            }
        }
    }
}