package com.binarium.chipredes.billeteapi;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.binarium.chipredes.emax.EmaxProvider;
import com.binarium.chipredes.emax.EmaxResponseParser;
import com.binarium.chipredes.emax.WsChipredManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;

import timber.log.Timber;

public class AddClientPipeline extends Thread
{
    // Constantes
    private static final String TAG = AddClientPipeline.class.getSimpleName();
    private static final String OK = "ok";
    private static final String ERROR = "error";

    // Variables
    private String chipredClientId;
    private String emaxClientId;

    // Objetos
    private Context context;
    private Handler handler;
    private JSONObject clientInputData;

    // Objetos para enviar peticiones
    private BEManager beManager;

    // Constructor
    public AddClientPipeline(Handler handler)
    {
        this.handler = handler;
    }

    public void addClient(Context context, JSONObject clientInputData)
    {
        this.context = context;
        this.clientInputData = clientInputData;
    }


    /**
     * Proceso:
     * <p>
     * 1.- Obtener datos de cliente
     * 2.- Agregar cliente en ChipRED
     * 3.- Agregar cliente en EMAX
     * 4.- Crear cuenta en ChipRED
     * 5.- Crear vehículo en EMAX
     **/

    private void initBEManager()
    {
        beManager = BEManager.getInstance();
        beManager.init(
                context,
                new BEManager.OnMessageReceived()
                {
                    @Override
                    public void onResponse(JSONObject response, int service)
                    {
                        Timber.v("Respuesta BEManager");
                        Timber.v(response.toString());

                        if (service == BEManager.ADD_CLIENT_WS)
                        {
                            addEmaxClient(response);
                        }
                        else if (service == BEManager.ADD_CLIENT_ACCOUNT_WS)
                        {
                            addEmaxVehicle(response);
                        }
                    }

                    @Override
                    public void onError(String message, int service)
                    {
                        returnMessage(ERROR, message);
                    }
                }
        );
    }

    private void addEmaxClient(JSONObject chipredResponse)
    {
        // Obtener datos necesarios para emax
        JSONObject emaxRequestBody = new JSONObject();
        try
        {
            chipredClientId = chipredResponse.getJSONObject("_id").getString("$oid");
            emaxRequestBody.put("rfc", chipredResponse.getString("clave_fiscal"));
            emaxRequestBody.put("nombre", chipredResponse.getString("nombre"));
            emaxRequestBody.put("email", chipredResponse.getString("email"));
        }
        catch (JSONException e)
        {
            returnMessage(ERROR, "Error al procesar información de cliente");
        }

        WsChipredManager.addCostumer(context, emaxRequestBody,
                new EmaxProvider.SimecobConnectionInterface()
                {
                    @Override
                    public void onSuccess(String response)
                    {
                        try
                        {
                            // Obtener id de cliente
                            JSONObject responseData =
                                    EmaxResponseParser.parseEmaxResponse(new ByteArrayInputStream(response.getBytes()));

                            // Validar id de cliente
                            if (responseData.has("id_de_cliente"))
                            {
                                // Guardar idCliente Emax
                                emaxClientId = responseData.getString("id_de_cliente");

                                // Crear cuenta en chipRed
                                beManager.addClientAccount(chipredClientId, emaxClientId);
                            }
                            else
                            {
                                returnMessage(ERROR, responseData.getString("msgerror"));
                            }
                        }
                        catch (Exception e)
                        {
                            returnMessage(ERROR, "Error al obtener id de cliente registrado");
                        }
                    }

                    @Override
                    public void onError(String response)
                    {
                        Log.d(TAG, "Response Error: ");
                        Log.d(TAG, response);
                        returnMessage(ERROR, "Error al registrar cliente en Emax");
                    }
                });
    }

    /*
    *
        vehicle.getString("id_cliente"),
        vehicle.getString("placas"),
        vehicle.getString("tipo_vehiculo"),
        vehicle.getString("no_tag"),
        vehicle.getString("nip"),
        vehicle.getString("consumo_maximo")
        * */

    private void addEmaxVehicle(JSONObject chipredResponse)
    {
        // Obtener datos necesarios para emax
        JSONObject emaxRequestBody = new JSONObject();
        try
        {
            JSONObject vehiculo = chipredResponse.getJSONObject("vehiculo");
            emaxRequestBody.put("id_cliente", emaxClientId);
            emaxRequestBody.put("placas", vehiculo.getString("placas"));
            emaxRequestBody.put("tipo_vehiculo", vehiculo.getString("tipo_vehiculo"));
            emaxRequestBody.put("no_tag", vehiculo.getString("no_tag"));
            emaxRequestBody.put("nip", vehiculo.getString("nip"));

            // Convertir consumo maximo a entero
            double val = vehiculo.getDouble("consumo_maximo");
            emaxRequestBody.put("consumo_maximo", (int) val);
        }
        catch (JSONException e)
        {
            returnMessage(ERROR, "Error al procesar información de vehículo");
        }

        WsChipredManager.addVehicle(context, emaxRequestBody,
                new EmaxProvider.SimecobConnectionInterface()
                {
                    @Override
                    public void onSuccess(String response)
                    {
                        try
                        {
                            Log.v(TAG, "Proceso de registro de cliente, completo");
                            returnMessage(OK, "Cliente registrado en ambos sistemas");
                        }
                        catch (Exception e)
                        {
                            returnMessage(ERROR, "Error al obtener id de cliente registrado");
                        }
                    }

                    @Override
                    public void onError(String response)
                    {
                        returnMessage(ERROR, "Error al registrar vehiculo en Emax");
                    }
                });
    }

    @Override
    public void run()
    {
        // Crear instancia de BEManager
        initBEManager();

        // Ejecutar servicio para crear cliente en ChipRED
        beManager.addClient(clientInputData);
    }

    private void returnMessage(String status, String messageVal)
    {
        Message message = new Message();
        JSONObject response = new JSONObject();

        try
        {
            response.put("result", status);
            response.put("message", messageVal);

            message.obj = response;
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        handler.sendMessage(message);
    }
}
