package com.binarium.chipredes.billeteapi

import androidx.lifecycle.*

import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.QueriedClient
import com.binarium.chipredes.wolke.models.ResendConfirmationEmailPost
import com.binarium.chipredes.wolke.models.SearchClientByEmailPost
import dagger.hilt.android.lifecycle.HiltViewModel

import kotlinx.coroutines.launch

import timber.log.Timber

import java.lang.Exception
import javax.inject.Inject


enum class ResendConfirmationDialogStatus { LOADING, ERROR, DONE }

@HiltViewModel
class ResendConfirmationViewModel @Inject constructor(
        private val wolkeApiService: WolkeApiService
) : ViewModel() {

    private val _status = MutableLiveData<ResendConfirmationDialogStatus>()
    val status: LiveData<ResendConfirmationDialogStatus>
        get() = _status

    private val _resultText = MutableLiveData<String>()
    val resultText: LiveData<String>
        get() = _resultText

    fun resendEmail(clientEmail: String) {
        viewModelScope.launch {
            _status.postValue(ResendConfirmationDialogStatus.LOADING)

            val clientId = getClientId(clientEmail)

            if (clientId.isEmpty()) {
                Timber.d("Id de cliente esta vacío")
                _resultText.value = "Error al obtener id de cliente de $clientEmail"
                _status.value = ResendConfirmationDialogStatus.ERROR
            } else {
                Timber.d("Id de cliente obtenido -> $clientId")
                Timber.d("Reenviando correo de confirmación")
                try {
                    val bodyRequest = ResendConfirmationEmailPost(clientId = clientId)
                    val response = wolkeApiService.resendConfirmationEmail(bodyRequest)
                    if (response.response == "Ok") {
                        _resultText.value = "Correo de confirmación enviado correctamente"
                        _status.value = ResendConfirmationDialogStatus.DONE
                    } else {
                        _resultText.value = response.message
                        _status.value = ResendConfirmationDialogStatus.ERROR
                    }
                } catch (error: Exception) {
                    Timber.d("Error al enviar correo de confirmación")
                    _resultText.value = "Error al enviar correo de confirmación"
                    _status.value = ResendConfirmationDialogStatus.ERROR
                }
            }
        }
    }

    private suspend fun getClientId(clientEmail: String): String {
        var clientId = ""

        // Buscar cliente
        Timber.i("Querying client -> $clientEmail")

        try {
            // Crear petición
            val bodyRequest = SearchClientByEmailPost(email = clientEmail)

            // Enviar petición a Wolke
            val chipredResponse = wolkeApiService.searchClientByEmail(bodyRequest)

            // Obtener datos de respuesta
            val data = chipredResponse.data
            if (data!!.isNotEmpty()) {
                val queriedClient = QueriedClient(data[0])
                clientId = queriedClient.id
                Timber.d("Id de cliente obtenido $clientId")
            } else {
                Timber.e("Error al obtener información de cliente $clientEmail")
            }
        } catch (e: Exception) {
            Timber.e("Error al obtener id de cliente $e")
        }

        return clientId
    }
}