package com.binarium.chipredes.billeteapi;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.Pumper;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.utils.RequestQueueSingelton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

import timber.log.Timber;

public class BEManager
{
    // Constantes
    private final static String TAG = BEManager.class.getSimpleName();
    // Servicios
    public final static int ADD_CLIENT_WS = 0;
    public final static int ADD_CLIENT_ACCOUNT_WS = 1;
    public final static int RESERVE_BILLET_SALE_WS = 2;
    public final static int CANCEL_SALE_RESERVATION = 3;
    public final static int ADD_TO_BALANCE = 4;
    public final static String WOLKE2_URL = ChipREDConstants.WOLKE2_BASE_URL;

    // Instancia
    private static BEManager beManager = null;

    // Variables
    private int currentService;

    // Objetos
    private Context context;
    public OnMessageReceived onMessageReceived;

    // Listeners
    public Response.Listener<JSONObject> onSuccessListener = new Response.Listener<JSONObject>()
    {
        @Override
        public void onResponse(JSONObject response)
        {
            onMessageReceived.onResponse(response, currentService);
        }
    };


    public BEManager()
    {
    }

    public static synchronized BEManager getInstance()
    {
        if (beManager == null)
        {
            beManager = new BEManager();
        }

        return beManager;
    }

    public void init(Context context, OnMessageReceived onMessageReceived)
    {
        this.context = context;
        this.onMessageReceived = onMessageReceived;
    }

    public void addClient(JSONObject client)
    {
        String ENDPOINT = "api/clientes";

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WOLKE2_URL + ENDPOINT,
                client,
                onSuccessListener,
                error -> {
                    String errorMessage = "Error al registrar cliente";
                    if (error.networkResponse != null && error.networkResponse.data != null)
                    {
                        // Obtener string
                        String errorJsonString = new String(error.networkResponse.data,
                                StandardCharsets.UTF_8);

                        // Convertir respuesta a JSON
                        try
                        {
                            JSONObject responseJson = new JSONObject(errorJsonString);
                            errorMessage = responseJson.getString("message");
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                        Timber.e(errorMessage);
                    }

                    onMessageReceived.onError(errorMessage, ADD_CLIENT_WS);
                }
        );

        currentService = ADD_CLIENT_WS;
        sendRequest(request);
    }

    public void addClientAccount(String clientId, String idClienteEmax)
    {
        String ENDPOINT = "api/clientes/%s/cuentas";
        String stationMongoId = SharedPreferencesManager.getString(context,
                ChipREDConstants.STATION_MONGO_ID);

        // Completar endpoint
        ENDPOINT = String.format(Locale.getDefault(), ENDPOINT, clientId);

        // Definir cuerpo de la petición
        JSONObject requestBody = new JSONObject();
        try
        {
            requestBody.put("estacion", stationMongoId);
            requestBody.put("id_cliente_local", idClienteEmax);
        }
        catch (JSONException e)
        {
            onMessageReceived.onError("Error al procesar información", ADD_CLIENT_ACCOUNT_WS);
        }

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WOLKE2_URL + ENDPOINT,
                requestBody,
                onSuccessListener,
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        String errorMessage = "Error al registrar cuenta de cliente";
                        if (error.networkResponse != null && error.networkResponse.data != null)
                        {
                            // Obtener string
                            String errorJsonString = new String(error.networkResponse.data,
                                    StandardCharsets.UTF_8);

                            // Convertir respuesta a JSON
                            try
                            {
                                JSONObject responseJson = new JSONObject(errorJsonString);
                                errorMessage = responseJson.getString("message");
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }

                            Log.e(TAG, errorMessage);
                        }

                        onMessageReceived.onError(errorMessage,
                                ADD_CLIENT_ACCOUNT_WS);
                    }
                }
        );

        currentService = ADD_CLIENT_ACCOUNT_WS;
        sendRequest(request);
    }

    public void reserveBilletSale(
            String clientId,
            JSONArray billetsIds,
            int loadingPosition,
            String plates,
            String platesUrl)
    {
        String ENDPOINT = "rpc/reservar_saldo";
        String stationMongoId = SharedPreferencesManager.getString(context,
                ChipREDConstants.STATION_MONGO_ID);

        // Completar endpoint
        ENDPOINT = String.format(Locale.getDefault(), ENDPOINT, clientId);

        // Definir cuerpo de la petición
        JSONObject requestBody = new JSONObject();
        try
        {
            requestBody.put("id_cliente", clientId);
            requestBody.put("id_estacion", stationMongoId);
            requestBody.put("ids_billetes", billetsIds);
            requestBody.put("posicion_carga", loadingPosition);
            requestBody.put("placas", plates);
            requestBody.put("url_placas", platesUrl == null ? "" : platesUrl);
        }
        catch (JSONException e)
        {
            onMessageReceived.onError("Error al procesar información", RESERVE_BILLET_SALE_WS);
        }

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WOLKE2_URL + ENDPOINT,
                requestBody,
                onSuccessListener,
                error ->
                {
                    String errorMessage = "Error al reservar posición de carga";
                    if (error.networkResponse != null && error.networkResponse.data != null)
                    {
                        // Obtener string
                        String errorJsonString = new String(error.networkResponse.data,
                                StandardCharsets.UTF_8);

                        // Convertir respuesta a JSON
                        try
                        {
                            JSONObject responseJson = new JSONObject(errorJsonString);
                            errorMessage = responseJson.getString("message");
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                        Log.e(TAG, errorMessage);
                    }

                    onMessageReceived.onError(errorMessage,
                            RESERVE_BILLET_SALE_WS);
                }
        );

        currentService = RESERVE_BILLET_SALE_WS;
        sendRequest(request);
    }

    public void cancelSaleReservation(
            String reservationId)
    {
        String ENDPOINT = "rpc/cancelar_reservacion_saldo";

        // Definir cuerpo de la petición
        JSONObject requestBody = new JSONObject();
        try
        {
            requestBody.put("id_reservacion", reservationId);
        }
        catch (JSONException e)
        {
            onMessageReceived.onError("Error al procesar información", CANCEL_SALE_RESERVATION);
        }

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WOLKE2_URL + ENDPOINT,
                requestBody,
                onSuccessListener,
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        String errorMessage = "Error al cancelar reservación";
                        if (error.networkResponse != null && error.networkResponse.data != null)
                        {
                            // Obtener string
                            String errorJsonString = new String(error.networkResponse.data,
                                    StandardCharsets.UTF_8);

                            // Convertir respuesta a JSON
                            try
                            {
                                JSONObject responseJson = new JSONObject(errorJsonString);
                                errorMessage = responseJson.getString("message");
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }

                            Log.e(TAG, errorMessage);
                        }

                        onMessageReceived.onError(errorMessage,
                                CANCEL_SALE_RESERVATION);
                    }
                }
        );

        currentService = CANCEL_SALE_RESERVATION;
        sendRequest(request);
    }

    public void addToBalance(String accountId, double amount, Pumper pumper)
    {
        String ENDPOINT = "rpc/abonar_saldo";

        // Definir cuerpo de la petición
        JSONObject requestBody = new JSONObject();
        try
        {
            requestBody.put("id_cuenta", accountId);
            requestBody.put("cantidad", amount);

            JSONObject pumperJson = new JSONObject();
            pumperJson.put("id", pumper.getEmaxId());
            pumperJson.put("nombre", pumper.getName());
            pumperJson.put("iniciales", pumper.getNameInitials());

            requestBody.put("despachador", pumperJson);
        }
        catch (JSONException e)
        {
            onMessageReceived.onError("Error al procesar información", ADD_TO_BALANCE);
        }

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WOLKE2_URL + ENDPOINT,
                requestBody,
                onSuccessListener,
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        String errorMessage = "Error al abonar saldo";
                        if (error.networkResponse != null && error.networkResponse.data != null)
                        {
                            // Obtener string
                            String errorJsonString = new String(error.networkResponse.data,
                                    StandardCharsets.UTF_8);

                            // Convertir respuesta a JSON
                            try
                            {
                                JSONObject responseJson = new JSONObject(errorJsonString);
                                errorMessage = responseJson.getString("message");
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }

                            Log.e(TAG, errorMessage);
                        }

                        onMessageReceived.onError(errorMessage,
                                ADD_TO_BALANCE);
                    }
                }
        );

        currentService = ADD_TO_BALANCE;
        sendRequest(request);
    }

    private void sendRequest(JsonObjectRequest request)
    {
        // Agregar parámetros a request
        request.setRetryPolicy(new DefaultRetryPolicy(20000, 0, 0));

        // Obtener requestQueue
        RequestQueueSingelton.getInstance(context.getApplicationContext()).getRequestQueue();

        try
        {
            Log.d(TAG, "Request: " + request.getBody().toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        // Agregar request
        RequestQueueSingelton.getInstance(context).addToRequestQueue(request);
    }

    public interface OnMessageReceived
    {
        void onResponse(JSONObject response, int service);

        void onError(String message, int service);
    }
}
