package com.binarium.chipredes.emax;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.utils.RequestQueueSingelton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import timber.log.Timber;

public class WsChipredManager {
    private static final String TAG = WsChipredManager.class.getSimpleName();

    public static void testConnection(Context context,
                                      final EmaxProvider.SimecobConnectionInterface simecobConnectionInterface) {
        String serviceBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3" +
                ".org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap" +
                ".org/soap/encoding/\">\n" +
                "   <SOAP-ENV:Body>\n" +
                "      <ns4835:testConnection xmlns:ns4835=\"http://tempuri.org\">\n" +
                "         <testConnection>\n" +
                "            <nombre xsi:type=\"xsd:string\">Prueba</nombre>\n" +
                "         </testConnection>\n" +
                "      </ns4835:testConnection>\n" +
                "   </SOAP-ENV:Body>\n" +
                "</SOAP-ENV:Envelope>";

        sendRequest(context, serviceBody, simecobConnectionInterface, "No responde servidor EMAX");
    }

    public static void addCostumer(Context context, JSONObject client,
                                   final EmaxProvider.SimecobConnectionInterface simecobConnectionInterface) {
        String serviceBody = "<?xml version=\"1.0\" encoding=\"utf-8\"?><SOAP-ENV:Envelope " +
                "SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3" +
                ".org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap" +
                ".org/soap/encoding/\"><SOAP-ENV:Body><ns5061:addCustomerChipRed " +
                "xmlns:ns5061=\"http://tempuri.org\"><addCustomerChipRed><rfc " +
                "xsi:type=\"xsd:string\">%s</rfc><razon_social " +
                "xsi:type=\"xsd:string\">%s</razon_social><correos " +
                "xsi:type=\"xsd:string\">%s</correos></addCustomerChipRed></ns5061" +
                ":addCustomerChipRed></SOAP-ENV:Body" +
                "></SOAP-ENV:Envelope>";

        // Agregar parámetros a la cadena
        try {
            serviceBody = String.format(Locale.getDefault(), serviceBody,
                    client.getString("rfc"),
                    client.getString("nombre"),
                    client.getString("email")
            );
        } catch (JSONException e) {
            e.printStackTrace();
            simecobConnectionInterface.onError("Error en datos proporcionados");
            return;
        }

        sendRequest(context, serviceBody, simecobConnectionInterface, "Error al agregar " +
                "cliente en EMAX");
    }

    public static void addVehicle(Context context, JSONObject vehicle,
                                  final EmaxProvider.SimecobConnectionInterface simecobConnectionInterface) {
        String serviceBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <SOAP-ENV:Envelope " +
                "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3" +
                ".org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas" +
                ".xmlsoap.org/soap/encoding/\"> <SOAP-ENV:Body> <ns2137:addVehicleChipRed " +
                "xmlns:ns2137=\"http://tempuri.org\"> <addVehicleChipRed> <idcliente " +
                "xsi:type=\"xsd:string\">%s</idcliente> <placas " +
                "xsi:type=\"xsd:string\">%s</placas> <tipo_vehiculo " +
                "xsi:type=\"xsd:string\">%s</tipo_vehiculo> <no_tag " +
                "xsi:type=\"xsd:string\">%s</no_tag> <nip " +
                "xsi:type=\"xsd:string\">%s</nip> <consumo_maximo " +
                "xsi:type=\"xsd:string\">%s</consumo_maximo> </addVehicleChipRed> " +
                "</ns2137:addVehicleChipRed> </SOAP-ENV:Body> </SOAP-ENV:Envelope>";

        // Agregar parámetros a la cadena
        try {
            serviceBody = String.format(Locale.getDefault(), serviceBody,
                    vehicle.getString("id_cliente"),
                    vehicle.getString("placas"),
                    vehicle.getString("tipo_vehiculo"),
                    vehicle.getString("no_tag"),
                    vehicle.getString("nip"),
                    vehicle.getString("consumo_maximo")
            );
        } catch (JSONException e) {
            e.printStackTrace();
            simecobConnectionInterface.onError("Error en datos proporcionados");
            return;
        }

        sendRequest(context, serviceBody, simecobConnectionInterface, "Error al registrar " +
                "vehículo en EMAX");
    }

    public static void registerDeposit(Context context, JSONObject depositData,
                                       final EmaxProvider.SimecobConnectionInterface simecobConnectionInterface) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        String serviceBody = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap" +
                ".org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap" +
                ".org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
                "    <SOAP-ENV:Body>\n" +
                "       <ns1267:registerDeposit xmlns:ns1267=\"http://tempuri.org\">\n" +
                "           <registerDeposit>\n" +
                "               <idclie xsi:type=\"xsd:string\">%s</idclie>\n" +
                "               <importe xsi:type=\"xsd:string\">%s</importe>\n" +
                "               <tipopago xsi:type=\"xsd:string\">%s</tipopago>\n" +
                "               <referencia xsi:type=\"xsd:string\">%s</referencia>\n" +
                "               <folio xsi:type=\"xsd:string\">%s</folio>\n" +
                "               <fecha xsi:type=\"xsd:string\">%s</fecha>\n" +
                "           </registerDeposit>\n" +
                "       </ns1267:registerDeposit>\n" +
                "   </SOAP-ENV:Body>\n" +
                "</SOAP-ENV:Envelope>";

        // Obtener parámetros del objeto JSON
        try {
            String amount = ChipREDConstants.TWO_DECIMALS_FORMAT.format(depositData.getDouble("cantidad"));
            String idCliente = depositData.getString("id_cliente_local");
            String tipoPago = depositData.getString("tipo_pago");
            String referencia = depositData.getString("referencia");
            String folio = depositData.getString("folio");

            serviceBody = String.format(Locale.getDefault(), serviceBody,
                    idCliente,
                    amount,
                    tipoPago,
                    referencia,
                    folio,
                    dateFormat.format(new Date())
            );
        } catch (JSONException e) {
            e.printStackTrace();
            simecobConnectionInterface.onError("Error en datos proporcionados");
            return;
        }

        sendRequest(context, serviceBody, simecobConnectionInterface, "Error al registrar " +
                "depósito en EMAX");
    }

    public static void getAuth(Context context, JSONObject clientData,
                               final EmaxProvider.SimecobConnectionInterface simecobConnectionInterface) {
        String serviceBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3" +
                ".org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap" +
                ".org/soap/encoding/\">\n" +
                "   <SOAP-ENV:Body>\n" +
                "      <ns6610:authCustomerChipRed xmlns:ns6610=\"http://tempuri.org\">\n" +
                "         <authCustomerChipRed>\n" +
                "            <identificador xsi:type=\"xsd:string\">%s</identificador>\n" +
                "            <nip xsi:type=\"xsd:string\">%s</nip>\n" +
                "         </authCustomerChipRed>\n" +
                "      </ns6610:authCustomerChipRed>\n" +
                "   </SOAP-ENV:Body>\n" +
                "</SOAP-ENV:Envelope>";

        // Obtener parámetros del objeto JSON
        try {
            String tagNumber = clientData.getString("identificador");
            String nip = clientData.getString("nip");

            serviceBody = String.format(Locale.getDefault(), serviceBody,
                    tagNumber,
                    nip
            );
        } catch (JSONException e) {
            e.printStackTrace();
            simecobConnectionInterface.onError("Error en datos proporcionados");
            return;
        }

        sendRequest(context, serviceBody, simecobConnectionInterface, "Error al solicitar " +
                "autorización a EMAX");
    }

    private static void sendSOAPRequest(Context context, final String body,
                                        Response.Listener<String>
                                                responseListener,
                                        Response.ErrorListener errorListener) {
        String emaxIp = SharedPreferencesManager.getString(context, ChipREDConstants.EMAX_URL);
        String emaxUrl = String.format(Locale.getDefault(), "http://%s/emax/extserv/wschipred.php"
                , emaxIp);

        // Mostrar cuerpo de petición
        Log.d(TAG, "Request body: ");
        Log.d(TAG, body);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, emaxUrl,
                responseListener, errorListener) {
            @Override
            public String getBodyContentType() {
                return "text/xml";
            }

            @Override
            public byte[] getBody() {
                return body.getBytes(StandardCharsets.UTF_8);
            }
        };


        // Obtener requestQueue
        RequestQueueSingelton.getInstance(context.getApplicationContext()).getRequestQueue();

        Log.d(TAG, "Request: " + stringRequest.toString());
        RequestQueueSingelton.getInstance(context).addToRequestQueue(stringRequest);
    }

    private static void sendRequest(Context context, final String body,
                                    final EmaxProvider.SimecobConnectionInterface simecobConnectionInterface,
                                    final String errorMessage) {
        String emaxIp = SharedPreferencesManager.getString(context, ChipREDConstants.EMAX_URL);
        final String emaxUrl = String.format(Locale.getDefault(), "http://%s/emax/extserv" +
                        "/wschipred.php"
                , emaxIp);

        // Mostrar cuerpo de petición
        Timber.d("Request body: ");
        Timber.d(body);

        // Crear nuevo Thread
        Thread requestThread = new Thread(() -> {
            OkHttpClient client = new OkHttpClient();

            RequestBody requestBody = RequestBody.create(body, MediaType.parse("text/xml"));
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(emaxUrl)
                    .post(requestBody)
                    .addHeader("Content-Type", "text/xml")
                    .build();

            try (okhttp3.Response response = client.newCall(request).execute()) {
                if (response.isSuccessful()) {
                    String responseString = null;
                    try {
                        responseString = response.body().string();
                    } catch (NullPointerException e) {
                        Timber.e(e.toString());
                    }

                    simecobConnectionInterface.onSuccess(responseString == null ?
                            "Petición ejecutada correctamente" :
                            responseString);
                } else {
                    simecobConnectionInterface.onError(errorMessage);
                }
            } catch (IOException e) {
                e.printStackTrace();
                simecobConnectionInterface.onError("Error al enviar petición a EMAX");
            }
        });

        requestThread.setName("WsChipredThread");
        requestThread.start();
    }
}
