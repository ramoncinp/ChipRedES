package com.binarium.chipredes.emax;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.SharedPreferencesManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Locale;

public class DataBaseConnector extends AsyncTaskLoader<JSONObject>
{
    private static final String TAG = DataBaseConnector.class.getSimpleName();

    private static final String USER = "binarium";
    private static final String PASS = "bin170903";
    private static final String DATABASE = "smax";
    private static final String TABLE = "cicislas";

    public static final String GET_DISPENSERS = "getDispensers";

    private Context context;
    private String key;

    public DataBaseConnector(@NonNull Context context)
    {
        super(context);
        this.context = context;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    @Nullable
    @Override
    public JSONObject loadInBackground()
    {
        JSONObject result = new JSONObject();

        try
        {
            // Obtener ip de servidor
            String emaxIp = SharedPreferencesManager.getString(context, ChipREDConstants.EMAX_URL);

            // Validar ip
            if (emaxIp.isEmpty())
            {
                Log.e(TAG, "No se encontró IP de servidor emax");
            }

            // Obtener librería
            Class.forName("com.mysql.jdbc.Driver");

            // Construir dirección para crear conexión
            String connectionAddress = String.format(Locale.getDefault(), "jdbc:mysql://%s/%s"
                    , emaxIp, DATABASE);

            // Crear conexión
            Connection conn = DriverManager.getConnection(connectionAddress, USER, PASS);

            // Crear query
            String mQuery = "SELECT * FROM " + TABLE;

            // Ejecutar query
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(mQuery);

            // Crear arrelgo JSON para almacenar los resultados
            JSONArray resultArray = new JSONArray();
            while (rs.next())
            {
                JSONObject disp = new JSONObject();

                // Obtener valores
                String ip = rs.getString("ciipaddress");
                String id = rs.getString("ciidisla");

                // Definir valores a objeto JSON
                disp.put("id", id);
                disp.put("ip", ip);

                // Agregar objeto al arreglo
                resultArray.put(disp);
            }

            // Almacenar arreglo en objeto JSON
            result.put("dispensarios", resultArray);

            // Cerrar conexión
            conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG, e.getMessage() == null ? "Error desconocido" : e.getMessage());
        }

        return result;
    }

    @Override
    public void deliverResult(@Nullable JSONObject data)
    {
        super.deliverResult(data);
    }

    @Override
    protected void onStartLoading()
    {
        // Start loading
        forceLoad();
    }

    @Override
    protected void onStopLoading()
    {
        super.onStopLoading();
    }

    @Override
    protected void onReset()
    {
        super.onReset();
    }
}
