package com.binarium.chipredes.emax.domain.usecases

import com.binarium.chipredes.emax.EmaxDBManager
import com.binarium.chipredes.emax.model.LoadingPositionState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetLoadingPositionStateUseCase @Inject constructor(
    private val emaxDBManager: EmaxDBManager,
) {

    suspend operator fun invoke(
        loadingPosition: Int,
    ): LoadingPositionState = withContext(Dispatchers.IO) {
        val state = try {
            emaxDBManager.getLoadingPositionState(loadingPosition)
        } catch (e: Exception) {
            null
        }

        return@withContext if (state != 0) LoadingPositionState.Incorrect else LoadingPositionState.Idle
    }
}
