package com.binarium.chipredes.emax.domain.usecases

import com.binarium.chipredes.billets.data.model.AdjustablePurchase
import com.binarium.chipredes.billets.data.model.EmaxPurchasesRequest
import com.binarium.chipredes.billets.data.model.StationClientInfo
import com.binarium.chipredes.billets.data.model.toAdjustablePurchase
import com.binarium.chipredes.wolke.domain.usecases.GetStationMongoIdUseCase
import com.binarium.chipredes.wolke.domain.usecases.GetStationNumberUseCase
import com.binarium.chipredes.emax.EmaxDBManager
import javax.inject.Inject

class GetAdjustableEmaxPurchasesUseCase @Inject constructor(
    val getStationMongoIdUseCase: GetStationMongoIdUseCase,
    val getStationNumberUseCase: GetStationNumberUseCase,
    val emaxDBManager: EmaxDBManager,
) {

    suspend operator fun invoke(
        clientChipRedId: String,
        emaxPurchasesRequest: EmaxPurchasesRequest,
    ): List<AdjustablePurchase> {
        val purchases = emaxPurchasesRequest.run {
            emaxDBManager.getClientPurchases(
                clientId, startDate, endDate
            )
        }
        return purchases?.map { it.toAdjustablePurchase(getStationClientInfo(clientChipRedId)) }
            ?: listOf()
    }

    private fun getStationClientInfo(clientChipRedId: String) = StationClientInfo(
        clientChipRedId,
        getStationMongoIdUseCase(),
        getStationNumberUseCase()
    )
}
