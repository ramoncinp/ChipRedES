package com.binarium.chipredes.emax.model

data class EmaxPurchase(
    val id: Int,
    val clientId: String,
    val vehicle: String,
    val saleType: String,
    val pumperId: String,
    val operationId: String,
    val dispenser: String,
    val product: String,
    val unitPrice: Double,
    val liters: Double,
    val amount: Double,
    val ticket: String,
    val dateTime: String,
    val loadingPosition: String,
    val preAuth: String? = null,
    val odometer: String? = null,
    val hoseNumber: String? = null,
    val counter: String? = null
)
