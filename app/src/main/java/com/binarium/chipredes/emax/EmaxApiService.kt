package com.binarium.chipredes.emax

import android.content.Context
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.SharedPreferencesManager
import com.binarium.chipredes.balance.models.PaymentData
import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke.models.QueriedClient
import com.binarium.chipredes.wolke2.models.ClientStationAccount
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import org.json.JSONObject
import ru.gildor.coroutines.okhttp.await
import java.io.ByteArrayInputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

private const val BASE_URL = "http://%s/emax/extserv/wschipred.php"

class EmaxApiService(application: Context, private val logger: Logger) {

    private val sharedPreferencesManager: SharedPreferencesManager =
        SharedPreferencesManager(application)

    private lateinit var ipAddress: String
    private lateinit var url: String

    init {
        setUrl()
    }

    private fun setUrl() {
        ipAddress = sharedPreferencesManager.getStringFromSP(ChipREDConstants.EMAX_URL, "")
        url = BASE_URL.format(ipAddress)
    }

    suspend fun testConnection(customIp: String = ipAddress): Boolean {
        val serviceBody = """<?xml version="1.0" encoding="UTF-8"?>
                            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                               <SOAP-ENV:Body>
                                  <ns4835:testConnection xmlns:ns4835="http://tempuri.org">
                                     <testConnection>
                                        <nombre xsi:type="xsd:string">Prueba</nombre>
                                     </testConnection>
                                  </ns4835:testConnection>
                               </SOAP-ENV:Body>
                            </SOAP-ENV:Envelope>"""

        url = BASE_URL.format(customIp)
        val response = sendRequest(serviceBody)
        var success = false

        if (response.isSuccessful) {
            success = true
        } else {
            val body = response.getBodyResponse().orEmpty()
            logger.postMessage(
                "[EmaxApiService::testConnection] ${response.message} - $body - $url",
                LogLevel.ERROR
            )
        }

        return success
    }

    suspend fun addClient(client: QueriedClient): JSONObject {
        val serviceBodyTemplate: String =
            "<?xml version=\"1.0\" encoding=\"utf-8\"?><SOAP-ENV:Envelope " +
                    "SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                    "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                    "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3" +
                    ".org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap" +
                    ".org/soap/encoding/\"><SOAP-ENV:Body><ns5061:addCustomerChipRed " +
                    "xmlns:ns5061=\"http://tempuri.org\"><addCustomerChipRed><rfc " +
                    "xsi:type=\"xsd:string\">%s</rfc><razon_social " +
                    "xsi:type=\"xsd:string\">%s</razon_social><correos " +
                    "xsi:type=\"xsd:string\">%s</correos></addCustomerChipRed></ns5061" +
                    ":addCustomerChipRed></SOAP-ENV:Body" +
                    "></SOAP-ENV:Envelope>"

        val emaxClientId: String

        val serviceBody = serviceBodyTemplate.format(
            client.claveFiscal,
            client.nombre,
            client.email
        )

        val response = sendRequest(serviceBody)

        val resultMap = JSONObject()
        if (response.code == 200) {
            // Obtener id de cliente
            try {
                val bodyString = response.body?.string()
                val responseData =
                    EmaxResponseParser.parseEmaxResponse(ByteArrayInputStream(bodyString?.toByteArray()))
                if (responseData.has("id_de_cliente")) {
                    emaxClientId = responseData["id_de_cliente"] as String
                    resultMap.put("result", "ok")
                    resultMap.put("message", "Cliente creado en EMAX")
                    resultMap.put("emax_client_id", emaxClientId)
                } else {
                    resultMap.put("result", "error")
                    resultMap.put("message", "msgerror")
                }
            } catch (e: Exception) {
                resultMap.put("result", "error")
                resultMap.put("message", "Error al procesar respuesta de EMAX")
            }
        } else {
            resultMap.put("result", "error")
            resultMap.put("message", "Error al crear cliente en EMAX")
        }

        return resultMap
    }

    suspend fun addVehicle(account: ClientStationAccount): Boolean {
        val serviceBodyTemplate = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <SOAP-ENV:Envelope " +
                "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3" +
                ".org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas" +
                ".xmlsoap.org/soap/encoding/\"> <SOAP-ENV:Body> <ns2137:addVehicleChipRed " +
                "xmlns:ns2137=\"http://tempuri.org\"> <addVehicleChipRed> <idcliente " +
                "xsi:type=\"xsd:string\">%s</idcliente> <placas " +
                "xsi:type=\"xsd:string\">%s</placas> <tipo_vehiculo " +
                "xsi:type=\"xsd:string\">%s</tipo_vehiculo> <no_tag " +
                "xsi:type=\"xsd:string\">%s</no_tag> <nip " +
                "xsi:type=\"xsd:string\">%s</nip> <consumo_maximo " +
                "xsi:type=\"xsd:string\">%s</consumo_maximo> </addVehicleChipRed> " +
                "</ns2137:addVehicleChipRed> </SOAP-ENV:Body> </SOAP-ENV:Envelope>"

        val serviceBody = serviceBodyTemplate.format(
            account.idClienteLocal,
            account.vehiculo.placas,
            account.vehiculo.tipoVehiculo,
            account.vehiculo.noTag,
            account.vehiculo.nip,
            account.vehiculo.consumoMaximo
        )

        val response = sendRequest(serviceBody)

        return response.code == 200
    }

    suspend fun getAuth(localClientData: LocalClientData): EmaxSaleAuthResult? {
        val serviceBodyTemplate = """<?xml version="1.0" encoding="UTF-8"?>
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
           <SOAP-ENV:Body>
              <ns6610:authCustomerChipRed xmlns:ns6610="http://tempuri.org">
                 <authCustomerChipRed>
                    <identificador xsi:type="xsd:string">%s</identificador>
                    <nip xsi:type="xsd:string">%s</nip>
                 </authCustomerChipRed>
              </ns6610:authCustomerChipRed>
           </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>"""

        val serviceBody = serviceBodyTemplate.format(
            localClientData.vehiculo?.noTag,
            localClientData.vehiculo?.nip
        )

        val response = sendRequest(serviceBody)
        val bodyResult = response.getBodyResponse()

        return if (bodyResult != null) EmaxParserUtils.parseBilletResult(bodyResult) else null
    }

    suspend fun registerDeposit(paymentData: PaymentData): EmaxRegisterDepositResult? {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val serviceBodyTemplate = """<?xml version="1.0" encoding="utf-8"?>
            <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
                <SOAP-ENV:Body>
                   <ns2217:registerDeposit xmlns:ns2217="http://tempuri.org">
                       <registerDeposit>
                           <idclie xsi:type="xsd:string">%s</idclie>
                           <importe xsi:type="xsd:string">%s</importe>
                           <tipopago xsi:type="xsd:string">%s</tipopago>
                           <referencia xsi:type="xsd:string">%s</referencia>
                           <folio xsi:type="xsd:string">%s</folio>
                           <fecha xsi:type="xsd:string">%s</fecha>
                           <idbanco xsi:type="xsd:string">%s</idbanco>
                           <terminal xsi:type="xsd:string">%s</terminal>
                       </registerDeposit>
                   </ns2217:registerDeposit>
               </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>"""

        val serviceBody = serviceBodyTemplate.format(
            paymentData.localClientData?.idClienteLocal,
            paymentData.amount,
            paymentData.emaxPaymentType?.value,
            paymentData.reference ?: "",
            paymentData.id ?: "",
            dateFormat.format(Date()),
            paymentData.emaxAccount ?: "",
            paymentData.terminal ?: ""
        )

        val response = sendRequest(serviceBody)
        val bodyResult = response.getBodyResponse()

        return if (bodyResult != null) EmaxParserUtils.parseRegisterDepositResult(bodyResult) else null
    }

    private suspend fun sendRequest(requestBody: String): Response {
        val body = requestBody.toRequestBody()
        val client = OkHttpClient.Builder().build()
        val request: Request = Request.Builder()
            .url(url)
            .post(body)
            .addHeader("Content-Type", "text/xml")
            .build()

        return client.newCall(request).await()
    }

    private fun Response.getBodyResponse() = body?.string()
}
