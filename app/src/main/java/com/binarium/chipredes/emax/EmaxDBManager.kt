package com.binarium.chipredes.emax

import android.content.Context
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.SharedPreferencesManager
import com.binarium.chipredes.balance.models.BankTerminal
import com.binarium.chipredes.balance.models.EmaxBank
import com.binarium.chipredes.config.domain.usecases.GetEmaxClientPrefUseCase
import com.binarium.chipredes.emax.model.EmaxPumper
import com.binarium.chipredes.emax.model.EmaxPurchase
import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.utils.DateUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.sql.Connection
import java.sql.DriverManager
import java.util.*

class EmaxDBManager (
    private val application: Context,
    private val getEmaxClientPrefUseCase: GetEmaxClientPrefUseCase,
    private val logger: Logger
) {

    companion object {
        private const val USER = "binarium"
        private const val PASS = "bin170903"
        private const val SMAX_DATABASE = "smax"
        private const val SCLIE_DATABASE = "sclie"
        private const val EXT_DRV_SELLS = "extdrvsells"
    }

    fun getBanks(): List<EmaxBank> {
        val connection = createConnection(SMAX_DATABASE) ?: return emptyList()
        val query = "SELECT idbanco, nombre FROM cat_bancos"

        val statement = connection.createStatement()
        val rs = statement.executeQuery(query)

        val banks = mutableListOf<EmaxBank>()
        while (rs.next()) {
            val id = rs.getString("idbanco")
            val name = rs.getString("nombre")
            banks.add(EmaxBank(id, name))
        }

        return banks
    }

    fun getBankTerminals(): List<BankTerminal> {
        val connection = createConnection(SMAX_DATABASE) ?: return emptyList()
        val query = "SELECT ccidclie, ccnombre FROM ccclient WHERE cctipocuenta = 2"

        val statement = connection.createStatement()
        val rs = statement.executeQuery(query)

        val banks = mutableListOf<BankTerminal>()
        while (rs.next()) {
            val id = rs.getString("ccidclie")
            val name = rs.getString("ccnombre")
            banks.add(BankTerminal(id, name))
        }

        return banks
    }

    fun getPumpers(): List<EmaxPumper>? {
        val pumpers = mutableListOf<EmaxPumper>()
        val connection = createConnection(SMAX_DATABASE) ?: return null
        val query =
            "SELECT cdiddesp as ID, cdcvedes as INICIALES, cdnombre as NOMBRE, ctidentifier as TAG\n" +
                    "FROM cddespac as despachadores\n" +
                    "INNER JOIN cttags as tags\n" +
                    "ON despachadores.cdiddesp = tags.cttequiva\n" +
                    "WHERE despachadores.cdiddesp >= 10000 \n" +
                    "AND despachadores.cdactivo = 1\n" +
                    "AND tags.ctstatus = 'A'\n" +
                    "AND tags.cttypetag = 'D'\n" +
                    "ORDER BY NOMBRE"

        val statement = connection.createStatement()
        val rs = statement.executeQuery(query)

        while (rs.next()) {
            val id = rs.getString("ID").trim()
            val iniciales = rs.getString("INICIALES").trim()
            val nombre = rs.getString("NOMBRE").trim()
            val tag = rs.getString("TAG").trim()

            val pumper = EmaxPumper(id, nombre, iniciales, tag)
            pumpers.add(pumper)
        }

        connection.close()
        return pumpers
    }

    fun getLoadingPositionState(loadingPosition: Int): Int? {
        var result = -1
        val connection = createConnection(SMAX_DATABASE) ?: return null
        val query = "SELECT cbstaaut FROM cbbuzonf WHERE cbposcar = $loadingPosition"

        val statement = connection.createStatement()
        val rs = statement.executeQuery(query)

        while (rs.next()) {
            result = rs.getInt("cbstaaut")
        }

        connection.close()
        return result
    }

    suspend fun getClientBalance(clientId: String): Double? = withContext(Dispatchers.IO) {
        var result = -1.0
        val connection = createConnection(SCLIE_DATABASE, getEmaxClientUrl()) ?: return@withContext null
        val query = "SELECT ccdebito FROM ccclient WHERE ccidclie = $clientId"

        val statement = connection.createStatement()
        val rs = statement.executeQuery(query)

        while (rs.next()) {
            result = rs.getDouble("ccdebito")
        }

        connection.close()
        return@withContext result
    }

    suspend fun getClientPurchases(
        clientId: String,
        startDate: Date,
        endDate: Date,
    ): List<EmaxPurchase>? =
        withContext(Dispatchers.IO) {
            val purchases = mutableListOf<EmaxPurchase>()
            val startDateStr = DateUtils.dateToChipredDateString(startDate, false)
            val endDateStr = DateUtils.dateToChipredDateString(endDate, false)

            val connection = createConnection(SCLIE_DATABASE, getEmaxClientUrl()) ?: return@withContext null
            val query =
                "SELECT dcidcons AS ID, " +
                        "dcidclie AS ID_CLIENTE, " +
                        "dcidvehi AS VEHICULO, " +
                        "dctipvta AS TIPO_VENTA, " +
                        "dciddesp AS DESPACHADOR, " +
                        "dcidoper AS ID_OPERACION, " +
                        "dcidisla AS DISPENSARIO, " +
                        "dcidprod AS PRODUCTO, " +
                        "dcprecio AS PRECIO, " +
                        "dccantid AS CANTIDAD, " +
                        "dcimpvta AS IMPORTE, " +
                        "dcticket AS TICKET, dcfchvta AS FECHA, dcposcar AS POSICION_CARGA\n" +
                        "FROM dcconsum\n" +
                        "WHERE dcidclie = $clientId\n" +
                        "AND dcfchvta > '$startDateStr'\n" +
                        "AND dcfchvta < '$endDateStr'\n" +
                        "ORDER BY dcticket DESC"

            val statement = connection.createStatement()
            val rs = statement.executeQuery(query)

            while (rs.next()) {
                purchases.add(
                    EmaxPurchase(
                        rs.getInt("ID"),
                        rs.getString("ID_CLIENTE").trim(),
                        rs.getString("VEHICULO").trim(),
                        rs.getString("TIPO_VENTA").trim(),
                        rs.getString("DESPACHADOR").trim(),
                        rs.getString("ID_OPERACION").trim(),
                        rs.getString("DISPENSARIO").trim(),
                        rs.getString("PRODUCTO").trim(),
                        rs.getDouble("PRECIO"),
                        rs.getDouble("CANTIDAD"),
                        rs.getDouble("IMPORTE"),
                        rs.getString("TICKET").trim(),
                        rs.getString("FECHA").adjustStringDate().trim(),
                        rs.getString("POSICION_CARGA").trim()
                    )
                )
            }

            purchases
        }

    suspend fun getPurchaseFromPreAuth(preAuth: String): EmaxPurchase? =
        withContext(Dispatchers.IO) {
            var purchase: EmaxPurchase? = null
            val connection = createConnection(EXT_DRV_SELLS, getEmaxClientUrl()) ?: return@withContext null
            val query =
                "SELECT id AS ID, " +
                        "idclien AS ID_CLIENTE, " +
                        "idvehicu AS VEHICULO, " +
                        "iddesp AS DESPACHADOR, " +
                        "pumpno AS DISPENSARIO, " +
                        "idprod AS PRODUCTO, " +
                        "price AS PRECIO, " +
                        "quantity AS CANTIDAD, " +
                        "cost AS IMPORTE, " +
                        "ticket AS TICKET, " +
                        "datesell AS FECHA, " +
                        "extauth AS PREAUTH, " +
                        "odometer AS ODOMETRO, " +
                        "hoseno AS MANGUERA, " +
                        "counter AS CONTADOR, " +
                        "shiftsell AS ID_TURNO, " +
                        "flgprocess AS TIPO_VENTA, " +
                        "prtticket AS FLAG_IMPRESION, " +
                        "poscar AS POSICION_CARGA\n" +
                        "FROM sells\n" +
                        "WHERE extauth = $preAuth\n" +
                        "ORDER BY id DESC"

            val statement = connection.createStatement()
            val rs = statement.executeQuery(query)

            while (rs.next()) {
                purchase = EmaxPurchase(
                    rs.getInt("ID"),
                    rs.getString("ID_CLIENTE").trim(),
                    rs.getString("VEHICULO").trim(),
                    rs.getString("TIPO_VENTA").trim(),
                    rs.getString("DESPACHADOR").trim(),
                    "",
                    rs.getString("DISPENSARIO").trim(),
                    rs.getString("PRODUCTO").trim(),
                    rs.getDouble("PRECIO"),
                    rs.getDouble("CANTIDAD"),
                    rs.getDouble("IMPORTE"),
                    rs.getString("TICKET").trim(),
                    rs.getString("FECHA").adjustStringDate().trim(),
                    rs.getString("POSICION_CARGA").trim(),
                    rs.getString("PREAUTH").trim(),
                    rs.getString("ODOMETRO"),
                    rs.getString("MANGUERA"),
                    rs.getString("CONTADOR")
                )
            }

            purchase
        }

    suspend fun isPreAuthCanceled(preAuth: String): Boolean =
        withContext(Dispatchers.IO) {
            val connection = createConnection(SMAX_DATABASE, getEmaxClientUrl()) ?: return@withContext false
            val query = "SELECT * FROM logauth WHERE authno = $preAuth AND coderror = -1"
            val statement = connection.createStatement()
            val rs = statement.executeQuery(query)
            rs.next()
        }

    private fun String.adjustStringDate(): String {
        // La fecha apuntando a SCLIE se regresa con el siguiente formato 2022-03-14 14:18:25.0
        // Es por eso que se debe recortar el String hasta el punto
        val pointIdx = indexOf(".")
        return if (pointIdx == -1) this else substring(0, pointIdx)
    }

    private fun createConnection(
        dataBase: String,
        urlKey: String = ChipREDConstants.EMAX_URL,
    ): Connection? {
        val emaxIp = SharedPreferencesManager.getString(application, urlKey)
        if (emaxIp.isEmpty()) {
            Timber.e("No se encontró IP del servidor EMAX")
            return null
        }

        // Obtener librería
        Class.forName("com.mysql.jdbc.Driver")

        // Construir dirección para crear conexión
        val connectionAddress =
            String.format(Locale.getDefault(), "jdbc:mysql://%s/%s", emaxIp, dataBase)

        // Crear conexión
        var connection: Connection? = null
        try {
            connection = DriverManager.getConnection(connectionAddress, USER, PASS)
        } catch (exception: Exception) {
            Timber.e("Error al crear conexión con BDL")
            logger.postMessage(
                "[EmaxDBManager::createConnection] Error al crear conexion con BDL: ${exception.message}",
                LogLevel.ERROR
            )
        }

        return connection
    }

    private fun getEmaxClientUrl(): String {
        val isEmaxClientEnabled = getEmaxClientPrefUseCase()
        return if (isEmaxClientEnabled) ChipREDConstants.EMAX_CLIENT_URL else ChipREDConstants.EMAX_URL
    }
}