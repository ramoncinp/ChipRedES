package com.binarium.chipredes.emax;

import android.util.Log;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

public class EmaxVehicle
{
    public static final String TAG = EmaxVehicle.class.getSimpleName();

    private String nip;
    private String placas;
    private String tipoVehiculo;
    private String noTag;
    private double consumoMaximo;

    public EmaxVehicle()
    {
    }

    public EmaxVehicle(JSONObject data)
    {
        try
        {
            nip = data.getString("nip");
            placas = data.getString("placas");
            tipoVehiculo = data.getString("tipo_vehiculo");
            noTag = data.getString("no_tag");
            consumoMaximo = data.getDouble("consumo_maximo");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Log.e(TAG, "Error al crear vehículo a partir de objeto JSON");
        }
    }

    public String getNip()
    {
        return nip;
    }

    public String getPlacas()
    {
        return placas;
    }

    public String getTipoVehiculo()
    {
        return tipoVehiculo;
    }

    public String getNoTag()
    {
        return noTag;
    }

    public double getConsumoMaximo()
    {
        return consumoMaximo;
    }

    @NonNull
    @Override
    public String toString()
    {
        JSONObject jsonObj = new JSONObject();
        try
        {
            jsonObj.put("nip", nip);
            jsonObj.put("placas", placas);
            jsonObj.put("tipo_vehiculo", tipoVehiculo);
            jsonObj.put("no_tag", noTag);
            jsonObj.put("consumo_maximo", consumoMaximo);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return jsonObj.toString();
    }
}
