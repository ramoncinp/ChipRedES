package com.binarium.chipredes.emax.domain.usecases

import com.binarium.chipredes.emax.EmaxDBManager
import javax.inject.Inject

class GetClientBalanceUseCase @Inject constructor(
    private val emaxDBManager: EmaxDBManager,
) {

    suspend operator fun invoke(clientId: String): Double? {
        return emaxDBManager.getClientBalance(clientId)
    }
}
