package com.binarium.chipredes.emax;

import android.util.Log;
import android.util.Xml;

import com.binarium.chipredes.Product;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class EmaxResponseParser
{
    private static final String TAG = EmaxResponseParser.class.getSimpleName();
    private static final String ns = null;

    public static ArrayList<Product> parseListProdResponse(InputStream in) throws XmlPullParserException,
            IOException
    {
        ArrayList<Product> products = new ArrayList<>();

        try
        {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            String response = getProdListString(parser);
            if (response != null)
                products = productsStringToList(response);
        }
        finally
        {
            in.close();
        }

        return products;
    }

    private static String getProdListString(XmlPullParser parser) throws XmlPullParserException,
            IOException
    {
        String prodList = "";
        String currentTag = "";
        int event = parser.getEventType();
        while (event != XmlPullParser.END_DOCUMENT)
        {
            if (parser.getName() != null) currentTag = parser.getName();
            switch (event)
            {
                case XmlPullParser.START_TAG:
                    Log.d(TAG, "Start! of " + currentTag);
                    break;

                case XmlPullParser.END_TAG:
                    Log.d(TAG, "End! of " + currentTag);
                    break;

                case XmlPullParser.TEXT:
                    if (currentTag.equals("listprod"))
                    {
                        prodList = parser.getText();
                    }
                    break;
            }
            event = parser.next();
        }

        return prodList;
    }

    private static ArrayList<Product> productsStringToList(String productsListString)
    {
        ArrayList<String> pcProducts = new ArrayList<>();

        int idx = 0;
        while (idx < productsListString.length())
        {
            String product;
            int commaIdx = productsListString.indexOf(',', idx);
            if (commaIdx == -1)
            {
                // No hay coma, asignar lo que resta
                product = productsListString.substring(idx);
                // Actualizar índice
                idx = productsListString.length();
            }
            else
            {
                // Obtener producto
                product = productsListString.substring(idx, commaIdx);
                // Actualizar índice
                idx = commaIdx + 1;
            }
            pcProducts.add(product);
        }

        // Construir mapa
        ArrayList<Product> products = new ArrayList<>();

        for (String product : pcProducts)
        {
            // Ejemplo: 1-11.32
            // Obtener id
            String id = product.substring(0, 1);
            String price = product.substring(2);

            Product productToAdd = new Product();
            productToAdd.setId(id);
            productToAdd.setLiterPrice(Double.parseDouble(price));
            products.add(productToAdd);
        }

        // Ordenarlos
        Collections.sort(products, new Comparator<Product>()
        {
            @Override
            public int compare(Product product, Product t1)
            {
                return product.getId().compareTo(t1.getId());
            }
        });

        return products;
    }

    public static JSONObject parseEmaxResponse(InputStream in) throws XmlPullParserException,
            IOException
    {
        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();

        // Convertir respuesta en xml a JSON
        JSONObject responseData = getEmaxResponse(parser);

        // Cerrar Stream
        in.close();

        return responseData;
    }

    private static JSONObject getEmaxResponse(XmlPullParser parser) throws XmlPullParserException,
            IOException
    {
        JSONObject response = new JSONObject();
        String currentTag = "";
        int event = parser.getEventType();
        while (event != XmlPullParser.END_DOCUMENT)
        {
            if (parser.getName() != null) currentTag = parser.getName();
            switch (event)
            {
                case XmlPullParser.START_TAG:
                    Log.d(TAG, "Start! of " + currentTag);
                    break;

                case XmlPullParser.END_TAG:
                    Log.d(TAG, "End! of " + currentTag);
                    break;

                case XmlPullParser.TEXT:
                    try
                    {
                        switch (currentTag)
                        {
                            case "iderror":
                                response.put("iderror", parser.getText());
                                break;
                            case "idbierror":
                                response.put("idbierror", parser.getText());
                                break;
                            case "idsierror":
                                response.put("idsierror", parser.getText());
                                break;
                            case "id_de_cliente":
                                response.put("id_de_cliente", parser.getText());
                                break;
                            case "msgerror":
                                response.put("msgerror", parser.getText());
                                break;
                            case "authno":
                                response.put("authno", parser.getText());
                                break;
                            case "msgerrorident":
                                response.put("msgerrorident", parser.getText());
                                break;
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                    break;
            }
            event = parser.next();
        }

        return response;
    }
}
