package com.binarium.chipredes.emax.model

sealed class LoadingPositionState {
    object Idle : LoadingPositionState()
    object Incorrect : LoadingPositionState()
}
