package com.binarium.chipredes.emax;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.Product;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.configstation.StationFileManager;
import com.binarium.chipredes.dispenser.Dispenser;
import com.binarium.chipredes.disppreset.DispIpInfo;
import com.binarium.chipredes.utils.RequestQueueSingelton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class EmaxProvider {
    private static final String TAG = EmaxProvider.class.getSimpleName();
    private static final int LOADER_ID = 1;

    public static void getEmaxProducts(final Context context, int loadingPosition,
                                       final ProductsInterface productsInterface) {
        String mBody = String.format(Locale.getDefault(), "<?xml version=\"1.0\" " +
                "encoding=\"UTF-8\"?>\n" +
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www" +
                ".w3.org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas" +
                ".xmlsoap.org/soap/encoding/\">\n" +
                "   <SOAP-ENV:Body>\n" +
                "      <ns8031:getProdPC xmlns:ns8031=\"http://tempuri.org\">\n" +
                "         <getProdPC>\n" +
                "            <poscar xsi:type=\"xsd:int\">%d</poscar>\n" +
                "         </getProdPC>\n" +
                "      </ns8031:getProdPC>\n" +
                "   </SOAP-ENV:Body>\n" +
                "</SOAP-ENV:Envelope>", loadingPosition);

        Response.Listener<String> responseListener = new
                Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Convertir respuesta a lista de productos
                        parseProductsResponse(context, response, productsInterface);
                    }
                };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                productsInterface.onError("Error al obtener productos");
            }
        };

        sendSOAPRequest(context, mBody, responseListener, errorListener);
    }

    public static void getDispensersIpInfo(final Context context,
                                           final DispensersInterface dispensersInterface) {
        // Obtener dispensarios registrados en la estación
        final ArrayList<Dispenser> registeredDispensers =
                StationFileManager.getStationDispensers(context);

        // Obtener arreglo de ip's de dispensarios
        JSONArray dispArray = StationFileManager.getDispensersIps(context);

        // Complementar lista de dispensarios registrados con lista de
        // ip's de dispensarios obtenida de base de datos de EMAX
        dispensersInterface.onFetchedDispensers(dispsJsonToList(dispArray,
                registeredDispensers));
    }

    public static void syncDispensersIpInfo(final Context context,
                                            final DispensersInterface dispensersInterface) {
        // Obtener dispensarios registrados en la estación
        final ArrayList<Dispenser> registeredDispensers =
                StationFileManager.getStationDispensers(context);

        LoaderManager.LoaderCallbacks<JSONObject> callbacks =
                new LoaderManager.LoaderCallbacks<JSONObject>() {
                    @NonNull
                    @Override
                    public Loader<JSONObject> onCreateLoader(int id, @Nullable Bundle args) {
                        // Crear instancia de loader
                        DataBaseConnector dataBaseConnector = new DataBaseConnector(context);
                        dataBaseConnector.setKey(DataBaseConnector.GET_DISPENSERS);

                        return dataBaseConnector;
                    }

                    @Override
                    public void onLoadFinished(@NonNull Loader<JSONObject> loader, JSONObject data) {
                        // Evaluar si el objeto esta vacío
                        if (data.length() == 0) {
                            dispensersInterface.onError("Error al obtener direcciones de " +
                                    "dispensarios");
                        } else {
                            try {
                                // Obtener arreglo de dispensario
                                JSONArray dispArray = data.getJSONArray("dispensarios");

                                // Evaluar arreglo
                                if (dispArray.length() > 0) {
                                    // Complementar lista de dispensarios registrados con lista de
                                    // ip's de dispensarios obtenida de base de datos de EMAX
                                    dispensersInterface.onFetchedDispensers(dispsJsonToList(dispArray, registeredDispensers));
                                } else {
                                    dispensersInterface.onError("No se encontraron dispensario");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dispensersInterface.onError("Error al obtener direcciones de " +
                                        "dispensarios");
                            }
                        }
                    }

                    @Override
                    public void onLoaderReset(@NonNull Loader<JSONObject> loader) {
                        Log.d(TAG, "Loader reset");
                    }
                };

        Bundle bundle = new Bundle();
        ((FragmentActivity) context).getSupportLoaderManager().initLoader(LOADER_ID, bundle,
                callbacks);
    }

    private static ArrayList<DispIpInfo> dispsJsonToList(JSONArray dispsArray,
                                                         ArrayList<Dispenser> registeredDispensers) {
        HashMap<Integer, String> dispensersIps = new HashMap<>();
        ArrayList<DispIpInfo> dispsList = new ArrayList<>();

        try {
            // Iterar en arreglo de dispensario
            for (int i = 0; i < dispsArray.length(); i++) {
                // Obtener objeto json con información de dispensario
                JSONObject dispInfoJson = dispsArray.getJSONObject(i);

                // Obtener sus datos
                int dispNumber = dispInfoJson.getInt("id");
                String dispIpAddress = dispInfoJson.getString("ip");

                // Agregarlo al HashMap
                dispensersIps.put(dispNumber, dispIpAddress);
            }

            // Agregar los datos obtenidos a la lista a retornar
            for (Dispenser dispenser : registeredDispensers) {
                // Crear objeto para almacenar ip
                DispIpInfo dispIpInfo = new DispIpInfo();
                dispIpInfo.setDispNumber(dispenser.getNumber());
                dispIpInfo.setIpAddress(dispensersIps.get(dispenser.getNumber()));
                dispsList.add(dispIpInfo);
            }

            // Ordenar lista
            Collections.sort(dispsList, new Comparator<DispIpInfo>() {
                @Override
                public int compare(DispIpInfo dispIpInfo, DispIpInfo t1) {
                    return Integer.compare(dispIpInfo.getDispNumber(), t1.getDispNumber());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dispsList;
    }

    private static void parseProductsResponse(Context context, String response,
                                              ProductsInterface productsInterface) {
        ArrayList<Product> products = new ArrayList<>();

        try {
            // Crear lista de productos con id y precio unitario
            products =
                    EmaxResponseParser.parseListProdResponse(new ByteArrayInputStream(response.getBytes())
                    );
        } catch (Exception e) {
            productsInterface.onError("Error al obtener productos");
        }

        // Obtener de archivo local, productos de la estación
        ArrayList<Product> stationProducts = StationFileManager.getStationProducts(context);

        // Complementar información
        for (Product product : products) {
            String id = product.getId();

            for (Product stationProduct : stationProducts) {
                if (id.equals(stationProduct.getId())) {
                    product.setDescription(stationProduct.getDescription());
                    product.setColor(stationProduct.getColor());
                }
            }
        }

        productsInterface.onFetchedProducts(products);
    }

    public static void testConnection(Context context,
                                      final SimecobConnectionInterface simecobConnectionInterface) {
        String serviceBody = "<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope " +
                "SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3" +
                ".org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap" +
                ".org/soap/encoding/\"><SOAP-ENV:Body><ns7978:getStatus " +
                "xmlns:ns7978=\"http://tempuri.org\"><getStatus " +
                "xsi:type=\"xsd:string\"></getStatus></ns7978:getStatus></SOAP-ENV:Body></SOAP" +
                "-ENV:Envelope>";

        Response.Listener<String> responseListener = new
                Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        simecobConnectionInterface.onSuccess(response);
                    }
                };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                simecobConnectionInterface.onError(error.getMessage());
            }
        };

        sendSOAPRequest(context, serviceBody, responseListener, errorListener);
    }

    private static void sendSOAPRequest(Context context, final String body,
                                        Response.Listener<String>
                                                responseListener,
                                        Response.ErrorListener errorListener) {
        String emaxIp = SharedPreferencesManager.getString(context, ChipREDConstants.EMAX_URL);
        String emaxUrl = String.format(Locale.getDefault(), "http://%s/emax/services/simecob.php"
                , emaxIp);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, emaxUrl,
                responseListener, errorListener) {
            @Override
            public String getBodyContentType() {
                return "text/xml; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return body.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using" +
                            " %s", body, "utf-8");
                    return null;
                }
            }
        };

        // Obtener requestQueue
        RequestQueueSingelton.getInstance(context.getApplicationContext()).getRequestQueue();

        Log.d(TAG, "Request: " + stringRequest.toString());
        RequestQueueSingelton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public interface ProductsInterface {
        void onFetchedProducts(ArrayList<Product> products);

        void onError(String message);
    }

    public interface DispensersInterface {
        void onFetchedDispensers(ArrayList<DispIpInfo> dispsIpInfo);

        void onError(String message);
    }

    public interface SimecobConnectionInterface {
        void onSuccess(String response);

        void onError(String response);
    }
}
