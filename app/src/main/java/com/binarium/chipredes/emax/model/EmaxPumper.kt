package com.binarium.chipredes.emax.model

data class EmaxPumper(
        val id: String?,
        val nombre: String?,
        val iniciales: String?,
        val tag: String?,
)