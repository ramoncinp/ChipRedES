package com.binarium.chipredes.emax.domain.usecases

import com.binarium.chipredes.emax.EmaxApiService
import com.binarium.chipredes.emax.EmaxSaleAuthResult
import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.wolke.models.LocalClientData
import java.lang.Exception
import javax.inject.Inject

class GetEmaxAuthUseCase @Inject constructor(
    private val emaxApiService: EmaxApiService,
    private val logger: Logger,
) {

    suspend operator fun invoke(localClientData: LocalClientData): EmaxAuthResult {
        try {
            if (!emaxApiService.testConnection()) {
                return EmaxAuthResult.Error("No responde servidor EMAX")
            }
        } catch (e: Exception) {
            val message = "Error al probar conexion con EMAX"
            postError(message)
            return EmaxAuthResult.Error(message)
        }

        emaxApiService.getAuth(localClientData)?.let {
            if (it.error.isNotEmpty()) {
                with(it.error) {
                    postError(this)
                    return EmaxAuthResult.Error(this)
                }
            } else {
                // Success
                return EmaxAuthResult.Success(it)
            }
        } ?: run {
            val message = "Error al obtener autorización de emax"
            postError(message)
            return EmaxAuthResult.Error(message)
        }
    }

    private fun postError(message: String) {
        logger.postMessage(message, LogLevel.ERROR)
    }
}

sealed class EmaxAuthResult {
    data class Success(val data: EmaxSaleAuthResult) : EmaxAuthResult()
    data class Error(val message: String) : EmaxAuthResult()
}
