package com.binarium.chipredes.emax

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class EmaxParserUtils {
    companion object {
        suspend fun parseBilletResult(data: String) = withContext(Dispatchers.IO) {
            var errorMessage = ""
            var auth = ""
            try {
                val decodedJsonResponse =
                    EmaxResponseParser.parseEmaxResponse(data.byteInputStream())
                val error1: Int =
                    if (decodedJsonResponse.has("iderror")) decodedJsonResponse.getInt("iderror") else 0
                val error2: Int =
                    if (decodedJsonResponse.has("idbierror")) decodedJsonResponse.getInt("idbierror") else 0
                val error3: Int =
                    if (decodedJsonResponse.has("idsierror")) decodedJsonResponse.getInt("idsierror") else 0

                if (error1 == 0 && error2 == 0 && error3 == 0) {
                    auth = decodedJsonResponse.getString("authno")
                } else {
                    errorMessage = when {
                        decodedJsonResponse.has("msgerror") -> {
                            decodedJsonResponse.getString("msgerror")
                        }
                        decodedJsonResponse.has("msgerrorident") -> {
                            decodedJsonResponse.getString("msgerrorident")
                        }
                        else -> {
                            "Error al obtener autorización"
                        }
                    }
                }

            } catch (e: java.lang.Exception) {
                Timber.e("Error al interpretar respuesta de EMAX")
            }

            return@withContext EmaxSaleAuthResult(errorMessage, auth)
        }

        suspend fun parseRegisterDepositResult(data: String) = withContext(Dispatchers.IO) {
            var errorMessage = ""
            var responseCode = 1
            var exceptionMessage: String? = null

            try {
                val decodedJsonResponse =
                    EmaxResponseParser.parseEmaxResponse(data.byteInputStream())

                responseCode = when {
                    decodedJsonResponse.has("iderror") -> {
                        decodedJsonResponse.getInt("iderror")
                    }
                    decodedJsonResponse.has("idbierror") -> {
                        decodedJsonResponse.getInt("idbierror")
                    }
                    decodedJsonResponse.has("idsierror") -> {
                        decodedJsonResponse.getInt("idsierror")
                    }
                    else -> {
                        1
                    }
                }

                errorMessage = when {
                    decodedJsonResponse.has("msgerror") -> {
                        decodedJsonResponse.getString("msgerror")
                    }
                    decodedJsonResponse.has("msgerrorident") -> {
                        decodedJsonResponse.getString("msgerrorident")
                    }
                    else -> {
                        "Error al abonar saldo en EMAX"
                    }
                }

            } catch (e: java.lang.Exception) {
                Timber.e("Error al interpretar respuesta de EMAX")
                errorMessage = "Error al interpretar respuesta de EMAX"
                exceptionMessage = e.message
            }

            return@withContext EmaxRegisterDepositResult(
                errorMessage, responseCode, exceptionMessage
            )
        }
    }
}

class EmaxSaleAuthResult(
    val error: String,
    val authNumber: String
)

class EmaxRegisterDepositResult(
    val errorMessage: String,
    val responseCode: Int,
    val exceptionMessage: String?
)
