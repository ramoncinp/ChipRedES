package com.binarium.chipredes;

import android.content.Context;
import android.graphics.Color;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class PurchaseArrayAdapter extends RecyclerView.Adapter<PurchaseArrayAdapter
        .PurchaseViewHolder> implements View.OnClickListener
{
    private final ArrayList<Purchase> purchases;
    private Context context;
    private View.OnClickListener listener;

    public PurchaseArrayAdapter(ArrayList<Purchase> purchases, Context context)
    {
        this.purchases = purchases;
        this.context = context;
    }

    @Override
    public int getItemCount()
    {
        return purchases.size();
    }

    @Override
    public PurchaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.purchase_layout,
                viewGroup, false);

        PurchaseViewHolder pvh = new PurchaseViewHolder(v, context, purchases);
        v.setOnClickListener(this);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PurchaseViewHolder purchaseViewHolder, int i)
    {
        String country = SharedPreferencesManager.getString(context, ChipREDConstants.COUNTRY,
                "mexico");

        DecimalFormat volumeFormat = new DecimalFormat("0.000");
        final Purchase purchase = purchases.get(i);
        String amount;

        if (country.equals("mexico"))
        {
            purchaseViewHolder.currencyIcon.setBackgroundResource(R.drawable.ic_purchase);
            amount = ChipREDConstants.MX_AMOUNT_FORMAT.format(purchase.getAmount()) + " MXN";
        }
        else
        {
            purchaseViewHolder.currencyIcon.setBackgroundResource(R.drawable.ic_colon_crc_blanco);
            amount = ChipREDConstants.CR_AMOUNT_FORMAT.format(purchase.getAmount()) + " CRC";
        }

        purchaseViewHolder.amount.setText(amount);
        purchaseViewHolder.volume.setText(volumeFormat.format(purchase.getVolume()) + " L");

        String dateHour = purchase.getDateHour();
        int token = dateHour.indexOf(' ');

        purchaseViewHolder.time.setText(dateHour.substring(token + 1));

        purchaseViewHolder.date.setText(dateHour.substring(0, token));

        ((CardView) purchaseViewHolder.itemView).setCardBackgroundColor(Color.parseColor(purchase
                .getProductColor()));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    public static class PurchaseViewHolder extends RecyclerView.ViewHolder
    {
        private TextView amount;
        private TextView volume;
        private TextView date;
        private TextView time;
        private ImageView currencyIcon;

        public PurchaseViewHolder(View itemView, Context context, ArrayList<Purchase> purchases)
        {
            super(itemView);

            amount = (TextView) itemView.findViewById(R.id.purchase_amount);
            date = (TextView) itemView.findViewById(R.id.purchase_date);
            time = (TextView) itemView.findViewById(R.id.purchase_time);
            volume = (TextView) itemView.findViewById(R.id.purchase_volume);
            currencyIcon = itemView.findViewById(R.id.purchase_currency_icon);
        }
    }
}

