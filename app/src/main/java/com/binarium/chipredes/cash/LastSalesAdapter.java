package com.binarium.chipredes.cash;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class LastSalesAdapter extends RecyclerView.Adapter<LastSalesAdapter.LastSaleViewHolder> implements View.OnClickListener
{
    private final ArrayList<JSONObject> lastSales;
    private Context context;
    private View.OnClickListener listener;
    private View.OnClickListener printButtonListener;

    public LastSalesAdapter(ArrayList<JSONObject> lastSales, Context context)
    {
        this.lastSales = lastSales;
        this.context = context;
    }

    @Override
    public int getItemCount()
    {
        return lastSales.size();
    }

    @Override
    public LastSaleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                .pending_sale_layout, viewGroup, false);

        LastSaleViewHolder lastSaleViewHolder = new LastSaleViewHolder(v);
        v.setOnClickListener(this);

        return lastSaleViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull LastSaleViewHolder lastSaleViewHolder, int i)
    {
        //Obtener país
        String country = SharedPreferencesManager.getString(context, ChipREDConstants.COUNTRY,
                "mexico");

        //Definir formato par cantidades
        DecimalFormat volumeFormat = new DecimalFormat("0.000");

        //Obtener objeto JSON
        JSONObject lastSale = lastSales.get(i);

        //Obtener valores representativos de la venta
        double amount = 0;
        double volume = 0;
        String dateHour = "";
        String productDesc = "";
        String idCliente = "";
        String clientEmail = "";
        String taxReceiptCode = "";

        try
        {
            if (lastSale.has("costo")) amount = lastSale.getDouble("costo");
            if (lastSale.has("cantidad")) volume = lastSale.getDouble("cantidad");
            if (lastSale.has("fecha_hora")) dateHour = lastSale.getString("fecha_hora");
            if (lastSale.has("combustible")) productDesc = lastSale.getString("combustible");
            if (lastSale.has("id_cliente")) idCliente = lastSale.getString("id_cliente");

            //Obtener datos extra
            if (lastSale.has("comprobante"))
            {
                JSONArray taxReceiptArray = lastSale.getJSONArray("comprobante");
                if (taxReceiptArray.length() != 0)
                {
                    try
                    {
                        taxReceiptCode = taxReceiptArray.getJSONObject(0).getString("tipo");
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }

            if (lastSale.has("cliente"))
            {
                JSONArray clientArray = lastSale.getJSONArray("cliente");
                if (clientArray.length() != 0)
                {
                    try
                    {
                        clientEmail = clientArray.getJSONObject(0).getString("email");
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        //Mostrar importe
        if (country.equals("mexico"))
        {
            lastSaleViewHolder.amount.setText(ChipREDConstants.MX_AMOUNT_FORMAT.format(amount) +
                    " MXN");
        }
        else
        {
            lastSaleViewHolder.amount.setText(ChipREDConstants.CR_AMOUNT_FORMAT.format(amount) +
                    " CRC");
        }

        //Mostrar cantidad de producto
        lastSaleViewHolder.volume.setText(volumeFormat.format(volume) + " L");

        //Cambiar formato a la fecha si se puede
        DateFormat dateFormatoSalida = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss", Locale.US);
        Date saleDate = ChipREDConstants.getDateFromJSON(lastSale);
        if (saleDate != null) dateHour = dateFormatoSalida.format(saleDate);

        //Mostrar datos de la fecha
        int token = dateHour.indexOf('T');
        if (token != -1)
        {
            lastSaleViewHolder.time.setText(dateHour.substring(token + 1));

            //Obtener la fecha del consumo
            if (saleDate != null)
            {
                //Obtener la fecha de hoy como referencia
                Calendar today = Calendar.getInstance();
                Calendar saleDay = Calendar.getInstance();
                saleDay.setTime(saleDate);

                //Comparar días
                int diff = today.get(Calendar.DAY_OF_YEAR) - saleDay.get(Calendar.DAY_OF_YEAR);

                switch (diff)
                {
                    case 0:
                        lastSaleViewHolder.date.setText("Hoy");
                        break;

                    case 1:
                        lastSaleViewHolder.date.setText("Ayer");
                        break;

                    case 2:
                        lastSaleViewHolder.date.setText("Hace 2 días");
                        break;

                    case 3:
                        lastSaleViewHolder.date.setText("Hace 3 días");
                        break;

                    default:
                        lastSaleViewHolder.date.setText(dateHour.substring(0, token));
                        break;
                }
            }
            else
            {
                lastSaleViewHolder.date.setText(dateHour.substring(0, token));
            }
        }

        //Mostrar nombre de producto
        lastSaleViewHolder.product.setText(productDesc);

        //Evaluar id de cliente
        if (!idCliente.equals("") && taxReceiptCode.equals("01"))
        {
            //Indicar las ventas ya facturadas
            ((CardView) lastSaleViewHolder.itemView).setCardBackgroundColor(ContextCompat
                    .getColor(context, R.color.green_money));

            //Mostrar views de datos extra
            lastSaleViewHolder.extras.setVisibility(View.VISIBLE);
            //Mostrar views de cliente
            lastSaleViewHolder.clientEmail.setVisibility(View.VISIBLE);
            lastSaleViewHolder.clientIcon.setVisibility(View.VISIBLE);

            //Mostrar views de datos extra
            lastSaleViewHolder.extras.setVisibility(View.VISIBLE);
            lastSaleViewHolder.clientEmail.setText(clientEmail);
            lastSaleViewHolder.taxReceiptTypeText.setText("Factura Electrónica");
            lastSaleViewHolder.taxReceiptTypeIcon.setImageResource(R.drawable.ic_invoice);
        }
        else
        {
            //Ver si fue un tiquete electrónico
            if (taxReceiptCode.equals("04"))
            {
                //Indicar las venta con tiquete
                ((CardView) lastSaleViewHolder.itemView).setCardBackgroundColor(ContextCompat
                        .getColor(context, R.color.green_money));

                //Mostrar views de datos extra
                lastSaleViewHolder.extras.setVisibility(View.VISIBLE);
                lastSaleViewHolder.taxReceiptTypeText.setText("Tiquete Electrónico");
                lastSaleViewHolder.taxReceiptTypeIcon.setImageResource(R.drawable.ic_receipt);

                //Ocultar views de cliente
                lastSaleViewHolder.clientEmail.setVisibility(View.GONE);
                lastSaleViewHolder.clientIcon.setVisibility(View.GONE);
            }
            else
            {
                //Indicar las ventas no facturadas
                ((CardView) lastSaleViewHolder.itemView).setCardBackgroundColor(ContextCompat
                        .getColor(context, R.color.colorAccent));

                lastSaleViewHolder.extras.setVisibility(View.GONE);
            }
        }

        //Definirle
        lastSaleViewHolder.printButton.setTag(i);
        lastSaleViewHolder.printButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (printButtonListener != null)
                {
                    printButtonListener.onClick(v);
                }
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    public void setPrintButtonListener(View.OnClickListener printButtonListener)
    {
        this.printButtonListener = printButtonListener;
    }

    static class LastSaleViewHolder extends RecyclerView.ViewHolder
    {
        private TextView amount;
        private TextView volume;
        private TextView date;
        private TextView time;
        private TextView product;

        private ConstraintLayout extras;
        private TextView clientEmail;
        private TextView taxReceiptTypeText;
        private ImageView taxReceiptTypeIcon;
        private ImageView clientIcon;
        private CardView printButton;

        LastSaleViewHolder(View itemView)
        {
            super(itemView);

            amount = itemView.findViewById(R.id.pending_sale_amount);
            date = itemView.findViewById(R.id.pending_sale_date);
            time = itemView.findViewById(R.id.pending_sale_time);
            product = itemView.findViewById(R.id.pending_sale_product);
            volume = itemView.findViewById(R.id.pending_sale_volume);
            extras = itemView.findViewById(R.id.normal_sale_extras);
            clientEmail = itemView.findViewById(R.id.sale_client);
            clientIcon = itemView.findViewById(R.id.sale_client_icon);
            taxReceiptTypeText = itemView.findViewById(R.id.tax_receipt_type);
            taxReceiptTypeIcon = itemView.findViewById(R.id.tax_receipt_type_icon);
            printButton = itemView.findViewById(R.id.print_button);
        }
    }
}

