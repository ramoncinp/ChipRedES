package com.binarium.chipredes.cash.models

import com.squareup.moshi.Json

data class LastSale(
    val id: String,
    @Json(name = "fecha_hora") val dateTime: String,
    @Json(name = "num_ticket") val ticketNumber: String,
    @Json(name = "cantidad") val volume: Double,
    @Json(name = "precio_unitario") val unitaryPrice: Double,
    @Json(name = "id_despachador_emax") val pumperId: String,
    @Json(name = "posicion_carga") val loadingPosition: String,
    @Json(name = "numero_dispensario") val dispenser: String,
    @Json(name = "costo") val amount: Double,
    @Json(name = "combustible") val fuel: String,
    @Json(name = "producto") val productId: String
)
