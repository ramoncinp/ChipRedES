package com.binarium.chipredes.cash;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cursoradapter.widget.CursorAdapter;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.SearchView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.AddClient;
import com.binarium.chipredes.ChipREDClient;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.barcode.QRScannerActivity;
import com.binarium.chipredes.R;
import com.binarium.chipredes.Sale;
import com.binarium.chipredes.Vehicle;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AssignClientFragment extends Fragment
{
    private static final String TAG = "AssignClientFragment";
    private static final String[] filterOptions = {"Por cédula", "Por email", "Por nombre"};

    private Activity activity;
    private ChipRedManager chipRedManager;
    private ChipREDClient chipREDClient;
    private Vehicle vehicle;
    private AssignClientInterface assignClientInterface;

    private Button assignClientButton;
    private SearchView searchView;
    private ProgressBar searchingClient;
    private ProgressDialog progressDialog;
    private ScrollView scrollView;

    //Views y datos de vehículo
    private MaterialAutoCompleteTextView platesTv;
    private MaterialEditText carNumberTv;
    private MaterialEditText odometerTv;
    private MaterialEditText marchamoTv;
    private MaterialEditText newOdometerTv;
    private MaterialEditText newMarchamoTv;

    private ArrayList<JSONObject> suggestionValues = new ArrayList<>();
    private ArrayList<JSONObject> platesValues = new ArrayList<>();
    private ArrayList<String> ccEmailList = new ArrayList<>();
    private ArrayList<String> platesSuggestions = new ArrayList<>();
    private CursorAdapter simpleCursorAdapter;

    private boolean showedInfo = false, showedVehicleInfo = false, selectedPlate = false;
    private boolean byQr = false, byId = false, platesByQr = false, invalidPlates = false;
    private int selectedFilterIdx = ChipRedManager.BYTAXCODE;
    private String clientCountry, carClientId, selectedPlates, selectedMainEmail;

    private Menu menu;

    public AssignClientFragment()
    {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_assign_client, container, false);
        scrollView = rootView.findViewById(R.id.assign_client_scroll_view);
        searchView = rootView.findViewById(R.id.sv_cliente_efectivo);
        searchingClient = rootView.findViewById(R.id.buscando_cliente_efectivo);
        assignClientButton = rootView.findViewById(R.id.assign_client_button);
        assignClientButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Esconder teclado
                assignClientInterface.hideKeyboardActivity();

                //Calcular rendimiento
                double vehiclePerformance = computeVehiclePerformance();

                //Validar datos de vehículo
                if (vehiclePerformance < 0)
                {
                    String message =
                            "Verificar kilometraje capturado";

                    GenericDialog genericDialog = new GenericDialog("Aviso", message, new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (confirmClientData()) confirmSelectedClient();
                        }
                    }, new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            //Nada
                        }
                    }, getActivity());

                    genericDialog.setPositiveText("Omitir");
                    genericDialog.setNegativeText("Regresar");
                    genericDialog.show();
                }
                else
                {
                    if (confirmClientData()) confirmSelectedClient();
                }
            }
        });

        CardView selectFilter = rootView.findViewById(R.id.select_filter_button);
        selectFilter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                searchClientFilterDialog();
            }
        });

        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();

        //Inicializar edit text para sugerencias de clientes
        setSearchViewAdapter(false);
        //Inicializar edit text para sugerencias de placas
        initVehicleForm();

        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        AssignClientFragment.this))
                    return;

                //Indicar que ya no esta esperando respuesta
                searchingClient.setVisibility(View.INVISIBLE);

                //Mostrar diálogo de error
                if (webServiceN == ChipRedManager.SEARCH_PLATES && byQr)
                {
                    if (progressDialog != null)
                    {
                        if (progressDialog.isShowing())
                        {
                            progressDialog.dismiss();
                        }
                    }

                    if (platesByQr)
                    {
                        GenericDialog genericDialog = new GenericDialog("Error", "No hay un " +
                                "cliente asignado al vehículo buscado", new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                //Nada
                            }
                        }, null, getActivity());
                        genericDialog.setPositiveText("Ok");
                        genericDialog.show();

                        platesByQr = false;
                    }

                    //Remover focus de editText
                    platesTv.clearFocus();
                    selectedPlate = false;
                    byQr = false;
                }

                Log.d("ChipRedManager", crMessage);
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        AssignClientFragment.this))
                    return;

                //Indicar que ya no esta esperando respuesta
                searchingClient.setVisibility(View.INVISIBLE);
                Log.d("ChipRedManager", errorMessage);
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        AssignClientFragment.this))
                    return;

                //Indicar que ya no esta esperando respuesta
                searchingClient.setVisibility(View.INVISIBLE);

                if (webServiceN == ChipRedManager.SEARCH_PLATES)
                {
                    if (byQr)
                    {
                        //Obtener datos que vienen al buscar placas
                        //Obtener dato de cliente
                        byQr = false;
                        try
                        {
                            Log.d("ChipREDManager", response.toString(2));

                            //Obtener cliente
                            response = response.getJSONArray("data").getJSONObject(0);

                            if (platesByQr)
                            {
                                getSelectedClientInfo(response.getJSONObject("cliente"));
                                platesByQr = false;
                            }

                            //Evaluar id de cliente
                            if (!evaluateCarClientId(response.getJSONObject("cliente").getString(
                                    "id_cliente"))) return;

                            //Obtener datos de consumo
                            getVehicleInfo(response.getJSONObject("ultimo_consumo").getJSONObject(
                                    "vehiculo"));

                            searchView.clearFocus();
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                        //No hacer que se tenga un adapter al ingresar query en searchView
                        setSearchViewAdapter(true);
                    }
                    else
                    {
                        showPlatesSuggestions(response);
                    }
                }
                else if (byId)
                {
                    try
                    {
                        //Obtener información del cliente seleccionado
                        getSelectedClientInfo(response.getJSONArray("data").getJSONObject(0));
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                    //Quitar focus de SearchView
                    searchView.clearFocus();

                    //Reiniciar bandera
                    byId = false;
                }
                else
                {
                    //DefinirAdapter para SearchView
                    setSearchViewAdapter(false);
                    //Interpretar respuesta y mostrar lista de coincidencias
                    showGottenClients(response);
                    try
                    {
                        Log.d(chipRedManager.TAG, "Clientes encontrados -> \n" + response.toString
                                (2));
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }, activity);

        setReadQrListener();

        //Asignar query text listener
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String s)
            {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s)
            {
                if (s.isEmpty()) return false;
                Log.d(TAG, "Query has changed: " + s);

                //Indicar que se esta buscando al cliente
                searchingClient.setVisibility(View.VISIBLE);
                //Enviar request para buscar lo indicado en el filtro
                if (selectedFilterIdx == ChipRedManager.BYPLATES)
                {
                    chipRedManager.searchPlates(selectedFilterIdx, s);
                }
                else
                {
                    chipRedManager.searchClient(selectedFilterIdx, s);
                }
                Log.d("ChipRedManager", "Buscar cliente -> " + s);
                return true;
            }
        });
        searchView.setQueryHint(getActivity().getResources().getString(R.string.search_view_cliente_cedula));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_assign_client, menu);
        this.menu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.add_account_menu) {//Intent hacia la actividad de agregar cliente
            Intent intent = new Intent(getActivity(), AddClient.class);
            intent.putExtra("activityAction", 1);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void searchClientFilterDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R
                .layout.simple_list_item_1);
        arrayAdapter.add(filterOptions[0]);
        arrayAdapter.add(filterOptions[1]);
        arrayAdapter.add(filterOptions[2]);

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String selectedOption = arrayAdapter.getItem(which);

                if (selectedOption != null)
                {
                    selectFilterOption(selectedOption);
                }
                else
                {
                    Toast.makeText(activity, "Error al elegir el filtro", Toast.LENGTH_SHORT)
                            .show();
                }
                dialog.dismiss();
            }
        });

        builder.setTitle("Filtro de búsqueda");

        Dialog dialog = builder.create();
        dialog.show();
    }

    private void selectFilterOption(String itemName)
    {
        searchView.setQuery("", false);
        if (itemName.equals(filterOptions[0]))
        {
            searchView.setQueryHint("Buscar cliente (cédula)");
            selectedFilterIdx = ChipRedManager.BYTAXCODE;

            //Cambiar inputType
            searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        else if (itemName.equals(filterOptions[1]))
        {
            searchView.setQueryHint("Buscar cliente (email)");
            selectedFilterIdx = ChipRedManager.BYEMAIL;

            //Cambiar inputType
            searchView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        }
        else if (itemName.equals(filterOptions[2]))
        {
            searchView.setQueryHint("Buscar cliente (nombre)");
            selectedFilterIdx = ChipRedManager.BYNAME;

            //Cambiar inputType
            searchView.setInputType(InputType.TYPE_CLASS_TEXT);
        }
    }

    private void setSearchViewAdapter(boolean forQr)
    {
        if (!forQr)
        {
            //Adaptador para las suggestions
            simpleCursorAdapter = new SimpleCursorAdapter(activity, android.R.layout
                    .simple_list_item_1, null, new String[]{"nombreCliente"},
                    new int[]{android.R.id.text1}, 0);

            searchView.setSuggestionsAdapter(simpleCursorAdapter);
            searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener()
            {
                @Override
                public boolean onSuggestionSelect(int position)
                {
                    return false;
                }

                @Override
                public boolean onSuggestionClick(int position)
                {
                    JSONObject client = suggestionValues.get(position);

                    if (selectedFilterIdx == ChipRedManager.BYPLATES)
                    {
                        try
                        {
                            //Si se hizo la busqueda por placas
                            byQr = true;
                            chipRedManager.searchPlates(ChipRedManager.BYID, client.getString("id"
                            ));
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        getSelectedClientInfo(client);
                        //No tiene vehículos
                        getVehicleInfo(null);
                    }

                    searchView.clearFocus();

                    return false;
                }
            });
        }
        else
        {
            searchView.setSuggestionsAdapter(null);
        }
    }

    private void comparePlates()
    {
        //Obtener placa
        String foundPlate;

        try
        {
            for (int i = 0; i < platesSuggestions.size(); i++)
            {
                //Obtener la placa del objeto encontrado
                foundPlate = platesValues.get(i).getString("placas");

                //Si es igual, indicar que ya existe y salir
                if (foundPlate.equals(platesTv.getText().toString()))
                {
                    GenericDialog dialog = new GenericDialog("Asignar vehículo", "Las placas ya " +
                            "están registradas", new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            //Nada
                        }
                    }, null, getActivity());

                    dialog.setPositiveText("Ok");
                    dialog.show();

                    platesTv.setError("Las placas ya están registradas");
                    invalidPlates = true;

                    return;
                }
            }
        }
        catch (JSONException | NullPointerException e)
        {
            e.printStackTrace();
        }

        invalidPlates = false;
    }

    private boolean getSelectedClientInfo(JSONObject client)
    {
        //Crear objeto y enviarlo a la actividad principal
        ChipREDClient chipREDClient = new ChipREDClient(client);

        //Asignar cliente CR
        this.chipREDClient = chipREDClient;

        //Obtener id de cliente para comparar posteriormente
        carClientId = chipREDClient.getIdCliente();

        //Obtener layout para mostrar info de cliente
        LinearLayout clientLinearLayout = activity.findViewById(R.id.client_info_layout);

        //Analizar si ya existe un view
        if (clientLinearLayout.getChildCount() != 0)
        {
            clientLinearLayout.removeAllViews();
        }

        //Obtener el view de su archivo y cargarlo al LinearLayout padre
        View clientInfo;
        clientInfo = getLayoutInflater().inflate(R.layout.assign_client_info, null);
        clientLinearLayout.addView(clientInfo);

        //Mostrar nombre del cliente seleccionado en el searchView
        Log.d(TAG, "Cliente seleccionado: " + client.toString());

        if (selectedFilterIdx != ChipRedManager.BYPLATES)
            searchView.setQuery(chipREDClient.getNombre(), false);

        TextView nombreTv = activity.findViewById(R.id.nombre_cliente_efectivo);
        TextView correoTv = activity.findViewById(R.id.correo_cliente_efectivo);
        TextView taxCodeTv = activity.findViewById(R.id.clave_fiscal_cliente_efectivo);
        TextView paisTv = activity.findViewById(R.id.pais_cliente_efectivo);
        ImageView paisImage = activity.findViewById(R.id.bandera_pais_cliente_efectivo);

        nombreTv.setText(chipREDClient.getNombre().trim());
        correoTv.setText(chipREDClient.getEmail());
        taxCodeTv.setText(chipREDClient.getClaveFiscal());
        paisTv.setText(chipREDClient.getCountry().toUpperCase());
        setFlagToImageView(paisImage, chipREDClient.getCountry());
        clientCountry = chipREDClient.getCountry();

        TextView claveFiscalDesc = activity.findViewById(R.id.clave_fiscal_cliente_efectivo_desc);
        if (clientCountry.equals("mexico"))
        {
            claveFiscalDesc.setText("RFC");
        }
        else
        {
            claveFiscalDesc.setText("Cédula");
        }

        CardView cuentaEfectivo = activity.findViewById(R.id.cuenta_efectivo);
        cuentaEfectivo.setVisibility(View.VISIBLE);

        if (!showedInfo)
        {
            cuentaEfectivo.setAlpha(0.0f);
            cuentaEfectivo.animate().alpha(1.0f).setListener(null);

            showedInfo = true;
        }

        //Mostrar boton
        assignClientButton.setVisibility(View.VISIBLE);
        scrollToEnd();

        return true;
    }

    private void getVehicleInfo(JSONObject jsonVehicle)
    {
        //Obtener cardView y mostrarla
        CardView saleCondition = activity.findViewById(R.id.sale_condition_cv);
        saleCondition.setVisibility(View.VISIBLE);

        //Obtener cardView y mostrarla
        CardView vehicleCv = activity.findViewById(R.id.vehiculo_efectivo_cv);
        vehicleCv.setVisibility(View.VISIBLE);

        //Obtener datos anteriores...
        String plates = "";
        String carNumber = "";
        String firstKm = "";
        String fistMarchamo = "";

        //Obtener texto que indica lo sucedido
        TextView noSpecifiedText = getActivity().findViewById(R.id.no_vehicle_specified_tv);
        noSpecifiedText.setVisibility(View.GONE);

        //Ocultar formulario
        LinearLayout form;
        form = getActivity().findViewById(R.id.vehicle_form_layout);
        form.setVisibility(View.VISIBLE);

        if (jsonVehicle != null)
        {
            //Mostrar datos
            try
            {
                plates = jsonVehicle.getString("placas");
                carNumber = jsonVehicle.getString("numero_economico");
                firstKm = jsonVehicle.getString("km_actual");
                fistMarchamo = jsonVehicle.getString("marchamo_actual");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            newOdometerTv.setVisibility(View.VISIBLE);
            newMarchamoTv.setVisibility(View.VISIBLE);
        }
        else
        {
            //Nueva captura
            newOdometerTv.setVisibility(View.GONE);
            newMarchamoTv.setVisibility(View.GONE);
            odometerTv.setVisibility(View.VISIBLE);
            marchamoTv.setVisibility(View.VISIBLE);
        }

        platesTv.setText(plates);
        carNumberTv.setText(carNumber);
        odometerTv.setText(firstKm);
        marchamoTv.setText(fistMarchamo);

        if (!showedVehicleInfo)
        {
            vehicleCv.setAlpha(0.0f);
            vehicleCv.animate().alpha(1.0f).setListener(null);

            showedVehicleInfo = true;
        }

        //Remover focus de editText
        platesTv.clearFocus();
        selectedPlate = false;

        if (progressDialog != null)
            progressDialog.dismiss();
    }

    private void setFlagToImageView(ImageView imageView, String country)
    {
        if (country.equals("mexico"))
        {
            imageView.setImageResource(R.drawable.ic_mexico);
        }
        else if (country.equals("costa rica"))
        {
            imageView.setImageResource(R.drawable.ic_costa_rica);
        }
    }

    private void setReadQrListener()
    {
        CardView readClientQr = activity.findViewById(R.id.read_client_qr);
        readClientQr.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Esconder teclado
                assignClientInterface.hideKeyboardActivity();
                //Iniciar actividad para leer QR
                startActivityForResult(new Intent(activity, QRScannerActivity.class),
                        ChipREDConstants.READ_QR_REQUEST);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ChipREDConstants.READ_QR_REQUEST)
        {
            try
            {
                //Usar el código
                if (data != null)
                {
                    byQr = true;
                    platesByQr = true;
                    selectedPlate = true;

                    String qrCode = data.getData().toString();
                    chipRedManager.searchPlates(ChipRedManager.BYID, qrCode);

                    if (progressDialog == null)
                        progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("");
                    progressDialog.show();

                    Log.d("SearchPlates", "Read QR: " + qrCode);
                }
            }
            catch (NullPointerException e)
            {
                e.printStackTrace();
                Toast.makeText(activity, "Error al obtener información del código QR", Toast
                        .LENGTH_SHORT).show();
            }
        }
        else if (requestCode == ChipREDConstants.COMPLETE_CLIENT_SIGNUP)
        {
            if (resultCode == 0)
            {
                //Activar bandera para que muestre la info del cliente
                byId = true;
                //Refrescar datos de cliente
                chipRedManager.searchClient(ChipRedManager.BYID, chipREDClient.getIdCliente());
            }
        }
    }

    private void showGottenClients(JSONObject response)
    {
        String suggestionString;
        try
        {
            //Crear suggestions de los clientes encontrados
            final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "nombreCliente"});

            //Obtener lista de elementos sugeridos
            JSONArray array = response.getJSONArray("data");
            suggestionValues.clear();

            for (int i = 0; i < array.length(); i++)
            {
                JSONObject element = array.getJSONObject(i);
                suggestionString = element.getString("nombre").trim();
                if (selectedFilterIdx == ChipRedManager.BYEMAIL)
                {
                    if (element.has("email"))
                    {
                        suggestionString += " - ";
                        suggestionString += element.getString("email");
                    }
                }
                else if (selectedFilterIdx == ChipRedManager.BYTAXCODE)
                {
                    suggestionString += " - ";
                    suggestionString += element.getString("clave_fiscal");
                }

                //Añadir objeto JSON
                suggestionValues.add(element);
                c.addRow(new Object[]{i, suggestionString});
            }

            simpleCursorAdapter.swapCursor(c);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public void confirmSelectedClient()
    {
        //Crear instancia de builder para dialogo
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        //Validar si existe mas de un correo para enviar los archivos
        if (chipREDClient.getContacts().size() > 0)
        {
            //Mostrar diálogo para seleccionar correos electrónicos
            selectEmailsDialog(builder);
        }
        else
        {
            //Obtener view de xml
            View content = getLayoutInflater().inflate(R.layout.dialog_confirm_client, null);
            TextView emailToAssign = content.findViewById(R.id.email_to_assign);

            //Definir view en dialogo
            builder.setView(content);
            builder.setTitle("Confirmar cliente");
            emailToAssign.setText(chipREDClient.getEmail());

            builder.setPositiveButton("Asignar", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    assignClientInterface.assignVehicle(vehicle);
                    assignClientInterface.assignClient(chipREDClient);
                    assignClientInterface.assignSaleCondition(getSaleCondition());
                }
            });

            builder.setNegativeButton("Regresar", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                }
            });

            Dialog dialog;
            builder.setView(content);
            dialog = builder.create();
            dialog.setCancelable(true);
            dialog.show();
        }
    }

    private void selectEmailsDialog(AlertDialog.Builder builder)
    {
        //Resetear variables
        selectedMainEmail = "";
        ccEmailList.clear();

        //Obtener view de xml
        View content = getLayoutInflater().inflate(R.layout.dialog_select_client_emails, null);

        //Referenciar views
        final LinearLayout selectMainLayout = content.findViewById(R.id.select_main_email_layout);
        final LinearLayout selectCCLayout = content.findViewById(R.id.select_cc_list_emails_layout);
        final RadioGroup radioGroup = content.findViewById(R.id.radio_button_group);
        Button submitButton = content.findViewById(R.id.submit_button);
        Button cancelButton = content.findViewById(R.id.start_button);

        //Definir parámetros de views
        final int textSize =
                getActivity().getResources().getInteger(R.integer.normal_screen_text_size);

        //Obtener medida de márgenes en dp's
        float d = getActivity().getResources().getDisplayMetrics().density;
        int marginInDp = (int) (8 * d);

        //Definir margenes para Layouts
        final LinearLayout.LayoutParams llMargins =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        llMargins.setMargins(0, 0, 0, marginInDp);

        RadioGroup.LayoutParams rgMargins =
                new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
        rgMargins.setMargins(0, 0, 0, marginInDp);

        //Agregar views a radioGroup
        //Correo principal
        final RadioButton mainEmail = new RadioButton(getActivity());
        mainEmail.setText(chipREDClient.getEmail());
        mainEmail.setId((int) 100);
        mainEmail.setChecked(true);
        mainEmail.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        mainEmail.setLayoutParams(rgMargins);
        radioGroup.addView(mainEmail);

        //Correos de lista de difusión
        for (int i = 0; i < chipREDClient.getContacts().size(); i++)
        {
            RadioButton alternativeMail = new RadioButton(getActivity());
            alternativeMail.setText(chipREDClient.getContacts().get(i));
            alternativeMail.setId(i + 101);
            alternativeMail.setChecked(false);
            alternativeMail.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
            alternativeMail.setLayoutParams(rgMargins);
            radioGroup.addView(alternativeMail);
        }

        //Definir views
        builder.setView(content);

        //Crear Diálogo
        final Dialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        //Definir listeners
        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (selectedMainEmail.isEmpty())
                {
                    //Buscar el correo seleccionado
                    int checkedRadioBtnId = radioGroup.getCheckedRadioButtonId();
                    View radioButton = radioGroup.findViewById(checkedRadioBtnId);
                    int idx = radioGroup.indexOfChild(radioButton);

                    RadioButton r = (RadioButton) radioGroup.getChildAt(idx);
                    selectedMainEmail = r.getText().toString();

                    //Agregar correos restantes para seleccionar lista de difusión
                    if (!selectedMainEmail.equals(chipREDClient.getEmail()))
                    {
                        CheckBox mainMailCb = new CheckBox(getActivity());
                        mainMailCb.setText(chipREDClient.getEmail());
                        mainMailCb.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                        mainMailCb.setLayoutParams(llMargins);
                        selectCCLayout.addView(mainMailCb);
                    }

                    for (int i = 0; i < chipREDClient.getContacts().size(); i++)
                    {
                        String contactEmail = chipREDClient.getContacts().get(i);
                        if (!selectedMainEmail.equals(contactEmail))
                        {
                            CheckBox mail = new CheckBox(getActivity());
                            mail.setText(contactEmail);
                            mail.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                            mail.setLayoutParams(llMargins);
                            selectCCLayout.addView(mail);
                        }
                    }

                    //Mostrar layout
                    selectMainLayout.setVisibility(View.GONE);
                    selectCCLayout.setVisibility(View.VISIBLE);
                }
                else
                {
                    //Obtener correos seleccionados
                    for (int i = 1; i < selectCCLayout.getChildCount(); i++)
                    {
                        CheckBox mail = (CheckBox) selectCCLayout.getChildAt(i);
                        if (mail.isChecked())
                        {
                            ccEmailList.add(mail.getText().toString());
                        }
                    }

                    //Definir variables
                    chipREDClient.setMainEmail(selectedMainEmail);
                    chipREDClient.setSelectedEmails(ccEmailList);
                    assignClientInterface.assignVehicle(vehicle);
                    assignClientInterface.assignClient(chipREDClient);
                    assignClientInterface.assignSaleCondition(getSaleCondition());

                    dialog.dismiss();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
    }

    private boolean confirmClientData()
    {
        if (chipREDClient == null)
        {
            ChipREDConstants.showSnackBarMessage("No hay un cliente seleccionado", getActivity());
            return false;
        }

        //Validar placas
        if (invalidPlates)
        {
            return false;
        }


        if (chipREDClient.getEmail().isEmpty())
        {
            //Armar el diálogo
            GenericDialog genericDialog = new GenericDialog("Asignar Venta", "No existe un " +
                    "correo" + " electrónico para este cliente, favor de asignar " + "uno", new
                    Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Intent intent = new Intent(getActivity(), AddClient.class);
                            intent.putExtra("activityAction", 3);
                            intent.putExtra(ChipREDConstants.CLIENT_TO_MODIFY,
                                    chipREDClient.getIdCliente
                                            ());
                            intent.setFlags(0);
                            startActivityForResult(intent, ChipREDConstants.COMPLETE_CLIENT_SIGNUP);
                        }
                    }, new Runnable()
            {
                @Override
                public void run()
                {
                    //nada :)
                }
            }, activity);

            genericDialog.setPositiveText("Asignar");
            genericDialog.setNegativeText("No ahora");
            genericDialog.show();
            return false;
        }
        else if (chipREDClient.getStatus().equals("PE") && !clientCountry.equals("costa rica"))
        {
            String message = "";
            message += "Favor de confirmar el correo electrónico de la cuenta:";
            message += "\n\t\t";
            message += chipREDClient.getEmail();

            //Armar el diálogo
            GenericDialog genericDialog = new GenericDialog("Asignar Venta", message, new Runnable()
            {
                @Override
                public void run()
                {

                }
            }, null, activity);

            genericDialog.setPositiveText("OK");
            genericDialog.show();
            return false;
        }
        else
        {
            return true;
        }
    }

    public void hideCreateClientMenuItem()
    {
        try
        {
            if (menu != null)
            {
                menu.getItem(0).setVisible(false);
            }
        }
        catch (IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }
    }

    public void showClientMenuItem()
    {
        try
        {
            if (menu != null)
            {
                menu.getItem(0).setVisible(true);
            }
        }
        catch (IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }
    }

    private double computeVehiclePerformance()
    {
        boolean allEmpty = true;
        String plates = "";
        String carNumber = "";
        String firstMarchamo = "";
        String firstKm = "";
        String odometer = "";
        String marchamo = "";

        if (platesTv.getText() != null)
        {
            plates = platesTv.getText().toString();
            allEmpty = (plates.length() == 0);
        }

        if (carNumberTv.getText() != null)
        {
            carNumber = carNumberTv.getText().toString();
            allEmpty &= (carNumber.length() == 0);
        }

        if (odometerTv.getText() != null)
        {
            firstKm = odometerTv.getText().toString();
            allEmpty &= (firstKm.length() == 0);
        }

        if (marchamoTv.getText() != null)
        {
            firstMarchamo = marchamoTv.getText().toString();
            allEmpty &= (firstMarchamo.length() == 0);
        }

        if (newOdometerTv.getText() != null)
        {
            odometer = newOdometerTv.getText().toString();
            allEmpty &= (odometer.length() == 0);
        }

        if (newMarchamoTv.getText() != null)
        {
            marchamo = newMarchamoTv.getText().toString();
            allEmpty &= (marchamo.length() == 0);
        }

        if (!allEmpty)
        {
            //Crear vehículo
            vehicle = new Vehicle();
            vehicle.setPlate(plates);
            vehicle.setCarNumber(carNumber);

            //Considerar el casode primera captura
            if (newOdometerTv.getVisibility() == View.GONE && newMarchamoTv.getVisibility() == View.GONE)
            {
                vehicle.setOdometer(firstKm);
                vehicle.setMarchamo(firstMarchamo);
                vehicle.setLastMarchamo(marchamo);
                vehicle.setLastOdometer(odometer);
            }
            else
            {
                //Flujo normal
                vehicle.setLastMarchamo(firstMarchamo);
                vehicle.setLastOdometer(firstKm);
                vehicle.setOdometer(odometer);
                vehicle.setMarchamo(marchamo);
            }
        }
        else
        {
            return 0;
        }

        //Obtener datos de la venta
        double performanceValue = 0;
        Sale sale = assignClientInterface.getSale();
        if (sale != null)
        {
            performanceValue = vehicle.getPerformanceValue(sale.getCantidad());
        }

        return performanceValue;
    }

    private String getSaleCondition()
    {
        RadioButton credito = getActivity().findViewById(R.id.pay_condition_credito);
        RadioButton contado = getActivity().findViewById(R.id.pay_condition_contado);

        if (credito.isChecked())
        {
            return "02";
        }
        else if (contado.isChecked())
        {
            return "01";
        }

        return "01"; //Contado por default
    }

    private void scrollToEnd()
    {
        scrollView.post(new Runnable()
        {
            @Override
            public void run()
            {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void initVehicleForm()
    {
        //Obtener layout para mostrar formulario de datos de vehículo
        LinearLayout vehicleLinearLayout = activity.findViewById(R.id.vehicle_info_layout);

        //Analizar si ya existe un view
        if (vehicleLinearLayout.getChildCount() != 0)
        {
            vehicleLinearLayout.removeAllViews();
        }

        //Obtener el view de su archivo y cargarlo al LinearLayout padre
        View vehicleForm;
        vehicleForm = getLayoutInflater().inflate(R.layout.assign_client_vehicle_form, null);
        vehicleLinearLayout.addView(vehicleForm);

        platesTv = vehicleForm.findViewById(R.id.client_car_plates_form);
        carNumberTv = vehicleForm.findViewById(R.id.client_car_number_form);
        odometerTv = vehicleForm.findViewById(R.id.client_car_odometer_form);
        marchamoTv = vehicleForm.findViewById(R.id.client_car_marchamo_form);
        newOdometerTv = vehicleForm.findViewById(R.id.client_car_new_odometer_form);
        newMarchamoTv = vehicleForm.findViewById(R.id.client_car_new_marchamo_form);

        //Asignar filtros de mayúscula
        InputFilter[] allCapsFilter = new InputFilter[]{new InputFilter.AllCaps()};
        platesTv.setFilters(allCapsFilter);
        marchamoTv.setFilters(allCapsFilter);
        newMarchamoTv.setFilters(allCapsFilter);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, new String[]{});

        platesTv.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                //Reiniciar adaptador a vacio
                if (charSequence.length() != 0 && !selectedPlate)
                    chipRedManager.searchPlates(ChipRedManager.BYPLATES, charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable)
            {

            }
        });

        platesTv.setThreshold(0);
        platesTv.setAdapter(adapter);
        platesTv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                try
                {
                    //Crear progress
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setCancelable(true);
                    progressDialog.setMessage("Obteniendo información");
                    progressDialog.show();

                    //Obtener Id
                    String id = platesValues.get(i).getString("id");
                    selectedPlates = platesValues.get(i).getString("placas");
                    //Obtener datos de placas y su ultimo consumo
                    chipRedManager.searchPlates(ChipRedManager.BYID, id);
                    //Indicar que se busca la placa por id como al leer un QR
                    byQr = true;
                    selectedPlate = true;
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    private void showPlatesSuggestions(JSONObject response)
    {
        //inicialziar lista
        platesValues.clear();
        platesSuggestions.clear();

        String suggestionString;
        try
        {
            //Obtener lista de elementos sugeridos
            JSONArray array = response.getJSONArray("data");

            for (int i = 0; i < array.length(); i++)
            {
                JSONObject element = array.getJSONObject(i);
                suggestionString = element.getString("placas");

                //Añadir objeto JSON
                platesValues.add(element);
                platesSuggestions.add(suggestionString);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        if (platesSuggestions.size() != 0)
        {
            String[] mSuggestions = platesSuggestions.toArray(new String[]{});
            platesTv.setAdapter(new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_list_item_1, mSuggestions));

            platesTv.showDropDown();
        }
    }

    private boolean evaluateCarClientId(String clientId)
    {
        if (!carClientId.equals(clientId))
        {
            //Remover focus de editText
            platesTv.clearFocus();
            selectedPlate = false;

            if (progressDialog != null)
                progressDialog.dismiss();

            GenericDialog genericDialog = new GenericDialog("Aviso",
                    "Las placas " + selectedPlates + " ya fueron registradas por otro cliente. " +
                            "Favor de corregir",
                    new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            //Hacer vacíos todos los campos
                            platesTv.setText("");
                            carNumberTv.setText("");
                            odometerTv.setText("");
                            marchamoTv.setText("");
                            newMarchamoTv.setText("");
                            newOdometerTv.setText("");
                            newMarchamoTv.setVisibility(View.GONE);
                            newOdometerTv.setVisibility(View.GONE);
                        }
                    }, null, getActivity());
            genericDialog.setCancelable(false);
            genericDialog.setPositiveText("Corregir");
            genericDialog.show();

            searchView.clearFocus();
            return false;
        }

        return true;
    }

    public void setAssignClientInterface(AssignClientInterface assignClientInterface)
    {
        this.assignClientInterface = assignClientInterface;
    }

    public interface AssignClientInterface
    {
        void assignClient(ChipREDClient chipREDClient);

        void assignVehicle(Vehicle vehicle);

        void assignSaleCondition(String saleCondition);

        void finishProcess(boolean withTaxReceipt, String paymentMethodCode);

        void hideKeyboardActivity();

        Sale getSale();
    }
}
