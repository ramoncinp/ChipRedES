package com.binarium.chipredes.cash;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.ExtraProduct;
import com.binarium.chipredes.ExtraProductArrayAdapter;
import com.binarium.chipredes.R;
import com.binarium.chipredes.Sale;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.ThreeRowListViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ExtraProductsFragment extends Fragment
{
    private static final String TAG = "ExtraProductsFragment";

    private Activity activity;

    private ArrayList<ExtraProduct> extraProducts;
    private ExtraProductsInterface extraProductsInterface;
    private ExtraProductArrayAdapter extraProductArrayAdapter;

    private String country;

    private RecyclerView recyclerView;
    private SearchView searchExtraProduct;
    private TextView totalTv;

    public ExtraProductsFragment()
    {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_extra_products, container, false);

        //Asignar views
        totalTv = rootView.findViewById(R.id.extra_products_total);
        recyclerView = rootView.findViewById(R.id.extra_products_list);
        searchExtraProduct = rootView.findViewById(R.id.search_extra_products);
        Button readyButton = rootView.findViewById(R.id.extra_products_ready_button);
        readyButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showSaleSummary();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();

        //Indicar que la actividad esta "Pensando"...
        extraProductsInterface.requestingInfo();

        //Obtener pais
        country = SharedPreferencesManager.getString(activity, ChipREDConstants.COUNTRY, "mexico");

        //Definir QueryListener del searchView
        searchExtraProduct.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String s)
            {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s)
            {
                if (extraProductArrayAdapter != null)
                {
                    //Definir el filtro ingresado por usuario
                    extraProductArrayAdapter.getFilter().filter(s);
                }
                return false;
            }
        });

        //Pedir productos extra
        ChipRedManager chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                extraProductsInterface.gottenInfo();
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                //Indicar que no se obtuvieron los productos extra
                TextView textView = activity.findViewById(R.id.no_extra_products);
                textView.setVisibility(View.VISIBLE);

                //Quitar visibilidad de layout de productos extra
                ConstraintLayout constraintLayout = activity.findViewById(R.id
                        .extra_products_constraint);
                constraintLayout.setVisibility(View.GONE);

                //Indicar que ya se obtuvo la información
                extraProductsInterface.gottenInfo();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                //Obtener información retornada por el servicio
                getExtraProducts(response);
                //Mostrarla en el view
                showExtraProducts();
            }
        }, activity);

        if (country.equals("mexico"))
        {
            totalTv.setText("TOTAL: 0.00 MXN");
        }
        else
        {
            totalTv.setText("TOTAL: 0 CRC");
        }
        chipRedManager.getExtraProducts();
    }

    private void getExtraProducts(JSONObject response)
    {
        try
        {
            //Loggear respuesta
            Log.d(TAG, response.toString(2));
            //Obtener arreglo de objetos JSON
            JSONArray data = response.getJSONArray("data");
            //Instanciar nueva lista para almacenar los objetos
            extraProducts = new ArrayList<>();

            for (int x = 0; x < data.length(); x++)
            {
                //Obtener objeto JSON del indice actual
                JSONObject extraProduct = data.getJSONObject(x);

                //Obtener cada uno de sus valores
                String categoria = extraProduct.getString("categoria");
                String existencias = extraProduct.getString("existencias");
                String desc = extraProduct.getString("descripcion");
                String codigo = extraProduct.getString("codigo");
                String presentacion = extraProduct.getString("presentacion");
                double precio = extraProduct.getDouble("precio");

                JSONObject moneda = extraProduct.getJSONObject("moneda");
                String monedaIco = moneda.getString("ico");
                String monedaIso = moneda.getString("iso");

                //Crear nueva instancia de la Clase "ExtraProduct"
                ExtraProduct extraProductObj = new ExtraProduct(desc, categoria);
                //Setear los demás valores
                extraProductObj.setCantidad(0);
                extraProductObj.setPrecio(precio);
                extraProductObj.setCodigo(codigo);
                extraProductObj.setPresentacion(presentacion);

                String[] monedaStrArray = {monedaIco, monedaIso};
                extraProductObj.setMondeda(monedaStrArray);

                int existenciasInt = Integer.parseInt(existencias);
                extraProductObj.setExistencias(existenciasInt);

                if (existenciasInt == 0)
                {
                    extraProductObj.setAgotado(true);
                }
                else
                {
                    extraProductObj.setAgotado(false);
                }

                extraProducts.add(extraProductObj);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void showExtraProducts()
    {
        //Se asigna al adapter la lista completa de aceites y aditivos
        //Lista que fue obtenida en la respuesta del servicio

        extraProductArrayAdapter = new ExtraProductArrayAdapter(extraProducts, activity, new
                ExtraProductArrayAdapter.AdapterInterface()
                {
                    @Override
                    public void removedOrAddedItem(ExtraProduct extraProduct)
                    {
                        refreshListElement(extraProduct);
                        if (country.contains("costa rica"))
                        {
                            totalTv.setText("TOTAL: " + ChipREDConstants.CR_AMOUNT_FORMAT.format
                                    (recalcularTotal()) + " CRC");
                        }
                        else
                        {
                            totalTv.setText("TOTAL: " + ChipREDConstants.MX_AMOUNT_FORMAT.format
                                    (recalcularTotal()) + " MXN");
                        }
                    }
                });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        extraProductArrayAdapter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                ExtraProduct extraProduct = extraProductArrayAdapter.getExtraProducts().get
                        (recyclerView.getChildAdapterPosition(view));

                if (!extraProduct.isAgotado())
                {
                    CardView cardView = view.findViewById(R.id.extra_product_card);
                    if (extraProduct.isSelected())
                    {
                        cardView.setCardBackgroundColor(activity.getResources().getColor(R.color
                                .white));
                        extraProduct.setSelected(false);
                    }
                    else
                    {
                        if (extraProduct.getCantidad() != 0)
                        {
                            cardView.setCardBackgroundColor(activity.getResources().getColor(R
                                    .color.activo));
                            extraProduct.setSelected(true);
                        }
                    }

                    refreshListElement(extraProduct);
                }
            }
        });

        recyclerView.setAdapter(extraProductArrayAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        extraProductsInterface.gottenInfo();
    }

    public void refreshListElement(ExtraProduct extraProduct)
    {
        for (int i = 0; i < extraProducts.size(); i++)
        {
            if (extraProduct.getCodigo().equals(extraProducts.get(i).getCodigo()))
            {
                extraProducts.remove(i);
                extraProducts.add(i, extraProduct);
                break;
            }
        }
    }

    public double recalcularTotal()
    {
        double importeTotal = 0;
        if (extraProducts != null)
        {
            for (ExtraProduct item : extraProducts)
            {
                if (item.isSelected())
                {
                    double importe = item.getCantidad() * item.getPrecio();
                    importeTotal += importe;
                }
            }
        }

        return importeTotal;
    }

    public void showSaleSummary()
    {
        //Definir formato para decimales
        DecimalFormat decimalFormat;

        if (country.equals("costa rica"))
        {
            decimalFormat = ChipREDConstants.CR_AMOUNT_FORMAT;
        }
        else
        {
            decimalFormat = ChipREDConstants.MX_AMOUNT_FORMAT;
        }

        //Instanciar lista nueva para almacenar los productos seleccionados
        ArrayList<ExtraProduct> selectedProducts = new ArrayList<>();

        //si la lista de total de productos no es nula
        if (extraProducts != null)
        {
            //Añadir los productos seleccionados uno por uno
            for (ExtraProduct product : extraProducts)
            {
                if (product.isSelected())
                {
                    selectedProducts.add(product);
                }
            }
        }
        //Retornar lista a la actividad principal
        extraProductsInterface.returnSelectedExtraProducts(selectedProducts);

        //Construir un dialogo para mostrar los productos que se seleccionaron
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Resumen de venta");
        builder.setCancelable(false);

        View content = getLayoutInflater().inflate(R.layout.dialog_sale_summary, null);
        ListView extraProductsLv = content.findViewById(R.id.lista_resumen_productos_extra);
        ListView saleInfo = content.findViewById(R.id.resumen_venta);

        if (selectedProducts.isEmpty())
        {
            extraProductsLv.setVisibility(View.GONE);
            TextView noExtraProducts = content.findViewById(R.id.no_productos_extra);
            noExtraProducts.setVisibility(View.VISIBLE);
        }
        else
        {
            ArrayList<String> quantities = new ArrayList<>();
            ArrayList<String> descriptions = new ArrayList<>();
            ArrayList<String> prices = new ArrayList<>();

            for (ExtraProduct extraProduct : selectedProducts)
            {
                quantities.add(String.valueOf(extraProduct.getCantidad()));
                descriptions.add(extraProduct.getDescripcion());

                double price = extraProduct.getPrecio() * extraProduct.getCantidad();
                prices.add(decimalFormat.format(price) + " " + extraProduct.getMondeda()[1]);
            }

            extraProductsLv.setAdapter(new ThreeRowListViewAdapter(activity, descriptions,
                    quantities, prices, 20));
        }

        //Obtener datos de la venta asignada
        Sale sale = extraProductsInterface.getAssignedSale();
        ArrayList<String> volume = new ArrayList<>();
        ArrayList<String> product = new ArrayList<>();
        ArrayList<String> cost = new ArrayList<>();

        volume.add(sale.getCantidad() + "L");
        product.add(sale.getProducto());

        double total = recalcularTotal() + sale.getCosto();
        TextView dialogTotalTv = content.findViewById(R.id.total_resumen_venta);

        if (country.equals("costa rica"))
        {
            cost.add(ChipREDConstants.CR_AMOUNT_FORMAT.format(sale.getCosto()) + " CRC");
            dialogTotalTv.setText(ChipREDConstants.CR_AMOUNT_FORMAT.format(total) + " CRC");
        }
        else
        {
            cost.add(ChipREDConstants.MX_AMOUNT_FORMAT.format(sale.getCosto()) + " MXN");
            dialogTotalTv.setText(ChipREDConstants.MX_AMOUNT_FORMAT.format(total) + " MXN");
        }

        saleInfo.setAdapter(new ThreeRowListViewAdapter(activity, volume, product, cost, 20));
        builder.setView(content);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                //Continuar
                extraProductsInterface.setNextFragment();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        AlertDialog mDialog = builder.create();
        mDialog.setCancelable(false);
        mDialog.show();
    }

    public void setExtraProductsInterface(ExtraProductsInterface extraProductsInterface)
    {
        this.extraProductsInterface = extraProductsInterface;
    }

    public interface ExtraProductsInterface
    {
        void requestingInfo();

        void gottenInfo();

        void returnSelectedExtraProducts(ArrayList<ExtraProduct> selectedProducts);

        Sale getAssignedSale();

        void setNextFragment();
    }
}
