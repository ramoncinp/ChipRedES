package com.binarium.chipredes.cash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.cash.models.LastSale
import com.binarium.chipredes.printer.domain.usecases.PrintRaffleTicketUseCase
import com.binarium.chipredes.printer.domain.usecases.TestPrinterUseCase
import com.binarium.chipredes.wolke.domain.usecases.RequestPrintRaffleTicketsUseCase
import com.binarium.chipredes.wolke.models.RequestPrintRaffleTicketResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CashSaleActionSelectorViewModel @Inject constructor(
    private val printRaffleTicketUseCase: PrintRaffleTicketUseCase,
    private val requestPrintRaffleTicketsUseCase: RequestPrintRaffleTicketsUseCase,
    private val testPrinterUseCase: TestPrinterUseCase
) : ViewModel() {

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    private val _printingMessage = MutableLiveData<String>()
    val printingMessage: LiveData<String>
        get() = _printingMessage

    private val _onFinished = MutableLiveData<Boolean>()
    val onFinished: LiveData<Boolean>
        get() = _onFinished

    var selectedSale: LastSale? = null
        set(value) {
            Timber.d("Selected sale set $value")
            field = value
        }

    fun requestPrintRaffleTickets() {
        viewModelScope.launch(Dispatchers.IO) {
            val ticketNumber = selectedSale?.ticketNumber
            val saleDate = selectedSale?.dateTime
            val loadingPosition = selectedSale?.loadingPosition

            if (ticketNumber != null && saleDate != null && loadingPosition != null) {
                _isLoading.postValue(true)

                val testPrinterResult = testPrinterUseCase(loadingPosition)

                if (testPrinterResult) {
                    val result = requestPrintRaffleTicketsUseCase(
                        ticketNumber,
                        saleDate
                    )

                    result.manageResult()
                } else {
                    _errorMessage.postValue("Error al conectarse con la impresora")
                    _isLoading.postValue(false)
                }
            }
        }
    }

    private suspend fun RequestPrintRaffleTicketResult.manageResult() {
        var message = ""
        error?.let {
            message = it
        } ?: run {
            if (tickets > 0) {
                selectedSale?.let { sale ->
                    printTickets(number = tickets, sale)
                } ?: {
                    message = "Error al obtener datos de la venta"
                }
            } else {
                message = "No hay tiquetes para imprimir"
            }
        }

        if (message.isNotEmpty()) {
            _errorMessage.postValue(message)
            _isLoading.postValue(false)
        }
    }

    private suspend fun printTickets(number: Int, sale: LastSale) {
        _isLoading.postValue(false)

        val message = "Imprimiendo $number ${if (number > 1) "tiquetes" else "tiquete"}"
        _printingMessage.postValue(message)

        val result = printRaffleTicketUseCase.invoke(number, sale)
        delay(1500)

        if (result) {
            _onFinished.postValue(true)
        } else {
            _errorMessage.postValue("Error al impirmir tiquetes")
        }
    }
}
