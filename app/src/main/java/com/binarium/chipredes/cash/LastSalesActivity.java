package com.binarium.chipredes.cash;

import android.app.ProgressDialog;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class LastSalesActivity extends AppCompatActivity implements ChipRedManager.OnMessageReceived
{
    //Coonstantes
    private static final String TAG = LastSalesActivity.class.getSimpleName();
    private static final int CASH_PAYMENT_ACTIVITY_SELECTED = 100;
    private static final int SALES_LIMIT = 10;

    //Listas
    private final ArrayList<JSONObject> sales = new ArrayList<>();

    //Objetos
    private ChipRedManager chipRedManager;
    private LastSalesAdapter lastSalesAdapter;

    //Variables
    private boolean allDocuments = false;
    private boolean fetchingMoreDocuments = false;
    private int loadingPosition;
    private int offset;
    private int selectedInvoice;

    //Views
    private ProgressBar paginationProgress;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private RecyclerView lastSalesList;
    private SwipeRefreshLayout swipeRefreshLayout;

    private LastSalesViewModel lastSalesViewModel;
    private boolean isRaffleActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_sales);
        setTitle(getResources().getString(R.string.select_sale));

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        initViews();
        initChipRedManager();
        initViewModel();
        getLastSales(false);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void chipRedMessage(String crMessage, int webServiceN)
    {
        if (LastSalesActivity.this.isFinishing()) return;

        if (webServiceN == ChipRedManager.GET_PRINTERS)
        {
            if (progressDialog != null)
            {
                if (progressDialog.isShowing()) progressDialog.dismiss();
            }
            ChipREDConstants.showSnackBarMessage(crMessage, LastSalesActivity.this);
        }
        else if (webServiceN == ChipRedManager.REPRINT_INVOICE)
        {
            ChipREDConstants.showSnackBarMessage(crMessage, LastSalesActivity.this);
        }
        else if (webServiceN == ChipRedManager.GET_LAST_SALES)
        {
            if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
            paginationProgress.setVisibility(View.GONE);
            allDocuments = true;
            fetchingMoreDocuments = false;
        }
    }

    @Override
    public void chipRedError(String errorMessage, int webServiceN)
    {
        if (LastSalesActivity.this.isFinishing()) return;

        if (webServiceN == ChipRedManager.GET_PRINTERS)
        {
            if (progressDialog != null)
            {
                if (progressDialog.isShowing()) progressDialog.dismiss();
            }
            ChipREDConstants.showSnackBarMessage(errorMessage, LastSalesActivity.this);
        }
        else if (webServiceN == ChipRedManager.REPRINT_INVOICE)
        {
            ChipREDConstants.showSnackBarMessage(errorMessage, LastSalesActivity.this);
        }
        else
        {
            if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
            showResultDialog(errorMessage);
            fetchingMoreDocuments = false;
        }
    }

    @Override
    public void showResponse(JSONObject response, int webServiceN)
    {
        if (LastSalesActivity.this.isFinishing()) return;

        if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        lastSalesList.setVisibility(View.VISIBLE);

        if (webServiceN == ChipRedManager.GET_PRINTERS)
        {
            if (progressDialog != null)
            {
                if (progressDialog.isShowing()) progressDialog.dismiss();
            }

            //Mostrar diálogo para dar opcion de impresoras
            showPrinters(response);
        }
        else
        {
            if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
            lastSalesList.setVisibility(View.VISIBLE);
            paginationProgress.setVisibility(View.GONE);
            fetchingMoreDocuments = false;
            parseSales(response);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CASH_PAYMENT_ACTIVITY_SELECTED)
        {
            if (resultCode == RESULT_OK)
            {
                finish();
            }
            else
            {
                sales.clear();
                offset = 0;
                allDocuments = false;
                getLastSales(false);
            }
        }
    }

    private void initViews()
    {
        lastSalesList = findViewById(R.id.sales_list);
        progressBar = findViewById(R.id.progress_bar);
        swipeRefreshLayout = findViewById(R.id.refresh_list);
        paginationProgress = findViewById(R.id.pagination_progress_bar);

        lastSalesList.setVisibility(View.GONE);

        //Obtener la posicion de carga seleccionada
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            loadingPosition = bundle.getInt(ChipREDConstants.SELECTED_LOADING_POSITION);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            //Limpiar lista de consumos y variables auxiliares
            sales.clear();
            offset = 0;
            allDocuments = false;
            lastSalesViewModel.getRaffleRules();
            getLastSales(false);
        });
    }

    private void initChipRedManager()
    {
        chipRedManager = new ChipRedManager(this, this);
    }

    private void initViewModel() {
        lastSalesViewModel = new ViewModelProvider(this).get(LastSalesViewModel.class);
        lastSalesViewModel.getRaffleData().observe(this, raffleActivated -> isRaffleActive = raffleActivated);
        lastSalesViewModel.getRaffleRules();
    }

    private void getLastSales(boolean pagination)
    {
        // Evaluar cual prgressBar mostrar
        if (!pagination)
        {
            progressBar.setVisibility(View.VISIBLE);
            lastSalesList.setVisibility(View.GONE);
        }

        // Ejecutar servicio para obtener últimas ventas
        chipRedManager.getLastSales(loadingPosition, offset);
    }

    private void showResultDialog(String message)
    {
        GenericDialog genericDialog = new GenericDialog("Obtener consumos", message,
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        LastSalesActivity.this.finish();
                    }
                }, null, this);

        genericDialog.setPositiveText("Regresar");
        genericDialog.show();
    }

    private void parseSales(JSONObject response)
    {
        //Obtener ventas en objeto JSON
        try
        {
            //Iterar en el arreglo de data
            JSONArray data = response.getJSONArray("data");

            //Agregar ventas a la lista
            for (int i = 0; i < data.length(); i++)
            {
                sales.add(data.getJSONObject(i));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Error al procesar la información", Toast.LENGTH_SHORT).show();
        }

        //Ordenar por fecha
        Collections.sort(sales, new Comparator<JSONObject>()
        {
            @Override
            public int compare(JSONObject jsonObject, JSONObject t1)
            {
                Date dateA = ChipREDConstants.getDateFromJSON(jsonObject);
                Date dateB = ChipREDConstants.getDateFromJSON(t1);

                if (dateA == null || dateB == null)
                {
                    return 0;
                }
                return dateB.compareTo(dateA);
            }
        });

        //Mostrar resultados
        if (lastSalesAdapter == null)
        {
            lastSalesAdapter = new LastSalesAdapter(sales, this);
            lastSalesAdapter.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if (isRaffleActive) {
                        showCashSaleActionSelector(view);
                        return;
                    }

                    validateSelectedSale(view);
                }
            });
            lastSalesAdapter.setPrintButtonListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    //Obtener posicion del consumo
                    selectedInvoice = (int) v.getTag();

                    //Mostrar progressDialog
                    progressDialog = ProgressDialog.show(LastSalesActivity.this, "", "Obteniendo " +
                            "impresoras", true);

                    //Pedir impresoras
                    chipRedManager.getPrinters();
                }
            });

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            lastSalesList.setAdapter(lastSalesAdapter);
            lastSalesList.setLayoutManager(linearLayoutManager);
            //setListOnScrollListener(linearLayoutManager);
        }
        else
        {
            lastSalesAdapter.notifyDataSetChanged();
        }
    }

    private void showCashSaleActionSelector(View selectedView) {
        JSONObject selectedSale = sales.get(lastSalesList.getChildAdapterPosition(selectedView));
        CashSaleActionSelector cashSaleActionSelector = new CashSaleActionSelector(
            selectedSale,
            selectedView
        );
        cashSaleActionSelector.setOnSelectInvoiceListener(new CashSaleActionListener() {
            @Override
            public void onSelectInvoice(@NonNull View selectedView) {
                validateSelectedSale(selectedView);
            }

            @Override
            public void onFinishPrint(@NonNull String message) {
                ChipREDConstants.showSnackBarMessage(message,
                        LastSalesActivity.this);
            }
        });
        cashSaleActionSelector.show(getSupportFragmentManager(), "CashSaleActionSelector");
    }

    private void validateSelectedSale(View view) {
        //Mostrar actividad para facturar venta seleccionada
        if (((CardView) view).getCardBackgroundColor().getDefaultColor() == getResources().getColor(R.color.green_money))
        {
            ChipREDConstants.showSnackBarMessage("La venta ya fue asignada",
                    LastSalesActivity.this);
        }
        else
        {
            onSelectedSale(sales.get(lastSalesList.getChildAdapterPosition(view)));
        }
    }

    private void onSelectedSale(JSONObject selectedSale)
    {
        if (selectedSale.has("id_cliente"))
        {
            try
            {
                if (selectedSale.getString("id_cliente").equals(""))
                {
                    Intent intent = new Intent(this, CashPaymentActivity.class);
                    intent.putExtra(ChipREDConstants.SELECTED_SALE, selectedSale.toString());
                    startActivityForResult(intent, CASH_PAYMENT_ACTIVITY_SELECTED);
                }
                else
                {
                    ChipREDConstants.showSnackBarMessage("La venta ya fue asignada", this);
                }
            }
            catch (JSONException e)
            {
                ChipREDConstants.showSnackBarMessage("Error al procesar información", this);
            }
        }
        else
        {
            Intent intent = new Intent(this, CashPaymentActivity.class);
            intent.putExtra(ChipREDConstants.SELECTED_SALE, selectedSale.toString());
            startActivityForResult(intent, CASH_PAYMENT_ACTIVITY_SELECTED);
        }
    }

    private void showPrinters(JSONObject response)
    {
        boolean foundPrinter = false;

        try
        {
            final JSONArray mPrinters = response.getJSONArray("data");
            for (int i = 0; i < mPrinters.length(); i++)
            {
                //Obtener objeto de impresora
                JSONObject printer = mPrinters.getJSONObject(i);

                if (printer.getInt("posicion_carga") == loadingPosition)
                {
                    //Obtener ip de impresora seleccionada
                    String printerIp = printer.getString("ip");

                    //Solicitar reimpresión
                    ChipREDConstants.showSnackBarMessage("Solicitando reimpresión",
                            LastSalesActivity.this);

                    //Obtener objeto jactura de la venta seleccionada
                    JSONObject invoiceData = sales.get(selectedInvoice).getJSONArray(
                            "comprobante").getJSONObject(0).getJSONObject("json_original");

                    invoiceData.put("tipo_comprobante", sales.get(selectedInvoice).getJSONArray(
                            "comprobante").getJSONObject(0).getString("tipo"));

                    //Enviar webService
                    chipRedManager.reprintInvoice(invoiceData, printerIp);
                    foundPrinter = true;
                }
            }

            if (!foundPrinter)
            {
                //Mostrar que no hubo impresora asignada
                ChipREDConstants.showSnackBarMessage("No hay impresora asignada a posicion de " +
                                "carga",
                        LastSalesActivity.this);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void setListOnScrollListener(final LinearLayoutManager linearLayoutManager)
    {
        lastSalesList.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                Log.v(TAG, "Obteniendo mas consumos para facturar");

                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = sales.size();
                int firstVisibleItemPosition =
                        linearLayoutManager.findFirstVisibleItemPosition();

                if (visibleItemCount + firstVisibleItemPosition == totalItemCount && !fetchingMoreDocuments && !allDocuments)
                {
                    // Pedir más elementos
                    fetchingMoreDocuments = true;
                    paginationProgress.setVisibility(View.VISIBLE);

                    // Aumentar offset
                    offset += SALES_LIMIT;

                    // Ejecutar servicio para más documentos
                    getLastSales(true);
                }
            }
        });
    }
}
