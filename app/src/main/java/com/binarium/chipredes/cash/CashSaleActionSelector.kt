package com.binarium.chipredes.cash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.airbnb.lottie.LottieDrawable
import com.binarium.chipredes.cash.mappers.toLastSale
import com.binarium.chipredes.databinding.DialogFragmentCashSaleActionSelectorBinding
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject

@AndroidEntryPoint
class CashSaleActionSelector(
    private val selectedSale: JSONObject,
    private val selectedView: View,
) : DialogFragment() {

    private lateinit var binding: DialogFragmentCashSaleActionSelectorBinding
    private lateinit var listener: CashSaleActionListener
    private val viewModel: CashSaleActionSelectorViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)

        binding = DialogFragmentCashSaleActionSelectorBinding.inflate(
            LayoutInflater.from(context)
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
        setObservers()
        viewModel.selectedSale = selectedSale.toLastSale()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    fun setOnSelectInvoiceListener(listener: CashSaleActionListener) {
        this.listener = listener
    }

    private fun setViews() {
        binding.selectInvoiceCardView.setOnClickListener {
            listener.onSelectInvoice(selectedView)
            dismiss()
        }

        binding.printRaffleTicketCv.setOnClickListener {
            viewModel.requestPrintRaffleTickets()
        }
    }

    private fun setObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            showContent(!it)
        }

        viewModel.printingMessage.observe(viewLifecycleOwner) {
            showPrinting(it)
        }

        viewModel.errorMessage.observe(viewLifecycleOwner) {
            listener.onFinishPrint(it)
            dismiss()
        }

        viewModel.onFinished.observe(viewLifecycleOwner) {
            if (it) {
                listener.onFinishPrint("Tiquetes impresos")
                dismiss()
            }
        }
    }

    private fun showContent(show: Boolean) {
        binding.progressBar.isVisible = !show
        binding.printRaffleTicketCv.isVisible = show
        binding.selectInvoiceCardView.isVisible = show
        binding.selectInvoiceText.isVisible = show
        binding.printRaffleTicketText.isVisible = show
    }

    private fun showPrinting(message: String) {
        showPrintingAnimation()
        binding.printingMessage.text = message
        binding.printAnimationView.isVisible = true
        binding.printingMessage.isVisible = true

        binding.progressBar.isVisible = false
        binding.printRaffleTicketCv.isVisible = false
        binding.selectInvoiceCardView.isVisible = false
        binding.selectInvoiceText.isVisible = false
        binding.printRaffleTicketText.isVisible = false
    }

    private fun showPrintingAnimation() {
        binding.printAnimationView.repeatCount = LottieDrawable.INFINITE
        binding.printAnimationView.playAnimation()
    }
}

interface CashSaleActionListener {
    fun onSelectInvoice(selectedView: View)
    fun onFinishPrint(message: String)
}
