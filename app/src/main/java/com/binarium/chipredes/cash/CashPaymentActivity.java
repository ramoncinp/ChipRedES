package com.binarium.chipredes.cash;

import android.app.Dialog;
import android.content.Intent;

import com.binarium.chipredes.config.domain.GetRemoteConfigBoolean;
import com.binarium.chipredes.config.domain.RemoteConfigKeys;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.binarium.chipredes.ChipREDClient;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.ExtraProduct;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.PaymentMethodObject;
import com.binarium.chipredes.Pumper;
import com.binarium.chipredes.R;
import com.binarium.chipredes.Sale;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.Vehicle;
import com.binarium.chipredes.adapters.ThreeRowRecyclerViewAdapter;
import com.binarium.chipredes.adapters.TwoRowRecyclerViewAdapter;
import com.binarium.chipredes.utils.AsciiInputFilter;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class CashPaymentActivity extends AppCompatActivity {
    private static final String TAG = CashPaymentActivity.class.getName();

    private boolean showExtraProducts = false;
    private int loadingPosition;
    private Integer selectedPaymentMethodIdx = null;
    private String invoiceCode = "00";
    private String country;
    private String saleConditionToAssign = "01"; //Contado por default, "02" es crédito

    //VIEWS
    //Generales
    private RelativeLayout progressBarLayout;
    private TextView progressBarText;
    private ScrollView cashPaymentScrollView;
    private FrameLayout frameLayout;
    private CardView extraProductsCardView;

    //Botones
    private Button addExtraProductsButton;
    private Button assignClientButton;
    private Button selectTaxReceiptButton;

    //Objetos
    private ChipRedManager chipRedManager;
    private ChipREDClient clientToAssign;
    private Sale lastSale;
    private PaymentMethodObject paymentMethod;
    private Pumper salePumper;
    private Vehicle vehicleToAssign;

    //Listas
    private ArrayList<ExtraProduct> selectedExtraProducts = new ArrayList<>();

    //Fragments
    private ExtraProductsFragment extraProductsFragment;
    private AssignClientFragment assignClientFragment;
    private FragmentManager fragmentManager;

    @Inject
    GetRemoteConfigBoolean getRemoteConfigBoolean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cash_payment_activity);
        setTitle("Seleccionar comprobante");

        //Crear variable para obtener venta
        JSONObject selectedSale = null;

        //Obtener la posicion de carga seleccionada
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            try {
                selectedSale = new JSONObject(bundle.getString(ChipREDConstants.SELECTED_SALE));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //Asignar views a sus respectivos objetos
        progressBarLayout = findViewById(R.id.cash_progress_bar_layout);
        progressBarText = findViewById(R.id.cash_progress_bar_text);
        cashPaymentScrollView = findViewById(R.id.cash_payment_scroll_view);
        frameLayout = findViewById(R.id.cash_fragment_container);
        extraProductsCardView = findViewById(R.id.extra_products_card_view);

        //Mostrar en log la posicion de carga seleccionada
        Log.d(TAG, "Posicion seleccionada = " + loadingPosition);

        //Obtener país de la estación
        country = SharedPreferencesManager.getString(this, ChipREDConstants.COUNTRY);
        evaluateCountry();
        getExtraProductsFlag();

        //Inicializar ChipRedManager y definir listener
        setChipRedManagerListener();

        //Inicializar listeners para botones
        setAddButtonListeners();

        //Obtener última venta
        cashPaymentScrollView.setVisibility(View.GONE);
        showProgressBar("Obteniendo última venta");

        if (selectedSale != null) {
            onLastSaleGotten(selectedSale);
        } else {
            chipRedManager.getLastSale(loadingPosition);
        }

        //Inicializar fragmentManager
        fragmentManager = getSupportFragmentManager();

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (chipRedManager != null) chipRedManager.cancellAllRequest();
    }

    private void setChipRedManagerListener() {
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                if (CashPaymentActivity.this.isFinishing()) return;

                hideProgressBar();
                showResultDialog(crMessage);
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                if (CashPaymentActivity.this.isFinishing()) return;
                hideProgressBar();

                if (webServiceN == ChipRedManager.SET_CASH_SALE) {
                    showRetryMessage(errorMessage);
                } else {
                    showResultDialog(errorMessage);
                }
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                if (CashPaymentActivity.this.isFinishing()) return;
                switch (webServiceN) {
                    case ChipRedManager.GET_LAST_SALE:
                        try {
                            JSONObject data = response.getJSONObject("data");
                            onLastSaleGotten(data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case ChipRedManager.GET_PUMPERS_WS:
                        //Asignar a la venta su despachador
                        setSalePumper(response);
                        showCashScrollView();
                        break;

                    case ChipRedManager.SET_CASH_SALE:
                        //Las cosas salieron bien
                        //Regresar al fragment de asignar cliente y reiniciarlo
                        Intent resultIntent = new Intent();
                        //Salió bien el registro
                        setResult(RESULT_OK, resultIntent);
                        finish();
                        break;

                }
            }
        }, this);
    }

    private void onLastSaleGotten(JSONObject response) {
        showSaleInfo(response);
        chipRedManager.getPumpers();
    }

    private void showResultDialog(String message) {
        GenericDialog genericDialog = new GenericDialog("Pago de contado", message, new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, null, this);

        genericDialog.show();
    }

    private void showRetryMessage(String message) {
        GenericDialog genericDialog = new GenericDialog("Pago de contado", message, new Runnable() {
            @Override
            public void run() {
                //Terminar venta en efectivo
                chipRedManager.setCashSale(lastSale, clientToAssign, selectedExtraProducts,
                        vehicleToAssign, saleConditionToAssign, 1);
                showProgressBar("Asignando venta...");
            }
        }, new Runnable() {
            @Override
            public void run() {
                //Cerrar actividad
                finish();
            }
        }, this);

        genericDialog.setPositiveText("Reintentar");
        genericDialog.setNegativeText("Cerrar");
        genericDialog.show();
    }

    public void showProgressBar(String text) {
        frameLayout.setVisibility(View.GONE);
        cashPaymentScrollView.setVisibility(View.GONE);
        progressBarText.setText(text);
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    private void showCashScrollView() {
        progressBarLayout.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
        cashPaymentScrollView.setVisibility(View.VISIBLE);

        if (assignClientFragment != null) assignClientFragment.hideCreateClientMenuItem();
    }

    private void showFrameLayout() {
        progressBarLayout.setVisibility(View.GONE);
        cashPaymentScrollView.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);

        if (assignClientFragment != null) assignClientFragment.showClientMenuItem();
    }

    public void showSaleInfo(JSONObject sale) {
        try {
            Log.d("ChipRedManager", sale.toString(2));
            //Obtener los datos de la venta
            //sale = sale.getJSONObject("data");

            lastSale = new Sale();
            lastSale.setId(sale.getString("id"));
            lastSale.setCantidad(sale.getDouble("cantidad"));
            lastSale.setCosto(sale.getDouble("costo"));
            lastSale.setPrecioUnitario(sale.getDouble("precio_unitario"));
            lastSale.setProducto(sale.getString("combustible"));
            lastSale.setNumProducto(sale.getInt("producto"));
            lastSale.setNumDispensario(sale.getInt("numero_dispensario"));
            lastSale.setPosicionDeCarga(sale.getString("posicion_carga"));

            String fechaHora = sale.getString("fecha_hora");
            lastSale.setFechaHora(fechaHora);
            fechaHora = convertDateFormats(fechaHora);

            lastSale.setFecha(fechaHora.substring(0, fechaHora.indexOf('T')));
            lastSale.setHora(fechaHora.substring(fechaHora.indexOf('T') + 1));
            lastSale.setNumeroTicket(sale.getString("num_ticket"));
            lastSale.setPumperId(sale.getString("id_despachador_emax"));

            String amount = "";
            String volume = ChipREDConstants.VOLUME_FORMAT.format(lastSale.getCantidad()) + " L";
            String unitPrice = "";
            TextView amountTv = findViewById(R.id.last_sale_amount_tv);
            TextView volumeTv = findViewById(R.id.last_sale_volume_tv);
            TextView unitPriceTv = findViewById(R.id.last_sale_unit_price_tv);
            TextView productTv = findViewById(R.id.last_sale_product_desc_tv);

            if (country.equals("mexico")) {
                amount = "$ " + ChipREDConstants.MX_AMOUNT_FORMAT.format(lastSale.getCosto());
                unitPrice = ChipREDConstants.MX_AMOUNT_FORMAT.format(lastSale.getPrecioUnitario()
                ) + " MXN";
            } else if (country.equals("costa rica")) {
                amount = "₡ " + ChipREDConstants.CR_AMOUNT_FORMAT.format(lastSale.getCosto());
                unitPrice = ChipREDConstants.MX_AMOUNT_FORMAT.format(lastSale.getPrecioUnitario()
                ) + " CRC";
            }

            amountTv.setText(amount);
            unitPriceTv.setText(unitPrice);
            volumeTv.setText(volume);
            productTv.setText(lastSale.getProducto());
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
            showSnackbarMessage("Error al procesar la información");
        }
    }

    private String convertDateFormats(String fechaDeEntrada) {
        //Formato de fecha requerida "dd-mm-yyyy hh:MM"
        //Formato de entrada "yyyy-mm-ddThh:MM:ss"

        Date date = null;
        DateFormat dateFormatEntrada = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        try {
            date = dateFormatEntrada.parse(fechaDeEntrada);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String fechaConFormato;
        DateFormat dateFormatSalida = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss", Locale.US);
        fechaConFormato = dateFormatSalida.format(date);

        return fechaConFormato;
    }

    private void setAddButtonListeners() {
        addExtraProductsButton = findViewById(R.id.add_extra_product_to_list);
        addExtraProductsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setExtraProductsFragment();
            }
        });

        assignClientButton = findViewById(R.id.add_client_to_assign);
        assignClientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAssignClientFragment();
            }
        });

        selectTaxReceiptButton = findViewById(R.id.select_tax_receipt);
        selectTaxReceiptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTaxReceipt();
            }
        });

        Button finishSale = findViewById(R.id.cash_payment_finish_sale);
        finishSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmCashPayment();
            }
        });
    }

    private void setExtraProductsFragment() {
        if (extraProductsFragment == null) {
            extraProductsFragment = new ExtraProductsFragment();
            extraProductsFragment.setExtraProductsInterface(new ExtraProductsFragment
                    .ExtraProductsInterface() {
                @Override
                public void requestingInfo() {
                    showProgressBar("Obteniendo productos extra...");
                }

                @Override
                public void gottenInfo() {
                    showFrameLayout();
                }

                @Override
                public void returnSelectedExtraProducts(ArrayList<ExtraProduct> selectedProducts) {
                    //Asignar lista de productos extra seleccionados
                    selectedExtraProducts = selectedProducts;
                    //Mostrar en la lista los productos extra seleccionados
                    showExtraProductsSummary();
                }

                @Override
                public Sale getAssignedSale() {
                    return lastSale;
                }

                @Override
                public void setNextFragment() {
                    showCashScrollView();
                }
            });
        }

        fragmentManager.beginTransaction().replace(R.id.cash_fragment_container,
                extraProductsFragment).commit();
        showFrameLayout();
    }

    private void showExtraProductsSummary() {
        //Obtener referencia del TextView que indica que no se seleccionaron aceites o aditivos
        TextView noExtraProducts = findViewById(R.id.no_extra_product_summary);
        //Obtener referencia de la lista
        RecyclerView extraProductsSummary = findViewById(R.id.extra_product_summary_rv);

        //Analizar la lista de productos extra
        if (selectedExtraProducts.isEmpty()) {
            extraProductsSummary.setVisibility(View.GONE);
            noExtraProducts.setVisibility(View.VISIBLE);
            addExtraProductsButton.setText("+");
        } else {
            showExtraProductSummaryInRecyclerView(extraProductsSummary);
            noExtraProducts.setVisibility(View.GONE);
            addExtraProductsButton.setText("...");
        }
    }

    private void showExtraProductSummaryInRecyclerView(RecyclerView recyclerView) {
        //Crear listas para cada columna
        ArrayList<String> extraProductDesc = new ArrayList<>();
        ArrayList<String> extraProductQuantity = new ArrayList<>();
        ArrayList<String> extraProductSubTotal = new ArrayList<>();

        //Obtener informacion de cada producto extra
        for (ExtraProduct extraProduct : selectedExtraProducts) {
            String desc = extraProduct.getDescripcion();
            int quantity = extraProduct.getCantidad();
            double subTotal = quantity * extraProduct.getPrecio();

            extraProductDesc.add(desc);
            extraProductQuantity.add(String.valueOf(quantity));

            if (country.equals("costa rica")) {
                extraProductSubTotal.add(ChipREDConstants.CR_AMOUNT_FORMAT.format(subTotal) + " "
                        + "CRC");
            } else if (country.equals("mexico")) {
                extraProductSubTotal.add(ChipREDConstants.MX_AMOUNT_FORMAT.format(subTotal) + " "
                        + "MXN");
            }
        }

        //Crear adaptador para mostrar las listas
        ThreeRowRecyclerViewAdapter threeRowRecyclerViewAdapter = new ThreeRowRecyclerViewAdapter
                (this, extraProductDesc, extraProductQuantity, extraProductSubTotal, 24);

        recyclerView.setAdapter(threeRowRecyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void setAssignClientFragment() {
        if (assignClientFragment == null) {
            assignClientFragment = new AssignClientFragment();
            assignClientFragment.setAssignClientInterface(new AssignClientFragment
                    .AssignClientInterface() {
                @Override
                public void assignClient(ChipREDClient chipREDClient) {
                    clientToAssign = chipREDClient;
                    showClientInfo();
                    showVehicleInfo();
                    showCashScrollView();
                    assignClientButton.setText("...");

                    scrollToEnd();
                }

                @Override
                public void assignSaleCondition(String saleCondition) {
                    saleConditionToAssign = saleCondition;
                }

                @Override
                public void finishProcess(boolean taxReceipt, String paymentMethodCode) {
                    //Nada aún
                }

                @Override
                public void hideKeyboardActivity() {

                }

                @Override
                public void assignVehicle(Vehicle vehicle) {
                    vehicleToAssign = vehicle;
                }

                @Override
                public Sale getSale() {
                    return lastSale;
                }
            });
        }

        fragmentManager.beginTransaction().replace(R.id.cash_fragment_container,
                assignClientFragment).commit();
        showFrameLayout();
    }

    private void showClientInfo() {
        //Obtener layout para mostrar info de cliente
        LinearLayout linearLayout = findViewById(R.id.client_info_summary);

        //Analizar si ya existe un view
        if (linearLayout.getChildCount() != 0) {
            linearLayout.removeAllViews();
        }

        //Obtener el view de su archivo y cargarlo al LinearLayout padre
        View clientInfo;
        clientInfo = getLayoutInflater().inflate(R.layout.assign_client_info, null);
        linearLayout.addView(clientInfo);

        TextView nombreTv = linearLayout.findViewById(R.id.nombre_cliente_efectivo);
        TextView correoTv = linearLayout.findViewById(R.id.correo_cliente_efectivo);
        TextView taxCodeTv = linearLayout.findViewById(R.id.clave_fiscal_cliente_efectivo);
        TextView paisTv = linearLayout.findViewById(R.id.pais_cliente_efectivo);
        ImageView paisImage = linearLayout.findViewById(R.id.bandera_pais_cliente_efectivo);

        nombreTv.setText(clientToAssign.getNombre().trim());
        taxCodeTv.setText(clientToAssign.getClaveFiscal());
        paisTv.setText(clientToAssign.getCountry().toUpperCase());

        if (clientToAssign.getSelectedEmails() != null) {
            StringBuilder emails = new StringBuilder();

            emails.append(clientToAssign.getMainEmail());
            for (int i = 0; i < clientToAssign.getSelectedEmails().size(); i++) {
                emails.append("\n");
                emails.append(clientToAssign.getSelectedEmails().get(i));
            }
            correoTv.setText(emails);
        } else {
            correoTv.setText(clientToAssign.getEmail());
        }

        if (clientToAssign.getCountry().equals("mexico")) {
            paisImage.setImageResource(R.drawable.ic_mexico);
        } else if (country.equals("costa rica")) {
            paisImage.setImageResource(R.drawable.ic_costa_rica);
        }

        TextView claveFiscalDesc = linearLayout.findViewById(R.id
                .clave_fiscal_cliente_efectivo_desc);
        if (clientToAssign.getCountry().equals("mexico")) {
            claveFiscalDesc.setText("RFC");
        } else {
            claveFiscalDesc.setText("Cédula");
        }

        linearLayout.setVisibility(View.VISIBLE);
        TextView noClientToAssign = findViewById(R.id.no_client_to_assign);
        noClientToAssign.setVisibility(View.GONE);
    }

    private void showVehicleInfo() {
        if (vehicleToAssign == null) return;

        //Mostrar cardView
        CardView vehicleCv = findViewById(R.id.vehicle_data_card_view);
        vehicleCv.setVisibility(View.VISIBLE);

        //Obtener layout para mostrar info de cliente
        LinearLayout linearLayout = findViewById(R.id.vehicle_info_summary);

        //Analizar si ya existe un view
        if (linearLayout.getChildCount() != 0) {
            linearLayout.removeAllViews();
        }

        //Obtener el view de su archivo y cargarlo al LinearLayout padre
        View vehicleInfo;
        vehicleInfo = getLayoutInflater().inflate(R.layout.assign_client_vehicle_info, null);
        linearLayout.addView(vehicleInfo);

        TextView plates = linearLayout.findViewById(R.id.client_car_plates);
        TextView carNumber = linearLayout.findViewById(R.id.client_car_number);
        TextView odometer = linearLayout.findViewById(R.id.client_car_odometer);
        TextView marchamo = linearLayout.findViewById(R.id.client_car_marchamo);
        TextView performance = linearLayout.findViewById(R.id.client_car_performance);

        plates.setText(vehicleToAssign.getPlate());
        carNumber.setText(vehicleToAssign.getCarNumber());

        if (!vehicleToAssign.getOdometer().isEmpty()) {
            odometer.setText(vehicleToAssign.getOdometer());
        } else {
            odometer.setText(vehicleToAssign.getLastOdometer());
        }

        if (!vehicleToAssign.getMarchamo().isEmpty()) {
            marchamo.setText(vehicleToAssign.getMarchamo());
        } else {
            marchamo.setText(vehicleToAssign.getLastMarchamo());
        }

        if (vehicleToAssign.getPerformanceValue(lastSale.getCantidad()) >= 0)
            performance.setText(vehicleToAssign.getPerformance(lastSale.getCantidad()));

        linearLayout.setVisibility(View.VISIBLE);
    }

    private void selectTaxReceipt() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Comprobante Fiscal");
        View content = getLayoutInflater().inflate(R.layout.dialog_select_tax_receipt, null);

        CardView invoiceCardView = content.findViewById(R.id.invoice_card_view);
        CardView ticketCardView = content.findViewById(R.id.ticket_card_view);
        final MaterialBetterSpinner paymentMethodSpinner = content.findViewById(R.id
                .payment_method_spinner);

        //Obtener lista de medios de pago
        ArrayList<String> paymentMethodsDesc = new ArrayList<>();
        final ArrayList<String> paymentMethodsCodes = new ArrayList<>();

        for (PaymentMethodObject paymentMethod : ChipREDConstants.PAYMENT_METHODS()) {
            paymentMethodsDesc.add(paymentMethod.getName());
            paymentMethodsCodes.add(paymentMethod.getCode());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout
                .simple_dropdown_item_1line, paymentMethodsDesc);
        paymentMethodSpinner.setAdapter(arrayAdapter);
        paymentMethodSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedPaymentMethodIdx = position;
                paymentMethodSpinner.clearFocus();
            }
        });

        if (selectedPaymentMethodIdx != null) {
            paymentMethodSpinner.setText(ChipREDConstants.PAYMENT_METHODS().get
                    (selectedPaymentMethodIdx).getName());
        }

        final Dialog dialog;
        builder.setView(content);
        dialog = builder.create();
        dialog.setCancelable(true);
        dialog.show();

        invoiceCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPaymentMethodIdx == null) {
                    paymentMethodSpinner.setError("Seleccionar medio de pago");
                } else {
                    //Obtener código de método de pago
                    paymentMethod = ChipREDConstants.PAYMENT_METHODS().get
                            (selectedPaymentMethodIdx);
                    //Código definido para factura electrónica
                    invoiceCode = "01";
                    //Mostrar comprobante seleccionado
                    showTaxReceiptSummary();

                    //Mostrar tarjeta de cliente
                    CardView clientCardView = findViewById(R.id.assign_client_card_view);
                    CardView commentCardView = findViewById(R.id.comments_card_view);
                    clientCardView.setVisibility(View.VISIBLE);
                    commentCardView.setVisibility(View.VISIBLE);

                    //Agregar filtro al campo de observaciones
                    MaterialEditText commentsEt = findViewById(R.id.comments_edit_text);
                    commentsEt.setFilters(new InputFilter[]{new AsciiInputFilter()});

                    dialog.dismiss();
                }
            }
        });

        ticketCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPaymentMethodIdx == null) {
                    paymentMethodSpinner.setError("Seleccionar medio de pago");
                } else {
                    //Obtener código de método de pago
                    paymentMethod = ChipREDConstants.PAYMENT_METHODS().get
                            (selectedPaymentMethodIdx);
                    //Código definido para tiquete electrónico
                    invoiceCode = "04";
                    //Mostrar comprobante seleccionado
                    showTaxReceiptSummary();

                    CardView clientCardView = findViewById(R.id.assign_client_card_view);
                    CardView vehicleCardView = findViewById(R.id.vehicle_data_card_view);
                    CardView commentCardView = findViewById(R.id.comments_card_view);
                    clientCardView.setVisibility(View.GONE);
                    vehicleCardView.setVisibility(View.GONE);
                    commentCardView.setVisibility(View.GONE);

                    //Confirmar objeto de clientToAssign como nulo
                    clientToAssign = null;

                    dialog.dismiss();
                }
            }
        });
    }

    private void showTaxReceiptSummary() {
        //Cambiar de texto el botón para agregar comprobante fiscal
        selectTaxReceiptButton.setText("...");

        //Ocultar el texto de "No Comprobante Fiscal"
        TextView noTaxReceipt = findViewById(R.id.no_tax_receipt);
        noTaxReceipt.setVisibility(View.GONE);

        //Obtener referencia de RecyclerView
        RecyclerView taxReceiptSummary = findViewById(R.id.cash_payment_tax_receipt_summary);
        showTaxReceiptInRecyclerView(taxReceiptSummary);

        //Hacer Scroll Automático
        scrollToEnd();
    }

    private void showTaxReceiptInRecyclerView(RecyclerView recyclerView) {
        //Crear listas de columnas y agregarles datos
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();

        keys.add("Tipo de Comprobante");
        keys.add("Medio de pago");

        if (invoiceCode.equals("01")) {
            values.add("Factura Electrónica");
        } else {
            values.add("Tiquete Electrónico");
        }
        values.add(paymentMethod.getName());

        //Crear adaptador
        TwoRowRecyclerViewAdapter adapter = new TwoRowRecyclerViewAdapter(this, keys, values,
                getResources().getInteger(R.integer.normal_screen_text_size),
                false);

        //Asignar adaptador a lista
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void confirmCashPayment() {
        //Validar cambpos obligatorios según país
        if (country.equals("mexico")) {
            if (clientToAssign == null) {
                showSnackbarMessage("Seleccionar un cliente");
                return;
            }
        } else {
            if (paymentMethod == null) {
                showSnackbarMessage("Seleccionar comprobante fiscal");
                return;
            } else {
                //si es factura, comprobar que se haya seleccionado un cliente
                if (invoiceCode.equals("01") && clientToAssign == null) {
                    showSnackbarMessage("Seleccionar un cliente");
                    return;
                }
            }
        }

        //Obtener ultimos datos
        //COMPROBANTE FISCAL
        if (paymentMethod != null) {
            //Asignar codigo de método de pago a la venta
            lastSale.setPaymentMethodCode(paymentMethod.getCode());
        }

        //Asignar valor de comprobante fiscal
        lastSale.setTaxReceiptCode(invoiceCode);

        //Obtener observaciones
        MaterialEditText commentsEt = findViewById(R.id.comments_edit_text);
        Editable commentsText = commentsEt.getText();
        if (commentsText != null) {
            String comments = commentsText.toString();
            if (!comments.isEmpty()) lastSale.setComments(comments);
        }

        if (!showExtraProducts) {
            //Pedir reimpresión
            final GenericDialog genericDialog = new GenericDialog(
                    "Pago de contado", "¿Desea copia adicional?",
                    () -> finishSale(1),
                    () -> finishSale(2),
                    this
            );
            genericDialog.setPositiveText("No");
            genericDialog.setNegativeText("Sí");
            genericDialog.setCancelable(true);
            genericDialog.show();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View content = getLayoutInflater().inflate(R.layout.dialog_confirm_cash_payment, null);

        //INFORMACIÓN DE LA VENTA
        //Crear variables para obtener costo total
        double grandTotal = 0;

        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();

        //Obtener datos de la venta y agregarlos a la lista
        keys.add("Importe:");
        keys.add("Cantidad:");

        //Obtener importe y agregarlo al total
        double saleAmount = lastSale.getCosto();
        grandTotal += saleAmount;

        //Formatear importe
        if (country.equals("mexico")) {
            values.add(ChipREDConstants.MX_AMOUNT_FORMAT.format(saleAmount) + " MXN");
        } else {
            values.add(ChipREDConstants.CR_AMOUNT_FORMAT.format(saleAmount) + " CRC");
        }
        //Formatear cantidad
        values.add(ChipREDConstants.VOLUME_FORMAT.format(lastSale.getCantidad()) + " L");

        //Asignar a RecyclerView
        RecyclerView saleRv = content.findViewById(R.id.cash_payment_sale_summary);
        saleRv.setAdapter(new TwoRowRecyclerViewAdapter(this, keys, values, 24, false));
        saleRv.setLayoutManager(new LinearLayoutManager(this));

        //PRODUCTOS EXTRA
        //Evaluar lista de productos extra
        if (selectedExtraProducts.isEmpty()) {
            TextView noExtraProducts = content.findViewById(R.id
                    .cash_payment_summary_no_extra_products);
            noExtraProducts.setVisibility(View.VISIBLE);
        } else {
            //Obtener referencia de lista
            RecyclerView extraProducts = content.findViewById(R.id
                    .cash_payment_summary_extra_products);
            //Mostrar resumen de productos extra
            showExtraProductSummaryInRecyclerView(extraProducts);
            //Acumular total de productos extra
            for (ExtraProduct extraProduct : selectedExtraProducts) {
                grandTotal += (extraProduct.getCantidad() * extraProduct.getPrecio());
            }
        }

        //INFORMACIÓN DE CLIENTE
        if (clientToAssign == null) {
            TextView noClientToAssign = content.findViewById(R.id.cash_payment_summary_no_client);
            noClientToAssign.setVisibility(View.VISIBLE);
        } else {
            //Crear listas
            ArrayList<String> clientKeys = new ArrayList<>();
            ArrayList<String> clientValues = new ArrayList<>();

            //Agregar valores a las listas
            clientKeys.add("Nombre");
            clientKeys.add("Email");
            clientValues.add(clientToAssign.getNombre());
            clientValues.add(clientToAssign.getEmail());

            //Agregar listas a RecyclerView
            RecyclerView clientRv = content.findViewById(R.id.cash_payment_summary_selected_client);
            clientRv.setAdapter(new TwoRowRecyclerViewAdapter(this, clientKeys, clientValues, 24,
                    false));
            clientRv.setLayoutManager(new LinearLayoutManager(this));
            clientRv.setVisibility(View.VISIBLE);
        }

        //COMPROBANTE FISCAL
        //Ocultar si no es Costa Rica
        if (country.equals("mexico")) {
            LinearLayout taxReceiptLayout = content.findViewById(R.id
                    .cash_payment_tax_receipt_layout);
            taxReceiptLayout.setVisibility(View.GONE);
        }
        if (paymentMethod == null) {
            //Si no hay método de pago, no hay comprobante fiscal
            TextView noTaxReceipt = content.findViewById(R.id.cash_payment_summary_no_tax_receipt);
            noTaxReceipt.setVisibility(View.VISIBLE);
        } else {
            //Obtener referencia de RecyclerView
            RecyclerView taxReceiptSummary = content.findViewById(R.id
                    .cash_payment_summary_tax_receipt);
            showTaxReceiptInRecyclerView(taxReceiptSummary);

            //Asignar codigo de método de pago a la venta
            lastSale.setPaymentMethodCode(paymentMethod.getCode());
        }
        //Asignar valor de comprobante fiscal
        lastSale.setTaxReceiptCode(invoiceCode);

        //Obtener observaciones
        /*MaterialEditText commentsEt = findViewById(R.id.comments_edit_text);

        Editable commentsText = commentsEt.getText();
        if (commentsText != null)
        {
            String comments = commentsText.toString();
            if (!comments.isEmpty()) lastSale.setComments(comments);
        }*/

        //Mostrar total
        TextView grandTotalTv = content.findViewById(R.id.cash_payment_summary_total);
        String totalString;
        if (country.equals("mexico")) {
            totalString = "TOTAL: " + ChipREDConstants.MX_AMOUNT_FORMAT.format(grandTotal) + " MXN";
        } else {
            totalString = "TOTAL: " + ChipREDConstants.CR_AMOUNT_FORMAT.format(grandTotal) + " CRC";
        }
        grandTotalTv.setText(totalString);

        //Asignar el view al builder
        builder.setView(content);

        //Crear Dialogo
        final Dialog dialog = builder.create();
        dialog.setCancelable(true);
        dialog.show();

        //Obtener referencias de los botones
        Button finishSale = content.findViewById(R.id.cash_payment_summary_confirm);
        finishSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                //Terminar venta en efectivo
                chipRedManager.setCashSale(lastSale, clientToAssign, selectedExtraProducts);
                showProgressBar("Asignando venta...");*/
                finishSale(1);

                dialog.dismiss();
            }
        });

        Button closeDialog = content.findViewById(R.id.cash_payment_summary_close);
        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void finishSale(int copies) {
        chipRedManager.setCashSale(lastSale, clientToAssign, selectedExtraProducts,
                vehicleToAssign, saleConditionToAssign, copies);

        showProgressBar("Asignando venta...");
        ChipREDConstants.hideKeyboardFromActivity(this);
    }

    private void setSalePumper(JSONObject response) {
        try {
            JSONArray pumpers = response.getJSONObject("data").getJSONArray("despachadores");
            for (int i = 0; i < pumpers.length(); i++) {
                JSONObject pumperJson = pumpers.getJSONObject(i);
                if (lastSale.getPumperId().equals(pumperJson.getString("id_emax"))) {
                    Pumper pumper = new Pumper();
                    pumper.setEmaxId(pumperJson.getString("id_emax"));
                    //pumper.setLocalId(pumperJson.getString("id_local"));
                    pumper.setName(pumperJson.getString("nombre"));
                    pumper.setNameInitials(pumperJson.getString("iniciales"));
                    pumper.setNip(pumperJson.getString("nip"));
                    pumper.setTagId(pumperJson.getString("tag"));
                    pumper.setListIndex(i);

                    salePumper = pumper;
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getExtraProductsFlag() {
        showExtraProducts = getRemoteConfigBoolean.invoke(RemoteConfigKeys.aceitesCostaRica);
        extraProductsCardView.setVisibility(showExtraProducts ? View.VISIBLE : View.GONE);
    }

    private void evaluateCountry() {
        if (country.equals("mexico")) {
            //No mostrar opcion de comprobante fiscal para México
            CardView taxReceiptCardView = findViewById(R.id.tax_receipt_card_view);
            taxReceiptCardView.setVisibility(View.GONE);
        } else if (country.equals("costa rica")) {
            //Ocultar asignación de cliente para Costa Rica al iniciar actividad
            CardView clientCardView = findViewById(R.id.assign_client_card_view);
            clientCardView.setVisibility(View.GONE);
            CardView vehicleCardView = findViewById(R.id.vehicle_data_card_view);
            vehicleCardView.setVisibility(View.GONE);
            CardView commentCardView = findViewById(R.id.comments_card_view);
            commentCardView.setVisibility(View.GONE);
        }
    }

    /*
    public void askPumperNip()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = getLayoutInflater();
        View dialogNip = inflater.inflate(R.layout.dialog_nip, null);
        final EditText etNipD = dialogNip.findViewById(R.id.etNipC);

        builder.setTitle("Ingresar NIP de despachador: " + salePumper.getName());
        builder.setView(dialogNip);
        builder.setPositiveButton("INGRESAR", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                if (etNipD.getText().toString().equals(salePumper.getNip()))
                {

                }
                else
                {
                    Toast.makeText(CashPaymentActivity.this, "No coincide NIP", Toast
                            .LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }
        });

        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }*/

    private void showSnackbarMessage(String text) {
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG).show();
    }

    private void scrollToEnd() {
        cashPaymentScrollView.post(() -> cashPaymentScrollView.fullScroll(View.FOCUS_DOWN));
    }

    @Override
    public void onBackPressed() {
        if (frameLayout.getVisibility() == View.VISIBLE) {
            showCashScrollView();
        } else {
            finish();
        }
    }
}
