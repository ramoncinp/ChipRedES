package com.binarium.chipredes.cash.mappers

import com.binarium.chipredes.cash.models.LastSale
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.json.JSONObject

private val moshi: Moshi by lazy {
    Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()
}

fun JSONObject.toLastSale(): LastSale? {
    val adapter = moshi.adapter(LastSale::class.java)
    return adapter.fromJson(this.toString())
}
