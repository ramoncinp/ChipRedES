package com.binarium.chipredes.cash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.wolke.domain.usecases.GetStationRaffleRulesUseCase
import com.binarium.chipredes.wolke.models.RafflePrintType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LastSalesViewModel @Inject constructor(
    private val getStationRaffleRulesUseCase: GetStationRaffleRulesUseCase
) : ViewModel() {

    private val _raffleData = MutableLiveData<Boolean>()
    val raffleData: LiveData<Boolean?>
        get() = _raffleData

    fun getRaffleRules() {
        viewModelScope.launch(Dispatchers.IO) {
            val isActivated = getStationRaffleRulesUseCase()?.let {
                it.isActive && it.printType == RafflePrintType.MANUAL
            } ?: false
            _raffleData.postValue(isActivated)
        }
    }
}
