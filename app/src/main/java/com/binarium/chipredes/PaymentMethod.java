package com.binarium.chipredes;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import com.binarium.chipredes.billets.ui.BilletSaleActivity;
import com.binarium.chipredes.disppreset.SendPresetActivity;
import com.binarium.chipredes.tokencash.ui.tokencashactivity.TokenCashActivity;
import com.google.android.material.snackbar.Snackbar;

import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.widget.Toast;

import com.binarium.chipredes.cash.LastSalesActivity;

import java.util.ArrayList;

import static com.binarium.chipredes.PaymentMethodsList.REQUEST_CASH_PAYMENT;
import static com.binarium.chipredes.PaymentMethodsList.REQUEST_READ_BILLETS;
import static com.binarium.chipredes.PaymentMethodsList.REQUEST_SEND_PRESET;
import static com.binarium.chipredes.PaymentMethodsList.REQUEST_TOKENCASH_PAYMENT;

public class PaymentMethod extends AppCompatActivity
{
    // Variables
    private String scannerResult;
    private int selectedPump;

    // Objetos
    PaymentMethodsList paymentMethodsList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();
        selectedPump = bundle.getInt("pump");

        //Checar si solamente hay un método de pago habilitado
        Integration singleIntegration = checkSinglePaymentMethod();
        if (singleIntegration != null)
        {
            setSelectedIntegration(singleIntegration);
        }
        else
        {
            //Existen más integraciones habilitadas
            setPaymentMethodsFragment();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case REQUEST_CASH_PAYMENT:
            case REQUEST_TOKENCASH_PAYMENT:
            case REQUEST_SEND_PRESET:
                finish();
                break;

            case REQUEST_READ_BILLETS:
                if (resultCode == RESULT_OK || paymentMethodsList == null)
                {
                    finish();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
        {
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void setPaymentMethodsFragment()
    {
        Bundle bundle = new Bundle();
        bundle.putInt(ChipREDConstants.SELECTED_LOADING_POSITION, selectedPump);

        paymentMethodsList = new PaymentMethodsList();
        paymentMethodsList.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.payment_method_container,
                paymentMethodsList).commit();
    }

    public void hideToolBar()
    {
        Toolbar toolbar = findViewById(R.id.devices_toolbar);

        if (getSupportActionBar() == null) setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().hide();
    }

    public void showToolbar()
    {
        Toolbar toolbar = findViewById(R.id.devices_toolbar);

        if (getSupportActionBar() == null) setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().show();
    }

    public String getScannerResult()
    {
        return scannerResult;
    }

    public void setScannerResult(String scannerResult)
    {
        this.scannerResult = scannerResult;
    }

    public void showToastMessage(String message)
    {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        finish();
    }

    public void showErrorDialog(String title, String errorMessage)
    {
        GenericDialog genericDialog = new GenericDialog(title, errorMessage, new Runnable()
        {
            @Override
            public void run()
            {
                finish();
            }
        }, null, this);

        genericDialog.setCancelable(false);
        genericDialog.show();
    }

    private boolean checkCameraPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager
                    .PERMISSION_GRANTED)
            {
                Snackbar.make(findViewById(android.R.id.content), "No se tienen permisos para " +
                        "utilizar la cámara", Snackbar.LENGTH_LONG).show();
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }

    private Integration checkSinglePaymentMethod()
    {
        ArrayList<Integration> enabledIntegrations = new ArrayList<>();

        //Añadir a la lista solo las integraciones habilitadas
        String[][] mIntegrations = ChipREDConstants.INTEGRATIONS;
        for (String[] integrationArray : mIntegrations)
        {
            Integration integration = new Integration(integrationArray[0], integrationArray[1],
                    this);
            integration.setImageResource(integrationArray[2]);

            if (integration.isEnabled())
            {
                enabledIntegrations.add(integration);
            }
        }

        if (enabledIntegrations.size() == 1)
        {
            return enabledIntegrations.get(0);
        }
        else
        {
            return null;
        }
    }

    private void setSelectedIntegration(Integration integration)
    {
        String integrationName = integration.getName();
        if (integrationName.contains(ChipREDConstants.INTEGRATIONS[0][0]))
        {
            Intent readBilletsIntent = new Intent(this, BilletSaleActivity.class);
            readBilletsIntent.putExtra(ChipREDConstants.SELECTED_LOADING_POSITION,
                    selectedPump);
            startActivityForResult(readBilletsIntent, REQUEST_READ_BILLETS);
        }
        else if (integrationName.contains(ChipREDConstants.INTEGRATIONS[1][0]))
        {
            //Iniciar fragment de venta en efectivo
            Intent cashIntent = new Intent(this, LastSalesActivity.class);
            cashIntent.putExtra(ChipREDConstants.SELECTED_LOADING_POSITION,
                    selectedPump);
            startActivityForResult(cashIntent, REQUEST_CASH_PAYMENT);
        }
        else if (integrationName.contains(ChipREDConstants.INTEGRATIONS[2][0]))
        {
            //Iniciar fragment de pago con token
            Intent cashIntent = new Intent(this, TokenCashActivity.class);
            cashIntent.putExtra(ChipREDConstants.SELECTED_LOADING_POSITION,
                    selectedPump);
            startActivityForResult(cashIntent, REQUEST_TOKENCASH_PAYMENT);
        }
        else if (integrationName.contains(ChipREDConstants.INTEGRATIONS[3][0]))
        {
            //Iniciar actividad para enviar preset
            Intent sendPresetIntent = new Intent(this, SendPresetActivity.class);
            sendPresetIntent.putExtra(ChipREDConstants.SELECTED_LOADING_POSITION, selectedPump);
            startActivityForResult(sendPresetIntent, REQUEST_SEND_PRESET);
        }
    }
}