package com.binarium.chipredes.di

import android.content.Context
import com.binarium.chipredes.db.StationDao
import com.binarium.chipredes.printer.kt.PrinterManagerKt
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(FragmentComponent::class)
object PrinterModule {

    @Provides
    fun providePrinterManager(
        @ApplicationContext context: Context,
        stationDao: StationDao,
    ): PrinterManagerKt = PrinterManagerKt(context, stationDao)
}
