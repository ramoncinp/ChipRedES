package com.binarium.chipredes.di

import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.LoggerImpl
import com.binarium.chipredes.wolke.domain.usecases.GetStationMongoIdUseCase
import com.binarium.chipredes.wolke2.Wolke2ApiService
import com.binarium.chipredes.wolke2.domain.usecases.LogEventUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object LoggerModule {

    @Provides
    fun provideLogger(
        wolke2ApiService: Wolke2ApiService,
        mongoIdUseCase: GetStationMongoIdUseCase
    ): Logger {
        val useCase = LogEventUseCase(wolke2ApiService, mongoIdUseCase)
        return LoggerImpl(useCase)
    }
}
