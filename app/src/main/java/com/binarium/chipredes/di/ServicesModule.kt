package com.binarium.chipredes.di

import com.binarium.chipredes.configstation.service.StationManagerService
import com.binarium.chipredes.local.LocalApiService
import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke2.Wolke2ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServicesModule {
    @Provides
    @Singleton
    fun provideLocalService(serviceBuilder: ServiceBuilder): LocalApiService =
        serviceBuilder.buildLocalService(LocalApiService::class.java)

    @Provides
    @Singleton
    fun provideWolkeService(serviceBuilder: ServiceBuilder): WolkeApiService =
        serviceBuilder.buildWolkeService(WolkeApiService::class.java)

    @Provides
    @Singleton
    fun provideWolke2Service(serviceBuilder: ServiceBuilder): Wolke2ApiService =
        serviceBuilder.buildWolke2Service(Wolke2ApiService::class.java)

    @Provides
    @Singleton
    fun provideStationManagerService(serviceBuilder: ServiceBuilder): StationManagerService =
        serviceBuilder.buildStationManagerService(StationManagerService::class.java)
}