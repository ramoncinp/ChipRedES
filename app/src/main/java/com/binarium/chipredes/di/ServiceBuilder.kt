package com.binarium.chipredes.di

import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named

class ServiceBuilder @Inject constructor(
        @Named("local") private val retrofitLocal: Retrofit,
        @Named("wolke") private val retrofitWolke: Retrofit,
        @Named("wolke2") private val retrofitWolke2: Retrofit,
        @Named("stationManager") private val stationManager: Retrofit
) {
    fun <T> buildLocalService(serviceType: Class<T>): T {
        return retrofitLocal.create(serviceType)
    }

    fun <T> buildWolkeService(serviceType: Class<T>): T {
        return retrofitWolke.create(serviceType)
    }

    fun <T> buildWolke2Service(serviceType: Class<T>): T {
        return retrofitWolke2.create(serviceType)
    }

    fun <T> buildStationManagerService(serviceType: Class<T>): T {
        return stationManager.create(serviceType)
    }
}
