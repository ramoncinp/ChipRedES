package com.binarium.chipredes.di

import android.content.SharedPreferences
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.local.data.LocalServerClient
import com.binarium.chipredes.local.data.LocalServerClientImpl
import com.binarium.chipredes.logger.Logger
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    private const val LOCAL_BASE_URL = "http://192.168.0.50/"
    private const val WOLKE_BASE_URL = "http://www.chipred.com/"
    private const val WOLKE2_BASE_URL = ChipREDConstants.WOLKE2_BASE_URL
    var localUrl: String = ""
    var wolkeUrl: String = ""
    var stationManagerUrl: String = ""

    @Provides
    @Singleton
    fun provideKotlinJsonAdapterFactory(): KotlinJsonAdapterFactory = KotlinJsonAdapterFactory()

    @Provides
    @Singleton
    fun provideMoshi(kotlinJsonAdapterFactory: KotlinJsonAdapterFactory): Moshi = Moshi.Builder()
        .add(kotlinJsonAdapterFactory)
        .build()

    @Provides
    @Singleton
    fun provideMoshiConverterFactory(moshi: Moshi): MoshiConverterFactory =
        MoshiConverterFactory.create(moshi)

    @Provides
    @Singleton
    fun provideScalarsConverterFactory(): ScalarsConverterFactory =
        ScalarsConverterFactory.create()

    @Provides
    @Singleton
    @Named("localHttpClient")
    fun provideLocalOkHttp(sharedPreferences: SharedPreferences): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                localUrl = sharedPreferences.getString(ChipREDConstants.CR_LOCAL_IP, "").toString()
                return try {
                    val request: Request = chain
                        .request()
                        .newBuilder()
                        .url(localUrl)
                        .addHeader("Content-Type", "application/json")
                        .build()
                    chain.proceed(request)
                } catch (e: java.lang.Exception) {
                    chain.proceed(chain.request())
                    throw e
                }
            }
        })

        httpClient.connectTimeout(5, TimeUnit.SECONDS)
        return httpClient.build()
    }

    @Provides
    @Singleton
    @Named("wolkeHttpClient")
    fun provideWolkeOkHttp(sharedPreferences: SharedPreferences): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(object : Interceptor {
            @Throws(Exception::class)
            override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                wolkeUrl = sharedPreferences.getString(ChipREDConstants.CR_WOLKE_IP, "").toString()
                return try {
                    val request: Request = chain
                        .request()
                        .newBuilder()
                        .url(wolkeUrl)
                        .addHeader("Content-Type", "application/json")
                        .build()
                    chain.proceed(request)
                } catch (e: Exception) {
                    chain.proceed(chain.request())
                    throw e
                }
            }
        })

        httpClient.connectTimeout(5, TimeUnit.SECONDS)
        return httpClient.build()
    }

    @Provides
    @Singleton
    @Named("local")
    fun provideLocalRetrofitClient(
        @Named("localHttpClient") okHttp: OkHttpClient,
        moshiConverterFactory: MoshiConverterFactory,
        scalarsConverterFactory: ScalarsConverterFactory,
    ): Retrofit = Retrofit.Builder()
        .addConverterFactory(moshiConverterFactory)
        .addConverterFactory(scalarsConverterFactory)
        .client(okHttp)
        .baseUrl(LOCAL_BASE_URL)
        .build()

    @Provides
    @Singleton
    @Named("wolke")
    fun provideWolkeRetrofitClient(
        @Named("wolkeHttpClient") okHttp: OkHttpClient,
        moshiConverterFactory: MoshiConverterFactory,
        scalarsConverterFactory: ScalarsConverterFactory,
    ): Retrofit = Retrofit.Builder()
        .addConverterFactory(moshiConverterFactory)
        .addConverterFactory(scalarsConverterFactory)
        .client(okHttp)
        .baseUrl(WOLKE_BASE_URL)
        .build()

    @Provides
    @Singleton
    @Named("wolke2")
    fun provideWolke2RetrofitClient(
        moshiConverterFactory: MoshiConverterFactory,
        scalarsConverterFactory: ScalarsConverterFactory,
    ): Retrofit = Retrofit.Builder()
        .addConverterFactory(moshiConverterFactory)
        .addConverterFactory(scalarsConverterFactory)
        .baseUrl(WOLKE2_BASE_URL)
        .build()

    @Provides
    @Singleton
    @Named("stationManager")
    fun provideStationManagerClient(
        moshiConverterFactory: MoshiConverterFactory,
        scalarsConverterFactory: ScalarsConverterFactory,
        sharedPreferences: SharedPreferences,
    ): Retrofit {
        val wolkeUrl = sharedPreferences.getString(ChipREDConstants.CR_WOLKE_IP, WOLKE_BASE_URL)
        val stationManagerBaseUrl = wolkeUrl?.replace("webservice_app/", "") ?: ""

        return Retrofit.Builder()
            .addConverterFactory(moshiConverterFactory)
            .addConverterFactory(scalarsConverterFactory)
            .baseUrl(stationManagerBaseUrl)
            .build()
    }

    @Provides
    @Singleton
    fun provideLocalServerClient(
        sharedPreferences: SharedPreferences,
        logger: Logger
    ): LocalServerClient = LocalServerClientImpl(sharedPreferences, logger)
}
