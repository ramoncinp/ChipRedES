package com.binarium.chipredes.di

import android.content.Context
import com.binarium.chipredes.config.domain.usecases.GetEmaxClientPrefUseCase
import com.binarium.chipredes.emax.EmaxApiService
import com.binarium.chipredes.emax.EmaxDBManager
import com.binarium.chipredes.logger.Logger
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object EmaxModule {
    @Provides
    @Singleton
    fun provideEmaxDBManager(
        @ApplicationContext appContext: Context,
        getEmaxClientPrefUseCase: GetEmaxClientPrefUseCase,
        logger: Logger
    ): EmaxDBManager = EmaxDBManager(appContext, getEmaxClientPrefUseCase, logger)

    @Provides
    @Singleton
    fun provideEmaxApiService(
        @ApplicationContext appContext: Context,
        logger: Logger
    ): EmaxApiService = EmaxApiService(appContext, logger)
}