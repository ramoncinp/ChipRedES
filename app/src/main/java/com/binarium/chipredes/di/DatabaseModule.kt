package com.binarium.chipredes.di

import android.content.Context
import androidx.room.Room
import com.binarium.chipredes.db.ChipRedDatabase
import com.binarium.chipredes.db.StationDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    fun provideStationDao(db: ChipRedDatabase): StationDao =
        db.stationDao()

    @Provides
    @Singleton
    fun provideChipRedDatabase(@ApplicationContext context: Context): ChipRedDatabase =
        Room.databaseBuilder(
            context.applicationContext,
            ChipRedDatabase::class.java,
            "ChipRedDatabase"
        ).fallbackToDestructiveMigration().build()
}
