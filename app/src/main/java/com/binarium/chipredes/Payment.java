package com.binarium.chipredes;

public class Payment
{
    private double paymentQuantity;
    private double newBalance;
    private double lastBalance;
    private String dateHour;
    private String account;
    private String pumper;
    private String station;

    public Payment()
    {

    }

    public double getPaymentQuantity()
    {
        return paymentQuantity;
    }

    public void setPaymentQuantity(double paymentQuantity)
    {
        this.paymentQuantity = paymentQuantity;
    }

    public double getNewBalance()
    {
        return newBalance;
    }

    public void setNewBalance(double newBalance)
    {
        this.newBalance = newBalance;
    }

    public String getStation()
    {
        return station;
    }

    public void setStation(String station)
    {
        this.station = station;
    }

    public double getLastBalance()
    {
        return lastBalance;
    }

    public void setLastBalance(double lastBalance)
    {
        this.lastBalance = lastBalance;
    }

    public String getDateHour()
    {
        return dateHour;
    }

    public void setDateHour(String dateHour)
    {
        this.dateHour = dateHour;
    }

    public String getAccount()
    {
        return account;
    }

    public void setAccount(String account)
    {
        this.account = account;
    }

    public String getPumper()
    {
        return pumper;
    }

    public void setPumper(String pumper)
    {
        this.pumper = pumper;
    }
}
