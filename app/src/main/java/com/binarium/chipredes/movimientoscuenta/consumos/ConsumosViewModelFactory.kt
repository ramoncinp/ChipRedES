package com.binarium.chipredes.movimientoscuenta.consumos

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.binarium.chipredes.printer.PrinterManager

class ConsumosViewModelFactory(
        private val printerManager: PrinterManager,
        private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ConsumosViewModel::class.java)) {
            return ConsumosViewModel(printerManager, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}