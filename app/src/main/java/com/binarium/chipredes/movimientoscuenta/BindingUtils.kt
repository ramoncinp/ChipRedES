package com.binarium.chipredes.movimientoscuenta

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.binarium.chipredes.wolke2.models.MovimientoCuenta

@BindingAdapter("movimientosListData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<MovimientoCuenta>?) {
    val adapter = recyclerView.adapter as MovimientosCuentaAdapter
    adapter.submitList(data)
}