package com.binarium.chipredes.movimientoscuenta

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import timber.log.Timber

class MovimientosCuentaFragment : Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val arguments = requireActivity().intent?.extras?.let { MovimientosCuentaFragmentArgs.fromBundle(it) }
        requireActivity().intent?.extras?.clear()
        Timber.i("My arguments are -> $arguments")

        // Navegar al fragment correspondiente
        when (arguments?.purchaseOrPayment) {
            1 -> this.findNavController().navigate(MovimientosCuentaFragmentDirections.actionMovimientosCuentaFragmentToConsumosFragment())
            2 -> this.findNavController().navigate(MovimientosCuentaFragmentDirections.actionMovimientosCuentaFragmentToAbonosFragment())
        }
    }
}