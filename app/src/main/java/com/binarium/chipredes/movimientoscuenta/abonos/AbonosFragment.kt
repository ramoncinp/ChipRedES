package com.binarium.chipredes.movimientoscuenta.abonos

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.binarium.chipredes.R
import com.binarium.chipredes.databinding.AbonosFragmentBinding
import com.binarium.chipredes.movimientoscuenta.MovimientosCuentaAdapter
import com.binarium.chipredes.printer.PrinterManager
import com.binarium.chipredes.printer.PrintersDialogFragment
import com.binarium.chipredes.searchclient.SelectedClientViewModel
import com.binarium.chipredes.utils.showSimpleDialog
import com.binarium.chipredes.wolke2.models.MovimientoCuenta
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber

class AbonosFragment : Fragment() {

    private lateinit var viewModel: AbonosViewModel
    private val selectedClientViewModel: SelectedClientViewModel by activityViewModels()

    private lateinit var printerManager: PrinterManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Observar cliente seleccionado
        selectedClientViewModel.selectedClient.observe(this) {
            Timber.i("Nuevo cliente seleccionado ${it.email}")
            viewModel.getPayments(it)
        }

        // Observar estado de cliente para manejar nave gación a Fragment de búsqueda
        if (selectedClientViewModel.selectedClient.value == null) {
            Timber.i("Navegar a SearchViewFragment")
            this.findNavController().navigate(AbonosFragmentDirections.actionAbonosFragmentToSearchLocalClientFragment())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inicializar printerManager
        initPrinterManager()

        val viewModelFactory = AbonosViewModelFactory(printerManager)
        viewModel = ViewModelProvider(this, viewModelFactory).get(AbonosViewModel::class.java)

        viewModel.errorMessage.observe(viewLifecycleOwner, {
            if (it != null) {
                context?.let { mContext -> showSimpleDialog(mContext, "Error", it) }
            }
        })

        // Crear binding con layout
        val binding: AbonosFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.abonos_fragment, container, false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        // Definir datos de lista de consumos
        binding.paymentsList.adapter = MovimientosCuentaAdapter(
                MovimientosCuentaAdapter.OnClickListener {

                    if (viewModel.printerIp.isEmpty()) {
                        requestPrinterIp(it)
                    } else {
                        showSnackBar("Imprimiendo...")
                        viewModel.printPayment(it)
                    }
                }
        )
        binding.paymentsList.layoutManager = LinearLayoutManager(context)

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.movimientos_cuenta_menu, menu)
    }

    override fun onResume() {
        super.onResume()

        requireActivity().onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                requireActivity().finish()
            }
        })

        activity?.title = "Abonos"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.searchLocalClientFragment) {
            this.findNavController().navigate(AbonosFragmentDirections.actionAbonosFragmentToSearchLocalClientFragment())
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initPrinterManager() {
        printerManager = PrinterManager(requireNotNull(activity).application, object : PrinterManager.PrinterListener {
            override fun onSuccesfulPrint(msg: String?) {
                if (msg != null) {
                    showSnackBar(msg)
                }
            }

            override fun onPrinterError(msg: String?) {
                if (msg != null) {
                    showSnackBar(msg)
                }
            }

        })
    }

    private fun showSnackBar(message: String) {
        val parentView: View = activity?.findViewById(android.R.id.content) ?: return
        val snackBar = Snackbar.make(
                parentView,
                message,
                Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }

    private fun requestPrinterIp(movimientoCuenta: MovimientoCuenta) {
        val printersDialogFragment = PrintersDialogFragment()
        printersDialogFragment.setPrinterDialogInterface {
            viewModel.printerIp = it

            showSnackBar("Imprimiendo...")
            viewModel.printPayment(movimientoCuenta)
            printersDialogFragment.dismiss()
        }

        activity?.supportFragmentManager?.let {
            printersDialogFragment.show(it, "printersDialog")
        }
    }
}