package com.binarium.chipredes.movimientoscuenta

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.databinding.AbonoLayoutBinding
import com.binarium.chipredes.databinding.ConsumoVbLayoutBinding
import com.binarium.chipredes.utils.DateUtils
import com.binarium.chipredes.wolke2.models.MovimientoCuenta

private const val CONSUMOS_TYPE = 1
private const val ABONOS_TYPE = 2

class MovimientosCuentaAdapter(private val onClickListener: OnClickListener) :
    ListAdapter<MovimientoCuenta, ViewHolder>(DiffCallback) {

    class ConsumosViewHolder(private var binding: ConsumoVbLayoutBinding) :
        ViewHolder(binding.root) {

        fun bind(clickListener: OnClickListener, movimientoCuenta: MovimientoCuenta) {
            val volume =
                ChipREDConstants.VOLUME_FORMAT.format(movimientoCuenta.consumo?.cantidad) + " L"
            val amount =
                "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(movimientoCuenta.consumo?.costo)
            val date = DateUtils.formatoFechaMovimientosCuenta(movimientoCuenta.fechaHora, false)

            binding.consumoVolume.text = volume
            binding.consumoAmount.text = amount
            binding.consumoHora.text = DateUtils.getHour(date)
            binding.consumoFecha.text = DateUtils.getDayMonthYear(date)
            binding.printMovimientoCv.setOnClickListener { clickListener.onClick(movimientoCuenta) }
            (binding.root as CardView).setCardBackgroundColor(Color.parseColor(movimientoCuenta.consumo?.producto?.color))
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ConsumoVbLayoutBinding.inflate(layoutInflater, parent, false)

                return ConsumosViewHolder(binding)
            }
        }
    }

    class AbonosViewHolder(private var binding: AbonoLayoutBinding) :
        ViewHolder(binding.root) {

        fun bind(clickListener: OnClickListener, movimientoCuenta: MovimientoCuenta) {
            val amount = "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(movimientoCuenta.cantidad)
            val date = DateUtils.formatoFechaMovimientosCuenta(movimientoCuenta.fechaHora, false)

            binding.abonoAmount.text = amount
            binding.abonoHora.text = DateUtils.getHour(date)
            binding.abonoFecha.text = DateUtils.getDayMonthYear(date)
            binding.printMovimientoCv.setOnClickListener { clickListener.onClick(movimientoCuenta) }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = AbonoLayoutBinding.inflate(layoutInflater, parent, false)

                return AbonosViewHolder(binding)
            }
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<MovimientoCuenta>() {
        override fun areItemsTheSame(
            oldItem: MovimientoCuenta,
            newItem: MovimientoCuenta,
        ): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(
            oldItem: MovimientoCuenta,
            newItem: MovimientoCuenta,
        ): Boolean {
            return if (oldItem.tipoMovimiento == "VB")
                oldItem.consumo?.numTicket == newItem.consumo?.numTicket
            else
                oldItem.folio == newItem.folio
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            CONSUMOS_TYPE -> ConsumosViewHolder.from(parent)
            ABONOS_TYPE -> AbonosViewHolder.from(parent)
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is ConsumosViewHolder -> {
                val movimientoCuenta = getItem(position)
                holder.bind(onClickListener, movimientoCuenta)
            }

            is AbonosViewHolder -> {
                val movimientoCuenta = getItem(position)
                holder.bind(onClickListener, movimientoCuenta)
            }
        }
    }

    class OnClickListener(val clickListener: (movimientoCuenta: MovimientoCuenta) -> Unit) {
        fun onClick(movimientoCuenta: MovimientoCuenta) = clickListener(movimientoCuenta)
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position).tipoMovimiento) {
            "VB" -> CONSUMOS_TYPE
            "DC" -> ABONOS_TYPE
            else -> CONSUMOS_TYPE
        }
    }
}