package com.binarium.chipredes.movimientoscuenta.consumos

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.binarium.chipredes.R
import com.binarium.chipredes.databinding.FragmentConsumosBinding
import com.binarium.chipredes.movimientoscuenta.MovimientosCuentaAdapter
import com.binarium.chipredes.printer.PrinterManager
import com.binarium.chipredes.searchclient.SelectedClientViewModel
import com.binarium.chipredes.utils.showSimpleDialog
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber

class ConsumosFragment : Fragment() {

    private lateinit var viewModel: ConsumosViewModel
    private val selectedClientViewModel: SelectedClientViewModel by activityViewModels()

    private lateinit var printerManager: PrinterManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Observar cliente seleccionado
        selectedClientViewModel.selectedClient.observe(this) {
            Timber.i("Nuevo cliente seleccionado ${it.email}")
            viewModel.getPurchases(it)
        }

        // Observar estado de cliente para manejar navegación a Fragment de búsqueda
        if (selectedClientViewModel.selectedClient.value == null) {
            Timber.i("Navegar a SearchViewFragment")
            this.findNavController().navigate(ConsumosFragmentDirections.actionConsumosFragmentToSearchLocalClientFragment())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val application = requireNotNull(activity).application

        // Inicializar printerManager
        initPrinterManager(application)

        // Obtener viewModel correspondiente a este Fragment
        val viewModelFactory = ConsumosViewModelFactory(printerManager, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ConsumosViewModel::class.java)

        // Observar mensaje de error para mostrar en diálogo
        viewModel.errorMessage.observe(viewLifecycleOwner, {
            if (it != null) {
                context?.let { mContext -> showSimpleDialog(mContext, "Error", it) }
            }
        })

        // Crear binding con layout
        val binding: FragmentConsumosBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_consumos, container, false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        // Definir datos de lista de consumos
        binding.consumosList.adapter = MovimientosCuentaAdapter(
                MovimientosCuentaAdapter.OnClickListener {
                    showSnackBar("Imprimiendo...")
                    viewModel.printPurchase(it)
                }
        )
        binding.consumosList.layoutManager = LinearLayoutManager(context)

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.movimientos_cuenta_menu, menu)
    }

    override fun onResume() {
        super.onResume()

        requireActivity().onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                requireActivity().finish()
            }
        })

        activity?.title = "Consumos"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.searchLocalClientFragment) {
            this.findNavController().navigate(ConsumosFragmentDirections.actionConsumosFragmentToSearchLocalClientFragment())
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initPrinterManager(application: Application) {
        printerManager = PrinterManager(application, object : PrinterManager.PrinterListener {
            override fun onSuccesfulPrint(msg: String?) {
                if (msg != null) {
                    showSnackBar(msg)
                }
            }

            override fun onPrinterError(msg: String?) {
                if (msg != null) {
                    showSnackBar(msg)
                }
            }

        })
    }

    private fun showSnackBar(message: String) {
        val parentView: View = activity?.findViewById(android.R.id.content) ?: return
        val snackBar = Snackbar.make(
                parentView,
                message,
                Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }
}
