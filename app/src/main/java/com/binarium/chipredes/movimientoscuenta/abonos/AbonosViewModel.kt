package com.binarium.chipredes.movimientoscuenta.abonos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.printer.PrinterManager
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke2.Wolke2Api
import com.binarium.chipredes.wolke2.models.MovimientoCuenta
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class AbonosViewModel(private val printerManager: PrinterManager) : ViewModel() {

    var printerIp: String = ""

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _noPayments = MutableLiveData<Boolean>()
    val noPayments: LiveData<Boolean>
        get() = _noPayments

    private val _payments = MutableLiveData<List<MovimientoCuenta>>()
    val payments: LiveData<List<MovimientoCuenta>>
        get() = _payments

    private val _errorMessage = MutableLiveData<String?>()
    val errorMessage: LiveData<String?>
        get() = _errorMessage

    fun getPayments(client: LocalClientData) {
        viewModelScope.launch {
            _noPayments.value = false
            _isLoading.value = true
            _errorMessage.value = null

            try {
                val payments: List<MovimientoCuenta>? = client.idCuenta?.let {
                    Wolke2Api.retrofitService.getClientAccountTransactions(
                        client.id!!,
                        it,
                        "DC",
                        "10"
                    )
                }

                _payments.value = payments!!
                Timber.i("Abonos obtenidos! ${_payments.value}")

                // Evaluar si existieron abonos
                _noPayments.value = payments.isEmpty()
            } catch (e: Exception) {
                Timber.e(e.toString())
                Timber.e(e.localizedMessage)
                _isLoading.value = false
                _payments.value = ArrayList()
                _errorMessage.value = "Error al obtener abonos"
            }

            _isLoading.value = false
        }
    }

    fun printPayment(movimientoCuenta: MovimientoCuenta) {
        viewModelScope.launch {
            _isLoading.value = true

            withContext(Dispatchers.IO) {
                printerManager.setIpAndInit(printerIp)
                printerManager.printPayment(movimientoCuenta, false)
            }

            _isLoading.value = false
        }
    }
}
