package com.binarium.chipredes.movimientoscuenta.consumos

import android.app.Application
import androidx.lifecycle.*
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.SharedPreferencesManager
import com.binarium.chipredes.local.LocalService
import com.binarium.chipredes.local.services.GetPrintersPost
import com.binarium.chipredes.local.services.Printer
import com.binarium.chipredes.printer.PrinterManager
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke.models.QueriedClient
import com.binarium.chipredes.wolke2.Wolke2Api
import com.binarium.chipredes.wolke2.models.MovimientoCuenta
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class ConsumosViewModel(printerManager: PrinterManager, application: Application) :
    AndroidViewModel(application) {

    private val _sharedPreferencesManager = SharedPreferencesManager(application)
    private val _printerManager: PrinterManager = printerManager

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _noPurchases = MutableLiveData<Boolean>()
    val noPurchases: LiveData<Boolean>
        get() = _noPurchases

    private val _purchases = MutableLiveData<List<MovimientoCuenta>>()
    val purchases: LiveData<List<MovimientoCuenta>>
        get() = _purchases

    private var _printers: List<Printer> = listOf()

    private val _errorMessage = MutableLiveData<String?>()
    val errorMessage: LiveData<String?>
        get() = _errorMessage

    private var _localUrl: String = ""


    init {
        _noPurchases.value = false

        _localUrl = _sharedPreferencesManager.getStringFromSP(ChipREDConstants.CR_LOCAL_IP, "")
        initPrinters()
    }

    fun getPurchases(client: LocalClientData) {
        viewModelScope.launch {
            _noPurchases.value = false
            _isLoading.value = true
            _errorMessage.value = null

            try {
                val purchases: List<MovimientoCuenta>? = client.idCuenta?.let {
                    Wolke2Api.retrofitService.getClientAccountTransactions(
                        client.id!!,
                        it,
                        "VB",
                        "10"
                    )
                }

                _purchases.value = purchases!!
                Timber.i("Consumos obtenidos! ${_purchases.value}")

                // Evaluar si existieron consumos
                _noPurchases.value = purchases.isEmpty()
            } catch (e: Exception) {
                Timber.e(e.toString())
                Timber.e(e.localizedMessage)
                _isLoading.value = false
                _purchases.value = ArrayList()
                _errorMessage.value = "Error al obtener consumos"
            }

            _isLoading.value = false
        }
    }

    fun printPurchase(movimientoCuenta: MovimientoCuenta) {
        viewModelScope.launch {
            _isLoading.value = true

            val printerIp = _printers.find { printer ->
                printer.posicionCarga.toString() == movimientoCuenta.consumo?.posicionCarga
            }?.ip

            withContext(Dispatchers.IO) {
                _printerManager.setIpAndInit(printerIp)
                _printerManager.printPurchase(movimientoCuenta)
            }

            _isLoading.value = false
        }
    }

    private fun initPrinters() {
        viewModelScope.launch {
            _printers = getPrinters()
        }
    }

    private suspend fun getPrinters(): List<Printer> {
        var printers = listOf<Printer>()

        try {
            // Crear petición
            val bodyRequest = GetPrintersPost()

            // Enviar petición a Wolke
            LocalService.host = _localUrl
            val printersResponse = LocalService.retrofitService.getLocalPrinters(bodyRequest)

            // Obtener datos de respuesta
            val data = printersResponse.body()?.data
            data?.let {
                printers = data.sortedBy {
                    it.posicionCarga
                }
            }
        } catch (e: java.lang.Exception) {
            Timber.e(e.toString())
        }

        return printers
    }
}