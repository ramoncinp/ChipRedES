package com.binarium.chipredes.movimientoscuenta.abonos

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.binarium.chipredes.printer.PrinterManager

class AbonosViewModelFactory(
        private val printerManager: PrinterManager) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AbonosViewModel::class.java)) {
            return AbonosViewModel(printerManager) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}