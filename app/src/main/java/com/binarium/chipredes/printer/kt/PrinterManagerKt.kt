package com.binarium.chipredes.printer.kt

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.binarium.chipredes.R
import com.binarium.chipredes.cash.models.LastSale
import com.binarium.chipredes.configstation.data.entities.ChipRedStation
import com.binarium.chipredes.db.StationDao
import com.binarium.chipredes.printer.EscPosImageImpl
import com.binarium.chipredes.utils.DateUtils
import com.binarium.chipredes.wolke.models.LastSaleData
import com.binarium.chipredes.wolke.models.Pumper
import com.binarium.chipredes.wolke.models.tokencash.TokenData
import com.binarium.chipredes.wolke.models.tokencash.TokenPaymentsData
import com.github.anastaciocintra.escpos.EscPos
import com.github.anastaciocintra.escpos.EscPosConst
import com.github.anastaciocintra.escpos.Style
import com.github.anastaciocintra.escpos.barcode.QRCode
import com.github.anastaciocintra.escpos.image.Bitonal
import com.github.anastaciocintra.escpos.image.BitonalThreshold
import com.github.anastaciocintra.escpos.image.EscPosImage
import com.github.anastaciocintra.escpos.image.RasterBitImageWrapper
import com.github.anastaciocintra.output.TcpIpOutputStream
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject

private const val PRINTER_PAGE_WIDTH = 42

@Suppress("BlockingMethodInNonBlockingContext")
class PrinterManagerKt @Inject constructor(
    @ApplicationContext private val context: Context,
    private val chipredDao: StationDao,
) {
    var ipAddress = ""

    // Estilos
    private val normalStyle: Style by lazy {
        Style()
            .setFontSize(Style.FontSize._1, Style.FontSize._1)
            .setJustification(EscPosConst.Justification.Center)
    }

    private val mediumTitleBoldStyle: Style by lazy {
        Style()
            .setFontSize(Style.FontSize._2, Style.FontSize._2)
            .setBold(true)
            .setJustification(EscPosConst.Justification.Center)
    }

    private val titleNormalStyle: Style by lazy {
        Style()
            .setFontSize(Style.FontSize._3, Style.FontSize._3)
            .setJustification(EscPosConst.Justification.Center)
    }

    private val titleBoldStyle: Style by lazy {
        Style()
            .setFontSize(Style.FontSize._3, Style.FontSize._3)
            .setJustification(EscPosConst.Justification.Center)
            .setBold(true)
    }

    private val boldCenterStyle: Style by lazy {
        Style()
            .setFontSize(Style.FontSize._1, Style.FontSize._1)
            .setBold(true)
            .setJustification(EscPosConst.Justification.Center)
    }

    private val normalLeftStyle: Style by lazy {
        Style()
            .setFontSize(Style.FontSize._1, Style.FontSize._1)
            .setJustification(EscPosConst.Justification.Left_Default)
    }

    private val boldLeftStyle: Style by lazy {
        Style()
            .setFontSize(Style.FontSize._1, Style.FontSize._1)
            .setBold(true)
            .setJustification(EscPosConst.Justification.Left_Default)
    }

    fun printToken(tokenData: TokenData, loadingPosition: String) {
        val escPos = createConnection()
        getTokenCashImage()?.let { printImage(it, escPos) }
        escPos.feed(1)

        val pumpDesc = "Posicion de carga: $loadingPosition"
        escPos.write(boldCenterStyle, String.format(Locale.US, "%1$-42s", pumpDesc))
        escPos.feed(1)

        escPos.style = normalStyle
        escPos.write("==========================================\n")
        escPos.write("Paga con la aplicacion tokencash")
        escPos.feed(2)

        escPos.write("Monto a pagar:\n")
        escPos.style = titleNormalStyle

        escPos.write(tokenData.costo.toString())
        escPos.feed(3)

        //Imprimir código QR
        val qrCode = QRCode()
        qrCode.setJustification(EscPosConst.Justification.Center)
        qrCode.setSize(12)
        escPos.write(qrCode, tokenData.token)
        escPos.feed(1)

        //Regresar a tamaño de texto normal
        escPos.style = normalStyle

        //Imprimir valor del token
        escPos.write("Tu token es: ")
        escPos.write(tokenData.token)
        escPos.feed(2)

        escPos.write("==========================================\n")
        escPos.write("No tienes tokencash?\n Descarga la aplicacion para\n")
        escPos.write("Android o iPhone\n tokencash.mx/app\n")
        escPos.write("==========================================\n")

        escPos.feed(5)
        escPos.cut(EscPos.CutMode.FULL)

        escPos.close()
    }

    suspend fun printPurchaseWithTokenCash(
        lastSaleData: LastSaleData,
        salePumper: Pumper?,
        tokenPaymentsData: TokenPaymentsData,
        withCopy: Boolean = false,
    ) {
        val escPos = createConnection()
        var isCopy = false

        for (times in 1..if (withCopy) 2 else 1) {
            getTokenCashImage()?.let { printImage(it, escPos) }
            escPos.feed(2)

            escPos.style = boldLeftStyle
            escPos.write("DISPENSARIO ${lastSaleData.numeroDispensario}, L${lastSaleData.posicionCarga}\n")

            printTicketHeader(escPos)

            escPos.style = normalStyle
            escPos.write("==========================================\n")
            escPos.write(boldCenterStyle, "TICKET DE CONSUMO\n")

            escPos.write("==========================================\n")

            if (isCopy.not()) escPos.write(boldCenterStyle, "****** original ******\n\n")
            else escPos.write(boldCenterStyle, "****** reimpresion ******\n\n")

            escPos.style = normalStyle

            for (i in 0 until PRINTER_PAGE_WIDTH) escPos.write("=")
            escPos.feed(1)
            escPos.style = boldCenterStyle
            escPos.write("Producto   Precio  Cantidad          Total\n")
            escPos.style = normalStyle
            for (i in 0 until PRINTER_PAGE_WIDTH) escPos.write("=")
            escPos.feed(1)

            escPos.write(
                String.format(
                    Locale.US,
                    """%1$-10s %2$6.2f  %3$8.3f %4$14.2f""".trimIndent(),
                    lastSaleData.combustible,
                    lastSaleData.precioUnitario,
                    lastSaleData.cantidad,
                    lastSaleData.costo
                )
            )

            escPos.feed(1)

            escPos.write("==========================================\n")
            escPos.write(boldCenterStyle, "Pagos con tokencash fidelidad\n")

            for (tokencashPayment in tokenPaymentsData.payments) {
                var line = ""
                line += String.format(
                    Locale.US,
                    "%7.2f %3s - ",
                    tokencashPayment.details?.to?.amount?.toDouble(),
                    tokencashPayment.details?.to?.currency
                )

                line += tokencashPayment.datetime
                escPos.write(line)
                escPos.feed(1)
            }
            escPos.feed(1)


            val summaryAmount = tokenPaymentsData.summary.amount
            val summaryCurrency = tokenPaymentsData.summary.currency
            escPos.write(boldCenterStyle, "Importe recibido: ")
            escPos.write(
                """${
                    String.format(
                        Locale.US,
                        "%20.2f",
                        summaryAmount
                    )
                } $summaryCurrency"""
            )

            escPos.feed(1)
            escPos.write(boldCenterStyle, "Importe faltante: ")
            escPos.write(
                String.format(
                    Locale.US, "%20.2f",
                    lastSaleData.costo?.minus(summaryAmount!!)
                ) + " " + summaryCurrency + "\n"
            )

            escPos.feed(1)

            escPos.write("==========================================\n")

            escPos.write("GRACIAS POR SU VISITA\n")
            escPos.write(DateUtils.dateObjectToString(Date()) + "\n")

            if (salePumper != null) escPos.write("""Le atendio ${salePumper.nombre}""".trimIndent())
            escPos.feed(8)
            escPos.cut(EscPos.CutMode.FULL)

            isCopy = true
        }

        escPos.close()
    }

    private fun printImage(bitmap: Bitmap, escPos: EscPos) {
        val imageWrapper = RasterBitImageWrapper()
        imageWrapper.setJustification(EscPosConst.Justification.Center)
        val algorithm: Bitonal = BitonalThreshold(200)
        val escPosImage = EscPosImage(EscPosImageImpl(bitmap), algorithm)

        escPos.write(imageWrapper, escPosImage)
    }

    private fun getTokenCashImage(): Bitmap? {
        val logo = BitmapFactory.decodeResource(context.resources, R.drawable.token_cash_icon)
        return Bitmap.createScaledBitmap(logo, 500, 100, false)
    }

    private fun getJsmImage(): Bitmap? {
        val logo = BitmapFactory.decodeResource(context.resources, R.drawable.jsm)
        return Bitmap.createScaledBitmap(logo, 500, 100, false)
    }

    suspend fun printJsmRaffleTicket(quantity: Int, sale: LastSale) {
        val escPos = createConnection()

        for (times in 1..quantity) {
            //getJsmImage()?.let { printImage(it, escPos) }
            escPos.printJsmTicketHeader()
            escPos.write("Dispensador: ${sale.dispenser}, L${sale.loadingPosition}\n")
            escPos.write("Venta: ${sale.ticketNumber}\n")

            escPos.printSeparator()
            escPos.feed(1)

            escPos.style = normalLeftStyle
            escPos.write("JSM te trae la promo que promete\n")
            escPos.write("Sorteo 14/02/2025\n")
            escPos.write("31 vehiculos Totalmente Nuevos!\n")
            escPos.write("2 sorteos de c40 000 Cada Quincena!\n")
            escPos.write("Por la compra minima de c5000 participas\n")
            escPos.feed(1)

            escPos.style = normalStyle
            escPos.write("Nombre: __________________________________")
            escPos.feed(1)
            escPos.write("Cédula: __________________________________")
            escPos.feed(1)
            escPos.write("Teléfono: ________________________________")
            escPos.feed(2)

            escPos.printSeparator()
            escPos.feed(1)
            escPos.style = normalStyle
            escPos.write("*Aplican restricciones\n")
            escPos.write("Ver reglamento en los servicentros\n")
            escPos.write("GRACIAS POR SU VISITA\n")
            escPos.write("${sale.id}\n")
            escPos.write(DateUtils.dateObjectToString(Date()) + "\n")

            escPos.feed(6)
            escPos.cut(EscPos.CutMode.FULL)
        }

        escPos.close()
    }

    private suspend fun printTicketHeader(escPos: EscPos) {
        val station = getStationData()
        val textData = StringBuilder()

        escPos.style = normalLeftStyle

        textData.append(station.nombreEstacion)
        textData.append("\n")

        textData.append(station.direccion.calle)
        textData.append(" ")
        textData.append(station.direccion.numeroEXT)
        textData.append("\n")

        textData.append("RFC     : ")
        textData.append(station.rfcCedula)
        textData.append("\n")

        textData.append("TELEFONO: ")
        textData.append(station.telefono.numero)
        textData.append("\n")

        textData.append("EMAIL   : ")
        textData.append(station.email)
        textData.append("\n")

        escPos.write(textData.toString())
    }

    private suspend fun EscPos.printJsmTicketHeader() {
        val station = getStationData()
        val textData = StringBuilder()

        style = boldCenterStyle
        textData.append(station.nombreEstacion)
        textData.append("\n")
        write(textData.toString())
        textData.clear()

        style = normalLeftStyle
        textData.append("Email   : ")
        textData.append(station.email)
        textData.append("\n")

        textData.append("Teléfono: ")
        textData.append(station.telefono.numero)
        textData.append("\n")

        write(textData.toString())
    }

    private fun EscPos.printSeparator() {
        style = boldCenterStyle
        for (i in 0 until PRINTER_PAGE_WIDTH) write("=")
    }

    private fun createConnection(): EscPos {
        val outputStream = TcpIpOutputStream(ipAddress)
        return EscPos(outputStream)
    }

    private suspend fun getStationData(): ChipRedStation = withContext(Dispatchers.IO) {
        chipredDao.getStation()[0]
    }
}
