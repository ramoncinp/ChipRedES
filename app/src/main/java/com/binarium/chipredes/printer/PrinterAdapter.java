package com.binarium.chipredes.printer;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.binarium.chipredes.R;

import java.util.ArrayList;

public class PrinterAdapter extends RecyclerView.Adapter<PrinterAdapter
        .PrinterViewHolder> implements View.OnClickListener, View.OnTouchListener
{
    private ArrayList<Printer> printers;
    private Context context;
    private View.OnClickListener listener;

    public PrinterAdapter(ArrayList<Printer> printers, Context context)
    {
        this.printers = printers;
        this.context = context;
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    @NonNull
    public PrinterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.printer_device_row,
                parent, false);

        PrinterViewHolder printerViewHolder = new PrinterViewHolder(v);
        v.setOnClickListener(this);
        v.setOnTouchListener(this);

        return printerViewHolder;
    }

    void addPrinter(Printer printer)
    {
        printers.add(printer);
    }

    void setPrinters(ArrayList<Printer> printers)
    {
        this.printers = printers;
    }

    ArrayList<Printer> getPrinters()
    {
        return printers;
    }

    @Override
    public void onBindViewHolder(@NonNull PrinterViewHolder holder, int position)
    {
        Printer printer = printers.get(position);

        holder.description.setText(printer.getDeviceName());
        holder.ipAddress.setText(printer.getCurrentIp());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent)
    {
        //Obtener views
        TextView desc = view.findViewById(R.id.printer_description_tv);
        TextView ip = view.findViewById(R.id.printer_ip_address_tv);
        ImageView icon = view.findViewById(R.id.printer_icon);
        ConstraintLayout layout = view.findViewById(R.id.printer_layout);

        switch (motionEvent.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                //Cambiar views de color
                desc.setTextColor(ContextCompat.getColor(context, R.color.white));
                ip.setTextColor(ContextCompat.getColor(context, R.color.white));
                icon.setImageResource(R.drawable.ic_print_white);
                layout.setBackgroundResource(R.color.colorAccent);
                return true;

            case MotionEvent.ACTION_UP:
                //Regresar views a su color normal
                desc.setTextColor(ContextCompat.getColor(context, R.color.black));
                ip.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                icon.setImageResource(R.drawable.ic_printer);
                layout.setBackgroundResource(R.color.white);
                //Dar click
                view.performClick();
                return true;

            case MotionEvent.ACTION_CANCEL:
                //Regresar views a su color normal
                desc.setTextColor(ContextCompat.getColor(context, R.color.black));
                ip.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                icon.setImageResource(R.drawable.ic_printer);
                layout.setBackgroundResource(R.color.white);
                return true;
        }
        return true;
    }

    @Override
    public int getItemCount()
    {
        return printers.size();
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null) listener.onClick(view);
    }

    static class PrinterViewHolder extends RecyclerView.ViewHolder
    {
        private TextView description;
        private TextView ipAddress;

        PrinterViewHolder(View itemView)
        {
            super(itemView);

            description = itemView.findViewById(R.id.printer_description_tv);
            ipAddress = itemView.findViewById(R.id.printer_ip_address_tv);
        }
    }
}
