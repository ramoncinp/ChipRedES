package com.binarium.chipredes.printer;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.R;
import com.binarium.chipredes.tokencash.repository.TokenCashManager;
import com.epson.epsonio.DevType;
import com.epson.epsonio.EpsonIoException;
import com.epson.epsonio.FilterOption;
import com.epson.epsonio.Finder;
import com.epson.epsonio.IoStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static com.epson.epsonio.Finder.getDeviceInfoList;

public class PrintersDialogFragment extends DialogFragment {
    //Constantes
    private static final String TAG = PrintersDialogFragment.class.getSimpleName();

    //Views
    private ConstraintLayout contentLayout;
    private LinearLayout buttonsLayout;
    private ProgressBar progressBar;
    private RecyclerView printersList;
    private TextView noPrinters;
    private TextView selectPrinter;

    //Objetos
    private Activity activity;
    private ArrayList<Printer> savedPrinters = new ArrayList<>();
    private Set<String> savedPrintersIps = new HashSet<>();
    private ChipRedManager chipRedManager;
    private PrinterAdapter printerAdapter;

    //Interfaces
    private PrinterDialogInterface printerDialogInterface;

    public static PrintersDialogFragment newInstance(PrinterDialogInterface listener) {
        PrintersDialogFragment fragment = new PrintersDialogFragment();
        fragment.setPrinterDialogInterface(listener);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        //Obtener layout
        View view = inflater.inflate(R.layout.dialog_fragment_printers, null);

        //Referenciar views
        contentLayout = view.findViewById(R.id.content_layout);
        buttonsLayout = view.findViewById(R.id.buttons_layout);
        noPrinters = view.findViewById(R.id.no_printers_text);
        progressBar = view.findViewById(R.id.progress_bar);
        printersList = view.findViewById(R.id.printers_list);
        selectPrinter = view.findViewById(R.id.select_printer_text);

        //Inicializar adaptador
        printerAdapter = new PrinterAdapter(new ArrayList<Printer>(), getContext());

        //Asignar adapter a recyclerView
        printersList.setAdapter(printerAdapter);
        printersList.setLayoutManager(new LinearLayoutManager(getContext()));

        //Asignar listener
        printerAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                //Obtener impresora seleccionada
                Printer selectedPrinter =
                        printerAdapter.getPrinters().get(printersList.getChildAdapterPosition(v));

                //Retornar la ip a través de la interfaz
                printerDialogInterface.onIpSelected(selectedPrinter.getCurrentIp());
            }
        });

        //Agregar listener a botones
        Button searchButton = view.findViewById(R.id.search_button);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scannPrinters();
                showProgressBar();
            }
        });

        Button returnButton = view.findViewById(R.id.return_button);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Obtener instancia del a actividad padre
        activity = getActivity();

        //Obtener impresoras
        getSavedPrinters();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private void showContent() {
        contentLayout.setVisibility(View.VISIBLE);
        buttonsLayout.setVisibility(View.VISIBLE);
        printersList.setVisibility(View.VISIBLE);
        selectPrinter.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        noPrinters.setVisibility(View.GONE);
    }

    public void showProgressBar() {
        contentLayout.setVisibility(View.GONE);
        buttonsLayout.setVisibility(View.GONE);
        printersList.setVisibility(View.GONE);
        selectPrinter.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        noPrinters.setVisibility(View.GONE);
    }

    private void showNoPrinters() {
        contentLayout.setVisibility(View.VISIBLE);
        buttonsLayout.setVisibility(View.VISIBLE);
        noPrinters.setVisibility(View.VISIBLE);
        selectPrinter.setVisibility(View.GONE);
        printersList.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    private void initChipRedManager() {
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                Toast.makeText(activity, crMessage, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                parseSavedPrinters(response);
            }
        }, getActivity());
    }

    private void scannPrinters() {
        //Vaciar lista de impresoras
        printerAdapter.setPrinters(new ArrayList<Printer>());
        printersList.setVisibility(View.VISIBLE);
        noPrinters.setVisibility(View.GONE);
        Thread searchPrinters = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    search();
                    Thread.sleep(1000);
                    getResult();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    stopDiscovery();
                }
            }
        });
        searchPrinters.start();
    }

    private void search() {
        int errStatus = IoStatus.SUCCESS;

        try {
            Finder.start(activity, DevType.TCP, "255.255.255.255");
        } catch (EpsonIoException e) {
            errStatus = e.getStatus();
        }

        if (errStatus == IoStatus.SUCCESS) {
            Log.d(TAG, "Iniciando escaneo...");
        } else {
            Log.d(TAG, "Hubo un problema al escanear");
        }
    }

    private void parseSavedPrinters(JSONObject data) {
        savedPrintersIps.clear();
        savedPrinters.clear();

        if (ChipREDConstants.getTokencashScenario(getActivity()).equals("e3")) {
            parseResponse(data, "printers");
        } else {
            parseResponse(data, "data");
        }

        // Actualizar adaptador
        printerAdapter.setPrinters(savedPrinters);
        printerAdapter.notifyDataSetChanged();

        // Evaluar cantidad de impresoras obtenidas
        if (printerAdapter.getPrinters().size() != 0) {
            showContent();
        } else {
            showNoPrinters();
        }
    }

    private void parseResponse(JSONObject data, String arrayKey) {
        try {
            // Obtener arreglo de impresoras
            JSONArray printersArray = data.getJSONArray(arrayKey);

            // Iterar en arreglo JSON
            for (int printerIdx = 0; printerIdx <= printersArray.length(); printerIdx++) {
                // Obtener objeto JSON del índice actual
                JSONObject printerObj = printersArray.getJSONObject(printerIdx);

                // Obtener ip de impresora del indice actual
                String ipAddress = printerObj.getString("ip");

                // Verificar que no exista la ip de la impresora obtenida
                if (!savedPrintersIps.contains(ipAddress)) {
                    // Crear impresora
                    Printer newPrinter = new Printer();
                    newPrinter.setCurrentIp(ipAddress);
                    newPrinter.setDeviceName("TM-T20II");

                    // Agregar a la lista
                    savedPrinters.add(newPrinter);
                    savedPrintersIps.add(ipAddress);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void stopDiscovery() {
        try {
            Finder.stop();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (printerAdapter.getPrinters().size() != 0) {
                        showContent();
                    } else {
                        showNoPrinters();
                    }
                }
            });
        } catch (EpsonIoException e) {
            e.printStackTrace();
            Log.d(TAG, "Error al detener escaneo");
        }
    }

    private void getResult() {
        com.epson.epsonio.DeviceInfo[] mList;

        //Get device list
        try {
            mList = getDeviceInfoList(FilterOption.PARAM_DEFAULT);
            for (com.epson.epsonio.DeviceInfo deviceInfo : mList) {
                Printer printer = new Printer();
                printer.setDeviceName(deviceInfo.getPrinterName());
                printer.setCurrentIp(deviceInfo.getIpAddress());
                printerAdapter.addPrinter(printer);
                Log.d(TAG, "Nueva impresora añadida");
            }
        } catch (EpsonIoException e) {
            Log.e(TAG, "Error al obtener impresora");
            Log.e(TAG, e.toString());
        } catch (NullPointerException e) {
            Log.e(TAG, "Error al obtener impresoras");
            Log.e(TAG, e.toString());
        }

        stopDiscovery();
    }

    private void getSavedPrinters() {
        // Evaluar si esta definida la estación como de E3 Tokencash
        if (ChipREDConstants.getTokencashScenario(getActivity()).equals("e3")) {
            // Obtener impresoras de microservicio de tokencash
            TokenCashManager.getPrinters(activity, new TokenCashManager.onMessageReceived() {
                @Override
                public void showResponse(JSONObject response) {
                    parseSavedPrinters(response);
                }

                @Override
                public void onError(String message) {
                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            // Obtener impresoras de servidor local
            initChipRedManager();
            chipRedManager.getPrinters();
        }
    }

    public void setPrinterDialogInterface(PrinterDialogInterface printerDialogInterface) {
        this.printerDialogInterface = printerDialogInterface;
    }

    public interface PrinterDialogInterface {
        void onIpSelected(String ip);
    }
}
