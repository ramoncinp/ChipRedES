package com.binarium.chipredes.printer.domain.usecases

import com.binarium.chipredes.cash.models.LastSale
import com.binarium.chipredes.printer.kt.PrinterManagerKt
import javax.inject.Inject

class PrintRaffleTicketUseCase @Inject constructor(
    private val getLocalPrinterUseCase: GetLocalPrinterUseCase,
    private val printerManagerKt: PrinterManagerKt
) {

    suspend operator fun invoke(quantity: Int, lastSale: LastSale): Boolean {
        val printer = getLocalPrinterUseCase.invoke(lastSale.loadingPosition)
        return try {
            printer?.let {
                with(printerManagerKt) {
                    ipAddress = it.ip.orEmpty()
                    printJsmRaffleTicket(quantity, lastSale)
                    true
                }
            } ?: false
        } catch (e: Exception) {
            false
        }
    }
}
