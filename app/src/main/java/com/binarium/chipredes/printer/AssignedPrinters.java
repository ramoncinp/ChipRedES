package com.binarium.chipredes.printer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.R;
import com.binarium.chipredes.tokencash.repository.TokenCashManager;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AssignedPrinters extends AppCompatActivity
{
    //Constantes
    private static final String TAG = AssignedPrinters.class.getSimpleName();

    //Objetos
    private ArrayList<Printer> printerArrayList = new ArrayList<>();
    private ChipRedManager chipRedManager;

    //Views
    private RecyclerView printers;
    private TextView noPrinters;
    private ProgressBar progressBar;
    private Button save;
    private Button close;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned_printers);
        setTitle("Impresoras asignadas");

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        printers = findViewById(R.id.assigned_pumps_to_printers_list);
        noPrinters = findViewById(R.id.no_assigned_printers);
        progressBar = findViewById(R.id.progress_bar);
        save = findViewById(R.id.save_pcs_button);
        close = findViewById(R.id.close_pcs_button);

        save.setTextSize(TypedValue.COMPLEX_UNIT_SP,
                getResources().getInteger(R.integer.normal_screen_text_size));
        close.setTextSize(TypedValue.COMPLEX_UNIT_SP,
                getResources().getInteger(R.integer.normal_screen_text_size));

        save.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Guardar cambios
                JSONArray array;

                // Obtener json de impresoras dependiendo del escenario tokencash
                if (ChipREDConstants.getTokencashScenario(AssignedPrinters.this).equals("e3"))
                {
                    array = PrinterUtils.PrinterListToE3JSONArray(printerArrayList);
                    if (array != null) updatePrinters(array);
                    else
                        Toast.makeText(AssignedPrinters.this, "Error al actualizar impresoras",
                                Toast.LENGTH_SHORT).show();
                }
                else
                {
                    array = PrinterUtils.PrinterListToJSONArray(printerArrayList);

                    // Definir impresoras
                    chipRedManager.setPrinters(array);
                }

                // Cerrar actividad
                finish();
            }
        });

        close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Cerrar actividad
                finish();
            }
        });

        // Obtener impresoras
        getPrinters();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showPrintersList(JSONObject response)
    {
        printerArrayList = PrinterUtils.parsePrinters(response);
        if (printerArrayList == null)
        {
            showErrorDialog("Lista de impresoras", "Error al procesar la información");
            return;
        }

        printers.setLayoutManager(new LinearLayoutManager(this));
        printers.setHasFixedSize(true);

        //Crear adaptador
        PrinterMasterAdapter printerMasterAdapter = new PrinterMasterAdapter(printerArrayList,
                this, new PrinterMasterAdapter.PrinterMasterInterface()
        {
            @Override
            public void onRemoveItemRequest(int itemNumber)
            {
                Log.d("Pump", "Remove pump " + itemNumber);
                final Context context = AssignedPrinters.this;

                if (ChipREDConstants.getTokencashScenario(context).equals("e3"))
                {
                    TokenCashManager.removePrinter(context, String.valueOf(itemNumber),
                            new TokenCashManager.onMessageReceived()
                            {
                                @Override
                                public void showResponse(JSONObject response)
                                {
                                    showSnackbarMessage("Impresora eliminada correctamente");
                                }

                                @Override
                                public void onError(String message)
                                {
                                    showSnackbarMessage(message);
                                }
                            });
                }
                else
                {
                    chipRedManager.deletePrinter(itemNumber);
                }
            }
        });

        printers.setAdapter(printerMasterAdapter);
        printers.setVisibility(View.VISIBLE);
        save.setVisibility(View.VISIBLE);
        close.setVisibility(View.VISIBLE);
    }

    private void showErrorDialog(String title, String text)
    {
        GenericDialog genericDialog = new GenericDialog(title, text, new Runnable()
        {
            @Override
            public void run()
            {
                finish();
            }
        }, null, AssignedPrinters.this);

        genericDialog.setPositiveText("Ok");
        genericDialog.show();
    }

    private void getPrinters()
    {
        // Evaluar modo de trabajo
        if (ChipREDConstants.getTokencashScenario(this).equals("e3"))
        {
            TokenCashManager.getPrinters(this, new TokenCashManager.onMessageReceived()
            {
                @Override
                public void showResponse(JSONObject response)
                {
                    // Ajustar respuesta a la definida por los métodos de la aplicación
                    JSONObject printersResponse = PrinterUtils.convertE3PrintersToNormalPrinters(response);

                    // Mostrar impresoras
                    showPrintersList(printersResponse);

                    // Ocultar progressBar
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError(String message)
                {
                    showErrorDialog("Error", message);
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
        else
        {
            //Inicializar ChipRedManager
            chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
            {
                @Override
                public void chipRedMessage(String crMessage, int webServiceN)
                {
                    if (webServiceN == ChipRedManager.GET_PRINTERS)
                    {
                        noPrinters.setText(crMessage);
                        noPrinters.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                    else if (webServiceN == ChipRedManager.SET_PRINTERS || webServiceN == ChipRedManager.REMOVE_PRINTER)
                    {
                        Toast.makeText(AssignedPrinters.this, crMessage, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void chipRedError(String errorMessage, int webServiceN)
                {
                    if (webServiceN == ChipRedManager.GET_PRINTERS || webServiceN == ChipRedManager
                            .SET_PRINTERS || webServiceN == ChipRedManager.REMOVE_PRINTER)
                    {
                        showErrorDialog("Error", errorMessage);
                        progressBar.setVisibility(View.GONE);
                    }
                }

                @Override
                public void showResponse(JSONObject response, int webServiceN)
                {
                    if (webServiceN == ChipRedManager.GET_PRINTERS)
                    {
                        showPrintersList(response);
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }, this);

            //Obtener lista de impresoras
            chipRedManager.getPrinters();
        }
    }

    private void updatePrinters(JSONArray printers)
    {
        TokenCashManager.onMessageReceived listener = new TokenCashManager.onMessageReceived()
        {
            @Override
            public void showResponse(JSONObject response)
            {
                Toast.makeText(AssignedPrinters.this, "Impresora actualizada",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String message)
            {
                Toast.makeText(AssignedPrinters.this, message, Toast.LENGTH_SHORT).show();
            }
        };

        try
        {
            for (int idx = 0; idx < printers.length(); idx++)
            {
                JSONObject printer = printers.getJSONObject(idx);
                TokenCashManager.updatePrinter(this, printer, listener);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Error al actualizar impresoras", Toast.LENGTH_SHORT).show();
        }
    }

    private void showSnackbarMessage(String text)
    {
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG).show();
    }
}
