package com.binarium.chipredes.printer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.binarium.chipredes.configstation.StationManager;
import com.binarium.chipredes.tokencash.repository.TokenCashManager;

import com.binarium.chipredes.comparators.PumpComparator;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.LoadingPosition;
import com.binarium.chipredes.adapters.LoadingPositionAdapter;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.dispenser.Pump;
import com.epson.epsonio.DevType;
import com.epson.epsonio.EpsonIoException;
import com.epson.epsonio.FilterOption;
import com.epson.epsonio.Finder;
import com.epson.epsonio.IoStatus;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import static com.epson.epsonio.Finder.getDeviceInfoList;

public class AssignPrinter extends AppCompatActivity
{
    //Constantes
    private static final String TAG = AssignPrinter.class.getSimpleName();
    private static final int ASSIGN_PC_TO_PRINTER_DIALOG = 1;
    private static final int ASSIGN_PC_TO_DEVICE_DIALOG = 2;

    //Variables
    private int selectedPrinterIdx;
    private int showedDialog;
    private String selectedPrinterIpString;
    private String tokencashWorkMode;

    //Vistas
    private Button discoverPrinters;
    private Button testPrinter;
    private ProgressBar discovering;
    private RecyclerView printerList;
    private MaterialEditText selectedPrinterIp;
    private TextView noPrinters;
    private Dialog assignPcsDialog;
    private RecyclerView loadingPositionsList;

    //Objetos
    private PrinterManager printerManager;
    private PrinterAdapter printerAdapter;
    private ChipRedManager chipRedManager;
    private StationManager stationManager;

    //Listas
    private ArrayList<Printer> assignedPrinters = new ArrayList<>();
    private HashSet<Integer> selectedPositions = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_printer);
        setTitle("Asignar impresora a posiciones de carga");

        // Mostrar flecha para salir de actividad
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        //Definir views
        discoverPrinters = findViewById(R.id.discovery_button);
        testPrinter = findViewById(R.id.test_printer);
        discovering = findViewById(R.id.printer_list_progress);
        printerList = findViewById(R.id.printer_list);
        selectedPrinterIp = findViewById(R.id.printer_ip);
        noPrinters = findViewById(R.id.no_printers);

        //Escribir la ip almacenada actualmente
        selectedPrinterIp.setText(getSavedPrinterIp());

        //Definir listeners de views
        discoverPrinters.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                scannPrinters();
            }
        });

        testPrinter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyboard();
                showSnackbarMessage("Imprimiendo...");
                new Thread(() -> {
                    printerManager.setIpAndInit(selectedPrinterIp.getText().toString());
                    printerManager.printTest();
                }).start();
            }
        });

        CardView assignPumpsToPrinter = findViewById(R.id.assign_pumps_to_printer);
        assignPumpsToPrinter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                evaluateSelectedPrinter();
            }
        });

        //Inicializar Adapter
        printerAdapter = new PrinterAdapter(new ArrayList<Printer>(), this);
        printerAdapter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Obtener objeto seleccionado
                Printer printer = printerAdapter.getPrinters().get(printerList
                        .getChildAdapterPosition(v));

                //Escribir ip en editText
                selectedPrinterIp.setText(printer.getCurrentIp());
            }
        });

        //Iniciar printer manager
        printerManager = new PrinterManager(this, new PrinterManager.PrinterListener()
        {
            @Override
            public void onSuccesfulPrint(final String msg)
            {
                Log.d("Printer", msg);
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        showSnackbarMessage(msg);
                    }
                });
            }

            @Override
            public void onPrinterError(final String msg)
            {
                Log.d("Printer", msg);
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        showSnackbarMessage(msg);
                    }
                });
            }

        });

        //Inicializar ChipREDManager
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                if (webServiceN == ChipRedManager.GET_PRINTERS)
                {
                    //No hay impresoras
                    stationManager.getGeneralData(null);
                }
                showSnackbarMessage(crMessage);
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                if (webServiceN == ChipRedManager.GET_PCS) notifyLoadingPositionsError();
                else if (webServiceN == ChipRedManager.SET_PRINTERS || webServiceN ==
                        ChipRedManager.GET_PRINTERS)
                {
                    showSnackbarMessage(errorMessage);
                }
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                if (webServiceN == ChipRedManager.GET_PCS)
                {
                    try
                    {
                        parseLoadingPositions(response.getJSONArray("data"));
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
                else //Ya se obtuvieron las impresoras, ahora pedir posiciones de carga
                {
                    //Obtener la lista de impresoras
                    assignedPrinters = PrinterUtils.parsePrinters(response);
                    stationManager.getGeneralData(null);
                }
            }
        }, this);

        // Inicializar StationManager (Que se usará para obtener posiciones de carga desde la nube)
        stationManager = new StationManager(this, new StationManager.OnResponse()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                Log.e(TAG, crMessage);
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                Log.e(TAG, errorMessage);
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                try
                {
                    parseLoadingPositions(response.getJSONObject("data").getJSONObject(
                            "datos_generales").getJSONArray("dispensarios"));
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        });

        //Asignar a recycler view el adaptador
        printerList.setAdapter(printerAdapter);
        printerList.setLayoutManager(new LinearLayoutManager(this));
        printerList.setHasFixedSize(true);

        //Buscar impresoras
        scannPrinters();

        //Obtener modo de trabajo
        tokencashWorkMode = ChipREDConstants.getTokencashScenario(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.printers_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int itemId = item.getItemId();
        if (itemId == R.id.printers_list) {
            Intent intent = new Intent(AssignPrinter.this, AssignedPrinters.class);
            startActivity(intent);
        } else if (itemId == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    private void savePrinterIp()
    {
        hideKeyboard();

        SharedPreferencesManager.putString(this, ChipREDConstants.PRINTER_IP_ADDR,
                selectedPrinterIp.getText().toString());
    }

    private String getSavedPrinterIp()
    {
        return SharedPreferencesManager.getString(this, ChipREDConstants.PRINTER_IP_ADDR, "");
    }

    private void showSnackbarMessage(String text)
    {
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG).show();
    }

    public void hideKeyboard()
    {
        try
        {
            View kb = this.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) (this.getSystemService(Context
                    .INPUT_METHOD_SERVICE));
            if (imm != null && kb != null)
            {
                imm.hideSoftInputFromWindow(kb.getWindowToken(), 0);
            }
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
    }

    private void scannPrinters()
    {
        //Iniciar discovery
        discovering.setVisibility(View.VISIBLE);
        discoverPrinters.setText("Buscando...");
        discoverPrinters.setBackground(AssignPrinter.this.getResources().getDrawable(R
                .drawable.round_corner_color_accent_button));

        //Vaciar lista de impresoras
        printerAdapter.setPrinters(new ArrayList<Printer>());

        printerList.setVisibility(View.VISIBLE);
        noPrinters.setVisibility(View.GONE);
        discoverPrinters.setEnabled(false);
        Thread searchPrinters = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    search();
                    Thread.sleep(2500);
                    getResult();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        });
        searchPrinters.start();
    }

    private void search()
    {
        int errStatus = IoStatus.SUCCESS;

        try
        {
            Finder.start(getBaseContext(), DevType.TCP, "255.255.255.255");
        }
        catch (EpsonIoException e)
        {
            errStatus = e.getStatus();
        }

        if (errStatus == IoStatus.SUCCESS)
        {
            Log.d("Printer", "Iniciando escaneo...");
        }
        else
        {
            Log.d("Printer", "Hubo un problema al escanear");
        }
    }

    private void stopDiscovery()
    {
        try
        {
            Finder.stop();
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    discovering.setVisibility(View.GONE);
                    discoverPrinters.setEnabled(true);
                    discoverPrinters.setText("Buscar");
                    discoverPrinters.setBackground(AssignPrinter.this.getResources().getDrawable
                            (R.drawable.round_corner_color_primary_button));
                    if (printerAdapter.getPrinters().size() != 0)
                    {
                        printerAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        printerList.setVisibility(View.GONE);
                        noPrinters.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
        catch (EpsonIoException e)
        {
            e.printStackTrace();
            Log.d("Printer", "Error al detener escaneo");
        }
    }

    private void getResult()
    {
        com.epson.epsonio.DeviceInfo[] mList;

        //Get device list
        try
        {
            mList = getDeviceInfoList(FilterOption.PARAM_DEFAULT);
            for (com.epson.epsonio.DeviceInfo deviceInfo : mList)
            {
                Printer printer = new Printer();
                printer.setDeviceName(deviceInfo.getPrinterName());
                printer.setCurrentIp(deviceInfo.getIpAddress());
                printerAdapter.addPrinter(printer);
                Log.d("Printer", "Nueva impresora añadida");
            }
        }
        catch (EpsonIoException e)
        {
            Log.d("Printer", "Error al obtener impresora");
            Log.d("Printer", e.toString());
            stopDiscovery();
        }
        catch (NullPointerException e)
        {
            stopDiscovery();
        }

        stopDiscovery();
    }

    private void evaluateSelectedPrinter()
    {
        selectedPrinterIpString = selectedPrinterIp.getText().toString();
        if (!selectedPrinterIpString.isEmpty())
        {
            // Validar modo de trabajo
            if (tokencashWorkMode.equals("e3"))
            {
                showAssignOptions();
            }
            else
            {
                // Mostrar diálogo para asignar posiciones de carga a impresora seleccionada
                showAssignPcsToPrinterDialog(selectedPrinterIpString);
            }
        }
        else
        {
            showSnackbarMessage("Seleccione una impresora");
        }
    }

    private void showAssignPcsToPrinterDialog(String selectedIp)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View content = getLayoutInflater().inflate(R.layout.dialog_assign_pc_to_printer, null);

        //Obtener views
        TextView selectedIpTv = content.findViewById(R.id.selected_printer_ip);
        Button assignPcsButton = content.findViewById(R.id.assign_pcs_button);
        assignPcsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                assignPcsDialog.dismiss();
                showSnackbarMessage("Asignando posiciones a impresora...");
                assignLoadingPositionsToPrinter();
            }
        });

        Button cancelPcsButton = content.findViewById(R.id.cancel_pcs_button);
        cancelPcsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                assignPcsDialog.dismiss();
            }
        });

        //Setear texto de la ip seleccionada
        selectedIpTv.setText(selectedIp);

        //Asignar view a dialogo
        builder.setView(content);

        // Pedir impresoras
        selectedPositions.clear();
        if (ChipREDConstants.getTokencashScenario(this).equals("e3"))
        {
            TokenCashManager.getPrinters(this, new TokenCashManager.onMessageReceived()
            {
                @Override
                public void showResponse(JSONObject response)
                {
                    // Convertir impresoras obtenidas de micro servicio
                    PrinterUtils.convertE3PrintersToNormalPrinters(response);

                    // Obtener impresoras
                    assignedPrinters = PrinterUtils.parsePrinters(response);

                    // Pedir posiciones de carga
                    stationManager.getGeneralData(null);
                }

                @Override
                public void onError(String message)
                {
                    showSnackbarMessage("Error al obtener lista de impresoras");

                    // Pedir posiciones de carga
                    stationManager.getGeneralData(null);
                }
            });
        }
        else
        {
            //Pedir posiciones de carga
            chipRedManager.getPrinters();
        }

        assignPcsDialog = builder.create();
        assignPcsDialog.show();
        showedDialog = ASSIGN_PC_TO_PRINTER_DIALOG;
    }

    private void showAssignPcsToDeviceDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View content = getLayoutInflater().inflate(R.layout.dialog_assign_pc_to_device, null);

        //Obtener views
        Button assignPcsButton = content.findViewById(R.id.assign_pcs_button);
        assignPcsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                assignPcsDialog.dismiss();
                showSnackbarMessage("Asignando posiciones a dispositivo...");
                assignPcsToDevice();
            }
        });

        Button cancelPcsButton = content.findViewById(R.id.cancel_pcs_button);
        cancelPcsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                assignPcsDialog.dismiss();
            }
        });

        //Asignar view a dialogo
        builder.setView(content);

        // Pedir posiciones de carga
        stationManager.getGeneralData(null);

        assignPcsDialog = builder.create();
        assignPcsDialog.show();
        showedDialog = ASSIGN_PC_TO_DEVICE_DIALOG;
    }

    private void parseLoadingPositions(JSONArray loadingPostions)
    {
        //Obtener views
        loadingPositionsList = assignPcsDialog.findViewById(R.id.select_pcs_grid_view);
        RelativeLayout progress = assignPcsDialog.findViewById(R.id.progress_layout);
        LinearLayout buttons = assignPcsDialog.findViewById(R.id.buttons_layout);

        //Ocultar progress
        progress.setVisibility(View.GONE);

        //Crear variables necesarias
        boolean selectedPrinterInDataBase = false;
        ArrayList<Integer> printerPositions = new ArrayList<>();
        Set<String> selectedPositionsForDevice = new HashSet<>();

        //Evaluar cual diálogo se esta mostrando
        if (showedDialog == ASSIGN_PC_TO_PRINTER_DIALOG)
        {
            //Buscar la impresora en base de datos para saber si ya fue registrada
            selectedPrinterIdx = PrinterUtils.indexOfPrinter(assignedPrinters,
                    selectedPrinterIpString);
            if (selectedPrinterIdx != -1)
            {
                //Si sí fue registrada, hacer la variable booleana verdadera y hacer un arreglo de
                // enteros
                //De las posiciones asignadas a la impresora seleccionada
                selectedPrinterInDataBase = true;
                for (Pump pump : assignedPrinters.get(selectedPrinterIdx).getPumps())
                {
                    printerPositions.add(pump.getNumber());
                }
            }
        }
        else
        {
            // Obtener lista de posiciones de carga almacenadas para el dispositivo
            selectedPositionsForDevice = SharedPreferencesManager.getStringSet(this,
                    ChipREDConstants.ASSIGNED_PCS_TO_DEVICE);
        }

        //Crear lista de posiciones de carga
        final ArrayList<LoadingPosition> positions = new ArrayList<>();

        //Obtener posiciones de carga del servicio
        try
        {
            for (int i = 0; i < loadingPostions.length(); i++)
            {
                int dispenserNumber;

                JSONObject dispJsonObject = loadingPostions.getJSONObject(i);
                dispenserNumber = dispJsonObject.getInt("numero_dispensario");

                JSONArray dispSides = dispJsonObject.getJSONArray("lados");
                for (int y = 0; y < dispSides.length(); y++)
                {
                    JSONObject dispSide = dispSides.getJSONObject(y);
                    int pumpNumber = dispSide.getInt("lado_estacion");
                    LoadingPosition loadingPosition = new LoadingPosition(pumpNumber);
                    loadingPosition.setDispenserNumber(dispenserNumber);
                    loadingPosition.setCmIp(dispSide.getString("cm_ip"));
                    loadingPosition.setCmPort(dispSide.getInt("cm_puerto"));
                    loadingPosition.setCmState(dispSide.getString("cm_estado"));
                    loadingPosition.setSide(dispSide.getInt("lado"));
                    loadingPosition.setSideTag(dispSide.getString("lado_tag"));
                    loadingPosition.setState(dispSide.getInt("estado"));

                    if (showedDialog == ASSIGN_PC_TO_PRINTER_DIALOG)
                    {
                        if (selectedPrinterInDataBase)
                        {
                            if (printerPositions.contains(pumpNumber))
                            {
                                //Asignar variable que dice que ha sido asignada a la impresora
                                loadingPosition.setSelected(true);

                                //Agregar a lista de posiciones de carga seleccionadas
                                selectedPositions.add(loadingPosition.getStationSide());
                            }
                        }
                    }
                    else
                    {
                        // Obtener posicion actual
                        String pcStr = String.valueOf(loadingPosition.getStationSide());

                        // Evaluar si se contiene la posicion de carga
                        if (selectedPositionsForDevice.contains(pcStr))
                        {
                            // Definir como seleccionada
                            loadingPosition.setSelected(true);
                        }
                    }

                    if (dispSide.has("lado_estacion_usuario"))
                    {
                        loadingPosition.setUserStationSide(dispSide.getInt("lado_estacion_usuario"
                        ));
                    }

                    positions.add(loadingPosition);
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Log.d("Impresoras", e.getMessage() + e.getCause());
        }

        // Ordenar posiciones de carga
        Collections.sort(positions, new Comparator<LoadingPosition>()
        {
            @Override
            public int compare(LoadingPosition loadingPosition, LoadingPosition t1)
            {
                return Integer.compare(loadingPosition.getStationSide(), t1.getStationSide());
            }
        });

        Collections.sort(positions, new PumpComparator());

        LoadingPositionAdapter loadingPositionAdapter = new LoadingPositionAdapter(this,
                positions, LoadingPositionAdapter.WHITE_THEME);
        loadingPositionAdapter.setClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                LoadingPositionAdapter adapter = (LoadingPositionAdapter) loadingPositionsList
                        .getAdapter();
                LoadingPosition loadingPosition = adapter.getLoadingPositions().get
                        (loadingPositionsList.getChildAdapterPosition(view));
                loadingPosition.setSelected(!loadingPosition
                        .isSelected());

                refreshStates();
            }
        });

        int columns = getResources().getInteger(R.integer.loading_positions_columns_number);
        loadingPositionsList.setAdapter(loadingPositionAdapter);
        loadingPositionsList.setLayoutManager(new GridLayoutManager(this, columns));
        loadingPositionsList.setVisibility(View.VISIBLE);

        progress.setVisibility(View.GONE);
        buttons.setVisibility(View.VISIBLE);
    }

    public void refreshStates()
    {
        LoadingPositionAdapter loadingPositionAdapter = (LoadingPositionAdapter)
                loadingPositionsList.getAdapter();

        ArrayList<LoadingPosition> loadingPositions = loadingPositionAdapter.getLoadingPositions();

        for (int i = 0; i < loadingPositions.size(); i++)
        {
            LoadingPosition loadingPosition;
            loadingPosition = loadingPositionAdapter.getItem(i);
            loadingPosition.setState(loadingPositions.get(i).getState());
        }

        loadingPositionAdapter.notifyDataSetChanged();
    }

    private void notifyLoadingPositionsError()
    {
        RelativeLayout progress = assignPcsDialog.findViewById(R.id.progress_layout);
        //Ocultar progress
        progress.setVisibility(View.GONE);

        RelativeLayout errorLayout = assignPcsDialog.findViewById(R.id.get_pcs_error);
        //Mostrar mensaje de error
        errorLayout.setVisibility(View.VISIBLE);
    }

    private void assignLoadingPositionsToPrinter()
    {
        HashSet<Integer> printersToAdd = new HashSet<>();
        HashSet<Integer> printersToRemove = new HashSet<>();

        final TokenCashManager.onMessageReceived tokencashListener =
                new TokenCashManager.onMessageReceived()
                {
                    @Override
                    public void showResponse(JSONObject response)
                    {
                        Log.v(TAG, "Impresora agregada o removida correctamente");
                        showSnackbarMessage("Datos de impresora modificados correctamente");
                    }

                    @Override
                    public void onError(String message)
                    {
                        Toast.makeText(AssignPrinter.this, message, Toast.LENGTH_SHORT).show();
                        showSnackbarMessage("Error al modificar impresora");
                    }
                };

        //Obtener lista contenida en el GridView
        ArrayList<LoadingPosition> loadingPositions = ((LoadingPositionAdapter)
                loadingPositionsList.getAdapter()).getLoadingPositions();

        //Crear lista de "Pumps" de las posiciones seleccionadas
        ArrayList<Pump> pumps = new ArrayList<>();

        for (LoadingPosition loadingPosition : loadingPositions)
        {
            if (loadingPosition.isSelected())
            {
                Pump pump = new Pump(loadingPosition.getStationSide());
                pump.setState("AC");
                pumps.add(pump);

                if (!selectedPositions.contains(loadingPosition.getStationSide()))
                {
                    // No estaba en la lista, se debe agregar
                    printersToAdd.add(loadingPosition.getStationSide());
                }
            }
            else
            {
                if (selectedPositions.contains(loadingPosition.getStationSide()))
                {
                    // Estaba en la lista pero ya no esta seleccionada, remover
                    printersToRemove.add(loadingPosition.getStationSide());
                }
            }
        }

        //Crear objeto Impresora actualizado
        Printer printer = new Printer();
        printer.setCurrentIp(selectedPrinterIpString);
        printer.setPumps(pumps);

        //Si la de posiciones de carga no esta vacía... agregarla a la lista
        if (!pumps.isEmpty()) assignedPrinters.add(printer);

        Log.d("Pumps", "Impresora actualizada con objetos recién seleccionados");
        Log.d("Pumps", "Enviar JSONArray");

        //Obtener JSONArray de la lista de impresoras
        JSONArray jsonArray;
        try
        {
            if (tokencashWorkMode.equals("e3"))
            {
                jsonArray = PrinterUtils.PrinterObjectToE3JSONArray(printer);

                if (jsonArray != null)
                {
                    // Agregar impresoras
                    for (int idx = 0; idx < jsonArray.length(); idx++)
                    {
                        // Verificar si se tiene que agregar
                        JSONObject jsonPrinter = jsonArray.getJSONObject(idx);
                        int id = jsonPrinter.getInt("id");
                        if (printersToAdd.contains(id))
                        {
                            // Agregar a base de datos
                            TokenCashManager.createPrinter(this, jsonPrinter, tokencashListener);
                        }
                    }

                    // Remover impresoras
                    for (Integer integer : printersToRemove)
                    {
                        TokenCashManager.removePrinter(this, String.valueOf(integer),
                                tokencashListener);
                    }
                }
                else
                {
                    showSnackbarMessage("Error al procesar información");
                }
            }
            else
            {
                jsonArray = PrinterUtils.PrinterObjectToJSONArray(printer);
                chipRedManager.setPrinters(jsonArray);
            }
            Log.d("Pumps", jsonArray.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            showSnackbarMessage("Error al procesar información");
        }
        catch (NullPointerException e)
        {
            showSnackbarMessage("Error al procesar información");
        }
    }

    private void assignPcsToDevice()
    {
        //Obtener lista contenida en el GridView
        ArrayList<LoadingPosition> loadingPositions = ((LoadingPositionAdapter)
                loadingPositionsList.getAdapter()).getLoadingPositions();

        //Crear set para almacenar las posiciones
        Set<String> selectedPositions = new HashSet<>();

        //Evaluar cuales posiciones estan seleccionadas
        for (LoadingPosition loadingPosition : loadingPositions)
        {
            if (loadingPosition.isSelected())
            {
                // Si esta seleccionada, agregar a la lista
                String value = String.valueOf(loadingPosition.getStationSide());
                selectedPositions.add(value);
            }
        }

        // Almacenar set
        SharedPreferencesManager.putStringSet(this, ChipREDConstants.ASSIGNED_PCS_TO_DEVICE,
                selectedPositions);

        // Mostrar resultado
        showSnackbarMessage("Posiciones de carga asignadas a dispositivo");
    }

    private void showAssignOptions()
    {
        final String[] options = {"Asignar impresora a posiciones de carga", "Asignar este " +
                "dispositivo " +
                "a posiciones de carga"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Seleccione opcion");

        // Crear adaptador para mostrar opciones
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1);

        // Agregar opciones
        arrayAdapter.add(options[0]);
        arrayAdapter.add(options[1]);

        // Definir clickListener
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                // Obtener opcion seleccionada
                String selectedOption = arrayAdapter.getItem(which);

                if (selectedOption != null)
                {
                    if (selectedOption.equals(options[0]))
                    {
                        showAssignPcsToPrinterDialog(selectedPrinterIpString);
                    }
                    else if (selectedOption.equals(options[1]))
                    {
                        showAssignPcsToDeviceDialog();
                    }
                }

                dialog.dismiss();
            }
        });

        // Mostrar
        builder.show();
    }
}
