package com.binarium.chipredes.printer;

import com.binarium.chipredes.dispenser.Pump;

import java.util.ArrayList;

public class Printer
{
    private String deviceName;
    private String currentIp;
    private ArrayList<Pump> pumps;

    public Printer()
    {

    }

    public String getDeviceName()
    {
        return deviceName;
    }

    public void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
    }

    public String getCurrentIp()
    {
        return currentIp;
    }

    public void setCurrentIp(String currentIp)
    {
        this.currentIp = currentIp;
    }

    public ArrayList<Pump> getPumps()
    {
        return pumps;
    }

    public void setPumps(ArrayList<Pump> pumps)
    {
        this.pumps = pumps;
    }

    public void addPump(Pump pump)
    {
        if (pumps == null)
        {
            pumps = new ArrayList<>();
        }
        pumps.add(pump);
    }
}
