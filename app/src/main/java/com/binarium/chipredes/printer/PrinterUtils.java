package com.binarium.chipredes.printer;

import com.binarium.chipredes.dispenser.Pump;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PrinterUtils
{
    public static ArrayList<Printer> parsePrinters(JSONObject response)
    {
        //Crear lista para almacenar impresoras asignadas
        ArrayList<Printer> printerArrayList = new ArrayList<>();
        try
        {
            //Obtener arreglo de impresoras
            JSONArray printersArray = response.getJSONArray("data");
            //Obtener posiciones de carga relacionadas con impresoras
            for (int i = 0; i < printersArray.length(); i++)
            {
                //Obtener el objeto
                JSONObject jsonPrinter = printersArray.getJSONObject(i);
                //Obtener su ip
                String printerIp = jsonPrinter.getString("ip");
                //Obtener la posicion de carga asignada y su estado
                int loadingPosition = jsonPrinter.getInt("posicion_carga");
                String state = jsonPrinter.getString("estado");
                //Obtener indice de la impresora
                int printerIdx = indexOfPrinter(printerArrayList, printerIp);

                //Crear el objeto Pump que se va a asignar
                Pump pump = new Pump(loadingPosition);
                pump.setState(state);

                //Checar si aun no existe la impresora en la lista
                if (printerIdx == -1)
                {
                    //No se ha agregado la impresora a la lista, agregarla.
                    Printer printer = new Printer();
                    printer.setCurrentIp(printerIp);
                    printer.addPump(pump);
                    printerArrayList.add(printer);
                }
                else
                {
                    printerArrayList.get(printerIdx).addPump(pump);
                }
            }

            Collections.sort(printerArrayList, new Comparator<Printer>()
            {
                @Override
                public int compare(Printer printer, Printer t1)
                {
                    return printer.getCurrentIp().compareTo(t1.getCurrentIp());
                }
            });

            for (Printer printer : printerArrayList)
            {
                Collections.sort(printer.getPumps(), new Comparator<Pump>()
                {
                    @Override
                    public int compare(Pump pump, Pump t1)
                    {
                        return Integer.compare(pump.getNumber(), t1.getNumber());
                    }
                });
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return null;
        }

        return printerArrayList;
    }


    /**
     * indexOfPrinter
     * Busca dentro de la lista el índice del objeto que contenga el valor (value) que
     * se este buscando.
     * Si ningún objeto contiene el valor buscado, regresa un -1
     *
     * @param arrayList: lista a analizar
     * @param value:     valor a encontrar
     * @return indice del objeto que contenga el valor, si no existe, regresa -1
     */

    static int indexOfPrinter(ArrayList<Printer> arrayList, String value)
    {
        int index = -1;
        for (int i = 0; i < arrayList.size(); i++)
        {
            if (arrayList.get(i).getCurrentIp().equals(value))
            {
                return i;
            }
        }

        return index;
    }

    static JSONArray PrinterListToJSONArray(ArrayList<Printer> printers)
    {
        try
        {
            JSONArray array = new JSONArray();
            for (Printer printer : printers)
            {
                for (Pump pump : printer.getPumps())
                {
                    JSONObject jsonPrinter = new JSONObject();
                    jsonPrinter.put("ip", printer.getCurrentIp());
                    jsonPrinter.put("estado", pump.getState());
                    jsonPrinter.put("posicion_carga", pump.getNumber());
                    array.put(jsonPrinter);
                }
            }
            return array;
        }
        catch (JSONException e)
        {
            return null;
        }
    }

    static JSONArray PrinterListToE3JSONArray(ArrayList<Printer> printers)
    {
        try
        {
            JSONArray array = new JSONArray();
            for (Printer printer : printers)
            {
                for (Pump pump : printer.getPumps())
                {
                    JSONObject jsonPrinter = new JSONObject();
                    jsonPrinter.put("id", pump.getNumber()); // Id igual a pc
                    jsonPrinter.put("ip", printer.getCurrentIp());
                    jsonPrinter.put("is_active", pump.getState().equals("AC"));
                    jsonPrinter.put("dispenser_side", pump.getNumber());
                    array.put(jsonPrinter);
                }
            }
            return array;
        }
        catch (JSONException e)
        {
            return null;
        }
    }

    static JSONArray PrinterObjectToJSONArray(Printer printer)
    {
        try
        {
            JSONArray array = new JSONArray();
            for (Pump pump : printer.getPumps())
            {
                JSONObject jsonPrinter = new JSONObject();
                jsonPrinter.put("ip", printer.getCurrentIp());
                jsonPrinter.put("estado", pump.getState());
                jsonPrinter.put("posicion_carga", pump.getNumber());
                array.put(jsonPrinter);
            }

            return array;
        }
        catch (JSONException e)
        {
            return null;
        }
    }

    static JSONArray PrinterObjectToE3JSONArray(Printer printer)
    {
        try
        {
            JSONArray array = new JSONArray();
            for (Pump pump : printer.getPumps())
            {
                JSONObject jsonPrinter = new JSONObject();
                jsonPrinter.put("id", pump.getNumber()); // Id igual a pc
                jsonPrinter.put("ip", printer.getCurrentIp());
                jsonPrinter.put("is_active", pump.getState().equals("AC"));
                jsonPrinter.put("dispenser_side", pump.getNumber());
                array.put(jsonPrinter);
            }

            return array;
        }
        catch (JSONException e)
        {
            return null;
        }
    }

    static JSONObject convertE3PrintersToNormalPrinters(JSONObject response)
    {
        try
        {
            // Ajustar respuesta a la definida por los métodos de la aplicación
            JSONArray printers = response.getJSONArray("printers");
            for (int idx = 0; idx < printers.length(); idx++)
            {
                // Obtener impresora
                JSONObject printer = printers.getJSONObject(idx);

                // Reemplazar nombres de claves
                printer.put("posicion_carga", printer.getInt("dispenser_side"));
                printer.remove("dispenser_side");

                boolean status = printer.getBoolean("is_active");
                printer.put("estado", status ? "AC" : "BA");
                printer.remove("is_active");
            }

            // Renombrar arreglo con contenido
            response.put("data", response.getJSONArray("printers"));
            response.remove("printers");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return response;
    }
}
