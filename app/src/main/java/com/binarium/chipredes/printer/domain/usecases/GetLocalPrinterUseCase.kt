package com.binarium.chipredes.printer.domain.usecases

import com.binarium.chipredes.local.LocalApiService
import com.binarium.chipredes.local.services.GetPrintersPost
import com.binarium.chipredes.local.services.Printer
import javax.inject.Inject

class GetLocalPrinterUseCase @Inject constructor(
    private val localApiService: LocalApiService
) {
    suspend operator fun invoke(loadingPosition: String): Printer? {
        var printer: Printer? = null
        val printers = getPrinters()
        printers?.let {
            printer = getLoadingPositionPrinter(
                loadingPosition,
                it
            )
        }
        return printer
    }

    private suspend fun getPrinters(): List<Printer>? {
        val getPrintersPost = GetPrintersPost()
        val response = localApiService.getLocalPrinters(getPrintersPost)
        return response.body()?.data
    }

    private fun getLoadingPositionPrinter(
        loadingPosition: String,
        printers: List<Printer>
    ): Printer? =
        printers.find {
            it.posicionCarga.toString() == loadingPosition
        }
}
