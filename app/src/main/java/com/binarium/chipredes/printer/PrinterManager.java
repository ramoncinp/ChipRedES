package com.binarium.chipredes.printer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.configstation.StationFileManager;
import com.binarium.chipredes.tokencash.models.ClosedTokens;
import com.binarium.chipredes.R;
import com.binarium.chipredes.Sale;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.tokencash.models.Coupon;
import com.binarium.chipredes.tokencash.models.TokenCashPayment;
import com.binarium.chipredes.utils.DateUtils;
import com.binarium.chipredes.wolke2.models.MovimientoCuenta;
import com.github.anastaciocintra.escpos.EscPos;
import com.github.anastaciocintra.escpos.EscPosConst;
import com.github.anastaciocintra.escpos.Style;
import com.github.anastaciocintra.escpos.barcode.QRCode;
import com.github.anastaciocintra.escpos.image.Bitonal;
import com.github.anastaciocintra.escpos.image.BitonalThreshold;
import com.github.anastaciocintra.escpos.image.EscPosImage;
import com.github.anastaciocintra.escpos.image.RasterBitImageWrapper;
import com.github.anastaciocintra.output.TcpIpOutputStream;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.Socket;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import timber.log.Timber;

public class PrinterManager {
    // Constantes
    private static final int PRINTTER_PAGE_WIDTH = 42;
    private static final String TAG = "PrinterManagerLog";

    // Variables
    private final String country;
    private String ip;
    private String rawIp;

    // Objetos
    private final DecimalFormat moneyFormat;
    private EscPos escPos;
    private final PrinterListener printerListener;
    private final Context context;

    // Estilos
    final Style normalStyle = new Style()
            .setFontSize(Style.FontSize._1, Style.FontSize._1)
            .setJustification(EscPosConst.Justification.Center);

    final Style mediumTitleBoldStyle = new Style()
            .setFontSize(Style.FontSize._2, Style.FontSize._2)
            .setBold(true)
            .setJustification(EscPosConst.Justification.Center);

    final Style titleNormalStyle = new Style()
            .setFontSize(Style.FontSize._3, Style.FontSize._3)
            .setJustification(EscPosConst.Justification.Center);

    final Style boldCenterStyle = new Style()
            .setFontSize(Style.FontSize._1, Style.FontSize._1)
            .setBold(true)
            .setJustification(EscPosConst.Justification.Center);

    final Style normalLeftStyle = new Style()
            .setFontSize(Style.FontSize._1, Style.FontSize._1)
            .setJustification(EscPosConst.Justification.Left_Default);

    final Style boldLeftStyle = new Style()
            .setFontSize(Style.FontSize._1, Style.FontSize._1)
            .setBold(true)
            .setJustification(EscPosConst.Justification.Left_Default);


    public PrinterManager(Context context) {
        this(context, new PrinterListener() {
            @Override
            public void onSuccesfulPrint(String msg) {
                Timber.v(msg);
            }

            @Override
            public void onPrinterError(String msg) {
                Timber.e(msg);
            }
        });
    }

    public PrinterManager(Context context, PrinterListener printerListener) {
        this.printerListener = printerListener;
        this.context = context;

        country = SharedPreferencesManager.getString(context, ChipREDConstants.COUNTRY, "mexico");
        this.ip = SharedPreferencesManager.getString(context, ChipREDConstants.PRINTER_IP_ADDR, "");
        ip = "TCP:" + ip;

        if (country.equals("costa rica")) {
            moneyFormat = ChipREDConstants.CR_AMOUNT_FORMAT;
        } else {
            moneyFormat = ChipREDConstants.MX_AMOUNT_FORMAT;
        }
    }

    public void setIpAndInit(String ip) {
        this.rawIp = ip;
        this.ip = "TCP:" + ip;
    }

    public void setIp(String ip) {
        this.rawIp = ip;
        this.ip = "TCP:" + ip;
    }

    public void printPurchase(MovimientoCuenta movimientoCuenta) {
        Bitmap logo = BitmapFactory.decodeResource(context.getResources(), R.drawable
                .chip_red_ticket_header);
        logo = Bitmap.createScaledBitmap(logo, 400, 100, false);

        StringBuilder textData;
        double grandTotal = 0;

        //"Setear" buffer con mensajes a enviar
        try {
            // Probar conexión
            testConnection();

            // Crear instancia de impresora genérica
            final TcpIpOutputStream outputStream = new TcpIpOutputStream(rawIp);
            escPos = new EscPos(outputStream);

            //Agregar imagen de cabecera
            printImage(logo);

            //Dar dos saltos de línea
            escPos.feed(2);

            //Definir texto en negritas
            //Alinear texto a la izquierda
            escPos.setStyle(normalLeftStyle);

            //Escribir datos de posicion de carga
            escPos.write("DISPENSARIO " +
                    movimientoCuenta.getConsumo().getNumeroDispensario() + ", L" +
                    movimientoCuenta.getConsumo().getPosicionCarga() + "\n");

            //Imprimir encabezado
            printTicketHeader();

            //Definir alineación de texto al centro
            //Añadir separador
            escPos.write(normalStyle, "==========================================\n");

            //Cambiar estilo de texto a "negritas"
            escPos.write(boldCenterStyle, "TICKET DE CONSUMO\n");

            //Regresar a texto normal
            escPos.write(normalStyle, "==========================================\n");

            //Volver a negritas
            escPos.write(boldCenterStyle, "****** reimpresion ******\n\n");

            //Volver a texto normal
            //Cambiar alineación de texto
            escPos.setStyle(normalLeftStyle);

            //Eliminar el valor de "textData"
            textData = new StringBuilder();

            //Obtener datos de cliente
            textData.append("CLIENTE: ");
            textData.append(removeSpecialChars(movimientoCuenta.getCliente().getNombre()));
            textData.append("\n");

            if (country.equals("costa rica")) {
                textData.append("CEDULA : ");
            } else {
                textData.append("RFC    : ");
            }
            textData.append(removeSpecialChars(movimientoCuenta.getCliente().getClaveFiscal()));
            textData.append("\n");

            textData.append("EMAIL  : ");
            textData.append(movimientoCuenta.getCliente().getEmail());
            textData.append("\n");

            String placas = "";
            if (movimientoCuenta.getReservacionSaldo() != null) {
                placas = movimientoCuenta.getReservacionSaldo().getPlacas();
            }
            textData.append("PLACAS : ");
            textData.append(placas);
            textData.append("\n\n");

            //Agregar el texto concatenado
            escPos.write(textData.toString());

            //Definir texto al centro
            //Definir texto en negritas
            escPos.setStyle(boldCenterStyle);

            //Escribir encabezado de las columnas
            for (int i = 0; i < PRINTTER_PAGE_WIDTH; i++) escPos.write("=");
            escPos.feed(1);
            //Escribir conceptos
            escPos.write("Producto    Precio  Cantidad      Subtotal\n");
            for (int i = 0; i < PRINTTER_PAGE_WIDTH; i++) escPos.write("=");
            escPos.feed(1);

            //Escribir nombre de producto con espacios incluidos (MAX 10 chars para producto)
            String fuelProduct = movimientoCuenta.getConsumo().getProducto().getDescripcion();

            //Definir texto normal
            escPos.setStyle(normalStyle);
            //Concatenar información del combustible vendido e imprimirla
            String infoLine = (String.format(Locale.US, "%1$-11s %2$-7.2f %3$-8.3f %4$13.2f\n\n",
                    fuelProduct,
                    (float) movimientoCuenta.getConsumo().getPrecioUnitario(),
                    (float) movimientoCuenta.getConsumo().getCantidad(),
                    (float) movimientoCuenta.getConsumo().getCosto()));

            escPos.write(infoLine);
            Timber.d(infoLine);

            //Acumular subtotal
            grandTotal += movimientoCuenta.getConsumo().getCosto();

            //Si la venta fue con productos extra
            /*if (purchase.has("productos_extra"))
            {
                JSONArray extraProducts = purchase.getJSONArray("productos_extra");
                for (int i = 0; i < extraProducts.length(); i++)
                {
                    //Obtener producto extra
                    JSONObject extraProduct = extraProducts.getJSONObject(i);
                    //Obtener descripcion del producto extra
                    String description = extraProduct.getString("descripcion");
                    //Crear variable que contendrá la linea a imprimir
                    String descriptionToPrint;

                    //Obtener índice
                    int index = getDescLine(description);
                    descriptionToPrint = description.substring(0, index);
                    description = description.substring(index);

                    //Imprimir linea de producto extra
                    //Concatenar información de los productos extra vendidos e imprimirla
                    String extraProductoLine = (String.format(Locale.US, "%1$-11s %2$-7.2f " +
                            "%3$-8d" + " %4$13.2f\n", descriptionToPrint, (float) extraProduct
                            .getDouble("precio"), extraProduct.getInt("cantidad"), (float)
                            extraProduct.getDouble("total")));
                    printer.addText(extraProductoLine);

                    //Evaluar la longitud de la descripción y evaluar si con lo que sobró cabe o no
                    if (description.length() != 0)
                    {
                        while (description.length() > 0)
                        {
                            index = getDescLine(description);
                            descriptionToPrint = description.substring(0, index);
                            description = description.substring(index);
                            printer.addText(String.format(Locale.US, "%1$-11s %2$30s",
                                    descriptionToPrint, "") + "\n");
                        }
                    }
                    printer.addFeedLine(1);

                    //Acumular subtotal
                    grandTotal += extraProduct.getDouble("total");
                }
            }*/

            //Alinear al centro
            //Definir texto en negritas
            escPos.setStyle(boldCenterStyle);

            //Escribir GRAN TOTAL
            String total;
            if (country.equals("mexico")) {
                total = String.format(Locale.US, "               Total %1$17.2f MXN", (float)
                        grandTotal);
            } else {
                total = String.format(Locale.US, "               Total %1$17.2f CRC", (float)
                        grandTotal);
            }
            escPos.write(total);
            Timber.d(total);

            escPos.feed(2);
            escPos.write(normalStyle, "Identificate con este codigo QR\nal hacer tus operaciones ChipRED\n");

            //Agregar código de barras
            final QRCode qrCode = new QRCode();
            qrCode.setJustification(EscPosConst.Justification.Center);
            qrCode.setSize(8);
            escPos.write(qrCode, movimientoCuenta.getCliente().getId());
            escPos.feed(1);

            //Volver a texto normal
            escPos.setStyle(normalStyle);

            //Agregar pie de ticket
            escPos.write("GRACIAS POR SU VISITA\n");
            escPos.write("www.chipred.com\n");
            escPos.write("ChipRED disponible en\nGoogle Play para Android y\nApp Store para iPhone\n\n");

            Date fechaConsumo =
                    DateUtils.formatoFechaMovimientosCuenta(movimientoCuenta.getFechaHora(), false);
            String fechaConsumoStr = DateUtils.dateObjectToString(fechaConsumo);
            escPos.write(fechaConsumoStr + "\n\n");

            if (!movimientoCuenta.getFoliosBilletes().isEmpty()) {
                //Agregar folios
                escPos.write(boldCenterStyle, "Billetes canjeados\n");

                // Obtener arreglo de billetes
                List<String> foliosBilletes = movimientoCuenta.getFoliosBilletes();

                // Imprimir cada folio
                for (int i = 0; i < foliosBilletes.size(); i++) {
                    escPos.write(normalStyle, foliosBilletes.get(i));
                    escPos.feed(1);
                }

                escPos.feed(2);
            }

            escPos.feed(5);
            escPos.cut(EscPos.CutMode.FULL);
        } catch (IOException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir consumo");
        }
    }

    public void printPurchaseWithTokenCash(Sale purchase, ArrayList<TokenCashPayment>
            tokenCashPayments, String summaryCurrency, double summaryAmount, boolean copy) {

        //Variable para habilitar reimpresion
        boolean printCopy = SharedPreferencesManager.getBoolean(context, ChipREDConstants
                .RE_PRINT_TOKEN_CASH_PURCHASE);

        //Obtener logo de tokencash
        Bitmap logo = BitmapFactory.decodeResource(context.getResources(), R.drawable
                .token_cash_icon);

        //Hacer resize de la imagen obtenida+636
        logo = Bitmap.createScaledBitmap(logo, 500, 100, false);

        try {
            // Probar conexión
            testConnection();

            // Crear instancia de impresora genérica
            final TcpIpOutputStream outputStream = new TcpIpOutputStream(rawIp);
            escPos = new EscPos(outputStream);

            for (int n = 0; n < 2; n++) {
                //Centrar para la imagen
                //Agregar imagen de cabecera
                printImage(logo);

                //Dar dos saltos de línea
                escPos.feed(2);

                //Definir texto en negritas
                //Alinear texto a la izquierda
                escPos.setStyle(boldLeftStyle);

                //Escribir datos de posicion de carga
                escPos.write("DISPENSARIO " + purchase.getNumDispensario() +
                        ", L" + purchase.getPosicionDeCarga() + "\n");

                //Imprimir encabezado
                printTicketHeader();

                //Definir alineación de texto al centro
                escPos.setStyle(normalStyle);

                //Añadir separador
                escPos.write("==========================================\n");

                //Cambiar estilo de texto a "negritas"
                escPos.write(boldCenterStyle, "TICKET DE CONSUMO\n");

                //Regresar a texto normal
                escPos.write("==========================================\n");

                //Volver a negritas
                if (!copy) escPos.write(boldCenterStyle, "****** original ******\n\n");
                else escPos.write(boldCenterStyle, "****** reimpresion ******\n\n");

                //Volver a texto normal
                escPos.setStyle(normalStyle);

                //Escribir encabezado de las columnas
                for (int i = 0; i < PRINTTER_PAGE_WIDTH; i++) escPos.write("=");
                escPos.feed(1);
                //Definir texto en negritas
                escPos.setStyle(boldCenterStyle);
                //Escribir conceptos
                escPos.write("Producto   Precio  Cantidad          Total\n");
                //Definir texto en normal
                escPos.setStyle(normalStyle);
                for (int i = 0; i < PRINTTER_PAGE_WIDTH; i++) escPos.write("=");
                escPos.feed(1);

                //Escribir nombre de producto con espacios incluidos (MAX 10 chars para producto)
                String fuelProduct = purchase.getProducto();

                //Concatenar información del combustible vendido e imprimirla
                escPos.write((String.format(Locale.US, "%1$-10s %2$6.2f  %3$8.3f " +
                        "%4$14.2f\n", fuelProduct, (float) purchase.getPrecioUnitario(), (float)
                        purchase.getCantidad(), (float) purchase.getCosto())));

                escPos.feed(1);

                //Añadir separador
                escPos.write("==========================================\n");

                //Cambiar estilo de texto a "negritas"
                escPos.write(boldCenterStyle, "Pagos con tokencash fidelidad\n");

                //Imprimir cada pago Tokencash
                for (TokenCashPayment tokencashPayment : tokenCashPayments) {
                    String line = "";
                    line += String.format(Locale.US, "%7.2f %3s - ", (float) tokencashPayment
                            .getAmount(), tokencashPayment.getCurrency());
                    line += tokencashPayment.getDateTime();

                    escPos.write(line);
                    escPos.feed(1);
                }
                escPos.feed(1);

                //Indicar el importe recibido y el importe faltante
                escPos.write(boldCenterStyle, "Importe recibido: ");
                escPos.write(String.format(Locale.US, "%20.2f", (float) summaryAmount) + " " +
                        summaryCurrency + "\n");
                escPos.write(boldCenterStyle, "Importe faltante: ");
                escPos.write(String.format(Locale.US, "%20.2f", (float) purchase.getCosto() -
                        summaryAmount) + " " + summaryCurrency + "\n");
                escPos.feed(1);

                //Regresar a texto normal
                escPos.write("==========================================\n");

                //Agregar pie de ticket
                escPos.write("GRACIAS POR SU VISITA\n");
                escPos.write(purchase.getFechaHora() + "\n");

                if (purchase.getPumper() != null)
                    escPos.write("Le atendio " + purchase.getPumper() + "\n\n");
                escPos.feed(5);
                escPos.cut(EscPos.CutMode.FULL);

                //Validar si esta habilitada la reimpresion, si no, romper el ciclo
                if (!printCopy) break;

                //Tooglear el valor de copy
                copy = !copy;
            }
        } catch (IOException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir consumo tokencash");
        }

        printerListener.onSuccesfulPrint("Comprobante impreso correctamente");
    }

    public void printClosedTokens(String pumper, ArrayList<ClosedTokens> closedTokens) {

        Bitmap logo = BitmapFactory.decodeResource(context.getResources(), R.drawable
                .token_cash_icon);
        logo = Bitmap.createScaledBitmap(logo, 500, 100, false);

        //"Setear" buffer con mensajes a enviar
        try {
            // Probar conexión
            testConnection();

            // Crear instancia de impresora genérica
            final TcpIpOutputStream outputStream = new TcpIpOutputStream(rawIp);
            escPos = new EscPos(outputStream);

            //Centrar para la imagen
            //Agregar imagen de cabecera
            printImage(logo);

            //Dar dos saltos de línea
            escPos.feed(2);

            //Imprimir encabezado
            printTicketHeader();

            //Definir alineación de texto al centro
            escPos.setStyle(normalStyle);

            //Añadir separador
            escPos.write("==========================================\n");

            //Cambiar estilo de texto a "negritas"
            escPos.write(boldCenterStyle, "Corte de turno tokencash\n");

            //Regresar a texto normal
            escPos.write("==========================================\n\n");

            //Volver a negritas
            escPos.write(boldCenterStyle, "****** tokencash fidelidad ******\n");

            //Escribir encabezado de las columnas
            for (int i = 0; i < PRINTTER_PAGE_WIDTH; i++) escPos.write("=");
            escPos.feed(1);
            //Escribir conceptos
            escPos.write(boldCenterStyle, "Token  Monto      Abono        Fecha      \n");
            for (int i = 0; i < PRINTTER_PAGE_WIDTH; i++) escPos.write("=");
            escPos.feed(1);

            double grandTotal = 0;

            //Ciclo para obtener todos los tokens cerrados
            for (ClosedTokens closedToken : closedTokens) {
                //Concatenar información del combustible vendido e imprimirla
                escPos.write((String.format(Locale.US, "%1$-6s %2$-10.2f %3$-7.2f ",
                        closedToken.getTokenValue(), (float) closedToken.getTotalAmount(),
                        (float) closedToken.getTokenAmount())));

                escPos.write(closedToken.getDateTime() + "\n");
                grandTotal += closedToken.getTokenAmount();
            }
            escPos.feed(1);

            //Escribir encabezado de las columnas
            for (int i = 0; i < PRINTTER_PAGE_WIDTH; i++) escPos.write("=");
            escPos.feed(1);

            //Escribir GRAN TOTAL
            escPos.write(boldCenterStyle, String.format(Locale.US, "               Total %1$17.2f MXN", (float)
                    grandTotal));
            escPos.feed(1);

            //Escribir encabezado de las columnas
            for (int i = 0; i < PRINTTER_PAGE_WIDTH; i++) escPos.write("=");
            escPos.feed(2);

            //Obtener fecha
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US);

            //Agregar pie de ticket
            escPos.write(simpleDateFormat.format(date) + "\n");
            escPos.write(pumper + "\n\n");
            escPos.feed(4);
            escPos.cut(EscPos.CutMode.FULL);
        } catch (IOException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir tokens cerrados");
        }

        printerListener.onSuccesfulPrint("Resumen impreso correctamente");
    }

    public void printPayment(MovimientoCuenta movimientoCuenta, boolean withCopy) {
        JSONObject payment = new JSONObject();
        JSONObject client = new JSONObject();

        try {
            payment.put("saldo_anterior", movimientoCuenta.getSaldoAnterior());
            payment.put("cantidad", movimientoCuenta.getCantidad());
            payment.put("saldo_nuevo", movimientoCuenta.getSaldoNuevo());

            client.put("id", movimientoCuenta.getCliente().getId());
            client.put("nombre", movimientoCuenta.getCliente().getNombre());
            client.put("email", movimientoCuenta.getCliente().getEmail());
            client.put("clave_fiscal", movimientoCuenta.getCliente().getClaveFiscal());
        } catch (JSONException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al procesar la información");
            Timber.e(e.toString());
            return;
        }

        printPayment(payment, client, withCopy);
    }

    public void printPayment(JSONObject payment, JSONObject client, boolean withCopy) {
        Timber.i("Payment %s", payment.toString());
        Timber.i("Client %s", client.toString());

        try {
            // Probar conexión
            testConnection();

            // Crear instancia de impresora genérica
            final TcpIpOutputStream outputStream = new TcpIpOutputStream(rawIp);
            escPos = new EscPos(outputStream);

            // Escribir texto de prueba
            escPos.setStyle(normalStyle);

            // Crear bitmap
            // Hacer resize de la imagen obtenida + 636
            final Bitmap logo = Bitmap.createScaledBitmap(
                    BitmapFactory.decodeResource(context.getResources(), R.drawable.chip_red_ticket_header),
                    400,
                    100,
                    false);

            // Preparar buffer de texto
            StringBuilder textData;

            try {
                for (int copyIdx = 0; copyIdx < (withCopy ? 2 : 1); copyIdx++) {
                    // Si no es 0, imprimir copia
                    boolean copy = copyIdx != 0;

                    // Imprimir imagen
                    printImage(logo);
                    escPos.feed(2);

                    // Imprimir encabezado
                    printTicketHeader();

                    // Definir estilo para texto en centro
                    escPos.write(normalStyle, "==========================================\n");
                    escPos.write(boldCenterStyle, "DEPOSITO A CUENTA\n");
                    escPos.write(normalStyle, "==========================================\n");

                    if (!withCopy) {
                        escPos.write(boldCenterStyle, "****** reimpresion ******\n\n");
                    } else {
                        if (!copy) escPos.write(boldCenterStyle, "****** original ******\n\n");
                        else escPos.write(boldCenterStyle, "****** reimpresion ******\n\n");
                    }

                    // Definir diferente estilo
                    escPos.setStyle(normalLeftStyle);

                    //Eliminar el valor de "textData"
                    textData = new StringBuilder();

                    //Obtener datos de cliente
                    textData.append("CLIENTE: ");
                    textData.append(removeSpecialChars(client.getString("nombre")));
                    textData.append("\n");

                    if (country.equals("costa rica")) {
                        textData.append("CEDULA : ");
                        textData.append(removeSpecialChars(client.getString("clave_fiscal")));
                        textData.append("\n");
                    } else {
                        textData.append("RFC    : ");
                        textData.append(removeSpecialChars(client.getString("clave_fiscal")));
                        textData.append("\n");
                    }

                    /*textData.append("CUENTA : ");
                    textData.append(removeSpecialChars(payment.getJSONObject("cliente").getJSONObject
                            ("cuentas").getString("descripcion")).toUpperCase());
                    textData.append("\n");*/

                    textData.append("EMAIL  : ");
                    textData.append(client.getString("email"));
                    textData.append("\n\n");

                    //Agregar el texto concatenado
                    escPos.write(textData.toString());

                    //Reiniciar StringBuilder
                    textData = new StringBuilder();

                    //Obtener cantidades de los abonos agregandoles formato
                    String[] paymentQuantities = new String[3];
                    paymentQuantities[0] = moneyFormat.format(payment.getDouble("saldo_anterior"));
                    paymentQuantities[1] = moneyFormat.format(payment.getDouble("cantidad"));
                    paymentQuantities[2] = moneyFormat.format(payment.getDouble("saldo_nuevo"));

                    //Crear arreglo de cadenas con el nombre de cada concepto
                    String[] keys = new String[3];
                    keys[0] = "Saldo Anterior";
                    keys[1] = "Monto Deposito";
                    keys[2] = "Saldo Nuevo";

                    String currency;
                    if (country.equals("mexico")) {
                        currency = "MXN";
                    } else {
                        currency = "CRC";
                    }

                    //Calcular espacios intermedios
                    for (int i = 0; i < paymentQuantities.length; i++) {
                        //Agregar caracteres de moneda
                        paymentQuantities[i] = paymentQuantities[i] + " " + currency;

                        //Crear cadena donde concatenar los datos
                        StringBuilder finalString = new StringBuilder();
                        //Agregar nombre de concepto
                        finalString.append(keys[i]);
                        //Obtener numero de espacios entre los textos
                        int spaces = (PRINTTER_PAGE_WIDTH) - (keys[i].length() + paymentQuantities[i]
                                .length());
                        for (int z = 0; z < spaces; z++) {
                            //Rellenar con espacios
                            finalString.append(" ");
                        }

                        //Concatenar cantidad
                        finalString.append(paymentQuantities[i]);

                        //Agregar al ticket
                        textData.append(finalString.toString());
                        textData.append("\n");
                    }

                    escPos.write(normalStyle, textData.toString());
                    escPos.feed(1);

                    final QRCode qrCode = new QRCode();
                    qrCode.setJustification(EscPosConst.Justification.Center);
                    qrCode.setSize(8);
                    escPos.write(qrCode, client.getString("id"));
                    escPos.feed(1);

                    //Agregar pie de ticket
                    escPos.setStyle(normalStyle);
                    escPos.write("GRACIAS POR SU VISITA\n");
                    escPos.write("www.chipred.com\n");
                    escPos.write("ChipRED disponible en\nGoogle Play para Android y\nApp Store para iPhone\n\n");
                    escPos.write(ChipREDConstants.DATE_FORMAT.format(new Date()));

                    escPos.feed(5);
                    escPos.cut(EscPos.CutMode.FULL);
                }
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
                printerListener.onPrinterError("Error al procesar la información");
                Timber.e(jsonException.toString());
            } finally {
                escPos.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir depósito");
        }
    }

    public void printToken(String tokenAmount, String tokenValue, String pc) {

        Bitmap logo = BitmapFactory.decodeResource(context.getResources(), R.drawable
                .token_cash_icon);
        logo = Bitmap.createScaledBitmap(logo, 500, 100, false);

        try {
            // Probar conexión
            testConnection();

            // Crear instancia de impresora genérica
            final TcpIpOutputStream outputStream = new TcpIpOutputStream(rawIp);
            escPos = new EscPos(outputStream);

            //Agregar imagen de cabecera
            printImage(logo);
            escPos.feed(1);

            String pumpDesc = "Posicion de carga: " + pc;
            escPos.write(boldCenterStyle, String.format(Locale.US, "%1$-42s", pumpDesc));
            escPos.feed(1);

            escPos.setStyle(normalStyle);
            escPos.write("==========================================\n");
            escPos.write("Paga con la aplicacion tokencash");
            escPos.feed(2);

            escPos.write("Monto a pagar:\n");
            escPos.setStyle(titleNormalStyle);

            //Imprimir denominación del token
            escPos.write(tokenAmount);
            escPos.feed(3);

            //Imprimir código QR
            final QRCode qrCode = new QRCode();
            qrCode.setJustification(EscPosConst.Justification.Center);
            qrCode.setSize(12);
            escPos.write(qrCode, tokenValue);
            escPos.feed(1);

            //Regresar a tamaño de texto normal
            escPos.setStyle(normalStyle);

            //Imprimir valor del token
            escPos.write("Tu token es: ");
            escPos.write(tokenValue);
            escPos.feed(2);

            escPos.write("==========================================\n");
            escPos.write("No tienes tokencash?\n Descarga la aplicacion para\n");
            escPos.write("Android o iPhone\n tokencash.mx/app\n");
            escPos.write("==========================================\n");

            escPos.feed(5);
            escPos.cut(EscPos.CutMode.FULL);

            printerListener.onSuccesfulPrint("Impresión realizada correctamente");
        } catch (IOException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir token");
        }
    }

    public void printCoupon(Coupon coupon) {

        Bitmap logo = BitmapFactory.decodeResource(context.getResources(), R.drawable
                .token_cash_icon);
        logo = Bitmap.createScaledBitmap(logo, 500, 100, false);

        try {
            // Probar conexión
            testConnection();

            // Crear instancia de impresora genérica
            final TcpIpOutputStream outputStream = new TcpIpOutputStream(rawIp);
            escPos = new EscPos(outputStream);

            printImage(logo);
            escPos.feed(1);

            // RECOMPENSA
            escPos.setStyle(normalStyle);
            escPos.write("==========================================\n");

            // Definir texto de recompensa
            String textoRecompensa =
                    "RECOMPENSA: $" + ChipREDConstants.MX_AMOUNT_FORMAT.format(coupon.getAmount()) + '\n';
            escPos.write(mediumTitleBoldStyle, textoRecompensa);
            escPos.write("==========================================\n");

            // CÓDIGO QR
            final QRCode qrCode = new QRCode();
            qrCode.setJustification(EscPosConst.Justification.Center);
            qrCode.setSize(10);
            escPos.write(qrCode, coupon.getCode());

            String codeValueText = "Tu codigo es: " + coupon.getCode() + '\n';
            escPos.write(codeValueText);

            String pumperString = "Le atendio: " + coupon.getPumper() + '\n';
            escPos.write(boldCenterStyle, pumperString);

            String pumpString = "Posicion de carga: " + coupon.getPump() + '\n';
            escPos.write(boldCenterStyle, pumpString);

            // FECHA DE EXPIRACION
            escPos.write("==========================================\n");

            // Definir texto de recompensa
            String textoFechaExpiracion =
                    "Escanealo antes de: " + coupon.getFechaExpiracion() + '\n';
            escPos.write(boldCenterStyle, textoFechaExpiracion);

            escPos.write("==========================================\n");
            escPos.write("No tienes tokencash?\n Descarga la aplicacion para\n");
            escPos.write("Android o iPhone\n Escanea y acumula dinero en cada compra\n");
            escPos.write("www.tokencash.mx\n\n");

            // Preparar variables para editables
            String editable1 = "";
            String editable2 = getStationName();
            try {
                // Obtener documento de fidelización
                JSONObject tokencashConfig = StationFileManager.getTokenCashConfig(context);

                // Tratar de obtener editable 1
                String ed1 = tokencashConfig.getJSONObject("editables").getString("editable_1");
                String ed2 = tokencashConfig.getJSONObject("editables").getString("editable_2");

                // Asignar valores obtenidos
                editable1 = ed1;
                editable2 = ed2;
            } catch (Exception e) {
                Timber.tag(TAG).v("Error al obtener editables");
            }

            escPos.write(editable1 + "\n");
            escPos.write("==========================================\n");
            escPos.write(editable2 + "\n");

            escPos.feed(5);
            escPos.cut(EscPos.CutMode.FULL);
        } catch (IOException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir token");
            return;
        }

        printerListener.onSuccesfulPrint("Cupón impreso correctamente");
    }

    private String getStationName() {
        //Obtener de las sharedPreferences los valores de la estacion
        String stationStringData = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_DATA);

        if (stationStringData.isEmpty()) {
            printerListener.onPrinterError("Error al obtener nombre de estacion");
            return "Estacion de servicio";
        }

        try {
            JSONObject station = new JSONObject(stationStringData);
            return station.getString("nombre_estacion");
        } catch (JSONException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al obtener nombre de estacion");
            return "Estacion de servicio";
        }
    }

    void printTest() {

        try {
            // Probar conexión
            testConnection();

            // Crear instancia de impresora genérica
            final TcpIpOutputStream outputStream = new TcpIpOutputStream(rawIp);
            escPos = new EscPos(outputStream);

            // Escribir texto de prueba
            escPos.setStyle(normalStyle);

            // Crear bitmap
            //Hacer resize de la imagen obtenida + 636
            final Bitmap logo = Bitmap.createScaledBitmap(
                    BitmapFactory.decodeResource(context.getResources(), R.drawable.chip_red_ticket_header),
                    400,
                    100,
                    false);

            // Imprimir imagen
            printImage(logo);
            escPos.feed(1);

            escPos.write("==========================================\n");
            escPos.write("Test de Impresora\n");
            escPos.write("==========================================\n");
            escPos.feed(1);

            final QRCode qrCode = new QRCode();
            qrCode.setJustification(EscPosConst.Justification.Center);
            qrCode.setSize(7);
            escPos.write(qrCode, "Test de impresora");

            escPos.feed(1);
            escPos.write(DateUtils.dateObjectToString(new Date()));

            escPos.feed(5);
            escPos.cut(EscPos.CutMode.FULL);
            escPos.close();

        } catch (IOException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir prueba");
        }
    }

    private static String removeSpecialChars(String input) {
        // Cadena de caracteres original a sustituir.
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        // Cadena de caracteres ASCII que reemplazarán los originales.
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        String output = input;
        for (int i = 0; i < original.length(); i++) {
            // Reemplazamos los caracteres especiales.
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }//for i
        return output;
    }

    private void printTicketHeader() {
        //Obtener de las sharedPreferences los valores de la estacion
        String stationStringData = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_DATA);

        if (stationStringData.isEmpty()) {
            printerListener.onPrinterError("Error al obtener datos de estación");
            return;
        }

        try {
            //Convertir String a objeto JSON para procesar la información
            JSONObject station = new JSONObject(stationStringData);
            StringBuilder textData = new StringBuilder();

            //Definir texto normal a la izquiera
            escPos.setStyle(normalLeftStyle);

            //Añadir nombre de estación
            textData.append(station.getString("nombre_estacion"));
            textData.append("\n");

            //Añadir dirección de estación
            //Obtener direccion
            JSONObject address = station.getJSONObject("direccion");
            if (country.equals("mexico")) {
                textData.append(address.getString("calle"));
                textData.append(" ");
                textData.append(address.getString("numero_ext"));
                textData.append("\n");

                //Añadir RFC
                textData.append("RFC     : ");
                textData.append(station.getString("rfc_cedula"));
                textData.append("\n");
            } else if (country.equals("costa rica")) {
                textData.append(address.getString("otras_senas"));
                textData.append("\n");

                //Añadir RFC
                textData.append("CEDULA  : ");
                textData.append(station.getString("rfc_cedula"));
                textData.append("\n");
            }

            //Añadir teléfono
            textData.append("TELEFONO: ");
            textData.append(station.getJSONObject("telefono").getString("numero"));
            textData.append("\n");

            //Añadir email
            textData.append("EMAIL   : ");
            textData.append(station.getString("email"));
            textData.append("\n");

            escPos.write(textData.toString());
        } catch (IOException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al imprimir");
        } catch (JSONException e) {
            e.printStackTrace();
            printerListener.onPrinterError("Error al procesar la información");
        }
    }

    private void printImage(Bitmap bitmap) throws IOException {
        final RasterBitImageWrapper imageWrapper = new RasterBitImageWrapper();
        imageWrapper.setJustification(EscPosConst.Justification.Center);
        final Bitonal algorithm = new BitonalThreshold(200);
        final EscPosImage escPosImage = new EscPosImage(
                new EscPosImageImpl(bitmap), algorithm
        );

        escPos.write(imageWrapper, escPosImage);
    }

    private void testConnection() throws IOException {
        final Socket socket = new Socket(rawIp, 9100);
        socket.close();
    }

    public interface PrinterListener {
        void onSuccesfulPrint(String msg);

        void onPrinterError(String msg);
    }
}
