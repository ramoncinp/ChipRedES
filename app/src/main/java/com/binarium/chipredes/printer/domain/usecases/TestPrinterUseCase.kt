package com.binarium.chipredes.printer.domain.usecases

import java.net.InetSocketAddress
import java.net.Socket
import javax.inject.Inject

class TestPrinterUseCase @Inject constructor(
    private val getLocalPrinterUseCase: GetLocalPrinterUseCase
) {
    suspend operator fun invoke(loadingPosition: String): Boolean {
        val printer = getLocalPrinterUseCase.invoke(loadingPosition)
        return printer?.let {
            try {
                testConnection(it.ip.orEmpty())
            } catch (e: Exception) {
                e.printStackTrace()
                return false
            }
            return true
        } ?: false
    }

    private fun testConnection(ipAddress: String) {
        val timeout = 5000
        val socket = Socket()
        val socketAddress = InetSocketAddress(ipAddress, 9100)
        socket.connect(socketAddress, timeout)
        socket.close()
    }
}
