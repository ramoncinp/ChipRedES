package com.binarium.chipredes.printer;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.R;
import com.binarium.chipredes.dispenser.Pump;
import com.binarium.chipredes.dispenser.PumpsArrayAdapter;

import java.util.ArrayList;

public class PrinterMasterAdapter extends RecyclerView.Adapter<PrinterMasterAdapter
        .PrinterMasterViewHolder>
{
    private PrinterMasterInterface printerMasterInterface;

    static class PrinterMasterViewHolder extends RecyclerView.ViewHolder
    {
        public TextView ipAddress;
        public RecyclerView pumpsList;

        PrinterMasterViewHolder(View itemView)
        {
            super(itemView);

            ipAddress = itemView.findViewById(R.id.text_view);
            pumpsList = itemView.findViewById(R.id.assigned_pumps_list);
        }
    }

    private ArrayList<Printer> printers;
    private Context context;

    PrinterMasterAdapter(ArrayList<Printer> printers, Context context, PrinterMasterInterface printerMasterInterface)
    {
        this.printers = printers;
        this.context = context;
        this.printerMasterInterface = printerMasterInterface;
    }

    @Override
    public PrinterMasterViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.printer_master,
                parent, false);

        return new PrinterMasterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PrinterMasterViewHolder holder, int position)
    {
        final Printer printer = printers.get(position);
        holder.ipAddress.setText(printer.getCurrentIp());

        final PumpsArrayAdapter pumpsArrayAdapter = new PumpsArrayAdapter(printer.getPumps(), context);
        pumpsArrayAdapter.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                final int pumpPosition = holder.pumpsList.getChildAdapterPosition(v);
                final Pump pump = printer.getPumps().get(pumpPosition);
                GenericDialog genericDialog = new GenericDialog("Impresora", "¿Esta seguro que " +
                        "quiere remover la impresión de esta posición de carga?", new Runnable()
                {
                    @Override
                    public void run()
                    {
                        pumpsArrayAdapter.getPumps().remove(pumpPosition);
                        pumpsArrayAdapter.notifyItemRemoved(pumpPosition);
                        printerMasterInterface.onRemoveItemRequest(pump.getNumber());
                    }
                }, new Runnable()
                {
                    @Override
                    public void run()
                    {
                        //Nada
                    }
                }, context);

                genericDialog.setPositiveText("Sí");
                genericDialog.setNegativeText("No");
                genericDialog.show();

                return true;
            }
        });

        RecyclerView recyclerView = holder.pumpsList;
        recyclerView.setAdapter(pumpsArrayAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount()
    {
        return printers.size();
    }

    public ArrayList<Printer> getPrinters()
    {
        return printers;
    }

    public interface PrinterMasterInterface
    {
        void onRemoveItemRequest(int itemNumber);
    }
}
