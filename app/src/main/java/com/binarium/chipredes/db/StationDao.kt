package com.binarium.chipredes.db

import androidx.room.*
import com.binarium.chipredes.configstation.data.entities.ChipRedStation

@Dao
interface StationDao {
    @Query("SELECT * FROM station")
    fun getStation(): List<ChipRedStation>

    @Insert
    fun insertStation(station: ChipRedStation)

    @Query("DELETE FROM station")
    fun dropDb()
}
