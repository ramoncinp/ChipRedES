package com.binarium.chipredes.db.mappers

import com.binarium.chipredes.configstation.data.entities.ChipRedStation
import com.binarium.chipredes.configstation.data.entities.Direccion
import com.binarium.chipredes.configstation.data.entities.Telefono
import org.json.JSONObject
import timber.log.Timber

fun stationEntityFromJson(mongoId: String, data: JSONObject): ChipRedStation? {
    var station: ChipRedStation? = null

    try {
        station = ChipRedStation(
            mongoId,
            data.getString("rfc_cedula"),
            getAddressFromJson(data.getJSONObject("direccion")),
            data.getString("nombre_estacion"),
            data.getString("id_estacion"),
            getPhoneFromJson(data.getJSONObject("telefono")),
            data.getString("email"),
            getBooleanOrEmpty("registro_venta_efectivo", data)
        )
    } catch (e: Exception) {
        Timber.e(e)
    }

    return station
}

private fun getAddressFromJson(data: JSONObject) = Direccion(
    getStringOrEmpty("numero_int", data),
    getStringOrEmpty("numero_ext", data),
    getStringOrEmpty("colonia", data),
    getStringOrEmpty("calle", data),
    getStringOrEmpty("codigo_postal", data),
    getStringOrEmpty("pais", data),
    getStringOrEmpty("estado", data),
    getStringOrEmpty("municipio", data)
)

private fun getPhoneFromJson(data: JSONObject) = Telefono(
    getStringOrEmpty("codigo", data),
    getStringOrEmpty("numero", data)
)

private fun getStringOrEmpty(field: String, data: JSONObject) =
    if (data.has(field)) data.getString(field) else ""

private fun getBooleanOrEmpty(field: String, data: JSONObject) =
    if (data.has(field)) data.getBoolean(field) else false
