package com.binarium.chipredes.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.binarium.chipredes.configstation.data.entities.ChipRedStation

@Database(entities = [ChipRedStation::class], version = 1)
abstract class ChipRedDatabase : RoomDatabase() {
    abstract fun stationDao(): StationDao
}
