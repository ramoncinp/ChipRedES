package com.binarium.chipredes.disppreset;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binarium.chipredes.R;

import java.util.ArrayList;
import java.util.Locale;

public class DispInfoAdapter extends RecyclerView.Adapter<DispInfoAdapter.DispIpInfoViewHolder> implements View.OnClickListener
{
    private ArrayList<DispIpInfo> disps;
    private Context context;
    private View.OnClickListener listener;

    public DispInfoAdapter(Context context, ArrayList<DispIpInfo> disps)
    {
        this.context = context;
        this.disps = disps;
    }

    @Override
    public int getItemCount()
    {
        return disps.size();
    }

    @NonNull
    @Override
    public DispIpInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.disp_send_preset_row,
                parent, false);

        return new DispIpInfoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DispIpInfoViewHolder holder, int position)
    {
        final DispIpInfo dispIpInfo = disps.get(position);

        holder.number.setText(String.format(Locale.getDefault(), "D%d",
                dispIpInfo.getDispNumber()));

        String ipAddress = dispIpInfo.getIpAddress();
        holder.ipAddress.setText(ipAddress == null ? "Sincronizar" : ipAddress);

        if (ipAddress == null)
        {
            holder.ipAddress.setText("Sincronizar");
            holder.connectionButton.setEnabled(false);
            holder.connectionButton.setAlpha(0.5f);
            holder.connectionButton.setVisibility(View.VISIBLE);
            holder.progressBar.setVisibility(View.GONE);
        }
        else
        {
            holder.connectionButton.setEnabled(true);
            holder.connectionButton.setAlpha(1f);

            // Evaluar estados
            if (dispIpInfo.getState() == DispIpInfo.IN_PROGRESS)
            {
                holder.connectionButton.setVisibility(View.GONE);
                holder.progressBar.setVisibility(View.VISIBLE);
            }
            else if (dispIpInfo.getState() == DispIpInfo.ON)
            {
                holder.connectionButton.setVisibility(View.VISIBLE);
                holder.progressBar.setVisibility(View.GONE);
                holder.connectionButton.setText("DESACTIVAR");
                holder.connectionButton.setBackground(context.getResources().getDrawable(R.drawable.round_corner_green_button));
            }
            else if (dispIpInfo.getState() == DispIpInfo.OFF)
            {
                holder.connectionButton.setVisibility(View.VISIBLE);
                holder.progressBar.setVisibility(View.GONE);
                holder.connectionButton.setText("ACTIVAR");
                holder.connectionButton.setBackground(context.getResources().getDrawable(R.drawable.round_corner_color_primary_button));
            }
            else if (dispIpInfo.getState() == DispIpInfo.UNAVAILABLE)
            {
                holder.connectionButton.setVisibility(View.GONE);
                holder.progressBar.setVisibility(View.GONE);
            }
        }

        // Definir clickListener a botón
        holder.connectionButton.setOnClickListener(this);
    }

    public void setListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onClick(v);
        }
    }

    static class DispIpInfoViewHolder extends RecyclerView.ViewHolder
    {
        private Button connectionButton;
        private ProgressBar progressBar;
        private TextView ipAddress;
        private TextView number;

        DispIpInfoViewHolder(View itemView)
        {
            super(itemView);
            connectionButton = itemView.findViewById(R.id.connection_button);
            ipAddress = itemView.findViewById(R.id.dispenser_ip_text);
            number = itemView.findViewById(R.id.dispenser_number_text);
            progressBar = itemView.findViewById(R.id.status_progress_bar);
        }
    }
}
