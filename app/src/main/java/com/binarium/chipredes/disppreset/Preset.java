package com.binarium.chipredes.disppreset;

public class Preset
{
    public static final String VOLUME_PRESET = "volumePreset";
    public static final String AMOUNT_PRESET = "amountPreset";

    private double value;
    private String presetType;

    public Preset(double value, String presetType)
    {
        this.value = value;
        this.presetType = presetType;
    }

    public double getValue()
    {
        return value;
    }

    public String getPresetType()
    {
        return presetType;
    }

    public void setValue(double value)
    {
        this.value = value;
    }
}
