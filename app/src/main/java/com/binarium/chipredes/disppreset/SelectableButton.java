package com.binarium.chipredes.disppreset;

public class SelectableButton
{
    private boolean selected;
    private int backgroundColor;
    private String text;

    public SelectableButton(int backgroundColor, String text)
    {
        this.backgroundColor = backgroundColor;
        this.text = text;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public int getBackgroundColor()
    {
        return backgroundColor;
    }

    public String getText()
    {
        return text;
    }
}
