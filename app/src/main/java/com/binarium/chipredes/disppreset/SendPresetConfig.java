package com.binarium.chipredes.disppreset;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.configstation.StationFileManager;
import com.binarium.chipredes.emax.EmaxProvider;
import com.github.leandroborgesferreira.loadingbutton.presentation.State;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import com.github.leandroborgesferreira.loadingbutton.customViews.CircularProgressButton;

public class SendPresetConfig extends AppCompatActivity
{
    // Constantes
    private static final String TAG = SendPresetConfig.class.getSimpleName();
    public static final String VOLUME_PRESETS = "presetsVolumen";
    public static final String MONEY_PRESETS = "presetsDinero";

    // Views
    private CardView syncDispIpsButton;
    private CircularProgressButton testEmaxButton;
    private MaterialEditText emaxIpEt;
    private ProgressBar dispensersProgressbar;
    private RecyclerView moneyPresetsList;
    private RecyclerView volumePresetsList;
    private RecyclerView dispensersList;

    // Objetos
    private ArrayList<DispIpInfo> disps = new ArrayList<>();
    private final ArrayList<Preset> moneyPresets = new ArrayList<>();
    private final ArrayList<Preset> litersPresets = new ArrayList<>();
    private DispInfoAdapter dispInfoAdapter;
    private PresetAdapter amountsAdapter;
    private PresetAdapter litersAdapter;
    private final Comparator<Preset> presetComparator = new Comparator<Preset>()
    {
        @Override
        public int compare(Preset preset, Preset t1)
        {
            return Double.compare(t1.getValue(), preset.getValue());
        }
    };

    // Callbacks
    private final EmaxProvider.DispensersInterface dispensersInterface =
            new EmaxProvider.DispensersInterface()
            {
                @Override
                public void onFetchedDispensers(ArrayList<DispIpInfo> dispsIpInfo)
                {
                    // Pasar referencia
                    disps = dispsIpInfo;

                    dispInfoAdapter = new DispInfoAdapter(SendPresetConfig.this,
                            disps);
                    dispInfoAdapter.setListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            setGetWorkMode(dispensersList.getChildAdapterPosition((View) view.getParent()),
                                    DispenserController.SET_MODE_COMMAND);
                        }
                    });

                    dispensersList.setLayoutManager(new LinearLayoutManager(SendPresetConfig.this));
                    dispensersList.setAdapter(dispInfoAdapter);

                    dispensersProgressbar.setVisibility(View.GONE);
                    dispensersList.setVisibility(View.VISIBLE);

                    checkDispensersStatus();
                }

                @Override
                public void onError(String message)
                {
                    Toast.makeText(SendPresetConfig.this, message, Toast.LENGTH_SHORT).show();
                    dispensersProgressbar.setVisibility(View.GONE);
                    dispensersList.setVisibility(View.VISIBLE);
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_preset_config);
        setTitle("Configuraciones");

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        initViews();
        setValues();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews()
    {
        emaxIpEt = findViewById(R.id.emax_ip_et);
        moneyPresetsList = findViewById(R.id.money_presets_list);
        volumePresetsList = findViewById(R.id.volume_presets_list);
        dispensersList = findViewById(R.id.disps_ip_info_list);
        dispensersProgressbar = findViewById(R.id.disps_progress);
        syncDispIpsButton = findViewById(R.id.refresh_disps_button);
        testEmaxButton = findViewById(R.id.test_emax_button);
        syncDispIpsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dispensersProgressbar.setVisibility(View.VISIBLE);
                dispensersList.setVisibility(View.GONE);
                syncDispensersData();
            }
        });

        testEmaxButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Validar ip
                if (emaxIpEt.validateWith(new METValidator("Campo obligatorio")
                {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
                    {
                        return !isEmpty;
                    }
                }))
                {
                    // Obtener texto
                    String emaxIp = emaxIpEt.getText().toString();

                    // Almacenar en preferencias
                    SharedPreferencesManager.putString(SendPresetConfig.this,
                            ChipREDConstants.EMAX_URL, emaxIp);

                    if (testEmaxButton.getState() == State.IDLE)
                    {
                        ChipREDConstants.hideKeyboardFromActivity(SendPresetConfig.this);
                        testEmaxButton.revertAnimation();
                        testEmaxButton.startAnimation();
                        emaxTestConnection();
                    }
                }
            }
        });

        Button saveButton = findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                saveValues();
            }
        });
    }

    private void setValues()
    {
        // Obtener ip de servidor emax
        String emaxIp = SharedPreferencesManager.getString(this, ChipREDConstants.EMAX_URL);
        emaxIpEt.setText(emaxIp);

        // Leer los demás datos almacenados
        setPresetsValues();
        setLitersValues();
        setMoneyValues();
        fetchDispensersData();
    }

    private void setPresetsValues()
    {
        // Valores default
        final double[] liters = {80, 60, 40, 20, 10, 0};
        final double[] amounts = {50000, 20000, 10000, 5000, 1000, 0};

        // Obtener valores almacenados
        Set<String> litersSet = SharedPreferencesManager.getStringSet(this, VOLUME_PRESETS);
        Set<String> amountsSet = SharedPreferencesManager.getStringSet(this, MONEY_PRESETS);

        if (litersSet.isEmpty())
        {
            // Utilizar valores default
            for (int i = 0; i < 6; i++)
            {
                if (liters[i] != 0) litersPresets.add(new Preset(liters[i], Preset.VOLUME_PRESET));
            }
        }
        else
        {
            // Utilizar valores guardados
            for (String preset : litersSet)
            {
                // Convertir a double
                double value = Double.parseDouble(preset);

                // Crear preset solo si el valor es diferente de cero
                if (value != 0) litersPresets.add(new Preset(value, Preset.VOLUME_PRESET));
            }
        }

        if (amountsSet.isEmpty())
        {
            // Utilizar valores default
            for (int i = 0; i < 6; i++)
            {
                if (amounts[i] != 0) moneyPresets.add(new Preset(amounts[i], Preset.AMOUNT_PRESET));
            }
        }
        else
        {
            // Utilizar valores guardados
            for (String preset : amountsSet)
            {
                // Convertir a double
                double value = Double.parseDouble(preset);

                // Crear preset solo si el valor es diferente de cero
                if (value != 0) moneyPresets.add(new Preset(value, Preset.AMOUNT_PRESET));
            }
        }
    }

    private void fetchDispensersData()
    {
        EmaxProvider.getDispensersIpInfo(this, dispensersInterface);
    }

    private void syncDispensersData()
    {
        EmaxProvider.syncDispensersIpInfo(this, dispensersInterface);
    }

    private void setLitersValues()
    {
        // Ordenar cantidades
        Collections.sort(litersPresets, presetComparator);
        litersAdapter = new PresetAdapter(litersPresets);
        litersAdapter.setListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Obtener preset
                Preset preset = litersPresets.get(volumePresetsList.getChildAdapterPosition(view));

                // Mostrarlo en diálogo
                showPresetValueDialog(preset);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);

        volumePresetsList.setLayoutManager(layoutManager);
        volumePresetsList.setAdapter(litersAdapter);
    }

    private void setMoneyValues()
    {
        final String currenySymbol = StationFileManager.getStationCountry(this).equals("mexico")
                ? "$"
                : "₡";

        // Ordenar cantidades
        Collections.sort(moneyPresets, presetComparator);
        amountsAdapter = new PresetAdapter(moneyPresets);
        amountsAdapter.setCurrencySymbol(currenySymbol);
        amountsAdapter.setListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Obtener preset
                Preset preset = moneyPresets.get(moneyPresetsList.getChildAdapterPosition(view));

                // Mostrarlo en diálogo
                showPresetValueDialog(preset);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);

        moneyPresetsList.setLayoutManager(layoutManager);
        moneyPresetsList.setAdapter(amountsAdapter);
    }

    private void showPresetValueDialog(final Preset preset)
    {
        final String presetType = preset.getPresetType().equals(Preset.VOLUME_PRESET) ? "litros" :
                "dinero";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Preset " + presetType);

        // Crear contenedor
        LinearLayout container = new LinearLayout(this);
        container.setOrientation(LinearLayout.VERTICAL);

        // Crear editText
        final EditText presetValueEt = new EditText(this);
        presetValueEt.setHint("0");
        presetValueEt.setText(ChipREDConstants.CR_AMOUNT_FORMAT.format(preset.getValue()));

        // Definir filtros
        int maxLength = preset.getPresetType().equals(Preset.VOLUME_PRESET) ? 3 : 7;
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(maxLength);
        presetValueEt.setFilters(filterArray);
        presetValueEt.setInputType(InputType.TYPE_CLASS_NUMBER);
        presetValueEt.setTextSize(TypedValue.COMPLEX_UNIT_SP,
                getResources().getDimension(R.dimen.text));

        // Definir márgenes
        LinearLayout.LayoutParams lp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(20, 0, 20, 0);
        presetValueEt.setLayoutParams(lp);

        // Definir textWatcher
        presetValueEt.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                // Obtener valor
                String valueString = editable.length() > 0 ? editable.toString() : "0";
                int value = Integer.parseInt(valueString.replace(",", ""));

                // Formatear preset si se el valor es mayor a cero
                if (value > 0)
                {
                    presetValueEt.removeTextChangedListener(this);
                    presetValueEt.setText(ChipREDConstants.CR_AMOUNT_FORMAT.format(value));
                    presetValueEt.addTextChangedListener(this);
                    presetValueEt.setSelection(presetValueEt.getText().length());
                }
            }
        });

        // Definir view
        container.addView(presetValueEt);
        builder.setView(container);

        // Definir botones
        builder.setPositiveButton("Guardar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                // Validar texto ingresado
                if (presetValueEt.length() == 0)
                {
                    Toast.makeText(SendPresetConfig.this, "Campo vacío", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    double value = Double.parseDouble(presetValueEt.getText().toString().replace(
                            ",",
                            ""));

                    if (value != 0)
                    {
                        // Evaluar si es diferente al valor anterior
                        if (value != preset.getValue())
                        {
                            if (presetValueExists(preset.getPresetType(), value))
                            {
                                Toast.makeText(SendPresetConfig.this, "El valor de preset ya esta" +
                                        " definido", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                preset.setValue(value);
                                onPresetModified(preset.getPresetType());
                            }
                        }
                    }
                    else
                    {
                        Toast.makeText(SendPresetConfig.this, "Valor no válido",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        });

        final Dialog dialog = builder.create();
        dialog.show();
    }

    private boolean presetValueExists(String presetType, double value)
    {
        final ArrayList<Preset> presets = presetType.equals(Preset.VOLUME_PRESET) ?
                litersPresets : moneyPresets;

        for (Preset preset : presets)
        {
            if (preset.getValue() == value)
            {
                return true;
            }
        }

        return false;
    }

    private void onPresetModified(String presetType)
    {
        if (presetType.equals(Preset.VOLUME_PRESET))
        {
            Collections.sort(litersPresets, presetComparator);
            litersAdapter.notifyDataSetChanged();
        }
        else
        {
            Collections.sort(moneyPresets, presetComparator);
            amountsAdapter.notifyDataSetChanged();
        }
    }

    private void saveValues()
    {
        // Validar dirección Emax
        boolean isValid = emaxIpEt.validateWith(new METValidator("Campo obligatorio")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        });

        if (isValid)
        {
            savePresets();
            saveDispenserIps();
            finish();
        }
    }

    private void savePresets()
    {
        Set<String> moneyPrestsToSave = new HashSet<>();
        Set<String> litersPrestsToSave = new HashSet<>();

        for (int i = 0; i < 5; i++)
        {
            moneyPrestsToSave.add(String.valueOf(moneyPresets.get(i).getValue()));
            litersPrestsToSave.add(String.valueOf(litersPresets.get(i).getValue()));
        }

        // Agregar el preset de tanque lleno
        moneyPrestsToSave.add("0");
        litersPrestsToSave.add("0");

        // Almacenar en SharedPreferences
        SharedPreferencesManager.putStringSet(this, MONEY_PRESETS, moneyPrestsToSave);
        SharedPreferencesManager.putStringSet(this, VOLUME_PRESETS, litersPrestsToSave);
    }

    private void saveDispenserIps()
    {
        // Crear arreglo para almacenar información
        JSONArray dispesnerIps = new JSONArray();

        // Convertir datos de dispensarios
        try
        {
            for (DispIpInfo dispIpInfo : disps)
            {
                // Solo almacenar información válida
                if (dispIpInfo.getIpAddress() != null)
                {
                    // Crear objeto JSON
                    JSONObject dispJson = new JSONObject();
                    dispJson.put("ip", dispIpInfo.getIpAddress());
                    dispJson.put("id", dispIpInfo.getDispNumber());

                    // Guardar objeto JSON
                    dispesnerIps.put(dispJson);
                }
            }

            // Almacenar archivo
            if (!StationFileManager.saveDispensersIps(this, dispesnerIps))
            {
                String message = "Error al guardar ip's de dispensarios";
                Log.e(TAG, message);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void emaxTestConnection()
    {
        EmaxProvider.testConnection(this, new EmaxProvider.SimecobConnectionInterface()
        {
            @Override
            public void onSuccess(String response)
            {
                final int colorResource = getResources().getColor(R.color.green_money);
                final Drawable drawable = getResources().getDrawable(R.drawable.ic_check);
                final Bitmap bitmap = ChipREDConstants.convertToBitmap(drawable);

                testEmaxButton.doneLoadingAnimation(colorResource, bitmap);
                testEmaxButton.setClickable(true);
            }

            @Override
            public void onError(String message)
            {
                final int colorResource = getResources().getColor(R.color.colorPrimary);
                final Drawable drawable = getResources().getDrawable(R.drawable.ic_clear);
                final Bitmap bitmap = ChipREDConstants.convertToBitmap(drawable);

                testEmaxButton.doneLoadingAnimation(colorResource, bitmap);
                testEmaxButton.setClickable(true);
            }
        });
    }

    private void checkDispensersStatus()
    {
        for (int idx = 0; idx < disps.size(); idx++)
        {
            DispIpInfo dispIpInfo = disps.get(idx);
            if (dispIpInfo.getIpAddress() != null)
            {
                setGetWorkMode(idx, DispenserController.READ_MODE_COMMAND);
            }
        }
    }

    private void setGetWorkMode(final int listPosition, String commandToSend)
    {
        // Obtener información de dispensario
        final DispIpInfo dispIpInfo = disps.get(listPosition);

        final DispenserController dispenserController =
                new DispenserController(dispIpInfo.getIpAddress());

        dispenserController.setDispControllerInterface(new DispenserController.DispControllerInterface()
        {
            @Override
            public void onResult(String command, String response)
            {
                Log.d(TAG, "Modo de trabajo en dispensario " + dispIpInfo.getDispNumber() + " -> "
                        + response);

                if (response.equals(DispenserController.MODE_ENABLED))
                {
                    dispIpInfo.setState(DispIpInfo.ON);
                }
                else
                {
                    dispIpInfo.setState(DispIpInfo.OFF);
                }

                dispInfoAdapter.notifyItemChanged(listPosition);
            }

            @Override
            public void onError(String command, String message)
            {
                if (command.equals(DispenserController.READ_MODE_COMMAND))
                {
                    dispIpInfo.setState(DispIpInfo.UNAVAILABLE);
                    dispInfoAdapter.notifyItemChanged(listPosition);
                }
                else if (command.equals(DispenserController.SET_MODE_COMMAND))
                {
                    dispIpInfo.setState(DispIpInfo.OFF);
                    dispInfoAdapter.notifyItemChanged(listPosition);
                }
            }
        });

        if (commandToSend.equals(DispenserController.READ_MODE_COMMAND))
        {
            dispenserController.getWorkMode();
        }
        else
        {
            if (dispIpInfo.getState() == DispIpInfo.ON)
            {
                // Desactivar si estaba activado
                dispenserController.setWorkMode(DispenserController.MODE_DISABLED);
            }
            else if (dispIpInfo.getState() == DispIpInfo.OFF)
            {
                // Activar si estaba desactivado
                dispenserController.setWorkMode(DispenserController.MODE_ENABLED);
            }

            dispIpInfo.setState(DispIpInfo.IN_PROGRESS);
            dispInfoAdapter.notifyItemChanged(listPosition);
        }
    }
}