package com.binarium.chipredes.disppreset;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.R;

import java.util.ArrayList;

public class PresetAdapter extends RecyclerView.Adapter<PresetAdapter.PresetViewHolder> implements View.OnClickListener
{
    private String currencySymbol;

    private ArrayList<Preset> values;
    private View.OnClickListener listener;

    public PresetAdapter(ArrayList<Preset> values)
    {
        this.values = values;
    }

    public void setCurrencySymbol(String currencySymbol)
    {
        this.currencySymbol = currencySymbol;
    }

    @Override
    public int getItemCount()
    {
        return values.size();
    }

    @NonNull
    @Override
    public PresetViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.preset_layout,
                parent, false);

        PresetViewHolder presetViewHolder = new PresetViewHolder(v);
        v.setOnClickListener(this);

        return presetViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PresetViewHolder holder, int position)
    {
        // Obtener preset
        Preset preset = values.get(position);

        // Crear cadena
        String formattedValue;
        if (preset.getPresetType().equals(Preset.VOLUME_PRESET))
        {
            formattedValue = "L " + ChipREDConstants.NO_DECIMAL_FORMAT.format(preset.getValue());
        }
        else
        {
            formattedValue =
                    currencySymbol + " " + ChipREDConstants.NO_DECIMAL_FORMAT.format(preset.getValue());
        }

        holder.text.setText(formattedValue);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onClick(v);
        }
    }

    static class PresetViewHolder extends RecyclerView.ViewHolder
    {
        private TextView text;

        PresetViewHolder(View itemView)
        {
            super(itemView);
            text = itemView.findViewById(R.id.preset_text);
        }
    }
}
