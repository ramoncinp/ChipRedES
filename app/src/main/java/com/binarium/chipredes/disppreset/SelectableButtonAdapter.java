package com.binarium.chipredes.disppreset;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.binarium.chipredes.R;

import java.util.ArrayList;

public class SelectableButtonAdapter extends RecyclerView.Adapter<SelectableButtonAdapter
        .SelectableButtonViewHolder> implements View.OnClickListener
{
    private ArrayList<SelectableButton> values;
    private Context context;
    private View.OnClickListener listener;

    public SelectableButtonAdapter(Context context, ArrayList<SelectableButton> values)
    {
        this.context = context;
        this.values = values;
    }

    @Override
    public int getItemCount()
    {
        return values.size();
    }

    @NonNull
    @Override
    public SelectableButtonViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.selectable_button,
                parent, false);

        SelectableButtonViewHolder viewHolder = new SelectableButtonViewHolder(v);
        v.setOnClickListener(this);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SelectableButtonViewHolder holder, int position)
    {
        final SelectableButton button = values.get(position);
        holder.text.setText(button.getText());
        holder.background.setCardBackgroundColor(ContextCompat.getColor(context,
                button.getBackgroundColor()));
        holder.border.setCardBackgroundColor(ContextCompat.getColor(context, button.isSelected()
                ? R.color.yellow : R.color.white));
    }

    public void setListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onClick(v);
        }
    }

    static class SelectableButtonViewHolder extends RecyclerView.ViewHolder
    {
        private CardView background;
        private CardView border;
        private TextView text;

        SelectableButtonViewHolder(View itemView)
        {
            super(itemView);
            background = itemView.findViewById(R.id.selectable_button_body_background);
            border = itemView.findViewById(R.id.selectable_button_border);
            text = itemView.findViewById(R.id.button_text);
        }
    }
}
