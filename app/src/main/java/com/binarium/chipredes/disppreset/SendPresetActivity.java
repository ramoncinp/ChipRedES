package com.binarium.chipredes.disppreset;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.Product;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.configstation.StationFileManager;
import com.binarium.chipredes.emax.EmaxProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;

public class SendPresetActivity extends AppCompatActivity
{
    // Constantes
    private static final String TAG = SendPresetActivity.class.getSimpleName();
    private static final int MONEY_PRESET = 0;
    private static final int VOLUME_PRESET = 1;

    // Variables
    private boolean presetSelectedFromButton = false;
    private boolean tanqueLleno = false;
    private double[] prices = {0, 0, 0};
    private double[] amountEqs = {0, 0, 0};
    private double[] volumeEqs = {0, 0, 0};
    private int currentProductIdx = -1;
    private int selectedColor, unSelectedColor;
    private int loadingPosition;
    private int presetType = MONEY_PRESET;
    private double[] moneyPresets = {50000, 20000, 10000, 5000, 2000, 0};
    private double[] litersPresets = {80, 60, 40, 20, 10, 0};
    private String dispIp;

    // Objetos
    private ArrayList<Product> products;
    private Dialog progressDialog;

    // Vistas
    private Button sendPresetButton;
    private CardView tanqueLlenoCv;
    private CardView product1cv;
    private CardView product2cv;
    private CardView product3cv;
    private CardView preset1cv;
    private CardView preset2cv;
    private CardView preset3cv;
    private CardView preset4cv;
    private CardView preset5cv;
    private EditText presetEt;
    private TextView preset1text;
    private TextView preset2text;
    private TextView preset3text;
    private TextView preset4text;
    private TextView preset5text;
    private TextView product1price;
    private TextView product2price;
    private TextView product3price;
    private TextView product1eq;
    private TextView product2eq;
    private TextView product3eq;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_preset);
        setTitle("Enviar preset");

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        loadingPosition =
                getIntent().getIntExtra(ChipREDConstants.SELECTED_LOADING_POSITION, 0);

        // Obtener ip de dispensario
        getDispIp();

        // Obtener presets
        getSavedPresets();

        // Inicializar colores
        initColors();

        // Inicializar vistas
        initViews();

        // Consultar productos
        getServerProducts();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    private void getServerProducts()
    {
        EmaxProvider.getEmaxProducts(this, loadingPosition, new EmaxProvider.ProductsInterface()
        {
            @Override
            public void onFetchedProducts(ArrayList<Product> products)
            {
                SendPresetActivity.this.products = products;
                showProducts();
            }

            @Override
            public void onError(String message)
            {
                Toast.makeText(SendPresetActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initColors()
    {
        selectedColor = ContextCompat.getColor(this, R.color.yellow);
        unSelectedColor = ContextCompat.getColor(this, R.color.white);
    }

    private void initViews()
    {
        // Obtener editText para ingresar preset
        presetEt = findViewById(R.id.preset_et);
        presetEt.setRawInputType(Configuration.KEYBOARD_12KEY);

        // Obtener botón para iniciar venta
        sendPresetButton = findViewById(R.id.send_preset_button);
        sendPresetButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showConfirmationDialog();
            }
        });

        // Agregar textListener para colones
        presetEt.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                // Evaluar si se deben iluminar todos los botones
                if (!presetSelectedFromButton)
                {
                    // Volver a iluminar todos los presets
                    selectPresetView(0);

                    // Asegurar que no hay tanque lleno
                    tanqueLleno = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                if (editable.length() == 0) return;

                // Obtener valor
                String valueString = editable.length() > 0 ? editable.toString() : "0";
                double value = Double.parseDouble(valueString.replace(",", ""));

                // Calcular equivalencias
                setProductEquivalency(value);

                // Darle formato a lo escrito
                presetEt.removeTextChangedListener(this);
                presetEt.setText(ChipREDConstants.NO_DECIMAL_FORMAT.format(value));
                presetEt.setSelection(presetEt.getText().length());
                presetEt.addTextChangedListener(this);

                if (value > 0)
                    // Desactivar bandera que indica cuando la cantidad se ingresó por botón
                    presetSelectedFromButton = false;

                // Evaluar si se debe habilitar botón para enviar preset
                checkStartButtonStatus();

                // Apagar bandera
                if (presetSelectedFromButton) presetSelectedFromButton = false;
            }
        });

        // Definir clickListener para presets
        View.OnClickListener presetClickListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onPresetSelected(view.getId());
            }
        };

        // Obtener botones de presets
        preset1cv = findViewById(R.id.preset_1_cv);
        preset2cv = findViewById(R.id.preset_2_cv);
        preset3cv = findViewById(R.id.preset_3_cv);
        preset4cv = findViewById(R.id.preset_4_cv);
        preset5cv = findViewById(R.id.preset_5_cv);
        tanqueLlenoCv = findViewById(R.id.no_preset_cv);

        // Obtener textView's de presets
        preset1text = findViewById(R.id.preset_1_text);
        preset2text = findViewById(R.id.preset_2_text);
        preset3text = findViewById(R.id.preset_3_text);
        preset4text = findViewById(R.id.preset_4_text);
        preset5text = findViewById(R.id.preset_5_text);

        // Definir touchListeners
        preset1cv.setOnClickListener(presetClickListener);
        preset2cv.setOnClickListener(presetClickListener);
        preset3cv.setOnClickListener(presetClickListener);
        preset4cv.setOnClickListener(presetClickListener);
        preset5cv.setOnClickListener(presetClickListener);
        tanqueLlenoCv.setOnClickListener(presetClickListener);

        // Escribir presets
        setPresetsValues();

        // Obtener icono de tipo de preset
        final ImageView presetIcon = findViewById(R.id.preset_icon);

        // Obtener botón para cambiar de tipo de preset
        final CardView tooglePresetType = findViewById(R.id.toogle_preset);
        tooglePresetType.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Actualizar variables
                presetType = presetType == MONEY_PRESET ? VOLUME_PRESET : MONEY_PRESET;

                // Actualizar views
                int maxLength;
                if (presetType == MONEY_PRESET)
                {
                    presetIcon.setImageResource(R.drawable.ic_colon_crc_blanco);
                    maxLength = 7;
                }
                else
                {
                    presetIcon.setImageResource(R.drawable.ic_litros);
                    maxLength = 3;
                }

                InputFilter[] filterArray = new InputFilter[1];
                filterArray[0] = new InputFilter.LengthFilter(maxLength);
                presetEt.setFilters(filterArray);

                // Limpiar caja de texto
                presetEt.setText("");

                // Actualizar valores de presets
                setPresetsValues();
                setProductEquivalency(0);
            }
        });
    }

    private void showProducts()
    {
        // Obtener botones de productos
        ProgressBar progressBar = findViewById(R.id.products_progress_bar);
        progressBar.setVisibility(View.GONE);
        product1cv = findViewById(R.id.product_1_cv);
        product2cv = findViewById(R.id.product_2_cv);
        product3cv = findViewById(R.id.product_3_cv);
        product1cv.setVisibility(View.INVISIBLE);
        product2cv.setVisibility(View.INVISIBLE);
        product3cv.setVisibility(View.INVISIBLE);

        // Obtener textos dentro de los botones de productos
        product1price = findViewById(R.id.product_1_precio);
        product2price = findViewById(R.id.product_2_precio);
        product3price = findViewById(R.id.product_3_precio);
        product1eq = findViewById(R.id.product_1_eq);
        product2eq = findViewById(R.id.product_2_eq);
        product3eq = findViewById(R.id.product_3_eq);

        CardView prod1back = findViewById(R.id.product_1_background);
        CardView prod2back = findViewById(R.id.product_2_background);
        CardView prod3back = findViewById(R.id.product_3_background);
        TextView prod1desc = findViewById(R.id.product_1_text);
        TextView prod2desc = findViewById(R.id.product_2_text);
        TextView prod3desc = findViewById(R.id.product_3_text);

        for (Product product : products)
        {
            String id = product.getId();
            String productPrice =
                    "₡ " + ChipREDConstants.CR_AMOUNT_FORMAT.format(product.getLiterPrice());

            switch (id)
            {
                case "1":
                    prices[0] = product.getLiterPrice();
                    product1price.setText(productPrice);
                    product1cv.setVisibility(View.VISIBLE);
                    prod1back.setCardBackgroundColor(Color.parseColor(product.getColor()));
                    prod1desc.setText(product.getDescription());
                    break;

                case "2":
                    prices[1] = product.getLiterPrice();
                    product2price.setText(productPrice);
                    product2cv.setVisibility(View.VISIBLE);
                    prod2back.setCardBackgroundColor(Color.parseColor(product.getColor()));
                    prod2desc.setText(product.getDescription());
                    break;

                case "3":
                    prices[2] = product.getLiterPrice();
                    product3price.setText(productPrice);
                    product3cv.setVisibility(View.VISIBLE);
                    prod3back.setCardBackgroundColor(Color.parseColor(product.getColor()));
                    prod3desc.setText(product.getDescription());
                    break;
            }
        }

        // Definir listeners para productos
        View.OnClickListener productsClickListeners = new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Obtener id del producto seleccionado
                int productViewId = view.getId();

                // Mostrar selección
                selectProductCardView(productViewId);
            }
        };

        // Asignar listeners
        product1cv.setOnClickListener(productsClickListeners);
        product2cv.setOnClickListener(productsClickListeners);
        product3cv.setOnClickListener(productsClickListeners);
    }

    private Product getProduct(String id)
    {
        for (Product product : products)
        {
            if (id.equals(product.getId()))
                return product;
        }

        return null;
    }

    private void getSavedPresets()
    {
        // Definir comparador
        final Comparator<Double> valuesComparator = new Comparator<Double>()
        {
            @Override
            public int compare(Double d1, Double d2)
            {
                return Double.compare(d2, d1);
            }
        };

        // Obtener sets
        Set<String> litersSet = SharedPreferencesManager.getStringSet(this,
                SendPresetConfig.VOLUME_PRESETS);
        Set<String> amountsSet = SharedPreferencesManager.getStringSet(this,
                SendPresetConfig.MONEY_PRESETS);

        // Evaluarlos
        if (!litersSet.isEmpty())
        {
            final ArrayList<Double> litersPrestsList = new ArrayList<>();

            for (String savedPreset : litersSet)
            {
                double value = Double.parseDouble(savedPreset);
                if (value != 0) litersPrestsList.add(value);
            }

            // Ordenar
            Collections.sort(litersPrestsList, valuesComparator);

            // Asignar a arreglo
            for (int i = 0; i < 5; i++)
            {
                litersPresets[i] = litersPrestsList.get(i);
            }
        }

        if (!amountsSet.isEmpty())
        {
            final ArrayList<Double> moneyPrestsList = new ArrayList<>();
            for (String savedPreset : amountsSet)
            {
                double value = Double.parseDouble(savedPreset);
                if (value != 0) moneyPrestsList.add(value);
            }

            // Ordenar
            Collections.sort(moneyPrestsList, valuesComparator);

            // Asignar a arreglo
            for (int i = 0; i < 5; i++)
            {
                moneyPresets[i] = moneyPrestsList.get(i);
            }
        }
    }

    private void setPresetsValues()
    {
        // Definir tamaño de fuente
        if (presetType == MONEY_PRESET)
        {
            setPresetsTextSize(32);
        }
        else
        {
            setPresetsTextSize(56);
        }

        for (int i = 0; i < 5; i++)
        {
            String presetText;
            if (presetType == MONEY_PRESET)
            {
                presetText = "₡ " + ChipREDConstants.CR_AMOUNT_FORMAT.format(moneyPresets[i]);
            }
            else
            {
                presetText = "L " + ChipREDConstants.NO_DECIMAL_FORMAT.format(litersPresets[i]);
            }

            switch (i)
            {
                case 0:
                    preset1text.setText(presetText);
                    break;

                case 1:
                    preset2text.setText(presetText);
                    break;

                case 2:
                    preset3text.setText(presetText);
                    break;

                case 3:
                    preset4text.setText(presetText);
                    break;

                case 4:
                    preset5text.setText(presetText);
                    break;
            }
        }
    }

    private void setPresetsTextSize(int size)
    {
        int resource = size == 32 ? R.dimen.preset_money_text : R.dimen.preset_liters_text;

        preset1text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(resource));
        preset2text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(resource));
        preset3text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(resource));
        preset4text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(resource));
        preset5text.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(resource));
    }

    private void onPresetSelected(int viewId)
    {
        // Activar bandera de que se seleccionó un preset
        presetSelectedFromButton = true;

        // Evaluar y definir si es tanque lleno
        tanqueLleno = (viewId == tanqueLlenoCv.getId());

        // Iluminar cardView
        selectPresetView(viewId);

        // Escribir cantidad seleccionada
        writeQuantityToEditText(viewId);
    }

    private void selectPresetView(int viewId)
    {
        // Iluminar preset seleccionado o si viewId == 0, iluminar todos sin fondo amarillo
        preset1cv.setAlpha(viewId == preset1cv.getId() || viewId == 0 ? 1.0f : 0.5f);
        preset2cv.setAlpha(viewId == preset2cv.getId() || viewId == 0 ? 1.0f : 0.5f);
        preset3cv.setAlpha(viewId == preset3cv.getId() || viewId == 0 ? 1.0f : 0.5f);
        preset4cv.setAlpha(viewId == preset4cv.getId() || viewId == 0 ? 1.0f : 0.5f);
        preset5cv.setAlpha(viewId == preset5cv.getId() || viewId == 0 ? 1.0f : 0.5f);
        tanqueLlenoCv.setAlpha(viewId == tanqueLlenoCv.getId() || viewId == 0 ? 1.0f : 0.5f);

        // Cambiar color de fondo si es un id específico
        preset1cv.setCardBackgroundColor(viewId == preset1cv.getId() ? selectedColor :
                unSelectedColor);
        preset2cv.setCardBackgroundColor(viewId == preset2cv.getId() ? selectedColor :
                unSelectedColor);
        preset3cv.setCardBackgroundColor(viewId == preset3cv.getId() ? selectedColor :
                unSelectedColor);
        preset4cv.setCardBackgroundColor(viewId == preset4cv.getId() ? selectedColor :
                unSelectedColor);
        preset5cv.setCardBackgroundColor(viewId == preset5cv.getId() ? selectedColor :
                unSelectedColor);
        tanqueLlenoCv.setCardBackgroundColor(viewId == tanqueLlenoCv.getId() ? selectedColor :
                unSelectedColor);
    }

    private void selectProductCardView(int viewId)
    {
        if (viewId == R.id.product_1_cv) {
            currentProductIdx = 0;
            product1cv.setAlpha(1.0f);
            product1cv.setCardBackgroundColor(selectedColor);
            product1price.setVisibility(View.VISIBLE);
            product1eq.setVisibility(tanqueLleno ? View.INVISIBLE : View.VISIBLE);
            product2cv.setAlpha(0.5f);
            product2cv.setCardBackgroundColor(unSelectedColor);
            product2price.setVisibility(View.INVISIBLE);
            product2eq.setVisibility(View.INVISIBLE);
            product3cv.setAlpha(0.5f);
            product3cv.setCardBackgroundColor(unSelectedColor);
            product3price.setVisibility(View.INVISIBLE);
            product3eq.setVisibility(View.INVISIBLE);
        } else if (viewId == R.id.product_2_cv) {
            currentProductIdx = 1;
            product1cv.setAlpha(0.5f);
            product1cv.setCardBackgroundColor(unSelectedColor);
            product1price.setVisibility(View.INVISIBLE);
            product1eq.setVisibility(View.INVISIBLE);
            product2cv.setAlpha(1.0f);
            product2cv.setCardBackgroundColor(selectedColor);
            product2price.setVisibility(View.VISIBLE);
            product2eq.setVisibility(tanqueLleno ? View.INVISIBLE : View.VISIBLE);
            product3cv.setAlpha(0.5f);
            product3cv.setCardBackgroundColor(unSelectedColor);
            product3price.setVisibility(View.INVISIBLE);
            product3eq.setVisibility(View.INVISIBLE);
        } else if (viewId == R.id.product_3_cv) {
            currentProductIdx = 2;
            product1cv.setAlpha(0.5f);
            product1cv.setCardBackgroundColor(unSelectedColor);
            product1price.setVisibility(View.INVISIBLE);
            product1eq.setVisibility(View.INVISIBLE);
            product2cv.setAlpha(0.5f);
            product2cv.setCardBackgroundColor(unSelectedColor);
            product2price.setVisibility(View.INVISIBLE);
            product2eq.setVisibility(View.INVISIBLE);
            product3cv.setAlpha(1.0f);
            product3cv.setCardBackgroundColor(selectedColor);
            product3price.setVisibility(View.VISIBLE);
            product3eq.setVisibility(tanqueLleno ? View.INVISIBLE : View.VISIBLE);
        }

        // Evaluar si se debe habilitar botón para enviar preset
        checkStartButtonStatus();
    }

    private void setProductEquivalency(double equivalency)
    {
        for (int i = 0; i < 3; i++)
        {
            String eqText = "";
            if (presetType == MONEY_PRESET)
            {
                // El valor de entrada es dinero.. convertir a Litros
                double liters = equivalency / prices[i];

                // Escribir valor con formato
                eqText = "L " + ChipREDConstants.VOLUME_FORMAT.format(liters);

                // Definir valores
                amountEqs[i] = equivalency;
                volumeEqs[i] = liters;
            }
            else
            {
                // El valor de entrada es volumen.. convertir a dinero
                double amount = equivalency * prices[i];

                // Escribir valor con formato
                eqText = "L " + ChipREDConstants.VOLUME_FORMAT.format(equivalency);

                // Definir valores
                amountEqs[i] = amount;
                volumeEqs[i] = equivalency;
            }

            // Cuando es tanque lleno o no es el producto seleccionado, se hace invisible la
            // equivalencia
            switch (i)
            {
                case 0:
                    if (product1eq != null)
                    {
                        product1eq.setText(eqText);
                        product1eq.setVisibility(tanqueLleno || currentProductIdx != i ?
                                View.INVISIBLE
                                : View.VISIBLE);
                    }
                    break;

                case 1:
                    if (product2eq != null)
                    {
                        product2eq.setText(eqText);
                        product2eq.setVisibility(tanqueLleno || currentProductIdx != i ?
                                View.INVISIBLE
                                : View.VISIBLE);
                    }
                    break;

                case 2:
                    if (product3eq != null)
                    {
                        product3eq.setText(eqText);
                        product3eq.setVisibility(tanqueLleno || currentProductIdx != i ?
                                View.INVISIBLE
                                : View.VISIBLE);
                    }
                    break;
            }
        }
    }

    private void writeQuantityToEditText(int viewId)
    {
        presetEt.setText("");

        int presetIdx = 0;
        if (viewId == R.id.preset_2_cv) {
            presetIdx = 1;
        } else if (viewId == R.id.preset_3_cv) {
            presetIdx = 2;
        } else if (viewId == R.id.preset_4_cv) {
            presetIdx = 3;
        } else if (viewId == R.id.preset_5_cv) {
            presetIdx = 4;
        } else if (viewId == R.id.no_preset_cv) {
            presetIdx = 5;
        }

        double value = presetType == MONEY_PRESET ? moneyPresets[presetIdx] :
                litersPresets[presetIdx];
        presetEt.setText(String.valueOf(value));
    }

    private void showConfirmationDialog()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View content = getLayoutInflater().inflate(R.layout.dialog_confirm_preset, null);

        // Obtener referencias de objetos
        final CardView productBackground = content.findViewById(R.id.selected_product_background);
        final TextView amount = content.findViewById(R.id.preset_amount);
        final TextView volume = content.findViewById(R.id.volume_preset);
        final TextView fullTank = content.findViewById(R.id.full_tank_text);
        final TextView selectedProductText = content.findViewById(R.id.selected_product_text);
        final Button returnButton = content.findViewById(R.id.return_button);
        final Button submitButton = content.findViewById(R.id.submit_button);

        if (tanqueLleno)
        {
            fullTank.setVisibility(View.VISIBLE);
            amount.setVisibility(View.GONE);
            volume.setVisibility(View.GONE);
        }
        else
        {
            fullTank.setVisibility(View.GONE);
            amount.setVisibility(View.VISIBLE);
            volume.setVisibility(View.VISIBLE);

            // Mostrar valores
            String amountText =
                    "₡ " + ChipREDConstants.CR_AMOUNT_FORMAT.format(amountEqs[currentProductIdx]);
            String volumeText =
                    "L " + ChipREDConstants.VOLUME_FORMAT.format(volumeEqs[currentProductIdx]);
            amount.setText(amountText);
            volume.setText(volumeText);
        }

        // Obtener producto
        Product selectedProduct = getProduct(String.valueOf(currentProductIdx + 1));
        productBackground.setCardBackgroundColor(Color.parseColor(selectedProduct.getColor()));
        selectedProductText.setText(selectedProduct.getDescription());

        // Definir contenido en diálogo
        builder.setView(content);

        final Dialog dialog = builder.create();
        dialog.show();

        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Toast.makeText(SendPresetActivity.this, "Enviando preset...", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                sendPreset();
            }
        });
        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
            }
        });
    }

    private void getDispIp()
    {
        String dispIp = StationFileManager.getDispenserIp(this, loadingPosition);

        if (dispIp == null)
        {
            ChipREDConstants.showMessageInDialog("Error", "No se encontró dirección para el " +
                    "dispensario seleccionado", this);
        }
        else
        {
            this.dispIp = dispIp;
        }
    }

    private void sendPreset()
    {
        final DispenserController dispenserController = new DispenserController(dispIp);
        dispenserController.setDispControllerInterface(new DispenserController.DispControllerInterface()
        {
            @Override
            public void onResult(String command, String response)
            {
                if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
            }

            @Override
            public void onError(String command, String message)
            {
                showProgressDialog(message);
            }
        });

        // Crear preset
        double presetValue = presetType == MONEY_PRESET ? amountEqs[currentProductIdx] :
                volumeEqs[currentProductIdx];

        Preset preset = new Preset(
                tanqueLleno ? 0 : presetValue,
                presetType == MONEY_PRESET ? Preset.AMOUNT_PRESET : Preset.VOLUME_PRESET);

        // Si es par, es lado 2
        int side = loadingPosition % 2 == 0 ? 2 : 1;

        // Enviar preset
        dispenserController.sendPreset(preset, side, currentProductIdx + 1,
                prices[currentProductIdx]);

        // Mostrar progress
        showProgressDialog(null);
    }

    private void checkStartButtonStatus()
    {
        boolean canEnable = currentProductIdx != -1 && presetEt.length() != 0;
        sendPresetButton.setAlpha(canEnable ? 1.0f : 0.5f);
        sendPresetButton.setEnabled(canEnable);
    }

    private void showProgressDialog(String resultMessage)
    {
        if (progressDialog == null)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View content = getLayoutInflater().inflate(R.layout.dialog_progress, null);
            builder.setView(content);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialogInterface)
                {
                    finish();
                }
            });

            progressDialog = builder.create();
            progressDialog.show();
        }

        if (resultMessage != null)
        {
            // Obtener views
            ProgressBar progressBar = progressDialog.findViewById(R.id.progress_bar);
            TextView text = progressDialog.findViewById(R.id.text);

            // Ocultar progress
            progressBar.setVisibility(View.GONE);
            text.setVisibility(View.VISIBLE);
            text.setText(resultMessage);
        }
    }
}