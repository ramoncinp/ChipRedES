package com.binarium.chipredes.disppreset;

public class DispIpInfo
{
    public static final int IN_PROGRESS = 0;
    public static final int ON = 1;
    public static final int OFF = 2;
    public static final int UNAVAILABLE = 2;

    private int state = IN_PROGRESS;
    private int dispNumber;
    private String ipAddress;

    public DispIpInfo()
    {
    }

    public int getState()
    {
        return state;
    }

    public void setState(int state)
    {
        this.state = state;
    }

    public int getDispNumber()
    {
        return dispNumber;
    }

    public void setDispNumber(int dispNumber)
    {
        this.dispNumber = dispNumber;
    }

    public String getIpAddress()
    {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }
}
