package com.binarium.chipredes.disppreset;

import android.util.Log;

import com.binarium.chipredes.TcpClient;

import java.util.Locale;

public class DispenserController
{
    private static final String TAG = DispenserController.class.getSimpleName();
    private static final int DISPENSER_PORT = 11011;

    public static final String READ_MODE_COMMAND = "RDMODE";
    public static final String SET_MODE_COMMAND = "STMODE";
    public static final String AUTH_SALE_COMMAND = "AUTHSL";
    public static final String MODE_ENABLED = "ENABLE";
    public static final String MODE_DISABLED = "DISABLE";


    // Variables
    private String command;
    private String host;

    // Objetos
    private DispControllerInterface dispControllerInterface;

    public DispenserController(String host)
    {
        this.host = host;
    }

    public void setDispControllerInterface(DispControllerInterface dispControllerInterface)
    {
        this.dispControllerInterface = dispControllerInterface;
    }

    public void getWorkMode()
    {
        // Definir comando
        command = READ_MODE_COMMAND;
        sendMessage(READ_MODE_COMMAND);
    }

    public void setWorkMode(String mode)
    {
        // Definir comando
        command = SET_MODE_COMMAND;

        // Construir petición
        String request = String.format(Locale.getDefault(), "%s|%s|", SET_MODE_COMMAND, mode);

        // Enviar mensaje
        sendMessage(request);
    }

    public void sendPreset(Preset preset, int side, int productId, double unitPrice)
    {
        // Construir cadena a enviar
        String presetString = String.format(Locale.getDefault(),
                "%s|%d|%d|%d|%.2f|%.2f|",
                AUTH_SALE_COMMAND,
                side,
                productId,
                preset.getPresetType().equals(Preset.AMOUNT_PRESET) ? 0 : 1,
                preset.getValue(),
                unitPrice);

        // Definir comando
        command = AUTH_SALE_COMMAND;

        // Enviar mensaje
        sendMessage(presetString);
    }

    private void sendMessage(String message)
    {
        TcpClient tcpClient = new TcpClient(host, DISPENSER_PORT);
        tcpClient.setTcpInterface(new TcpClient.TcpInterface()
        {
            @Override
            public void onResponse(String message)
            {
                decodeMessage(message);
            }

            @Override
            public void onError()
            {
                dispControllerInterface.onError(command, "Error al enviar mensaje");
            }
        });
        tcpClient.execute(message);
    }

    private void decodeMessage(String response)
    {
        Log.d(TAG, response);
        int idx1 = response.indexOf('|');
        int idx2 = response.indexOf('|', idx1 + 1);
        int idx3 = response.indexOf('|', idx2 + 1);
        if (idx3 >= response.length()) idx3 = response.length() - 1;

        String command = response.substring(0, idx1);
        String status = response.substring(idx1 + 1, idx2);
        String message = response.substring(idx2 + 1, idx3);

        String errorMessage;
        String responseMessage;
        switch (command)
        {
            case READ_MODE_COMMAND:
                errorMessage = message.isEmpty() ? "Error al leer modo de trabajo" : message;
                responseMessage = message;
                break;

            case SET_MODE_COMMAND:
                errorMessage = message.isEmpty() ? "Error al definir modo de trabajo" : message;
                responseMessage = message;
                break;

            case AUTH_SALE_COMMAND:
                errorMessage = message.isEmpty() ? "Error al enviar preset" : message;
                responseMessage = "Preset enviado correctamente";
                break;

            default:
                errorMessage = "Error";
                responseMessage = "Comando enviado";
                break;
        }

        if (!status.equals("0"))
        {
            dispControllerInterface.onError(command, errorMessage);
        }
        else
        {
            dispControllerInterface.onResult(command, responseMessage);
        }
    }

    public interface DispControllerInterface
    {
        void onResult(String command, String response);

        void onError(String command, String message);
    }
}
