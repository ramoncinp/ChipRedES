package com.binarium.chipredes;

public class Sale {
    private String id;
    private String fecha;
    private String hora;
    private String numeroTicket;
    private String producto;
    private String posicionDeCarga;
    private String fechaHora;
    private String pumper;
    private String paymentMethodCode;
    private String pumperId;
    private String taxReceiptCode;
    private String comments;
    private String token;
    private String tokenId;
    private String tokenTransaction;
    private int numProdcuto;
    private int numDispensario;
    private double cantidad;
    private double precioUnitario;
    private double costo;

    public Sale() {

    }

    public String getTaxReceiptCode() {
        return taxReceiptCode;
    }

    public void setTaxReceiptCode(String taxReceiptCode) {
        this.taxReceiptCode = taxReceiptCode;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setNumeroTicket(String numeroTicket) {
        this.numeroTicket = numeroTicket;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNumProducto(int numProdcuto) {
        this.numProdcuto = numProdcuto;
    }

    public void setNumDispensario(int numDispensario) {
        this.numDispensario = numDispensario;
    }

    public void setPosicionDeCarga(String posicionDeCarga) {
        this.posicionDeCarga = posicionDeCarga;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getCosto() {
        return costo;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public String getNumeroTicket() {
        return numeroTicket;
    }

    public String getProducto() {
        return producto;
    }

    public String getId() {
        return id;
    }

    public int getNumProdcuto() {
        return numProdcuto;
    }

    public int getNumDispensario() {
        return numDispensario;
    }

    public String getPosicionDeCarga() {
        return posicionDeCarga;
    }

    public String getFechaHora() {
        return fechaHora.replace('T', ' ');
    }

    public String getPumper() {
        return pumper;
    }

    public void setPumper(String pumper) {
        this.pumper = pumper;
    }

    public String getPaymentMethodCode() {
        if (paymentMethodCode == null) return "00";
        else return paymentMethodCode;
    }

    public void setPaymentMethodCode(String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }

    public String getPumperId() {
        return pumperId;
    }

    public void setPumperId(String pumperId) {
        this.pumperId = pumperId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getToken() {
        if (token == null) return "";
        else return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenTransaction() {
        return tokenTransaction == null ? "" : tokenTransaction;
    }

    public String getTokenId() {
        return tokenId == null ? "" : tokenId;
    }

    public void setTokenTransaction(String tokenTransaction) {
        this.tokenTransaction = tokenTransaction;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }
}
