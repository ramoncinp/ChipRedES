package com.binarium.chipredes.searchclient

import android.content.SharedPreferences
import androidx.lifecycle.*
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.wolke2.domain.usecases.SearchLocalClientUseCase
import com.binarium.chipredes.wolke.models.LocalClientData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.mapLatest
import timber.log.Timber
import javax.inject.Inject

private const val SEARCH_DELAY_MILLIS = 500L

@FlowPreview
@ExperimentalCoroutinesApi
@HiltViewModel
class SearchLocalClientViewModel @Inject constructor(
    private val searchLocalClientUseCase: SearchLocalClientUseCase,
    sharedPreferences: SharedPreferences,
) : ViewModel() {

    val stationId: String =
        sharedPreferences.getString(ChipREDConstants.STATION_MONGO_ID, "").toString()

    private val _searchingClient = MutableLiveData<Boolean>()
    val searchingClient: LiveData<Boolean> = _searchingClient

    val queryChannel = BroadcastChannel<String>(Channel.CONFLATED)
    private val _clients = queryChannel
        .asFlow()
        .debounce(SEARCH_DELAY_MILLIS)
        .mapLatest {
            if (it.isEmpty()) emptyList()
            else searchClient(it)
        }
        .catch {
            Timber.e("Error searching clients")
            _searchingClient.value = false
        }
    val queriedClients = _clients.asLiveData()

    init {
        _searchingClient.value = false
    }

    suspend fun searchClient(query: String): List<LocalClientData> {
        Timber.i("Querying client -> $query")
        _searchingClient.value = true
        val clients = viewModelScope.async { searchLocalClientUseCase(query, stationId) }
        val result = clients.await()
        _searchingClient.value = false

        return result
    }
}
