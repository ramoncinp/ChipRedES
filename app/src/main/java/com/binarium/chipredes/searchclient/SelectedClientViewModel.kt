package com.binarium.chipredes.searchclient

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.binarium.chipredes.wolke.models.LocalClientData

class SelectedClientViewModel : ViewModel() {
    val selectedClient = MutableLiveData<LocalClientData>()
}
