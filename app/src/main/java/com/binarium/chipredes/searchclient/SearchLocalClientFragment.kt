@file:OptIn(ObsoleteCoroutinesApi::class)

package com.binarium.chipredes.searchclient

import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.binarium.chipredes.R
import com.binarium.chipredes.addstationaccount.LocalClientAdapter
import com.binarium.chipredes.databinding.FragmentSearchClientBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.ObsoleteCoroutinesApi
import timber.log.Timber

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class SearchLocalClientFragment : Fragment() {

    // Objetos
    private val viewModel: SearchLocalClientViewModel by viewModels()
    private val selectedClientViewModel: SelectedClientViewModel by activityViewModels()

    // Views
    private var searchView: SearchView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        setHasOptionsMenu(true)
        activity?.title = "Buscar cliente"

        val binding: FragmentSearchClientBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_search_client, container, false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.queriedClientsList.adapter = LocalClientAdapter(
            LocalClientAdapter.OnClickListener {
                Timber.i("Cliente seleccionado ${it.email} / ${it.idCuenta}")
                selectedClientViewModel.selectedClient.value = it
                view?.findNavController()?.popBackStack()
            }
        )

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_client_menu, menu)
        val searchMenuItem = menu.findItem(R.id.action_search)
        searchView = searchMenuItem.actionView as SearchView?
        searchView?.isIconified = false
        searchView?.queryHint = "Cliente"
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                viewModel.queryChannel.trySend(query ?: "").isSuccess
                return true
            }
        })

        menu.performIdentifierAction(R.id.action_search, Menu.FLAG_PERFORM_NO_CLOSE)
    }

    override fun onResume() {
        super.onResume()
        requireActivity().onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    requireActivity().finish()
                }
            })
    }
}
