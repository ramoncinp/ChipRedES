package com.binarium.chipredes;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipredes.billets.ui.BilletSaleActivity;
import com.binarium.chipredes.cash.LastSalesActivity;
import com.binarium.chipredes.disppreset.SendPresetActivity;
import com.binarium.chipredes.tokencash.ui.tokencashactivity.TokenCashActivity;
import com.binarium.chipredes.utils.DebouncedOnClickListener;

import java.util.ArrayList;

public class PaymentMethodsList extends Fragment {
    public static final int REQUEST_CASH_PAYMENT = 1000;
    public static final int REQUEST_TOKENCASH_PAYMENT = 1001;
    public static final int REQUEST_READ_BILLETS = 1002;
    public static final int REQUEST_SEND_PRESET = 1003;

    private RecyclerView recyclerView;
    private int loadingPosition;

    public PaymentMethodsList() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_payment_methods_list, container, false);
        recyclerView = v.findViewById(R.id.payment_method_list);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            loadingPosition = bundle.getInt(ChipREDConstants.SELECTED_LOADING_POSITION);
        }

        setPaymentMethods();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CASH_PAYMENT:
            case REQUEST_TOKENCASH_PAYMENT:
            case REQUEST_SEND_PRESET:
                getActivity().finish();
                break;

            case REQUEST_READ_BILLETS:
                if (resultCode == getActivity().RESULT_OK) {
                    getActivity().finish();
                }
                break;
        }
    }

    private void setPaymentMethods() {
        final ArrayList<String> descriptions = new ArrayList<>();
        ArrayList<Integer> images = new ArrayList<>();

        //Añadir a la lista solo las integraciones habilitadas
        String[][] mIntegrations = ChipREDConstants.INTEGRATIONS;
        for (String[] integrationArray : mIntegrations) {
            Integration integration = new Integration(integrationArray[0], integrationArray[1],
                    getContext());
            integration.setImageResource(integrationArray[2]);

            if (integration.isEnabled()) {
                int resource = getContext().getResources().getIdentifier(integration
                        .getImageResource(), "mipmap", getContext().getPackageName());
                descriptions.add(integration.getName());
                images.add(resource);
            }
        }

        if (descriptions.isEmpty()) {
            TextView noPaymentMethods = getActivity().findViewById(R.id.no_payment_methods);
            noPaymentMethods.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            ListElementsAdapter listElementsAdapter = new ListElementsAdapter(descriptions,
                    images);

            listElementsAdapter.setOnClickListener(new DebouncedOnClickListener(3000) {
                @Override
                public void onDebouncedClick(View v) {
                    String desc = descriptions.get(recyclerView.getChildAdapterPosition(v));

                    if (desc.contains(ChipREDConstants.INTEGRATIONS[0][0])) {
                        Intent readBilletsIntent = new Intent(getActivity(), BilletSaleActivity.class);
                        readBilletsIntent.putExtra(ChipREDConstants.SELECTED_LOADING_POSITION,
                                loadingPosition);
                        startActivityForResult(readBilletsIntent, REQUEST_READ_BILLETS);
                    } else if (desc.contains(ChipREDConstants.INTEGRATIONS[1][0])) {
                        //Iniciar fragment de venta en efectivo
                        Intent cashIntent = new Intent(getActivity(), LastSalesActivity.class);
                        cashIntent.putExtra(ChipREDConstants.SELECTED_LOADING_POSITION,
                                loadingPosition);
                        startActivityForResult(cashIntent, REQUEST_CASH_PAYMENT);
                    } else if (desc.contains(ChipREDConstants.INTEGRATIONS[2][0])) {
                        //Iniciar fragment de pago con token
                        Intent cashIntent = new Intent(getActivity(), TokenCashActivity.class);
                        cashIntent.putExtra(ChipREDConstants.SELECTED_LOADING_POSITION,
                                loadingPosition);
                        startActivityForResult(cashIntent, REQUEST_TOKENCASH_PAYMENT);
                    } else if (desc.contains(ChipREDConstants.INTEGRATIONS[3][0])) {
                        //Iniciar fragment de pago con token
                        Intent presetIntent = new Intent(getActivity(), SendPresetActivity.class);
                        presetIntent.putExtra(ChipREDConstants.SELECTED_LOADING_POSITION,
                                loadingPosition);
                        startActivityForResult(presetIntent, REQUEST_SEND_PRESET);
                    }
                }
            });

            recyclerView.setAdapter(listElementsAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(linearLayoutManager);
        }
    }
}
