package com.binarium.chipredes;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SharedPreferencesManager
{
    protected Context context;
    private SharedPreferences sharedPreferences;

    public SharedPreferencesManager(Context mContext)
    {
        this.context = mContext;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getStringFromSP(String mKey, String defaultValue)
    {
        return sharedPreferences.getString(mKey, defaultValue);
    }

    public static String getString(Context context, String key)
    {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(key, "");
    }

    public static String getString(Context context, String key, String defaultValue)
    {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(key, defaultValue);
    }


    public static Set<String> getStringSet(Context context, String key)
    {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getStringSet(key, new HashSet<String>());
    }

    public static boolean getBoolean(Context context, String mKey)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(mKey, false);
    }

    public static boolean getBoolean(Context context, String mKey, boolean defaultValue)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(mKey,
                defaultValue);
    }

    public int getIntFromSP(String mKey, int defaultValue)
    {
        return sharedPreferences.getInt(mKey, defaultValue);
    }

    public boolean getBooleanFromSP(String mKey, boolean defaultValue)
    {
        return sharedPreferences.getBoolean(mKey, defaultValue);
    }

    public float getDoubleFromSP(String mKey, float defaultValue)
    {
        return sharedPreferences.getFloat(mKey, defaultValue);
    }

    public long getLongFromSP(String mKey, long defaultValue)
    {
        return sharedPreferences.getLong(mKey, defaultValue);
    }

    public void putStringInSP(String mKey, String value)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(mKey, value).apply();
    }

    public static void putString(Context context, String key, String value)
    {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(key, value)
                .apply();
    }

    public static void putBoolean(Context context, String key, boolean value)
    {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(key, value)
                .apply();
    }

    public static void putStringSet(Context context, String key, Set<String> value)
    {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putStringSet(key, value)
                .apply();
    }

    public void putIntInSP(String mKey, int value)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(mKey, value).apply();
    }

    public void putBooleanInSP(String mKey, boolean value)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(mKey, value).apply();

    }

    public void putFloatInSP(String mKey, float value)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(mKey, value).apply();
    }

    public void putLongInSP(String mKey, long value)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(mKey, value).apply();
    }

    public String backupSPData()
    {
        File backupPath = Environment.getExternalStorageDirectory();
        backupPath = new File(backupPath.getAbsolutePath() + "/download");

        if (!backupPath.exists())
        {
            backupPath.mkdirs();
        }

        File mFile = new File(backupPath, "MySharedPreferences.txt");
        try
        {
            FileWriter fw = new FileWriter(mFile);
            PrintWriter pw = new PrintWriter(fw);

            Map<String, ?> prefsMap = sharedPreferences.getAll();

            for (Map.Entry<String, ?> entry : prefsMap.entrySet())
            {
                pw.println(entry.getKey() + ":" + entry.getValue().toString() + ":" + entry.getValue().getClass().toString());
            }

            pw.close();
            fw.close();

            return "Archivo creado";
        }
        catch (Exception e)
        {
            // what a terrible failure...
            Log.wtf(getClass().getName(), e.toString());
            return "Error al crear el archivo";
        }
    }

    private StringBuilder readSPData()
    {
        File backupPath = Environment.getExternalStorageDirectory();
        backupPath = new File(backupPath.getAbsolutePath() + "/download");

        if (!backupPath.exists())
        {
            backupPath.mkdirs();
        }

        File mFile = new File(backupPath, "MySharedPreferences.txt");
        StringBuilder mText = new StringBuilder();

        try
        {
            BufferedReader br = new BufferedReader(new FileReader(mFile));
            String line;

            while ((line = br.readLine()) != null)
            {
                mText.append(line);
                mText.append('\n');
            }

            br.close();
        }
        catch (Exception e)
        {
            // what a terrible failure...
            Log.wtf(getClass().getName(), e.toString());
            mText = null;
            return mText;
        }

        return mText;
    }

    public String importData()
    {
        String mSpData = readSPData().toString();

        if (mSpData != null) //Si sí encontró el archivo
        {
            while (mSpData.contains("\n"))
            {
                String theKey = mSpData.substring(0, mSpData.indexOf(":"));
                String trimmedString = mSpData.substring(mSpData.indexOf(":") + 1,
                        mSpData.indexOf("\n"));
                String theValue = trimmedString.substring(0, trimmedString.indexOf(":"));
                String theType = trimmedString.substring(trimmedString.indexOf(":") + 1,
                        trimmedString.length());

                if (theType.contains("String"))
                {
                    putStringInSP(theKey, theValue);
                }
                else if (theType.contains("Integer"))
                {
                    putIntInSP(theKey, Integer.parseInt(theValue));
                }
                else if (theType.contains("Boolean"))
                {
                    putBooleanInSP(theKey, Boolean.parseBoolean(theValue));
                }
                else if (theType.contains("Long"))
                {
                    putLongInSP(theKey, Long.parseLong(theValue));
                }

                if (mSpData.indexOf("\n") + 1 < mSpData.length())
                {
                    mSpData = mSpData.substring(mSpData.indexOf("\n") + 1);
                }
                else
                {
                    break;
                }
            }

            return "Datos importados";
        }
        else
        {
            return "Error al importar datos";
        }
    }

    public boolean isContained(String varName)
    {
        return sharedPreferences.contains(varName);
    }

    public void remove(String varName)
    {
        if (isContained(varName))
        {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(varName).apply();
        }
    }

    public static boolean isContained(Context context, String pref)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).contains(pref);
    }

    public static void remove(Context context, String pref)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove(pref).apply();
    }
}
