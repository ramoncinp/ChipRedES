package com.binarium.chipredes.assignvehicleqr;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDClient;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.R;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.github.leandroborgesferreira.loadingbutton.customViews.CircularProgressButton;

public class AssignVehicleQRForm extends AppCompatActivity implements ChipRedManager.OnMessageReceived
{
    //Variables
    private boolean selectedClient = false, byId = false, invalid = false;
    private int selectedFilterIdx = ChipRedManager.BYTAXCODE;
    private String readQr;
    private String selectedClientId = "";
    private static final String[] filterOptions = {"Por cédula", "Por email", "Por " +
            "nombre"};

    //Views
    private Button closeActivity;
    private Button readAnotherQR;
    private CardView onResultCv;
    private CardView selectFilterCv;
    private CircularProgressButton submitButton;
    private ConstraintLayout content;
    private LinearLayout progressLayout;
    private LinearLayout carClientLayout;
    private LinearLayout qrDataForm;
    private MaterialEditText readQrTv;
    private MaterialEditText platesTv;
    private MaterialEditText carNumberTv;
    private MaterialEditText odometerTv;
    private MaterialEditText marchamoTv;
    private MaterialAutoCompleteTextView searchClientEt;
    private ProgressDialog progressDialog;
    private TextView resultTv;

    //Objetos
    private ArrayList<ChipREDClient> chipREDClients;
    private ArrayList<String> clientsIds;
    private ArrayList<String> suggestions;
    private ChipREDClient chipREDClient;
    private ChipRedManager chipRedManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_qr_to_vehicle_activity);

        //Obtener dato del QR
        if (getIntent().hasExtra("qr_tag"))
        {
            readQr = getIntent().getStringExtra("qr_tag");
        }
        else
        {
            Toast.makeText(this, "Error al obtener información del código QR",
                    Toast.LENGTH_SHORT).show();
            finishActivity();
        }

        initViews();
        initAutoComplete();
        initPlatesWatcher();
        readQrTv.setText(readQr);

        chipRedManager = new ChipRedManager(this, this);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        submitButton.dispose();
    }

    private void initViews()
    {
        //Linearlayouts
        content = findViewById(R.id.content);
        qrDataForm = findViewById(R.id.qr_data_form);
        carClientLayout = findViewById(R.id.car_client_layout);
        progressLayout = findViewById(R.id.progress_layout);

        //EditText's
        readQrTv = findViewById(R.id.read_qr);
        platesTv = findViewById(R.id.client_car_plates_form);
        platesTv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        carNumberTv = findViewById(R.id.client_car_number_form);
        carNumberTv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        odometerTv = findViewById(R.id.client_car_odometer_form);
        marchamoTv = findViewById(R.id.client_car_marchamo_form);
        marchamoTv.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        searchClientEt = findViewById(R.id.car_client_et);

        //CardViews
        selectFilterCv = findViewById(R.id.select_filter_button);
        selectFilterCv.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                searchClientFilterDialog();
            }
        });
        onResultCv = findViewById(R.id.assign_vehicle_qr_result);

        //ProgressButton
        submitButton = findViewById(R.id.assign_vehicle_qr_submit);
        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (validateData())
                    confirmSelectedClient();
            }
        });

        //Button
        readAnotherQR = findViewById(R.id.read_another_qr);
        readAnotherQR.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finishActivity();
            }
        });
        closeActivity = findViewById(R.id.close_activity);
        closeActivity.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finishActivity();
            }
        });

        //TextView
        resultTv = findViewById(R.id.assign_vehicle_qr_result_text);
    }

    private void searchClientFilterDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R
                .layout.simple_list_item_1);
        arrayAdapter.add(filterOptions[0]);
        arrayAdapter.add(filterOptions[1]);
        arrayAdapter.add(filterOptions[2]);

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String selectedOption = arrayAdapter.getItem(which);

                if (selectedOption != null)
                {
                    selectFilterOption(selectedOption);
                }
                else
                {
                    Toast.makeText(AssignVehicleQRForm.this, "Error al elegir el filtro",
                            Toast.LENGTH_SHORT)
                            .show();
                }
                dialog.dismiss();
            }
        });

        builder.setTitle("Filtro de búsqueda");

        Dialog dialog = builder.create();
        dialog.show();
    }

    private void selectFilterOption(String itemName)
    {
        searchClientEt.setText("");
        if (itemName.equals(filterOptions[0]))
        {
            searchClientEt.setHint("Buscar cliente (cédula)");
            selectedFilterIdx = ChipRedManager.BYTAXCODE;
        }
        else if (itemName.equals(filterOptions[1]))
        {
            searchClientEt.setHint("Buscar cliente (email)");
            selectedFilterIdx = ChipRedManager.BYEMAIL;
        }
        else if (itemName.equals(filterOptions[2]))
        {
            searchClientEt.setHint("Buscar cliente (nombre)");
            selectedFilterIdx = ChipRedManager.BYNAME;
        }
    }

    private void initAutoComplete()
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, new String[]{});

        searchClientEt.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                if (charSequence.toString().trim().length() != 0)
                {
                    chipRedManager.searchClient(selectedFilterIdx, charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable)
            {

            }
        });

        searchClientEt.setHint("Buscar cliente (cédula)");
        searchClientEt.setThreshold(0);
        searchClientEt.setAdapter(adapter);
        searchClientEt.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                String client = (String) adapterView.getItemAtPosition(i);
                selectedClientId = clientsIds.get(i);
                chipREDClient = chipREDClients.get(i);

                if (selectedFilterIdx != ChipRedManager.BYNAME) //Si no fue buscado por nombre
                {
                    try
                    {
                        //Obtener solo el nombre
                        client = client.substring(0, client.indexOf("-") - 1);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                searchClientEt.setText(client);
                searchClientEt.clearFocus();
                selectedClient = true;

                if (getCurrentFocus() != null)
                {
                    InputMethodManager inputMethodManager =
                            (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

                    if (inputMethodManager != null)
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken()
                                , 0);
                }
            }
        });
    }

    private void initPlatesWatcher()
    {
        platesTv.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View view, boolean b)
            {
                //Si su focus cambio de verdadero a falso, pedir comparación a wolke
                if (!b)
                {
                    //Cambió su focus a falso
                    MaterialEditText editText = (MaterialEditText) view;
                    try
                    {
                        String text = editText.getText().toString();
                        chipRedManager.searchPlates(ChipRedManager.BYPLATES, text);
                    }
                    catch (NullPointerException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void showSuggestions(JSONObject response)
    {
        //inicialziar lista
        chipREDClients = new ArrayList<>();
        clientsIds = new ArrayList<>();
        suggestions = new ArrayList<>();

        //Obtener sugerencias de webservice
        try
        {
            //Obtener lista de clientes
            JSONArray clientArray = response.getJSONArray("data");

            for (int i = 0; i < clientArray.length(); i++)
            {
                JSONObject jsonClient = clientArray.getJSONObject(i);
                String suggestionString = jsonClient.getString("nombre");

                if (selectedFilterIdx == ChipRedManager.BYEMAIL)
                {
                    if (jsonClient.has("email"))
                    {
                        suggestionString += " - ";
                        suggestionString += jsonClient.getString("email");
                    }
                }
                else if (selectedFilterIdx == ChipRedManager.BYTAXCODE)
                {
                    suggestionString += " - ";
                    suggestionString += jsonClient.getString("clave_fiscal");
                }

                //Agregar valores a listas
                suggestions.add(suggestionString);
                clientsIds.add(jsonClient.getString("id"));
                chipREDClients.add(new ChipREDClient(jsonClient));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        String[] mSuggestions = suggestions.toArray(new String[]{});
        searchClientEt.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, mSuggestions));

        if (!selectedClient)
        {
            searchClientEt.showDropDown();
        }
        else
        {
            searchClientEt.clearFocus();
            if (getCurrentFocus() != null)
            {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

                if (inputMethodManager != null)
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken()
                            , 0);
            }
            selectedClient = false;
        }
    }

    private void setVehicleQR()
    {
        //Obtener datos de los EditText
        String plates = "";
        String carNumber = "";
        String marchamo = "";
        int odometer = 0;

        if (platesTv.getText() != null)
        {
            plates = platesTv.getText().toString();
        }

        if (carNumberTv.getText() != null)
        {
            carNumber = carNumberTv.getText().toString();
        }

        if (odometerTv.getText() != null)
        {
            if (odometerTv.getText().length() != 0)
            {
                odometer = Integer.parseInt(odometerTv.getText().toString());
            }
        }

        if (marchamoTv.getText() != null)
        {
            marchamo = marchamoTv.getText().toString();
        }

        //Armar objeto JSON
        JSONObject data = new JSONObject();
        try
        {
            data.put("qr_tag", readQr);
            data.put("placas", plates);
            data.put("numero_economico", carNumber);
            data.put("primer_km", String.valueOf(odometer));
            data.put("primer_marchamo", marchamo);
            data.put("id_cliente", selectedClientId);

            //Mostrar progress
            progressDialog = new ProgressDialog(AssignVehicleQRForm.this);
            progressDialog.setMessage("");
            progressDialog.show();

            //Enviar servicio
            submitButton.startAnimation();
            chipRedManager.setVehicleQR(data);
        }
        catch (JSONException e)
        {
            showErrorMessage("Error al procesar la información, intente otra vez.");
        }
    }

    private void showSetQrResult(String message, boolean success)
    {
        //Mostrar texto
        resultTv.setText(message);

        //Definir color de botón en base a la respuesta
        if (success)
        {
            onResultCv.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_ok));
        }
        else
        {
            onResultCv.setCardBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }

        submitButton.setVisibility(View.GONE);
        submitButton.revertAnimation();
        onResultCv.setVisibility(View.VISIBLE);
        //readAnotherQR.setVisibility(View.VISIBLE);
    }

    public boolean validateData()
    {
        boolean isValid = platesTv.validateWith(new METValidator("Campo obligatorio")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        });

        isValid &= searchClientEt.validateWith(new METValidator("Seleccionar cliente")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        });

        return isValid && !invalid;
    }

    public void confirmSelectedClient()
    {
        //Validar si el cliente ya tiene estatus activo
        //Si esta pendiente o su direccion de correo esta vacía
        if (chipREDClient == null)
        {
            searchClientEt.setError("Seleccionar cliente");
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Asignar vehículo");
        View content = getLayoutInflater().inflate(R.layout.dialog_confirm_client, null);

        TextView emailToAssign = content.findViewById(R.id.email_to_assign);
        emailToAssign.setText(chipREDClient.getEmail());
        TextView textToShow = content.findViewById(R.id.text);
        textToShow.setText("Se asignará el vehículo a la siguiente cuenta:");

        builder.setPositiveButton("Asignar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                setVehicleQR();
            }
        });

        builder.setNegativeButton("Regresar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        Dialog dialog;
        builder.setView(content);
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void showErrorMessage(String message)
    {
        ChipREDConstants.showSnackBarMessage(message, this);
    }

    private void comparePlates(JSONObject response)
    {
        //Obtener placa
        String foundPlate;

        try
        {
            //Obtener arreglo de coincidencias
            JSONArray array = response.getJSONArray("data");

            for (int i = 0; i < array.length(); i++)
            {
                //Obtener la placa del objeto encontrado
                foundPlate = array.getJSONObject(i).getString("placas");

                //Si es igual, indicar que ya existe y salir
                if (foundPlate.equals(platesTv.getText().toString()))
                {
                    GenericDialog dialog = new GenericDialog("Asignar vehículo", "Las placas ya " +
                            "están registradas", new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            //Nada
                        }
                    }, null, this);

                    dialog.setPositiveText("Ok");
                    dialog.show();

                    platesTv.setError("Las placas ya están registradas");
                    invalid = true;
                    return;
                }
            }
        }
        catch (JSONException | NullPointerException e)
        {
            e.printStackTrace();
        }

        invalid = false;
    }

    @Override
    public void chipRedMessage(String crMessage, int webServiceN)
    {
        if (progressDialog != null) progressDialog.dismiss();
        if (webServiceN == ChipRedManager.SET_VEHICLE_QR)
        {
            showSetQrResult(crMessage, false);
            selectedClient = false;
        }
        else if (webServiceN == ChipRedManager.SEARCH_PLATES)
        {
            invalid = false;
        }
    }

    @Override
    public void chipRedError(String errorMessage, int webServiceN)
    {
        if (progressDialog != null) progressDialog.dismiss();
        if (webServiceN == ChipRedManager.SET_VEHICLE_QR)
        {
            showSetQrResult(errorMessage, false);
            selectedClient = false;
        }
    }

    @Override
    public void showResponse(JSONObject response, int webServiceN)
    {
        switch (webServiceN)
        {
            case ChipRedManager.SET_VEHICLE_QR:

                if (progressDialog != null) progressDialog.dismiss();
                String message = "Código QR asignado";
                try
                {
                    message = response.getString("message");
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                showSetQrResult(message, true);
                break;

            case ChipRedManager.SEARCH_CLIENT_WS:
                showSuggestions(response);
                selectedClient = false;
                break;

            case ChipRedManager.SEARCH_PLATES:
                comparePlates(response);
                break;
        }
    }

    public void finishActivity()
    {
        Intent qrData = new Intent();
        setResult(0, qrData);
        finish();
    }
}
