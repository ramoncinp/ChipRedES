package com.binarium.chipredes.assignvehicleqr;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.barcode.QRScannerFragment;
import com.binarium.chipredes.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

//<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a
// href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is
// licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
// title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>

public class AssignVehicleQR extends AppCompatActivity implements QRScannerFragment.QRInterface,
        ChipRedManager.OnMessageReceived
{
    //Constantes
    public static final int ASSIGN_QR = 0;
    public static final int UNASSIGN_QR = 1;

    //Variables
    private int action = 0;
    private String readQr;

    //Views
    private Dialog progressDialog;
    private Dialog vehicleInfoDialog;

    private LinearLayout vehicleForm;
    private ProgressBar dialogProgressBar;

    //Objetos
    private ChipRedManager chipRedManager;

    //Fragments
    private QRScannerFragment qrScannerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assing_vehicle_qr);

        //Obtener acción
        if (getIntent().hasExtra("action"))
        {
            action = getIntent().getIntExtra("action", ASSIGN_QR);
        }

        //Inicializar ChipREDManager
        chipRedManager = new ChipRedManager(this, this);

        setScanQRFragment();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0)
        {
            //Viene de la actividad de asignar datos
            qrScannerFragment.setDetected(false);
        }
    }

    private void setScanQRFragment()
    {
        qrScannerFragment = new QRScannerFragment();
        qrScannerFragment.setQrInterface(this);

        Bundle arguments = new Bundle();
        if (action == 0)
            arguments.putString("text", getResources().getString(R.string.assign_qr_text));
        else
            arguments.putString("text", getResources().getString(R.string.unassign_qr_text));

        qrScannerFragment.setArguments(arguments);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, qrScannerFragment).commit();
    }

    @Override
    public void onReadQR(String readQr)
    {
        this.readQr = readQr;

        //Mostrar dialogo con progress activado
        showInProgressDialog();

        if (action == ASSIGN_QR)
        {
            //Validar QR
            chipRedManager.checkVehicleQR(readQr);
        }
        else if (action == UNASSIGN_QR)
        {
            //Buscar placas para desasignar
            chipRedManager.searchPlates(ChipRedManager.BYID, readQr);
        }
    }

    private void showInProgressDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View content = getLayoutInflater().inflate(R.layout.dialog_progress, null);
        builder.setView(content);

        progressDialog = builder.create();
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void showForm()
    {
        //Pasar a actividad con formulario
        Intent intent = new Intent(this, AssignVehicleQRForm.class);
        intent.putExtra("qr_tag", readQr);
        startActivityForResult(intent, 0);
    }

    private void showVehicleInfo(JSONObject response)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //Obtener layout
        View content = getLayoutInflater().inflate(R.layout.dialog_search_unassign_vehicle, null);

        //Obtener LinearLayout de formulario
        vehicleForm = content.findViewById(R.id.vehicle_form);

        //Obtener EditText's
        MaterialEditText platesEt = content.findViewById(R.id.client_car_plates_form);
        MaterialEditText carNumberEt = content.findViewById(R.id.client_car_number_form);
        MaterialEditText clientNameEt = content.findViewById(R.id.client_car_name);
        MaterialEditText clientEmailEt = content.findViewById(R.id.client_car_email);

        //Hacer nullos sus "keyListeners"
        platesEt.setKeyListener(null);
        carNumberEt.setKeyListener(null);
        clientNameEt.setKeyListener(null);
        clientEmailEt.setKeyListener(null);

        //Obtener linearLayout del formulario
        vehicleForm = content.findViewById(R.id.vehicle_form);

        //Obtener progressBar
        dialogProgressBar = content.findViewById(R.id.progress_bar);

        String plates = "";
        String carNumber = "";
        String clientName = "";
        String clientEmail = "";

        try
        {
            JSONObject data = response.getJSONArray("data").getJSONObject(0);
            plates = data.getString("placas");
            carNumber = data.getString("numero_economico");

            JSONObject client = data.getJSONObject("cliente");
            clientName = client.getString("nombre").trim();
            clientEmail = client.getString("email");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        platesEt.setText(plates);
        carNumberEt.setText(carNumber);
        clientEmailEt.setText(clientEmail);
        clientNameEt.setText(clientName);

        //Obtener botones
        Button returnButton = content.findViewById(R.id.return_button);
        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                qrScannerFragment.setDetected(false);
                vehicleInfoDialog.dismiss();
            }
        });

        Button unassignButton = content.findViewById(R.id.unassign_button);
        unassignButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Mostrar progress
                dialogProgressBar.setVisibility(View.VISIBLE);

                //Ocultar linearLayout de formulario
                vehicleForm.setVisibility(View.GONE);

                //Enviar webService
                chipRedManager.unsetVehicleQR(readQr);
            }
        });

        //Asignar view al objeto "builder"
        builder.setView(content);

        //Crear AlertDialog
        vehicleInfoDialog = builder.create();
        vehicleInfoDialog.setCancelable(false);
        vehicleInfoDialog.show();
    }

    private void showUnassignResult(String message, boolean success)
    {
        //Obtener Views
        TextView resultText = vehicleInfoDialog.findViewById(R.id.result_text);
        resultText.setText(message);

        if (success)
        {
            CardView checkedCv = vehicleInfoDialog.findViewById(R.id.checked_cv);
            checkedCv.setVisibility(View.VISIBLE);
        }

        RelativeLayout relativeLayout = vehicleInfoDialog.findViewById(R.id.result_layout);
        relativeLayout.setVisibility(View.VISIBLE);

        Button returnButton = vehicleInfoDialog.findViewById(R.id.return_button_2);
        returnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                vehicleInfoDialog.dismiss();
                qrScannerFragment.setDetected(false);
            }
        });

        //Ocultar progress
        dialogProgressBar.setVisibility(View.GONE);
    }

    private void showErrorMessageInDialog(String message)
    {
        if (progressDialog != null)
        {
            TextView textView = progressDialog.findViewById(R.id.text);
            textView.setText(message);
            textView.setVisibility(View.VISIBLE);

            ProgressBar progressBar = progressDialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.GONE);

            Button closeDialog = progressDialog.findViewById(R.id.button);
            closeDialog.setVisibility(View.VISIBLE);
            closeDialog.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    qrScannerFragment.setDetected(false);
                    progressDialog.dismiss();
                }
            });

            progressDialog.show();
        }
        else
        {
            ChipREDConstants.showSnackBarMessage(message, this);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    qrScannerFragment.setDetected(false);
                }
            }, 2000);
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void chipRedMessage(String crMessage, int webServiceN)
    {
        if (webServiceN == ChipRedManager.UN_SET_VEHICLE_QR)
        {
            showUnassignResult(crMessage, false);
        }
        else
        {
            //Mostrar error en diálogo
            showErrorMessageInDialog(crMessage);
        }
    }

    @Override
    public void chipRedError(String errorMessage, int webServiceN)
    {
        if (webServiceN == ChipRedManager.UN_SET_VEHICLE_QR)
        {
            showUnassignResult(errorMessage, false);
        }
        else
        {
            //Mostrar error en diálogo
            showErrorMessageInDialog(errorMessage);
        }
    }

    @Override
    public void showResponse(JSONObject response, int webServiceN)
    {
        if (progressDialog != null) progressDialog.dismiss();

        switch (webServiceN)
        {
            case ChipRedManager.CHECK_VEHICLE_QR:
                showForm();
                break;

            case ChipRedManager.SEARCH_PLATES:
                showVehicleInfo(response);
                break;

            case ChipRedManager.UN_SET_VEHICLE_QR:
                String message = "Código QR desasignado";
                try
                {
                    response.getString("message");
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                showUnassignResult(message, true);
                break;
        }
    }
}
