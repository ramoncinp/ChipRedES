package com.binarium.chipredes;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PumperFragment extends Fragment
{
    private static final String TAG = "FragmentDespachadores";

    private Activity activity;
    private JSONObject pumper;

    private ListView pumpersLv;
    private ListViewPumperAdapter lvAdapter;
    private List<JSONObject> pumpers;

    private PumperFragmentInterface pumperFragmentInterface;

    public PumperFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_pumper, container, false);
        pumpersLv = rootView.findViewById(R.id.cash_pumpers_list_view);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // Obtener actividad padre
        activity = getActivity();

        // Definir título
        activity.setTitle("Despachadores");

        // Mostrar que se esta obteniendo información
        pumperFragmentInterface.requestingInfo();

        ChipRedManager chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                Toast.makeText(activity, crMessage, Toast.LENGTH_SHORT).show();
                pumperFragmentInterface.gottenInfo();
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                TextView noPumpers = activity.findViewById(R.id.no_pumpers);
                noPumpers.setVisibility(View.VISIBLE);
                pumpersLv.setVisibility(View.GONE);

                GenericDialog genericDialog = new GenericDialog("Obtener Despachadores",
                        errorMessage, new Runnable()
                {
                    @Override
                    public void run()
                    {
                        activity.finish();
                    }
                }, null, activity);

                genericDialog.setCancelable(false);
                genericDialog.show();

                pumperFragmentInterface.gottenInfo();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                showPumpersList(response);
            }
        }, activity);

        chipRedManager.getPumpers();
    }

    private void showPumpersList(JSONObject jsonObject)
    {
        try
        {
            Log.d(TAG, jsonObject.toString(2));

            JSONObject data = jsonObject.getJSONObject("data");
            JSONArray pumpersArray = data.getJSONArray("despachadores");

            ArrayList<String> names = new ArrayList<>();
            ArrayList<String> initials = new ArrayList<>();

            //Ordenar despachadores por orden alfabético
            sortPumpersByName(pumpersArray);

            //Agregar valores a las listas
            for (int x = 0; x < pumpers.size(); x++)
            {
                names.add(x, pumpers.get(x).getString("nombre"));
                initials.add(x, pumpers.get(x).getString("iniciales"));
            }

            lvAdapter = new ListViewPumperAdapter(activity, names, initials);
            pumpersLv.setAdapter(lvAdapter);

            //Definir onItemClickListener
            setListOnItemSelected();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            pumperFragmentInterface.gottenInfo();
        }

        pumpersLv.setVisibility(View.VISIBLE);
        pumperFragmentInterface.gottenInfo();
    }

    private void sortPumpersByName(JSONArray arrDesp)
    {
        pumpers = new ArrayList<>();
        try
        {
            for (int i = 0; i < arrDesp.length(); i++)
            {
                pumpers.add(arrDesp.getJSONObject(i));
            }

            Collections.sort(pumpers, new JSONComparator());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void setListOnItemSelected()
    {
        pumpersLv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                try
                {
                    //Asignar el despachador de la clase
                    pumper = pumpers.get(i);

                    //Validar el id de despachador seleccionado con el del que hizo la venta
                    String lastSalePumperId = pumperFragmentInterface.getSalePumperId();
                    String selectedPumperId = pumper.getString("id_emax");

                    //La actividad no necesita un despachador específico
                    if (lastSalePumperId == null)
                    {
                        askPumperNip();
                    }
                    else //Esperar un despachador específico
                    {
                        if (lastSalePumperId.equals(selectedPumperId))
                        {
                            //Ids coinciden
                            //Asignar despachador a la venta
                            pumperFragmentInterface.setPumperNameOrTag(pumper.getString("nombre"));
                            askPumperNip();
                        }
                        else
                        {
                            //Ids no coinciden
                            Toast.makeText(activity, "No coincide despachador",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    private void askPumperNip()
    {
        try
        {
            ChipREDConstants.showKeyboard(getActivity());

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            final LayoutInflater inflater = activity.getLayoutInflater();
            View dialogNip = inflater.inflate(R.layout.dialog_nip, null);
            final EditText etNipD = dialogNip.findViewById(R.id.etNipC);

            builder.setTitle("Ingresar NIP de despachador: " + pumper.get("nombre"));
            builder.setView(dialogNip);
            builder.setPositiveButton("INGRESAR", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    try
                    {
                        if (etNipD.getText().toString().equals(pumper.get("nip").toString()))
                        {
                            pumperFragmentInterface.setJsonPumper(pumper);
                            pumperFragmentInterface.setNextFragment();
                        }
                        else
                        {
                            Toast.makeText(activity, "No coincide NIP", Toast.LENGTH_SHORT)
                                    .show();

                            dialog.dismiss();
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        dialog.dismiss();
                    }
                }
            });

            builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public void setPumperFragmentInterface(PumperFragmentInterface pumperFragmentInterface)
    {
        this.pumperFragmentInterface = pumperFragmentInterface;
    }

    public interface PumperFragmentInterface
    {
        void requestingInfo();

        void gottenInfo();

        String getSalePumperId();

        void setNextFragment();

        void setPumperNameOrTag(String pumperNameOrTag);

        void setJsonPumper(JSONObject pumper);
    }

    private class JSONComparator implements Comparator<JSONObject>
    {
        public int compare(JSONObject a, JSONObject b)
        {
            try
            {
                String nombreA = a.getString("nombre");
                String nombreB = b.getString("nombre");

                return nombreA.compareTo(nombreB);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
                return 0;
            }
        }
    }
}
