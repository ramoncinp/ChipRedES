package com.binarium.chipredes;

public class Vehicle
{
    private String odometer = "";
    private String id;
    private String plate;
    private String color;
    private String model;
    private String company;
    private String productDescription;
    private String productId;
    private String marchamo = "";
    private String carNumber;
    private String client;
    private String lastMarchamo = "";
    private String lastOdometer = "";

    public Vehicle()
    {
    }

    public String getOdometer()
    {
        return odometer;
    }

    public void setOdometer(String odometer)
    {
        this.odometer = odometer;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getPlate()
    {
        return plate;
    }

    public void setPlate(String plate)
    {
        this.plate = plate;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getProductDescription()
    {
        return productDescription;
    }

    public void setProductDescription(String productDescription)
    {
        this.productDescription = productDescription;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getClient()
    {
        return client;
    }

    public void setClient(String client)
    {
        this.client = client;
    }

    public String getMarchamo()
    {
        return marchamo;
    }

    public void setMarchamo(String marchamo)
    {
        this.marchamo = marchamo;
    }

    public String getCarNumber()
    {
        return carNumber;
    }

    public void setCarNumber(String carNumber)
    {
        this.carNumber = carNumber;
    }

    public String getLastMarchamo()
    {
        return lastMarchamo;
    }

    public void setLastMarchamo(String lastMarchamo)
    {
        this.lastMarchamo = lastMarchamo;
    }

    public String getLastOdometer()
    {
        return lastOdometer;
    }

    public void setLastOdometer(String lastOdometer)
    {
        this.lastOdometer = lastOdometer;
    }

    public String getPerformance(double liters)
    {
        //Obtener valores enteros
        int kmA = 0;
        int kmB = 0;

        if (!odometer.equals(""))
        {
            kmA = Integer.parseInt(odometer);
        }

        if (!lastOdometer.equals(""))
        {
            kmB = Integer.parseInt(lastOdometer);
        }

        //Checar si hubo conversion exitosa
        if (kmA == 0 && kmB == 0)
        {
            return "0";
        }
        else
        {
            if (kmB > kmA) return "0";
            else return ChipREDConstants.MX_AMOUNT_FORMAT.format((kmA - kmB) / liters) + " Km/L";
        }
    }

    public double getPerformanceValue(double liters)
    {
        //Obtener valores enteros
        int kmA = 0;
        int kmB = 0;

        if (!odometer.equals(""))
        {
            kmA = Integer.parseInt(odometer);
        }

        if (!lastOdometer.equals(""))
        {
            kmB = Integer.parseInt(lastOdometer);
        }

        //Checar si hubo conversion exitosa
        if (kmA == 0 && kmB == 0)
        {
            return 0;
        }
        else
        {
            return (kmA - kmB) / liters;
        }
    }
}
