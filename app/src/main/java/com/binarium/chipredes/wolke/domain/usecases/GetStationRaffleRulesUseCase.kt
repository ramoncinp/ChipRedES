package com.binarium.chipredes.wolke.domain.usecases

import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.RaffleRulesData
import com.binarium.chipredes.wolke.models.RaffleRulesRequest
import javax.inject.Inject

class GetStationRaffleRulesUseCase @Inject constructor(
    private val getStationMongoIdUseCase: GetStationMongoIdUseCase,
    private val wolkeApiService: WolkeApiService
) {

    suspend operator fun invoke(): RaffleRulesData? {
        val stationId = getStationMongoIdUseCase()
        return fetchRaffleData(stationId)
    }

    private suspend fun fetchRaffleData(stationId: String): RaffleRulesData? {
        val raffleRulesRequest = RaffleRulesRequest(
            stationId = stationId
        )

        val raffleDataResponse = try {
            wolkeApiService.getRaffleRules(raffleRulesRequest)
        } catch (e: Exception) {
            null
        }

        return raffleDataResponse?.body()?.data
    }
}
