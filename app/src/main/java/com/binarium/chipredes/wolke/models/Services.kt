package com.binarium.chipredes.wolke.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize
import java.util.*


/** MODELOS DE SERVICIOS **/

class QueriedClient(map: Map<*, *>) {
    var id: String = map["id"] as String
    var nombre: String = map["nombre"] as String
    var email: String = map["email"] as String
    var claveFiscal: String = map["clave_fiscal"] as String
    var idCuenta: String? = map["id_cuenta"] as String?
}

data class ResendConfirmationEmailPost(
    val key: String = "reenviar_correo_confirmacion",
    @Json(name = "id_cliente") val clientId: String,
)

data class TestConnectionPost(
    val key: String = "prueba_conexion",
)

data class SearchClientByEmailPost(
    val key: String = "buscar_cliente",
    val email: String,
)

data class SearchClientByNamePost(
    val key: String = "buscar_cliente",
    @Json(name = "nombre_cliente") val name: String,
)

data class SearchClientByTaxCodePost(
    val key: String = "buscar_cliente",
    @Json(name = "cedula_cliente") val taxCode: String,
)

data class SearchStationClientByEmailPost(
    val key: String = "buscar_cliente",
    val email: String,
    @Json(name = "id_estacion") val stationId: String,
)

data class SearchStationClientByNamePost(
    val key: String = "buscar_cliente",
    @Json(name = "nombre_cliente") val name: String,
    @Json(name = "id_estacion") val stationId: String,
)

data class SearchStationClientByTaxCodePost(
    val key: String = "buscar_cliente",
    @Json(name = "cedula_cliente") val taxCode: String,
    @Json(name = "id_estacion") val stationId: String,
)

data class GetBilletDataPost(
    val key: String = "consultar_datos_billete",
    @Json(name = "id_billete") val idBillete: String? = null,
)

data class GetBilletDataResult(
    val message: String? = null,
    val data: BilletData? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null,
)

@Parcelize
data class BilletData(
    val folio: String? = null,
    val estatus: String? = null,
    @Json(name = "fecha_alta") val fechaAlta: String? = null,
    @Json(name = "fecha_uso") val fechaUso: String? = null,
    val combustible: String? = null,
    @Json(name = "id_dispositivo") val idDispositivo: String? = null,
    val monto: Double? = null,
    val origen: String? = null,
    @Json(name = "monto_uso") val montoUso: Double? = null,
    val id: String,
    @Json(name = "id_cliente") val idCliente: String? = null,
    @Json(name = "fecha_cancelacion") val fechaCancelacion: String? = null,
) : Parcelable

data class SearchStationClientByIdPost(
    val key: String = "buscar_cliente",
    @Json(name = "id_cliente") val clientId: String? = null,
    @Json(name = "id_estacion") val stationId: String,
)

data class SearchClientResult(
    val message: String? = null,
    val data: List<LocalClientData>? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null,
)

@Parcelize
data class LocalClientData(
    @Json(name = "id_cuenta") val idCuenta: String? = null,
    val estatus: String?,
    val vehiculo: Vehiculo? = null,
    val email: String? = null,
    @Json(name = "id_cliente_local") val idClienteLocal: String? = null,
    val saldo: Double? = null,
    val nombre: String? = null,
    @Json(name = "captura_placas") val capturaPlacas: Boolean? = null,
    val id: String? = null,
    @Json(name = "clave_fiscal") val claveFiscal: String? = null,
) : Parcelable {
    override fun toString(): String {
        return "$nombre - $email"
    }
}

@Parcelize
data class Vehiculo(
    val nip: String? = null,
    val placas: String? = null,
    @Json(name = "tipo_vehiculo") val tipoVehiculo: String? = null,
    @Json(name = "no_tag") val noTag: String? = null,
    @Json(name = "consumo_maximo") val consumoMaximo: Long? = null,
) : Parcelable

data class GetRegistroVentaEfectivoPost(
    val key: String = "consultar_registro_venta_efectivo",
    @Json(name = "numero_estacion") val numeroEstacion: String,
)

data class GetRegistroVentaEfectivoResult(
    val message: String? = null,
    val data: RegistroVentaEfectivo? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null,
)

data class RegistroVentaEfectivo(
    @Json(name = "registro_venta_efectivo") val registroVentaEfectivo: Boolean,
)

data class SetRegistroVentaEfectivoResult(
    val message: String? = null,
    val data: Map<*, *>? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null,
)

data class SetRegistroVentaEfectivoPost(
    val key: String = "definir_registro_venta_efectivo",
    @Json(name = "numero_estacion") val numeroEstacion: String,
    val activado: Boolean,
)

data class StationGeneralDataResponse(
    val message: String? = null,
    @Json(name = "data") val data: StationGeneralData? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null,
)

data class StationGeneralData(
    @Json(name = "datos_generales") val generalData: ChipRedStation,
)

data class ChipRedStation(
    val id: String,
    @Json(name = "rfc_cedula") val rfcCedula: String,
    val direccion: Direccion,
    @Json(name = "nombre_estacion") val nombreEstacion: String,
    @Json(name = "id_estacion") val idEstacion: String,
    val telefono: Telefono,
    val email: String,
    @Json(name = "registro_venta_efectivo") val registroVentaEfectivo: Boolean,
)

data class Direccion(
    @Json(name = "numero_int") val numeroInt: String,
    @Json(name = "numero_ext") val numeroEXT: String,
    val colonia: String,
    val calle: String,
    @Json(name = "codigo_postal") val codigoPostal: String,
    val pais: String,
    val estado: String,
    val municipio: String,
)

data class Telefono(
    val codigo: String,
    val numero: String,
)

data class StationsPumpersResponse(
    val message: String? = null,
    @Json(name = "data") val data: StationPumpersData? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null,
)

data class StationPumpersData(
    @Json(name = "despachadores") val pumpers: List<Pumper>? = null,
)

@Parcelize
data class Pumper(
    val iniciales: String? = null,
    @Json(name = "id_emax") val idEmax: String? = null,
    val nip: String? = null,
    val tag: String? = null,
    val nombre: String? = null,
) : Parcelable {
    override fun toString(): String {
        return "$iniciales - ${nombre?.uppercase(Locale.ROOT)}"
    }
}

data class ChipRedResponse(
    val message: String,
    val data: List<Map<*, *>>?,
    val jsonapi: JsonApiData,
    val response: String,
)

data class JsonApiData(
    val version: String,
)
