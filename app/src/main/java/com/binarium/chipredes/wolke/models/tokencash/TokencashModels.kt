package com.binarium.chipredes.wolke.models.tokencash

import com.binarium.chipredes.wolke.models.JsonApiData
import com.squareup.moshi.Json

data class TicketLessCoupon(
    @Json(name = "id_consumo") val idConsumo: String? = null,
    val cupon: Long? = null,
    val id: String? = null,
    val importe: Double? = null
)

data class GetCouponResponse(
    val message: String? = null,
    val data: TicketLessCoupon? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null
)

data class GetCouponRequest(
    val key: String = "obtener_cupon",
    @Json(name = "id_consumo") val idConsumo: String? = null,
    @Json(name = "numero_estacion") val numeroEstacion: String? = null
)

data class GetTokenCashConfigRequest(
    val key: String = "obtener_parametros_tokencash",
    @Json(name = "numero_estacion") val numeroEstacion: String
)

data class GetTokenCashConfigResponse(
    val message: String? = null,
    val data: TokenCashConfigurationData? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null
)

data class TokenCashConfigurationData(
    @Json(name = "maximo_porcentaje_cobro") val maximoPorcentajeCobro: String? = null,
    @Json(name = "impresion_token_cobro") val impresionTokenCobro: Boolean? = null,
    @Json(name = "tipo_emision") val tipoEmision: Int? = null,
    val emitir: Boolean? = null,
    @Json(name = "monto_maximo") val montoMaximo: Double? = null,
    @Json(name = "tipo_cliente") val tipoCliente: List<Int>? = null,
    val editables: Editables? = null,
    val porcentaje: Long? = null,
    val productos: List<Producto>? = null,
    val dias: List<Int>? = null
)

data class Editables(
    @Json(name = "editable_2") val editable2: String? = null,
    @Json(name = "editable_1") val editable1: String? = null
)

data class Producto(
    val descripcion: String? = null,
    val activo: Boolean? = null,
    val id: Int? = null,
    val porcentaje: Double? = null
)

data class GenerateTokenRequest(
    val key: String = "generar_token",
    @Json(name = "numero_estacion") val numeroEstacion: String,
    val id: String,
    val costo: Double,
    @Json(name = "posicion_carga") val loadingPosition: Int
)

data class GenerateTokenResponse(
    val message: String? = null,
    val data: TokenData? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null
)

data class TokenData(
    @Json(name = "id_consumo") val idConsumo: String? = null,
    val transaccion: String? = null,
    val estatus: String? = null,
    @Json(name = "numero_estacion") val numeroEstacion: String? = null,
    val costo: Double? = null,
    val token: String? = null,
    val id: String? = null
)

data class GetTokenPaymentsRequest(
    val key: String = "consultar_pagos_token",
    @Json(name = "numero_estacion") val numeroEstacion: String? = null,
    val transaccion: String?
)

data class GetTokenPaymentsResponse(
    val message: String? = null,
    @Json(name = "data") val data: TokenPaymentsData? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null
)

data class TokenPaymentsData(
    val status: String,
    val payments: List<Payment>,
    val summary: Summary
)

data class Payment(
    val transaction: String? = null,
    val reference: String? = null,
    val datetime: String? = null,
    val token: Token? = null,
    val details: Details? = null,
    val timezone: String? = null,
    @Json(name = "cash_register") val cashRegister: String? = null,
    val authorization: String? = null,
    val documento: String? = null
)

data class Details(
    val to: To? = null,
    val from: From? = null
)

data class From(
    val account: String? = null
)

data class To(
    val account: String? = null,
    @Json(name = "tip_amount") val tipAmount: Double? = null,
    val subaccount: String? = null,
    val currency: String? = null,
    @Json(name = "fee_amount") val feeAmount: String? = null,
    val amount: String? = null
)

data class Token(
    val transaction: String? = null,
    val number: String? = null
)

data class Summary(
    val currency: String? = null,
    val account: String? = null,
    val fee: Double? = null,
    val tip: Double? = null,
    val amount: Double? = null
)

data class CloseTokenRequest(
    val key: String = "cerrar_pagos_token",
    val transaccion: String,
    val payments: List<Payment>,
    val summary: Summary
)

data class CloseTokenResponse(
    val response: String?,
    val message: String?,
    val data: TokenTransactionData
)

data class TokenTransactionData(
    val transaccion: String
)
