package com.binarium.chipredes.wolke.models.exception

class WolkeException(message: String) : Exception(message)