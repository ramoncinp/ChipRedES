package com.binarium.chipredes.wolke

import com.binarium.chipredes.wolke.models.*
import com.binarium.chipredes.wolke.models.tokencash.*
import retrofit2.Response
import retrofit2.http.*

interface WolkeApiService {
    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun testConnection(@Body bodyRequest: TestConnectionPost): ChipRedResponse

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun searchStationClientById(@Body bodyRequest: SearchStationClientByIdPost): SearchClientResult

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun searchClientByEmail(@Body bodyRequest: SearchClientByEmailPost): ChipRedResponse

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun searchClientByName(@Body bodyRequest: SearchClientByNamePost): ChipRedResponse

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun searchClientByTaxCode(@Body bodyRequest: SearchClientByTaxCodePost): ChipRedResponse

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun searchStationClientByEmail(@Body bodyRequest: SearchStationClientByEmailPost): SearchClientResult

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun searchStationClientByName(@Body bodyRequest: SearchStationClientByNamePost): SearchClientResult

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun searchStationClientByTaxCode(@Body bodyRequest: SearchStationClientByTaxCodePost): SearchClientResult

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun resendConfirmationEmail(@Body bodyRequest: ResendConfirmationEmailPost): ChipRedResponse

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun getBilletData(@Body bodyRequest: GetBilletDataPost): GetBilletDataResult

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun getRegistroVentaEfectivo(@Body bodyRequest: GetRegistroVentaEfectivoPost): GetRegistroVentaEfectivoResult

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun setRegistroVentaEfectivo(@Body bodyRequest: SetRegistroVentaEfectivoPost): SetRegistroVentaEfectivoResult

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun getLastSale(@Body bodyRequest: LastSaleRequest): Response<LastSaleResponse>

    /************ TokenCash ************/
    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun getTicketLessCoupon(@Body bodyRequest: GetCouponRequest): Response<GetCouponResponse>

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun getTokenCashConfig(@Body bodyRequest: GetTokenCashConfigRequest): Response<GetTokenCashConfigResponse>

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun generateToken(@Body bodyRequest: GenerateTokenRequest): Response<GenerateTokenResponse>

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun getTokenPayments(@Body bodyRequest: GetTokenPaymentsRequest): Response<GetTokenPaymentsResponse>

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun closeToken(@Body bodyRequest: CloseTokenRequest): CloseTokenResponse

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun getRaffleRules(@Body bodyRequest: RaffleRulesRequest): Response<GetRaffleRulesResponse>

    @Headers("Content-Type: application/json")
    @POST("webservice_app/")
    suspend fun printRaffleTickets(@Body bodyRequest: RequestPrintRaffleTickets): Response<PrintRaffleTicketsResponse>
}
