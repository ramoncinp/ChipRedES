package com.binarium.chipredes.wolke.domain.usecases

import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.RequestPrintRaffleTicketResult
import com.binarium.chipredes.wolke.models.RequestPrintRaffleTickets
import java.util.*
import javax.inject.Inject

class RequestPrintRaffleTicketsUseCase @Inject constructor(
    private val getStationMongoIdUseCase: GetStationMongoIdUseCase,
    private val wolkeApiService: WolkeApiService,
) {

    suspend operator fun invoke(
        ticketNumber: String,
        saleDate: String,
    ): RequestPrintRaffleTicketResult {
        val stationId = getStationMongoIdUseCase()
        return requestPrintTickets(
            stationId,
            ticketNumber,
            saleDate.replace("T", " ")
        )
    }

    private suspend fun requestPrintTickets(
        stationId: String,
        ticketNumber: String,
        saleDate: String,
    ): RequestPrintRaffleTicketResult {
        val printTicketsPost = RequestPrintRaffleTickets(
            stationId = stationId,
            ticketNumber = ticketNumber,
            saleDate = saleDate
        )

        val printTicketsResponse = try {
            wolkeApiService.printRaffleTickets(
                printTicketsPost
            )
        } catch (e: Exception) {
            null
        }

        val response = printTicketsResponse?.body()
        val error = response?.response?.lowercase(Locale.ROOT) != "ok"
        val message = response?.message
        val tickets = response?.data?.tickets

        return RequestPrintRaffleTicketResult(
            tickets = tickets ?: 0,
            error = if (error) message else null
        )
    }
}
