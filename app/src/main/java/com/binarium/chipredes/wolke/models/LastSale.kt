package com.binarium.chipredes.wolke.models

import com.squareup.moshi.Json

data class LastSaleRequest(
    val key: String? = "ultima_venta",
    @Json(name = "id_estacion") val idEstacion: String? = null,
    @Json(name = "posicion_carga") val posicionCarga: Int? = null
)

data class LastSaleResponse(
    val message: String? = null,
    val data: LastSaleData? = null,
    val jsonapi: JsonApiData? = null,
    val response: String? = null
)

data class LastSaleData(
    @Json(name = "tag_despachador") val tagDespachador: String? = null,
    val prod: Boolean? = null,
    @Json(name = "tipo_consumo") val tipoConsumo: String? = null,
    @Json(name = "num_ticket") val numTicket: String? = null,
    val cantidad: Double? = null,
    val producto: String? = null,
    @Json(name = "numero_dispensario") val numeroDispensario: String? = null,
    @Json(name = "fecha_hora") val fechaHora: String? = null,
    val costo: Double? = null,
    @Json(name = "numero_estacion") val numeroEstacion: String? = null,
    @Json(name = "posicion_carga") val posicionCarga: String? = null,
    @Json(name = "id_despachador_emax") val idDespachadorEmax: String? = null,
    val id: String? = null,
    val combustible: String? = null,
    @Json(name = "precio_unitario") val precioUnitario: Double? = null
)