package com.binarium.chipredes.wolke.domain.usecases

import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.*
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

class SearchWolkeClientUseCase @Inject constructor(
    private val wolkeApiService: WolkeApiService,
) {

    suspend operator fun invoke(query: String): List<QueriedClient> = run {
        val queriedClients = mutableListOf<QueriedClient>()

        try {
            val clientsByEmail = searchByEmail(query)
            queriedClients.addAll(clientsByEmail)

            val clientsByName = searchByName(query)
            queriedClients.addAll(clientsByName)

            val clientsByTaxCode = searchByTaxCode(query)
            queriedClients.addAll(clientsByTaxCode)

        } catch (e: Exception) {
            Timber.e("Error searching client $query -> $e")
        }

        queriedClients.toSet().toList()
    }

    private suspend fun searchByEmail(query: String) = run {
        val request = SearchClientByEmailPost(email = query)
        val responseByEmail = wolkeApiService.searchClientByEmail(request)
        responseByEmail.dataToQueriedClients()
    }

    private suspend fun searchByName(query: String) = run {
        val request = SearchClientByNamePost(name = query)
        val responseByName = wolkeApiService.searchClientByName(request)
        responseByName.dataToQueriedClients()
    }

    private suspend fun searchByTaxCode(query: String) = run {
        val request =
            SearchClientByTaxCodePost(taxCode = query.uppercase())
        val responseByTaxCode = wolkeApiService.searchClientByTaxCode(request)
        responseByTaxCode.dataToQueriedClients()
    }

    private fun ChipRedResponse.dataToQueriedClients() = run {
        data?.map {
            QueriedClient(it)
        }?.sortedBy { it.nombre } ?: emptyList()
    }
}
