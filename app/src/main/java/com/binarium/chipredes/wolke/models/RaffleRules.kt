package com.binarium.chipredes.wolke.models

import com.binarium.chipredes.wolke.models.RafflePrintType.MANUAL
import com.squareup.moshi.Json

object RafflePrintType {
    const val AUTOMATIC = "automatico"
    const val MANUAL = "manual"
}

data class RaffleRulesRequest(
    val key: String = "consultar_reglas_sorteo",
    @Json(name = "id_estacion") val stationId: String
)

data class GetRaffleRulesResponse(
    val message: String,
    val data: RaffleRulesData?,
    val jsonapi: JsonApiData,
    val response: String
)

data class RaffleRulesData(
    @Json(name = "id_estacion") val stationId: String,
    @Json(name = "cantidad") val amount: Double,
    @Json(name = "unidad") val type: String,
    @Json(name = "activo") val isActive: Boolean,
    @Json(name = "tipo_impresion") val printType: String? = MANUAL
)

data class RequestPrintRaffleTickets(
    val key: String = "solicitar_impresion_boletos",
    @Json(name = "id_estacion") val stationId: String,
    @Json(name = "numero_ticket") val ticketNumber: String,
    @Json(name = "fecha_consumo") val saleDate: String
)

data class PrintRaffleTicketsResponse(
    val message: String,
    val data: PrintRaffleTicketsData?,
    val jsonapi: JsonApiData,
    val response: String
)

data class PrintRaffleTicketsData(
    @Json(name = "numero_impresiones") val tickets: Int
)

data class RequestPrintRaffleTicketResult(
    val tickets: Int = 0,
    val error: String? = null
)
