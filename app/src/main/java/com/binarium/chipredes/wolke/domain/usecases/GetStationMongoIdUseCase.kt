package com.binarium.chipredes.wolke.domain.usecases

import android.content.SharedPreferences
import com.binarium.chipredes.ChipREDConstants
import javax.inject.Inject

class GetStationMongoIdUseCase @Inject constructor(
    private val sharedPreferences: SharedPreferences,
) {

    operator fun invoke(): String =
        sharedPreferences.getString(ChipREDConstants.STATION_MONGO_ID, "").toString()
}
