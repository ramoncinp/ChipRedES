package com.binarium.chipredes;

import java.util.ArrayList;

public class Purchase
{
    private double amount;
    private double volume;

    private String ticketNumber;
    private String dateHour;
    private String unitPrice;
    private String pumper;
    private String productDesc;
    private String productColor;

    private ArrayList<ExtraProduct> extraProducts;

    public Purchase()
    {

    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    public double getVolume()
    {
        return volume;
    }

    public void setVolume(double volume)
    {
        this.volume = volume;
    }

    public String getTicketNumber()
    {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber)
    {
        this.ticketNumber = ticketNumber;
    }

    public String getProductDesc()
    {
        return productDesc;
    }

    public void setProductDesc(String productDesc)
    {
        this.productDesc = productDesc;
    }

    public String getProductColor()
    {
        return productColor;
    }

    public void setProductColor(String productColor)
    {
        this.productColor = productColor;
    }

    public String getDateHour()
    {
        return dateHour;
    }

    public void setDateHour(String dateHour)
    {
        this.dateHour = dateHour;
    }

    public String getUnitPrice()
    {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice)
    {
        this.unitPrice = unitPrice;
    }

    public String getPumper()
    {
        return pumper;
    }

    public void setPumper(String pumper)
    {
        this.pumper = pumper;
    }

    public ArrayList<ExtraProduct> getExtraProducts()
    {
        return extraProducts;
    }

    public void setExtraProducts(ArrayList<ExtraProduct> extraProducts)
    {
        this.extraProducts = extraProducts;
    }
}
