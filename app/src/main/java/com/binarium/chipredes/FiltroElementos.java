package com.binarium.chipredes;

import android.widget.Filter;

import java.util.ArrayList;

public class FiltroElementos extends Filter
{
    ExtraProductArrayAdapter arrayAdapter;
    ArrayList<ExtraProduct> lista;

    FiltroElementos(ArrayList<ExtraProduct> lista, ExtraProductArrayAdapter arrayAdapter)
    {
        this.arrayAdapter = arrayAdapter;
        this.lista = lista;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint)
    {
        FilterResults results = new FilterResults();

        if (constraint != null && constraint.length() > 0)
        {
            constraint = constraint.toString().toUpperCase();
            ArrayList<ExtraProduct> productosFiltrados = new ArrayList<>();

            for (int i = 0; i < lista.size(); i++)
            {
                if (lista.get(i).getDescripcion().toUpperCase().contains(constraint))
                {
                    productosFiltrados.add(lista.get(i));
                }
            }

            results.count = productosFiltrados.size();
            results.values = productosFiltrados;
        }
        else
        {
            results.count = lista.size();
            results.values = lista;
        }

        return results;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        ArrayList<ExtraProduct> listaActualizada = (ArrayList<ExtraProduct>) results.values;
        arrayAdapter.setExtraProducts(listaActualizada);
        arrayAdapter.notifyDataSetChanged();
    }
}
