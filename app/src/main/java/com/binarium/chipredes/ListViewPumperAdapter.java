package com.binarium.chipredes;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewPumperAdapter extends BaseAdapter
{
    private final Context context;
    private final ArrayList<String> nombres;
    private final ArrayList<String> iniciales;

    public ListViewPumperAdapter(Context context, ArrayList<String> nombres, ArrayList<String> iniciales)
    {
        this.context = context;
        this.nombres = nombres;
        this.iniciales = iniciales;
    }

    @Override
    public int getCount()
    {
        return nombres.size();
    }

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater;

        TextView tvNombre, tvIniciales;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        @SuppressLint("ViewHolder") View itemView = inflater.inflate(R.layout.pumper_row, parent, false);

        tvNombre = itemView.findViewById(R.id.name_tv);
        tvIniciales = itemView.findViewById(R.id.initials_tv);

        tvNombre.setText(nombres.get(position).toUpperCase());
        tvIniciales.setText(iniciales.get(position).toUpperCase());

        return itemView;
    }
}
