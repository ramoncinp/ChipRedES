package com.binarium.chipredes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.binarium.chipredes.barcode.QRScannerActivity;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.view.View.GONE;

public class AddModifyVehicle extends AppCompatActivity
{
    private static final String TAG = "AddModifyVehicle";
    private final static int ADD_VEHICLE = 1;
    private final static int MODIFY_CLIENT = 2;

    //Views
    private Button saveVehicleButton;
    private SearchView searchView;
    private CardView vehicleContentLayout;
    private CardView readClientQr;
    private LinearLayout thinkingLayout;
    private TextView thinkingText;

    //EditText
    private MaterialEditText companyEt;
    private MaterialEditText modelEt;
    private MaterialEditText platesEt;
    private MaterialEditText odometerEt;
    private MaterialEditText colorEt;
    private MaterialBetterSpinner products;
    private MaterialEditText clientEt;

    //Objetos
    private ChipRedManager chipRedManager;
    private JSONObject selectedSuggestion;
    private SimpleCursorAdapter suggestionsAdapter;
    private Vehicle vehicleToRegister;

    //Listas
    private ArrayList<String> productsDescriptions;
    private ArrayList<String> productsIds;

    //Variables
    private boolean searchByQr;
    private int selectedProduct;
    private int activityAction;

    public AddModifyVehicle()
    {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_modify_vehicle);

        searchView = findViewById(R.id.sv_vehicles);
        readClientQr = findViewById(R.id.read_client_qr);
        thinkingLayout = findViewById(R.id.vehicles_think_layout);
        thinkingText = findViewById(R.id.vehicles_think_text);
        vehicleContentLayout = findViewById(R.id.vehicle_info_content);

        //EditTexts
        companyEt = findViewById(R.id.vehicle_company);
        modelEt = findViewById(R.id.vehicle_model);
        platesEt = findViewById(R.id.vehicle_plate);
        odometerEt = findViewById(R.id.vehicle_odometer);
        colorEt = findViewById(R.id.vehicle_color);
        clientEt = findViewById(R.id.vehicle_client);

        //Asignar filtros a cada EditText
        setAllCaps(companyEt);
        setAllCaps(modelEt);
        setAllCaps(platesEt);
        setAllCaps(colorEt);

        //Saber si se va a crear o modificar vehículo
        activityAction = getIntent().getIntExtra(ChipREDConstants.ADD_OR_MODIFY_VEHICLE, 1);

        //Botones
        saveVehicleButton = findViewById(R.id.save_vehicle);
        saveVehicleButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                saveVehicleInfo();
            }
        });

        //Spinner
        products = findViewById(R.id.products_selector);
        products.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                selectedProduct = position;
            }
        });
        showThinkingLayout("Obteniendo productos de estación");

        if (activityAction == ADD_VEHICLE) setTitle("Agregar vehículo");
        else setTitle("Modificar vehículo");

        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                //Dejar de mostrar progressBar
                hideThinkingLayout();

                if (webServiceN == ChipRedManager.SEARCH_CLIENT_WS && searchByQr)
                {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddModifyVehicle
                            .this);
                    dialogBuilder.setTitle("Error");
                    dialogBuilder.setMessage(crMessage);
                    dialogBuilder.setCancelable(false);
                    dialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });

                    Dialog dialog = dialogBuilder.create();
                    dialog.show();

                    searchView.setQuery("", false);
                    vehicleContentLayout.setVisibility(View.INVISIBLE);

                    //Regresar variable a su estado normal
                    searchByQr = false;
                }
                else if (webServiceN == ChipRedManager.ADD_OR_MODIFY_VEHICLE)
                {
                    vehicleContentLayout.setVisibility(View.VISIBLE);
                    searchView.clearFocus();
                    showSnackbarMessage(crMessage);
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                //Dejar de mostrar progressBar
                hideThinkingLayout();

                //Cuando es un aviso por parte de ChipRED y se buscó cliente por QR
                if (webServiceN != ChipRedManager.SEARCH_CLIENT_WS && webServiceN !=
                        ChipRedManager.SEARCH_VEHICLE)
                {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddModifyVehicle
                            .this);
                    dialogBuilder.setTitle("Error");
                    dialogBuilder.setMessage(errorMessage);
                    dialogBuilder.setCancelable(false);
                    dialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });

                    Dialog dialog = dialogBuilder.create();
                    dialog.show();
                }

                //Regresar variable a su estado normal
                searchByQr = false;
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                //Dejar de mostrar progressBar
                hideThinkingLayout();

                //Si se respondió a buscar cliente...
                if (webServiceN == ChipRedManager.SEARCH_CLIENT_WS || webServiceN ==
                        ChipRedManager.SEARCH_VEHICLE)
                {
                    //Si se pidió buscarlo por QR
                    if (searchByQr)
                    {
                        //Regresar variable a su estado normal
                        searchByQr = false;

                        //Utilizar id de cliente leído por QR
                        try
                        {
                            assignClienteOrVehicle(response.getJSONArray("data").getJSONObject(0));
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        //Mostrar las sugerencias
                        getSuggestions(response);
                    }
                }
                else if (webServiceN == ChipRedManager.GET_STATION_PRODUCTS)
                {
                    productsDescriptions = new ArrayList<>();
                    productsIds = new ArrayList<>();

                    //Agregar "TODOS" por default
                    productsDescriptions.add("TODOS");
                    productsIds.add("0");

                    //Obtener las descripciones y Id's de cada producto de la estacion
                    try
                    {
                        JSONArray products = response.getJSONObject("data").getJSONArray
                                ("productos");
                        for (int i = 0; i < products.length(); i++)
                        {
                            productsDescriptions.add(products.getJSONObject(i).getString
                                    ("descripcion").toUpperCase());
                            productsIds.add(products.getJSONObject(i).getString("id_producto"));
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                    //Setear adaptador al Spinner
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(AddModifyVehicle.this,
                            android.R.layout.simple_dropdown_item_1line, productsDescriptions);
                    products.setAdapter(arrayAdapter);

                    //Mostrar layout para buscar clientes
                    CardView searchViewCard = findViewById(R.id.card_view_search_client_vehicles);
                    searchViewCard.setVisibility(View.VISIBLE);

                    //Dependiendo de la accion del fragment, mostrar el hint correspondiente
                    if (activityAction == MODIFY_CLIENT)
                    {
                        searchView.setQueryHint("Buscar vehículo (placas)");
                        readClientQr.setVisibility(View.GONE);
                    }
                    else
                    {
                        readClientQr.setVisibility(View.VISIBLE);
                    }

                    //Hacer focus para empezar a buscar
                    searchView.requestFocus();
                }
            }
        }, this);

        //Preparar listeners de cada objeto
        setListeners();

        //Cargar productos de estacion
        chipRedManager.getStationProducts();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ChipREDConstants.READ_QR_REQUEST)
        {
            if (resultCode == RESULT_OK)
            {
                //Usar el código
                if (data != null)
                {
                    searchByQr = true;

                    String qrCode = data.getData().toString();
                    chipRedManager.searchClient(ChipRedManager.BYID, qrCode);
                    Log.d(TAG, "Read QR: " + qrCode);
                }
            }
        }
    }

    private void setListeners()
    {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                if (activityAction == ADD_VEHICLE)
                {
                    //Enviar peticion de buscar cliente cada que cambia el texto
                    chipRedManager.searchClient(ChipRedManager.BYNAME_EMAIL, newText);
                }
                else
                {
                    //Enviar peticion de buscar un vehículo cada que cambia el texto
                    chipRedManager.searchVehicle(newText);
                }
                return false;
            }
        });

        //Adaptador para los suggestions
        final String[] from = new String[]{"item"};
        final int[] to = new int[]{android.R.id.text1};
        suggestionsAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1,
                null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        //Asignar a SearchView el adaptador recien creado
        searchView.setSuggestionsAdapter(suggestionsAdapter);

        readClientQr.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Ocultar teclado
                hideKeyboard();

                //Iniciar actividad para leer QR
                startActivityForResult(new Intent(AddModifyVehicle.this, QRScannerActivity.class)
                        , ChipREDConstants.READ_QR_REQUEST);
            }
        });
    }

    private void getSuggestions(JSONObject response)
    {
        //Crear suggestions de los clientes encontrados
        final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "item"});
        final ArrayList<JSONObject> suggestionsValues = new ArrayList<>();

        try
        {
            JSONArray arrItems = response.getJSONArray("data");
            for (int i = 0; i < arrItems.length(); i++)
            {
                suggestionsValues.add(arrItems.getJSONObject(i));

                if (activityAction == ADD_VEHICLE)
                {
                    c.addRow(new Object[]{i, arrItems.getJSONObject(i).get("nombre").toString() +
                            " - " + arrItems.getJSONObject(i).get("email").toString()});
                }
                else
                {
                    c.addRow(new Object[]{i, arrItems.getJSONObject(i).get("placas").toString() +
                            " - " + arrItems.getJSONObject(i).get("marca").toString() + " " +
                            arrItems.getJSONObject(i).get("modelo").toString()});
                }
            }
            suggestionsAdapter.changeCursor(c);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener()
        {
            @Override
            public boolean onSuggestionSelect(int position)
            {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position)
            {
                assignClienteOrVehicle(suggestionsValues.get(position));
                return false;
            }
        });
    }

    private void assignClienteOrVehicle(JSONObject jsonObject)
    {
        //Asignar a atributo de la clase
        this.selectedSuggestion = jsonObject;

        //Obtener el nombre y ponerlo en el Query del SearchView
        try
        {
            if (activityAction == ADD_VEHICLE) //Si se va a agregar vehículo, buscar el nombre de
            // cliente
            {
                searchView.setQuery(jsonObject.getString("nombre").toUpperCase(), false);
            }
            else
            {
                //Si no, buscar la descripción del vehículo
                searchView.setQuery(jsonObject.getString("placas").toUpperCase(), false);
                //Llenar formulario con la información del objeto
                vehicleToRegister = setVehicleInfo(jsonObject);
            }
            searchView.clearFocus();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        vehicleContentLayout.setVisibility(View.VISIBLE);
        saveVehicleButton.setVisibility(View.VISIBLE);
    }


    private boolean validateData()
    {
        boolean validCompany = companyEt.validateWith(new METValidator("Marca inválida")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        });

        boolean validModel = modelEt.validateWith(new METValidator("Modelo inválido")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        });

        boolean validPlates = platesEt.validateWith(new METValidator("Placas inválidas")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        });

        boolean validOdometer = odometerEt.validateWith(new METValidator("Odómetro inválido")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        });

        boolean validColor = colorEt.validateWith(new METValidator("Color inválido")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                return !isEmpty;
            }
        });

        boolean validProduct;
        String product = products.getText().toString();
        if (product.isEmpty())
        {
            validProduct = false;
            products.setError("Seleccione una opcion");
        }
        else
        {
            validProduct = true;
        }

        return validColor && validCompany && validModel && validOdometer && validPlates &&
                validProduct;
    }

    private void saveVehicleInfo()
    {
        //Ocultar teclado
        hideKeyboard();

        //Validar si se seleccionó un cliente
        if (selectedSuggestion == null)
        {
            if (activityAction == ADD_VEHICLE)
            {
                //Indicar que no hay un cliente
                showSnackbarMessage("No hay un cliente seleccionado");

                //Pedir que se elija un cliente
                searchView.requestFocus();
            }
        }
        else
        {
            //Si los datos no son válidos
            if (!validateData())
            {
                Log.e(TAG, "Datos inváldos, algunos datos no son válidos");
            }
            else
            {
                vehicleContentLayout.setVisibility(View.INVISIBLE);
                if (activityAction == ADD_VEHICLE)
                {
                    showThinkingLayout("Registrando vehículo...");
                }
                else
                {
                    showThinkingLayout("Modificando vehículo...");
                }
                try
                {
                    //Obtener el vehículo con los datos ingresados
                    if (activityAction == ADD_VEHICLE) vehicleToRegister = getVehicleInfo();
                        //Si se va a editar, pasar como parametro el objeto antes creado
                    else vehicleToRegister = getVehicleInfo(vehicleToRegister);

                    //Armar un objeto JSON para almacenar la información
                    JSONObject request = new JSONObject();
                    request.put("marca", vehicleToRegister.getCompany());
                    request.put("modelo", vehicleToRegister.getModel());
                    request.put("placas", vehicleToRegister.getPlate());
                    request.put("odometro", vehicleToRegister.getOdometer());
                    request.put("color", vehicleToRegister.getColor());
                    request.put("tipo_combustible", vehicleToRegister.getProductId());

                    if (activityAction == ADD_VEHICLE)
                    {
                        request.put("key", "registrar_vehiculo");
                        request.put("id_cliente", selectedSuggestion.getString("id"));
                        chipRedManager.addOrModifyVehicle(request, true);
                    }
                    else
                    {
                        request.put("key", "modificar_vehiculo");
                        request.put("_id", vehicleToRegister.getId());
                        chipRedManager.addOrModifyVehicle(request, false);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private Vehicle getVehicleInfo()
    {
        Vehicle vehicle = new Vehicle();
        vehicle.setCompany(companyEt.getText().toString());
        vehicle.setColor(colorEt.getText().toString());
        vehicle.setModel(modelEt.getText().toString());
        vehicle.setOdometer(odometerEt.getText().toString());
        vehicle.setPlate(platesEt.getText().toString());
        vehicle.setProductDescription(productsDescriptions.get(selectedProduct));
        vehicle.setProductId(productsIds.get(selectedProduct));

        return vehicle;
    }

    private Vehicle getVehicleInfo(Vehicle vehicle)
    {
        vehicle.setCompany(companyEt.getText().toString());
        vehicle.setColor(colorEt.getText().toString());
        vehicle.setModel(modelEt.getText().toString());
        vehicle.setOdometer(odometerEt.getText().toString());
        vehicle.setPlate(platesEt.getText().toString());
        vehicle.setProductDescription(productsDescriptions.get(selectedProduct));
        vehicle.setProductId(productsIds.get(selectedProduct));

        return vehicle;
    }

    private Vehicle setVehicleInfo(JSONObject jsonObject)
    {
        Vehicle vehicle = new Vehicle();
        try
        {
            vehicle.setId(jsonObject.getString("_id"));
            vehicle.setPlate(jsonObject.getString("placas"));
            vehicle.setModel(jsonObject.getString("modelo"));
            vehicle.setColor(jsonObject.getString("color"));
            vehicle.setCompany(jsonObject.getString("marca"));
            vehicle.setOdometer(jsonObject.getString("odometro"));
            vehicle.setClient(jsonObject.getJSONArray("cliente").getJSONObject(0).getString
                    ("nombre"));

            //Obtener numero que indica el tipo de combustible
            String fuelTypeId = jsonObject.getJSONObject("restricciones").getString
                    ("tipo_combustible");
            int fuelTyepIdx = Integer.parseInt(fuelTypeId);
            vehicle.setProductDescription(productsDescriptions.get(fuelTyepIdx));
            vehicle.setProductId(fuelTypeId);

            //Seleccionar la opcion del spinner
            products.setText(vehicle.getProductDescription());

            //Indicar a la variable correspondiente, el indice del producto seleccionado
            selectedProduct = fuelTyepIdx;

            //Llenar información de formulario
            companyEt.setText(vehicle.getCompany());
            colorEt.setText(vehicle.getColor());
            modelEt.setText(vehicle.getModel());
            platesEt.setText(vehicle.getPlate());
            odometerEt.setText(vehicle.getOdometer());

            clientEt.setVisibility(View.VISIBLE);
            clientEt.setText(vehicle.getClient());

            companyEt.setEnabled(false);
            modelEt.setEnabled(false);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return vehicle;
    }

    private void showThinkingLayout(String text)
    {
        if (text != null)
        {
            thinkingText.setText(text);
        }
        thinkingLayout.setVisibility(View.VISIBLE);
    }

    private void hideThinkingLayout()
    {
        thinkingLayout.setVisibility(GONE);
    }

    private void setAllCaps(MaterialEditText editText)
    {
        //Preparar arreglos
        InputFilter[] editFilters = editText.getFilters();
        InputFilter[] newFilters = new InputFilter[editFilters.length + 1];
        System.arraycopy(editFilters, 0, newFilters, 0, editFilters.length);

        //Agregar filtro de texto a mayuscula
        newFilters[editFilters.length] = new InputFilter.AllCaps();

        //Asignar filtro a editText
        editText.setFilters(newFilters);
    }

    public void hideKeyboard()
    {
        try
        {
            View kb = this.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) (this.getSystemService(Context
                    .INPUT_METHOD_SERVICE));
            if (imm != null && kb != null)
            {
                imm.hideSoftInputFromWindow(kb.getWindowToken(), 0);
            }
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
    }

    private void showSnackbarMessage(String text)
    {
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG).show();
    }
}
