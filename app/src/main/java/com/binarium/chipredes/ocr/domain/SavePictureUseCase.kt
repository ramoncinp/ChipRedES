package com.binarium.chipredes.ocr.domain

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class SavePictureUseCase @Inject constructor() {

    suspend operator fun invoke(data: ByteArray, baseDir: String) = withContext(Dispatchers.Default) {
        runCatching {
            // Convertir arreglo a bitmap
            var bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)

            // Rotar si es necesario
            if (bitmap.width > bitmap.height) {
                val matrix = Matrix()
                matrix.postRotate(90f)
                bitmap = Bitmap.createBitmap(
                    bitmap,
                    0, 0,
                    bitmap.width,
                    bitmap.height,
                    matrix,
                    true
                )
            }

            // Optimizar bitmap
            val newHeight = 800
            val newWidth = 500
            bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);

            // Convertir bitmap optimizado a arreglo de bytes
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)

            val optimizedData = stream.toByteArray()
            bitmap.recycle()

            // Obtener directorio del album para imagenes en datos de la aplicación
            var path = "$baseDir/imagenes/"
            val dir = File(path)
            if (!dir.exists()) {
                // Crear directorio
                if (!dir.mkdir()) {
                    Timber.e("Error al crear directorio de imagenes")
                }
            }

            path += "placas_capturadas.png"

            val outputStream = FileOutputStream(path)
            outputStream.write(optimizedData)
            outputStream.close()

            // Definir path de la imagen
            path
        }.getOrDefault("")
    }
}
