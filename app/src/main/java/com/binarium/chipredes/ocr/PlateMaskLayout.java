package com.binarium.chipredes.ocr;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.binarium.chipredes.R;

import timber.log.Timber;

public class PlateMaskLayout extends View {
    private int TopMargin = 0;
    private int RectWidth = 0;
    private int RectHeight = 0;
    private int RadiusInner = 0;
    private int RadiusOuter = 0;
    private int stroke = 0;
    private int transparentHeight = 0;
    private int mainWidth = 0;
    private int mainHeight = 0;

    private int color;

    private boolean isDrawn = false;

    private OnLayoutListener layoutListener;

    private RectF mRect;

    public PlateMaskLayout(Context context) {
        super(context);
    }

    public PlateMaskLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        Timber.i("init");
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TransparentRectangle);
        TopMargin = a.getInt(R.styleable.TransparentRectangle_TopMargin, TopMargin);
        RectWidth = a.getInt(R.styleable.TransparentRectangle_Width, RectWidth);
        RectHeight = a.getInt(R.styleable.TransparentRectangle_Height, RectHeight);
        RadiusInner = a.getInt(R.styleable.TransparentRectangle_RadiusInner,
                RadiusInner);
        RadiusOuter = a.getInt(R.styleable.TransparentRectangle_RadiusOuter,
                RadiusOuter);
        color = a.getInt(R.styleable.TransparentRectangle_Color, color);
        a.recycle();
    }

    private void defaultAttributes() {
        Timber.i("defaultAttributes");
        mainWidth = getWidth();
        mainHeight = getHeight();
        TopMargin = 0;
        RectWidth = mainWidth;
        RectHeight = mainHeight;
        RadiusInner = RectWidth / 6;
        RadiusOuter = RadiusInner + (RadiusInner / 10);
        stroke = 0;
        transparentHeight = 0;
        color = R.color.gray_medium_alpha;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Timber.v("onDraw : getWidth: " + getWidth() + ", getHeight: " + getHeight());

        if (!isDrawn)
            defaultAttributes();

        isDrawn = true;

        Bitmap bitmap = bitmapDraw();
        canvas.drawBitmap(bitmap, getWidth() / 2f - RectWidth / 2f, TopMargin, null);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        defaultAttributes();

        if (this.layoutListener != null && isDrawn)
            this.layoutListener.onLayout();

        isDrawn = true;
    }

    private Bitmap bitmapDraw() {
        Bitmap bitmap = Bitmap.createBitmap(RectWidth, RectHeight, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(Color.TRANSPARENT);

        Canvas canvasBitmap = new Canvas(bitmap);
        canvasBitmap.drawColor(getResources().getColor(color));

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        float left = mainWidth / 8f;
        float right = left * 7;
        float top = (mainHeight / 8f) * 3;
        float bottom = (mainHeight / 8f) * 5;
        RectF innerRectangle = new RectF(left, top, right, bottom);
        canvasBitmap.drawRect(innerRectangle, paint);

        // Asignar rectangulo
        mRect = new RectF(mainWidth / 8f, mainHeight / 8f * 3, mainWidth / 8f * 7, mainHeight / 8f * 5);

        // Notificar que el rectangulo que filta fue dibujado
        if (this.layoutListener != null && isDrawn)
            this.layoutListener.onLayout();

        RectF outerRectangle = new RectF(0, 0, RectWidth, transparentHeight);
        canvasBitmap.drawRect(outerRectangle, paint);

        paint.setColor(getResources().getColor(color));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(stroke);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        return bitmap;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public RectF getmRect() {
        return mRect;
    }

    public void setLayoutListener(OnLayoutListener layoutListener) {
        this.layoutListener = layoutListener;
    }

    public interface OnLayoutListener {
        void onLayout();
    }
}
