package com.binarium.chipredes.ocr;

import com.google.android.gms.vision.text.TextBlock;

import java.util.Comparator;

public class RectComparator implements Comparator<TextBlock>
{
    @Override
    public int compare(TextBlock tb1, TextBlock tb2)
    {
        return Integer.compare(tb2.getBoundingBox().height(), tb1.getBoundingBox().height());
    }
}