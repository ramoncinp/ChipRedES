package com.binarium.chipredes.ocr;

import android.graphics.RectF;
import android.util.SparseArray;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;

import java.util.ArrayList;
import java.util.Collections;

public class OcrDetectorProcessor implements Detector.Processor<TextBlock> {
    // Lista de bloques
    private final ArrayList<TextBlock> textBlocks = new ArrayList<>();
    private final GraphicOverlay<OcrGraphic> graphicOverlay;
    private OcrInterface ocrInterface;
    private RectF rectFilter;

    public OcrDetectorProcessor(GraphicOverlay<OcrGraphic> graphicOverlay) {
        this.graphicOverlay = graphicOverlay;
    }

    public void setOcrInterface(OcrInterface ocrInterface) {
        this.ocrInterface = ocrInterface;
    }

    public void setRectFilter(RectF rectFilter) {
        this.rectFilter = rectFilter;
    }

    @Override
    public void release() {
        graphicOverlay.clear();
    }

    @Override
    public void receiveDetections(Detector.Detections<TextBlock> detections) {
        textBlocks.clear();
        graphicOverlay.clear();
        SparseArray<TextBlock> items = detections.getDetectedItems();
        for (int i = 0; i < items.size(); i++) {
            TextBlock item = items.valueAt(i);
            if (item != null) {
                item.getValue();
                textBlocks.add(item);
            }
        }

        if (textBlocks.size() > 0) {
            // Ordenar
            Collections.sort(textBlocks, new RectComparator());

            // Obtener texto más grande
            TextBlock interestedText = textBlocks.get(0);

            // Obtener coordenadas
            float x = interestedText.getBoundingBox().centerX() * graphicOverlay.getWidthScaleFactor();
            float y = interestedText.getBoundingBox().centerY() * graphicOverlay.getHeightScaleFactor();

            ocrInterface.onTextDetected(interestedText);

            // Validar
            if (isInsideRectFilter(x, y)) {
                // Retornar el texto más grande
                ocrInterface.onTextDetected(interestedText);
            }
        }
    }

    private boolean isInsideRectFilter(float x, float y) {
        float maxX = rectFilter.right;
        float minX = rectFilter.left;
        float maxY = rectFilter.bottom;
        float minY = rectFilter.top;

        return (x <= maxX && x >= minX && y <= maxY && y >= minY);
    }

    public interface OcrInterface {
        void onTextDetected(TextBlock textBlock);
    }
}
