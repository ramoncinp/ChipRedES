package com.binarium.chipredes;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GenericDialog
{
    private Runnable positiveAction, negativeAction;
    private String title, message, positiveText, negativeText;
    private Context context;
    private Dialog dialog;
    boolean cancelable;

    public GenericDialog(String title,
                         String message,
                         Runnable positiveAction,
                         Runnable negativeAction,
                         Context context)
    {
        this.title = title;
        this.message = message;
        this.positiveAction = positiveAction;
        this.negativeAction = negativeAction;
        this.context = context;

        positiveText = "Ok";
        negativeText = "Cancelar";
    }

    public void setPositiveText(String positiveText)
    {
        this.positiveText = positiveText;
    }

    public void setNegativeText(String negativeText)
    {
        this.negativeText = negativeText;
    }

    public void setCancelable(boolean cancelable)
    {
        this.cancelable = cancelable;
    }

    public void show()
    {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(cancelable);

        if (positiveAction != null)
        {
            alertDialog.setPositiveButton(positiveText,
                    new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i)
                        {
                            positiveAction.run();
                        }
                    });
        }

        if (negativeAction != null)
        {
            alertDialog.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i)
                        {
                            if (negativeAction != null) negativeAction.run();
                            dialogInterface.dismiss();
                        }
                    });
        }

        dialog = alertDialog.create();
        if ((context != null))
            dialog.show();
    }

    public void showCustomDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);

        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View content = inflater.inflate(R.layout.generic_dialog, null);
        builder.setView(content);

        TextView textView = content.findViewById(R.id.text_view);
        Button positiveButton = content.findViewById(R.id.positive_button);
        Button negativeButton = content.findViewById(R.id.negative_button);

        textView.setText(message);
        positiveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                positiveAction.run();
                if (dialog != null) dialog.dismiss();
            }
        });
        positiveButton.setText(positiveText);

        negativeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                negativeAction.run();
                if (dialog != null) dialog.dismiss();
            }
        });
        negativeButton.setText(negativeText);

        dialog = builder.create();
        dialog.show();
    }

    public void dismiss()
    {
        if (dialog != null) dialog.dismiss();
    }
}
