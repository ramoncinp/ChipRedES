package com.binarium.chipredes.utils.mappers

import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke.models.QueriedClient

internal fun LocalClientData.toQueriedClient(): QueriedClient {
    val map = mapOf(
        "id" to id,
        "nombre" to nombre,
        "email" to email,
        "clave_fiscal" to claveFiscal,
        "id_cuenta" to idCuenta
    )
    return QueriedClient(map)
}
