package com.binarium.chipredes.utils.validator

import com.rengwuxian.materialedittext.MaterialEditText
import com.rengwuxian.materialedittext.validation.METValidator

internal fun MaterialEditText.validateEmpty() =
    this.validateWith(object : METValidator("Campo obligatorio") {
        override fun isValid(text: CharSequence, isEmpty: Boolean): Boolean = isEmpty.not()
    })