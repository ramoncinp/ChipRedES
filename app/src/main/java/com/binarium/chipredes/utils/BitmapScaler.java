package com.binarium.chipredes.utils;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

public class BitmapScaler implements Transformation
{
    private int height;
    private int width;

    public BitmapScaler(int height, int width)
    {
        this.height = height;
        this.width = width;
    }

    @Override
    public Bitmap transform(Bitmap source)
    {
        float heightScale;
        float widthScale;
        int newHeightSize;
        int newWidthSize;
        Bitmap scaledBitmap;

        heightScale = (float) height / source.getHeight();
        newHeightSize = Math.round(source.getWidth() * heightScale);
        widthScale = (float) width / source.getWidth();
        newWidthSize = Math.round(source.getHeight() * widthScale);
        scaledBitmap = Bitmap.createScaledBitmap(source, newWidthSize, newHeightSize, true);

        source.recycle();

        return scaledBitmap;
    }

    @Override
    public String key()
    {
        return "ImagenEscalada";
    }
}
