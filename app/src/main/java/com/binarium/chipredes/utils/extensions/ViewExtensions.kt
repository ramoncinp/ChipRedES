package com.binarium.chipredes.utils.extensions

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.utils.DatePickerFragment
import com.binarium.chipredes.utils.DateUtils
import com.rengwuxian.materialedittext.MaterialEditText

fun View.setVisible(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.setDatePicker(fragmentManager: FragmentManager, onSelected: (date: String) -> Unit) {
    setOnClickListener {
        val datePickerFragment = DatePickerFragment.newInstance { _, year, month, day ->
            val dateEt = this as MaterialEditText
            val gottenDate =
                "${ChipREDConstants.twoDigits(day)}/${DateUtils.monthNumberToString(month)}/$year"

            if (validateSelectedDate(gottenDate)) {
                dateEt.setText(gottenDate)
                onSelected(gottenDate)
            } else {
                dateEt.error = "Fecha debe ser mayor a 01/FEB/2022"
            }
        }
        datePickerFragment.show(fragmentManager, "datePicker")
    }
}

private fun validateSelectedDate(gottenDate: String): Boolean {
    val minDate = 1643695200000L // 01/FEB/2022
    val date = DateUtils.dateStringToObject("$gottenDate 00:00:00")
    return date.time > minDate
}

fun RecyclerView.addSeparators() {
    val decorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
    addItemDecoration(decorator)
}

private fun Drawable.toBitmap(): Bitmap? {
    val mutableBitmap = Bitmap.createBitmap(48, 48, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(mutableBitmap)
    setBounds(0, 0, 48, 48)
    draw(canvas)
    return mutableBitmap
}
