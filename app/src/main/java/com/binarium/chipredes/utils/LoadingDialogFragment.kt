package com.binarium.chipredes.utils

import com.binarium.chipredes.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class LoadingDialogFragment : BottomSheetDialogFragment(R.layout.loading_dialog) {

    companion object {
        const val TAG = "LoadingDialogFragment"
    }
}
