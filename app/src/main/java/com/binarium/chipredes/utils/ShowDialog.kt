package com.binarium.chipredes.utils

import android.app.AlertDialog
import android.content.Context

fun showSimpleDialog(
    context: Context,
    title: String,
    message: String,
    onPositiveClicked: (() -> Unit)? = null
) {
    val builder = AlertDialog.Builder(context)

    builder.setTitle(title)
    builder.setMessage(message)
    builder.setCancelable(false)

    builder.setPositiveButton("OK") { dialog, _ ->
        dialog.dismiss()
        onPositiveClicked?.let { it() }
    }

    builder.show()
}
