package com.binarium.chipredes.utils

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import timber.log.Timber

class QRGenerator {

    companion object {

        @JvmStatic
        fun encodeAsBitmap(data: String, height: Int, width: Int): Bitmap {
            val white = Color.WHITE
            val black = Color.BLACK

            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            val codeWriter = MultiFormatWriter()
            try {
                val bitMatrix = codeWriter.encode(data, BarcodeFormat.QR_CODE, width, height)
                for (x in 0 until width) {
                    for (y in 0 until height) {
                        bitmap.setPixel(x, y, if (bitMatrix[x, y]) black else white)
                    }
                }
            } catch (e: WriterException) {
                Timber.d("GenerateQRCode: ${e.message}")
            }
            return bitmap
        }
    }
}
