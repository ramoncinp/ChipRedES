package com.binarium.chipredes.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    public static SimpleDateFormat chipRedDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

    public static Date getFirstDayOfTheWeek() {
        // Definir periodo semanal por default
        // Crear instancia de calendario
        Calendar calendar = Calendar.getInstance();

        // Obtener el primer día de la semana por default
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        // Definir la fecha de inicio
        return calendar.getTime();
    }

    public static Date getLastDayOfTheWeek() {
        // Definir periodo semanal por default
        // Crear instancia de calendario
        Calendar calendar = Calendar.getInstance();

        // Obtener el último día de la semana por default
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        // Definir la fecha final
        return calendar.getTime();
    }

    public static Date getDeltaDate(Date dateReference, int daysDiff) {
        // Definir periodo semanal por default
        // Crear instancia de calendario
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateReference);

        // Restar días
        calendar.add(Calendar.DAY_OF_YEAR, daysDiff);

        // Definir la fecha de inicio
        return calendar.getTime();
    }

    public static Date getFirstDayOfTheMonth() {
        // Definir periodo semanal por default
        // Crear instancia de calendario
        Calendar calendar = Calendar.getInstance();

        // Obtener el primer día de la semana por default
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        // Definir la fecha de inicio
        return calendar.getTime();
    }

    public static Date getLastDayOfTheMonth() {
        // Obtener primer día
        Date firsDateOfTheMonth = getFirstDayOfTheMonth();

        // Definir periodo semanal por default
        // Crear instancia de calendario
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(firsDateOfTheMonth);

        // Moverse un mes después
        calendar.add(Calendar.MONTH, 1);
        // Regresar un día
        calendar.add(Calendar.DAY_OF_YEAR, -1);

        // Definir la fecha de inicio
        return calendar.getTime();
    }

    public static Date getFirstTimeOfDay() {
        Date today = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    public static Date getLastTimeOfDay() {
        Date today = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        return calendar.getTime();
    }

    public static String dateObjectToString(Date date) {
        String dateString;

        //Definir dateFormat
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm",
                Locale.getDefault());

        //Convertir Fecha a String
        try {
            dateString = dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        try {
            //Obtener el primer índice del caracter "/"
            int idx1 = dateString.indexOf("/");
            //Obtener el último índice del caracter "/"
            int idx2 = dateString.lastIndexOf("/");

            //Obtener el mes de la fecha
            int month = Integer.parseInt(dateString.substring(idx1 + 1, idx2));

            //Reemplazar su abreviación con un número
            dateString =
                    dateString.substring(0, idx1 + 1) + monthNumberToString(month - 1) + dateString.substring(idx2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return dateString;
    }

    /*
     *   Desc: Reemplaza los meses de las fechas a texto
     *   Formato: dd-MM-yyyy HH:mm
     *
     *   @param String: dateString
     *   @return String: formattedString
     * */
    public static String getFormattedDateString(String dateString) {
        String formattedString;
        try {
            //Obtener el primer índice del caracter "-"
            int idx1 = dateString.indexOf("-");
            //Obtener el último índice del caracter "-"
            int idx2 = dateString.lastIndexOf("-");

            //Obtener el mes de la fecha
            int month = Integer.parseInt(dateString.substring(idx1 + 1, idx2));

            //Reemplazar su abreviación con un número
            formattedString =
                    dateString.substring(0, idx1 + 1) + monthNumberToString(month - 1) + dateString.substring(idx2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return formattedString;
    }

    public static Date dateStringToObject(String dateString) {
        //Esperando una fecha como "30/ENE/2019 10:22"...
        //Convertir a objeto de Fecha

        try {
            //Obtener el primer índice del caracter "/"
            int idx1 = dateString.indexOf("/");
            //Obtener el último índice del caracter "/"
            int idx2 = dateString.lastIndexOf("/");

            //Obtener el mes de la fecha
            String month = dateString.substring(idx1 + 1, idx2);

            //Reemplazar su abreviación con un número
            dateString =
                    dateString.substring(0, idx1 + 1) + monthStringToNumber(month) + dateString.substring(idx2);
        } catch (Exception e) {
            return null;
        }

        //Definir dateFormat
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());

        //Crear objeto de fecha
        Date date;

        //Convertir String a Fecha
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return date;
    }

    public static String monthNumberToString(int monthNumber) {
        switch (monthNumber) {
            case 0:
                return "ENE";

            case 1:
                return "FEB";

            case 2:
                return "MAR";

            case 3:
                return "ABR";

            case 4:
                return "MAY";

            case 5:
                return "JUN";

            case 6:
                return "JUL";

            case 7:
                return "AGO";

            case 8:
                return "SEP";

            case 9:
                return "OCT";

            case 10:
                return "NOV";

            case 11:
                return "DIC";

            default:
                return (monthNumber <= 9) ? ("0" + monthNumber) : String.valueOf(monthNumber);
        }
    }

    private static String monthStringToNumber(String monthString) {
        switch (monthString) {
            case "ENE":
                return "01";

            case "FEB":
                return "02";

            case "MAR":
                return "03";

            case "ABR":
                return "04";

            case "MAY":
                return "05";

            case "JUN":
                return "06";

            case "JUL":
                return "07";

            case "AGO":
                return "08";

            case "SEP":
                return "09";

            case "OCT":
                return "10";

            case "NOV":
                return "11";

            case "DIC":
                return "12";

            default:
                return monthString;
        }
    }

    public static Date serverDateToLocalDate(String serverDate) {
        Date mDate = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm",
                Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            mDate = simpleDateFormat.parse(serverDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mDate;
    }

    public static Date formatoFechaEmax(String dateString) {
        Date mDate = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());

        try {
            mDate = simpleDateFormat.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mDate;
    }

    public static Date formatoFechaMovimientosCuenta(String dateString, boolean fromUTC) {
        Date mDate = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss",
                Locale.getDefault());
        if (fromUTC) simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        else simpleDateFormat.setTimeZone(TimeZone.getDefault());

        try {
            mDate = simpleDateFormat.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mDate;
    }

    public static Date stringToChipRedDate(String date) {
        Date mDate = new Date();

        try {
            mDate = chipRedDateFormat.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mDate;
    }

    public static String dateToChipredDateString(Date date, boolean fromUTC) {
        String formattedDate = "";
        try {
            formattedDate = chipRedDateFormat.format(date);
            if (fromUTC) chipRedDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            else chipRedDateFormat.setTimeZone(TimeZone.getDefault());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return formattedDate;
    }

    public static String getHour(Date mDate) {
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(mDate);
    }

    public static String getHourWithSeconds(Date mDate) {
        return new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(mDate);
    }

    public static String getDayMonthYear(Date mDate) {
        return new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(mDate);
    }

    public static String getDayNameMonthYear(Date mDate) {
        return new SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault())
                .format(mDate)
                .toUpperCase(Locale.US)
                .replace(".", "");
    }
}
