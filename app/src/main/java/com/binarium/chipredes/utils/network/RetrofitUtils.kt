package com.binarium.chipredes.utils.network

import okhttp3.ResponseBody
import org.json.JSONObject
import java.util.*

fun getJsonFromBodyResponse(responseBody: ResponseBody?): JSONObject {
    val bodyString = responseBody?.charStream().toString()
    return JSONObject(bodyString)
}

fun isWolkeResponseSuccessful(response: String): Boolean = response.toLowerCase(Locale.ROOT) == "ok"
