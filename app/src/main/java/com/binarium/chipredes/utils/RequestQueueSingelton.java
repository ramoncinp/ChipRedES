package com.binarium.chipredes.utils;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class RequestQueueSingelton
{
    private static RequestQueueSingelton instance;
    private RequestQueue requestQueue;
    private static Context ctx;

    private RequestQueueSingelton(Context context)
    {
        ctx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized RequestQueueSingelton getInstance(Context context)
    {
        if (instance == null)
        {
            instance = new RequestQueueSingelton(context);
        }

        return instance;
    }

    public RequestQueue getRequestQueue()
    {
        if (requestQueue == null)
        {
            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req)
    {
        getRequestQueue().add(req);
    }
}
