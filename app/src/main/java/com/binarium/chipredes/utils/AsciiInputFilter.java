package com.binarium.chipredes.utils;

import android.text.InputFilter;
import android.text.Spanned;

public class AsciiInputFilter implements InputFilter
{
    private char min = 0x00, max = 0x7F;

    public AsciiInputFilter()
    {

    }

    @Override
    public CharSequence filter(CharSequence charSequence, int start, int end, Spanned spanned,
                               int i2, int i3)
    {
        boolean keepOriginal = true;

        StringBuilder validString = new StringBuilder(end - start);
        for (int inputLength = start; inputLength < end; inputLength++)
        {
            //Obtener char a evaluar
            char mChar = charSequence.charAt(inputLength);
            if (isInRange(min, max, mChar))
            {
                validString.append(mChar);
            }
            else
            {
                keepOriginal = false;
            }
        }

        if (keepOriginal)
        {
            return null;
        }
        else
        {
            return "";
        }
    }

    private boolean isInRange(char a, char b, char c)
    {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}
