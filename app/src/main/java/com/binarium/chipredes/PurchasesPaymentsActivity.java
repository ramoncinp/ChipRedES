package com.binarium.chipredes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.adapters.ThreeRowRecyclerViewAdapter;
import com.binarium.chipredes.adapters.TwoRowRecyclerViewAdapter;
import com.binarium.chipredes.barcode.QRScannerActivity;
import com.binarium.chipredes.printer.PrinterManager;
import com.binarium.chipredes.printer.PrintersDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static android.view.View.GONE;

public class PurchasesPaymentsActivity extends AppCompatActivity
{
    private final static int PURCHASE = 0;
    private final static int PAYMENT = 1;

    // Variables
    private int searchClientFilter = ChipRedManager.BYEMAIL;
    private String searchFilterString;
    private String printerIp;

    // Views
    private Dialog filterSelectorDialog;
    private PrintersDialogFragment printersDialogFragment;
    private SearchView searchView;
    private TextView noElements;
    private TextView thinkText;
    private LinearLayout thinkingLayout;
    private RecyclerView recyclerView;

    private CursorAdapter suggestionsAdapter;
    private ArrayList<Purchase> purchases;
    private ArrayList<Payment> payments;
    private JSONArray jsonElements;

    private JSONObject jsonClient;

    private ChipRedManager chipRedManager;
    private PrinterManager printerManager;

    private boolean byQr = false;
    private int selector;

    //Definición de listener de la impresora
    private PrinterManager.PrinterListener printerListener = new PrinterManager.PrinterListener()
    {
        @Override
        public void onSuccesfulPrint(final String msg)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    hideThinkingLayout();
                    Toast.makeText(PurchasesPaymentsActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onPrinterError(final String msg)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    hideThinkingLayout();
                    Toast.makeText(PurchasesPaymentsActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            });
        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.purchases_payments_layout);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //Saber si se va a crear o modificar vehículo
        selector = getIntent().getIntExtra(ChipREDConstants.PURCHASE_OR_PAYMENT, 0);

        searchView = findViewById(R.id.sv_purchases);
        thinkingLayout = findViewById(R.id.purchase_think_layout);
        noElements = findViewById(R.id.no_elements);
        recyclerView = findViewById(R.id.purchases_list);
        thinkText = findViewById(R.id.purchase_payment_think_text);

        if (selector == PURCHASE)
        {
            setTitle("Consultar / Reimprimir Consumos");
            thinkText.setText("Obteniendo consumos");
        }
        else if (selector == PAYMENT)
        {
            setTitle("Consultar / Reimprimir Abonos");
            thinkText.setText("Obteniendo abonos");
        }

        jsonElements = new JSONArray();

        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                byQr = false;
                if (webServiceN == ChipRedManager.GET_PURCHASES || webServiceN == ChipRedManager
                        .GET_PAYMENTS)
                {
                    GenericDialog genericDialog = new GenericDialog("Aviso", crMessage, new
                            Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    //nada...
                                }
                            }, null, PurchasesPaymentsActivity.this);

                    genericDialog.show();
                    noElements.setVisibility(View.VISIBLE);
                    thinkingLayout.setVisibility(GONE);

                    if (selector == PURCHASE) noElements.setText("No hay consumos");
                    else noElements.setText("No hay abonos");
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                byQr = false;
                if (webServiceN != ChipRedManager.SEARCH_CLIENT_WS)
                {
                    GenericDialog genericDialog = new GenericDialog("Error", errorMessage, new
                            Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    //nada...
                                }
                            }, null, PurchasesPaymentsActivity.this);

                    genericDialog.show();

                    if (webServiceN == ChipRedManager.GET_PAYMENTS || webServiceN ==
                            ChipRedManager.GET_PURCHASES)
                    {
                        noElements.setVisibility(View.VISIBLE);
                        thinkingLayout.setVisibility(GONE);
                    }

                    if (selector == PURCHASE) noElements.setText("No hay consumos");
                    else noElements.setText("No hay abonos");
                }
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                noElements.setVisibility(GONE);

                if (webServiceN == ChipRedManager.SEARCH_CLIENT_WS)
                {
                    Log.d("BuscarCliente", "Coincidencias encontradas");
                    if (byQr)
                    {
                        thinkingLayout.setVisibility(GONE);
                        try
                        {
                            sendClientRequest(response.getJSONArray("data").getJSONObject(0));
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        getSuggestions(response);
                    }
                }
                else if (webServiceN == ChipRedManager.GET_PURCHASES)
                {
                    parsePurchases(response); //11953
                }
                else if (webServiceN == ChipRedManager.GET_PAYMENTS)
                {
                    parsePayments(response);
                }
            }
        }, this);

        setListeners();

        // Mostrar selector de impresoras
        showPrinterSelector();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        else if (item.getItemId() == R.id.read_client_qr)// Leer QR de cliente
        {
            //Ocultar teclado
            hideKeyboard();

            //Iniciar actividad para leer QR
            startActivityForResult(new Intent(PurchasesPaymentsActivity.this,
                    QRScannerActivity.class), ChipREDConstants.READ_QR_REQUEST);
        }
        else if (item.getItemId() == R.id.select_filter_button)
        {
            showFilterSelectorDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.read_qr_and_filter_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ChipREDConstants.READ_QR_REQUEST)
        {
            if (resultCode == RESULT_OK)
            {
                //Usar el código
                if (data != null)
                {
                    String qrCode = data.getData().toString();
                    //Mostrar progressBar
                    showThinkingLayout(null);
                    //Indicar que se leyó el id por QR
                    byQr = true;
                    //Buscar cliente con el código recibido
                    chipRedManager.searchClient(ChipRedManager.BYID, qrCode, true);
                    Log.d("PurchasesOrPayments", "Read QR: " + qrCode);
                }
            }
        }
    }

    private void setListeners()
    {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                if (!newText.isEmpty() && newText.length() >= 3)
                {
                    chipRedManager.searchClient(searchClientFilter, newText, true);
                    Log.d("BuscarCliente",
                            "Buscando cliente por " + searchFilterString + " -> " + newText);
                }

                return false;
            }
        });

        //Adaptador para los suggestions
        final String[] from = new String[]{"nombreCliente"};
        final int[] to = new int[]{android.R.id.text1};

        suggestionsAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1,
                null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        searchView.setSuggestionsAdapter(suggestionsAdapter);

        // Inicializar texto de filtro actual
        searchFilterString = getResources().getString(R.string.fp_email);
    }

    private void parsePurchases(JSONObject response)
    {
        purchases = new ArrayList<>();
        try
        {
            JSONArray jsonPurchases = response.getJSONArray("data");
            this.jsonElements = jsonPurchases;

            for (int i = 0; i < jsonPurchases.length(); i++)
            {
                JSONObject jsonPurchase = jsonPurchases.getJSONObject(i);
                JSONObject stationData = jsonPurchase.getJSONObject("estacion");

                Purchase purchase = new Purchase();
                purchase.setTicketNumber(jsonPurchase.getString("num_ticket"));
                purchase.setVolume(jsonPurchase.getDouble("cantidad"));
                purchase.setDateHour(jsonPurchase.getString("fecha_hora"));
                purchase.setAmount(jsonPurchase.getDouble("costo"));
                purchase.setUnitPrice(jsonPurchase.getString("precio_unitario"));
                purchase.setPumper(stationData.getJSONObject("despachadores").getString("nombre"));
                purchase.setProductDesc(stationData.getJSONObject("productos").getString
                        ("descripcion"));
                purchase.setProductColor(stationData.getJSONObject("productos").getString("color"));

                //Validar si tiene productos extra
                if (jsonPurchase.has("productos_extra"))
                {
                    //Obtener arreglo de productos extra
                    JSONArray extraProducts = jsonPurchase.getJSONArray("productos_extra");
                    //Validar que el arreglo tenga elementos
                    if (extraProducts.length() != 0)
                    {
                        ArrayList<ExtraProduct> extraProductArrayList = new ArrayList<>();
                        for (int x = 0; x < extraProducts.length(); x++)
                        {
                            JSONObject jsonExtraProduct = extraProducts.getJSONObject(x);
                            //Obtener producto extra
                            ExtraProduct extraProduct = new ExtraProduct();
                            extraProduct.setDescripcion(jsonExtraProduct.getString("descripcion"));
                            extraProduct.setPrecio(jsonExtraProduct.getDouble("precio"));
                            extraProduct.setCodigo(jsonExtraProduct.getString("codigo"));
                            extraProduct.setCantidad(jsonExtraProduct.getInt("cantidad"));

                            //Agregar a la lista
                            extraProductArrayList.add(extraProduct);
                        }
                        //Agregar lista de productos extra al objeto "Consumo"
                        purchase.setExtraProducts(extraProductArrayList);
                    }
                }
                purchases.add(purchase);
            }
            Log.d("PurchasePayFrgmnt", "Consumos -> \n\n" + response.toString(1));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        listPurchases();
    }

    private void parsePayments(JSONObject response)
    {
        payments = new ArrayList<>();
        try
        {
            JSONArray jsonPayments = response.getJSONArray("data");
            this.jsonElements = jsonPayments;

            for (int i = 0; i < jsonPayments.length(); i++)
            {
                JSONObject jsonPayment = jsonPayments.getJSONObject(i);

                Payment payment = new Payment();
                payment.setPaymentQuantity(jsonPayment.getDouble("cantidad"));
                payment.setNewBalance(jsonPayment.getDouble("saldo_nuevo"));
                payment.setDateHour(jsonPayment.getString("fecha"));
                payment.setLastBalance(jsonPayment.getDouble("saldo_anterior"));
                payment.setAccount(jsonPayment.getJSONObject("cliente").getJSONObject("cuentas")
                        .getString("descripcion"));
                payment.setStation(jsonPayment.getJSONObject("estacion").getString
                        ("nombre_estacion"));

                payments.add(payment);
            }
            Log.d("PurchasePayFrgmnt", "Abonos -> \n\n" + response.toString(1));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Error al obtener abonos", Toast.LENGTH_SHORT).show();
        }

        listPayments();
    }

    private void listPurchases()
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        PurchaseArrayAdapter arrayAdapter = new PurchaseArrayAdapter(purchases, this);

        arrayAdapter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showPurchaseDetailDialog(recyclerView.getChildAdapterPosition(v));
            }
        });

        thinkingLayout.setVisibility(GONE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(arrayAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
    }

    private void listPayments()
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        PaymentArrayAdapter arrayAdapter = new PaymentArrayAdapter(payments, this);

        arrayAdapter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showPaymentDetailDialog(recyclerView.getChildAdapterPosition(v));
            }
        });

        thinkingLayout.setVisibility(GONE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(arrayAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
    }

    public void showPurchaseDetailDialog(final int position)
    {
        DecimalFormat volumeFormat = new DecimalFormat("0.000");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Detalle de consumo");

        View content = getLayoutInflater().inflate(R.layout.dialog_purchase_detail, null);
        LinearLayout purchaseDetailLayout = content.findViewById(R.id.purchases_detail_layout);
        RecyclerView purchaseConceptsLv = content.findViewById(R.id.purchase_detail_list);
        RecyclerView extraProductsLv = content.findViewById(R.id.purchase_detail_extra_products);
        LinearLayout extraProductLinearLayout = content.findViewById(R.id
                .purchase_detail_extra_product_layout);
        TextView grandTotal = content.findViewById(R.id.purchase_detail_total_tv);

        purchaseDetailLayout.setVisibility(View.VISIBLE);

        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();

        try
        {
            JSONObject purchase = jsonElements.getJSONObject(position);

            keys.add("Número de ticket: ");
            values.add(purchase.getString("num_ticket"));

            keys.add("Volumen: ");
            values.add(volumeFormat.format(purchase.getDouble("cantidad")) + " L");

            keys.add("Importe: ");
            keys.add("Precio unitario: ");

            if (SharedPreferencesManager.getString(this, ChipREDConstants.COUNTRY).contains
                    ("costa rica"))
            {
                values.add(ChipREDConstants.CR_AMOUNT_FORMAT.format(purchase.getDouble("costo")) + " CRC");
                values.add(ChipREDConstants.CR_AMOUNT_FORMAT.format(purchase.getDouble(
                        "precio_unitario")) + " " +
                        "" + "" + "" + "" + "CRC");
            }
            else
            {
                values.add(ChipREDConstants.MX_AMOUNT_FORMAT.format(purchase.getDouble("costo")) + " MXN");
                values.add(ChipREDConstants.MX_AMOUNT_FORMAT.format(purchase.getDouble(
                        "precio_unitario")) + " " +
                        "" + "" + "" + "" + "MXN");
            }

            keys.add("Producto: ");
            values.add(purchase.getJSONObject("estacion").getJSONObject("productos").getString
                    ("descripcion"));

            String fechaHora = purchase.getString("fecha_hora");
            keys.add("Fecha: ");
            values.add(fechaHora.substring(0, fechaHora.indexOf(" ")));

            keys.add("Hora: ");
            values.add(fechaHora.substring(fechaHora.indexOf(" ") + 1));

            keys.add("Estación: ");
            values.add(purchase.getJSONObject("estacion").getString("nombre_estacion"));

            keys.add("Posición de carga: ");
            values.add(purchase.getString("posicion_carga"));

            keys.add("Despachador: ");
            values.add(purchase.getJSONObject("estacion").getJSONObject("despachadores").
                    getString("nombre"));

            purchaseConceptsLv.setAdapter(new TwoRowRecyclerViewAdapter(this, keys, values));
            purchaseConceptsLv.setLayoutManager(new LinearLayoutManager(this));
            purchaseConceptsLv.setHasFixedSize(true);

            //Mostrar productos extra si existen
            Purchase purchaseObject = purchases.get(position);
            if (purchaseObject.getExtraProducts() != null)
            {
                //Obtener lista y almacenarla
                ArrayList<ExtraProduct> extraProductArrayList = purchaseObject.getExtraProducts();
                //Crear listas para cada columna
                ArrayList<String> extraProductDesc = new ArrayList<>();
                ArrayList<String> extraProductQuantity = new ArrayList<>();
                ArrayList<String> extraProductSubTotal = new ArrayList<>();

                //Calcular total
                double total = purchase.getDouble("costo");

                //Obtener informacion de cada producto extra
                for (ExtraProduct extraProduct : extraProductArrayList)
                {
                    String desc = extraProduct.getDescripcion();
                    int quantity = extraProduct.getCantidad();
                    double subTotal = quantity * extraProduct.getPrecio();

                    extraProductDesc.add(desc);
                    extraProductQuantity.add(String.valueOf(quantity));

                    if (SharedPreferencesManager.getString(this, ChipREDConstants.COUNTRY).equals("costa rica"))
                    {
                        extraProductSubTotal.add(ChipREDConstants.CR_AMOUNT_FORMAT.format(subTotal) + " CRC");
                    }
                    else if (SharedPreferencesManager.getString(this, ChipREDConstants.COUNTRY).equals("mexico"))
                    {
                        extraProductSubTotal.add(ChipREDConstants.MX_AMOUNT_FORMAT.format(subTotal) + " MXN");
                    }

                    //Acumular
                    total += subTotal;
                }

                ThreeRowRecyclerViewAdapter threeRowListViewAdapter =
                        new ThreeRowRecyclerViewAdapter
                                (this, extraProductDesc, extraProductQuantity,
                                        extraProductSubTotal);
                extraProductsLv.setAdapter(threeRowListViewAdapter);
                extraProductsLv.setLayoutManager(new LinearLayoutManager(this));
                extraProductsLv.setHasFixedSize(true);

                if (SharedPreferencesManager.getString(this, ChipREDConstants.COUNTRY)
                        .contains("costa rica"))
                {
                    //Escribir total
                    grandTotal.setText(ChipREDConstants.CR_AMOUNT_FORMAT.format(total) + " CRC");
                }
                else
                {
                    //Escribir total
                    grandTotal.setText(ChipREDConstants.MX_AMOUNT_FORMAT.format(total) + " MXN");
                }

                extraProductLinearLayout.setVisibility(View.VISIBLE);
            }

            builder.setView(content);

            final Dialog dialog = builder.create();
            dialog.setCancelable(true);
            dialog.show();

            Button printButton = content.findViewById(R.id.purchase_print_button);
            printButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    //Nueva instancia de impresora
                    printerManager = new PrinterManager(PurchasesPaymentsActivity.this,
                            printerListener);

                    printerManager.setIpAndInit(printerIp);

                    showThinkingLayout("Impriminedo consumo");
                    new Thread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            /*
                            try
                            {
                                printerManager.printPurchase(jsonElements.getJSONObject(position)
                                , jsonClient);
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                                hideThinkingLayout();
                            }*/
                        }
                    }).start();
                    dialog.dismiss();
                }
            });

            Button dismissButton = content.findViewById(R.id.purchase_close_button);
            dismissButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    dialog.dismiss();
                }
            });

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialog)
                {
                    searchView.clearFocus();
                }
            });
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Error al obtener detalle de consumo", Toast.LENGTH_SHORT).show();
        }
    }

    public void showPaymentDetailDialog(final int position)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Detalle de abono");

        //Preparar Views
        View content = getLayoutInflater().inflate(R.layout.dialog_purchase_detail, null);
        RecyclerView paymentConceptsLv = content.findViewById(R.id.payment_detail_list);
        paymentConceptsLv.setVisibility(View.VISIBLE);

        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();

        Payment payment = payments.get(position);

        keys.add("Cantidad: ");
        keys.add("Salddo Nuevo: ");
        keys.add("Salddo Anterior: ");

        if (SharedPreferencesManager.getString(this, ChipREDConstants.COUNTRY).contains
                ("costa rica"))
        {
            values.add(ChipREDConstants.CR_AMOUNT_FORMAT.format(payment.getPaymentQuantity()) +
                    " CRC");
            values.add(ChipREDConstants.CR_AMOUNT_FORMAT.format(payment.getNewBalance()) + " CRC");
            values.add(ChipREDConstants.CR_AMOUNT_FORMAT.format(payment.getLastBalance()) + " CRC");
        }
        else
        {
            values.add(ChipREDConstants.MX_AMOUNT_FORMAT.format(payment.getPaymentQuantity()) +
                    " MXN");
            values.add(ChipREDConstants.MX_AMOUNT_FORMAT.format(payment.getNewBalance()) + " MXN");
            values.add(ChipREDConstants.MX_AMOUNT_FORMAT.format(payment.getLastBalance()) + " MXN");
        }

        keys.add("Cuenta: ");
        values.add(payment.getAccount());

        String datHour = payment.getDateHour();
        keys.add("Hora: ");
        values.add(datHour.substring(datHour.indexOf(' ') + 1));

        keys.add("Fecha: ");
        values.add(datHour.substring(0, datHour.indexOf(' ')));

        keys.add("Estación: ");
        values.add(payment.getStation());

        paymentConceptsLv.setAdapter(new TwoRowRecyclerViewAdapter(this, keys, values));
        paymentConceptsLv.setLayoutManager(new LinearLayoutManager(this));
        paymentConceptsLv.setHasFixedSize(true);

        builder.setView(content);
        final Dialog dialog = builder.create();
        dialog.setCancelable(true);
        dialog.show();

        Button printButton = content.findViewById(R.id.purchase_print_button);
        printButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Nueva instancia de impresora
                printerManager = new PrinterManager(PurchasesPaymentsActivity.this,
                        printerListener);
                printerManager.setIpAndInit(printerIp);

                showThinkingLayout("Impriminedo abono");
                new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            printerManager.printPayment(jsonElements.getJSONObject(position),
                                    jsonClient, true);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            hideThinkingLayout();
                        }
                    }
                }).start();
                dialog.dismiss();
            }
        });

        Button dismissButton = content.findViewById(R.id.purchase_close_button);
        dismissButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                searchView.clearFocus();
            }
        });
    }

    private void getSuggestions(JSONObject response)
    {
        //Crear suggestions de los clientes encontrados
        final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "nombreCliente"});

        final ArrayList<JSONObject> suggestionsValues = new ArrayList<>();

        try
        {
            JSONArray arrClientes = response.getJSONArray("data");
            for (int i = 0; i < arrClientes.length(); i++)
            {
                suggestionsValues.add(arrClientes.getJSONObject(i));
                c.addRow(new Object[]{i, arrClientes.getJSONObject(i).get("nombre").toString() +
                        " - " + arrClientes.getJSONObject(i).get("email").toString()});
            }
            suggestionsAdapter.changeCursor(c);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener()
        {
            @Override
            public boolean onSuggestionSelect(int position)
            {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position)
            {
                sendClientRequest(suggestionsValues.get(position));
                return false;
            }
        });
    }

    private void sendClientRequest(JSONObject jsonClient)
    {
        showThinkingLayout(null);

        //Definir cliente seleccionado de la clase
        this.jsonClient = jsonClient;
        try
        {
            if (searchClientFilter == ChipRedManager.BYNAME)
            {
                searchView.setQuery(jsonClient.getString("nombre").toUpperCase(), false);
            }
            else if (searchClientFilter == ChipRedManager.BYEMAIL)
            {
                searchView.setQuery(jsonClient.getString("email"), false);
            }
            else if (searchClientFilter == ChipRedManager.BYTAXCODE)
            {
                searchView.setQuery(jsonClient.getString("RFC/Cédula").toUpperCase(), false);
            }

            if (selector == PURCHASE)
            {
                chipRedManager.getPurchases(jsonClient.getString("id"));
            }
            else
            {
                chipRedManager.getPayments(jsonClient.getString("id"));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void showThinkingLayout(String text)
    {
        if (text != null)
        {
            thinkText.setText(text);
        }

        hideKeyboard();
        thinkingLayout.setVisibility(View.VISIBLE);
        noElements.setVisibility(GONE);
        recyclerView.setVisibility(View.INVISIBLE);
    }

    private void hideThinkingLayout()
    {
        hideKeyboard();
        recyclerView.setVisibility(View.VISIBLE);
        thinkingLayout.setVisibility(GONE);
        noElements.setVisibility(GONE);
    }

    public void hideKeyboard()
    {
        try
        {
            View kb = getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) (getSystemService(Context
                    .INPUT_METHOD_SERVICE));
            if (imm != null && kb != null)
            {
                imm.hideSoftInputFromWindow(kb.getWindowToken(), 0);
            }
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
    }

    private void showFilterSelectorDialog()
    {
        if (filterSelectorDialog == null)
        {
            // Crear builder
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            // Crear layout
            View content =
                    getLayoutInflater().inflate(R.layout.dialog_search_client_filter_selector,
                            null);

            // Definir layout
            builder.setView(content);

            // Definir boton de regresar
            builder.setNegativeButton("Regresar", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                }
            });

            // Crear diálogo
            filterSelectorDialog = builder.create();

            // Checkear filtro actual
            RadioGroup list = content.findViewById(R.id.filters_radio_group);
            for (int i = 0; i < list.getChildCount(); i++)
            {
                View view = list.getChildAt(i);
                if (view instanceof RadioButton)
                {
                    // Hacer cast a RadioButton del view actual
                    RadioButton mRadioButton = (RadioButton) view;

                    // Checkear el filtro correspondiente
                    if (mRadioButton.getText().toString().equals(searchFilterString))
                    {
                        mRadioButton.setChecked(true);
                    }
                }
            }

            // Crear listener para los elementos
            View.OnClickListener filterSelected = new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // Obtener texto de radio button
                    RadioButton selectedButton = (RadioButton) v;

                    // Definir filtro actual
                    searchFilterString = selectedButton.getText().toString();
                    searchView.setQueryHint("Buscar cliente (" + searchFilterString + ")");

                    // Definir nuevo filtro
                    if (searchFilterString.equals(getResources().getString(R.string.fp_email)))
                    {
                        searchClientFilter = ChipRedManager.BYEMAIL;
                    }
                    else if (searchFilterString.equals(getResources().getString(R.string.fp_name)))
                    {
                        searchClientFilter = ChipRedManager.BYNAME;
                    }
                    else if (searchFilterString.equals(getResources().getString(R.string.fp_tax_code)))
                    {
                        searchClientFilter = ChipRedManager.BYTAXCODE;
                    }

                    // Cerar diálogo
                    filterSelectorDialog.dismiss();
                }
            };

            // Obtener layout y seleccionar el filtro actual
            for (int i = 0; i < list.getChildCount(); i++)
            {
                View view = list.getChildAt(i);
                if (view instanceof RadioButton)
                {
                    // Hacer cast a RadioButton del view actual
                    RadioButton mRadioButton = (RadioButton) view;

                    // Definir clickListener
                    mRadioButton.setOnClickListener(filterSelected);
                }
            }
        }

        // Mostrar diálogo
        filterSelectorDialog.show();
    }

    private void showPrinterSelector()
    {
        printersDialogFragment = new PrintersDialogFragment();
        printersDialogFragment.setPrinterDialogInterface(new PrintersDialogFragment.PrinterDialogInterface()
        {
            @Override
            public void onIpSelected(String ip)
            {
                printerIp = ip;
                printersDialogFragment.dismiss();
            }
        });

        printersDialogFragment.show(getSupportFragmentManager(), "printer_dialog");
    }
}
