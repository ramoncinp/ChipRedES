package com.binarium.chipredes.dispenser;

public class Pump
{
    private int number;
    private String state;

    public Pump(int number)
    {
        this.number = number;
    }

    public int getNumber()
    {
        return number;
    }

    public void setNumber(int number)
    {
        this.number = number;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }
}
