package com.binarium.chipredes.dispenser;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipredes.R;

import java.util.ArrayList;

public class PumpsArrayAdapter extends RecyclerView.Adapter<PumpsArrayAdapter.PumpsViewHolder>
        implements View.OnLongClickListener
{
    private ArrayList<Pump> pumps;
    private View.OnLongClickListener onLongClickListener;
    private Context context;

    public PumpsArrayAdapter(ArrayList<Pump> pumps, Context context)
    {
        this.pumps = pumps;
        this.context = context;
    }

    @Override
    public PumpsViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.assigned_pump_item, parent, false);

        PumpsViewHolder pumpsViewHolder = new PumpsViewHolder(view);
        view.setOnLongClickListener(this);

        return pumpsViewHolder;
    }

    @Override
    public void onBindViewHolder(PumpsViewHolder holder, int position)
    {
        final Pump pump = pumps.get(position);
        holder.number.setText(String.valueOf(pump.getNumber()));

        final CardView enableDisableButton = holder.enableDisable;
        enableDisableButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (pump.getState().equals("AC"))
                {
                    pump.setState("BA");
                    enableDisableButton.setCardBackgroundColor(context.getResources().getColor(R.color.red));
                }
                else
                {
                    pump.setState("AC");
                    enableDisableButton.setCardBackgroundColor(context.getResources().getColor(R.color.green_money));
                }
            }
        });

        if (pump.getState().equals("AC"))
        {
            enableDisableButton.setCardBackgroundColor(context.getResources().getColor(R.color.green_money));
        }
        else
        {
            enableDisableButton.setCardBackgroundColor(context.getResources().getColor(R.color.red));
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public boolean onLongClick(View v)
    {
        if (onLongClickListener != null)
        {
            onLongClickListener.onLongClick(v);
        }
        return true;
    }

    public ArrayList<Pump> getPumps()
    {
        return pumps;
    }

    public void setPumps(ArrayList<Pump> pumps)
    {
        this.pumps = pumps;
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener)
    {
        this.onLongClickListener = onLongClickListener;
    }

    @Override
    public int getItemCount()
    {
        return pumps.size();
    }

    static class PumpsViewHolder extends RecyclerView.ViewHolder
    {
        private TextView number;
        private CardView enableDisable;

        PumpsViewHolder(View itemView)
        {
            super(itemView);

            number = itemView.findViewById(R.id.text_view);
            enableDisable = itemView.findViewById(R.id.enable_disable);
        }
    }
}
