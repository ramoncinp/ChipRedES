package com.binarium.chipredes.dispenser;

import com.binarium.chipredes.LoadingPosition;

public class Dispenser
{
    private int number;
    private String serialNumber;
    private LoadingPosition side1;
    private LoadingPosition side2;

    public Dispenser()
    {
    }

    public int getNumber()
    {
        return number;
    }

    public void setNumber(int number)
    {
        this.number = number;
    }

    public LoadingPosition getSide1()
    {
        return side1;
    }

    public void setSide1(LoadingPosition side1)
    {
        this.side1 = side1;
    }

    public LoadingPosition getSide2()
    {
        return side2;
    }

    public void setSide2(LoadingPosition side2)
    {
        this.side2 = side2;
    }

    public String getSerialNumber()
    {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber)
    {
        this.serialNumber = serialNumber;
    }
}
