package com.binarium.chipredes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import com.google.android.material.snackbar.Snackbar;

import androidx.fragment.app.Fragment;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

public class ChipREDConstants {
    /**
     * Direcciones IP de servidor
     **/
    public final static String CR_LOCAL_IP = "local";
    public final static String CR_WOLKE_IP = "wolke";
    public final static String CR_WEB_SOCKET_IP = "socket";
    public final static String PRINTER_IP_ADDR = "printer_address";
    public final static String TOKENCASH_API_URL = "https://tokencash.appspot.com/api/";
    public final static String EMAX_URL = "emaxUrl";
    public final static String EMAX_CLIENT_URL = "emaxClientUrl";
    public final static String WOLKE2_BASE_URL = "https://wolke-v2.uc.r.appspot.com/";
    public final static String EMAX_CLIENT_ENABLED = "emaxClientEnabled";

    public final static String APP_PIN = "app_pin";
    public final static String PAYMENT_LIMIT = "payment_limit_value";

    public final static String STATION_ID = "id_estacion";
    public final static String STATION_MONGO_ID = "mongo_id_estacion";
    public final static String STATION_TAX_CODE = "cedula_estacion";
    public final static String COUNTRY = "pais";
    public final static String DEVICE_IP_ADDRESS = "device_ip_address";

    public final static String LOADING_POSITIONS_FROM_LOCAL = "dispensarios";
    public final static String LOADING_POSITIONS_FROM_WEB_SOCKET = "data";

    /**
     * Parámetros para fragments y actividades
     **/
    public final static String PURCHASE_OR_PAYMENT = "purchaseOrPayment";
    public final static String ADD_OR_MODIFY_VEHICLE = "addOrModifyVehicle";
    public final static String STATION_DATA = "datos_estacion";
    public final static String SELECTED_LOADING_POSITION = "selected_loading_position";
    public final static String NO_LAST_SALE = "SinUltimaVenta";
    public final static String PURCHASE_ID_FOR_TOKENCASH = "purchaseIdForTokencash";
    public final static String PURCHASE_AMOUNT_FOR_TOKENCASH = "purchaseAmountForTokencash";
    public final static String PUMP_NUMBER_FOR_TOKEN_CASH = "pumpNumberForTokencash";
    public final static String CONFIG_NEW_STATION = "configNewStation";
    public final static String NULL_ACTIVITIES_TAG = "nullActivities";
    public final static String SELECTED_SALE = "selectedSale";
    public final static String TOKENCASH_SCENARIO = "escenario_tokencash";
    public final static String ASSIGNED_PCS_TO_DEVICE = "pcs_asignadas";

    /**
     * Integraciones
     **/
    /*
        El arreglo consta de una tabla que relaciona nombre, key y resource id de cada integracion
        * Descripcion es el nombre de la integración
        * Key es para almacenar en las sharedPreferences
        * Resource Id es el id de la imagen que se muestra al elegir el método de pago
       -------------------------------------------------------------------------
      | Descripcion        |Key                            |ResourceID          |
       -------------------------------------------------------------------------
      | BilleteElectrónico  integrationElectronicBillet     pokebola800         |
      | EnPista             integrationCash                 billete800          |
      | tokencash           integrationTokenCash            boton_token         |
      | Enviar preset       sendPresetIntegration           ic_send_preset      |
       -------------------------------------------------------------------------
     */
    public final static String[][] INTEGRATIONS = {
            {"Billete Electrónico", "integrationElectronicBillet", "pokebola800"},
            {"En Pista", "integrationCash", "billete800"},
            {"tokencash", "integrationTokenCash", "boton_token"},
            {"Enviar preset", "sendPresetIntegration", "ic_send_preset"}
    };

    /**
     * Elementos del menu
     **/
    /*
        Los elementos del menu se almacenan de una manera similar
        El arreglo consta de una tabla que relaciona nombre, key y resource id de cada integracion
        * Descripcion es el nombre de la integración
        * Key es para almacenar en las sharedPreferences e identificar en el xml del menu
       -----------------------------------------------------
      | Descripcion                |Key                     |
       -----------------------------------------------------
      | Agregar cliente             add_client              |
      | Modificar cliente           modify_client           |
      | Agregar vehículo            add_vehicle             |
      | Modificar vehículo          modify_vehicle          |
      | Consultar saldo             get_balance             |
      | Abonar saldo                add_balance             |
      | Consultar consumos          get_purchases           |
      | Consultar abonos            get_payments            |
      | Resumen tokencash           print_tokens            |
      | Consumos tokencash          tokencash_purchases     |
       -----------------------------------------------------
     */
    public final static String[][] MENU_ITEMS = {
            {"Agregar cliente", "add_client"},
            {"Modificar cliente", "modify_client"},
            {"Agregar cuenta estación", "add_account"},
            //{"Agregar vehículo", "add_vehicle"},
            //{"Modificar vehículo", "modify_vehicle"},
            //{"Consultar " + "saldo", "get_balance"},
            {"Abonar saldo", "add_balance"},
            {"Consultar" + " consumos", "get_purchases"},
            {"Consultar abonos", "get_payments"},
            {"Resumen " + "tokencash", "print_tokens"},
            {"Consumos tokencash", "tokencash_purchases"}};

    /**
     * Elementos de configuración para una estación
     **/
    /*
        El arreglo consta de una tabla que relaciona nombre, key y resource id de cada elemento
        * Descripcion es el nombre del elemento
        * Key es para almacenar en las sharedPreferences
        * Resource Id es el id de la imagen que se muestra al elegir el método de pago
       ----------------------------------
      | Descripcion        |ResourceID   |
       ----------------------------------
      | DatosGenerales      ic_station   |
      | Despachadores       ic_account   |
      | Productos           ic_product   |
      | Dispensarios        ic_dispenser |
       ----------------------------------
     */
    public final static String[][] STATION_ITEMS = {
            {"Datos generales", "ic_data_list"},
            {"Despachadores", "ic_pumper"},
            {"Productos", "ic_product"},
            {"Dispensarios", "ic_pump"}
    };

    /**
     * PAISES
     **/
    public final static String[] COUNTRIES = {"mexico", "costa rica"};

    public final static int READ_QR_REQUEST = 1;
    public final static int COMPLETE_CLIENT_SIGNUP = 2;

    //Constante para enviar un id de cliente al inicio de la actividad de "Modificar Cliente"
    public final static String CLIENT_TO_MODIFY = "clientToModify";

    //Constante para activar la reimpresion en consumos tokencash
    public final static String RE_PRINT_TOKEN_CASH_PURCHASE = "rePrintTokenCash";

    //Constante para modificar la generacion de tokens de cobro
    public final static String GENERATE_TOKEN = "generateToken";

    /**
     * Formatos de moneda
     **/
    public final static DecimalFormat NO_DECIMAL_FORMAT = new DecimalFormat("###,###,###,###,##0");
    public final static DecimalFormat CR_AMOUNT_FORMAT = new DecimalFormat("###,###,###,###,##0");
    public final static DecimalFormat MX_AMOUNT_FORMAT = new DecimalFormat("###,###,###,###," +
            "##0.00");
    public final static DecimalFormat TWO_DECIMALS_FORMAT = new DecimalFormat("0.00");

    /**
     * Formatos de volumen
     **/
    public final static DecimalFormat VOLUME_FORMAT = new DecimalFormat("###,##0.000");

    /**
     * Formatos de fecha
     **/
    public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"
            , Locale.US);

    /**
     * Medios de pago
     **/
    public static ArrayList<PaymentMethodObject> PAYMENT_METHODS() {
        ArrayList<PaymentMethodObject> paymentMethodObjects = new ArrayList<>();
        paymentMethodObjects.add(new PaymentMethodObject("Contado", "01"));
        paymentMethodObjects.add(new PaymentMethodObject("Tarjeta", "02"));
        paymentMethodObjects.add(new PaymentMethodObject("Cheque", "03"));
        paymentMethodObjects.add(new PaymentMethodObject("Transferencia", "04"));
        paymentMethodObjects.add(new PaymentMethodObject("Recaudo", "05"));
        paymentMethodObjects.add(new PaymentMethodObject("Otros", "99"));

        return paymentMethodObjects;
    }

    public static int getPaymentMethodIdx(String code) {
        switch (code) {
            case "01":
                return 0;
            case "02":
                return 1;
            case "03":
                return 2;
            case "04":
                return 3;
            case "05":
                return 4;
            case "99":
                return 5;
            default:
                return 6;
        }
    }

    public static String getPaymentMethodName(String code) {
        switch (code) {
            case "01":
                return "Contado";
            case "02":
                return "Tarjeta";
            case "03":
                return "Cheque";
            case "04":
                return "Transferencia";
            case "05":
                return "Recaudo";
            case "99":
                return "Otros";
            default:
                return "Contado";
        }
    }

    public static final String GENERAL_NOTIFICATION_CHANNEL_ID = "00";

    public static final String NEW_VERSION_AVAILABLE = "newVersionAvailable";

    public static final String CAJA_MATRIX_TEXT = "cajaMatrixText";

    /* Métodos estáticos */
    public static boolean isAlphaNumeric(String str) {
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            //Dejar pasar el caso en el que es un espacio
            if (c != ' ' && c != '.' && c != '(' && c != ')' && c != ':') {
                if (c < 0x30 || (c >= 0x3a && c <= 0x40) || (c > 0x5a && c <= 0x60) || c > 0x7a)
                    return false;
            }
        }
        return true;
    }

    public static void showSnackBarMessage(String message, Activity activity) {
        Snackbar.make(activity.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG)
                .show();
    }

    public static void showMessageInDialog(String titulo, String message, final Activity activity) {
        GenericDialog genericDialog = new GenericDialog(titulo, message, activity::finish, null, activity);
        genericDialog.setCancelable(false);
        genericDialog.show();
    }

    //
    public static void hideKeyboardFromActivity(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity
                    .INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            //Si no hay ningún view, crear uno
            if (view == null) {
                view = new View(activity);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void showKeyboard(Context context) {
        try {
            ((InputMethodManager) (context).getSystemService(Context.INPUT_METHOD_SERVICE))
                    .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager
                            .HIDE_IMPLICIT_ONLY);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void hideKeyboard(Activity activity) {
        try {
            View kb = activity.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) (activity.getSystemService(Context
                    .INPUT_METHOD_SERVICE));
            if (imm != null && kb != null) {
                imm.hideSoftInputFromWindow(kb.getWindowToken(), 0);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject GET_DUMMY_JSON_TC_CONFIG() {
        JSONObject request = new JSONObject();
        JSONObject tokenCash = new JSONObject();
        try {
            request.put("emitir", false);
            request.put("ticketless", false);
            request.put("monto_maximo", 12);
            request.put("porcentaje", 2);
            request.put("tipo_emision", 0);
            request.put("modo_trabajo", "e1");
            request.put("maximo_porcentaje_cobro", "100");
            request.put("impresion_token_cobro", true);

            JSONArray days = new JSONArray();
            JSONArray clients = new JSONArray();
            JSONArray products = new JSONArray();

            // Agregar días por default
            for (int day = 0; day < 7; day++) {
                days.put(day);
            }

            // Agregar cliente 0 por default
            clients.put(0);

            // Agregar arreglos y objetos
            request.put("dias", days);
            request.put("tipo_cliente", clients);
            request.put("productos", products);

            tokenCash.put("tokencash", request);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return tokenCash;
    }

    /**
     * Método: checkNullParentActivity
     * <p>
     * Checa el estado de la actividad y retorna true si es nula, false si no lo es
     * Utilizada para callBacks que se quedan activos cuando la actividad se destruye
     *
     * @return boolean:
     */

    public static boolean checkNullParentActivity(Activity activity, Fragment fragment) {
        //Asegurarse que este viva la actividad padre
        if (activity == null || !fragment.isAdded()) {
            Timber.e("Error de actividad nula");
            return true;
        }

        return false;
    }

    public static Date getDateFromJSON(JSONObject data) {
        String dateHour;
        try {
            //Obtener fecha en String
            dateHour = data.getString("fecha_hora");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        Date date;
        DateFormat dateFormatEntrada = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        try {
            date = dateFormatEntrada.parse(dateHour);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Dialog showProgressDialog(Activity activity) {
        return showProgressDialog(activity, "");
    }

    public static Dialog showProgressDialog(Activity activity, String text) {
        // Crear builder
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        // Obtener layout
        View v = activity.getLayoutInflater().inflate(R.layout.progress_bar_layout, null);

        // Definir layout
        builder.setView(v);

        // Validar texto
        if (!text.isEmpty()) {
            // Obtener textView
            TextView textView = v.findViewById(R.id.progress_bar_text);
            textView.setText(text);
            textView.setVisibility(View.VISIBLE);
        }

        // Regresar diálogo
        return builder.create();
    }

    public static void setCurrentScenario(Context context, String tokencashScenario) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(TOKENCASH_SCENARIO, tokencashScenario)
                .apply();
    }

    public static String getTokencashScenario(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(TOKENCASH_SCENARIO, "e1");
    }

    /**
     * Métodos
     **/
    public static String twoDigits(int n) {
        return (n <= 9) ? ("0" + n) : String.valueOf(n);
    }

    public static Bitmap convertToBitmap(Drawable drawable) {
        Bitmap mutableBitmap = Bitmap.createBitmap(48, 48, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, 48, 48);
        drawable.draw(canvas);

        return mutableBitmap;
    }
}
