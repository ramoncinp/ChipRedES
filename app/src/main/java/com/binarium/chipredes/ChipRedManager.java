package com.binarium.chipredes;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.binarium.chipredes.utils.RequestQueueSingelton;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

public class ChipRedManager
{
    public final String TAG = this.getClass().getSimpleName();

    //WebServices
    public static final int GET_STATION_DATA_WS = 3;
    public static final int SEARCH_CLIENT_WS = 4;
    public static final int ADD_OR_MODIFY_CLIENT_WS = 5;
    public static final int GET_PUMPERS_WS = 6;
    public static final int ADD_BALANCE_WS = 7;
    public static final int GET_BALANCE_WS = 8;
    public static final int PRINT_BALANCE_WS = 9;
    public static final int GET_BILLET_DATA_WS = 10;
    public static final int CANCEL_BILLET_TRANSACTION_WS = 11;
    public static final int START_SALE_WS = 12;
    public static final int LOADING_POS_STATE_CHANGE = 13;
    public static final int GET_PURCHASES = 14;
    public static final int GET_PAYMENTS = 15;
    public static final int REPRINT_SALE = 16;
    public static final int GET_LAST_SALE = 17;
    public static final int GET_EXTRA_PRODUCTS = 18;
    public static final int SET_CASH_SALE = 19;
    public static final int GET_STATION_PRODUCTS = 20;
    public static final int ADD_OR_MODIFY_VEHICLE = 21;
    public static final int SEARCH_VEHICLE = 22;
    public static final int GENERATE_TOKEN = 23;
    public static final int GET_TOKEN = 24;
    public static final int CLOSE_TOKEN = 25;
    public static final int GET_CLOSED_TOKENS = 26;
    public static final int PRINT_TOKENS_SUMMARY = 27;
    public static final int RESEND_CONFIRMATION_EMAIL = 28;
    public static final int COMPLETE_CLIENT_SIGNUP = 29;
    public static final int SET_CM_TEXT = 35;
    public static final int GET_PCS = 36;
    public static final int GET_PRINTERS = 37;
    public static final int SET_PRINTERS = 38;
    public static final int REMOVE_PRINTER = 39;
    public static final int CONFIG_TOKEN_CASH = 40;
    public static final int GET_TOKEN_CASH_CONFIG = 41;
    public static final int GET_PEDNING_SALE = 42;
    public static final int ADD_TAX_CODE_TO_SALE = 43;
    public static final int GET_PENDING_LOCAL_SALES = 44;
    public static final int CHECK_VEHICLE_QR = 45;
    public static final int SET_VEHICLE_QR = 46;
    public static final int SEARCH_PLATES = 47;
    public static final int UN_SET_VEHICLE_QR = 48;
    public static final int GET_LAST_SALES = 49;
    public static final int GET_GENERATED_COUPON = 50;
    public static final int GET_TOKENCASH_PURCHASES = 51;
    public static final int REPRINT_INVOICE = 52;
    public static final int GET_NETWORK_STATE = 53;
    public static final int SET_ESTADO_CONTINGENCIA = 54;
    public static final int NO_URL_REQUEST = 55;
    public static final int LIBERAR_POSICION = 56;

    //Direcciones a buscar de un cliente de CR
    public static final int GET_PROVINCIAS = 30;
    public static final int GET_CANTONES = 31;
    public static final int GET_DISTRITOS = 32;
    public static final int GET_BARRIOS = 33;

    //Valores a buscar de un cliente
    public final static int BYNAME = 1;
    public final static int BYEMAIL = 2;
    public final static int BYID = 3;
    public final static int BYTAXCODE = 4;
    public final static int BYPHONE = 5;
    public final static int BYNAME_EMAIL = 6;
    public final static int BYPLATES = 7;

    //Valores del estado de la red
    public final static int NETWORK_OK = 0;
    public final static int WOLKE_NO_OK = 1;
    public final static int CONTINGENCIA_ACTIVADA = 2;

    public final static int TEST_LOCAL = 0;
    public final static int TEST_WOLKE = 1;
    public final static int TEST_WEB_SOCKET = 2;

    private OnMessageReceived messageReceived;
    private Context context;

    private WebSocketClient webSocketClient;

    private String url;

    public ChipRedManager(OnMessageReceived messageReceived, Context activityContext)
    {
        this.context = activityContext;
        this.messageReceived = messageReceived;
    }

    public void setURL(String url)
    {
        if (url.contains("0.0.0.0:0"))
        {
            Log.v(TAG, "Direccion desactivada");
            return;
        }
        this.url = url;
    }

    /**
     * @params serverType:
     * 0 para local
     * 1 para wolke
     * 2 para webSocket
     **/
    public void testConnection(final int serverType)
    {
        if (serverType != 2)
        {
            JSONObject request = new JSONObject();

            try
            {
                request.put("key", "prueba_conexion");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response)
                {
                    messageReceived.chipRedMessage("Conexión exitosa", serverType);
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    messageReceived.chipRedError("Error en respuesta de petición", serverType);
                }
            };

            sendRequest(request, responseListener, errorListener);
        }
        else
        {
            URI uri;
            try
            {
                uri = new URI(url);
            }
            catch (URISyntaxException e)
            {
                e.printStackTrace();
                return;
            }

            WebSocketClient mWebSocketClient = new WebSocketClient(uri)
            {
                @Override
                public void onOpen(ServerHandshake serverHandshake)
                {
                    if (serverHandshake.getHttpStatus() == 101)
                    {
                        Log.v("WebSocket", "onOpen\n" + String.valueOf(serverHandshake
                                .getHttpStatus()));
                        messageReceived.chipRedMessage("Conexión exitosa", serverType);
                    }
                }

                @Override
                public void onMessage(String s)
                {
                    Log.v("WebSocket", "onMessage\n" + s);
                }

                @Override
                public void onClose(int i, String s, boolean b)
                {
                    Log.v("WebSocket", "onClose\n" + s);
                }

                @Override
                public void onError(Exception e)
                {
                    Log.v("WebSocket", "onError\n" + e.getMessage());
                    messageReceived.chipRedError("Error en respuesta de petición", serverType);
                }
            };

            mWebSocketClient.connect();
        }
    }

    public void getStationData()
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);
        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "tablet_obtener_datos_estacion");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    JSONObject station = response.getJSONObject("estacion");
                    messageReceived.showResponse(station, 0);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error en respuesta de petición", GET_STATION_DATA_WS);
            }
        };
        sendRequest(request, responseListener, errorListener);
    }

    public void getPcs()
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        if (!url.isEmpty() && !stationId.isEmpty())
        {
            setURL(url);
        }
        else
        {
            return;
        }

        final JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_get_posiciones");
            request.put("id_estacion", stationId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").equals("Ok"))
                    {
                        messageReceived.showResponse(response, GET_PCS);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"), GET_PCS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al procesar la información", GET_PCS);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error en respuesta de petición\nNo responde " +
                        "servidor", GET_PCS);
            }
        };
        sendRequest(request, responseListener, errorListener);
    }

    public void searchClient(int key, String value)
    {
        searchClient(key, value, false);
    }

    public void searchClient(int key, String value, boolean localStation)
    {
        String stationId = SharedPreferencesManager.getString(context,
                ChipREDConstants.STATION_MONGO_ID);
        String attributeToSearch = "id";

        switch (key)
        {
            case BYNAME:
                attributeToSearch = "nombre_cliente";
                break;

            case BYEMAIL:
                attributeToSearch = "email";
                break;

            case BYID:
                attributeToSearch = "id_cliente";
                break;

            case BYTAXCODE:
                attributeToSearch = "cedula_cliente";
                break;

            case BYPHONE:
                attributeToSearch = "telefono_cliente";
                break;

            case BYNAME_EMAIL:
                attributeToSearch = "email_nombre";
                break;
        }

        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "buscar_cliente");
            request.put(attributeToSearch, value);

            if (localStation)
                request.put("id_estacion", stationId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString());
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, SEARCH_CLIENT_WS);
                    }
                    else if (response.getString("response").contains("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                SEARCH_CLIENT_WS);
                    }
                    else
                    {
                        messageReceived.chipRedError("Error del servidor al buscar cliente",
                                SEARCH_CLIENT_WS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.e(TAG, error.toString());
                messageReceived.chipRedError("Error del servidor al buscar cliente",
                        SEARCH_CLIENT_WS);
            }
        };

        sendTagRequest(request, responseListener, errorListener, "searchClient");
    }

    public void searchPlates(int key, String value)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "buscar_placas");

            if (key == BYID)
            {
                request.put("qr_tag", value);
            }
            else if (key == BYPLATES)
            {
                request.put("placas", value);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    //Loggear respuesta
                    Log.d(TAG, response.toString(2));

                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, SEARCH_PLATES);
                    }
                    else
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                SEARCH_PLATES);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error del servidor al buscar placas\nNo responde " +
                                "servidor",
                        SEARCH_PLATES);
            }
        };

        cancellAllRequest();

        sendRequest(request, responseListener, errorListener, 20000);
    }

    public void searchVehicle(String platesToFind)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "buscar_vehiculos");
            request.put("clave", "placas");
            request.put("valor", platesToFind);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, SEARCH_VEHICLE);
                    }
                    else if (response.getString("response").contains("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                SEARCH_VEHICLE);
                    }
                    else
                    {
                        messageReceived.chipRedError("Error al buscar vehículo", SEARCH_VEHICLE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al buscar vehículo\nNo responde servidor",
                        SEARCH_VEHICLE);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void addOrModifyClient(JSONObject request, final boolean add, String clientId)
    {
        final String keyWord;
        if (add)
        {
            keyWord = "registrar";
        }
        else
        {
            keyWord = "modificar";
        }

        try
        {
            if (add)
            {
                request.put("key", "alta_cliente");
            }
            else
            {
                request.put("key", "modificar_datos_cliente");
                request.put("id_cliente", clientId);
            }
            request.put("combustible_billetes", "0");

            //Loggear request
            Log.d(TAG, request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                Log.d(TAG, response.toString());
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        if (add)
                        {
                            messageReceived.chipRedMessage("Registrado", ADD_OR_MODIFY_CLIENT_WS);
                        }
                        else
                        {
                            messageReceived.chipRedMessage("Modificado", ADD_OR_MODIFY_CLIENT_WS);
                        }
                    }
                    else if (response.getString("response").contains("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                ADD_OR_MODIFY_CLIENT_WS);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                ADD_OR_MODIFY_CLIENT_WS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error.networkResponse == null)
                {
                    messageReceived.chipRedError("Error al " + keyWord + " cliente\nNo responde "
                            + "servidor", ADD_OR_MODIFY_CLIENT_WS);
                }
            }
        };

        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        sendRequest(request, responseListener, errorListener);
    }

    public void addBalance(JSONObject addBalanceData)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        try
        {
            addBalanceData.put("key", "tablet_abonar_saldo");
            addBalanceData.put("id_estacion", stationId);
        }
        catch (JSONException e)
        {
            messageReceived.chipRedError("Error al agregar datos a enviar",
                    ADD_BALANCE_WS);
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response,
                                ADD_BALANCE_WS);
                    }
                    else if (response.getString("response").contains("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                ADD_BALANCE_WS);
                    }
                    else
                    {
                        messageReceived.chipRedError("Error del servidor al abonar saldo",
                                ADD_BALANCE_WS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al obtener respuesta de servidor",
                            ADD_BALANCE_WS);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al abonar saldo\nNo responde servidor",
                        ADD_BALANCE_WS);
            }
        };

        setURL(url);
        sendRequest(addBalanceData, responseListener, errorListener, 20000);
    }

    public void getPumpers()
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "despachadores_estacion");
            request.put("id_estacion", stationId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, GET_PUMPERS_WS);
                    }
                    else if (response.getString("response").contains("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                GET_PUMPERS_WS);
                    }
                    else
                    {
                        messageReceived.chipRedError("Error del servidor al obtener " +
                                "despachadores", GET_PUMPERS_WS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al modificar obtener despachadores\nNo " +
                        "responde servidor", GET_PUMPERS_WS);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void getStationProducts()
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "productos_estacion");
            request.put("id_estacion", stationId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, GET_STATION_PRODUCTS);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                GET_STATION_PRODUCTS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al modificar obtener productos\nNo " +
                        "responde servidor", GET_PUMPERS_WS);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void getLastSale(int loadingPosition)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "ultima_venta");
            request.put("id_estacion", stationId);
            request.put("posicion_carga", loadingPosition);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, GET_LAST_SALE);
                    }
                    else if (response.getString("response").contains("Aviso"))
                    {
                        if (response.getJSONObject("message").getInt("codigo") == 0)
                        {
                            messageReceived.chipRedMessage(ChipREDConstants.NO_LAST_SALE,
                                    GET_LAST_SALE);
                        }
                        else
                        {
                            messageReceived.chipRedError("La venta ya ha sido asignada",
                                    GET_LAST_SALE);
                        }
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"), GET_LAST_SALE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener última venta\nNo " + "responde " +
                        "servidor", GET_LAST_SALE);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void getLastSales(int loadingPosition, int offset)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "consultar_ultimos_consumos_estacion");
            request.put("id_estacion", stationId);
            request.put("posicion_carga", String.valueOf(loadingPosition));
            request.put("cantidad", 15);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, GET_LAST_SALES);
                    }
                    else if (response.getString("response").contains("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                GET_LAST_SALES);
                    }
                    else
                    {
                        if (response.has("message"))
                        {
                            messageReceived.chipRedError(response.getString("message"),
                                    GET_LAST_SALES);
                        }
                        else
                        {
                            messageReceived.chipRedError("Error al obtener últimas ventas",
                                    GET_LAST_SALES);
                        }
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener últimas ventas\nNo " + "responde " +
                        "servidor", GET_LAST_SALES);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void getGeneratedCoupon(String purchaseId)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "obtener_cupon");
            request.put("id_consumo", purchaseId);
            request.put("numero_estacion", stationId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, GET_GENERATED_COUPON);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                GET_GENERATED_COUPON);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener cupón\nNo " + "responde " +
                        "servidor", GET_GENERATED_COUPON);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void getTokencashPurchases(int offset, int limit, Date startDate, Date endDate, String status)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "consultar_tokens_de_cobro");
            request.put("numero_estacion", stationId);
            request.put("offset", offset);
            request.put("limite", limit);
            if (startDate != null && endDate != null)
            {
                // Convertir fechas a formato
                //yyyy-mm-dd hh:mm:ss
                SimpleDateFormat chipReddf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                String startDateString = chipReddf.format(startDate);
                String endDateString = chipReddf.format(endDate);
                request.put("fecha_inicial", startDateString);
                request.put("fecha_final", endDateString);
            }

            if (!status.isEmpty()){
                request.put("estatus", status);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, GET_TOKENCASH_PURCHASES);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                GET_TOKENCASH_PURCHASES);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener consumos de tokencash\nNo " +
                        "responde " +
                        "servidor", GET_TOKENCASH_PURCHASES);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void setCashSale(Sale sale, ChipREDClient chipREDClient, ArrayList<ExtraProduct>
            extraProducts, Vehicle vehicle, String saleCondition, int copies)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");
        String stationTaxCode = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_TAX_CODE, "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            //Objeto de datos de estacion
            JSONObject stationData = new JSONObject();
            stationData.put("rfc_cedula", stationTaxCode);

            //Armar datos del consumo
            request.put("key", "tablet_venta_efectivo");
            request.put("id", sale.getId());
            request.put("id_estacion", stationId);
            request.put("producto", sale.getNumProdcuto());
            request.put("precio", String.valueOf(sale.getPrecioUnitario()));
            request.put("cantidad", sale.getCantidad());
            request.put("costo", sale.getCosto());
            request.put("numero_dispensario", sale.getNumDispensario());
            request.put("posicion_carga", sale.getPosicionDeCarga());
            request.put("num_ticket", sale.getNumeroTicket());
            request.put("fecha_hora", sale.getFechaHora());
            request.put("comprobante_fiscal", sale.getTaxReceiptCode());
            request.put("medio_pago", sale.getPaymentMethodCode());
            request.put("datos_estacion", stationData);

            if (sale.getComments() != null)
            {
                request.put("observaciones", sale.getComments());
            }

            JSONObject cliente = new JSONObject();
            //Obtener datos del cliente
            if (chipREDClient != null)
            {
                cliente.put("id_cliente", chipREDClient.getIdCliente());
                cliente.put("nombre", chipREDClient.getNombre());
                cliente.put("clave_fiscal", chipREDClient.getClaveFiscal());
                cliente.put("email", chipREDClient.getEmail());

                //Agregar objeto de correos electrónicos
                JSONObject emails = new JSONObject();

                //Validar si hay más correos por agregar
                if (chipREDClient.getSelectedEmails() != null)
                {
                    //Adjuntar email principal
                    emails.put("principal", chipREDClient.getMainEmail());

                    JSONArray contactsArray = new JSONArray();
                    for (String contact : chipREDClient.getSelectedEmails())
                    {
                        contactsArray.put(contact);
                    }

                    //Agregar arreglo de contactos a objeto de correos
                    emails.put("contactos", contactsArray);
                }
                else
                {
                    emails.put("principal", chipREDClient.getEmail());
                }

                //Agregar objeto de correos a objeto de cliente
                cliente.put("emails", emails);

                //Obtener el pais del cliente
                JSONObject address = new JSONObject();
                address.put("pais", chipREDClient.getCountry());
                cliente.put("direccion", address);
            }
            //Agregar datos de cliente al objeto principal
            request.put("datos_cliente", cliente);

            JSONObject jsonVehicle = new JSONObject();
            //Obtener datos de vehículo
            if (vehicle != null)
            {
                jsonVehicle.put("km_actual", vehicle.getOdometer());
                jsonVehicle.put("km_anterior", vehicle.getLastOdometer());
                jsonVehicle.put("marchamo_actual", vehicle.getMarchamo());
                jsonVehicle.put("placas", vehicle.getPlate());
                jsonVehicle.put("rendimiento", vehicle.getPerformance(sale.getCantidad()));
                jsonVehicle.put("marchamo_anterior", vehicle.getLastMarchamo());
                jsonVehicle.put("numero_economico", vehicle.getCarNumber());
            }
            else
            {
                jsonVehicle.put("km_actual", "");
                jsonVehicle.put("km_anterior", "");
                jsonVehicle.put("marchamo_actual", "");
                jsonVehicle.put("placas", "");
                jsonVehicle.put("rendimiento", "");
                jsonVehicle.put("marchamo_anterior", "");
                jsonVehicle.put("numero_economico", "");
            }
            //Agregar datos de vehiculo al objeto principal
            request.put("vehiculo", jsonVehicle);

            //Agregar condicion de venta
            request.put("condicion_venta", saleCondition);

            //Obtener productos extra
            JSONArray productos = new JSONArray();
            if (extraProducts != null)
            {
                for (ExtraProduct extraProduct : extraProducts)
                {
                    double total = extraProduct.getPrecio() * extraProduct.getCantidad();

                    JSONObject aceiteAditivoJson = new JSONObject();
                    aceiteAditivoJson.put("codigo", extraProduct.getCodigo());
                    aceiteAditivoJson.put("descripcion", extraProduct.getDescripcion());
                    aceiteAditivoJson.put("cantidad", extraProduct.getCantidad());
                    aceiteAditivoJson.put("precio", extraProduct.getPrecio());
                    aceiteAditivoJson.put("total", total);
                    productos.put(aceiteAditivoJson);
                }
            }
            request.put("productos_extra", productos);

            //Indicar numero de reimpresiones
            request.put("num_reimpresiones", copies);

            Log.d(TAG, request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString(2));
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, SET_CASH_SALE);
                    }
                    else if (response.getString("response").contains("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                SET_CASH_SALE);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"), SET_CASH_SALE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.toString());
                messageReceived.chipRedError("Error al asignar venta\nNo " + "responde servidor",
                        SET_CASH_SALE);
            }
        };

        sendRequest(request, responseListener, errorListener, 60000);
    }

    public void addOrModifyVehicle(JSONObject request, boolean add)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        //Agregar el id de estación
        try
        {
            if (add)
            {
                request.put("id_estacion", stationId);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                ADD_OR_MODIFY_VEHICLE);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                ADD_OR_MODIFY_VEHICLE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al agregar vehículo\nNo responde " +
                        "servidor", ADD_OR_MODIFY_VEHICLE);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    //curl -X POST -H "Content-Type: application/json" -d'{"key":"tablet_obtener_datos_estacion"}'
    // http://192.168.0.50:9999/webservice/tablet/
    private void sendRequest(JSONObject request, Response.Listener<JSONObject> responseListener,
                             Response.ErrorListener errorListener)
    {
        // Validar URL
        if (url == null)
        {
            Log.v(TAG, "Request con URL nulo...");
            messageReceived.chipRedMessage("Error en petición a servidor", NO_URL_REQUEST);
            return;
        }

        // Obtener requestQueue
        RequestQueueSingelton.getInstance(context.getApplicationContext()).getRequestQueue();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, request,
                responseListener, errorListener);

        req.setRetryPolicy(new DefaultRetryPolicy(40000, 0, 0));

        Log.d(TAG, "Request: " + request.toString());
        RequestQueueSingelton.getInstance(context).addToRequestQueue(req);
    }

    private void sendRequest(JSONObject request, Response.Listener<JSONObject> responseListener,
                             Response.ErrorListener errorListener, int timeout)
    {
        // Obtener requestQueue
        RequestQueueSingelton.getInstance(context.getApplicationContext()).getRequestQueue();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, request,
                responseListener, errorListener);

        req.setRetryPolicy(new DefaultRetryPolicy(timeout, 0, 0));

        try
        {
            Log.d(TAG, "Request: " + request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        RequestQueueSingelton.getInstance(context).addToRequestQueue(req);
    }

    private void sendTagRequest(JSONObject request, Response.Listener<JSONObject> responseListener,
                                Response.ErrorListener errorListener, String tag)
    {
        // Obtener requestQueue
        final RequestQueue queue =
                RequestQueueSingelton.getInstance(context.getApplicationContext()).getRequestQueue();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, request,
                responseListener, errorListener);

        try
        {
            Log.d(TAG, "Request: " + request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        // Cancelar una petición anterior con el mismo tag
        queue.cancelAll(tag);

        // Definir tag a petición
        req.setTag(tag);

        // Enviar petición
        RequestQueueSingelton.getInstance(context).addToRequestQueue(req);
    }

    void getBalance(String clientId, String clientNip)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "tablet_consultar_saldos");
            request.put("id_cliente", clientId);
            request.put("nip", clientNip);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.has("Ok") || response.has("ok"))
                    {
                        messageReceived.showResponse(response, GET_BALANCE_WS);
                    }
                    else if (response.has("aviso"))
                    {
                        messageReceived.chipRedError(response.getString("aviso"), GET_BALANCE_WS);
                    }
                    else
                    {
                        messageReceived.chipRedError("Error al consultar saldo", GET_BALANCE_WS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al consultar saldo\nNo responde servidor",
                        GET_BALANCE_WS);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    void printBalance(JSONObject client)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "tablet_imprimir_saldos");
            request.put("cliente", client);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.has("Ok") || response.has("ok"))
                    {
                        messageReceived.chipRedMessage("Saldo impreso correctamente",
                                PRINT_BALANCE_WS);
                    }
                    else if (response.has("aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("aviso"),
                                PRINT_BALANCE_WS);
                    }
                    else
                    {
                        messageReceived.chipRedMessage("Error al imprimir saldo", PRINT_BALANCE_WS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al imprimir saldo\nNo responde servidor",
                        PRINT_BALANCE_WS);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    void getPurchases(String clientId)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "consultar_ultimos_consumos");
            request.put("id_cliente", clientId);
            request.put("cantidad", 10);

            Log.d(TAG, "Enviando petición: ");
            Log.d(TAG, request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, "Respuesta a petición: ");
                    Log.d(TAG, response.toString(2));

                    if (response.getString("response").contains("Ok") || response.getString
                            ("response").contains("ok"))
                    {
                        messageReceived.showResponse(response, GET_PURCHASES);
                    }
                    else if (response.getString("response").contains("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                GET_PURCHASES);
                    }
                    else if (response.getString("response").contains("Error"))
                    {
                        messageReceived.chipRedError(response.getString("message"), GET_PURCHASES);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al consultar consumos", GET_PURCHASES);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener consumos del cliente\nNo responde " +
                        "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + ""
                        + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" +
                        "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + ""
                        + "" + "" + "" + "" + "" + "" + "servidor", GET_PURCHASES);

                Log.d(TAG, "Error en respuesta de petición");
                Log.d(TAG, error.toString());
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    void getPayments(String clientId)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "consultar_ultimos_abonos");
            request.put("id_cliente", clientId);
            request.put("cantidad", 10);

            Timber.d("Enviando petición: ");
            Timber.d(request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Timber.d("Respuesta a petición: ");
                    Timber.d(response.toString(2));

                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, GET_PAYMENTS);
                    }
                    else if (response.getString("response").contains("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"), GET_PAYMENTS);
                    }
                    else if (response.getString("response").contains("Error"))
                    {
                        messageReceived.chipRedError(response.getString("message"), GET_PAYMENTS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al consultar abonos", GET_PAYMENTS);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener abonos del cliente\nNo responde "
                        + "servidor", GET_PAYMENTS);

                Timber.d("Error en respuesta de petición");
                Timber.d(error.toString());
            }
        };
        sendRequest(request, responseListener, errorListener);
    }

    public void getExtraProducts()
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "productos_extra_estacion");
            request.put("id_estacion", stationId);

            Timber.d("Enviando petición: ");
            Timber.d(request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Timber.d("Respuesta a petición: ");
                    Timber.d(response.toString(2));

                    //Obtener valor de la respuesta
                    String responseValue = response.getString("response");
                    if (responseValue.contains("Error") || responseValue.contains("Aviso"))
                    {
                        Timber.e("Error en la respuesta");
                        messageReceived.chipRedError("Error al obtener productos extra",
                                GET_EXTRA_PRODUCTS);
                    }
                    else
                    {
                        messageReceived.showResponse(response, GET_EXTRA_PRODUCTS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al obtener procesar información",
                            GET_EXTRA_PRODUCTS);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener productos extra\nNo responde " +
                        "servidor", GET_EXTRA_PRODUCTS);

                Timber.d("Error en respuesta de petición");
                Timber.d(error.toString());
            }
        };
        sendRequest(request, responseListener, errorListener);
    }

    public void generateToken(String purchaseId, double purchaseAmount, int pumpNumber)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "generar_token");
            request.put("numero_estacion", stationId);
            request.put("id", purchaseId);
            request.put("costo", purchaseAmount);
            request.put("posicion_carga", pumpNumber);

            Timber.d("Enviando petición: ");
            Timber.d(request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, "Respuesta a petición: ");
                    Log.d(TAG, response.toString(2));

                    //Obtener valor de la respuesta
                    String responseValue = response.getString("response");
                    if (responseValue.contains("Error"))
                    {
                        messageReceived.chipRedError(response.getString("message"), GENERATE_TOKEN);
                    }
                    else if (responseValue.contains("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                GENERATE_TOKEN);
                    }
                    else
                    {
                        messageReceived.showResponse(response, GENERATE_TOKEN);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al obtener procesar información",
                            GENERATE_TOKEN);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al generar Token\nNo responde " + "servidor",
                        GENERATE_TOKEN);

                Log.d(TAG, "Error en respuesta de petición");
                Log.d(TAG, error.toString());
            }
        };
        sendRequest(request, responseListener, errorListener);
    }

    public void getTokenInfo(String transactionId)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "consultar_pagos_token");
            request.put("transaccion", transactionId);
            request.put("numero_estacion", stationId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").equals("Ok"))
                    {
                        messageReceived.showResponse(response, GET_TOKEN);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"), GET_TOKEN);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al procesar la información", GET_TOKEN);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener información de token\nNo responde " +
                        "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + ""
                        + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" +
                        "" + "" + "" + "" + "" + "" + "" + "" + "servidor", GET_TOKEN);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void closeToken(String id, String transactionId, JSONArray payments, JSONObject summary)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "cerrar_pagos_token");
            request.put("id", id);
            request.put("transaccion", transactionId);
            request.put("payments", payments);
            request.put("summary", summary);

            Log.d(TAG, "Enviando petición: ");
            Log.d(TAG, request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, "Respuesta a petición: ");
                    Log.d(TAG, response.toString(2));

                    //Obtener valor de la respuesta
                    String responseValue = response.getString("response");
                    if (responseValue.contains("Ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"), CLOSE_TOKEN);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"), CLOSE_TOKEN);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al obtener procesar información",
                            CLOSE_TOKEN);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al cerrar pagos\nNo responde " + "servidor",
                        CLOSE_TOKEN);

                Log.d(TAG, "Error en respuesta de petición");
                Log.d(TAG, error.toString());
            }
        };
        sendRequest(request, responseListener, errorListener);
    }

    public void printTokensSummary(String pumperTag)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "solicitar_impresion_resumen_tokens");
            request.put("tag_despachador", pumperTag);
            request.put("numero_estacion", stationId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").equals("Ok"))
                    {
                        messageReceived.chipRedMessage("Ok", PRINT_TOKENS_SUMMARY);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                PRINT_TOKENS_SUMMARY);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al procesar la información",
                            PRINT_TOKENS_SUMMARY);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al solicitar la impresión\nNo responde " +
                        "servidor", PRINT_TOKENS_SUMMARY);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void configTokenCash(JSONObject tokenCashParameters)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        try
        {
            tokenCashParameters.put("key", "modificar_parametros_tokencash");
            tokenCashParameters.put("numero_estacion", stationId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").equals("Ok") || response.getString
                            ("response").equals("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                CONFIG_TOKEN_CASH);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                CONFIG_TOKEN_CASH);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al procesar la información",
                            CONFIG_TOKEN_CASH);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al configurar token cash\nNo responde " +
                        "servidor", CONFIG_TOKEN_CASH);
            }
        };

        sendRequest(tokenCashParameters, responseListener, errorListener);
    }

    public void getTokenCashConfiguration()
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "obtener_parametros_tokencash");
            request.put("numero_estacion", stationId);
            Log.d(TAG, request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").equals("Ok"))
                    {
                        messageReceived.showResponse(response, GET_TOKEN_CASH_CONFIG);
                    }
                    else if (response.getString("response").equals("Aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                GET_TOKEN_CASH_CONFIG);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                GET_TOKEN_CASH_CONFIG);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al procesar la información",
                            GET_TOKEN_CASH_CONFIG);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener configuración de token cash\nNo "
                        + "responde " + "servidor", GET_TOKEN_CASH_CONFIG);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void getClosedTokens(String pumperTag)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID,
                "");

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "obtener_tokens_cerrados");
            request.put("tag_despachador", pumperTag);
            request.put("numero_estacion", stationId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").equals("Ok"))
                    {
                        messageReceived.showResponse(response, GET_CLOSED_TOKENS);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                GET_CLOSED_TOKENS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    messageReceived.chipRedError("Error al procesar la información",
                            GET_CLOSED_TOKENS);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener tokens cerrados\nNo responde " +
                        "servidor", GET_CLOSED_TOKENS);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void reserveBilletSale(String clientId, JSONArray billetsIds, int pump, String plates,
                                  String platesUrl)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_datos_venta_billete");
            request.put("id_cliente", clientId);
            request.put("ids_billetes", billetsIds);
            request.put("posicion_carga", pump);
            request.put("placas", plates);
            request.put("url_placas", platesUrl);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.has("aviso"))
                    {
                        messageReceived.chipRedError(response.getString("aviso"),
                                GET_BILLET_DATA_WS);
                    }
                    else
                    {
                        messageReceived.showResponse(response, GET_BILLET_DATA_WS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener datos del Billete\nNo responde " +
                        "servidor", GET_BILLET_DATA_WS);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    // No reserva saldo
    public void getBilletData(final String billetId)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();
        try
        {
            request.put("key", "consultar_datos_billete");
            request.put("id_billete", billetId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG,
                            "Respuesta obtenida para billete " + billetId + " " + response.toString(2));
                    String responseVal = response.getString("response");
                    if (responseVal.contains("error"))
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                GET_BILLET_DATA_WS);
                    }
                    else if (responseVal.contains("aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                GET_BILLET_DATA_WS);
                    }
                    else
                    {
                        messageReceived.showResponse(response, GET_BILLET_DATA_WS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener datos del Billete\nNo responde " +
                        "servidor", GET_BILLET_DATA_WS);
            }
        };

        Log.d(TAG, "Buscando billete -> " + billetId);
        sendRequest(request, responseListener, errorListener);
    }

    public void completeClientSignUp(JSONObject request, String clientId)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        try
        {
            request.put("key", "completar_registro_cliente_cr");
            request.put("id_cliente", clientId);
            request.put("combustible_billetes", "0");

            Log.d(TAG, request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString(2));

                    if (response.getString("response").toLowerCase().equals("ok"))
                    {
                        messageReceived.showResponse(response, COMPLETE_CLIENT_SIGNUP);
                    }
                    else if (response.getString("response").toLowerCase().equals("aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                COMPLETE_CLIENT_SIGNUP);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                COMPLETE_CLIENT_SIGNUP);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.toString());
                messageReceived.chipRedError("Error al tratar de registrar cliente\nNo responde "
                        + "servidor", COMPLETE_CLIENT_SIGNUP);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void resendConfirmationEmail(String clientId)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();
        try
        {
            request.put("key", "reenviar_correo_confirmacion");
            request.put("id_cliente", clientId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.has("Ok") || response.has("ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                RESEND_CONFIRMATION_EMAIL);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                RESEND_CONFIRMATION_EMAIL);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al tratar de reenviar correo de " +
                        "confirmación\nNo responde " + "servidor", RESEND_CONFIRMATION_EMAIL);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void cancelBilletTransaction(String transactionId, int pump)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "tablet_cancelar_transaccion_billete");
            request.put("id_transaccion", transactionId);
            request.put("posicion_carga", pump);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.has("ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("ok"),
                                CANCEL_BILLET_TRANSACTION_WS);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("aviso"),
                                CANCEL_BILLET_TRANSACTION_WS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al cancelar transacción\nNo responde " +
                        "servidor", CANCEL_BILLET_TRANSACTION_WS);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void liberateLoadingPosition(int loadingPosition)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "tablet_liberar_posicion");
            request.put("posicion", loadingPosition);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.has("ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("ok"),
                                LIBERAR_POSICION);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("aviso"),
                                LIBERAR_POSICION);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al liberar posición de carga\nNo responde " +
                        "servidor", LIBERAR_POSICION);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public WebSocketClient openWebSocketConnection(String url)
    {
        URI uri;
        try
        {
            uri = new URI(url);
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
            messageReceived.chipRedError("Error al iniciar WebSocket", LOADING_POS_STATE_CHANGE);
            return null;
        }

        webSocketClient = new WebSocketClient(uri)
        {
            @Override
            public void onOpen(ServerHandshake serverHandshake)
            {
                if (serverHandshake.getHttpStatus() == 101)
                {
                    Log.v("WebSocket", "onOpen\n" + String.valueOf(serverHandshake.getHttpStatus
                            ()));
                    //messageReceived.chipRedMessage("Conexión exitosa", LOADING_POS_STATE_CHANGE);
                }
            }

            @Override
            public void onMessage(String s)
            {
                Log.v("WebSocket", "onMessage\n" + s);
                try
                {
                    JSONObject object = new JSONObject(s);
                    messageReceived.showResponse(object, LOADING_POS_STATE_CHANGE);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onClose(int i, String s, boolean b)
            {
                Log.v("WebSocket", "onClose\n" + s);
            }

            @Override
            public void onError(Exception e)
            {
                Log.v("WebSocket", "onError\n" + e.getMessage());
            }
        };

        webSocketClient.connect();
        return webSocketClient;
    }

    public void startSaleBillet(JSONObject temTransaction)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        try
        {
            temTransaction.put("key", "tablet_iniciar_venta_billete");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.has("ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("ok"), START_SALE_WS);
                    }
                    else
                    {
                        String message;
                        if (response.has("aviso"))
                        {
                            message = response.getString("aviso");
                        }
                        else
                        {
                            message = response.getString("error");
                        }
                        messageReceived.chipRedError(message, START_SALE_WS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al iniciar venta\nNo responde servidor",
                        START_SALE_WS);
            }
        };

        sendRequest(temTransaction, responseListener, errorListener);
    }

    public void setCmText(String text)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_texto_cajamatrix");
            request.put("texto", text);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.has("ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("ok"), SET_CM_TEXT);
                    }
                    else
                    {
                        messageReceived.chipRedError("Error al cambiar texto de CajaMatrix",
                                SET_CM_TEXT);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al cambiar texto de CajaMatrix\nNo responde "
                        + "servidor", SET_CM_TEXT);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void getPrinters()
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_consultar_impresoras");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, GET_PRINTERS);
                    }
                    else if (response.getString("response").contains("Error"))
                    {
                        messageReceived.chipRedError(response.getString("message"), GET_PRINTERS);
                    }
                    else
                    {
                        messageReceived.chipRedMessage(response.getString("message"), GET_PRINTERS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener impresoras\nNo responde " +
                        "servidor", GET_PRINTERS);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void setPrinters(JSONArray printers)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_modificar_insertar_impresoras");
            request.put("impresoras", printers);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"), SET_PRINTERS);
                    }
                    else if (response.getString("response").contains("Error"))
                    {
                        messageReceived.chipRedError(response.getString("message"), SET_PRINTERS);
                    }
                    else
                    {
                        messageReceived.chipRedMessage(response.getString("message"), SET_PRINTERS);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al definir impresoras\nNo responde " +
                        "servidor", SET_PRINTERS);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void deletePrinter(int pumpNumber)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_eliminar_impresora");
            request.put("posicion_carga", pumpNumber);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                REMOVE_PRINTER);
                    }
                    else if (response.getString("response").contains("Error"))
                    {
                        messageReceived.chipRedError(response.getString("message"), REMOVE_PRINTER);
                    }
                    else
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                REMOVE_PRINTER);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al remover impresora\nNo responde " +
                        "servidor", REMOVE_PRINTER);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void reprintSale(JSONObject sale, JSONObject client)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_reimprimir_consumo");
            request.put("datos_consumo", sale);
            //request.put("datos_cliente", client);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.chipRedMessage("Reimpresión realizada correctamente",
                                REPRINT_SALE);
                    }
                    else
                    {
                        String message = response.getString("message");
                        messageReceived.chipRedError(message, REPRINT_SALE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al re-imprimir consumo\nNo responde " +
                        "servidor", REPRINT_SALE);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void getPendingSale(int pumpNumber)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_ultima_venta");
            request.put("posicion_carga", pumpNumber);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, GET_PEDNING_SALE);
                    }
                    else if (response.getString("response").contains("Error"))
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                GET_PEDNING_SALE);
                    }
                    else
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                GET_PEDNING_SALE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener última venta\nNo responde " +
                        "servidor", GET_PEDNING_SALE);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void getPendingLocalSales(int pumpNumber, int offset)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_obtener_consumos_pendientes");
            request.put("posicion_carga", pumpNumber);
            request.put("idx_inicio", offset);
            request.put("limite", 10);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response, GET_PENDING_LOCAL_SALES);
                    }
                    else if (response.getString("response").contains("Error"))
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                GET_PENDING_LOCAL_SALES);
                    }
                    else
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                GET_PENDING_LOCAL_SALES);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener consumos pendientes\nNo responde "
                        + "servidor", GET_PEDNING_SALE);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void addTaxCodeToSale(JSONObject saleWithTaxCode)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_crear_factura_pendiente");
            request.put("consumo", saleWithTaxCode);
            Log.d(TAG, request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString(2));
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                ADD_TAX_CODE_TO_SALE);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                ADD_TAX_CODE_TO_SALE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al agregar cédula al consumo\nNo responde " +
                        "servidor", ADD_TAX_CODE_TO_SALE);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void checkVehicleQR(String qrCode)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "check_qr");
            request.put("qr_tag", qrCode);
            Log.d(TAG, request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString(2));
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response,
                                CHECK_VEHICLE_QR);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"),
                                CHECK_VEHICLE_QR);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al validar código QR\nNo responde " +
                        "servidor", CHECK_VEHICLE_QR);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void setVehicleQR(JSONObject data)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        if (!url.isEmpty())
        {
            setURL(url);
        }

        try
        {
            data.put("key", "asignar_placas");
            Log.d(TAG, data.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString(2));
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response,
                                SET_VEHICLE_QR);
                    }
                    else
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                SET_VEHICLE_QR);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al asignar código QR\nNo responde " +
                        "servidor", SET_VEHICLE_QR);
            }
        };

        sendRequest(data, responseListener, errorListener);
    }

    public void unsetVehicleQR(String qrTag)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject data = new JSONObject();

        try
        {
            data.put("key", "unasignar_placas");
            data.put("qr_tag", qrTag);
            Log.d(TAG, data.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString(2));
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response,
                                UN_SET_VEHICLE_QR);
                    }
                    else
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                UN_SET_VEHICLE_QR);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al desasignar código QR\nNo responde " +
                        "servidor", UN_SET_VEHICLE_QR);
            }
        };

        sendRequest(data, responseListener, errorListener);
    }

    /*
        Métodos para buscar direcciones de Provincia, Cantón, Distrito y Barrio
     */

    public void getCrAddressElement(final int type, ArrayList<String> parentCode)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            switch (type)
            {
                case GET_PROVINCIAS:
                    request.put("key", "buscar_provincias");
                    break;

                case GET_CANTONES:
                    request.put("key", "buscar_cantones");
                    request.put("codigo_provincia", parentCode.get(0));
                    break;

                case GET_DISTRITOS:
                    request.put("key", "buscar_distritos");
                    request.put("codigo_provincia", parentCode.get(0));
                    request.put("codigo_canton", parentCode.get(1));
                    break;

                case GET_BARRIOS:
                    request.put("key", "buscar_barrios");
                    request.put("codigo_provincia", parentCode.get(0));
                    request.put("codigo_canton", parentCode.get(1));
                    request.put("codigo_distrito", parentCode.get(2));
                    break;
            }
            Log.d(TAG, request.toString(2));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString(2));
                    if (response.getString("response").equals("Ok"))
                    {
                        messageReceived.showResponse(response, type);
                    }
                    else
                    {
                        messageReceived.chipRedError(response.getString("message"), type);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG, error.toString());
                messageReceived.chipRedError("Error al obtener direcciones\nNo responde " +
                        "servidor", type);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void reprintInvoice(JSONObject invoiceData, String printerIp)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        final JSONObject request = new JSONObject();

        try
        {
            request.put("key", "tablet_reimprimir_factura");
            request.put("datos_factura", invoiceData);
            request.put("ip_impresora", printerIp);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    if (response.getString("response").toLowerCase().equals("ok"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                REPRINT_INVOICE);
                    }
                    else if (response.getString("response").toLowerCase().equals("aviso"))
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                REPRINT_INVOICE);
                    }
                    else
                    {
                        messageReceived.chipRedMessage("Error al realizar reimpresión",
                                REPRINT_INVOICE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al reimprimir\nNo responde servidor",
                        REPRINT_INVOICE);
            }
        };

        sendRequest(request, responseListener, errorListener, 10000);
    }

    /**
     * Método: getNetworkState
     *
     * @param: vacío
     * @return: vacío
     * <p>
     * Descripción:
     * Solicita al servidor local el estado actual de la red, la respuesta puede tener 3 valores:
     * 0 - Conexión funcionando correctamente
     * 1 - Wolke no responde
     * 2 - Método de contingenia manual activado
     */

    public void getNetworkState()
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_obtener_estado_red");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString(2));
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response,
                                GET_NETWORK_STATE);
                    }
                    else
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                GET_NETWORK_STATE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al obtener estado de la red\nNo responde " +
                        "servidor", GET_NETWORK_STATE);
            }
        };

        sendRequest(request, responseListener, errorListener);
    }

    public void setEstadoContingencia(boolean estado)
    {
        String url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);

        if (!url.isEmpty())
        {
            setURL(url);
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "tablet_modificar_estado_contingencia");
            request.put("estado", estado);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString(2));
                    if (response.getString("response").contains("Ok"))
                    {
                        messageReceived.showResponse(response,
                                SET_ESTADO_CONTINGENCIA);
                    }
                    else
                    {
                        messageReceived.chipRedMessage(response.getString("message"),
                                SET_ESTADO_CONTINGENCIA);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                messageReceived.chipRedError("Error al definir estado de contingencia manual\nNo " +
                        "responde " +
                        "servidor", SET_ESTADO_CONTINGENCIA);
            }
        };

        sendRequest(request, responseListener, errorListener, 20000);
    }

    public void cancellAllRequest()
    {
        // Obtener requestQueue
        RequestQueue queue =
                RequestQueueSingelton.getInstance(context.getApplicationContext()).getRequestQueue();
        queue.cancelAll(new RequestQueue.RequestFilter()
        {
            @Override
            public boolean apply(Request<?> request)
            {
                return true;
            }
        });
    }

    public void setMessageReceived(OnMessageReceived messageReceived)
    {
        this.messageReceived = messageReceived;
    }

    public interface OnMessageReceived
    {
        void chipRedMessage(String crMessage, int webServiceN);

        void chipRedError(String errorMessage, int webServiceN);

        void showResponse(JSONObject response, int webServiceN);
    }
}
