package com.binarium.chipredes;

public class LoadingPosition
{
    private int cmPort;
    private int dispenserNumber;
    private int side;
    private int realState;
    private int userStationSide;
    private int stationSide;
    private int state;

    private boolean selected = false;

    private String cmState;
    private String cmIp;
    private String sideTag;

    public LoadingPosition(int stationSide)
    {
        this.stationSide = stationSide;
    }

    public String getCmIp()
    {
        return cmIp;
    }

    public int getCmPort()
    {
        return cmPort;
    }

    public String getCmState()
    {
        return cmState;
    }

    public int getDispenserNumber()
    {
        return dispenserNumber;
    }

    public int getRealState()
    {
        return realState;
    }

    public int getSide()
    {
        return side;
    }

    public String getSideTag()
    {
        return sideTag;
    }

    public int getState()
    {
        return state;
    }

    public int getStationSide()
    {
        return stationSide;
    }

    public void setCmIp(String cmIp)
    {
        this.cmIp = cmIp;
    }

    public void setCmPort(int cmPort)
    {
        this.cmPort = cmPort;
    }

    public void setDispenserNumber(int dispenserNumber)
    {
        this.dispenserNumber = dispenserNumber;
    }

    public void setSide(int side)
    {
        this.side = side;
    }

    public void setRealState(int realState)
    {
        this.realState = realState;
    }

    public void setStationSide(int stationSide)
    {
        this.stationSide = stationSide;
    }

    public void setState(int state)
    {
        this.state = state;
    }

    public void setCmState(String cmState)
    {
        this.cmState = cmState;
    }

    public void setSideTag(String sideTag)
    {
        this.sideTag = sideTag;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public int getUserStationSide()
    {
        return userStationSide;
    }

    public void setUserStationSide(int userStationSide)
    {
        this.userStationSide = userStationSide;
    }
}
