package com.binarium.chipredes;

import android.content.Context;

import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ExtraProductArrayAdapter extends RecyclerView.Adapter<ExtraProductArrayAdapter.ExtraProductViewHolder>
        implements View.OnClickListener, Filterable
{
    private ArrayList<ExtraProduct> extraProducts;
    private Context context;
    private View.OnClickListener listener;
    private String pais;
    private FiltroElementos filtro;

    private AdapterInterface adapterInterface;

    public ExtraProductArrayAdapter(ArrayList<ExtraProduct> extraProducts,
                                    Context context, AdapterInterface adapterInterface)
    {
        this.extraProducts = extraProducts;
        this.context = context;
        this.adapterInterface = adapterInterface;

        pais = SharedPreferencesManager.getString(context, ChipREDConstants.COUNTRY, "");
    }

    public void setExtraProducts(ArrayList<ExtraProduct> extraProducts)
    {
        this.extraProducts = extraProducts;
    }

    public ArrayList<ExtraProduct> getExtraProducts()
    {
        return extraProducts;
    }

    @Override
    public int getItemCount()
    {
        return extraProducts.size();
    }

    @Override
    public ExtraProductViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.extra_product_layout,
                viewGroup,
                false);

        ExtraProductViewHolder aavh = new ExtraProductViewHolder(v, context, extraProducts);

        v.setOnClickListener(this);
        return aavh;
    }

    @Override
    public void onBindViewHolder(final ExtraProductViewHolder extraProductViewHolder, int i)
    {
        extraProductViewHolder.desc.setText(extraProducts.get(i).getDescripcion());
        extraProductViewHolder.categoria.setText(extraProducts.get(i).getCategoria());
        extraProductViewHolder.cantidad.setText(String.valueOf(extraProducts.get(i).getCantidad()));

        int existencias = extraProducts.get(i).getExistencias();
        extraProductViewHolder.existencias.setText(String.valueOf(existencias));

        if (existencias < 20 && existencias >= 10)
        {
            extraProductViewHolder.existencias.setTextColor(
                    context.getResources().getColor(R.color.orange)
            );
        }
        else if (existencias < 10)
        {
            extraProductViewHolder.existencias.setTextColor(
                    context.getResources().getColor(R.color.red)
            );
        }
        else
        {
            extraProductViewHolder.existencias.setTextColor(
                    context.getResources().getColor(R.color.green_money)
            );
        }

        String importe;
        if (pais.equals("costa rica"))
        {
            importe =
                    ChipREDConstants.CR_AMOUNT_FORMAT.format(extraProducts.get(i).getPrecio()) +
                            " CRC";
        }
        else
        {
            importe =
                    ChipREDConstants.MX_AMOUNT_FORMAT.format(extraProducts.get(i).getPrecio()) +
                            " MXN";
        }

        extraProductViewHolder.importe.setText(importe);
        extraProductViewHolder.quitar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setAddOrRemoveListener(extraProductViewHolder, false);
            }
        });

        extraProductViewHolder.agregar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setAddOrRemoveListener(extraProductViewHolder, true);
            }
        });

        if (extraProducts.get(i).isAgotado())
        {
            extraProductViewHolder.cardView.setCardBackgroundColor(
                    ContextCompat.getColor(context, R.color.cancelado));
        }
        else
        {
            if (extraProducts.get(i).isSelected())
            {
                extraProductViewHolder.cardView.setCardBackgroundColor(
                        ContextCompat.getColor(context, R.color.activo));
            }
            else
            {
                extraProductViewHolder.cardView.setCardBackgroundColor(
                        ContextCompat.getColor(context, R.color.white));
            }
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    public static class ExtraProductViewHolder extends RecyclerView.ViewHolder
    {
        private TextView desc;
        private TextView categoria;
        private TextView existencias;
        private TextView cantidad;
        private TextView importe;

        private ImageView agregar;
        private ImageView quitar;

        private CardView cardView;

        public ExtraProductViewHolder(View itemView, Context context,
                                      ArrayList<ExtraProduct> extraProducts)
        {
            super(itemView);

            desc = (TextView) itemView.findViewById(R.id.desc_aceite_aditivo);
            categoria = (TextView) itemView.findViewById(R.id.categoria_aceite_aditivo);
            existencias = (TextView) itemView.findViewById(R.id.existencias_aceite_aditivo);
            cantidad = (TextView) itemView.findViewById(R.id.cantidad_aceite_aditivo);
            importe = (TextView) itemView.findViewById(R.id.importe_aceite_aditivo);

            agregar = (ImageView) itemView.findViewById(R.id.agregar_aceite_aditivo);
            quitar = (ImageView) itemView.findViewById(R.id.quitar_aceite_aditivo);

            cardView = (CardView) itemView.findViewById(R.id.extra_product_card);
        }
    }

    public interface AdapterInterface
    {
        void removedOrAddedItem(ExtraProduct extraProduct);
    }

    @Override
    public Filter getFilter()
    {
        if (filtro == null)
        {
            filtro = new FiltroElementos(extraProducts, this);
        }

        return filtro;
    }

    public void setAddOrRemoveListener(ExtraProductViewHolder extraProductViewHolder, boolean add)
    {
        ExtraProduct extraProduct = extraProducts
                .get(extraProductViewHolder.getAdapterPosition());

        if (!extraProduct.isAgotado())
        {
            //Darle valor inicial a la variable de cantidad en caso de que no se cumpla ninguna
            // condicion
            int nuevaCantidad = extraProduct.getCantidad();

            if (add)
            {
                if (extraProduct.getCantidad() < extraProduct.getExistencias())
                {
                    if (nuevaCantidad == 0)
                    {
                        extraProduct.setSelected(true);
                        extraProductViewHolder.cardView.setCardBackgroundColor(
                                ContextCompat.getColor(context, R.color.activo));
                    }
                    nuevaCantidad = extraProduct.getCantidad() + 1;
                }
            }
            else
            {
                if (extraProduct.getCantidad() > 0)
                {
                    nuevaCantidad = extraProduct.getCantidad() - 1;
                    if (nuevaCantidad == 0)
                    {
                        extraProduct.setSelected(false);
                        extraProductViewHolder.cardView.setCardBackgroundColor(
                                ContextCompat.getColor(context, R.color.white));
                    }
                }
            }

            extraProduct.setCantidad(nuevaCantidad);
            double nuevoImporte = nuevaCantidad * extraProduct.getPrecio();
            extraProductViewHolder.cantidad.setText(String.valueOf(nuevaCantidad));

            if (pais.equals("costa rica"))
                extraProductViewHolder.importe.setText(ChipREDConstants.CR_AMOUNT_FORMAT.format(nuevoImporte) + " CRC");
            else
                extraProductViewHolder.importe.setText(ChipREDConstants.MX_AMOUNT_FORMAT.format(nuevoImporte) + " MXN");

            adapterInterface.removedOrAddedItem(extraProduct);
        }
    }
}
