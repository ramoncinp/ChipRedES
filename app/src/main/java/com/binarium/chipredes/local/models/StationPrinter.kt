package com.binarium.chipredes.local.models

class StationPrinter(map: Map<*, *>) {
    var ip: String = map["ip"] as String
    var estado: String = map["estado"] as String
    var posicionCarga: Int = (map["posicion_carga"] as Double).toInt()
}