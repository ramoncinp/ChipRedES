package com.binarium.chipredes.local

import com.binarium.chipredes.local.models.GetSaleRequestPost
import com.binarium.chipredes.local.models.GetSaleRequestResult
import com.binarium.chipredes.local.services.GetPrintersPost
import com.binarium.chipredes.local.services.GetPrintersResponse
import com.binarium.chipredes.local.services.StartBilletSalePost
import com.binarium.chipredes.wolke.models.TestConnectionPost
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import java.io.IOException
import java.util.concurrent.TimeUnit

private const val BASE_URL = "http://192.168.0.50/"

interface LocalApiService {

    @Headers("Content-Type: application/json")
    @POST("/")
    suspend fun getLocalPrinters(@Body bodyRequest: GetPrintersPost): Response<GetPrintersResponse>

    @Headers("Content-Type: application/json")
    @POST("/")
    suspend fun startBilletSale(@Body bodyRequest: StartBilletSalePost): ResponseBody

    @Headers("Content-Type: application/json")
    @POST("/")
    suspend fun testConnection(@Body bodyRequest: TestConnectionPost): ResponseBody

    @Headers("Content-Type: application/json")
    @POST("/")
    suspend fun getSaleRequests(@Body bodyRequest: GetSaleRequestPost): Response<GetSaleRequestResult>
}

object LocalService {
    var host: String = ""

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addConverterFactory(ScalarsConverterFactory.create())
        .client(getHttpClient())
        .baseUrl(BASE_URL)
        .build()

    private fun getHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                return try {
                    val request: Request = chain
                        .request()
                        .newBuilder()
                        .url(host)
                        .addHeader("Content-Type", "application/json")
                        .build()
                    chain.proceed(request)
                } catch (e: Exception) {
                    chain.proceed(chain.request())
                }
            }
        })

        httpClient.connectTimeout(5, TimeUnit.SECONDS)
        return httpClient.build()
    }

    val retrofitService: LocalApiService by lazy {
        retrofit.create(LocalApiService::class.java)
    }
}