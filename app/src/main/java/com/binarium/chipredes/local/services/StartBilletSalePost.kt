package com.binarium.chipredes.local.services

import com.squareup.moshi.Json

data class StartBilletSalePost(
        val key: String = "tablet_iniciar_venta_billete",
        @Json(name = "id_venta_billete") val reservationId: String,
        @Json(name = "id_cuenta") val clientAccountId: String,
        @Json(name = "id_cliente") val clientId: String,
        @Json(name = "posicion") val loadingPosition: Int,
        val combustible: Int = 0,
        val cantidad: Double,
        val autorizacion: String,
)