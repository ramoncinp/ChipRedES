package com.binarium.chipredes.local.domain

import com.binarium.chipredes.local.LocalApiService
import com.binarium.chipredes.local.services.StartBilletSalePost
import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import okhttp3.ResponseBody
import org.json.JSONObject
import java.lang.Exception
import javax.inject.Inject

class StartBilletSaleUseCase @Inject constructor(
    private val localApiService: LocalApiService,
    private val logger: Logger
) {

    suspend operator fun invoke(data: StartBilletSalePost): StartBilletSaleResult {
        return try {
            val startSaleResponse = localApiService.startBilletSale(data).toJson()
            if (startSaleResponse.has("ok")) {
                postSuccessMessage(data.clientId)
                StartBilletSaleResult.Success
            } else {
                val errorMessage: String = if (startSaleResponse.has("aviso")) {
                    startSaleResponse.getString("aviso")
                } else {
                    startSaleResponse.getString("error")
                }
                postError("Error al enviar preset(Local): $errorMessage", data.clientId)
                StartBilletSaleResult.Error("Error al enviar preset: $errorMessage")
            }
        } catch (e: Exception) {
            postError("Error al enviar preset: ${e.message}", data.clientId)
            StartBilletSaleResult.Error("Error al enviar preset")
        }
    }

    private fun ResponseBody.toJson() = JSONObject(string())

    private fun postError(message: String, clientId: String) {
        logger.postMessage(
            message = message,
            level = LogLevel.ERROR,
            client = clientId
        )
    }

    private fun postSuccessMessage(clientId: String) {
        logger.postMessage(
            "Reservacion enviada al dispensario",
            LogLevel.INFO,
            client = clientId
        )
    }
}

sealed class StartBilletSaleResult {
    object Success : StartBilletSaleResult()
    data class Error(val message: String) : StartBilletSaleResult()
}
