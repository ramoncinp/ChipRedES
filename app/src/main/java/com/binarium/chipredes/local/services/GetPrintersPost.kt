package com.binarium.chipredes.local.services

import com.squareup.moshi.Json

data class GetPrintersPost(
    val key: String = "tablet_consultar_impresoras"
)

data class GetPrintersResponse(
    val message: String? = null,
    val data: List<Printer>? = null,
    val jsonapi: Jsonapi? = null,
    val response: String? = null
)

data class Printer(
    val ip: String? = null,
    val estado: String? = null,
    @Json(name = "posicion_carga") val posicionCarga: Long? = null
)

data class Jsonapi(
    val version: String? = null
)
