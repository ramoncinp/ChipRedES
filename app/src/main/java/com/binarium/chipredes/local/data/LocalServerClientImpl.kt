package com.binarium.chipredes.local.data

import android.content.SharedPreferences
import android.net.Uri
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.emax.model.EmaxPurchase
import com.binarium.chipredes.local.data.DispenserCommandsParams.CANCELACION_VALUE
import com.binarium.chipredes.local.data.DispenserCommandsParams.CANTIDAD_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.COMANDO_11_VALUE
import com.binarium.chipredes.local.data.DispenserCommandsParams.COMANDO_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.COSTO_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.DISPENSARIO_CONTADOR_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.FECHA_HORA_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.FLAG_IMPRESION_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.FLAG_TIPO_VENTA_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.ID_CLIENTE_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.ID_DESPACHADOR_EMAX_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.ID_EMAX_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.ID_PRODUCTO_EMAX_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.ID_TURNO_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.ID_VEHICULO_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.NO_DISPONIBLE_VALUE
import com.binarium.chipredes.local.data.DispenserCommandsParams.NUMERO_DISPENSARIO_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.NUMERO_ESTACION_EMAX_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.NUMERO_MANGUERA_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.NUM_TICKET_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.ODOMETRO_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.POSICION_CARGA_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.PREAUTORIZACION_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.PRECIO_UNITARIO_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.TAG_CLIENTE_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.TAG_DESPACHADOR_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.TIPO_CLIENTE_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.TIPO_VENTA_KEY
import com.binarium.chipredes.local.data.DispenserCommandsParams.VENTA_DISPENSARIO_VALUE
import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.utils.DateUtils.dateToChipredDateString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import timber.log.Timber
import java.io.IOException
import java.util.Date

class LocalServerClientImpl(
    private val sharedPreferences: SharedPreferences,
    private val logger: Logger
) : LocalServerClient {

    private val client = OkHttpClient()

    override suspend fun registerDispenserSale(sale: EmaxPurchase): String? =
        withContext(Dispatchers.IO) {
            val localCommandsUrl = buildUrl()
            val url = localCommandsUrl.toHttpUrlOrNull()
                ?.newBuilder()
                ?.addQueryParameter(KEY, VENTA_DISPENSARIO_VALUE)
                ?.addQueryParameter(COMANDO_KEY, COMANDO_11_VALUE)
                ?.addQueryParameter(NUMERO_DISPENSARIO_KEY, sale.dispenser)
                ?.addQueryParameter(ID_DESPACHADOR_EMAX_KEY, sale.pumperId)
                ?.addQueryParameter(NUMERO_ESTACION_EMAX_KEY, NO_DISPONIBLE_VALUE)
                ?.addQueryParameter(ID_PRODUCTO_EMAX_KEY, sale.product)
                ?.addQueryParameter(PRECIO_UNITARIO_KEY, sale.unitPrice.toString())
                ?.addQueryParameter(CANTIDAD_KEY, sale.liters.toString())
                ?.addQueryParameter(COSTO_KEY, sale.amount.toString())
                ?.addQueryParameter(POSICION_CARGA_KEY, sale.loadingPosition)
                ?.addQueryParameter(PREAUTORIZACION_KEY, sale.preAuth)
                ?.addQueryParameter(FECHA_HORA_KEY, sale.dateTime)
                ?.addQueryParameter(NUM_TICKET_KEY, sale.ticket)
                ?.addQueryParameter(TAG_DESPACHADOR_KEY, sale.pumperId)
                ?.addQueryParameter(ODOMETRO_KEY, sale.odometer)
                ?.addQueryParameter(ID_CLIENTE_KEY, sale.clientId)
                ?.addQueryParameter(NUMERO_MANGUERA_KEY, sale.hoseNumber)
                ?.addQueryParameter(DISPENSARIO_CONTADOR_KEY, sale.counter)
                ?.addQueryParameter(ID_TURNO_KEY, NO_DISPONIBLE_VALUE)
                ?.addQueryParameter(FLAG_TIPO_VENTA_KEY, sale.saleType)
                ?.addQueryParameter(FLAG_IMPRESION_KEY, NO_DISPONIBLE_VALUE)
                ?.addQueryParameter(ID_VEHICULO_KEY, sale.vehicle)
                ?.addQueryParameter(ID_EMAX_KEY, sale.id.toString())
                ?.addQueryParameter(TAG_CLIENTE_KEY, NO_DISPONIBLE_VALUE)
                ?.addQueryParameter(TIPO_CLIENTE_KEY, "1")
                ?.addQueryParameter(TIPO_VENTA_KEY, if (sale.clientId == "1") "0" else "1")
                ?.build() ?: return@withContext null

            val request = Request.Builder()
                .url(url)
                .get()
                .build()

            try {
                val response = client.newCall(request).execute()
                response.body.toString()
            } catch (e: IOException) {
                Timber.e(e)
                logger.postMessage(
                    message = "Error al registrar venta a servidor local: ${e.message}",
                    level = LogLevel.ERROR,
                    client = sale.clientId
                )
                null
            }
        }

    override suspend fun cancelSale(preAuth: String): String? =
        withContext(Dispatchers.IO) {
            val localCommandsUrl = buildUrl()
            val dateTime = dateToChipredDateString(Date(), false)
            val url = localCommandsUrl.toHttpUrlOrNull()
                ?.newBuilder()
                ?.addQueryParameter(KEY, CANCELACION_VALUE)
                ?.addQueryParameter(COMANDO_KEY, COMANDO_11_VALUE)
                ?.addQueryParameter(PREAUTORIZACION_KEY, preAuth)
                ?.addQueryParameter(FECHA_HORA_KEY, dateTime)
                ?.build() ?: return@withContext null

            val request = Request.Builder()
                .url(url)
                .get()
                .build()

            try {
                val response = client.newCall(request).execute()
                response.body.toString()
            } catch (e: IOException) {
                Timber.e(e)
                logger.postMessage(
                    message = "Error al cancelar reservacion a servidor local: ${e.message}",
                    level = LogLevel.ERROR
                )
                null
            }
        }

    private fun buildUrl(): String {
        val localUrl = sharedPreferences.getString(ChipREDConstants.CR_LOCAL_IP, "").toString()
        val uri = try {
            Uri.parse(localUrl)
        } catch (e: Exception) {
            null
        }

        return uri?.let { "http://${it.host}:${it.port}/webservice/comandos_dispensario/" } ?: ""
    }
}
