package com.binarium.chipredes.local.domain

import com.binarium.chipredes.local.LocalApiService
import com.binarium.chipredes.wolke.models.TestConnectionPost
import java.lang.Exception
import javax.inject.Inject

class TestLocalConnectionUseCase @Inject constructor(
    private val localApiService: LocalApiService
) {

    suspend operator fun invoke(): TestLocalResult {
        return try {
            localApiService.testConnection(TestConnectionPost())
            TestLocalResult.Success
        } catch (e: Exception) {
            TestLocalResult.Error("Error al probar conexión")
        }
    }
}

sealed class TestLocalResult {
    object Success: TestLocalResult()
    data class Error(val message: String): TestLocalResult()
}
