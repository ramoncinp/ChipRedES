package com.binarium.chipredes.local.models

import com.binarium.chipredes.local.services.Jsonapi
import com.squareup.moshi.Json

data class GetSaleRequestPost(
    val key: String = "tablet_obtener_solicitud_venta",
    @Json(name = "id_reservacion") val reservationId: String,
)

data class GetSaleRequestResult(
    val message: String? = "tablet_obtener_solicitud_venta",
    val data: SaleRequestData?,
    val jsonapi: Jsonapi,
    val response: String?
)

data class SaleRequestData (
    @Json(name = "id_cuenta") val idCuenta: String,
    val estatus: String,
    val preautorizacion: String,
    val cantidad: Long,
    @Json(name = "fecha_solicitud") val fechaSolicitud: String,
    @Json(name = "mensaje_wolke_2") val mensajeWolke2: String?,
    val combustible: Long,
    @Json(name = "fecha_hora") val fechaHora: String?,
    @Json(name = "id_reservacion") val idReservacion: String,
    @Json(name = "lado_estacion") val ladoEstacion: Long,
    val id: String,
    @Json(name = "id_cliente") val idCliente: String,
    @Json(name = "tipo_venta") val tipoVenta: String
)
