package com.binarium.chipredes.local.data

import com.binarium.chipredes.emax.model.EmaxPurchase

interface LocalServerClient {

    suspend fun registerDispenserSale(sale: EmaxPurchase): String?

    suspend fun cancelSale(preAuth: String): String?
}
