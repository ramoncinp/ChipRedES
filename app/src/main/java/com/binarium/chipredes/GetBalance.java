package com.binarium.chipredes;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.barcode.QRScannerActivity;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetBalance extends AppCompatActivity implements CardView.OnClickListener
{
    private LinearLayout thinkingLayout;
    private CardView clientDataLayout;
    private ScrollView balanceLayout;

    //EditTexts
    private MaterialAutoCompleteTextView clientNameEmail;
    private MaterialEditText clientNipEditText;

    private ArrayAdapter<String> clientsAdapter;
    private ArrayList<String> clientIds;

    private Button requestBalance;

    private CardView searchQrButton;

    private JSONObject clientDataToPrint;

    private TextView errorMessage;
    private TextView clientEmailEt;
    private TextView[] balanceViews;
    private TextView[] balanceItemsViews;

    private ChipRedManager chipRedManager;

    private String clientId, clientNip, clientMail, country;

    private boolean byQr;

    public GetBalance()
    {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_get_balance);

        thinkingLayout = findViewById(R.id.get_balance_think);
        clientDataLayout = findViewById(R.id.get_balance_select_client_layout);
        balanceLayout = findViewById(R.id.get_balance_content);

        clientNameEmail = findViewById(R.id.get_balance_select_client);
        clientNipEditText = findViewById(R.id.get_balance_client_password);

        requestBalance = findViewById(R.id.get_balance_request);

        balanceViews = new TextView[3];
        balanceItemsViews = new TextView[3];

        clientEmailEt = findViewById(R.id.get_balance_client_email);
        balanceItemsViews[0] = findViewById(R.id.balanceitm0);
        balanceItemsViews[1] = findViewById(R.id.balanceitm1);
        balanceItemsViews[2] = findViewById(R.id.balanceitm2);

        balanceViews[0] = findViewById(R.id.balance0);
        balanceViews[1] = findViewById(R.id.balance1);
        balanceViews[2] = findViewById(R.id.balance2);

        balanceLayout.setVisibility(View.GONE);
        thinkingLayout.setVisibility(View.GONE);
        clientDataLayout.setVisibility(View.VISIBLE);

        errorMessage = findViewById(R.id.get_balance_error_message);
        searchQrButton = findViewById(R.id.read_client_qr);

        setTitle("Consultar saldo");
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                //Si ocurrió algun error y la variable era verdadera
                byQr = false;
                if (webServiceN == ChipRedManager.PRINT_BALANCE_WS)
                {
                    Toast.makeText(GetBalance.this, crMessage, Toast.LENGTH_SHORT).show();
                    setBalanceViews(null);
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                //Si ocurrió algun error y la variable era verdadera
                byQr = false;
                if (webServiceN == ChipRedManager.GET_BALANCE_WS)
                {
                    setErrorLayout(errorMessage);
                }
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                if (webServiceN == ChipRedManager.SEARCH_CLIENT_WS)
                {
                    //Si el servicio se pidió por leer un código QR
                    if (byQr)
                    {
                        //Obtener datos recibidos del servicio y asignarlos
                        setGottenClientInfo(response);
                        //Volver la variable a falso con la información ya procesada
                        byQr = false;
                    }
                    else //O si se pidio por seleccionar un correo de cliente
                    {
                        updateClientsSuggestionList(response);
                    }
                }
                else
                {
                    setBalanceViews(response);
                }
            }
        }, this);

        clientNameEmail.addTextChangedListener(setClientEtTextWatcher());

        requestBalance.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (validateData())
                {
                    setThinkingLayout(null);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            chipRedManager.getBalance(clientId, clientNip);
                        }
                    }, 500);
                }
            }
        });

        Button printButton = findViewById(R.id.get_balance_print);
        printButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (clientDataToPrint != null)
                {
                    setThinkingLayout("Imprimiendo saldo...");

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            chipRedManager.printBalance(clientDataToPrint);
                        }
                    }, 500);
                }
                else
                {
                    Toast.makeText(GetBalance.this, "No existe un cliente seleccionado", Toast
                            .LENGTH_SHORT).show();
                }
            }
        });

        //Asignar listener a botón de busqueda por QR
        searchQrButton.setOnClickListener(this);
        country = SharedPreferencesManager.getString(this, ChipREDConstants.COUNTRY, "mexico");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ChipREDConstants.READ_QR_REQUEST)
        {
            if (resultCode == RESULT_OK)
            {
                //Usar el código
                if (data != null)
                {
                    byQr = true;

                    String qrCode = data.getData().toString();
                    chipRedManager.searchClient(ChipRedManager.BYID, qrCode);
                    Log.d("GetBalance", "Read QR: " + qrCode);
                }
            }
        }
    }

    /**
     * Método de onClick que escuche cuando se presione el botón de buscar cliente
     * por lectura de código QR
     *
     * @param v: View al cual se le asigna el listener
     */
    @Override
    public void onClick(View v)
    {
        //Ocultar teclado
        hideKeyboard();

        //Iniciar actividad para leer QR
        startActivityForResult(new Intent(GetBalance.this, QRScannerActivity.class),
                ChipREDConstants.READ_QR_REQUEST);
    }

    private void updateClientsSuggestionList(JSONObject response)
    {
        try
        {
            JSONArray clients = response.getJSONArray("data");
            clientIds = null;
            clientIds = new ArrayList<>();

            String[] clientsNames = new String[clients.length()];
            for (int i = 0; i < clients.length(); i++)
            {
                JSONObject client = clients.getJSONObject(i);

                String value = client.getString("nombre") + " - " + client.getString("email");

                clientsNames[i] = value;
                clientIds.add(i, client.getString("id"));
            }

            clientsAdapter = new ArrayAdapter<>(this, android.R.layout
                    .simple_dropdown_item_1line, clientsNames);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        clientNameEmail.setAdapter(clientsAdapter);
        clientNameEmail.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Log.d("GetBalance", String.valueOf(position));
                String fullDescp = parent.getAdapter().getItem(position).toString();

                clientId = clientIds.get(position);
                clientNameEmail.setText(fullDescp.substring(0, fullDescp.indexOf(" -")));
                clientMail = fullDescp.substring(fullDescp.indexOf(" - ") + 3);
                clientNipEditText.requestFocus();
            }
        });
    }

    private void setGottenClientInfo(JSONObject response)
    {
        try
        {
            //Obtener datos de cliente
            JSONObject client = response.getJSONArray("data").getJSONObject(0);
            clientNameEmail.setText(client.getString("nombre"));
            clientMail = client.getString("email");
            clientId = client.getString("id");

            //Seleccionar el edit text para ingresar el NIP del cliente
            clientNipEditText.requestFocus();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private TextWatcher setClientEtTextWatcher()
    {
        return new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                chipRedManager.searchClient(ChipRedManager.BYNAME_EMAIL, s.toString());
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        };
    }

    private boolean validateData()
    {
        boolean val1 = clientNameEmail.validateWith(new METValidator("Nombre de cliente inválido")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                int size = text.length();
                if (size <= 3 || isEmpty)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        });

        boolean val2 = clientNipEditText.validateWith(new METValidator("NIP inválido")
        {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
            {
                if (isEmpty)
                {
                    return false;
                }
                else
                {
                    clientNip = text.toString();
                    return true;
                }
            }
        });

        return val1 && val2;
    }

    public void setThinkingLayout(String message)
    {
        hideKeyboard();

        thinkingLayout.setVisibility(View.VISIBLE);
        clientDataLayout.setVisibility(View.GONE);
        balanceLayout.setVisibility(View.GONE);

        if (message != null)
        {
            TextView messageTv = findViewById(R.id.get_balance_think_text);
            messageTv.setText(message);
        }
    }

    public void setErrorLayout(String message)
    {
        clientDataLayout.setVisibility(View.VISIBLE);
        thinkingLayout.setVisibility(View.GONE);
        balanceLayout.setVisibility(View.GONE);

        errorMessage.setVisibility(View.VISIBLE);
        errorMessage.setText(message);
    }

    public void hideKeyboard()
    {
        try
        {
            View kb = this.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) (this.getSystemService(Context
                    .INPUT_METHOD_SERVICE));
            if (imm != null && kb != null)
            {
                imm.hideSoftInputFromWindow(kb.getWindowToken(), 0);
            }
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
    }

    public void setBalanceViews(JSONObject response)
    {
        clientDataLayout.setVisibility(View.GONE);
        thinkingLayout.setVisibility(View.GONE);
        balanceLayout.setVisibility(View.VISIBLE);

        clientEmailEt.setText(clientMail);

        if (response != null)
        {
            JSONArray balanceData;
            try
            {
                clientDataToPrint = response.getJSONObject("data").getJSONArray("cliente")
                        .getJSONObject(0);

                balanceData = clientDataToPrint.getJSONArray("cuentas");

                for (int i = 0; i < 3; i++)
                {
                    JSONObject account = balanceData.getJSONObject(i);
                    double balanceValue = account.getDouble("saldo");
                    String balance;
                    String description = account.getString("descripcion");

                    balanceItemsViews[i].setText(description);
                    if (country.equals("costa rica"))
                    {
                        balance = ChipREDConstants.CR_AMOUNT_FORMAT.format(balanceValue);
                        balance += " CRC";
                    }
                    else
                    {
                        balance = ChipREDConstants.MX_AMOUNT_FORMAT.format(balanceValue);
                        balance += " MXN";
                    }
                    balanceViews[i].setText(balance);

                    if (balanceValue > 0)
                    {
                        balanceViews[i].setTextColor(getResources().getColor(R.color.green_money));
                    }
                    else if (balanceValue == 0)
                    {
                        balanceViews[i].setTextColor(getResources().getColor(R.color.colorAccent));
                    }
                    else if (balanceValue < 0)
                    {
                        balanceViews[i].setTextColor(getResources().getColor(R.color.red));
                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }
}
