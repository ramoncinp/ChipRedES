package com.binarium.chipredes.configstation;

import android.content.Context;

import androidx.annotation.Nullable;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.LoadingPosition;
import com.binarium.chipredes.Product;
import com.binarium.chipredes.Pumper;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.dispenser.Dispenser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Descripcioón
 * <p>
 * Administrar los siguientes datos de la estación directamente con el servidor local:
 * - Datos Generales
 * - Despachadores
 * - Productos
 * - Dispensarios
 * - Fidelización
 */

public class StationManager
{
    //Constantes
    private static final String TAG = StationManager.class.getSimpleName();
    private static final int TIMEOUT = 30 * 1000; //30 segundos de timeout

    //WebServices para obtener datos
    public static final int GET_GENERAL_DATA = 0;
    static final int SET_GENERAL_DATA = 1;
    static final int MODIFY_GENERAL_DATA = 2;
    static final int GET_PUMPERS = 3;
    static final int ADD_PUMPER = 4;
    static final int DELETE_PUMPER = 5;
    static final int MODIFY_PUMPER = 6;
    public static final int GET_PRODUCTS = 7;
    static final int ADD_PRODUCT = 8;
    static final int MODIFY_PRODUCT = 9;
    static final int DELETE_PRODUCT = 10;
    static final int GET_DISPENSERS = 11;
    static final int ADD_DISPENSERS = 12;
    static final int DELETE_DISPENSER = 13;
    static final int GET_EMAX_PRODUCTS = 14;
    static final int REGISTER_STATION_AS_USER = 15;
    static final int SET_TC_CONFIG = 16;
    public static final int STATION_LOGIN = 17;
    public static final int SYNC_LOCAL_SERVER = 18;
    static final int MODIFY_DISPENSER = 19;

    //Objetos
    private Context context;
    private OnResponse onResponseListener;

    //Objetos de Request
    private RequestQueue queue;

    //URL general
    private String url;

    public StationManager(Context context, OnResponse onResponseListener)
    {
        this.context = context;
        this.onResponseListener = onResponseListener;

        //Armar URL
        url = SharedPreferencesManager.getString(context, ChipREDConstants.CR_WOLKE_IP);
        if (!url.isEmpty())
        {
            //Reempalzar el index "tablet" con "estacion"
            url = url.replace("webservice_app", "webservice");
            url += "estacion/";
        }
    }

    public void getGeneralData(String mongoStationId)
    {
        if (mongoStationId == null)
        {
            mongoStationId = SharedPreferencesManager.getString(context, ChipREDConstants
                    .STATION_MONGO_ID, "");

            if (mongoStationId.isEmpty())
            {
                onResponseListener.chipRedMessage("No existe un id de estación", GET_GENERAL_DATA);
                return;
            }
        }

        //Armar parametros para petición
        String params = "?key=obtener_datos_generales_estacion&_id=" + mongoStationId;

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, GET_GENERAL_DATA);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    GET_GENERAL_DATA);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    GET_GENERAL_DATA);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            GET_GENERAL_DATA);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al obtener datos de estación\nNo responde "
                        + "servidor", GET_GENERAL_DATA);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendGetRequest(url, params, responseListener, errorListener);
    }

    void setGeneralData(JSONObject mRequest)
    {
        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "registrar_datos_generales_estacion");
            request.put("datos_generales_estacion", mRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, SET_GENERAL_DATA);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    SET_GENERAL_DATA);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    SET_GENERAL_DATA);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            SET_GENERAL_DATA);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al registrar datos de estación\nNo " +
                        "responde " + "servidor", SET_GENERAL_DATA);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        Log.d("StationManager", request.toString());
        sendPostRequest(url, request, responseListener, errorListener);
    }

    void setDummyTcConfig(String mongoId)
    {
        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "registrar_fidelizacion_estacion");
            request.put("_id", mongoId);
            request.put("fidelizacion", ChipREDConstants.GET_DUMMY_JSON_TC_CONFIG());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, SET_TC_CONFIG);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    SET_TC_CONFIG);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    SET_TC_CONFIG);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            SET_TC_CONFIG);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al registrar datos de fidelizacion\nNo " +
                        "responde " + "servidor", SET_TC_CONFIG);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        Log.d("StationManager", request.toString());
        sendPostRequest(url, request, responseListener, errorListener);
    }

    public void stationLogin(String username, String password)
    {
        //Armar parametros para petición
        String params =
                "?key=obtener_datos_usuario_estacion&username=" + username + "&password=" + password;

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, STATION_LOGIN);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    STATION_LOGIN);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            STATION_LOGIN);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al obtener datos de estación\nNo responde "
                        + "servidor", STATION_LOGIN);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendGetRequest(url, params, responseListener, errorListener);
    }

    void registerStationAsUser(JSONObject mRequest, String mongoId)
    {
        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "registrar_usuario_estacion");
            request.put("datos_usuario_estacion", mRequest);
            request.put("_id", mongoId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, REGISTER_STATION_AS_USER);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    REGISTER_STATION_AS_USER);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    REGISTER_STATION_AS_USER);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            REGISTER_STATION_AS_USER);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al registrar estación como usuario.\nNo " +
                        "responde " + "servidor", REGISTER_STATION_AS_USER);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendPostRequest(url, request, responseListener, errorListener);
    }

    void getStationPumpers()
    {
        String mongoStationId = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_MONGO_ID);

        if (mongoStationId.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", GET_PUMPERS);
        }

        //Armar parametros para petición
        String params = "?key=obtener_despachadores_estacion&_id=" + mongoStationId;

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, GET_PUMPERS);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    GET_PUMPERS);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    GET_PUMPERS);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            GET_PUMPERS);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al obtener despachadores\nNo responde " +
                        "servidor", GET_PUMPERS);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendGetRequest(url, params, responseListener, errorListener);
    }

    public void getStationProducts()
    {
        String mongoStationId = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_MONGO_ID);

        if (mongoStationId.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", GET_PRODUCTS);
        }

        //Armar parametros para petición
        String params = "?key=obtener_productos_estacion&_id=" + mongoStationId;

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, GET_PRODUCTS);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    GET_PRODUCTS);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    GET_PRODUCTS);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            GET_PRODUCTS);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al obtener productos de estación\nNo " +
                        "responde " + "servidor", GET_PRODUCTS);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendGetRequest(url, params, responseListener, errorListener);
    }

    void getStationDispensers()
    {
        String mongoStationId = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_MONGO_ID);

        if (mongoStationId.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", GET_DISPENSERS);
        }

        //Armar parametros para petición
        String params = "?key=obtener_dispensarios_estacion&_id=" + mongoStationId;

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, GET_DISPENSERS);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    GET_DISPENSERS);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    GET_DISPENSERS);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            GET_DISPENSERS);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al obtener dispensarios de estación\nNo "
                        + "responde " + "servidor", GET_DISPENSERS);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendGetRequest(url, params, responseListener, errorListener);
    }

    public void addPumper(Pumper pumper)
    {
        String mongoStationId = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_MONGO_ID);

        if (mongoStationId.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", ADD_PUMPER);
        }

        //Armar cuerpo para petición
        JSONObject request = new JSONObject();
        try
        {
            //Definir key y id de estación
            request.put("key", "registrar_despachadores_estacion");
            request.put("_id", mongoStationId);

            //Crear objeto para almacenar despachadores
            JSONArray pumpersArray = new JSONArray();

            //Obtener cada despachador y hacerlo un objeto JSON
            JSONObject pumperObjetc = new JSONObject();
            pumperObjetc.put("iniciales", pumper.getNameInitials());
            pumperObjetc.put("tag", pumper.getTagId());
            //pumperObjetc.put("id_local", pumper.getLocalId());
            pumperObjetc.put("id_emax", pumper.getEmaxId());
            pumperObjetc.put("nombre", pumper.getName());
            pumperObjetc.put("nip", pumper.getNip());

            //Agregar objeto JSON al arreglo
            pumpersArray.put(pumperObjetc);

            //Agregar arreglo al cuerpo de la petición
            request.put("despachadores", pumpersArray);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, ADD_PUMPER);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    ADD_PUMPER);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    ADD_PUMPER);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información", ADD_PUMPER);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al agregar despachador\nNo responde " +
                        "servidor", ADD_PUMPER);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendPostRequest(url, request, responseListener, errorListener);
    }

    public void addProduct(Product product)
    {
        String mongoStationId = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_MONGO_ID);

        if (mongoStationId.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", ADD_PRODUCT);
        }

        //Armar cuerpo para petición
        JSONObject request = new JSONObject();
        try
        {
            //Definir key y id de estación
            request.put("key", "registrar_productos_estacion");
            request.put("_id", mongoStationId);

            //Crear objeto para almacenar despachadores
            JSONArray productsArray = new JSONArray();

            //Obtener cada despachador y hacerlo un objeto JSON
            JSONObject productObject = new JSONObject();
            productObject.put("id_producto", product.getId());
            productObject.put("descripcion", product.getDescription());
            productObject.put("precio_litro", product.getLiterPrice());
            productObject.put("color", product.getColor());

            //Agregar objeto JSON al arreglo
            productsArray.put(productObject);

            //Agregar arreglo al cuerpo de la petición
            request.put("productos", productsArray);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, ADD_PRODUCT);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    ADD_PRODUCT);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    ADD_PRODUCT);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            ADD_PRODUCT);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al agregar producto\nNo responde " +
                        "servidor", ADD_PRODUCT);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendPostRequest(url, request, responseListener, errorListener);
    }

    public void addDispensers(ArrayList<Dispenser> dispensersToAdd)
    {
        String mongoStationId = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_MONGO_ID);

        if (mongoStationId.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", ADD_DISPENSERS);
        }

        if (dispensersToAdd.isEmpty())
        {
            onResponseListener.chipRedMessage("No se agregó ningún dispensario", ADD_DISPENSERS);
            return;
        }

        //Armar cuerpo para petición
        JSONObject request = new JSONObject();
        try
        {
            //Definir key y id de estación
            request.put("key", "registrar_dispensarios_estacion");
            request.put("_id", mongoStationId);

            //Agregar arreglo al cuerpo de la petición
            request.put("dispensarios", dispensersListToJsonArray(dispensersToAdd));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, ADD_DISPENSERS);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    ADD_DISPENSERS);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    ADD_DISPENSERS);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            ADD_DISPENSERS);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al agregar dispensarios\nNo responde " +
                        "servidor", ADD_DISPENSERS);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendPostRequest(url, request, responseListener, errorListener);
    }

    public static JSONArray loadingPositionsToDispensers(ArrayList<LoadingPosition> loadingPositions)
    {
        //Crear objeto para almacenar despachadores
        JSONArray dispensersArray = new JSONArray();

        //Evaluar si no esta vacía
        if (loadingPositions.isEmpty()) return dispensersArray;

        try
        {
            int pcIdx = 0;
            do
            {
                //Crear Objeto Dispensario
                JSONObject dispenserJson = new JSONObject();

                //Obtener número de dispensario
                //Numero de dispensario = (pcLado1 + 1)/2
                int dispN = (loadingPositions.get(pcIdx).getStationSide() + 1) / 2;
                //Asignarlo al objeto actual
                dispenserJson.put("numero_dispensario", dispN);

                //Obtener número de serie
                String serialNumber = String.format(Locale.getDefault(), "dis%06d", dispN)
                        .replace(' ', '0');
                //Asignarlo al objeto actual
                dispenserJson.put("numero_serie", serialNumber);

                //Crear arreglo de lados
                JSONArray sides = new JSONArray();

                for (int i = 0; i < 2; i++)
                {
                    //Obtener posicion de carga actual
                    LoadingPosition loadingPosition = loadingPositions.get(pcIdx);

                    //Obtener lo de la posicion de carga actual
                    JSONObject side = new JSONObject();
                    side.put("lado_tag", loadingPosition.getSideTag());
                    side.put("lado", loadingPosition.getSide());
                    side.put("lado_estacion", loadingPosition.getStationSide());
                    side.put("estado", loadingPosition.getState());
                    side.put("estado_r", loadingPosition.getRealState());
                    side.put("cm_estado", loadingPosition.getCmState());
                    side.put("cm_ip", loadingPosition.getCmIp());
                    side.put("cm_puerto", loadingPosition.getCmPort());

                    //Agregar al arreglo
                    sides.put(side);

                    //Incrementar el indice de posiciones de carga
                    pcIdx++;
                }

                //Agregar posiciones de carga al dispensario
                dispenserJson.put("lados", sides);

                //Agregar dispensario al arreglo
                dispensersArray.put(dispenserJson);
            }
            while (pcIdx < loadingPositions.size());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return dispensersArray;
    }

    public static JSONArray dispensersListToJsonArray(ArrayList<Dispenser> dispensers)
    {
        //Crear objeto para almacenar despachadores
        JSONArray dispensersArray = new JSONArray();

        //Evaluar si no esta vacía
        if (dispensers.isEmpty()) return dispensersArray;

        //Crear índice de dispensario
        int dispenserIdx = 0;
        do
        {
            //Obtener dispensario
            Dispenser dispenser = dispensers.get(dispenserIdx);

            //Crear Objeto Dispensario
            JSONObject dispenserJson = dispenserToJson(dispenser);

            //Agregar dispensario al arreglo
            dispensersArray.put(dispenserJson);

            //Incrementar el indice de dispensarios
            dispenserIdx++;
        }
        while (dispenserIdx < dispensers.size());

        return dispensersArray;
    }

    private static JSONObject dispenserToJson(Dispenser dispenser)
    {
        JSONObject dispenserJson = new JSONObject();

        try
        {
            dispenserJson.put("numero_dispensario", dispenser.getNumber());
            dispenserJson.put("numero_serie", dispenser.getSerialNumber());

            JSONArray sides = new JSONArray();
            sides.put(loadingPositionToJson(dispenser.getSide1()));
            sides.put(loadingPositionToJson(dispenser.getSide2()));

            dispenserJson.put("lados", sides);
        }
        catch (JSONException e)
        {
            Log.e(TAG, "Error al convertir objeto Dispenser a JSON");
        }

        return dispenserJson;
    }

    private static JSONObject loadingPositionToJson(LoadingPosition loadingPosition)
    {
        JSONObject side = new JSONObject();

        try
        {
            //Obtener lo de la posicion de carga actual
            side.put("lado_tag", loadingPosition.getSideTag());
            side.put("lado", loadingPosition.getSide());
            side.put("lado_estacion", loadingPosition.getStationSide());
            side.put("lado_estacion_usuario", loadingPosition.getUserStationSide());
            side.put("estado", loadingPosition.getState());
            side.put("estado_r", loadingPosition.getRealState());
            side.put("cm_estado", loadingPosition.getCmState());
            side.put("cm_ip", loadingPosition.getCmIp());
            side.put("cm_puerto", loadingPosition.getCmPort());
        }
        catch (JSONException e)
        {
            Log.e(TAG, "Error al convertir objeto LoadingPosition a JSON");
        }

        return side;
    }

    public void deletePumper(Pumper pumper)
    {
        String mongoStationId = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_MONGO_ID);

        if (mongoStationId.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", DELETE_PUMPER);
        }

        //Armar parametros para petición
        String params = "?key=eliminar_despachador_estacion&_id=" + mongoStationId + "&id_emax="
                + pumper.getEmaxId();

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, DELETE_PUMPER);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    DELETE_PUMPER);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    DELETE_PUMPER);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            DELETE_PUMPER);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al eliminar despachador de estación\nNo "
                        + "responde " + "servidor", DELETE_PUMPER);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendDeleteRequest(url, params, responseListener, errorListener);
    }

    public void deleteProduct(Product product)
    {
        String mongoStationId = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_MONGO_ID);

        if (mongoStationId.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", DELETE_PRODUCT);
        }

        //Armar parametros para petición
        String params = "?key=eliminar_producto_estacion&_id=" + mongoStationId + "&id_producto="
                + product.getId();

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, DELETE_PRODUCT);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    DELETE_PRODUCT);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    DELETE_PRODUCT);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            DELETE_PUMPER);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al eliminar producto de estación\nNo " +
                        "responde " + "servidor", DELETE_PRODUCT);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendDeleteRequest(url, params, responseListener, errorListener);
    }

    public void deleteDispenser(int number)
    {
        String mongoStationId = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_MONGO_ID);

        if (mongoStationId.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", DELETE_DISPENSER);
        }

        //Armar parametros para petición
        String params = "?key=eliminar_dispensario_estacion&_id=" + mongoStationId +
                "&numero_dispensario=" + number;

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, DELETE_DISPENSER);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    DELETE_DISPENSER);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    DELETE_DISPENSER);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            DELETE_DISPENSER);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al eliminar producto de estación\nNo " +
                        "responde " + "servidor", DELETE_DISPENSER);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendDeleteRequest(url, params, responseListener, errorListener);
    }

    public void modifyGeneralData(JSONObject generalData)
    {
        String stationID = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_ID, "");

        if (stationID.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", MODIFY_GENERAL_DATA);
        }

        //Armar cuerpo para petición
        JSONObject request = new JSONObject();
        try
        {
            //Definir key y id de estación
            request.put("key", "actualizar_datos_generales_estacion");
            request.put("id_estacion", stationID);

            //Obtener datos modificables
            request.put("nombre_estacion", generalData.getString("nombre_estacion"));
            request.put("rfc_cedula", generalData.getString("rfc_cedula"));
            request.put("email", generalData.getString("email"));
            request.put("codigo", generalData.getJSONObject("telefono").getString("codigo"));
            request.put("numero", generalData.getJSONObject("telefono").getString("numero"));
            if (generalData.has("codigo_actividad"))
                request.put("codigo_actividad", generalData.getString("codigo_actividad"));
            if (generalData.has("margen_distribucion"))
                request.put("margen_distribucion", generalData.getString("margen_distribucion"));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, MODIFY_GENERAL_DATA);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    MODIFY_GENERAL_DATA);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    MODIFY_GENERAL_DATA);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            MODIFY_GENERAL_DATA);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al modificar información general\nNo " +
                        "responde " +
                        "servidor", MODIFY_GENERAL_DATA);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendPutRequest(url, request, responseListener, errorListener);
    }

    public void modifyProduct(Product product)
    {
        String stationID = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_ID, "");

        if (stationID.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", MODIFY_PRODUCT);
        }

        //Armar cuerpo para petición
        JSONObject request = new JSONObject();
        try
        {
            //Definir key y id de estación
            request.put("key", "actualizar_producto_estacion");
            request.put("id_estacion", stationID);
            request.put("id_producto", product.getId());
            request.put("descripcion", product.getDescription());
            request.put("precio_litro", product.getLiterPrice());
            request.put("color", product.getColor());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, MODIFY_PRODUCT);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    MODIFY_PRODUCT);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    MODIFY_PRODUCT);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            MODIFY_PRODUCT);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al modificar producto\nNo responde " +
                        "servidor", MODIFY_PRODUCT);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendPutRequest(url, request, responseListener, errorListener);
    }

    public void modifyPumper(Pumper pumper)
    {
        String stationID = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_ID, "");

        if (stationID.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", MODIFY_PUMPER);
        }

        //Armar cuerpo para petición
        JSONObject request = new JSONObject();
        try
        {
            //Definir key y id de estación
            request.put("key", "actualizar_despachador_estacion");
            request.put("id_estacion", stationID);
            request.put("id_emax", pumper.getEmaxId());
            request.put("nombre", pumper.getName());
            request.put("tag", pumper.getTagId());
            request.put("nip", pumper.getNip());
            request.put("iniciales", pumper.getNameInitials());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, MODIFY_PUMPER);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    MODIFY_PUMPER);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    MODIFY_PUMPER);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            MODIFY_PUMPER);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al modificar información general\nNo " +
                        "responde " +
                        "servidor", MODIFY_PUMPER);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendPutRequest(url, request, responseListener, errorListener);
    }

    public void modifyDispenser(Dispenser dispenser)
    {
        String stationID = SharedPreferencesManager.getString(context, ChipREDConstants
                .STATION_ID, "");

        if (stationID.isEmpty())
        {
            onResponseListener.chipRedError("No existe un id de estación", MODIFY_DISPENSER);
        }

        //Armar cuerpo para petición
        JSONObject request = new JSONObject();
        try
        {
            //Definir key y id de estación
            request.put("key", "actualizar_dispensario_estacion");
            request.put("id_estacion", stationID);
            request.put("dispensario", dispenserToJson(dispenser));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, MODIFY_DISPENSER);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    MODIFY_DISPENSER);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    MODIFY_DISPENSER);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            MODIFY_DISPENSER);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al modificar dispensario", MODIFY_DISPENSER);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendPutRequest(url, request, responseListener, errorListener);
    }

    public void syncLocalServer()
    {
        //Evaluar si se debe sincronizar servidor local
        if (ChipREDConstants.getTokencashScenario(context).equals("e3"))
        {
            // No se debe sincornizar servidor local, no hay para E3 (escenario 3)
            Log.v(TAG, "No hay servidor local para E3");
            return;
        }

        //Obtener id de mongo
        String mongoId =
                SharedPreferencesManager.getString(context, ChipREDConstants.STATION_MONGO_ID, "");
        if (mongoId.isEmpty())
        {
            onResponseListener.chipRedMessage("Error al sincronizar servidor local",
                    SYNC_LOCAL_SERVER);
            return;
        }

        //Obtener objeto JSON del archivo local
        JSONObject mStation = StationFileManager.getStation(context);
        if (mStation == null)
        {
            onResponseListener.chipRedMessage("Error al sincronizar servidor local",
                    SYNC_LOCAL_SERVER);
            return;
        }

        JSONObject request = new JSONObject();
        try
        {
            request.put("key", "registrar_datos_generales_estacion");
            request.put("datos_generales_estacion", mStation);
            request.put("_id_estacion_wolke", mongoId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    Log.d(TAG, response.toString(2));
                    switch (response.getString("response"))
                    {
                        case "Ok":
                            onResponseListener.showResponse(response, SYNC_LOCAL_SERVER);
                            break;

                        case "Aviso":
                            onResponseListener.chipRedMessage(response.getString("message"),
                                    SYNC_LOCAL_SERVER);
                            break;

                        default:
                            onResponseListener.chipRedError(response.getString("message"),
                                    SYNC_LOCAL_SERVER);
                            break;
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    onResponseListener.chipRedError("Error al procesar la información",
                            SYNC_LOCAL_SERVER);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, error.toString());
                onResponseListener.chipRedError("Error al sincronizar servidor local\nNo " +
                        "responde " + "servidor", SYNC_LOCAL_SERVER);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        //Obtener URL de servidor local
        String localServerURL =
                SharedPreferencesManager.getString(context, ChipREDConstants.CR_LOCAL_IP);
        if (localServerURL.isEmpty())
        {
            onResponseListener.chipRedMessage("Error al sincronizar servidor local",
                    SYNC_LOCAL_SERVER);
            return;
        }
        else if (localServerURL.contains("0.0.0.0"))
        {
            Log.v(TAG, "Servidor local desactivado");
            return;
        }

        //Adecuar url
        localServerURL = localServerURL.replace("tablet", "estacion");

        //Enviar petición
        sendPostRequest(localServerURL, request, responseListener, errorListener);
    }

    public void getEmaxProducts(int loadingPosition)
    {
        String mBody = String.format(Locale.getDefault(), "<?xml version=\"1.0\" " +
                "encoding=\"UTF-8\"?>\n" +
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www" +
                ".w3.org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas" +
                ".xmlsoap.org/soap/encoding/\">\n" +
                "   <SOAP-ENV:Body>\n" +
                "      <ns8031:getProdPC xmlns:ns8031=\"http://tempuri.org\">\n" +
                "         <getProdPC>\n" +
                "            <poscar xsi:type=\"xsd:int\">%d</poscar>\n" +
                "         </getProdPC>\n" +
                "      </ns8031:getProdPC>\n" +
                "   </SOAP-ENV:Body>\n" +
                "</SOAP-ENV:Envelope>", loadingPosition);

        Response.Listener<String> responseListener = new
                Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        onResponseListener.chipRedMessage(response, GET_EMAX_PRODUCTS);
                    }
                };

        Response.ErrorListener errorListener = new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                onResponseListener.chipRedMessage("error!", GET_EMAX_PRODUCTS);
            }
        };

        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }

        sendSOAPRequest(mBody, responseListener, errorListener);
    }

    private void sendSOAPRequest(final String body, Response.Listener<String>
            responseListener, Response.ErrorListener errorListener)
    {
        String emaxUrl = "http://192.168.0.136/emax/services/simecob.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, emaxUrl,
                responseListener, errorListener)
        {
            @Override
            public String getBodyContentType()
            {
                return "text/xml; charset=utf-8";
            }

            @Override
            public byte[] getBody()
            {
                try
                {
                    return body.getBytes("utf-8");
                }
                catch (UnsupportedEncodingException uee)
                {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using" +
                            " %s", body, "utf-8");
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT, 0, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }

    private void sendGetRequest(String url, @Nullable String params, Response
            .Listener<JSONObject> responseListener, Response.ErrorListener errorListener)
    {
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url + params, null,
                responseListener, errorListener)
        {
            @Override
            public Map<String, String> getHeaders()
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT, 0, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    private void sendPostRequest(String url, JSONObject request, Response.Listener<JSONObject>
            responseListener, Response.ErrorListener errorListener)
    {
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, request,
                responseListener, errorListener);

        req.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT, 0, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    private void sendPutRequest(String url, JSONObject request, Response.Listener<JSONObject>
            responseListener, Response.ErrorListener errorListener)
    {
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT,
                url,
                request, responseListener, errorListener);

        req.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT, 0, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    private void sendDeleteRequest(String url, @Nullable String params, Response
            .Listener<JSONObject> responseListener, Response.ErrorListener errorListener)
    {
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE, url + params, null,
                responseListener, errorListener)
        {
            @Override
            public Map<String, String> getHeaders()
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT, 0, DefaultRetryPolicy
                .DEFAULT_BACKOFF_MULT));

        queue.add(req);
    }

    public void cancellAllRequest()
    {
        if (queue != null)
        {
            queue.cancelAll(new RequestQueue.RequestFilter()
            {
                @Override
                public boolean apply(Request<?> request)
                {
                    return true;
                }
            });
        }
    }

    public interface OnResponse
    {
        void chipRedMessage(String crMessage, int webServiceN);

        void chipRedError(String errorMessage, int webServiceN);

        void showResponse(JSONObject response, int webServiceN);
    }
}
