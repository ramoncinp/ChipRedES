package com.binarium.chipredes.configstation.service

import com.binarium.chipredes.wolke.models.StationGeneralDataResponse
import com.binarium.chipredes.wolke.models.StationsPumpersResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface StationManagerService {

    @Headers("Content-Type: application/json")
    @GET("webservice/estacion/")
    suspend fun getStationPumpers(
        @Query("key") key: String = "obtener_despachadores_estacion",
        @Query("_id") stationId: String
    ): StationsPumpersResponse

    @Headers("Content-Type: application/json")
    @GET("webservice/estacion/")
    suspend fun getStationGeneralData(
        @Query("key") key: String = "obtener_datos_generales_estacion",
        @Query("_id") stationId: String
    ): StationGeneralDataResponse
}
