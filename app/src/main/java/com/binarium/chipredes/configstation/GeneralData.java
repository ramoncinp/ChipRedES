package com.binarium.chipredes.configstation;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.AddressElementCR;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.Country;
import com.binarium.chipredes.CountryArrayAdapter;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.adapters.SimpleImageTextRow;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import timber.log.Timber;

public class GeneralData extends Fragment implements StationManager.OnResponse {
    // Constants
    public static final String TAG = GeneralData.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST_FINE_LOCATION = 111;

    //Variables
    private boolean registerOrModify = true; //True para registrar, false para modificar
    private int currentFocusedView;

    //Objetos
    private Country selectedCountry;
    private ChipRedManager chipRedManager; //Para direcciones
    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private JSONObject generalData;
    private StationManager stationManager;

    private double latitude;
    private double longitude;

    //Elementos de direccion para Costa Rica
    private AddressElementCR provincia;
    private AddressElementCR canton;
    private AddressElementCR distrito;
    private AddressElementCR barrio;

    //Views
    private Button submitButton;
    private CardView getLocationCv;
    private Dialog crAddressesDialog;
    private ImageView locationImageView;
    private MaterialEditText stationNameEt;
    private MaterialEditText stationIdEt;
    private MaterialEditText stationEmailEt;
    private MaterialEditText stationPhoneEt;
    private MaterialEditText stationTaxCodeEt;
    private MaterialEditText latitudeEt;
    private MaterialEditText longitudeEt;
    private MaterialBetterSpinner countrySelector;
    private ProgressBar locationProgressBar;
    private ProgressBar progressBar;
    private ScrollView contentScrollView;

    //Interfaces
    private GeneralDataInterface generalDataInterface;

    public GeneralData() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        // Inflate the layout for this fragment
        View content = inflater.inflate(R.layout.fragment_general_data, container, false);

        contentScrollView = content.findViewById(R.id.general_data_content);
        progressBar = content.findViewById(R.id.progress_bar);
        countrySelector = content.findViewById(R.id.country_selector);

        stationNameEt = content.findViewById(R.id.station_name_et);
        stationIdEt = content.findViewById(R.id.station_id);
        stationEmailEt = content.findViewById(R.id.station_email);
        stationPhoneEt = content.findViewById(R.id.station_phone);
        stationTaxCodeEt = content.findViewById(R.id.station_tax_code);
        getLocationCv = content.findViewById(R.id.get_location_button);
        locationProgressBar = content.findViewById(R.id.asking_location_progress);
        locationImageView = content.findViewById(R.id.location_image_view);
        latitudeEt = content.findViewById(R.id.station_latitude);
        longitudeEt = content.findViewById(R.id.station_longitude);
        submitButton = content.findViewById(R.id.submit_button);
        Button cancelButton = content.findViewById(R.id.start_button);
        setUnEditable(latitudeEt);
        setUnEditable(longitudeEt);

        //Listener para botón de localización
        getLocationCv.setOnClickListener(view -> getStationLocation());

        //Listener para botones
        cancelButton.setOnClickListener(view -> generalDataInterface.returnToPreviousFragment());

        submitButton.setOnClickListener(view -> {
            ChipREDConstants.hideKeyboard(getActivity());
            getAndValidateData();
        });

        return content;
    }

    @Override
    public void chipRedMessage(String crMessage, int webServiceN) {
        if (ChipREDConstants.checkNullParentActivity(getActivity(), GeneralData.this)) return;

        progressBar.setVisibility(View.GONE);
        contentScrollView.setVisibility(View.VISIBLE);

        Toast.makeText(getActivity(), crMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void chipRedError(String errorMessage, int webServiceN) {
        if (ChipREDConstants.checkNullParentActivity(getActivity(), GeneralData.this)) return;

        GenericDialog genericDialog = new GenericDialog("Error",
                errorMessage, () -> {
            if (!ChipREDConstants.checkNullParentActivity(getActivity(), GeneralData.this)) {
                requireActivity().finish();
            }
        }, null, getActivity());

        genericDialog.setPositiveText("Regresar");
        genericDialog.show();
    }

    @Override
    public void showResponse(JSONObject response, int webServiceN) {
        if (ChipREDConstants.checkNullParentActivity(getActivity(), GeneralData.this)) return;

        try {
            switch (webServiceN) {
                case StationManager.GET_GENERAL_DATA:
                    Timber.d(response.toString(2));
                    //Obtener datos de estación
                    parseGeneralData(response);
                    break;

                case StationManager.SET_GENERAL_DATA:
                    //Almacenar ID de estación
                    SharedPreferencesManager.putString(getContext(), ChipREDConstants.STATION_ID,
                            generalData.getString("id_estacion"));

                    //Obtener ID de mongo
                    String mongoId = response.getJSONObject("data").getString("_id");

                    //Almacenar ID de Mongo
                    SharedPreferencesManager.putString(getContext(),
                            ChipREDConstants.STATION_MONGO_ID,
                            mongoId);

                    ChipREDConstants.showSnackBarMessage(
                            response.getString("message"),
                            requireActivity()
                    );

                    //Agregar objeto de fidelización con datos default
                    stationManager.setDummyTcConfig(mongoId);

                    //Agregar objeto de fidelización a archivo local .json
                    try {
                        generalData.put("fidelizacion",
                                ChipREDConstants.GET_DUMMY_JSON_TC_CONFIG());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //Guardar datos generales en archivo json de estación
                    StationFileManager.saveStationGeneralData(generalData, requireActivity());
                    //Sincronizar servidor local
                    stationManager.syncLocalServer();

                    //Registrar estación como usuario
                    JSONObject user = new JSONObject();

                    user.put("username", generalData.getString("email"));
                    user.put("id_estacion", generalData.getString("id_estacion"));
                    user.put("tipo_usuario", "liquidacion_express");
                    user.put("pais", generalData.getJSONObject("direccion").getString("pais"));
                    user.put("password", generalData.getString("rfc_cedula"));

                    //Registrar estación como usuario
                    stationManager.registerStationAsUser(user, mongoId);

                    break;

                case StationManager.MODIFY_GENERAL_DATA:
                    ChipREDConstants.showSnackBarMessage(response.getString("message"),
                            requireActivity());

                    //Guardar datos generales en archivo json de estación
                    StationFileManager.syncGeneralData(generalData, getActivity());
                    //Sincronizar servidor local
                    stationManager.syncLocalServer();

                    //Terminar el fragment
                    generalDataInterface.returnToPreviousFragment();

                    break;

                case StationManager.REGISTER_STATION_AS_USER:
                    Toast.makeText(getActivity(), response.getString("message"),
                            Toast.LENGTH_SHORT).show();

                    //Terminar el fragment
                    generalDataInterface.returnToPreviousFragment();

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        contentScrollView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Inicializar Spinner
        setCountrySpinner();
        //Inicializar API de Places
        initPlacesApi();
        //Inicializar StationManager
        initStationManager();
        //Inicializar ChipRedManager
        initChipRedManager();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST_FINE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getStationLocation();
            } else {
                Toast.makeText(getActivity(), "No hay permiso para obtener localización",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setGeneralDataInterface(GeneralDataInterface generalDataInterface) {
        this.generalDataInterface = generalDataInterface;
    }

    private void setCountrySpinner() {
        //Enlistar países
        ArrayList<Country> countries = new ArrayList<>();
        countries.add(new Country("México", R.drawable.ic_mexico, ChipREDConstants.COUNTRIES[0]));
        countries.add(new Country("Costa Rica", R.drawable.ic_costa_rica, ChipREDConstants
                .COUNTRIES[1]));

        //Definir su adaptador
        CountryArrayAdapter countryArrayAdapter = new CountryArrayAdapter(getActivity(), countries);
        countrySelector.setAdapter(countryArrayAdapter);
        countrySelector.setOnItemClickListener((parent, view, position, id) -> {
            Country country = (Country) parent.getItemAtPosition(position);
            onSelectedCountry(country);
        });
    }

    private void selectStationCountry(String stationCountry) {
        //Definir un país por default
        Country stationCountryObject;
        int countryIdx = -1;
        if (stationCountry.equals("mexico")) {
            countryIdx = 0;
        } else if (stationCountry.equals("costa rica")) {
            countryIdx = 1;
        }

        //Si existe un país valido, definirlo
        if (countryIdx != -1) {
            countrySelector.setListSelection(countryIdx);
            stationCountryObject = (Country) countrySelector.getAdapter().getItem(countryIdx);

            onSelectedCountry(stationCountryObject);
        }
    }

    private void onSelectedCountry(Country country) {
        countrySelector.setText(country.getName());

        //Asignar a variable global
        selectedCountry = country;

        //Mostrar layout correspondiente
        setForm(country);
        countrySelector.clearFocus();
    }

    private void setForm(Country country) {
        //Definir imagen del pais
        ImageView countryImage = requireActivity().findViewById(R.id.country_image);
        countryImage.setImageResource(country.getImageResource());

        //Obtener el layout del formulario
        LinearLayout linearLayout = requireActivity().findViewById(R.id.station_info_form_layout);

        //Analizar si ya existe un view
        if (linearLayout.getChildCount() != 0) {
            linearLayout.removeAllViews();
        }

        //Obtener el view de la forma correspondiente
        View addressForm;
        if (country.getIdentifier().contains("mexico")) {
            //Editar campos para clave fiscal
            stationTaxCodeEt.setHint("RFC");
            stationTaxCodeEt.setFloatingLabelText("RFC");

            addressForm = getLayoutInflater().inflate(R.layout.client_form_mexico_address, null);
            linearLayout.addView(addressForm);
        } else {
            //Editar campos para clave fiscal
            stationTaxCodeEt.setHint("Cédula");
            stationTaxCodeEt.setFloatingLabelText("Cédula");

            addressForm = getLayoutInflater().inflate(
                    R.layout.client_form_costa_rica_address,
                    null
            );
            linearLayout.addView(addressForm);
            defineAddressListeners();
        }
    }

    private void initStationManager() {
        stationManager = new StationManager(getActivity(), this);
        stationManager.getGeneralData(null);
    }

    private void initChipRedManager() {
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                //
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {

            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                showFoundAddressElements(response);
            }
        }, getActivity());
    }

    private void getAndValidateData() {
        //Validar los datos
        boolean isValid;

        METValidator emptyValidator = new METValidator("Campo obligatorio") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                return !isEmpty;
            }
        };

        isValid = stationNameEt.validateWith(emptyValidator);
        isValid &= stationIdEt.validateWith(emptyValidator);
        isValid &= stationEmailEt.validateWith(emptyValidator);
        isValid &= stationTaxCodeEt.validateWith(emptyValidator);

        //Checar si fue valido
        if (isValid) {
            //Obtener toda la información
            //Crear objeto JSON
            JSONObject request = new JSONObject();
            try {
                request.put("nombre_estacion", stationNameEt.getText().toString());
                request.put("id_estacion", stationIdEt.getText().toString());
                request.put("rfc_cedula", stationTaxCodeEt.getText().toString());
                request.put("email", stationEmailEt.getText().toString().trim());

                // Crear objeto JSON para gps
                JSONObject gps = new JSONObject();
                String latitudeVal = "";
                String longitudeVal = "";

                // Validar editTexts
                if (latitudeEt.getText().length() > 0)
                    latitudeVal = latitudeEt.getText().toString();
                if (longitudeEt.getText().length() > 0)
                    longitudeVal = longitudeEt.getText().toString();

                // Agregar valores de gps
                gps.put("latitud", latitudeVal);
                gps.put("longitud", longitudeVal);


                request.put("gps", gps);
                request = getAndValidatePhoneAndAddress(request);

                // Salir de la función si no fueron válidos los datos de dirección
                if (request == null) {
                    return;
                }

                //Guardar datos en variable global para almacenarla en archivo
                generalData = request;

                if (registerOrModify) {
                    //Enviar request al servidor
                    stationManager.setGeneralData(request);
                } else {
                    //Enviar request para modificar información
                    stationManager.modifyGeneralData(request);
                }

                //Mostrar views de progreso
                contentScrollView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                Timber.e(e);
                Toast.makeText(
                        getActivity(),
                        "Error al procesar la información", Toast
                                .LENGTH_SHORT
                ).show();
            }
        }
    }

    private JSONObject getAndValidatePhoneAndAddress(JSONObject request) {
        //Crear objeto JSON para almacenar la información
        JSONObject address = new JSONObject();

        //Definir validador
        METValidator emptyValidator = new METValidator("Campo obligatorio") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                return !isEmpty;
            }
        };

        if (selectedCountry.getIdentifier().equals("mexico")) {
            //Obtener datos ingresados
            MaterialEditText calle = getActivity().findViewById(R.id.mexico_addr_calle);
            MaterialEditText numeroExterior = getActivity().findViewById(R.id
                    .mexico_addr_numero_exterior);
            MaterialEditText numeroInterior = getActivity().findViewById(R.id
                    .mexico_addr_numero_interior);
            MaterialEditText colonia = getActivity().findViewById(R.id.mexico_addr_colonia);
            MaterialEditText municipio = getActivity().findViewById(R.id.mexico_addr_municipio);
            MaterialEditText estado = getActivity().findViewById(R.id.mexico_addr_estado);
            MaterialEditText codigoPostal = getActivity().findViewById(R.id
                    .mexico_addr_codigo_postal);

            boolean isValid;
            isValid = calle.validateWith(emptyValidator);
            isValid &= numeroExterior.validateWith(emptyValidator);
            isValid &= colonia.validateWith(emptyValidator);
            isValid &= municipio.validateWith(emptyValidator);
            isValid &= estado.validateWith(emptyValidator);
            isValid &= codigoPostal.validateWith(emptyValidator);

            if (!isValid) {
                return null;
            } else {
                try {
                    address.put("pais", "mexico");
                    address.put("calle", calle.getText().toString());
                    address.put("numero_ext", numeroExterior.getText().toString());
                    address.put("numero_int", numeroInterior.getText().toString());
                    address.put("colonia", colonia.getText().toString());
                    address.put("municipio", municipio.getText().toString());
                    address.put("estado", estado.getText().toString());
                    address.put("codigo_postal", codigoPostal.getText().toString());

                    JSONObject phone = new JSONObject();
                    phone.put("numero", stationPhoneEt.getText().toString());
                    phone.put("codigo", "52");

                    request.put("telefono", phone);
                    request.put("direccion", address);
                } catch (JSONException e) {
                    Timber.e(e);
                    Toast.makeText(getActivity(), "Error al procesar información",
                            Toast.LENGTH_SHORT).show();
                    return null;
                }
            }
        } else {
            //Obtener datos ingresados
            MaterialEditText activityCode = getActivity().findViewById(R.id.activity_code);
            MaterialEditText distributionMargin =
                    getActivity().findViewById(R.id.distribution_margin);
            MaterialBetterSpinner provincia = getActivity().findViewById(R.id
                    .costa_rica_addr_provincia);
            MaterialBetterSpinner canton = getActivity().findViewById(R.id.costa_rica_addr_canton);
            MaterialBetterSpinner distrito = getActivity().findViewById(R.id
                    .costa_rica_addr_distrito);
            MaterialBetterSpinner barrio = getActivity().findViewById(R.id.costa_rica_addr_barrio);
            MaterialEditText otrasSenias = getActivity().findViewById(R.id
                    .costa_rica_addr_otras_senias);

            boolean isValid;
            isValid = provincia.validateWith(emptyValidator);
            isValid &= canton.validateWith(emptyValidator);
            isValid &= distrito.validateWith(emptyValidator);
            isValid &= otrasSenias.validateWith(emptyValidator);
            isValid &= activityCode.validateWith(emptyValidator);

            if (!isValid) {
                //Si no es válido, retornar nulo
                return null;
            } else {
                try {
                    //Ajustar formatos para Canton, Distrito y Barrio
                    this.canton.setCodeFormat();
                    this.distrito.setCodeFormat();

                    address.put("pais", "costa rica");
                    address.put("provincia", this.provincia.getCode());
                    address.put("canton", this.canton.getCode());
                    address.put("distrito", this.distrito.getCode());
                    address.put("otras_senas", otrasSenias.getText().toString());

                    //Manejar datos opcionales
                    if (this.barrio != null) {
                        address.put("barrio", this.barrio.getCode());
                        this.barrio.setCodeFormat();
                    }

                    //Capturar teléfono
                    JSONObject phone = new JSONObject();
                    phone.put("numero", stationPhoneEt.getText().toString());
                    phone.put("codigo", "506");

                    request.put("telefono", phone);
                    request.put("direccion", address);
                    request.put("codigo_actividad", activityCode.getText().toString());

                    // Guardar margen de distribucion
                    String margenDistribucionVal = distributionMargin.getText().toString();
                    request.put("margen_distribucion",
                            margenDistribucionVal.isEmpty() ? "" :
                                    Double.parseDouble(margenDistribucionVal));
                } catch (JSONException e) {
                    //Devolver el objeto como ingresó al método
                    Timber.e(e);
                    Toast.makeText(getActivity(), "Error al procesar información",
                            Toast.LENGTH_SHORT).show();
                    return null;
                }
            }
        }

        //Regresar objeto JSON con la dirección ingresada
        return request;
    }

    private void parseGeneralData(JSONObject response) {
        try {
            //Obtener objeto principal
            JSONObject root = response.getJSONObject("data").getJSONObject("datos_generales");

            //Guardar datos generales en archivo json de estación
            StationFileManager.saveStationGeneralData(root, requireActivity());
            //Sincronizar servidor local
            stationManager.syncLocalServer();

            if (root.has("nombre_estacion"))
                stationNameEt.setText(root.getString("nombre_estacion"));
            if (root.has("id_estacion")) stationIdEt.setText(root.getString("id_estacion"));
            if (root.has("rfc_cedula")) stationTaxCodeEt.setText(root.getString("rfc_cedula"));
            if (root.has("email")) stationEmailEt.setText(root.getString("email"));
            if (root.has("telefono"))
                stationPhoneEt.setText(root.getJSONObject("telefono").getString("numero"));
            if (root.has("gps")) {
                String latitudeVal = root.getJSONObject("gps").getString("latitud");
                String longitudeVal = root.getJSONObject("gps").getString("longitud");
                if (!latitudeVal.isEmpty()) latitude = Double.parseDouble(latitudeVal);
                if (!longitudeVal.isEmpty()) longitude = Double.parseDouble(longitudeVal);
                setLatLng();
            }

            //Restringir la edición de ciertos campos
            stationIdEt.setEnabled(false);

            //Obtener dirección
            if (root.has("direccion")) {
                JSONObject address = root.getJSONObject("direccion");
                //Definir país
                selectStationCountry(address.getString("pais"));
                //Obtener datos de dirección
                setAddressForm(address);
            }

            // Obtener elementos específicos de Costa Rica
            if (selectedCountry.getIdentifier().equals("costa rica")) {
                //Obtener codigo de actividad
                if (root.has("codigo_actividad")) {
                    MaterialEditText activityCode = getActivity().findViewById(R.id.activity_code);
                    activityCode.setText(root.getString("codigo_actividad"));
                }

                //Obtener margen de distribución
                if (root.has("margen_distribucion")) {
                    MaterialEditText distributionMarginEt =
                            getActivity().findViewById(R.id.distribution_margin);
                    distributionMarginEt.setText(root.getString("margen_distribucion"));
                }
            }

            //Indicar que se puede modificar y no registrar
            registerOrModify = false;
            submitButton.setText("Guardar");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAddressForm(JSONObject address) {
        try {
            //Para este momento, el país ya fue obtenido
            if (selectedCountry.getIdentifier().equals("mexico")) {
                stationTaxCodeEt.setHint("RFC");
                stationTaxCodeEt.setFloatingLabelText("RFC");

                MaterialEditText calle = getActivity().findViewById(R.id.mexico_addr_calle);
                MaterialEditText numeroExterior = getActivity().findViewById(R.id
                        .mexico_addr_numero_exterior);
                MaterialEditText numeroInterior = getActivity().findViewById(R.id
                        .mexico_addr_numero_interior);
                MaterialEditText colonia = getActivity().findViewById(R.id.mexico_addr_colonia);
                MaterialEditText municipio = getActivity().findViewById(R.id.mexico_addr_municipio);
                MaterialEditText estado = getActivity().findViewById(R.id.mexico_addr_estado);
                MaterialEditText codigoPostal = getActivity().findViewById(R.id
                        .mexico_addr_codigo_postal);

                calle.setText(address.getString("calle"));
                calle.setEnabled(false);

                numeroExterior.setText(address.getString("numero_ext"));
                numeroExterior.setEnabled(false);

                numeroInterior.setText(address.getString("numero_int"));
                numeroInterior.setEnabled(false);

                colonia.setText(address.getString("colonia"));
                colonia.setEnabled(false);

                municipio.setText(address.getString("municipio"));
                municipio.setEnabled(false);

                estado.setText(address.getString("estado"));
                estado.setEnabled(false);

                codigoPostal.setText(address.getString("codigo_postal"));
                codigoPostal.setEnabled(false);
            } else if (selectedCountry.getIdentifier().equals("costa rica")) {
                //Editar campos para clave fiscal
                stationTaxCodeEt.setHint("Cédula");
                stationTaxCodeEt.setFloatingLabelText("Cédula");

                MaterialBetterSpinner provincia = getActivity().findViewById(R.id
                        .costa_rica_addr_provincia);
                MaterialBetterSpinner canton = getActivity().findViewById(R.id
                        .costa_rica_addr_canton);
                MaterialBetterSpinner distrito = getActivity().findViewById(R.id
                        .costa_rica_addr_distrito);
                MaterialBetterSpinner barrio = getActivity().findViewById(R.id
                        .costa_rica_addr_barrio);
                MaterialEditText otrasSenias = getActivity().findViewById(R.id
                        .costa_rica_addr_otras_senias);
                provincia.setEnabled(false);
                canton.setEnabled(false);
                distrito.setEnabled(false);
                barrio.setEnabled(false);
                otrasSenias.setEnabled(false);

                String provinciaString = "";
                String cantonString = "";
                String distritoString = "";
                String barrioString = "";
                String otrasSenasString = "";

                if (address.has("provincia")) provinciaString = address.getString("provincia");
                if (address.has("canton")) cantonString = address.getString("canton");
                if (address.has("distrito")) distritoString = address.getString("distrito");
                if (address.has("otras_senas")) otrasSenasString = address.getString("otras_senas");
                if (address.has("barrio"))
                    barrioString = address.getString("barrio"); //Puede no venir este campo

                if (cantonString.length() == 1) {
                    cantonString = "0" + cantonString;
                }

                if (distritoString.length() == 1) {
                    distritoString = "0" + distritoString;
                }

                if (barrioString.length() == 1) {
                    barrioString = "0" + barrioString;
                }

                provincia.setText(provinciaString);
                canton.setText(cantonString);
                distrito.setText(distritoString);
                barrio.setText(barrioString);
                otrasSenias.setText(otrasSenasString);

                if (!provinciaString.isEmpty())
                    GeneralData.this.provincia = new AddressElementCR("", provinciaString);
                if (!cantonString.isEmpty())
                    GeneralData.this.canton = new AddressElementCR("", cantonString);
                if (!distritoString.isEmpty())
                    GeneralData.this.distrito = new AddressElementCR("", distritoString);
                if (!barrioString.isEmpty())
                    GeneralData.this.barrio = new AddressElementCR("", barrioString);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        countrySelector.setEnabled(false);
    }

    private void defineAddressListeners() {
        View.OnFocusChangeListener focusListener = (v, hasFocus) -> {
            if (hasFocus) currentFocusedView = v.getId();
        };

        View.OnClickListener onClickListener = v -> {
            final int addressType;
            final ArrayList<String> parentCodes = new ArrayList<>();

            if (currentFocusedView == R.id.costa_rica_addr_canton) {
                addressType = AddressElementCR.CANTON;
                parentCodes.add(provincia.getCode());
            } else if (currentFocusedView == R.id.costa_rica_addr_distrito) {
                addressType = AddressElementCR.DISTRITO;
                parentCodes.add(provincia.getCode());
                parentCodes.add(canton.getCode());
            } else if (currentFocusedView == R.id.costa_rica_addr_barrio) {
                addressType = AddressElementCR.BARRIO;
                parentCodes.add(provincia.getCode());
                parentCodes.add(canton.getCode());
                parentCodes.add(distrito.getCode());
            } else {
                addressType = AddressElementCR.PROVINCIA;
            }

            //Enviar peticion para buscar lo seleccionado
            chipRedManager.getCrAddressElement(addressType, parentCodes);
            createDialogForAddressElements(addressType);
        };

        MaterialBetterSpinner provincia = getActivity().findViewById(R.id
                .costa_rica_addr_provincia);
        MaterialBetterSpinner canton = getActivity().findViewById(R.id.costa_rica_addr_canton);
        MaterialBetterSpinner distrito = getActivity().findViewById(R.id.costa_rica_addr_distrito);
        MaterialBetterSpinner barrio = getActivity().findViewById(R.id.costa_rica_addr_barrio);

        ArrayAdapter<String> dummyAdapter = new ArrayAdapter<>(getActivity(), android.R.layout
                .simple_list_item_1, new String[]{});
        provincia.setAdapter(dummyAdapter);
        canton.setAdapter(dummyAdapter);
        distrito.setAdapter(dummyAdapter);
        barrio.setAdapter(dummyAdapter);

        provincia.setOnFocusChangeListener(focusListener);
        canton.setOnFocusChangeListener(focusListener);
        distrito.setOnFocusChangeListener(focusListener);
        barrio.setOnFocusChangeListener(focusListener);

        provincia.setOnClickListener(onClickListener);
        canton.setOnClickListener(onClickListener);
        distrito.setOnClickListener(onClickListener);
        barrio.setOnClickListener(onClickListener);
    }

    private void createDialogForAddressElements(int addressType) {
        //Crear diálogo
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String title;

        switch (addressType) {
            case AddressElementCR.PROVINCIA:
                title = "Provincias";
                break;

            case AddressElementCR.CANTON:
                title = "Cantónes";
                break;

            case AddressElementCR.DISTRITO:
                title = "Distritos";
                break;

            case AddressElementCR.BARRIO:
                title = "Barrios";
                break;

            default:
                title = "Direcciones";
        }

        //Definir titulo según el valor encontrado
        builder.setTitle(title);

        //Crear view
        View content = getLayoutInflater().inflate(R.layout.dialog_simple_recycler_view, null);
        builder.setView(content);

        //Definir boton de cerrar diálogo
        builder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        crAddressesDialog = builder.create();
        crAddressesDialog.setCancelable(true);
        crAddressesDialog.show();
    }

    private void showFoundAddressElements(JSONObject response) {
        //Obtener progressbar
        ProgressBar progressBar = crAddressesDialog.findViewById(R.id.progress_bar);

        //Obtener textView de error
        TextView noValue = crAddressesDialog.findViewById(R.id.no_values);

        //Obtener su recycler view
        final RecyclerView addressElementsList = crAddressesDialog.findViewById(R.id.servers_list);

        //Obtener arreglo de direcciones
        final ArrayList<AddressElementCR> addressElementCRS = new ArrayList<>();

        try {
            JSONArray array = response.getJSONArray("data");
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String name = object.getString("nombre");
                String codigo = object.getString("codigo");

                AddressElementCR addressElementCR = new AddressElementCR(name, codigo);
                addressElementCRS.add(addressElementCR);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            noValue.setVisibility(View.VISIBLE);
        }

        if (!addressElementCRS.isEmpty()) {
            SimpleImageTextRow simpleImageTextRow = new SimpleImageTextRow(addressElementCRS);
            simpleImageTextRow.setListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = addressElementsList.getChildAdapterPosition(v);
                    defineSelectedAddressElement(addressElementCRS.get(position));
                    crAddressesDialog.dismiss();
                }
            });
            addressElementsList.setAdapter(simpleImageTextRow);
            addressElementsList.setLayoutManager(new LinearLayoutManager(getActivity()));
            addressElementsList.setHasFixedSize(true);
            addressElementsList.setVisibility(View.VISIBLE);
        } else {
            noValue.setVisibility(View.VISIBLE);
        }
        progressBar.setVisibility(View.GONE);
    }

    private void defineSelectedAddressElement(AddressElementCR addressElementCR) {
        MaterialBetterSpinner provinciaSpinner = getActivity().findViewById(R.id
                .costa_rica_addr_provincia);
        MaterialBetterSpinner cantonSpinner = getActivity().findViewById(R.id
                .costa_rica_addr_canton);
        MaterialBetterSpinner distritoSpinner = getActivity().findViewById(R.id
                .costa_rica_addr_distrito);
        MaterialBetterSpinner barrioSpinner = getActivity().findViewById(R.id
                .costa_rica_addr_barrio);

        if (currentFocusedView == R.id.costa_rica_addr_provincia) {
            provincia = addressElementCR;
            provinciaSpinner.setText(provincia.toString());

            canton = null;
            cantonSpinner.setText("");

            distrito = null;
            distritoSpinner.setText("");

            barrio = null;
            barrioSpinner.setText("");

            cantonSpinner.setEnabled(true);
            distritoSpinner.setEnabled(false);
            barrioSpinner.setEnabled(false);
        } else if (currentFocusedView == R.id.costa_rica_addr_canton) {
            canton = addressElementCR;
            cantonSpinner.setText(canton.toString());

            distrito = null;
            distritoSpinner.setText("");

            barrio = null;
            barrioSpinner.setText("");

            distritoSpinner.setEnabled(true);
            barrioSpinner.setEnabled(false);
        } else if (currentFocusedView == R.id.costa_rica_addr_distrito) {
            distrito = addressElementCR;
            distritoSpinner.setText(distrito.toString());

            barrio = null;
            barrioSpinner.setText("");

            barrioSpinner.setEnabled(true);
        } else if (currentFocusedView == R.id.costa_rica_addr_barrio) {
            barrio = addressElementCR;
            barrioSpinner.setText(barrio.toString());
        }
    }

    private void initPlacesApi() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
    }

    @SuppressLint("MissingPermission")
    private void getStationLocation() {
        if (askForPermissions()) {
            if (isLocationEnabled()) {
                try {
                    if (locationRequest == null) {
                        locationRequest = new LocationRequest();
                        locationRequest.setInterval(100);
                        locationRequest.setFastestInterval(100);
                        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    }

                    if (locationCallback == null) {
                        locationCallback = new LocationCallback() {
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                super.onLocationResult(locationResult);
                                latitude = locationResult.getLastLocation().getLatitude();
                                longitude = locationResult.getLastLocation().getLongitude();
                                setLatLng();
                                fusedLocationClient.removeLocationUpdates(locationCallback);
                                showLocationProgress(false);
                            }
                        };
                    }

                    fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback,
                            Looper.myLooper());

                    showLocationProgress(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Error al obtener localización de la estación " +
                                    "-> " + e.getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "La localización esta deshabilitada",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setLatLng() {
        if (latitude != 0 && longitude != 0) {
            String latitude = String.format(Locale.getDefault(), "%.6f", (float) this.latitude);
            String longitude = String.format(Locale.getDefault(), "%.6f", (float) this.longitude);

            latitudeEt.setText(latitude);
            longitudeEt.setText(longitude);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            fusedLocationClient.removeLocationUpdates(locationCallback);
        } catch (Exception e) {
            Timber.e("Error al remover actualizacion de localización");
        }
    }

    ///////////////////////////////////////

    private boolean askForPermissions() {
        //Mapas y Localizacion
        boolean hasPermission = false;
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission
                .ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //Pedir Permisos al usuario
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest
                    .permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_FINE_LOCATION);
        } else {
            hasPermission = true;
        }

        return hasPermission;
    }

    private boolean isLocationEnabled() {
        LocationManager lm =
                (LocationManager) requireActivity().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            Timber.e("Error al obtener estado de la localización");
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            Timber.e("Error al obtener estado de la red");
        }

        return gps_enabled && network_enabled;
    }

    private void setUnEditable(View view) {
        MaterialEditText materialEditText = (MaterialEditText) view;
        materialEditText.setKeyListener(null);
    }

    private void showLocationProgress(boolean showProgress) {
        getLocationCv.setBackgroundColor(showProgress ?
                getResources().getColor(R.color.colorPrimary) :
                getResources().getColor(R.color.colorAccent));

        getLocationCv.setRadius(4);

        locationProgressBar.setVisibility(showProgress ? View.VISIBLE : View.GONE);
        locationImageView.setVisibility(showProgress ? View.GONE : View.VISIBLE);
    }

    public interface GeneralDataInterface {
        void returnToPreviousFragment();

        void showProgress();
    }
}
