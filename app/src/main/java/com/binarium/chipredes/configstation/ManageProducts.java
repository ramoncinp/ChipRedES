package com.binarium.chipredes.configstation;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.github.evilbunny2008.androidmaterialcolorpickerdialog.ColorPicker;
import com.github.evilbunny2008.androidmaterialcolorpickerdialog.ColorPickerCallback;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.Product;
import com.binarium.chipredes.R;
import com.binarium.chipredes.utils.DebouncedOnClickListener;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class ManageProducts extends Fragment implements StationManager.OnResponse
{
    //Constantes
    private static final int MODIFY_PRODUCT_INFO = 0;
    private static final int REMOVE_PRODUCT = 1;

    //Variables
    private boolean addOrModify = false; //True para agregar, false para modificar
    private int positionToRemoveOrModify;

    //Objetos
    private Product productToAddOrModify;
    private StationManager stationManager;

    //Views
    private Dialog addProductDialog;
    private ProductCardAdapter productCardAdapter;
    private ProgressBar progressBar;
    private RecyclerView productsList;
    private TextView noProducts;

    //Listas
    private ArrayList<Product> products;
    private HashMap<Integer, String> productsPrices = new HashMap<>();

    private ManageProductsInterface manageProductsInterface;

    public ManageProducts()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState)

    {
        View content = inflater.inflate(R.layout.fragment_manage_products, container, false);
        noProducts = content.findViewById(R.id.no_products);
        productsList = content.findViewById(R.id.products_list);
        progressBar = content.findViewById(R.id.progress_bar);

        FloatingActionButton fab = content.findViewById(R.id.add_product);
        fab.setOnClickListener(new DebouncedOnClickListener(800)
        {
            @Override
            public void onDebouncedClick(View v)
            {
                showAddProductDialog(null, v);
            }
        });

        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        initStationManager();
        stationManager.getStationProducts();
    }

    public void setManageProductsInterface(ManageProductsInterface manageProductsInterface)
    {
        this.manageProductsInterface = manageProductsInterface;
    }

    private void initStationManager()
    {
        stationManager = new StationManager(getContext(), this);
    }

    //Métodos para comunicación con servidor
    @Override
    public void chipRedMessage(String crMessage, int webServiceN)
    {
        progressBar.setVisibility(View.GONE);

        if (webServiceN == StationManager.GET_EMAX_PRODUCTS)
        {
            //Actualizar precios de productos
            parseEmaxResponse(crMessage);
        }
        else
        {
            //Preparar y mostrar en base a la lista de productos
            parseProducts(null);
            //Mostrar mensaje
            Toast.makeText(getContext(), crMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void chipRedError(String errorMessage, int webServiceN)
    {
        progressBar.setVisibility(View.GONE);

        if (webServiceN == StationManager.GET_EMAX_PRODUCTS)
        {
            onDrawnProducts();
        }
        else
        {
            GenericDialog genericDialog = new GenericDialog("Error", errorMessage, new Runnable()
            {
                @Override
                public void run()
                {
                    if (getActivity() != null) getActivity().onBackPressed();
                }
            }, null, getContext());

            genericDialog.setPositiveText("Sí");
            genericDialog.setNegativeText("No");
            genericDialog.show();
        }
    }

    @Override
    public void showResponse(JSONObject response, int webServiceN)
    {
        progressBar.setVisibility(View.GONE);

        switch (webServiceN)
        {
            case StationManager.GET_PRODUCTS:
                parseProducts(response);
                break;

            case StationManager.ADD_PRODUCT:
                try
                {
                    //Notificar que se pudo agregar a la bd
                    ChipREDConstants.showSnackBarMessage(response.getString("message"),
                            getActivity());

                    onProductAdded();
                }
                catch (JSONException | NullPointerException e)
                {
                    e.printStackTrace();
                }
                break;

            case StationManager.DELETE_PRODUCT:
                //Mostrar lista de productos
                productsList.setVisibility(View.VISIBLE);
                //Eliminar despachador de la lista
                onProductRemoved();
                break;

            case StationManager.MODIFY_PRODUCT:
                //Reemplazar elemento
                products.set(positionToRemoveOrModify, productToAddOrModify);
                //Actualizar adaptador
                productCardAdapter.notifyDataSetChanged();
                //Mostrar lista de productos
                productsList.setVisibility(View.VISIBLE);

                //Actualizar archivo .json
                StationFileManager.syncProductsInStationFile(products, getContext());
                //Sincronizar servidor local
                manageProductsInterface.syncLocalServer();
                break;
        }
    }

    private void parseProducts(@Nullable JSONObject response)
    {
        products = new ArrayList<>();

        try
        {
            //Obtener el arreglo de productos
            JSONArray productsArray = response.getJSONObject("data").getJSONArray("productos");

            //Obtener cada producto
            for (int i = 0; i < productsArray.length(); i++)
            {
                //Obtener objeto
                JSONObject mProduct = productsArray.getJSONObject(i);
                //Obtener atributos
                Product product = new Product();
                if (mProduct.has("descripcion"))
                    product.setDescription(mProduct.getString("descripcion"));

                if (mProduct.has("id_producto")) product.setId(mProduct.getString("id_producto"));

                if (mProduct.has("precio_litro"))
                    product.setLiterPrice(mProduct.getDouble("precio_litro"));

                if (mProduct.has("color")) product.setColor(mProduct.getString("color"));

                products.add(product);
            }
        }
        catch (JSONException | NullPointerException e)
        {
            e.printStackTrace();
        }

        productCardAdapter = new ProductCardAdapter(products);
        productCardAdapter.setOnClickListener(new DebouncedOnClickListener(800)
        {
            @Override
            public void onDebouncedClick(View view)
            {
                productOptionSelected(view, MODIFY_PRODUCT_INFO);
            }
        });
        productCardAdapter.setLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                productOptionSelected(view, REMOVE_PRODUCT);
                return true;
            }
        });
        productsList.setAdapter(productCardAdapter);
        productsList.setLayoutManager(new GridLayoutManager(getContext(), getResources().getInteger(R.integer.station_elements_cards_columns_number)));

        onDrawnProducts();
    }

    private void showAddProductDialog(@Nullable Product product, final View dialog)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Agregar producto");

        int red = 127;
        int green = 127;
        int blue = 127;

        View view = getLayoutInflater().inflate(R.layout.add_product_layout, null);
        //Obtener EditText's
        final MaterialEditText idEt = view.findViewById(R.id.add_product_id);
        final MaterialEditText descriptionEt = view.findViewById(R.id.add_product_description);
        final MaterialEditText literPriceEt = view.findViewById(R.id.add_product_liter_price);
        final MaterialEditText colorEt = view.findViewById(R.id.add_product_color);
        final CardView pickedColorCv = view.findViewById(R.id.picked_color_card_view);

        if (product != null)
        {
            idEt.setText(product.getId());
            descriptionEt.setText(product.getDescription());
            colorEt.setText(product.getColor());

            String literPriceString = ChipREDConstants.MX_AMOUNT_FORMAT.format(product
                    .getLiterPrice());
            literPriceEt.setText(literPriceString);

            if (product.getColor() != null && !product.getColor().isEmpty())
            {
                pickedColorCv.setVisibility(View.VISIBLE);
                pickedColorCv.setCardBackgroundColor(Color.parseColor(product.getColor()));

                //Descomponer color en RGB
                int[] rgbColor = hexColorToRgb(product.getColor());
                if (rgbColor != null)
                {
                    red = rgbColor[0];
                    green = rgbColor[1];
                    blue = rgbColor[2];
                }
            }

            addOrModify = false;
        }
        else
        {
            addOrModify = true;
        }

        //Crear ColorPicker
        final ColorPicker colorPicker = new ColorPicker(getActivity(), red, green, blue);
        //Asignar callBack
        colorPicker.setCallback(new ColorPickerCallback()
        {
            @Override
            public void onColorChosen(int color)
            {
                //Obtener valor en String
                String mColor = String.format("#%06X", (0xFFFFFF & color));
                colorEt.setText(mColor);
                pickedColorCv.setVisibility(View.VISIBLE);
                pickedColorCv.setCardBackgroundColor(Color.parseColor(mColor));
            }
        });

        //Eliminar keyListener para el editText del color
        colorEt.setKeyListener(null);

        //Agregar listener al EditText del color
        colorEt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                colorPicker.show();
                colorPicker.enableAutoClose();
            }
        });

        colorEt.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View view, boolean b)
            {
                if (b)
                {
                    colorPicker.show();
                    colorPicker.enableAutoClose();
                }
            }
        });

        //Obtener botones
        Button cancelButton = view.findViewById(R.id.add_product_close_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                addProductDialog.dismiss();
            }
        });

        Button submitButton = view.findViewById(R.id.add_product_submit_button);
        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Crear validador
                METValidator emptyValidator = new METValidator("Campo obligatorio")
                {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
                    {
                        return !isEmpty;
                    }
                };

                //Validar información
                boolean validInfo;
                validInfo = idEt.validateWith(emptyValidator);
                validInfo &= descriptionEt.validateWith(emptyValidator);
                validInfo &= literPriceEt.validateWith(emptyValidator);
                validInfo &= colorEt.validateWith(emptyValidator);

                if (validInfo)
                {
                    //Crear objeto
                    Product newProduct = new Product();
                    newProduct.setId(idEt.getText().toString());
                    newProduct.setDescription(descriptionEt.getText().toString());
                    newProduct.setLiterPrice(Double.parseDouble(literPriceEt.getText().toString()));
                    newProduct.setColor(colorEt.getText().toString());

                    productToAddOrModify = newProduct;

                    if (addOrModify)
                    {
                        stationManager.addProduct(newProduct);
                    }
                    else
                    {
                        stationManager.modifyProduct(newProduct);
                        positionToRemoveOrModify = productsList.getChildAdapterPosition(dialog);
                    }

                    //Ocultar RecyclerView
                    productsList.setVisibility(View.GONE);
                    //Quitar diálogo
                    addProductDialog.dismiss();
                }
            }
        });

        builder.setView(view);

        addProductDialog = builder.create();
        addProductDialog.show();
    }

    private void productOptionSelected(final View view, int op)
    {
        final int position = productsList.getChildAdapterPosition(view);
        final Product product = products.get(position);

        switch (op)
        {
            case MODIFY_PRODUCT_INFO:
                showAddProductDialog(product, view);
                break;

            case REMOVE_PRODUCT:
                GenericDialog genericDialog = new GenericDialog("Eliminar producto", "¿Esta " +
                        "seguro que desea remover el producto seleccionado?", new Runnable()
                {
                    @Override
                    public void run()
                    {
                        //Eliminar producto
                        stationManager.deleteProduct(product);
                        //Mostrar progressBar
                        progressBar.setVisibility(View.VISIBLE);
                        //Ocultar lista
                        productsList.setVisibility(View.GONE);

                        positionToRemoveOrModify = position;
                    }
                }, new Runnable()
                {
                    @Override
                    public void run()
                    {
                        //Nada
                    }
                }, getContext());

                genericDialog.setPositiveText("Sí");
                genericDialog.setNegativeText("No");
                genericDialog.show();

                break;
        }
    }

    private void onProductAdded()
    {
        //Agregar despachador a la lista
        products.add(productToAddOrModify);
        productCardAdapter.notifyItemInserted(products.size() - 1);

        //Agregar despachador al archivo .json
        StationFileManager.syncProductsInStationFile(products, getContext());

        //Sincronizar servidor local
        manageProductsInterface.syncLocalServer();

        //Mostrar lista de despachadores
        productsList.setVisibility(View.VISIBLE);

        //Ocultar el mensaje si es que antes no había productos
        noProducts.setVisibility(View.GONE);
    }

    private void onProductRemoved()
    {
        products.remove(positionToRemoveOrModify);
        productCardAdapter.notifyItemRemoved(positionToRemoveOrModify);
        productCardAdapter.notifyItemRangeChanged(positionToRemoveOrModify, products.size());

        //Agregar despachador al archivo .json
        StationFileManager.syncProductsInStationFile(products, getContext());
        //Sincronizar servidor local
        manageProductsInterface.syncLocalServer();

        if (products.isEmpty())
        {
            noProducts.setVisibility(View.VISIBLE);
            productsList.setVisibility(View.GONE);
        }
    }

    private int[] hexColorToRgb(String hexColor)
    {
        if (hexColor.contains("#"))
        {
            if (hexColor.substring(1).length() == 6)
            {
                int red = Integer.decode("0x" + hexColor.substring(1, 3));
                int green = Integer.decode("0x" + hexColor.substring(3, 5));
                int blue = Integer.decode("0x" + hexColor.substring(5, 7));

                Log.d("ManageProducts", "R ->" + String.valueOf(red));
                Log.d("ManageProducts", "G ->" + String.valueOf(green));
                Log.d("ManageProducts", "B ->" + String.valueOf(blue));

                return new int[]{red, green, blue};
            }
        }

        return null;
    }

    private void onDrawnProducts()
    {
        if (products.isEmpty())
        {
            productsList.setVisibility(View.GONE);
            noProducts.setVisibility(View.VISIBLE);
        }
        else
        {
            productsList.setVisibility(View.VISIBLE);
            noProducts.setVisibility(View.GONE);

            Collections.sort(products, new ProductComparator());
        }
    }

    private void parseEmaxResponse(String response)
    {
        //Buscar el cierre de etiqueta de "/listprod"
        int closeTagIdx = response.indexOf("</listprod>");

        if (closeTagIdx == -1)
        {
            //Error, abortar proceso!
            onDrawnProducts();
        }
        else
        {
            //Buscar el inicio de la etiqueta
            int startTagIdx = closeTagIdx;

            while (response.charAt(startTagIdx) != '>')
            {
                if (startTagIdx == 0)
                {
                    //Error!, abortar proceso!
                    onDrawnProducts();
                    return;
                }
                startTagIdx--;
            }

            //Analizar el valor de la etiqueta... ejemplo "3-13.57,2-12.95" -> "id-precio,
            // id-precio"
            String content = response.substring(startTagIdx + 1, closeTagIdx);

            int contentIndex = 0;
            while (contentIndex <= content.length())
            {
                //Recortar si ya va más de una iteración
                content = content.substring(contentIndex);

                int separatorIdx = content.indexOf("-");
                int commaIdx = content.indexOf(",");

                if (separatorIdx == -1)
                {
                    //Error!, abortar proceso!
                    onDrawnProducts();
                    return;
                }

                if (commaIdx == -1)
                {
                    commaIdx = content.length();
                }

                //Buscar id de producto
                int productId = Integer.parseInt(content.substring(0, separatorIdx));
                String price = content.substring(separatorIdx + 1, commaIdx);

                if (!productsPrices.containsKey(productId))
                {
                    productsPrices.put(productId, price);
                }

                contentIndex = commaIdx + 1;
            }
        }
    }

    public interface ManageProductsInterface
    {
        void syncLocalServer();
    }

    private class ProductComparator implements Comparator<Product>
    {
        public int compare(Product a, Product b)
        {
            Integer idA = Integer.parseInt(a.getId());
            Integer idB = Integer.parseInt(b.getId());

            return idA.compareTo(idB);
        }
    }
}
