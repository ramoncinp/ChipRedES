package com.binarium.chipredes.configstation;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.binarium.chipredes.R;

import java.util.ArrayList;

public class StationElementsAdapter extends RecyclerView.Adapter<StationElementsAdapter.StationElementsViewHolder>
        implements View.OnClickListener
{
    private ArrayList<StationElement> stationElements;
    private Context context;
    private View.OnClickListener listener;

    StationElementsAdapter(ArrayList<StationElement> stationElements, Context context)
    {
        this.context = context;
        this.stationElements = stationElements;
    }

    @Override
    public int getItemCount()
    {
        return stationElements.size();
    }

    @NonNull
    @Override
    public StationElementsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.payment_method_card_view,
                viewGroup,
                false);

        StationElementsViewHolder viewHolder = new StationElementsViewHolder(v);

        v.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull StationElementsViewHolder stationElementsViewHolder,
                                 int i)
    {
        StationElement stationElement = stationElements.get(i);
        stationElementsViewHolder.desc.setText(stationElement.getDescription());
        stationElementsViewHolder.image.setImageResource(stationElement.getImage());

        if (stationElement.isComplete())
        {
            ((CardView) stationElementsViewHolder.itemView).setCardBackgroundColor(ContextCompat.getColor(context, R.color.activo));
        }
        else
        {
            ((CardView) stationElementsViewHolder.itemView).setCardBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    static class StationElementsViewHolder extends RecyclerView.ViewHolder
    {
        private TextView desc;
        private ImageView image;

        StationElementsViewHolder(View itemView)
        {
            super(itemView);

            desc = itemView.findViewById(R.id.payment_method_desc);
            image = itemView.findViewById(R.id.payment_method_image);
        }
    }
}