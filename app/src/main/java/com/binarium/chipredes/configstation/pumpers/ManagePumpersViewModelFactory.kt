package com.binarium.chipredes.configstation.pumpers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.binarium.chipredes.emax.EmaxDBManager

class ManagePumpersViewModelFactory(
        private val emaxDBManager: EmaxDBManager) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ManagePumpersViewModel::class.java)) {
            return ManagePumpersViewModel(emaxDBManager) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}