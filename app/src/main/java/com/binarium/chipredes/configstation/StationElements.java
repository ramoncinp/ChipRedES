package com.binarium.chipredes.configstation;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.R;

import org.json.JSONObject;

import java.util.ArrayList;

public class StationElements extends Fragment
{
    //Views
    private CardView readyCv;
    private RecyclerView listElements;

    //Objetos
    private ArrayList<StationElement> stationElementsList;
    private StationElementsInterface stationElementsInterface;
    private StationElementsAdapter stationElementsAdapter;

    public StationElements()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState)

    {
        View content = inflater.inflate(R.layout.fragment_station_elements, container, false);
        listElements = content.findViewById(R.id.station_elements_list);
        readyCv = content.findViewById(R.id.ready_cv);
        readyCv.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                stationElementsInterface.onReadyButtonPressed();
            }
        });

        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        setListElements();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        //Refrescar datos según la información que contenga el archivo
        validateCompletedElements();
    }

    public void setStationElementsInterface(StationElementsInterface stationElementsInterface)
    {
        this.stationElementsInterface = stationElementsInterface;
    }

    private void setListElements()
    {
        //Crear listas para descripciones y referencias de imagenes
        stationElementsList = new ArrayList<>();

        //Obtener los elementos de una estación
        String[][] mStationElements = ChipREDConstants.STATION_ITEMS;
        for (String[] stationElement : mStationElements)
        {
            //Obtener referencia de la imagen
            int resource = getContext().getResources().getIdentifier(stationElement[1], "mipmap",
                    getContext().getPackageName());

            //Crear objeto y agregar valor a la lista
            stationElementsList.add(new StationElement(stationElement[0], resource));
        }

        //Crear adaptador
        stationElementsAdapter = new StationElementsAdapter(stationElementsList, getContext());
        stationElementsAdapter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Indicar cual posicion fue seleccionada
                stationElementsInterface.onListElementSelected(listElements
                        .getChildAdapterPosition(view));
            }
        });

        listElements.setAdapter(stationElementsAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        listElements.setLayoutManager(linearLayoutManager);
    }

    private void validateCompletedElements()
    {
        //Obtener datos de estación almacenados
        JSONObject mStation = StationFileManager.getStation(getContext());

        if (mStation != null)
        {
            boolean hasGeneralData;

            hasGeneralData = mStation.has("rfc_cedula");
            hasGeneralData &= mStation.has("direccion");
            hasGeneralData &= mStation.has("nombre_estacion");
            hasGeneralData &= mStation.has("id_estacion");
            hasGeneralData &= mStation.has("telefono");
            hasGeneralData &= mStation.has("email");
            hasGeneralData &= mStation.has("gps");

            //Definir visualmente si el elemento 0 de la lista (Datos generales) esta completo
            if (hasGeneralData)
            {
                stationElementsList.get(0).setComplete(true);
                readyCv.setVisibility(View.VISIBLE);
            }
            else
            {
                stationElementsList.get(0).setComplete(false);
                readyCv.setVisibility(View.GONE);
            }

            //Definir si hay al menos un despachador
            stationElementsList.get(1).setComplete(mStation.has("despachadores"));

            //Definir si hay al menos un producto
            stationElementsList.get(2).setComplete(mStation.has("productos"));

            //Definir si hay al menos un dispensario
            stationElementsList.get(3).setComplete(mStation.has("dispensarios"));

            //Refrescar adaptor
            stationElementsAdapter.notifyDataSetChanged();
        }
    }

    public interface StationElementsInterface
    {
        void onListElementSelected(int listElement);

        void onReadyButtonPressed();
    }
}
