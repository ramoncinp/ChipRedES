package com.binarium.chipredes.configstation

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.GetRegistroVentaEfectivoPost
import com.binarium.chipredes.wolke.models.SetRegistroVentaEfectivoPost
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class CashConfigViewModel @Inject constructor(
    private val wolkeApiService: WolkeApiService,
    private val sharedPreferences: SharedPreferences
) : ViewModel() {

    var settingChecked: Boolean = false

    private val _stationNumber = getThisStationNumber()

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _isActivated = MutableLiveData<Boolean>()
    val isActivated: LiveData<Boolean>
        get() = _isActivated

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage


    init {
        getConfigValue()
    }

    fun getConfigValue() {
        CoroutineScope(Dispatchers.IO).launch {
            fetchConfigValue()
        }
    }

    fun setNewConfigValue(newValue: Boolean) {
        CoroutineScope(Dispatchers.IO).launch {
            Timber.d("Definiendo nuevo valor")
            _isLoading.postValue(true)

            updateConfigValue(newValue)

            _isLoading.postValue(false)
            Timber.d("Nuevo valor definido")
        }
    }

    suspend fun fetchConfigValue() {
        try {
            val post = GetRegistroVentaEfectivoPost(
                numeroEstacion = _stationNumber
            )
            val result = wolkeApiService.getRegistroVentaEfectivo(post)
            _isActivated.postValue(result.data?.registroVentaEfectivo)

        } catch (e: Exception) {
            Timber.e("Error al obtener valor")
            _errorMessage.postValue("Error al obtener valor actual")
        }
    }

    suspend fun updateConfigValue(newValue: Boolean) {
        try {
            val post = SetRegistroVentaEfectivoPost(
                numeroEstacion = _stationNumber,
                activado = newValue
            )

            val result = wolkeApiService.setRegistroVentaEfectivo(post)
            if (result.response.equals("Ok")) {
                _isActivated.postValue(newValue)
            }
        } catch (e: Exception) {
            Timber.e("Error al obtener valor")
            _errorMessage.postValue("Error al definir nuevo valor")
            _isActivated.postValue(!newValue)
        }
    }

    fun getThisStationNumber(): String {
        return sharedPreferences.getString(ChipREDConstants.STATION_ID, "").toString()
    }
}