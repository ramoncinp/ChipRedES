package com.binarium.chipredes.configstation.pumpers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.Pumper
import com.binarium.chipredes.emax.EmaxDBManager
import com.binarium.chipredes.emax.model.EmaxPumper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ManagePumpersViewModel @Inject constructor(
    private val emaxDBManager: EmaxDBManager,
) : ViewModel() {

    private var emaxPumpers = ArrayList<EmaxPumper>()
    var chipredPumpers = ArrayList<Pumper>()
    var filteredPumpers = ArrayList<EmaxPumper>()

    fun getEmaxPumpersFromDb() {
        viewModelScope.launch {
            fetchEmaxPumpers()
        }
    }

    private suspend fun fetchEmaxPumpers() {
        withContext(Dispatchers.IO) {
            try {
                val list = emaxDBManager.getPumpers()
                if (list != null) {
                    emaxPumpers = list as ArrayList<EmaxPumper>
                    Timber.d("EmaxPumpers fetched -> $emaxPumpers")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Creates a resulting list, showing just the pumpers that are not registered in the ChipRED
     * station
     */
    fun filterPumpers() {
        filteredPumpers.clear()
        emaxPumpers.forEach { emaxPumper ->
            if (!isPumperContained(emaxPumper))
                filteredPumpers.add(emaxPumper)
        }
    }

    private fun isPumperContained(emaxPumper: EmaxPumper): Boolean {
        var isContained = false
        for (crPumper in chipredPumpers) {
            if (crPumper.emaxId == emaxPumper.id &&
                crPumper.tagId == emaxPumper.tag
            ) {
                isContained = true
                break
            }
        }
        return isContained
    }

    fun isPumperRegistered(emaxPumper: EmaxPumper): Boolean {
        var isContained = false
        for (crPumper in chipredPumpers) {
            if (crPumper.emaxId == emaxPumper.id ||
                crPumper.tagId == emaxPumper.tag
            ) {
                isContained = true
                break
            }
        }
        return isContained
    }
}
