package com.binarium.chipredes.configstation.data.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "station")
data class ChipRedStation(
    @PrimaryKey
    val id: String,
    val rfcCedula: String,
    @Embedded
    val direccion: Direccion,
    val nombreEstacion: String,
    val idEstacion: String,
    @Embedded
    val telefono: Telefono,
    val email: String,
    val registroVentaEfectivo: Boolean?
)

data class Direccion(
    val numeroInt: String,
    val numeroEXT: String,
    val colonia: String,
    val calle: String,
    val codigoPostal: String,
    val pais: String,
    val estado: String,
    val municipio: String
)

data class Telefono(
    val codigo: String,
    val numero: String
)
