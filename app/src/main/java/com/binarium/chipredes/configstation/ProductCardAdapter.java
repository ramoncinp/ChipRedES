package com.binarium.chipredes.configstation;

import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.Product;
import com.binarium.chipredes.R;

import java.util.ArrayList;

public class ProductCardAdapter extends RecyclerView.Adapter<ProductCardAdapter
        .ProductCardViewHolder> implements View.OnLongClickListener, View.OnClickListener
{
    private ArrayList<Product> products;
    private View.OnClickListener onClickListener;
    private View.OnLongClickListener longClickListener;

    ProductCardAdapter(ArrayList<Product> products)
    {
        this.products = products;
    }

    @Override
    public int getItemCount()
    {
        return products.size();
    }

    @NonNull
    @Override
    public ProductCardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                .station_product_card, viewGroup, false);

        v.setOnClickListener(this);
        v.setOnLongClickListener(this);

        return new ProductCardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductCardViewHolder productCardViewHolder, int i)
    {
        Product product = products.get(i);
        productCardViewHolder.id.setText(product.getId());
        productCardViewHolder.description.setText(product.getDescription());

        //Obtener precio por litro
        String mLiterPrice = ChipREDConstants.MX_AMOUNT_FORMAT.format(product.getLiterPrice());
        productCardViewHolder.literPrice.setText(mLiterPrice);

        //Obtener color y setear el obtenido en la CardView
        ((CardView) productCardViewHolder.itemView).setCardBackgroundColor(Color.parseColor
                (product.getColor()));
    }

    public void setOnClickListener(View.OnClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    void setLongClickListener(View.OnLongClickListener longClickListener)
    {
        this.longClickListener = longClickListener;
    }

    @Override
    public void onClick(View view)
    {
        if (onClickListener != null)
        {
            onClickListener.onClick(view);
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if (longClickListener != null)
        {
            longClickListener.onLongClick(view);
        }
        return true;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    static class ProductCardViewHolder extends RecyclerView.ViewHolder
    {
        private TextView id;
        private TextView description;
        private TextView literPrice;

        ProductCardViewHolder(@NonNull View itemView)
        {
            super(itemView);
            id = itemView.findViewById(R.id.product_card_id);
            description = itemView.findViewById(R.id.product_card_description);
            literPrice = itemView.findViewById(R.id.product_card_liter_price);
        }
    }
}
