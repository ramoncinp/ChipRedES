package com.binarium.chipredes.configstation;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipredes.Pumper;
import com.binarium.chipredes.R;

import java.util.ArrayList;

public class PumperCardAdapter extends RecyclerView.Adapter<PumperCardAdapter
        .PumperCardViewHolder> implements View.OnClickListener, View.OnLongClickListener
{
    private ArrayList<Pumper> pumpers;
    private View.OnClickListener onClickListener;
    private View.OnLongClickListener longClickListener;

    PumperCardAdapter(ArrayList<Pumper> pumpers)
    {
        this.pumpers = pumpers;
    }

    @Override
    public int getItemCount()
    {
        return pumpers.size();
    }

    @NonNull
    @Override
    public PumperCardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pumper_credential,
                viewGroup, false);

        v.setOnClickListener(this);
        v.setOnLongClickListener(this);

        return new PumperCardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PumperCardViewHolder pumperCardViewHolder, int i)
    {
        Pumper pumper = pumpers.get(i);
        pumperCardViewHolder.name.setText(pumper.getName());
        pumperCardViewHolder.emaxId.setText(pumper.getEmaxId());
        pumperCardViewHolder.tag.setText(pumper.getTagId());
        pumperCardViewHolder.initals.setText(pumper.getNameInitials());

        if (pumper.getNip() != null)
        {
            if (!pumper.getNip().isEmpty())
            {
                pumperCardViewHolder.password.setText("••••");
            }
        }
    }

    void setOnClickListener(View.OnClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    void setLongClickListener(View.OnLongClickListener longClickListener)
    {
        this.longClickListener = longClickListener;
    }

    @Override
    public void onClick(View view)
    {
        if (onClickListener != null)
        {
            onClickListener.onClick(view);
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if (longClickListener != null)
        {
            longClickListener.onLongClick(view);
        }
        return true;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    static class PumperCardViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name;
        private TextView emaxId;
        private TextView tag;
        private TextView password;
        private TextView initals;

        PumperCardViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.pumper_card_name);
            emaxId = itemView.findViewById(R.id.pumper_card_emax_id);
            tag = itemView.findViewById(R.id.pumper_card_tag);
            password = itemView.findViewById(R.id.pumper_card_password);
            initals = itemView.findViewById(R.id.pumper_card_initials);
        }
    }
}
