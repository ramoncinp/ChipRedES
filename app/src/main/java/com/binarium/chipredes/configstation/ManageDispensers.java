package com.binarium.chipredes.configstation;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.binarium.chipredes.dispenser.Dispenser;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.LoadingPosition;
import com.binarium.chipredes.R;
import com.binarium.chipredes.adapters.LoadingPositionAdapter;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

public class ManageDispensers extends Fragment implements StationManager.OnResponse
{
    //Views
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private TextView noDispensers;

    //Variables
    int dispenserToRemove = 0;

    //Objetos
    private DispenserCardAdapter dispensersAdapter;
    private LoadingPositionAdapter loadingPositionAdapter;
    private StationManager stationManager;

    //Listas
    private ArrayList<Dispenser> dispensers;
    private ArrayList<Dispenser> dispensersToAdd;
    private ArrayList<LoadingPosition> loadingPositions;
    private ArrayList<LoadingPosition> loadingPositionsToAdd;

    private ManageDispensersInterface manageDispensersInterface;

    public ManageDispensers()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState)
    {
        // Inflate the layout for this fragment
        View content = inflater.inflate(R.layout.fragment_manage_dispensers, container, false);

        progressBar = content.findViewById(R.id.progress_bar);
        recyclerView = content.findViewById(R.id.dispensers_list);
        noDispensers = content.findViewById(R.id.no_dispensers);

        FloatingActionButton fab = content.findViewById(R.id.add_dispensers);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                addDispensers();
            }
        });

        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        initDispensersList();
        initStationManager();
    }

    public void setManageDispensersInterface(ManageDispensersInterface manageDispensersInterface)
    {
        this.manageDispensersInterface = manageDispensersInterface;
    }

    //Métodos para comunicación con servidor
    @Override
    public void chipRedMessage(String crMessage, int webServiceN)
    {
        progressBar.setVisibility(View.GONE);
        //Refrescar lista por si no hay dispensarios registrados
        parseDispensers(null);
        Toast.makeText(getContext(), crMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void chipRedError(String errorMessage, int webServiceN)
    {
        progressBar.setVisibility(View.GONE);
        GenericDialog genericDialog = new GenericDialog("Error", errorMessage, new Runnable()
        {
            @Override
            public void run()
            {
                if (getActivity() != null) getActivity().onBackPressed();
            }
        }, null, getContext());

        genericDialog.setPositiveText("Sí");
        genericDialog.setNegativeText("No");
        genericDialog.show();
    }

    @Override
    public void showResponse(JSONObject response, int webServiceN)
    {
        switch (webServiceN)
        {
            case StationManager.GET_DISPENSERS:
                parseDispensers(response);
                break;

            case StationManager.ADD_DISPENSERS:
                //Agregar las posiciones de carga
                dispensers.addAll(dispensersToAdd);
                onDrawnDispensers();
                dispensersAdapter.notifyDataSetChanged();

                //Agregar dispensarios al archivo .json
                StationFileManager.syncDispensersInStationFile(StationManager.dispensersListToJsonArray(dispensers), getContext());
                manageDispensersInterface.syncLocalServer();

                break;

            case StationManager.DELETE_DISPENSER:
                onDispenserRemoved();
                break;

            case StationManager.MODIFY_DISPENSER:
                onDispenserModified();
                break;
        }

        progressBar.setVisibility(View.GONE);
    }

    private void initStationManager()
    {
        stationManager = new StationManager(getContext(), this);
        stationManager.getStationDispensers();
    }

    private void initDispensersList()
    {
        dispensers = new ArrayList<>();
        dispensersAdapter = new DispenserCardAdapter(dispensers);
        dispensersAdapter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Obtener posicion de la lista
                int pos = recyclerView.getChildAdapterPosition(view);

                //Mostrar información para modificar
                showPcData(pos);
            }
        });
        dispensersAdapter.setLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                showDeleteDialog(view);
                return true;
            }
        });

        recyclerView.setAdapter(dispensersAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void showPcData(int position)
    {
        final Dispenser dispenser = dispensers.get(position);

        // Crear diálogo
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Obtener view
        View content = getActivity().getLayoutInflater().inflate(R.layout.dialog_modify_pc, null);
        builder.setView(content);

        // Definir título
        builder.setTitle("Modificar lado");

        // Obtener views del layout
        Button submitButton = content.findViewById(R.id.submit_button);
        Button cancelButton = content.findViewById(R.id.cancel_button);
        final MaterialEditText side1Et = content.findViewById(R.id.side_1_et);
        final MaterialEditText side2Et = content.findViewById(R.id.side_2_et);
        TextView dispNumber = content.findViewById(R.id.dispenser_number_tv);

        // Crear textos
        String dispNumbStr = String.format(Locale.getDefault(), "Dispensario %1$d",
                dispenser.getNumber());

        // Mostrarlos
        dispNumber.setText(dispNumbStr);

        int side1Number = dispenser.getSide1().getUserStationSide() == 0 ?
                dispenser.getSide1().getStationSide() :
                dispenser.getSide1().getUserStationSide();
        int side2Number = dispenser.getSide2().getUserStationSide() == 0 ?
                dispenser.getSide2().getStationSide() :
                dispenser.getSide2().getUserStationSide();

        // Escribir datos
        side1Et.setText(String.valueOf(side1Number));
        side2Et.setText(String.valueOf(side2Number));

        // Mostrar diálogo
        final Dialog dialog = builder.create();
        dialog.show();

        // Crear listeneres para los botones
        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                // Validar editText
                METValidator metValidator = new METValidator("Campo obligatorio")
                {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty)
                    {
                        return !isEmpty;
                    }
                };

                boolean isValid = side1Et.validateWith(metValidator);
                isValid &= side2Et.validateWith(metValidator);

                if (isValid)
                {
                    // Obtener dato
                    int userStationSide1 = Integer.parseInt(side1Et.getText().toString());
                    int userStationSide2 = Integer.parseInt(side2Et.getText().toString());

                    // Guardar dato de posicion de carga
                    dispenser.getSide1().setUserStationSide(userStationSide1);
                    dispenser.getSide2().setUserStationSide(userStationSide2);
                    dialog.dismiss();

                    // Registrar cambios
                    stationManager.modifyDispenser(dispenser);
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();
            }
        });
    }

    private void parseDispensers(@Nullable JSONObject response)
    {
        try
        {
            //Obtener arreglo
            JSONArray dispensersArray = response.getJSONObject("data").getJSONArray("dispensarios");

            for (int i = 0; i < dispensersArray.length(); i++)
            {
                //Crear dispensario
                Dispenser dispenser = new Dispenser();

                //Obtener numero de dispensario
                int dispenserNumber;
                JSONObject dispJsonObject = dispensersArray.getJSONObject(i);
                dispenserNumber = dispJsonObject.getInt("numero_dispensario");

                //Definir número de dispensario
                dispenser.setNumber(dispenserNumber);

                //Definir número de serie
                String serialNumber = String.format(Locale.getDefault(), "dis%06d",
                        dispenser.getNumber())
                        .replace(' ', '0');
                dispenser.setSerialNumber(serialNumber);

                JSONArray dispSides = dispJsonObject.getJSONArray("lados");
                for (int y = 0; y < dispSides.length(); y++)
                {
                    JSONObject dispSide = dispSides.getJSONObject(y);
                    LoadingPosition loadingPosition = new LoadingPosition(dispSide.getInt
                            ("lado_estacion"));

                    loadingPosition.setDispenserNumber(dispenserNumber);
                    loadingPosition.setCmIp(dispSide.getString("cm_ip"));
                    loadingPosition.setCmPort(dispSide.getInt("cm_puerto"));
                    loadingPosition.setCmState(dispSide.getString("cm_estado"));
                    loadingPosition.setSide(dispSide.getInt("lado"));
                    loadingPosition.setSideTag(dispSide.getString("lado_tag"));
                    loadingPosition.setState(dispSide.getInt("estado"));

                    if (dispSide.has("lado_estacion_usuario"))
                    {
                        loadingPosition.setUserStationSide(dispSide.getInt("lado_estacion_usuario"
                        ));
                    }

                    //Agregar posicion de carga a dispensario
                    if (y == 0) dispenser.setSide1(loadingPosition);
                    else dispenser.setSide2(loadingPosition);
                }

                // Agregar dispensario a la lista
                dispensers.add(dispenser);
                dispensersAdapter.notifyItemInserted(dispensers.size() - 1);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            ChipREDConstants.showSnackBarMessage("Error al procesar la información", getActivity());
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }

        onDrawnDispensers();
    }

    private void addDispensers()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Agregar dispensarios");

        final Dialog addDispensersDialog;

        View view = getLayoutInflater().inflate(R.layout.add_dispensers_layout, null);
        //Obtener EditText's
        final MaterialEditText quantity = view.findViewById(R.id.add_dispensers_quantity);
        final MaterialEditText initialDispNumb = view.findViewById(R.id
                .add_dispensers_initial_number);

        builder.setView(view);
        addDispensersDialog = builder.create();
        addDispensersDialog.show();

        //Obtener botones
        Button cancelButton = view.findViewById(R.id.add_dispensers_close_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                addDispensersDialog.dismiss();
            }
        });

        Button submitButton = view.findViewById(R.id.add_dispensers_submit_button);
        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Validar y obtener valores ingresados
                boolean valid = true;

                if (quantity.length() == 0)
                {
                    //Valor inválido
                    quantity.setError("Valor inválido");
                    valid = false;
                }

                if (initialDispNumb.length() == 0)
                {
                    //Valor inválido
                    initialDispNumb.setError("Valor inválido");
                    valid = false;
                }

                //Si fue válido
                if (valid)
                {
                    //Obtener valores
                    int mQuantity = Integer.parseInt(quantity.getText().toString());
                    int mInitialDispNumb = Integer.parseInt(initialDispNumb.getText().toString());

                    //Agregar la cantidad de dispensarios ingresada
                    addDispensers(mQuantity, mInitialDispNumb);

                    //Registrar dispensarios
                    registerDispensers();

                    //Cerrar Diálogo
                    addDispensersDialog.dismiss();
                }
            }
        });
    }

    private void addLoadingPositions(int quantity, int initialDispNumb)
    {
        loadingPositionsToAdd = new ArrayList<>();

        //Ciclo para construir cada posicion de carga
        for (int i = 0; i < quantity; i++)
        {
            int loadingPositionN = ((initialDispNumb + i) * 2) - 1;
            for (int i2 = 0; i2 < 2; i2++)
            {
                //Validar que la posicion de carga no exista
                if (!isLpAdded(loadingPositionN + i2))
                {
                    //Crear posicion de carga con valores default
                    LoadingPosition loadingPosition = new LoadingPosition(loadingPositionN + i2);
                    loadingPosition.setCmIp("0.0.0.0");
                    loadingPosition.setCmPort(80);
                    loadingPosition.setCmState("IN");
                    loadingPosition.setRealState(0);
                    loadingPosition.setSide(i2 + 1);
                    loadingPosition.setSideTag("A0" + String.valueOf(i2 + 1));
                    loadingPosition.setState(0);

                    //Agergar objeto a la lista
                    loadingPositionsToAdd.add(loadingPosition);
                }
            }
        }
    }

    private void addDispensers(int quantity, int initialDispNumb)
    {
        dispensersToAdd = new ArrayList<>();

        //Ciclo para construir cada posicion de carga
        for (int i = 0; i < quantity; i++)
        {
            Dispenser dispenser = new Dispenser();
            dispenser.setNumber(initialDispNumb + i);

            // Validar que el dispensario no exista
            if (!isDispenserAdded(initialDispNumb))
            {
                int loadingPositionN = ((initialDispNumb + i) * 2) - 1;
                for (int i2 = 0; i2 < 2; i2++)
                {
                    //Crear posicion de carga con valores default
                    LoadingPosition loadingPosition = new LoadingPosition(loadingPositionN + i2);
                    loadingPosition.setCmIp("0.0.0.0");
                    loadingPosition.setCmPort(80);
                    loadingPosition.setCmState("IN");
                    loadingPosition.setRealState(0);
                    loadingPosition.setSide(i2 + 1);
                    loadingPosition.setSideTag("A0" + (i2 + 1));
                    loadingPosition.setState(0);

                    //Agregar posicion de carga
                    if (i2 == 0) dispenser.setSide1(loadingPosition);
                    else dispenser.setSide2(loadingPosition);
                }

                // Agregar a la lista de dispensarios por registrar
                dispensersToAdd.add(dispenser);
            }
        }
    }

    private boolean isLpAdded(int stationSide)
    {
        for (LoadingPosition loadingPosition : loadingPositions)
        {
            if (loadingPosition.getStationSide() == stationSide)
            {
                return true;
            }
        }

        return false;
    }

    private boolean isDispenserAdded(int dispNumb)
    {
        for (Dispenser dispenser : dispensers)
        {
            if (dispenser.getNumber() == dispNumb)
            {
                return true;
            }
        }

        return false;
    }

    private void showDeleteDialog(View view)
    {
        final int listPosition = recyclerView.getChildAdapterPosition(view);
        final Dispenser selectedDispenser = dispensers.get(listPosition);

        GenericDialog genericDialog = new GenericDialog("Eliminar dispensario", "¿Esta " +
                "seguro que desea remover el dispensario seleccionado?", new Runnable()
        {
            @Override
            public void run()
            {
                dispenserToRemove = listPosition;
                //Eliminar producto
                stationManager.deleteDispenser(selectedDispenser.getNumber());
                //Mostrar progressBar
                progressBar.setVisibility(View.VISIBLE);
                //Ocultar lista
                recyclerView.setVisibility(View.GONE);
            }
        }, new Runnable()
        {
            @Override
            public void run()
            {
                //Nada
            }
        }, getContext());

        genericDialog.setPositiveText("Sí");
        genericDialog.setNegativeText("No");
        genericDialog.show();
    }

    private void onDispenserModified()
    {
        // Actualizar archivo .json
        StationFileManager.syncDispensersInStationFile(StationManager.dispensersListToJsonArray(dispensers), getContext());
        manageDispensersInterface.syncLocalServer();
    }

    private void onDispenserRemoved()
    {
        // Remover dispensario de la lista
        dispensers.remove(dispenserToRemove);

        // Notificar adaptador
        dispensersAdapter.notifyItemRemoved(dispenserToRemove);

        // Mostrar cambios en lista
        onDrawnDispensers();

        // Actualizar archivo .json
        StationFileManager.syncDispensersInStationFile(StationManager.dispensersListToJsonArray(dispensers), getContext());
        manageDispensersInterface.syncLocalServer();
    }

    private void onDrawnDispensers()
    {
        if (dispensers.isEmpty())
        {
            noDispensers.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            noDispensers.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            //Acomodar posiciones de carga
            Collections.sort(dispensers, new Comparator<Dispenser>()
            {
                @Override
                public int compare(Dispenser dispenser, Dispenser t1)
                {
                    return Integer.compare(dispenser.getNumber(), t1.getNumber());
                }
            });
        }
    }

    private void addNewDispensers()
    {
        // Incrementar pares
        for (int i = 0; i < loadingPositionsToAdd.size(); i += 2)
        {
            // Obtener nuevas posiciones de carga de lista "loadingPositionsToAdd"
            LoadingPosition side1 = loadingPositionsToAdd.get(i);
            LoadingPosition side2 = loadingPositionsToAdd.get(i + 1);

            // Obtener número de dispensario
            // Numero de dispensario = (pcLado1 + 1)/2
            int dispN = (side1.getStationSide() + 1) / 2;

            //Crear nuevo dispensario
            Dispenser dispenser = new Dispenser();
            dispenser.setNumber(dispN);
            dispenser.setSide1(side1);
            dispenser.setSide2(side2);

            //Agregar dispensario a la lista global
            dispensers.add(dispenser);
        }
    }

    private void registerDispensers()
    {
        recyclerView.setVisibility(View.GONE);
        noDispensers.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        stationManager.addDispensers(dispensersToAdd);
    }

    public interface ManageDispensersInterface
    {
        void syncLocalServer();
    }
}
