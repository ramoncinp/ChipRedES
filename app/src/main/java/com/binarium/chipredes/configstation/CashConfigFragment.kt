package com.binarium.chipredes.configstation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.binarium.chipredes.databinding.FragmentCashConfigBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CashConfigFragment : DialogFragment() {
    val viewModel: CashConfigViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val binding = FragmentCashConfigBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.switchMaterial.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setNewConfigValue(isChecked)
        }

        viewModel.isActivated.observe(viewLifecycleOwner) { isChecked ->
            binding.switchMaterial.isChecked = isChecked
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
    }
}