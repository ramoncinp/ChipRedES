package com.binarium.chipredes.configstation;

import android.app.ProgressDialog;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.R;
import com.binarium.chipredes.config.GetStationReference;

import org.json.JSONException;
import org.json.JSONObject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ConfigStation extends AppCompatActivity
{
    public static final String TAG = ConfigStation.class.getSimpleName();

    public static final int GENERAL_STATION_INFO = 0;
    public static final int MANAGE_PUMPERS = 1;
    public static final int MANAGE_PRODUCTS = 2;
    public static final int MANAGE_DISPENSERS = 3;

    public static final int PLACE_PICKER_REQUEST = 1;
    public static final int GET_STATION_REFERENCE = 2;

    private GeneralData generalData;
    private FragmentManager fragmentManager;
    private StationManager stationManager;
    private boolean inMainFragment;
    private boolean newStation = false;

    private ProgressBar progressBar;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_station);

        //Agregar flecha para salir de actividad
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Obtener dato para saber si es una estacion nueva
        if (getIntent().hasExtra(ChipREDConstants.CONFIG_NEW_STATION))
        {
            newStation = getIntent().getBooleanExtra(ChipREDConstants.CONFIG_NEW_STATION, false);
        }

        //Obtener fragmentManager
        fragmentManager = getSupportFragmentManager();

        //Obtener progressBar
        progressBar = findViewById(R.id.progress_bar);

        //Inicializar objeto que sincroniza servidor local
        initStationManager();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GET_STATION_REFERENCE)
        {
            //Refrescar el obtener datos generales
            stationManager.getGeneralData(null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_config_station, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int itemId = item.getItemId();
        if (itemId == R.id.sync_local_server) {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(this);
            }
            progressDialog.setMessage("Sincronizando servidor local...");
            progressDialog.show();

            stationManager.syncLocalServer();
        } else if (itemId == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setStationElementsFragment()
    {
        StationElements stationElements = new StationElements();
        stationElements.setStationElementsInterface(new StationElements.StationElementsInterface()
        {
            @Override
            public void onListElementSelected(int listElement)
            {
                switch (listElement)
                {
                    case GENERAL_STATION_INFO:
                        setGeneralStationInfoFragment();
                        break;

                    case MANAGE_PUMPERS:
                        setManagePumpersFragment();
                        break;

                    case MANAGE_PRODUCTS:
                        setManageProductsFragment();
                        break;

                    case MANAGE_DISPENSERS:
                        setManageDispensers();
                        break;
                }
            }

            @Override
            public void onReadyButtonPressed()
            {
                Intent intent = new Intent();
                setResult(GetStationReference.SET_STATION_ELEMENTS_OK, intent);
                finish();
            }
        });

        fragmentManager.beginTransaction().replace(R.id.config_station_frame_layout,
                stationElements).addToBackStack(null).commit();

        inMainFragment = true;
        setTitle("Datos de estación");
    }

    private void setGeneralStationInfoFragment()
    {
        generalData = new GeneralData();
        generalData.setGeneralDataInterface(new GeneralData.GeneralDataInterface()
        {
            @Override
            public void returnToPreviousFragment()
            {
                onBackPressed();
            }

            @Override
            public void showProgress()
            {
                if (progressDialog == null)
                {
                    progressDialog = new ProgressDialog(ConfigStation.this);
                    progressDialog.setCancelable(true);
                    progressDialog.setMessage("Cargando...");
                }
                progressDialog.show();
            }
        });

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().addToBackStack(null).replace(R.id
                .config_station_frame_layout, generalData).commit();

        inMainFragment = false;
        setTitle("Datos generales");
    }

    private void setManagePumpersFragment()
    {
        ManagePumpers managePumpers = new ManagePumpers();
        managePumpers.setManagePumpersInterface(new ManagePumpers.ManagePumpersInterface()
        {
            @Override
            public void syncLocalServer()
            {
                stationManager.syncLocalServer();
            }
        });

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().addToBackStack(null).replace(R.id
                .config_station_frame_layout, managePumpers).commit();

        inMainFragment = false;
        setTitle("Despachadores");
    }

    private void setManageProductsFragment()
    {
        ManageProducts manageProducts = new ManageProducts();
        manageProducts.setManageProductsInterface(new ManageProducts.ManageProductsInterface()
        {
            @Override
            public void syncLocalServer()
            {
                stationManager.syncLocalServer();
            }
        });

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().addToBackStack(null).replace(R.id
                .config_station_frame_layout, manageProducts).commit();

        inMainFragment = false;
        setTitle("Productos");
    }

    private void setManageDispensers()
    {
        ManageDispensers manageDispensers = new ManageDispensers();
        manageDispensers.setManageDispensersInterface(new ManageDispensers.ManageDispensersInterface()
        {
            @Override
            public void syncLocalServer()
            {
                stationManager.syncLocalServer();
            }
        });

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().addToBackStack(null).replace(R.id
                .config_station_frame_layout, manageDispensers).commit();

        inMainFragment = false;
        setTitle("Dispensarios");
    }

    private void initStationManager()
    {
        stationManager = new StationManager(this, new StationManager.OnResponse()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                if (ConfigStation.this.isFinishing()) return;

                if (webServiceN == StationManager.GET_GENERAL_DATA)
                {
                    progressBar.setVisibility(View.GONE);

                    //Error con idMongo
                    if (!newStation) showMongoErrorDialog();
                    else setStationElementsFragment(); //Iniciar primer fragment
                }
                else
                {
                    if (progressDialog != null)
                    {
                        progressDialog.dismiss();
                    }
                    Toast.makeText(ConfigStation.this, crMessage, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                if (ConfigStation.this.isFinishing()) return;

                if (webServiceN == StationManager.GET_GENERAL_DATA)
                {
                    Log.d(TAG, "Error de ChipRED manager -> " + errorMessage);

                    GenericDialog genericDialog = new GenericDialog("Error",
                            errorMessage, new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            ConfigStation.this.finish();
                        }
                    }, null, ConfigStation.this);

                    genericDialog.setPositiveText("Regresar");
                    genericDialog.show();
                }
                else
                {
                    if (progressDialog != null)
                    {
                        progressDialog.dismiss();
                    }
                    Toast.makeText(ConfigStation.this, errorMessage, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                if (ConfigStation.this.isFinishing()) return;

                if (webServiceN == StationManager.GET_GENERAL_DATA)
                {
                    progressBar.setVisibility(View.GONE);

                    //Se confirmó que el mongoId es válido
                    //Iniciar primer fragment
                    setStationElementsFragment();
                }
                else
                {
                    if (progressDialog != null)
                    {
                        progressDialog.dismiss();
                    }

                    try
                    {
                        Log.d(TAG, response.getString("message"));
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });

        //Probar que el idMongo sea de una estación de la colección de estaciones en Wolke
        stationManager.getGeneralData(null);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (stationManager != null)
        {
            stationManager.cancellAllRequest();
        }
    }

    @Override
    public void onBackPressed()
    {
        if (inMainFragment)
        {
            finish();
        }
        else
        {
            fragmentManager.popBackStackImmediate();
            inMainFragment = true;
            setTitle("Datos de estación");
        }
    }

    private void showMongoErrorDialog()
    {
        final GenericDialog genericDialog = new GenericDialog("Aviso",
                getResources().getString(R.string.mongo_id_error_dialog), new Runnable()
        {
            @Override
            public void run()
            {
                //Hacer logIn de estación
                Intent intent = new Intent(ConfigStation.this, GetStationReferenceActivity.class);
                startActivityForResult(intent, GET_STATION_REFERENCE);
            }
        }, new Runnable()
        {
            @Override
            public void run()
            {
                finish();
            }
        }, this);

        genericDialog.setPositiveText("Ingresar");
        genericDialog.setNegativeText("Regresar");
        genericDialog.setCancelable(false);
        genericDialog.showCustomDialog();
    }
}
