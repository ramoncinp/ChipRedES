package com.binarium.chipredes.configstation;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.binarium.chipredes.R;
import com.binarium.chipredes.config.GetStationReference;

public class GetStationReferenceActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_station_reference);

        setFragment();
    }

    private void setFragment()
    {
        GetStationReference fragment = new GetStationReference();

        //Agregar argumentos
        Bundle args = new Bundle();
        args.putInt(GetStationReference.ORIGIN, GetStationReference.FROM_SETTINGS);
        fragment.setArguments(args);

        fragment.setGetStationInterface(new GetStationReference.GetStationInterface()
        {
            @Override
            public void onStationGotten()
            {
                Intent intent = new Intent();
                setResult(0, intent);
                finish();
            }
        });

        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, fragment).commit();
    }
}
