package com.binarium.chipredes.configstation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binarium.chipredes.R;
import com.binarium.chipredes.dispenser.Dispenser;

import java.util.ArrayList;
import java.util.Locale;

public class DispenserCardAdapter extends RecyclerView.Adapter<DispenserCardAdapter
        .DispenserCardViewHolder> implements View.OnClickListener, View.OnLongClickListener
{
    private ArrayList<Dispenser> dispensers;
    private View.OnClickListener onClickListener;
    private View.OnLongClickListener longClickListener;

    DispenserCardAdapter(ArrayList<Dispenser> dispensers)
    {
        this.dispensers = dispensers;
    }

    @Override
    public int getItemCount()
    {
        return dispensers.size();
    }

    @NonNull
    @Override
    public DispenserCardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dispenser_model,
                viewGroup, false);

        v.setOnClickListener(this);
        v.setOnLongClickListener(this);

        return new DispenserCardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DispenserCardViewHolder dispenserCardViewHolder, int i)
    {
        // Obtener dispensario
        Dispenser dispenser = dispensers.get(i);

        // Crear nombre de dispensario
        String dispNumberStr = String.format(Locale.getDefault(), "Dispensario %1$d",
                dispenser.getNumber());

        // Mostrar nombre de dispensario
        dispenserCardViewHolder.dispNumbTv.setText(dispNumberStr);
    }

    void setOnClickListener(View.OnClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    void setLongClickListener(View.OnLongClickListener longClickListener)
    {
        this.longClickListener = longClickListener;
    }

    @Override
    public void onClick(View view)
    {
        if (onClickListener != null)
        {
            onClickListener.onClick(view);
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if (longClickListener != null)
        {
            longClickListener.onLongClick(view);
        }
        return true;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    static class DispenserCardViewHolder extends RecyclerView.ViewHolder
    {
        private TextView dispNumbTv;

        DispenserCardViewHolder(@NonNull View itemView)
        {
            super(itemView);
            dispNumbTv = itemView.findViewById(R.id.dispenser_number_tv);
        }
    }
}
