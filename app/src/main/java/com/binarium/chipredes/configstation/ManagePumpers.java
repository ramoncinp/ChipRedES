package com.binarium.chipredes.configstation;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.Pumper;
import com.binarium.chipredes.R;
import com.binarium.chipredes.adapters.EmaxPumperAdapter;
import com.binarium.chipredes.configstation.pumpers.ManagePumpersViewModel;
import com.binarium.chipredes.emax.model.EmaxPumper;
import com.binarium.chipredes.utils.AsciiInputFilter;
import com.binarium.chipredes.utils.DebouncedOnClickListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ManagePumpers extends Fragment implements StationManager.OnResponse {
    //Constantes
    private static final int MODIFY_PUMPER_INFO = 0;
    private static final int REMOVE_PUMPER = 1;

    //Variables
    private boolean addOdModify = true; //True para agregar, false para modificar
    private int positionToDeleteOrModify;

    //Views
    private Dialog addPumperDialog;
    private ProgressBar progressBar;
    private RecyclerView pumpersList;
    private TextView noPumpers;

    //Objetos
    private ManagePumpersInterface managePumpersInterface;
    private Pumper pumperToAddOrModify; //Despachador que se va a agregar a la lista
    private PumperCardAdapter pumperCardAdapter;
    private StationManager stationManager;
    private ManagePumpersViewModel viewModel;

    public ManagePumpers() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View content = inflater.inflate(R.layout.fragment_manage_pumpers_old, container, false);
        pumpersList = content.findViewById(R.id.pumpers_list);
        noPumpers = content.findViewById(R.id.no_pumpers);
        progressBar = content.findViewById(R.id.progress_bar);

        FloatingActionButton fab = content.findViewById(R.id.add_pumper);
        fab.setOnClickListener(new DebouncedOnClickListener(800) {
            @Override
            public void onDebouncedClick(View v) {
                showAddPumperOptions();
            }
        });

        viewModel = new ViewModelProvider(this).get(ManagePumpersViewModel.class);

        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getPumpers();
        viewModel.getEmaxPumpersFromDb();
    }

    public void setManagePumpersInterface(ManagePumpersInterface managePumpersInterface) {
        this.managePumpersInterface = managePumpersInterface;
    }

    //Métodos para comunicación con servidor
    @Override
    public void chipRedMessage(String crMessage, int webServiceN) {
        progressBar.setVisibility(View.GONE);
        parsePumpersResponse(null);
        Toast.makeText(getContext(), crMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void chipRedError(String errorMessage, int webServiceN) {
        progressBar.setVisibility(View.GONE);
        GenericDialog genericDialog = new GenericDialog("Error", errorMessage, () -> {
            if (getActivity() != null) getActivity().onBackPressed();
        }, null, getContext());

        genericDialog.setPositiveText("Sí");
        genericDialog.setNegativeText("No");
        genericDialog.show();
    }

    @Override
    public void showResponse(JSONObject response, int webServiceN) {
        try {
            switch (webServiceN) {
                case StationManager.GET_PUMPERS:
                    parsePumpersResponse(response);
                    break;

                case StationManager.ADD_PUMPER:
                    //Mostrar despachador agregado
                    onPumperAdded();
                    //Notificar que se pudo agregar a la bd
                    ChipREDConstants.showSnackBarMessage(response.getString("message"),
                            getActivity());
                    break;

                case StationManager.DELETE_PUMPER:
                    //Mostrar lista de despachadores
                    pumpersList.setVisibility(View.VISIBLE);
                    //Eliminar despachador de la lista
                    onPumperRemoved();
                    break;

                case StationManager.MODIFY_PUMPER:
                    //Notificar que se pudo modificar en la bd
                    ChipREDConstants.showSnackBarMessage(response.getString("message"),
                            getActivity());
                    //Mostrar lista de despachadores
                    pumpersList.setVisibility(View.VISIBLE);
                    //Reemplazar con objeto ya modificado
                    viewModel.getChipredPumpers().set(positionToDeleteOrModify, pumperToAddOrModify);
                    //Guardar cambios para la lista
                    pumperCardAdapter.notifyDataSetChanged();

                    //Actualizar archivo JSON
                    StationFileManager.syncPumpersInStationFile(viewModel.getChipredPumpers(), getContext());
                    //Sincronizar servidor local
                    managePumpersInterface.syncLocalServer();

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        progressBar.setVisibility(View.GONE);
    }

    private void showAddPumperDialog(@Nullable Pumper pumper, final View selectedView) {
        //Filtro para mayúsculas
        InputFilter[] allCapsFilter = new InputFilter[]{new InputFilter.AllCaps(),
                new AsciiInputFilter()};

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Agregar despachador");

        View view = getLayoutInflater().inflate(R.layout.add_pumper_layout, null);
        //Obtener EditText's
        final MaterialEditText name = view.findViewById(R.id.add_pumper_name);
        final MaterialEditText emaxId = view.findViewById(R.id.add_pumper_emax_id);
        final MaterialEditText tag = view.findViewById(R.id.add_pumper_tag);
        final MaterialEditText nip = view.findViewById(R.id.add_pumper_nip);
        final MaterialEditText initials = view.findViewById(R.id.add_pumper_initials);

        //Agregar filtros para sólo mayúsculas a los editText que los necesitan
        name.setFilters(allCapsFilter);
        tag.setFilters(allCapsFilter);
        initials.setFilters(allCapsFilter);

        if (pumper != null) {
            name.setText(pumper.getName());
            emaxId.setText(pumper.getEmaxId());
            tag.setText(pumper.getTagId());
            nip.setText(pumper.getNip());
            initials.setText(pumper.getNameInitials());

            emaxId.setEnabled(false);
            addOdModify = false;
        } else {
            addOdModify = true;
        }

        //Obtener botones
        Button cancelButton = view.findViewById(R.id.add_pumper_close_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPumperDialog.dismiss();
            }
        });

        Button submitButton = view.findViewById(R.id.add_pumper_submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Crear validador
                METValidator emptyValidator = new METValidator("Campo obligatorio") {
                    @Override
                    public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                        return !isEmpty;
                    }
                };

                //Validar información
                boolean validInfo;
                validInfo = name.validateWith(emptyValidator);
                validInfo &= emaxId.validateWith(emptyValidator);
                validInfo &= tag.validateWith(emptyValidator);
                validInfo &= nip.validateWith(emptyValidator);
                validInfo &= initials.validateWith(emptyValidator);

                //Si la info es válida
                if (validInfo) {
                    //Crear objeto despachador
                    Pumper mPumper = new Pumper();
                    mPumper.setName(name.getText().toString());
                    mPumper.setEmaxId(emaxId.getText().toString());
                    mPumper.setTagId(tag.getText().toString());
                    mPumper.setNip(nip.getText().toString());
                    mPumper.setNameInitials(initials.getText().toString());

                    //Si el objeto "Pumper" es nulo, es un nuevo despachador
                    if (addOdModify) {
                        stationManager.addPumper(mPumper);
                    } else {
                        stationManager.modifyPumper(mPumper);
                        //Obtener posicion en RecyclerView
                        positionToDeleteOrModify =
                                pumpersList.getChildAdapterPosition(selectedView);
                    }
                    pumperToAddOrModify = mPumper;

                    //Ocultar recyclerView
                    pumpersList.setVisibility(View.GONE);
                    //Mostrar progressBar
                    progressBar.setVisibility(View.VISIBLE);

                    //Quitar diálogo
                    addPumperDialog.dismiss();
                }
            }
        });

        builder.setView(view);

        addPumperDialog = builder.create();
        addPumperDialog.show();
    }

    private void getPumpers() {
        stationManager = new StationManager(getContext(), this);
        stationManager.getStationPumpers();
    }

    private void parsePumpersResponse(@Nullable JSONObject response) {
        //Inicializar lista
        viewModel.setChipredPumpers(new ArrayList<>());

        //Obtener lista de despachadores
        try {
            JSONArray pumpersJSON = response.getJSONObject("data").getJSONArray("despachadores");

            for (int i = 0; i < pumpersJSON.length(); i++) {
                JSONObject jsonPumper = pumpersJSON.getJSONObject(i);

                Pumper pumper = new Pumper();
                pumper.setName(jsonPumper.getString("nombre"));
                pumper.setEmaxId(jsonPumper.getString("id_emax"));
                pumper.setTagId(jsonPumper.getString("tag"));
                pumper.setNip(jsonPumper.getString("nip"));
                pumper.setNameInitials(jsonPumper.getString("iniciales"));
                viewModel.getChipredPumpers().add(pumper);
            }
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }


        pumperCardAdapter = new PumperCardAdapter(viewModel.getChipredPumpers());
        pumperCardAdapter.setOnClickListener(new DebouncedOnClickListener(800) {
            @Override
            public void onDebouncedClick(View v) {
                pumperOptionSelected(v, MODIFY_PUMPER_INFO);
            }
        });

        pumperCardAdapter.setLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                pumperOptionSelected(view, REMOVE_PUMPER);
                return true;
            }
        });

        pumpersList.setAdapter(pumperCardAdapter);
        pumpersList.setLayoutManager(new GridLayoutManager(getContext(),
                getResources().getInteger(R.integer.station_elements_cards_columns_number)));

        if (viewModel.getChipredPumpers().isEmpty()) {
            pumpersList.setVisibility(View.GONE);
            noPumpers.setVisibility(View.VISIBLE);
        } else {
            pumpersList.setVisibility(View.VISIBLE);
            noPumpers.setVisibility(View.GONE);
        }

        Collections.sort(viewModel.getChipredPumpers(), new PumperComparator());
    }

    private void pumperOptionSelected(final View view, int op) {
        final int position = pumpersList.getChildAdapterPosition(view);
        final Pumper pumper = viewModel.getChipredPumpers().get(position);

        switch (op) {
            case MODIFY_PUMPER_INFO:
                showAddPumperDialog(pumper, view);
                break;

            case REMOVE_PUMPER:
                GenericDialog genericDialog = new GenericDialog("Eliminar despachador", "¿Esta "
                        + "seguro que desea remover al despachador seleccionado?", () -> {
                    //Pedir eliminar el despachador seleccionado
                    stationManager.deletePumper(pumper);

                    //Definir el despachador que se va a eliminar
                    positionToDeleteOrModify = position;

                    //Mostrar progressBar
                    progressBar.setVisibility(View.VISIBLE);
                    //Ocultar lista
                    pumpersList.setVisibility(View.GONE);
                }, () -> {
                    //Nada
                }, getContext());

                genericDialog.setPositiveText("Sí");
                genericDialog.setNegativeText("No");
                genericDialog.show();

                break;
        }
    }

    private void onPumperAdded() {
        //Agregar despachador a la lista
        viewModel.getChipredPumpers().add(pumperToAddOrModify);
        pumperCardAdapter.notifyItemInserted(viewModel.getChipredPumpers().size() - 1);

        //Agregar despachador a archivo JSON
        StationFileManager.syncPumpersInStationFile(viewModel.getChipredPumpers(), getContext());

        //Sincronizar servidor local
        managePumpersInterface.syncLocalServer();

        //Mostrar lista de despachadores
        pumpersList.setVisibility(View.VISIBLE);
        noPumpers.setVisibility(View.GONE);
    }

    private void onPumperRemoved() {
        viewModel.getChipredPumpers().remove(positionToDeleteOrModify);
        pumperCardAdapter.notifyItemRemoved(positionToDeleteOrModify);
        pumperCardAdapter.notifyItemRangeChanged(positionToDeleteOrModify, viewModel.getChipredPumpers().size());

        //Agregar despachador a archivo JSON
        StationFileManager.syncPumpersInStationFile(viewModel.getChipredPumpers(), getContext());

        //Sincronizar servidor local
        managePumpersInterface.syncLocalServer();

        if (viewModel.getChipredPumpers().isEmpty()) {
            noPumpers.setVisibility(View.VISIBLE);
            pumpersList.setVisibility(View.GONE);
        }
    }

    private void showAddPumperOptions() {
        viewModel.filterPumpers();

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Seleccione opción");

        // Obtener layout
        final View content = getLayoutInflater().inflate(R.layout.dialog_add_pumper, null);
        builder.setView(content);

        // Obtener referencias de views
        final TextView addNewPumper = content.findViewById(R.id.add_new_pumper_tv);
        final RecyclerView emaxPumpersList = content.findViewById(R.id.pumpers_list);

        // Crear adaptador
        final EmaxPumperAdapter adapter = new EmaxPumperAdapter(viewModel.getFilteredPumpers());
        emaxPumpersList.setAdapter(adapter);

        // Mostrar diálogo
        final Dialog dialog = builder.create();
        dialog.show();

        addNewPumper.setOnClickListener(v -> {
            showAddPumperDialog(null, v);
            dialog.dismiss();
        });

        adapter.setListener(v -> {
            final int position = emaxPumpersList.getChildAdapterPosition(v);
            final EmaxPumper selectedPumper = viewModel.getFilteredPumpers().get(position);

            if (viewModel.isPumperRegistered(selectedPumper)) {
                final GenericDialog genericDialog = new GenericDialog(
                        "Error",
                        "Ya hay un despachador registrado con el mismo id o tag",
                        null,
                        () -> {
                            // NADA
                        },
                        getContext()
                );
                genericDialog.show();
            } else {
                addEmaxPumperToChipRed(selectedPumper);
            }
            dialog.dismiss();
        });
    }

    private void addEmaxPumperToChipRed(final EmaxPumper emaxPumper) {
        //Crear objeto despachador
        Pumper mPumper = new Pumper();
        mPumper.setName(emaxPumper.getNombre());
        mPumper.setEmaxId(emaxPumper.getId());
        mPumper.setTagId(emaxPumper.getTag());
        mPumper.setNip("1234");
        mPumper.setNameInitials(emaxPumper.getIniciales());

        //Si el objeto "Pumper" es nulo, es un nuevo despachador
        stationManager.addPumper(mPumper);
        pumperToAddOrModify = mPumper;

        //Ocultar recyclerView
        pumpersList.setVisibility(View.GONE);
        //Mostrar progressBar
        progressBar.setVisibility(View.VISIBLE);
    }

    public interface ManagePumpersInterface {
        void syncLocalServer();
    }

    private static class PumperComparator implements Comparator<Pumper> {
        public int compare(Pumper a, Pumper b) {
            String nombreA = a.getName();
            String nombreB = b.getName();

            return nombreA.compareTo(nombreB);
        }
    }
}
