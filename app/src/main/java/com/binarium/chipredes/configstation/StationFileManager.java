package com.binarium.chipredes.configstation;

import android.content.Context;

import androidx.annotation.NonNull;

import android.util.Log;

import com.binarium.chipredes.Product;
import com.binarium.chipredes.Pumper;
import com.binarium.chipredes.dispenser.Dispenser;
import com.binarium.chipredes.disppreset.DispIpInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;

public class StationFileManager
{
    private static final String TAG = StationFileManager.class.getSimpleName();

    static JSONObject getStation(Context context)
    {
        //Obtener el PATH
        String filePath = context.getFilesDir() + "/station.json";

        //Obtener archivo
        File stationFile = new File(filePath);

        //Leer archivo
        StringBuilder stringBuilder = new StringBuilder();
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(stationFile));
            String line;

            while ((line = br.readLine()) != null)
            {
                stringBuilder.append(line);
            }

            br.close();

            //Convertir contenido a JSON
            return new JSONObject(stringBuilder.toString());
        }
        catch (IOException | JSONException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static JSONObject getTokenCashConfig(Context context)
    {
        // Obtener JSON de estacion
        JSONObject station = getStation(context);

        try
        {
            if (station != null)
            {
                return station.getJSONObject("fidelizacion").getJSONObject(
                        "tokencash");
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static String getStationCountry(Context context)
    {
        //Obtener archivo de la estacion
        JSONObject station = getStation(context);

        //Verificar que no sea nula
        if (station != null)
        {
            //Buscar el objeto direccion
            if (station.has("direccion"))
            {
                try
                {
                    //Obtener el objeto "direccion"
                    JSONObject addr = station.getJSONObject("direccion");

                    if (addr.has("pais"))
                    {
                        //Retornar pais
                        Log.d(TAG, "Retornando valor de pais de archivo local " + addr.getString(
                                "pais"));
                        return addr.getString("pais");
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    Log.d(TAG, e.getMessage());
                }
            }
        }

        return "";
    }

    public static boolean saveStationGeneralData(JSONObject station, Context context)
    {
        //Obtener el PATH
        String filePath = context.getFilesDir() + "/station.json";

        //Obtener archivo
        File stationFile = new File(filePath);

        try
        {
            Writer output = new BufferedWriter(new FileWriter(stationFile));
            output.write(station.toString());
            output.close();
            return true;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    static void syncGeneralData(JSONObject generalData, Context context)
    {
        //Obtener archivo de la estacion
        JSONObject station = getStation(context);

        //Evaluar que no sea nulo
        if (station != null)
        {
            //Actualizar cada una de los keys de datos generales
            Iterator<String> iter = generalData.keys();
            while (iter.hasNext())
            {
                String key = iter.next();
                try
                {
                    Object value = generalData.get(key);
                    //Validar que la key se encuentre en el archivo local
                    if (station.has(key))
                    {
                        //Remover para poder actualizar
                        station.remove(key);
                    }

                    //Agregar dato válido
                    station.put(key, value);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }

            //Actualizar documento
            saveStationGeneralData(station, context);
        }
    }

    static void syncPumpersInStationFile(ArrayList<Pumper> pumpers, Context context)
    {
        //Obtener archivo de la estacion
        JSONObject station = getStation(context);

        //Evaluar que no sea nulo
        if (station != null)
        {
            try
            {
                //Obtener despachadores si esque existen
                JSONArray pumpersArray;

                //Evaluar que existan despachadores en el documento
                if (station.has("despachadores"))
                {
                    pumpersArray = station.getJSONArray("despachadores");
                    //Remover para poder editarlo
                    station.remove("despachadores");

                    if (pumpers.isEmpty())
                    {
                        //Actualizar documento
                        saveStationGeneralData(station, context);
                        return;
                    }
                }

                //Crear nuevo arreglo json para objetos "Despachadores"
                pumpersArray = new JSONArray();

                for (Pumper pumper : pumpers)
                {
                    //Agregar despachador a arreglo
                    pumpersArray.put(pumper.toJson());
                }

                //Agregar al objeto principal
                station.put("despachadores", pumpersArray);

                //Actualizar documento
                saveStationGeneralData(station, context);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    static void syncProductsInStationFile(ArrayList<Product> products, Context context)
    {
        //Obtener archivo de la estacion
        JSONObject station = getStation(context);

        //Evaluar que no sea nulo
        if (station != null)
        {
            try
            {
                //Obtener productos si esque existen
                JSONArray productsArray;

                //Evaluar que existan productos en el documento
                if (station.has("productos"))
                {
                    productsArray = station.getJSONArray("productos");
                    //Remover para poder editarlo
                    station.remove("productos");

                    if (products.isEmpty())
                    {
                        //Actualizar documento
                        saveStationGeneralData(station, context);
                        return;
                    }
                }

                //Crear nuevo arreglo json para objetos "Productos"
                productsArray = new JSONArray();

                for (Product product : products)
                {
                    //Agregar despachador a arreglo
                    productsArray.put(product.toJson());
                }

                //Agregar al objeto principal
                station.put("productos", productsArray);

                //Actualizar documento
                saveStationGeneralData(station, context);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    public static ArrayList<Dispenser> getStationDispensers(Context context)
    {
        // Crear lista a retornar
        ArrayList<Dispenser> dispensers = new ArrayList<>();

        //Obtener archivo de la estacion
        JSONObject station = getStation(context);

        //Evaluar que no sea nulo
        if (station != null)
        {
            try
            {
                //Evaluar que existan despachadores en el documento
                if (station.has("dispensarios"))
                {
                    // Obtener arreglo de dispensarios
                    JSONArray dispsArray = station.getJSONArray("dispensarios");

                    // Iterar en el arreglo
                    for (int i = 0; i < dispsArray.length(); i++)
                    {
                        // Obtener objeto JSON
                        JSONObject jsonDisp = dispsArray.getJSONObject(i);

                        // Obtener número de dispensario
                        int dispNumber = jsonDisp.getInt("numero_dispensario");

                        // Crear dispensarios
                        Dispenser dispenser = new Dispenser();
                        dispenser.setNumber(dispNumber);

                        // Agregar a la lista
                        dispensers.add(dispenser);
                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return dispensers;
    }

    public static boolean saveDispensersIps(Context context, JSONArray ips)
    {
        //Obtener el PATH
        String filePath = context.getFilesDir() + "/dispensers_ips.json";

        //Obtener archivo
        File dispIpsFile = new File(filePath);

        try
        {
            Writer output = new BufferedWriter(new FileWriter(dispIpsFile));
            output.write(ips.toString());
            output.close();
            return true;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public static JSONArray getDispensersIps(Context context)
    {
        JSONArray dispensersIps = new JSONArray();

        //Obtener el PATH
        String filePath = context.getFilesDir() + "/dispensers_ips.json";

        //Obtener archivo
        File dispIpsFile = new File(filePath);

        //Leer archivo
        StringBuilder stringBuilder = new StringBuilder();
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(dispIpsFile));
            String line;

            while ((line = br.readLine()) != null)
            {
                stringBuilder.append(line);
            }
            br.close();

            //Convertir contenido a JSONArray
            if (stringBuilder.length() > 0)
            {
                dispensersIps = new JSONArray(stringBuilder.toString());
            }
        }
        catch (IOException | JSONException e)
        {
            e.printStackTrace();
        }

        return dispensersIps;
    }

    public static String getDispenserIp(Context context, int loadingPosition)
    {
        String dispIp = null;

        // Convertir posicion de carga a dispensario
        int dispNumber = loadingPosition % 2 == 0 ? loadingPosition / 2 : (loadingPosition + 1) / 2;

        // Obtener arreglo con relación de dispensarios - ip's
        JSONArray dispsArray = getDispensersIps(context);

        try
        {
            // Buscar ip del dispensario
            for (int i = 0; i < dispsArray.length(); i++)
            {
                JSONObject disp = dispsArray.getJSONObject(i);
                int jsonDispNumber = disp.getInt("id");
                String ip = disp.getString("ip");

                if (dispNumber == jsonDispNumber)
                {
                    // Asignar resultado
                    dispIp = ip;
                    break;
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return dispIp;
    }

    public static ArrayList<Product> getStationProducts(Context context)
    {
        ArrayList<Product> productsList = new ArrayList<>();
        JSONArray productsArray;

        //Obtener archivo de la estacion
        JSONObject station = getStation(context);

        //Evaluar que no sea nulo
        if (station != null)
        {
            try
            {
                //Evaluar que existan productos en el documento
                if (station.has("productos"))
                {
                    // Obtener arreglo de productos
                    productsArray = station.getJSONArray("productos");

                    try
                    {
                        //Obtener cada producto
                        for (int i = 0; i < productsArray.length(); i++)
                        {
                            //Obtener objeto
                            JSONObject mProduct = productsArray.getJSONObject(i);
                            //Obtener atributos
                            Product product = new Product();
                            if (mProduct.has("descripcion"))
                                product.setDescription(mProduct.getString("descripcion"));

                            if (mProduct.has("id_producto"))
                                product.setId(mProduct.getString("id_producto"));

                            if (mProduct.has("precio_litro"))
                                product.setLiterPrice(mProduct.getDouble("precio_litro"));

                            if (mProduct.has("color"))
                                product.setColor(mProduct.getString("color"));

                            productsList.add(product);
                        }
                    }
                    catch (JSONException | NullPointerException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return productsList;
    }

    static void syncDispensersInStationFile(JSONArray dispensersToAdd, Context context)
    {
        //Obtener archivo de la estacion
        JSONObject station = getStation(context);

        //Evaluar que no sea nulo
        if (station != null)
        {
            try
            {
                //Evaluar que existan despachadores en el documento
                if (station.has("dispensarios"))
                {
                    //Remover para poder editarlo
                    station.remove("dispensarios");

                    if (dispensersToAdd.length() == 0)
                    {
                        //Actualizar documento
                        saveStationGeneralData(station, context);
                        return;
                    }
                }

                //Obtener despachadores si esque existen
                JSONArray dispensers = new JSONArray();

                //Concatenar arreglos
                for (int i = 0; i < dispensersToAdd.length(); i++)
                {
                    dispensers.put(dispensersToAdd.get(i));
                }

                //Agregar al objeto principal
                station.put("dispensarios", dispensers);

                //Actualizar documento
                saveStationGeneralData(station, context);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    public static void syncTokenCashInFile(@NonNull JSONObject tokenCashObject, Context context)
    {
        //Obtener archivo de la estacion
        JSONObject station = getStation(context);

        //Evaluar que no sea nulo
        if (station != null)
        {
            try
            {
                //Evaluar que existan despachadores en el documento
                if (station.has("fidelizacion"))
                {
                    //Remover para poder editarlo
                    station.remove("fidelizacion");
                }

                //Agregar al objeto principal
                station.put("fidelizacion", tokenCashObject);

                //Actualizar documento
                saveStationGeneralData(station, context);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }
}
