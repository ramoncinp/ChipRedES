package com.binarium.chipredes.configstation;

public class StationElement
{
    private String description;
    private int image;
    private boolean complete;

    public StationElement(String description, int image)
    {
        this.description = description;
        this.image = image;
    }

    public String getDescription()
    {
        return description;
    }

    public int getImage()
    {
        return image;
    }

    public boolean isComplete()
    {
        return complete;
    }

    public void setComplete(boolean complete)
    {
        this.complete = complete;
    }
}
