package com.binarium.chipredes.comparators;

import com.binarium.chipredes.LoadingPosition;

import java.util.Comparator;

public class PumpComparator implements Comparator<LoadingPosition>
{
    @Override
    public int compare(LoadingPosition loadingPosition, LoadingPosition t1)
    {
        int sideToDisplay1 =
                loadingPosition.getUserStationSide() == 0 ?
                        loadingPosition.getStationSide() :
                        loadingPosition.getUserStationSide();

        int sideToDisplay2 =
                t1.getUserStationSide() == 0 ?
                        t1.getStationSide() :
                        t1.getUserStationSide();

        return Integer.compare(sideToDisplay1, sideToDisplay2);
    }
}