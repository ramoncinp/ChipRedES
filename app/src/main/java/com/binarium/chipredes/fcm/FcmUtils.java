package com.binarium.chipredes.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.SharedPreferencesManager;
import com.google.firebase.messaging.FirebaseMessaging;

public class FcmUtils
{
    private static final String TAG = "FCM Utils";
    private static final String SHARED_PREFERENCES_KEY = "pushMessagingSubscription";

    public static void subscribeToCouponMessaging(Context context)
    {
        // Obtener id de estacion
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Evaluar
        if (stationId.isEmpty())
        {
            Log.e(TAG, "No se encontró un id de estacion");
            return;
        }

        // Crear cadena con el topic
        String topic = "cupones_" + stationId;

        // Crear instancia de sharedPreferences
        SharedPreferences topics = context.getSharedPreferences(SHARED_PREFERENCES_KEY, 0);

        // Checar si ya se esta suscrito, si ya esta, salir de la función
        if (topics.contains(topic))
        {
            Log.d(TAG, "Ya esta suscrito a " + topic);
            return;
        }

        // Guardar que ya esta suscrito
        topics.edit().putBoolean(topic, true).apply();

        // Suscribirse
        FirebaseMessaging.getInstance().subscribeToTopic(topic);
        Log.d(TAG, "Suscrito a canal -> " + topic);
    }

    public static void unsubscribeToCouponMessaging(Context context)
    {
        // Crear instancia de sharedPreferences
        SharedPreferences topics = context.getSharedPreferences(SHARED_PREFERENCES_KEY, 0);

        // Validar que haya suscripciones
        if (topics.getAll().keySet().isEmpty())
        {
            Log.d(TAG, "No se está suscrito a ningún canal de notificaciones para cupones");
            return;
        }

        // Des-suscribirse de todos los topics guardados
        for (String topic : topics.getAll().keySet())
        {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
            Log.d(TAG, "Des-suscrito de canal -> " + topic);
        }

        // Borrar todos los topics de sharedPreferences
        topics.edit().clear().apply();
    }
}
