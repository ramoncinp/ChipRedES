package com.binarium.chipredes.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.binarium.chipredes.BuildConfig;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.main.MainActivity;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.printer.PrinterManager;
import com.binarium.chipredes.tokencash.models.Coupon;
import com.binarium.chipredes.utils.DateUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import timber.log.Timber;

/**
 * La notificacion se envia cuando se utiliza el siguiente servicio
 * <p>
 * URL: https://fcm.googleapis.com/fcm/send
 * Headers:
 * Content-Type: application/json
 * Authorization: key="server_key"
 * <p>
 * Body:
 * {
 * "to":"/topics/ChipREDES",
 * "data":{
 * "build_number":"x.x.x"
 * }
 * }
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String COUPONS_TAG = "Cupones";

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Timber.d("Mensaje recibido: %s", remoteMessage.getData().toString());

        //Obtener los datos de la notificación
        Map<String, String> data = remoteMessage.getData();

        //Obtener el nombre de la versión
        if (data.containsKey("build_number")) {
            //Si se tiene una versión diferente...
            if (!Objects.equals(data.get("build_number"), BuildConfig.VERSION_NAME)) {
                showDialogAndNotification(data.get("build_number"));
            }
        } else if (data.containsKey("cupon")) {
            getCoupon(data);
        }
    }

    private void showDialogAndNotification(String buildNumber) {
        //Crear canal de notificacion
        createNotificationChannel();

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,
                ChipREDConstants.GENERAL_NOTIFICATION_CHANNEL_ID).setSmallIcon(R.drawable
                .icon_chipred).setContentTitle("Actualización disponible").setContentText
                ("Instale la versión " + buildNumber + " para asegurar el buen funcionamiento de " +
                        "" + "" + "" + "" + "la aplicación").setPriority(NotificationCompat
                .PRIORITY_DEFAULT).setAutoCancel(true);

        //Agregar Intent a la notificación
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1000, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE |
                Notification.DEFAULT_LIGHTS);

        //Mostrar notificación
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(0, mBuilder.build());

        //Guardar en sharedPreferences una alerta para mostrar diálogo siempre que se Resuma
        //la actividad principal
        SharedPreferencesManager.putString(this, ChipREDConstants.NEW_VERSION_AVAILABLE,
                buildNumber);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.general_notification_channel);
            String description = getString(R.string.general_notification_channel);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(ChipREDConstants
                    .GENERAL_NOTIFICATION_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) notificationManager.createNotificationChannel(channel);
        }
    }

    private void getCoupon(Map<String, String> data) {
        Timber.d("Cupón recibido -> %s", data.get("cupon"));
        try {
            JSONObject couponObj = new JSONObject(Objects.requireNonNull(data.get("cupon")));
            JSONObject printer = new JSONObject(Objects.requireNonNull(data.get("printer")));
            String message = data.get("message");

            if (message != null)
                Timber.tag("Cupones").v(message);

            // Validar si a este dispositivo le corresponde imprimir
            Set<String> setPcs = SharedPreferencesManager.getStringSet(this,
                    ChipREDConstants.ASSIGNED_PCS_TO_DEVICE);

            String loadingPosition = printer.getString("posicion_carga");
            String printerIp = printer.getString("ip");

            if (setPcs.contains(loadingPosition)) {
                // Si le corresponde a este dispositivo
                // Convertir a objeto
                Coupon coupon = new Coupon();
                coupon.setAmount(couponObj.getDouble("importe"));
                coupon.setCode(couponObj.getString("cupon"));
                coupon.setFechaExpiracion(couponObj.getString("fecha_expiracion"));
                coupon.setPump(couponObj.getString("posicion_carga"));
                coupon.setPumper(couponObj.getString("especialista"));
                coupon.setFechaGeneracion(couponObj.getString("fecha_generacion"));

                final Date fechaGeneracion = DateUtils.stringToChipRedDate(coupon.getFechaGeneracion());
                if (validarFechaGeneracion(fechaGeneracion.getTime())) {
                    // Imprimir!!
                    Timber.d("Imprimir cupón en la impresora %s", printerIp);
                    Timber.d("Cupón %s", couponObj.toString());
                    printCoupon(coupon, printerIp);
                } else {
                    Timber.e("La fecha de generación fue hace más de 5 minutos");
                }
            } else {
                Timber.tag(COUPONS_TAG).v("La posicion de carga no le corresponde a este dispositivo");
            }
        } catch (JSONException e) {
            Timber.e("Error al convertir a JSON los datos -> %s", e.getMessage());
        } catch (NullPointerException e) {
            Timber.e("Error al obtener dato en el mensaje -> %s", e.getMessage());
        }
    }

    private void printCoupon(Coupon coupon, String printerIp) {
        // Crear printerManager
        PrinterManager printerManager = new PrinterManager(this,
                new PrinterManager.PrinterListener() {
                    @Override
                    public void onSuccesfulPrint(String msg) {
                        Timber.tag(COUPONS_TAG).v(msg);
                    }

                    @Override
                    public void onPrinterError(String msg) {
                        Timber.e(msg);
                    }

                });

        printerManager.setIpAndInit(printerIp);
        printerManager.printCoupon(coupon);

        Timber.tag(COUPONS_TAG).v("Imprimiendo cupon en impresora %s", printerIp);
    }

    private boolean validarFechaGeneracion(long sentDate) {
        // Evaluar fecha de envío con la actual
        long diff = new Date().getTime() - sentDate;

        // Diferencia
        Timber.d("Diferencia de fechas -> %s", diff);

        // Es válido si es menor a 5 minutos
        return diff < 60 * 1000 * 5;
    }
}