package com.binarium.chipredes;

import com.binarium.chipredes.emax.EmaxVehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class ChipREDClient
{
    private boolean capturePlates = false;
    private String idCliente = "";
    private String nombre = "";
    private String claveFiscal = "";
    private String email = "";
    private String status = "";
    private String mainEmail = "";
    private ArrayList<String> contacts = new ArrayList<>();
    private ArrayList<String> selectedEmails;

    private JSONObject clientInformation = new JSONObject();
    private JSONObject address = new JSONObject();
    private JSONObject phone = new JSONObject();

    private EmaxVehicle emaxVehicle;


    public ChipREDClient()
    {

    }

    public ChipREDClient(JSONObject clientJson)
    {
        clientInformation = clientJson;
        try
        {
            if (clientJson.has("id"))
                idCliente = clientJson.getString("id");
            if (clientJson.has("id_cliente"))
                idCliente = clientJson.getString("id_cliente");
            if (clientJson.has("estatus")) status = clientJson.getString("estatus");

            nombre = clientJson.getString("nombre").trim();
            email = clientJson.getString("email");
            claveFiscal = clientJson.getString("clave_fiscal");

            if (clientJson.has("direccion"))
                address = clientJson.getJSONObject("direccion");
            if (clientJson.has("telefono"))
                phone = clientJson.getJSONObject("telefono");

            //Obtener preferencia para capturar placas obligatoriamente
            if (clientJson.has("captura_placas"))
                capturePlates = clientJson.getBoolean("captura_placas");

            //Obtener arreglo de contactos
            if (clientJson.has("contactos"))
            {
                JSONArray contactsArray = clientJson.getJSONArray("contactos");
                for (int i = 0; i < contactsArray.length(); i++)
                {
                    contacts.add(contactsArray.getString(i));
                }
            }

            // Obtener vehíuclo, si lo tiene
            if (clientJson.has("vehiculo"))
            {
                // Obtener objeto JSON
                JSONObject stationVehicle = clientJson.getJSONObject("vehiculo");

                // Crear objeto vehículo
                // Asignar vehículo
                this.emaxVehicle = new EmaxVehicle(stationVehicle);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public String getAddress()
    {
        try
        {
            StringBuilder addressString = new StringBuilder();

            Iterator<String> iterator = address.keys();
            while (iterator.hasNext())
            {
                String value = address.getString(iterator.next());
                if (!value.isEmpty())
                {
                    addressString.append(value.toUpperCase());
                    addressString.append("\n");
                }
            }
            return addressString.toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return "";
        }
        catch (NullPointerException e)
        {
            return "";
        }
    }

    public String getCountry()
    {
        try
        {
            return address.getString("pais");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return "";
        }
        catch (NullPointerException e)
        {
            return "";
        }
    }

    public String getIdCliente()
    {
        return idCliente;
    }

    public void setIdCliente(String idCliente)
    {
        this.idCliente = idCliente;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getClaveFiscal()
    {
        return claveFiscal;
    }

    public void setClaveFiscal(String claveFiscal)
    {
        this.claveFiscal = claveFiscal;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public ArrayList<String> getContacts()
    {
        return contacts;
    }

    public void setContacts(ArrayList<String> contacts)
    {
        this.contacts = contacts;
    }

    public JSONObject getClientInformation()
    {
        return clientInformation;
    }

    public void setClientInformation(JSONObject clientInformation)
    {
        this.clientInformation = clientInformation;
    }

    public JSONObject getJsonAddress()
    {
        return this.address;
    }

    public ArrayList<String> getSelectedEmails()
    {
        return selectedEmails;
    }

    public void setSelectedEmails(ArrayList<String> selectedEmails)
    {
        this.selectedEmails = selectedEmails;
    }

    public String getMainEmail()
    {
        return mainEmail;
    }

    public void setMainEmail(String mainEmail)
    {
        this.mainEmail = mainEmail;
    }

    public boolean isCapturePlates()
    {
        return capturePlates;
    }

    public void setCapturePlates(boolean capturePlates)
    {
        this.capturePlates = capturePlates;
    }

    public EmaxVehicle getEmaxVehicle()
    {
        return emaxVehicle;
    }
}
