package com.binarium.chipredes;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ThreeRowListViewAdapter extends BaseAdapter
{
    Context context;
    ArrayList<String> columna1, columna2, columna3;
    LayoutInflater inflater;

    private float textSize = 0;

    public ThreeRowListViewAdapter(Context context, ArrayList<String> columna1, ArrayList<String> columna2, ArrayList<String> columna3)
    {
        this.context = context;
        this.columna1 = columna1;
        this.columna2 = columna2;
        this.columna3 = columna3;
    }

    public ThreeRowListViewAdapter(Context context, ArrayList<String> columna1, ArrayList<String> columna2, ArrayList<String> columna3, float textSize)
    {
        this.context = context;
        this.columna1 = columna1;
        this.columna2 = columna2;
        this.columna3 = columna3;
        this.textSize = textSize;
    }

    @Override
    public int getCount()
    {
        return columna1.size();
    }

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        TextView tvColumna1, tvColumna2, tvColumna3;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.three_column_row, parent, false);

        tvColumna1 = (TextView) itemView.findViewById(R.id.columna1_fila_tres_columnas);
        tvColumna2 = (TextView) itemView.findViewById(R.id.columna2_fila_tres_columnas);
        tvColumna3 = (TextView) itemView.findViewById(R.id.columna3_fila_tres_columnas);

        tvColumna1.setText(columna1.get(position));
        tvColumna2.setText(columna2.get(position));
        tvColumna3.setText(columna3.get(position));

        if (textSize != 0)
        {
            tvColumna1.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
            tvColumna2.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
            tvColumna3.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        }

        return itemView;
    }
}
