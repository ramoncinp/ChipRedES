package com.binarium.chipredes.wifidevices;

import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.R;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;

public class WiFiDeviceLogger extends AppCompatActivity
{
    private static final String TAG = WiFiDeviceLogger.class.getCanonicalName();
    private int toggleCounter = 0;

    //Views
    private CardView ledsVerdes;
    private CardView ledsRojos;
    private CardView ledsToToggle;

    private ImageView wifiImage;
    private ImageView uhfImage;

    private TextView readTagText;
    private TextView serverText;
    private TextView wifiText;

    private Handler handler;

    //Objetos
    private WebSocketClient webSocketClient;

    final Runnable toggle = new Runnable()
    {
        @Override
        public void run()
        {
            if (ledsToToggle.getAlpha() == 0.25f)
            {
                ledsToToggle.setAlpha(1f);
            }
            else
            {
                ledsToToggle.setAlpha(0.25f);
            }

            toggleCounter++;
            if (toggleCounter < 4) handler.postDelayed(toggle, 250);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wi_fi_device_logger);
        initViews();

        handler = new Handler();

        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            String deviceIp = extras.getString(ChipREDConstants.DEVICE_IP_ADDRESS);
            initWebSocket(deviceIp);
        }
        else
        {
            finish();
        }
    }

    private void initViews()
    {
        ledsVerdes = findViewById(R.id.leds_verdes);
        ledsRojos = findViewById(R.id.leds_rojos);

        wifiImage = findViewById(R.id.wifi_status);
        wifiText = findViewById(R.id.wifi_text);

        uhfImage = findViewById(R.id.uhf_image);
        readTagText = findViewById(R.id.read_tag_text);
        serverText = findViewById(R.id.server_text);
    }

    private void initWebSocket(String deviceIp)
    {
        URI uri;
        try
        {
            uri = new URI("ws://" + deviceIp + ":82/");
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
            return;
        }

        webSocketClient = new WebSocketClient(uri)
        {
            @Override
            public void onOpen(ServerHandshake handshakedata)
            {
                Log.d(TAG, handshakedata.toString());
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        showWebsocketStatus(true);
                    }
                });
            }

            @Override
            public void onMessage(final String message)
            {
                Log.d(TAG, message);
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        parseMessage(message);
                    }
                });
            }

            @Override
            public void onClose(int code, String reason, boolean remote)
            {
                Log.d(TAG, reason);
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        showWebsocketStatus(false);
                    }
                });
            }

            @Override
            public void onError(Exception ex)
            {
                Log.d(TAG, ex.toString());
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        showWebsocketStatus(false);
                    }
                });
            }
        };

        Log.d(TAG, "Iniciando webSocketClient...");
        webSocketClient.connect();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (webSocketClient.isOpen()) webSocketClient.close();
    }

    private void parseMessage(String message)
    {
        /*
         * El logger para antena UHF se utiliza con los siguientes conceptos
         * 1.- Conexión WiFi
         *  1.1.- "ok"
         *  1.2.- "aviso"
         *  1.3.- "error"
         *      1.1.1.- Mensaje
         *
         * 2.- Tag leído
         *  2.1.- "ok"
         *  2.2.- "aviso"
         *  2.3.- "error"
         *      2.1.1.- Mensaje
         *
         * 3.- Comunicacion con servidor
         *  3.1.- "ok"
         *  3.2.- "aviso"
         *  3.3.- "error"
         *      3.1.1.- Mensaje
         *
         * 4.- UHF
         *  4.1.- "leds_verdes"
         *  4.2.- "leds_rojos"
         *      4.1.1.- "on".. "off"..
         *
         * */

        try
        {
            Log.d(TAG, message);

            JSONObject object = new JSONObject(message);
            String key, subkey, value;

            key = object.getString("key");
            subkey = object.getString("subkey");
            value = object.getString("value");

            if (key.equals("1")) //Conexión WiFi
            {
                if (subkey.equals("ok"))
                {
                    wifiImage.setImageResource(R.drawable.ic_wifi_green);
                    wifiText.setText(value);
                    wifiText.setTextColor(getResources().getColor(R.color.green_ok));
                }
                else if (subkey.equals("aviso"))
                {
                    wifiImage.setImageResource(R.drawable.ic_wifi_gray);
                    wifiText.setText(value);
                    wifiText.setTextColor(getResources().getColor(R.color.black));
                }
                else if (subkey.equals("error"))
                {
                    wifiImage.setImageResource(R.drawable.ic_wifi_gray);
                    wifiText.setText(value);
                    wifiText.setTextColor(getResources().getColor(R.color.red));
                }
            }
            else if (key.equals("2"))
            {
                if (subkey.equals("ok"))
                {
                    uhfImage.setAlpha(1f);
                    readTagText.setText(value);
                    readTagText.setTextColor(getResources().getColor(R.color.green_ok));
                }
                else if (subkey.equals("aviso"))
                {
                    uhfImage.setAlpha(0.25f);
                    readTagText.setText(value);
                    readTagText.setTextColor(getResources().getColor(R.color.black));
                }
                else if (subkey.equals("error"))
                {
                    uhfImage.setAlpha(0.25f);
                    readTagText.setText(value);
                    readTagText.setTextColor(getResources().getColor(R.color.red));
                }
            }
            else if (key.equals("3"))
            {
                if (subkey.equals("ok"))
                {
                    serverText.setText(value);
                    serverText.setTextColor(getResources().getColor(R.color.green_ok));
                }
                else if (subkey.equals("aviso"))
                {
                    serverText.setText(value);
                    serverText.setTextColor(getResources().getColor(R.color.black));
                }
                else if (subkey.equals("error"))
                {
                    serverText.setText(value);
                    serverText.setTextColor(getResources().getColor(R.color.red));
                }
            }
            else if (key.equals("4"))
            {
                if (subkey.equals("leds_rojos"))
                {
                    if (value.equals("0"))
                    {
                        ledsRojos.setAlpha(0.25f);
                    }
                    else if (value.equals("1"))
                    {
                        ledsRojos.setAlpha(1f);
                    }
                    else
                    {
                        ledsToToggle = ledsRojos;
                        toogleLeds();
                    }
                }
                else if (subkey.equals("leds_verdes"))
                {
                    if (value.equals("0"))
                    {
                        ledsVerdes.setAlpha(0.25f);
                    }
                    else if (value.equals("1"))
                    {
                        ledsVerdes.setAlpha(1f);
                    }
                    else
                    {
                        ledsToToggle = ledsVerdes;
                        toogleLeds();
                    }
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void toogleLeds()
    {
        toggleCounter = 0;
        ledsToToggle.setAlpha(0.25f);
        toggle.run();
    }

    private void showResultDialog(String message)
    {
        GenericDialog genericDialog = new GenericDialog("Logger", message, new Runnable()
        {
            @Override
            public void run()
            {
                finish();
            }
        }, null, this);

        genericDialog.show();
    }

    private void showWebsocketStatus(boolean connected)
    {
        TextView webSocketStatusTv = findViewById(R.id.web_socket_status_tv);

        if (connected)
        {
            webSocketStatusTv.setText("Conectado");
            webSocketStatusTv.setBackgroundColor(getResources().getColor(R.color.green_ok));
        }
        else
        {
            webSocketStatusTv.setText("Desonectado");
            webSocketStatusTv.setBackgroundColor(getResources().getColor(R.color.red));
        }
    }
}
