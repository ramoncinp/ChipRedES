package com.binarium.chipredes.tokencash.ui;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.PumperFragment;
import com.binarium.chipredes.R;

import org.json.JSONObject;

public class TokenCashSummary extends AppCompatActivity
{
    //Views
    private RelativeLayout progressBarLayout;
    private TextView progressBarText;
    private FrameLayout frameLayout;

    //Variables
    private JSONObject pumper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_token_cash_summary);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        progressBarLayout = findViewById(R.id.token_cash_summary_progress_bar_layout);
        progressBarText = findViewById(R.id.token_cash_summary_progress_bar_text);
        frameLayout = findViewById(R.id.token_cash_summary_container);

        setPumpersFragment();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setPumpersFragment()
    {
        PumperFragment pumperFragment = new PumperFragment();
        pumperFragment.setPumperFragmentInterface(new PumperFragment.PumperFragmentInterface()
        {
            @Override
            public void requestingInfo()
            {
                showProgressBar("Obteniendo despachadores de estación...");
            }

            @Override
            public void gottenInfo()
            {
                hideProgressBar();
            }

            @Override
            public String getSalePumperId()
            {
                //No se utiliza en esta actividad
                return null;
            }

            @Override
            public void setNextFragment()
            {
                setClosedTokensFragment();
            }

            @Override
            public void setPumperNameOrTag(String pumperNameOrTag)
            {

            }

            @Override
            public void setJsonPumper(JSONObject pumper)
            {
                TokenCashSummary.this.pumper = pumper;
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.token_cash_summary_container,
                pumperFragment).commit();
    }

    private void setClosedTokensFragment()
    {
        ClosedTokensFragment closedTokensFragment = new ClosedTokensFragment();
        closedTokensFragment.setClosedTokensInterface(new ClosedTokensFragment
                .ClosedTokensInterface()
        {
            @Override
            public void requestingInfo(String text)
            {
                showProgressBar(text);
            }

            @Override
            public void gottenInfo()
            {
                hideProgressBar();
            }

            @Override
            public JSONObject getPumper()
            {
                return pumper;
            }

            @Override
            public void closeFragment(String text)
            {
                showMessageAndFinish(text);
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.token_cash_summary_container,
                closedTokensFragment).commit();
    }

    public void showProgressBar(String text)
    {
        frameLayout.setVisibility(View.INVISIBLE);
        progressBarText.setText(text);
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar()
    {
        progressBarLayout.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);
    }

    public void showMessageAndFinish(String message)
    {
        GenericDialog genericDialog = new GenericDialog("Error", message, new Runnable()
        {
            @Override
            public void run()
            {
                returnToLoadingPositions();
            }
        }, null, this);
        genericDialog.show();
    }

    private void returnToLoadingPositions()
    {
        finish();
    }
}
