package com.binarium.chipredes.tokencash.ui.lastsale

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.GenericDialog
import com.binarium.chipredes.R
import com.binarium.chipredes.databinding.FragmentLastSaleBinding
import com.binarium.chipredes.tokencash.ui.tokencashactivity.TokenCashViewModel
import com.binarium.chipredes.utils.DateUtils
import com.binarium.chipredes.utils.extensions.setVisible
import com.binarium.chipredes.wolke.models.LastSaleData
import com.binarium.chipredes.wolke.models.tokencash.TicketLessCoupon
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class LastSaleFragment : Fragment() {

    private lateinit var binding: FragmentLastSaleBinding
    private val sharedViewModel: TokenCashViewModel by activityViewModels()
    private val viewModel: LastSaleViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getLoadingPositionFromActivity()
    }

    private fun getLoadingPositionFromActivity() {
        val extras = requireActivity().intent.extras
        val loadingPosition = extras?.getInt(ChipREDConstants.SELECTED_LOADING_POSITION)
        viewModel.loadingPosition = loadingPosition.toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        requireActivity().title = "Última venta"
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_last_sale, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewListeners()
        initObservers()
        viewModel.getLastSale()
    }

    private fun initViewListeners() {
        binding.continueButton.setOnClickListener {
            if (viewModel.salePumper == null) {
                showNoPumperSelected()
            } else {
                askPumperNip()
            }
        }
    }

    private fun initObservers() {
        viewModel.lastSale.observe(viewLifecycleOwner, {
            showSaleData(it)
            viewModel.getStationPumper()
            viewModel.getCoupon()
            sharedViewModel.saleData = it
        })

        viewModel.ticketLessCoupon.observe(viewLifecycleOwner, {
            showTicketLessCoupon(it)
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, {
            Timber.e(it)
            binding.progressBar.setVisible(false)
            binding.errorTextTv.setVisible(true)
            binding.errorTextTv.text = it
        })
    }

    private fun showSaleData(lastSaleData: LastSaleData) {
        val importeVal = "\$${ChipREDConstants.MX_AMOUNT_FORMAT.format(lastSaleData.costo)}"
        val cantidadVal = "${ChipREDConstants.VOLUME_FORMAT.format(lastSaleData.cantidad)} L"
        val precioUnitarioVal =
            "\$${ChipREDConstants.MX_AMOUNT_FORMAT.format(lastSaleData.precioUnitario)}"

        binding.importeTv.text = importeVal
        binding.cantidadTv.text = cantidadVal
        binding.precioUnitarioTv.text = precioUnitarioVal
        binding.productTv.text = lastSaleData.combustible
        //binding.ticketTv.text = lastSaleData.numTicket

        val date = DateUtils.formatoFechaMovimientosCuenta(lastSaleData.fechaHora, false)
        binding.fechaTv.text = DateUtils.dateObjectToString(date)

        binding.progressBar.setVisible(false)
        binding.lastSaleContent.setVisible(true)
        binding.continueButton.setVisible(true)
    }

    private fun showTicketLessCoupon(coupon: TicketLessCoupon) {
        val amount = "\$${ChipREDConstants.MX_AMOUNT_FORMAT.format(coupon.importe)}"
        binding.ticketlessQr.setImageBitmap(viewModel.couponBitmap)
        binding.tokenCashRewardTv.text = amount
        binding.ticketLessCard.setVisible(true)
    }

    private fun askPumperNip() {
        val salePumper = viewModel.salePumper
        val builder = AlertDialog.Builder(requireContext())
        val dialogNip = requireActivity().layoutInflater.inflate(R.layout.dialog_nip, null)

        val etNipD = dialogNip.findViewById<EditText>(R.id.etNipC)
        builder.setTitle("Ingresar NIP de despachador: " + salePumper?.nombre)
        builder.setView(dialogNip)
        builder.setPositiveButton(
            "INGRESAR"
        ) { dialog: DialogInterface, _: Int ->
            if (etNipD.text.toString() == salePumper?.nip) {
                onContinue()
            } else {
                Toast.makeText(requireContext(), "No coincide NIP", Toast.LENGTH_SHORT).show()
                dialog.dismiss()
            }
        }
        builder.setNegativeButton(
            "CANCELAR"
        ) { dialog: DialogInterface, _: Int -> dialog.dismiss() }

        val dialog = builder.create()
        dialog.show()
        ChipREDConstants.showKeyboard(requireContext())
    }

    private fun showNoPumperSelected() {
        val genericDialog = GenericDialog(
            "Error", "No coincide despachador de " +
                    "venta con alguno registrado", {}, null, requireContext()
        )
        genericDialog.setCancelable(false)
        genericDialog.setPositiveText("Ok")
        genericDialog.show()
    }

    private fun onContinue() {
        findNavController().navigate(
            LastSaleFragmentDirections.actionLastSaleFragmentToGenerateTokenFragment(
                viewModel.salePumper!!
            )
        )
    }
}
