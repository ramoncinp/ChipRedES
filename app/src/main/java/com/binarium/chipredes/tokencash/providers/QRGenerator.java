package com.binarium.chipredes.tokencash.providers;

import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

public class QRGenerator
{
    static public Bitmap encodeAsBitmap(String data, int width)
    {
        BitMatrix result;
        Bitmap bitmap;
        try
        {
            result = new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, width, width,
                    null);

            int w = result.getWidth();
            int h = result.getHeight();
            int[] pixels = new int[w * h];
            for (int y = 0; y < h; y++)
            {
                int offset = y * w;
                for (int x = 0; x < w; x++)
                {
                    pixels[offset + x] = result.get(x, y) ? 0xFF000000 : 0xFFFFFFFF;
                }
            }
            bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, w, h);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
        return bitmap;
    }
}
