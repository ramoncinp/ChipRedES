package com.binarium.chipredes.tokencash.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.Pumper;
import com.binarium.chipredes.R;
import com.binarium.chipredes.printer.PrinterManager;
import com.binarium.chipredes.printer.PrintersDialogFragment;
import com.binarium.chipredes.tokencash.repository.TokenCashManager;
import com.binarium.chipredes.tokencash.adapters.ClosedTokensArrayAdapter;
import com.binarium.chipredes.tokencash.models.ClosedTokens;
import com.binarium.chipredes.utils.DateUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class ClosedTokensFragment extends Fragment
{
    private PrintersDialogFragment printersDialogFragment;
    private RecyclerView recyclerView;
    private Button printSummaryButton;
    private TextView noTokens;
    private TextView tokenCashTotal;
    private String currency, printerIp = "";

    private PrinterManager printerManager;
    private ChipRedManager chipRedManager;
    private ClosedTokensInterface closedTokensInterface;

    private ArrayList<ClosedTokens> closedTokens;

    private Pumper pumper;

    public ClosedTokensFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState)

    {
        View contentView = inflater.inflate(R.layout.fragment_closed_tokens, container, false);
        recyclerView = contentView.findViewById(R.id.closed_tokens_list);
        printSummaryButton = contentView.findViewById(R.id.print_tokens_summary_button);
        noTokens = contentView.findViewById(R.id.no_tokens);
        tokenCashTotal = contentView.findViewById(R.id.token_cash_total);
        // Inflate the layout for this fragment
        return contentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // Definir título
        getActivity().setTitle("Tokens cerrados");

        // Inicializar chipredManager
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived()
        {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN)
            {
                if (webServiceN == ChipRedManager.GET_PRINTERS)
                {
                    ChipREDConstants.showSnackBarMessage(crMessage, getActivity());
                }
                else
                {
                    printTokenSummary();
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN)
            {
                if (webServiceN == ChipRedManager.GET_PRINTERS)
                {
                    ChipREDConstants.showSnackBarMessage(errorMessage, getActivity());
                }
                else
                {
                    closedTokensInterface.closeFragment(errorMessage);
                }
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN)
            {
                if (webServiceN == ChipRedManager.GET_CLOSED_TOKENS)
                {
                    getClosedTokens(response);
                    showPrinterSelector();
                }
            }
        }, getContext());

        printSummaryButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v)
            {
                closedTokensInterface.requestingInfo("Imprimiendo tokens cerrados...");
                requestPrintTokenSummary();
            }
        });

        closedTokensInterface.requestingInfo("Obteniendo tokens cerrados...");

        //Pedir a la actividad el objeto de despachador
        pumper = new Pumper();
        try

        {
            JSONObject pumperJson = closedTokensInterface.getPumper();
            pumper.setName(pumperJson.getString("nombre"));
            pumper.setTagId(pumperJson.getString("tag"));
            pumper.setEmaxId(pumperJson.getString("id_emax"));
        }
        catch (
                JSONException e)

        {
            e.printStackTrace();
        }

        // Ejecutar petición para obtener tokens cerrados
        getClosedTokens();
    }

    private void getClosedTokens()
    {
        if (ChipREDConstants.getTokencashScenario(getContext()).equals("e3"))
        {
            TokenCashManager.getClosedTokens(getContext(), pumper.getEmaxId(),
                    new TokenCashManager.onMessageReceived()
                    {
                        @Override
                        public void showResponse(JSONObject response)
                        {
                            getClosedTokens(adjustResponse(response));
                            showPrinterSelector();
                        }

                        @Override
                        public void onError(String message)
                        {
                            closedTokensInterface.closeFragment(message);
                        }
                    });
        }
        else
        {
            chipRedManager.getClosedTokens(pumper.getTagId());
        }
    }

    private JSONObject adjustResponse(JSONObject response)
    {
        // Crear objeto JSON para guardar datos ajustados
        JSONObject adjustedJSON = new JSONObject();

        try
        {
            // Obtener arreglo de datos obtenidos del servicio
            JSONArray serviceData = response.getJSONArray("data");

            // Crear arreglo para almacenar datos ajustados
            JSONArray adjustedData = new JSONArray();

            // Iterar en arreglo obtenido
            for (int idx = 0; idx < serviceData.length(); idx++)
            {
                // Obtener objeto JSON
                JSONObject closedTokenObj = serviceData.getJSONObject(idx);

                // Crear nuevo objeto JSON
                JSONObject adjustedClosedToken = new JSONObject();
                String dateTime = closedTokenObj.getString("date_time");
                String convertedDateTime =
                        DateUtils.dateObjectToString(DateUtils.serverDateToLocalDate(dateTime));

                adjustedClosedToken.put("fecha_hora", convertedDateTime);
                adjustedClosedToken.put("costo", closedTokenObj.getDouble("amount"));
                adjustedClosedToken.put("token", closedTokenObj.getString("token"));

                // Obtener todos los pagos
                double total = 0;
                String currency = "";
                JSONArray payments = closedTokenObj.getJSONArray("payments");
                for (int paymentsIdx = 0; paymentsIdx < payments.length(); paymentsIdx++)
                {
                    double amount =
                            payments.getJSONObject(paymentsIdx).getJSONObject("details").getJSONObject("details_to").getDouble("amount");
                    currency =
                            payments.getJSONObject(paymentsIdx).getJSONObject("details").getJSONObject("details_to").getString("currency");

                    // Acumular para dar el resumen
                    total += amount;
                }

                // Crear resumen
                JSONObject summary = new JSONObject();
                summary.put("currency", currency);
                summary.put("amount", total);
                adjustedClosedToken.put("summary", summary);

                // Agregar objeto de token ajustado
                adjustedData.put(adjustedClosedToken);
            }

            // Agregar data
            adjustedJSON.put("data", adjustedData);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return adjustedJSON;
    }

    private void getClosedTokens(JSONObject response)
    {
        double grandTokenCashTotal = 0;

        closedTokens = new ArrayList<>();
        //Hacer un recorrido por el objeto data
        try
        {
            JSONArray data = response.getJSONArray("data");
            if (data.length() == 0)
            {
                closedTokensInterface.gottenInfo();
                recyclerView.setVisibility(View.INVISIBLE);
                noTokens.setVisibility(View.VISIBLE);
            }
            else
            {
                recyclerView.setVisibility(View.VISIBLE);
                noTokens.setVisibility(View.INVISIBLE);

                for (int i = 0; i < data.length(); i++)
                {
                    JSONObject object = data.getJSONObject(i);

                    ClosedTokens closedToken = new ClosedTokens();
                    closedToken.setCurrency(object.getJSONObject("summary").getString(
                            "currency"));
                    closedToken.setDateTime(object.getString("fecha_hora"));
                    closedToken.setTotalAmount(object.getDouble("costo"));
                    closedToken.setTokenAmount(object.getJSONObject("summary").getDouble(
                            "amount"));
                    closedToken.setTokenValue(object.getString("token"));
                    closedTokens.add(closedToken);

                    grandTokenCashTotal += closedToken.getTokenAmount();
                }

                currency = closedTokens.get(0).getCurrency();
                String totalString = String.format(Locale.US, "TOTAL: %.2f " + currency, (float)
                        grandTokenCashTotal);
                tokenCashTotal.setText(totalString);

                ClosedTokensArrayAdapter closedTokensArrayAdapter = new ClosedTokensArrayAdapter
                        (closedTokens, getContext());
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

                recyclerView.setAdapter(closedTokensArrayAdapter);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setHasFixedSize(true);

                closedTokensInterface.gottenInfo();
            }
        }
        catch (JSONException e)
        {
            closedTokensInterface.closeFragment("Error al procesar la información");
        }
    }

    private void requestPrintTokenSummary()
    {
        if (ChipREDConstants.getTokencashScenario(getContext()).equals("e3"))
        {
            TokenCashManager.printClosedTokens(getContext(), pumper.getEmaxId(),
                    new TokenCashManager.onMessageReceived()
                    {
                        @Override
                        public void showResponse(JSONObject response)
                        {
                            printTokenSummary();
                        }

                        @Override
                        public void onError(String message)
                        {
                            closedTokensInterface.closeFragment(message);
                        }
                    });
        }
        else
        {
            chipRedManager.printTokensSummary(pumper.getTagId());
        }
    }

    private void printTokenSummary()
    {
        closedTokensInterface.requestingInfo("Imprimiendo tokens cerrados...");

        //Imprimir ticket
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                printerManager = null;
                printerManager = new PrinterManager(getContext(), new PrinterManager
                        .PrinterListener()
                {
                    @Override
                    public void onSuccesfulPrint(String msg)
                    {
                        getActivity().runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                closedTokensInterface.gottenInfo();
                                getActivity().finish();
                            }
                        });
                        Log.d("Printer", "Impresión realizada correctamente");
                    }

                    @Override
                    public void onPrinterError(String msg)
                    {
                        getActivity().runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                closedTokensInterface.gottenInfo();
                                Log.d("Printer", "Error al imprimir corte tokencash");
                                final GenericDialog genericDialog = new GenericDialog("Error"
                                        , "Error al " +
                                        "imprimir corte tokencash", new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        printTokenSummary();
                                    }
                                }, new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        //nada
                                    }
                                }, getContext());

                                genericDialog.setCancelable(false);
                                genericDialog.setPositiveText("Reintentar");
                                genericDialog.setNegativeText("Cerrar");
                                genericDialog.show();
                            }
                        });
                    }

                });

                printerManager.setIp(printerIp);
                printerManager.printClosedTokens(pumper.getName(), closedTokens);
            }
        }).start();
    }

    private void showFakeClosedTokens()
    {
        double grandTokenCashTotal = 0;

        closedTokens = new ArrayList<>();
        currency = "MXN";

        ClosedTokens closedToken1 = new ClosedTokens();
        closedToken1.setCurrency("MXN");
        closedToken1.setDateTime("14-12-2018");
        closedToken1.setTotalAmount(100);
        closedToken1.setTokenAmount(10);
        closedToken1.setTokenValue("7756");
        closedTokens.add(closedToken1);
        grandTokenCashTotal += closedToken1.getTokenAmount();

        ClosedTokens closedToken2 = new ClosedTokens();
        closedToken2.setCurrency("MXN");
        closedToken2.setDateTime("13-12-2018");
        closedToken2.setTotalAmount(150);
        closedToken2.setTokenAmount(5);
        closedToken2.setTokenValue("7757");
        closedTokens.add(closedToken2);

        ClosedTokens closedToken3 = new ClosedTokens();
        closedToken3.setCurrency("MXN");
        closedToken3.setDateTime("14-12-2018");
        closedToken3.setTotalAmount(100);
        closedToken3.setTokenAmount(10);
        closedToken3.setTokenValue("7756");
        closedTokens.add(closedToken3);
        grandTokenCashTotal += closedToken3.getTokenAmount();

        ClosedTokens closedToken4 = new ClosedTokens();
        closedToken4.setCurrency("MXN");
        closedToken4.setDateTime("13-12-2018");
        closedToken4.setTotalAmount(150);
        closedToken4.setTokenAmount(5);
        closedToken4.setTokenValue("7757");
        closedTokens.add(closedToken4);

        grandTokenCashTotal += closedToken4.getTokenAmount();

        String totalString = String.format(Locale.US, "TOTAL: %.2f " + currency, (float)
                grandTokenCashTotal);
        tokenCashTotal.setText(totalString);

        ClosedTokensArrayAdapter closedTokensArrayAdapter = new ClosedTokensArrayAdapter
                (closedTokens, getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        recyclerView.setAdapter(closedTokensArrayAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        closedTokensInterface.gottenInfo();
    }

    public void setClosedTokensInterface(ClosedTokensInterface closedTokensInterface)
    {
        this.closedTokensInterface = closedTokensInterface;
    }

    private void showPrinterSelector()
    {
        printersDialogFragment = new PrintersDialogFragment();
        printersDialogFragment.setPrinterDialogInterface(new PrintersDialogFragment.PrinterDialogInterface()
        {
            @Override
            public void onIpSelected(String ip)
            {
                printerIp = ip;
                printersDialogFragment.dismiss();
            }
        });

        printersDialogFragment.show(getActivity().getSupportFragmentManager(), "printer_dialog");
    }

    public interface ClosedTokensInterface
    {
        void requestingInfo(String text);

        void gottenInfo();

        void closeFragment(String text);

        JSONObject getPumper();
    }
}
