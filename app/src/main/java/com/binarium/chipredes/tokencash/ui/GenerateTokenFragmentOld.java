package com.binarium.chipredes.tokencash.ui;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.printer.PrinterManager;
import com.binarium.chipredes.R;
import com.binarium.chipredes.Sale;
import com.binarium.chipredes.tokencash.repository.TokenCashManager;
import com.binarium.chipredes.tokencash.models.TokenCashPayment;
import com.binarium.chipredes.tokencash.models.Token;
import com.binarium.chipredes.tokencash.providers.QRGenerator;
import com.google.zxing.WriterException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

import timber.log.Timber;

public class GenerateTokenFragmentOld extends Fragment {
    private TextView productAmount;
    private TextView tokenValue;
    private ImageView tokenView;
    private Button confirmPayment;

    private GenerateTokenInterface generateTokenInterface;
    private ChipRedManager chipRedManager;
    private PrinterManager printerManager;
    private Token token;

    private boolean printIt = true;
    private int pumpNumber;
    private String printerIp;

    private final DecimalFormat decimalFormat = new DecimalFormat("$ ###,##0.00");

    private final PrinterManager.PrinterListener listenerForToken = new PrinterManager.PrinterListener() {
        @Override
        public void onSuccesfulPrint(String msg) {
            if (ChipREDConstants.checkNullParentActivity(getActivity(), GenerateTokenFragmentOld.this))
                return;

            Timber.d("Impresión realizada correctamente");
            getActivity().runOnUiThread(() -> generateTokenInterface.gottenData());
        }

        @Override
        public void onPrinterError(String msg) {
            if (ChipREDConstants.checkNullParentActivity(getActivity(), GenerateTokenFragmentOld.this))
                return;

            getActivity().runOnUiThread(() -> {
                generateTokenInterface.gottenData();
                Timber.d("Error al imprimir token");
                final GenericDialog genericDialog = new GenericDialog("Error", "Error al " +
                        "imprimir" + " " + "token", () -> printToken(), () -> {
                    //nada
                }, getContext());

                genericDialog.setCancelable(false);
                genericDialog.setPositiveText("Reintentar");
                genericDialog.setNegativeText("Cerrar");
                genericDialog.show();
            });
        }
    };

    private final PrinterManager.PrinterListener listenerForPurchase = new PrinterManager
            .PrinterListener() {
        @Override
        public void onSuccesfulPrint(String msg) {
            if (ChipREDConstants.checkNullParentActivity(getActivity(), GenerateTokenFragmentOld.this))
                return;

            getActivity().runOnUiThread(() -> {
                generateTokenInterface.gottenData();
                generateTokenInterface.closeFragment();
            });
            Timber.d("Impresión realizada correctamente");
        }

        @Override
        public void onPrinterError(String msg) {
            if (ChipREDConstants.checkNullParentActivity(getActivity(), GenerateTokenFragmentOld.this))
                return;

            getActivity().runOnUiThread(() -> {
                generateTokenInterface.gottenData();
                Timber.d("Error al imprimir consumo");
                final GenericDialog genericDialog = new GenericDialog("Error", "Error al " +
                        "imprimir" + " " + "consumo", () -> printPurchaseWithTokenCash(), () -> {
                    //nada
                }, getContext());

                genericDialog.setCancelable(false);
                genericDialog.setPositiveText("Reintentar");
                genericDialog.setNegativeText("Cerrar");
                genericDialog.show();
            });
        }
    };

    private JSONObject tokenSummary;
    private JSONArray tokenPayments;

    public GenerateTokenFragmentOld() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_generate_token, container, false);
        productAmount = rootView.findViewById(R.id.token_product_amount);
        tokenValue = rootView.findViewById(R.id.token_value_tv);
        tokenView = rootView.findViewById(R.id.token_image_view);
        confirmPayment = rootView.findViewById(R.id.check_payment);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() != null)
            getActivity().setTitle("Token de cobro");

        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                if (webServiceN == ChipRedManager.GENERATE_TOKEN) {
                    generateTokenInterface.closeFragment(crMessage);
                } else if (webServiceN == ChipRedManager.GET_TOKEN) {
                    generateTokenInterface.gottenData();
                    generateTokenInterface.showSnackbarMessage("No se pudo obtener información "
                            + "del token, intente más tarde");
                } else if (webServiceN == ChipRedManager.GET_PRINTERS) {
                    generateTokenInterface.showSnackbarMessage("Error al obtener dirección de la " +
                            "impresora");
                } else if (webServiceN == ChipRedManager.CLOSE_TOKEN) {
                    printPurchaseWithTokenCash();
                } else if (webServiceN == ChipRedManager.GET_TOKEN_CASH_CONFIG) {
                    //Generar token
                    validateAndGenerateToken();
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                if (webServiceN == ChipRedManager.GENERATE_TOKEN) {
                    generateTokenInterface.closeFragment(errorMessage);
                } else if (webServiceN == ChipRedManager.GET_PRINTERS) {
                    generateTokenInterface.showSnackbarMessage("Error al obtener dirección de la " +
                            "impresora");
                } else if (webServiceN == ChipRedManager.GET_TOKEN_CASH_CONFIG) {
                    //Generar token
                    validateAndGenerateToken();
                } else {
                    generateTokenInterface.gottenData();
                    generateTokenInterface.showSnackbarMessage("No se pudo realizar el " + "pago," +
                            "" + "" + "" + "" + "" + "" + "" + "" + " intente" + " más " + "tarde");
                }
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                if (webServiceN == ChipRedManager.GENERATE_TOKEN) {
                    getTokenData(response);
                } else if (webServiceN == ChipRedManager.GET_PRINTERS) {
                    evaluatePrinter(response);
                } else if (webServiceN == ChipRedManager.GET_TOKEN_CASH_CONFIG) {
                    //Definir si se puede imprimir token de cobro
                    try {
                        printIt = response.getJSONObject("data").getBoolean(
                                "impresion_token_cobro");

                        if (!printIt) {
                            Toast.makeText(getContext(), "Impresión desactivada",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //Generar token
                    validateAndGenerateToken();
                } else {
                    generateTokenInterface.gottenData();
                    checkTokenPayment(response);
                }
            }
        }, getContext());

        confirmPayment.setOnClickListener(v -> {
            generateTokenInterface.requestingData("Obteniendo información...");

            if (ChipREDConstants.getTokencashScenario(getActivity()).equals("e3")) {
                TokenCashManager.getTokenPayments(getActivity(), token.getTransactionId(),
                        new TokenCashManager.onMessageReceived() {
                            @Override
                            public void showResponse(JSONObject response) {
                                generateTokenInterface.gottenData();
                                checkTokenPayment(adjustTokenPayments(response));
                            }

                            @Override
                            public void onError(String message) {
                                generateTokenInterface.showSnackbarMessage(message);
                            }
                        });
            } else {
                chipRedManager.getTokenInfo(token.getTransactionId());
            }
        });

        //Obtener set de reglas
        chipRedManager.getTokenCashConfiguration();

        //Mostrar progressBar
        generateTokenInterface.requestingData("Generando token...");
    }

    private void validateAndGenerateToken() {
        double saleAmount;
        String saleId;

        Bundle arguments = getArguments();
        if (arguments.containsKey(ChipREDConstants.PURCHASE_ID_FOR_TOKENCASH) && arguments
                .containsKey(ChipREDConstants.PURCHASE_AMOUNT_FOR_TOKENCASH) && arguments
                .containsKey(ChipREDConstants.PUMP_NUMBER_FOR_TOKEN_CASH)) {
            saleAmount = arguments.getDouble(ChipREDConstants.PURCHASE_AMOUNT_FOR_TOKENCASH);
            saleId = arguments.getString(ChipREDConstants.PURCHASE_ID_FOR_TOKENCASH);
            pumpNumber = arguments.getInt(ChipREDConstants.PUMP_NUMBER_FOR_TOKEN_CASH);

            generateToken(saleAmount, saleId);
        } else {
            generateTokenInterface.closeFragment("Error al procesar la inforación, intente de " +
                    "nuevo");
        }
    }

    public void generateToken(double amount, String saleId) {
        // Validar el escenario
        if (ChipREDConstants.getTokencashScenario(getActivity()).equals("e3")) {
            TokenCashManager.createToken(getActivity(), saleId,
                    new TokenCashManager.onMessageReceived() {
                        @Override
                        public void showResponse(JSONObject response) {
                            getTokenData(adjustTokenData(response));
                        }

                        @Override
                        public void onError(String message) {
                            generateTokenInterface.closeFragment(message);
                        }
                    });
        } else {
            chipRedManager.generateToken(saleId, amount, pumpNumber);
        }
    }

    private JSONObject adjustTokenData(JSONObject response) {
        try {
            JSONObject data = new JSONObject();
            JSONObject root = new JSONObject();

            data.put("id", response.getJSONObject("_id").getString("$oid"));
            data.put("estatus", response.getString("status"));
            data.put("token", response.getString("token"));
            data.put("transaccion", response.getString("transaction"));
            data.put("id_consumo", response.getJSONObject("sale").getString("$oid"));
            data.put("costo", response.getDouble("amount"));

            root.put("message", "Token de cobro generado correctamente");
            root.put("data", data);

            return root;
        } catch (JSONException e) {
            e.printStackTrace();
            generateTokenInterface.closeFragment("Error al procesar la información del token");
        }

        return null;
    }

    private JSONObject adjustTokenPayments(JSONObject response) {
        // Crear nuevo objeto JSON
        JSONObject root = new JSONObject();
        JSONObject data;

        try {
            // Guardar datos en "data"
            data = response;
            root.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return root;
    }

    private void getTokenData(JSONObject response) {
        String resultMessage = null;
        token = new Token();
        try {
            JSONObject jsonToken = response.getJSONObject("data");
            token.setId(jsonToken.getString("id"));
            token.setPurchaseAmount(jsonToken.getDouble("costo"));
            token.setPurchaseId(jsonToken.getString("id_consumo"));
            token.setStatus(jsonToken.getString("estatus"));
            token.setTokenValue(jsonToken.getString("token"));
            token.setTransactionId(jsonToken.getString("transaccion"));
            resultMessage = response.getString("message");
        } catch (Exception e) {
            e.printStackTrace();
            generateTokenInterface.closeFragment("Error al procesar la información del token");
        }

        //Escribir datos en TextViews
        tokenValue.setText(token.getTokenValue());
        productAmount.setText(decimalFormat.format(token.getPurchaseAmount()));

        //Generar código QR
        generateQrCode(token.getTokenValue(), resultMessage);

        //Obtener Ip de impresora
        if (ChipREDConstants.getTokencashScenario(getActivity()).equals("e3")) {
            getPrinter();
        } else {
            chipRedManager.getPrinters();
        }
    }

    /**
     * El método consumo el servicio para obtener impresoras de la estación
     * Cuando la respuesta es existosa, se obtiene el siguiente arreglo:
     * <p>
     * "printers": [
     * {
     * "id": 21,
     * "ip": "192.168.0.49",
     * "dispenser_side": 21,
     * "is_active": true
     * },
     * {
     * "id": 22,
     * "ip": "192.168.0.49",
     * "dispenser_side": 22,
     * "is_active": true
     * }
     * ]
     */
    private void getPrinter() {
        TokenCashManager.getPrinters(getActivity(), new TokenCashManager.onMessageReceived() {
            @Override
            public void showResponse(JSONObject response) {
                printerIp = "";
                try {
                    // Obtener arreglo de impresoras
                    JSONArray printers = response.getJSONArray("printers");
                    for (int i = 0; i < printers.length(); i++) {
                        JSONObject printer = printers.getJSONObject(i);
                        int pump = printer.getInt("dispenser_side");
                        if (pump == pumpNumber) {
                            //Obtener ip de impresora
                            printerIp = printer.getString("ip");
                            break;
                        }
                    }

                    if (printIt) printToken();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String message) {
                Toast.makeText(getActivity(), "Error al obtener dirección de la " +
                        "impresora", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void evaluatePrinter(JSONObject response) {
        //{"message":"Impresoras encontradas correctamente.","data":[{"ip":"192.168.0.49",
        // "estado":"AC","posicion_carga":21},{"ip":"192.168.0.49","estado":"AC",
        // "posicion_carga":22}],"jsonapi":{"version":"2.0"},"response":"Ok"}
        printerIp = "";

        try {
            JSONArray data = response.getJSONArray("data");
            for (int i = 0; i < data.length(); i++) {
                JSONObject printer = data.getJSONObject(i);

                int pump = printer.getInt("posicion_carga");
                if (pump == pumpNumber) {
                    //Obtener ip de impresora
                    printerIp = printer.getString("ip");
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (printIt) printToken();
    }

    private void printToken() {
        generateTokenInterface.requestingData("Imprimiendo token...");

        //Imprimir token
        new Thread(() -> {
            printerManager = null;
            //Imprimir token
            printerManager = new PrinterManager(getContext(), listenerForToken);
            printerManager.setIp(printerIp);
            printerManager.printToken(decimalFormat.format(token.getPurchaseAmount()), token
                    .getTokenValue(), generateTokenInterface.getActivitySale()
                    .getPosicionDeCarga());
        }).start();
    }

    private void checkTokenPayment(JSONObject response) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = getLayoutInflater();
            View confirmDialog = inflater.inflate(R.layout.check_tokencash_payments, null);

            TextView saleAmount = confirmDialog.findViewById(R.id.confirm_tokencash_sale_amount);
            TextView receivedAmount = confirmDialog.findViewById(R.id
                    .confirm_tokencash_received_amount);
            TextView pendingAmount = confirmDialog.findViewById(R.id
                    .confirm_tokencash_pending_amount);

            //Obtener el contenido
            JSONObject data = response.getJSONObject("data");

            //Obtener los pagos
            ArrayList<TokenCashPayment> tokenCashPayments = new ArrayList<>();
            tokenPayments = data.getJSONArray("payments");
            if (tokenPayments.length() != 0) {
                for (int i = 0; i < tokenPayments.length(); i++) {
                    TokenCashPayment tokencashPayment = new TokenCashPayment();
                    JSONObject tokencashPaymentJson = tokenPayments.getJSONObject(i);

                    tokencashPayment.setAmount(tokencashPaymentJson.getJSONObject("details")
                            .getJSONObject("to").getDouble("amount"));
                    tokencashPayment.setCurrency(tokencashPaymentJson.getJSONObject("details")
                            .getJSONObject("to").getString("currency"));
                    tokencashPayment.setDateTime(tokencashPaymentJson.getString("datetime"));
                    //tokencashPayment.setDocument(tokencashPaymentJson.getString("document"));

                    tokenCashPayments.add(tokencashPayment);
                }
            }
            token.setTokenCashPayments(tokenCashPayments);

            tokenSummary = data.getJSONObject("summary");
            double drecibido = tokenSummary.getDouble("amount");
            String recibido = String.format(Locale.ROOT, "%.2f", drecibido);

            //Obtener la información de la venta y su costo
            final Sale sale = generateTokenInterface.getActivitySale();
            final Double drestante = sale.getCosto() - drecibido;
            final String restante = String.format(Locale.ROOT, "%.2f", drestante);
            saleAmount.setText(String.format(Locale.ROOT, "$ %.2f", sale.getCosto()));

            final String receivedAmountVal = "$ " + recibido;
            final String pendingAmountVal = "$ " + restante;
            receivedAmount.setText(receivedAmountVal);
            pendingAmount.setText(pendingAmountVal);

            builder.setTitle("Cerrar pagos de tokencash");
            builder.setView(confirmDialog);
            builder.setPositiveButton("REGRESAR", (dialog, which) -> dialog.cancel()).setNegativeButton("CONTINUAR", (dialog, which) -> closeTokencashPayment());

            AlertDialog dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            generateTokenInterface.showSnackbarMessage("Error al procesar información");
        }
    }

    private void closeTokencashPayment() {
        if (token.getTokenCashPayments().size() == 0) {
            GenericDialog genericDialog = new GenericDialog("Pago tokencash", "No se ha " +
                    "realizado" + " ningún pago", () -> {
                //
            }, null, getContext());
            genericDialog.setPositiveText("Cerrar");
            genericDialog.show();
        } else {
            generateTokenInterface.requestingData("Cerrando pagos...");

            if (ChipREDConstants.getTokencashScenario(getActivity()).equals("e3")) {
                TokenCashManager.closeToken(getActivity(), token.getTransactionId(),
                        generateTokenInterface.getActivitySale().getPumperId(),
                        new TokenCashManager.onMessageReceived() {
                            @Override
                            public void showResponse(JSONObject response) {
                                printPurchaseWithTokenCash();
                            }

                            @Override
                            public void onError(String message) {
                                generateTokenInterface.gottenData();
                                generateTokenInterface.showSnackbarMessage(message);
                            }
                        });
            } else {
                chipRedManager.closeToken(token.getId(), token.getTransactionId(), tokenPayments, tokenSummary);
            }
        }
    }

    private void printPurchaseWithTokenCash() {
        //Imprimir recibo
        new Thread(() -> {
            printerManager = null;
            printerManager = new PrinterManager(getContext(), listenerForPurchase);
            printerManager.setIp(printerIp);

            try {
                printerManager.printPurchaseWithTokenCash(generateTokenInterface
                        .getActivitySale(), token.getTokenCashPayments(), tokenSummary
                        .getString("currency"), tokenSummary.getDouble("amount"), false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void generateQrCode(String data, String message) {
        try {
            Bitmap qrcode = QRGenerator.encodeAsBitmap(data, 400);
            if (qrcode == null) throw new WriterException("Bitmap is null");

            tokenView.setImageBitmap(qrcode);
            generateTokenInterface.gottenData();
            generateTokenInterface.showSnackbarMessage(message);
        } catch (WriterException e) {
            TextView qrError = getActivity().findViewById(R.id.qr_code_error);
            ImageView qrView = getActivity().findViewById(R.id.token_image_view);
            qrError.setVisibility(View.VISIBLE);
            qrView.setVisibility(View.INVISIBLE);

            generateTokenInterface.gottenData();
        }
    }

    public void setGenerateTokenInterface(GenerateTokenInterface generateTokenInterface) {
        this.generateTokenInterface = generateTokenInterface;
    }

    public interface GenerateTokenInterface {
        void closeFragment(String message);

        void closeFragment();

        void requestingData(String text);

        void gottenData();

        void showSnackbarMessage(String text);

        Sale getActivitySale();
    }
}
