package com.binarium.chipredes.tokencash.models;

public class Coupon
{
    private double amount;
    private String code;
    private String fechaExpiracion;
    private String fechaGeneracion;
    private String pump;
    private String pumper;

    public Coupon()
    {
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getFechaExpiracion()
    {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(String fechaExpiracion)
    {
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getFechaGeneracion()
    {
        return fechaGeneracion;
    }

    public void setFechaGeneracion(String fechaGeneracion)
    {
        this.fechaGeneracion = fechaGeneracion;
    }

    public String getPumper()
    {
        return pumper;
    }

    public void setPumper(String pumper)
    {
        this.pumper = pumper;
    }

    public String getPump()
    {
        return pump;
    }

    public void setPump(String pump)
    {
        this.pump = pump;
    }
}