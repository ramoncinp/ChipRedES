package com.binarium.chipredes.tokencash.ui.generatetoken

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.R
import com.binarium.chipredes.databinding.FragmentGenerateTokenBinding
import com.binarium.chipredes.tokencash.ui.tokencashactivity.TokenCashViewModel
import com.binarium.chipredes.utils.extensions.setVisible
import com.binarium.chipredes.utils.showSimpleDialog
import com.binarium.chipredes.wolke.models.tokencash.TokenData
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GenerateTokenFragment : Fragment() {

    private lateinit var binding: FragmentGenerateTokenBinding
    private val viewModel: GenerateTokenViewModel by viewModels()
    private val sharedViewModel: TokenCashViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.lastSaleData = sharedViewModel.saleData
        getNavigationArguments()
    }

    private fun getNavigationArguments() {
        val arguments = GenerateTokenFragmentArgs.fromBundle(requireArguments())
        viewModel.salePumper = arguments.salePumper
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        requireActivity().title = "Generar token"
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_generate_token, container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.startGenerateTokenProcess()
        initViewListeners()
        initObservers()
    }

    private fun initViewListeners() {
        binding.checkPayment.setOnClickListener {
            viewModel.getTokenPayments()
        }

        binding.closeTokenButton.setOnClickListener {
            confirmCloseToken(requireContext()) { viewModel.closeToken() }
        }
    }

    private fun initObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner, {
            binding.generateTokenContent.setVisible(it.not())
            binding.progressBar.setVisible(it)
        })

        viewModel.message.observe(viewLifecycleOwner, {
            ChipREDConstants.showSnackBarMessage(it, requireActivity())
        })

        viewModel.generatedToken.observe(viewLifecycleOwner, {
            showTokenData(it)
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, {
            showSimpleDialog(requireContext(), "Error", it) {
                requireActivity().finish()
            }
        })

        viewModel.receivedAmount.observe(viewLifecycleOwner, {
            showPaymentData(it)
        })

        viewModel.isDone.observe(viewLifecycleOwner, {
            requireActivity().finish()
        })
    }

    private fun showTokenData(token: TokenData) {
        val tokenAmount = "\$ ${ChipREDConstants.MX_AMOUNT_FORMAT.format(token.costo)}"
        binding.tokenProductAmount.text = tokenAmount
        binding.tokenValueTv.text = token.token
        binding.tokenImageView.apply {
            setImageBitmap(viewModel.tokenBitmap)
            setVisible(true)
        }
    }

    private fun showPaymentData(receivedAmount: Double) {
        val receivedAmountText = "\$ ${ChipREDConstants.MX_AMOUNT_FORMAT.format(receivedAmount)}"
        val remainingAmountText = "\$ ${
            ChipREDConstants.MX_AMOUNT_FORMAT.format(
                viewModel.lastSaleData?.costo?.minus(
                    receivedAmount
                )
            )
        }"

        binding.tokenReceivedAmount.text = receivedAmountText
        binding.tokenRemainingAmount.text = remainingAmountText

        binding.tokenPaymentRow.setVisible(true)
        binding.tokenRemainingRow.setVisible(true)

        if (receivedAmount > 0) binding.closeTokenButton.setVisible(true)
    }
}
