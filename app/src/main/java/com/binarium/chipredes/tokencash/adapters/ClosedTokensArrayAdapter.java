package com.binarium.chipredes.tokencash.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipredes.R;
import com.binarium.chipredes.tokencash.models.ClosedTokens;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ClosedTokensArrayAdapter extends RecyclerView.Adapter<ClosedTokensArrayAdapter
        .ClosedTokenViewHolder> implements View.OnClickListener
{
    private final ArrayList<ClosedTokens> closedTokens;
    private Context context;
    private View.OnClickListener listener;

    public ClosedTokensArrayAdapter(ArrayList<ClosedTokens> closedTokens, Context context)
    {
        this.closedTokens = closedTokens;
        this.context = context;
    }

    @Override
    public int getItemCount()
    {
        return closedTokens.size();
    }

    @Override
    public ClosedTokenViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                .closed_token_layout, viewGroup, false);

        ClosedTokenViewHolder closedTokenViewHolder = new ClosedTokenViewHolder(v, context,
                closedTokens);
        v.setOnClickListener(this);

        return closedTokenViewHolder;
    }

    @Override
    public void onBindViewHolder(ClosedTokensArrayAdapter.ClosedTokenViewHolder holder, int
            position)
    {
        DecimalFormat decimalFormat = new DecimalFormat("###,##0.00");
        ClosedTokens closedToken = closedTokens.get(position);

        String dateTime = closedToken.getDateTime();
        dateTime = dateTime.replace(' ', '\n');

        holder.value.setText(String.valueOf(closedToken.getTokenValue()));
        holder.dateTime.setText(dateTime);
        holder.totalAmount.setText(decimalFormat.format(closedToken.getTotalAmount()) + " " +
                closedToken.getCurrency());
        holder.receivedAmount.setText(decimalFormat.format(closedToken.getTokenAmount()) + " " +
                closedToken.getCurrency());
        holder.pendingAmount.setText(decimalFormat.format(closedToken.getTotalAmount() -
                closedToken.getTokenAmount()) + " " + closedToken.getCurrency());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    public static class ClosedTokenViewHolder extends RecyclerView.ViewHolder
    {
        private TextView value;
        private TextView dateTime;
        private TextView totalAmount;
        private TextView receivedAmount;
        private TextView pendingAmount;

        public ClosedTokenViewHolder(View itemView, Context context, ArrayList<ClosedTokens>
                closedTokens)
        {
            super(itemView);

            value = (TextView) itemView.findViewById(R.id.token_value_tv);
            dateTime = (TextView) itemView.findViewById(R.id.token_datetime);
            totalAmount = (TextView) itemView.findViewById(R.id.token_total_amount);
            receivedAmount = (TextView) itemView.findViewById(R.id.token_received_amount);
            pendingAmount = (TextView) itemView.findViewById(R.id.token_pending_amount);
        }
    }
}

