package com.binarium.chipredes.tokencash.ui.tokencashactivity

import androidx.lifecycle.ViewModel
import com.binarium.chipredes.wolke.models.LastSaleData

class TokenCashViewModel : ViewModel() {
    var saleData: LastSaleData? = null
}