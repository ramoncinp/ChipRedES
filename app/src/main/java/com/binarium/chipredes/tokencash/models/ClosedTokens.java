package com.binarium.chipredes.tokencash.models;

public class ClosedTokens
{
    private double totalAmount;
    private double tokenAmount;
    private String tokenValue;
    private String dateTime;
    private String currency;

    public ClosedTokens()
    {

    }

    public double getTotalAmount()
    {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount)
    {
        this.totalAmount = totalAmount;
    }

    public double getTokenAmount()
    {
        return tokenAmount;
    }

    public void setTokenAmount(double tokenAmount)
    {
        this.tokenAmount = tokenAmount;
    }

    public String getTokenValue()
    {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue)
    {
        this.tokenValue = tokenValue;
    }

    public String getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(String dateTime)
    {
        this.dateTime = dateTime;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }
}
