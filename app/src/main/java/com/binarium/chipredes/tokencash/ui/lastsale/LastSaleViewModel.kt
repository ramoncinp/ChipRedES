package com.binarium.chipredes.tokencash.ui.lastsale

import android.content.SharedPreferences
import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.configstation.service.StationManagerService
import com.binarium.chipredes.utils.QRGenerator
import com.binarium.chipredes.utils.network.getJsonFromBodyResponse
import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.LastSaleData
import com.binarium.chipredes.wolke.models.LastSaleRequest
import com.binarium.chipredes.wolke.models.Pumper
import com.binarium.chipredes.wolke.models.tokencash.GetCouponRequest
import com.binarium.chipredes.wolke.models.tokencash.TicketLessCoupon
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.json.JSONObject
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class LastSaleViewModel @Inject constructor(
    private val stationManagerService: StationManagerService,
    private val wolkeApiService: WolkeApiService,
    sharedPreferences: SharedPreferences
) : ViewModel() {

    var loadingPosition: String? = ""
    var couponBitmap: Bitmap? = null
    private val stationId = sharedPreferences.getString(ChipREDConstants.STATION_ID, "").toString()
    private val stationMongoId =
        sharedPreferences.getString(ChipREDConstants.STATION_MONGO_ID, "").toString()

    var salePumper: Pumper? = null

    private val _lastSale = MutableLiveData<LastSaleData>()
    val lastSale: LiveData<LastSaleData>
        get() = _lastSale

    private val _ticketLessCoupon = MutableLiveData<TicketLessCoupon>()
    val ticketLessCoupon: LiveData<TicketLessCoupon>
        get() = _ticketLessCoupon

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage


    fun getLastSale() {
        viewModelScope.launch {
            try {
                val request = LastSaleRequest(
                    idEstacion = stationId,
                    posicionCarga = loadingPosition?.toInt()
                )

                val response = wolkeApiService.getLastSale(request)
                if (response.isSuccessful) {
                    val responseBody = response.body()
                    if (responseBody == null) {
                        _errorMessage.value = "Error al procesar la informacion"
                    } else {
                        responseBody.data?.let {
                            _lastSale.value = it
                        }
                    }
                } else {
                    val errorMap = getJsonFromBodyResponse(response.errorBody())
                    val messageObj = errorMap["message"] as JSONObject
                    _errorMessage.value = messageObj.getString("aviso")
                }
            } catch (e: Exception) {
                Timber.e("Error! -> $e")
                _errorMessage.value = "Error al obtener última venta\nNo responde servidor"
            }
        }
    }

    fun getCoupon() {
        viewModelScope.launch {
            try {
                val request = GetCouponRequest(
                    idConsumo = _lastSale.value?.id,
                    numeroEstacion = stationId
                )

                val response = wolkeApiService.getTicketLessCoupon(request)
                if (response.isSuccessful) {
                    val coupon = response.body()?.data
                    coupon?.let {
                        generateCouponQR(it)
                        _ticketLessCoupon.value = it
                    }
                } else {
                    val errorMap = getJsonFromBodyResponse(response.errorBody())
                    val message = errorMap["message"] as String
                    Timber.e(message)
                }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private fun generateCouponQR(ticketLessCoupon: TicketLessCoupon) {
        val size = 256
        couponBitmap = QRGenerator.encodeAsBitmap(ticketLessCoupon.cupon.toString(), size, size)
    }

    fun getStationPumper() {
        viewModelScope.launch {
            // Buscar cliente
            Timber.i("Fetching station pumpers")

            try {
                // Crear petición
                val pumpersRequest =
                    stationManagerService.getStationPumpers(stationId = stationMongoId)

                // Obtener data
                val pumpers = pumpersRequest.data?.pumpers
                pumpers?.let { list ->

                    // Get sale pumper
                    salePumper = list.first { it.idEmax == _lastSale.value?.idDespachadorEmax }
                }

            } catch (e: Exception) {
                Timber.e("Error fetching station pumpers -> $e")
            }
        }
    }
}
