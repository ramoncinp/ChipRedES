package com.binarium.chipredes.tokencash.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.R;
import com.binarium.chipredes.Sale;
import com.binarium.chipredes.tokencash.models.TokenCashPayment;

import java.util.ArrayList;

public class TokencashPurchaseAdapter extends RecyclerView.Adapter<TokencashPurchaseAdapter.TokencashPurchaseViewHolder>
{
    private ArrayList<ArrayList<TokenCashPayment>> tokencashPayments;
    private ArrayList<Sale> sales;
    private View.OnClickListener listener, syncListener;
    private Context context;

    public TokencashPurchaseAdapter(ArrayList<ArrayList<TokenCashPayment>> tokencashPayments,
                                    ArrayList<Sale> sales, Context context)
    {
        this.tokencashPayments = tokencashPayments;
        this.sales = sales;
        this.context = context;
    }

    public void setPrintButtonListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    public void setSyncButtonListener(View.OnClickListener listener)
    {
        this.syncListener = listener;
    }

    @NonNull
    @Override
    public TokencashPurchaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_tokencash_purchase, viewGroup, false);

        return new TokencashPurchaseViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final TokencashPurchaseViewHolder tokencashPurchaseViewHolder
            , int i)
    {
        //Obtener objetos
        ArrayList<TokenCashPayment> mPayments = tokencashPayments.get(i);
        Sale sale = sales.get(i);

        //Obtener total de los pagos tokencash
        double total = 0;
        for (TokenCashPayment payment : mPayments)
        {
            total += payment.getAmount();
        }

        //Formatear cantidades
        String saleAmount = "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(sale.getCosto());
        String saleVolume = ChipREDConstants.VOLUME_FORMAT.format(sale.getCantidad()) + "L";
        String tokenAmount = "Pago: $" + ChipREDConstants.MX_AMOUNT_FORMAT.format(total);

        //Obtener indice de separador de fechaHora
        int idx = sale.getFechaHora().indexOf(" ");
        String date = sale.getFechaHora().substring(0, idx);
        String time = sale.getFechaHora().substring(idx + 1);

        //Mostrar datos de consumo
        tokencashPurchaseViewHolder.saleAmount.setText(saleAmount);
        tokencashPurchaseViewHolder.saleVolume.setText(saleVolume);
        tokencashPurchaseViewHolder.tokenAmount.setText(tokenAmount);
        tokencashPurchaseViewHolder.saleDate.setText(date);
        tokencashPurchaseViewHolder.saleTime.setText(time);
        tokencashPurchaseViewHolder.saleProduct.setText(sale.getProducto());

        if (sale.getToken().isEmpty())
        {
            tokencashPurchaseViewHolder.tokenCode.setVisibility(View.GONE);
        }
        else
        {
            tokencashPurchaseViewHolder.tokenCode.setText("Token: " + sale.getToken());
        }

        //Evaluar si tiene pagos
        if (total == 0)
        {
            //Hacer fondo rojo
            ((CardView) tokencashPurchaseViewHolder.itemView).setCardBackgroundColor(ContextCompat
                    .getColor(context, R.color.colorPrimary));

            //Ocultar cardView de impresion
            tokencashPurchaseViewHolder.printButton.setVisibility(View.GONE);
            tokencashPurchaseViewHolder.syncButton.setVisibility(View.VISIBLE);

            //Definir listener a cardview
            tokencashPurchaseViewHolder.syncButton.setTag(i);
            tokencashPurchaseViewHolder.syncButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (syncListener != null)
                    {
                        syncListener.onClick(v);
                    }
                }
            });
        }
        else
        {
            //Hacer fondo rojo
            ((CardView) tokencashPurchaseViewHolder.itemView).setCardBackgroundColor(ContextCompat
                    .getColor(context, R.color.colorAccent));

            //Mostrar cardView de impresion
            tokencashPurchaseViewHolder.printButton.setVisibility(View.VISIBLE);
            tokencashPurchaseViewHolder.syncButton.setVisibility(View.GONE);

            //Definir listener a cardview
            tokencashPurchaseViewHolder.printButton.setTag(i);
            tokencashPurchaseViewHolder.printButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (listener != null)
                    {
                        listener.onClick(v);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount()
    {
        return tokencashPayments.size();
    }

    static class TokencashPurchaseViewHolder extends RecyclerView.ViewHolder
    {
        private TextView saleTime;
        private TextView saleDate;
        private TextView saleAmount;
        private TextView saleVolume;
        private TextView saleProduct;
        private TextView tokenAmount;
        private TextView tokenCode;
        private CardView printButton;
        private CardView syncButton;

        TokencashPurchaseViewHolder(@NonNull View itemView)
        {
            super(itemView);

            tokenAmount = itemView.findViewById(R.id.token_amount);
            tokenCode = itemView.findViewById(R.id.token_code);
            saleAmount = itemView.findViewById(R.id.sale_amount);
            saleVolume = itemView.findViewById(R.id.sale_volume);
            saleTime = itemView.findViewById(R.id.sale_time);
            saleDate = itemView.findViewById(R.id.sale_date);
            saleProduct = itemView.findViewById(R.id.sale_product);
            printButton = itemView.findViewById(R.id.print_tokencash_purchase);
            syncButton = itemView.findViewById(R.id.sync_tokencash_purchase);
        }
    }
}
