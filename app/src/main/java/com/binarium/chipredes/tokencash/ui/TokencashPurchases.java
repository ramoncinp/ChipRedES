package com.binarium.chipredes.tokencash.ui;

import android.app.Dialog;
import android.app.TimePickerDialog;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.R;
import com.binarium.chipredes.Sale;
import com.binarium.chipredes.printer.PrinterManager;
import com.binarium.chipredes.printer.PrintersDialogFragment;
import com.binarium.chipredes.tokencash.adapters.TokencashPurchaseAdapter;
import com.binarium.chipredes.tokencash.models.TokenCashPayment;
import com.binarium.chipredes.tokencash.repository.TokenCashManager;
import com.binarium.chipredes.utils.DatePickerFragment;
import com.binarium.chipredes.utils.DateUtils;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

public class TokencashPurchases extends AppCompatActivity {
    //Constantes
    private static final String TAG = TokencashPurchases.class.getSimpleName();
    public static final String DAYS_BEFORE = "daysBefore";
    private final int SALES_LIMIT = 15;

    //Variables
    private boolean allDocuments = false;
    private boolean fetchingMoreDocuments = false;
    private int offset;
    private int daysBefore = -3;
    private String printerIp;
    private String tokenCashScenario;

    //Views
    private RecyclerView list;
    private PrintersDialogFragment printersDialogFragment;
    private ProgressBar paginationProgress;
    private ProgressBar progressBar;
    private TextView datesRangeTv;

    //Listas
    private final ArrayList<Sale> sales = new ArrayList<>();
    private final ArrayList<ArrayList<TokenCashPayment>> paymentsList = new ArrayList<>();

    //Objetos
    private ChipRedManager chipRedManager;
    private final Date startDate = new Date();
    private final Date endDate = DateUtils.getLastTimeOfDay();
    private PrinterManager printerManager;
    private Sale selectedSale;
    private TokencashPurchaseAdapter adapter;

    private final PrinterManager.PrinterListener listenerForPurchase = new PrinterManager
            .PrinterListener() {
        @Override
        public void onSuccesfulPrint(String msg) {
            runOnUiThread(() -> {
                Toast.makeText(TokencashPurchases.this, "Impresión realizada correctamente",
                        Toast.LENGTH_SHORT).show();

                list.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            });
            Timber.d("Impresión realizada correctamente");
        }

        @Override
        public void onPrinterError(String msg) {
            runOnUiThread(() -> {
                Timber.d("Error al imprimir consumo");
                Toast.makeText(TokencashPurchases.this, "Error al tratar de imprimir",
                        Toast.LENGTH_SHORT).show();

                list.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            });
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tokencash_purchases);
        setTitle("Consumos tokencash");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        datesRangeTv = findViewById(R.id.date_range_title);
        progressBar = findViewById(R.id.progress_bar);
        paginationProgress = findViewById(R.id.pagination_progress_bar);
        list = findViewById(R.id.token_purchases);

        if (getIntent().hasExtra(DAYS_BEFORE)) {
            daysBefore = getIntent().getIntExtra(DAYS_BEFORE, -3);
        }

        // Obtener escenario tokencash
        tokenCashScenario = ChipREDConstants.getTokencashScenario(this);

        // Definir fecha inicial como hoy hace 3 días si no hay filtro, si sí, 1 día
        startDate.setTime(DateUtils.getDeltaDate(new Date(), daysBefore).getTime());

        //Inicializar y pedir datos
        getTokenPurchases();

        //Mostrar impresoras
        showPrinterSelector();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tc_sales_menu, menu);
        menu.findItem(R.id.date_range).setVisible(!tokenCashScenario.equals("e3"));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.finish();
        } else if (id == R.id.date_range) {
            showDateSelectorDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initChipRedManager() {
        // Validar si ya esta creado
        if (chipRedManager != null) {
            return;
        }

        // Crear instancia
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                if (webServiceN == ChipRedManager.CLOSE_TOKEN) {
                    // Reiniciar variables de paginación
                    offset = 0;
                    allDocuments = false;
                    adapter = null;

                    //Obtener información de tokens otra vez
                    getTokenPurchases();
                } else {
                    paginationProgress.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                    fetchingMoreDocuments = false;
                }

                Timber.d(crMessage);
                ChipREDConstants.showSnackBarMessage(crMessage, TokencashPurchases.this);
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                progressBar.setVisibility(View.GONE);
                paginationProgress.setVisibility(View.GONE);
                list.setVisibility(View.VISIBLE);

                Timber.d(errorMessage);
                ChipREDConstants.showSnackBarMessage(errorMessage, TokencashPurchases.this);

                if (webServiceN == ChipRedManager.GET_TOKENCASH_PURCHASES)
                    fetchingMoreDocuments = false;
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                Timber.d(response.toString());

                if (webServiceN == ChipRedManager.GET_TOKENCASH_PURCHASES) {
                    //Parsear datos y mostrarlos
                    showPurchases(response);
                    progressBar.setVisibility(View.GONE);
                    paginationProgress.setVisibility(View.GONE);
                } else if (webServiceN == ChipRedManager.GET_TOKEN) {
                    try {
                        Timber.d("TokenData -> %s", response.toString(2));
                        syncTokenInfo(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                fetchingMoreDocuments = false;
            }
        }, this);
    }

    private void getTokenPurchases() {
        if (tokenCashScenario.equals("e3")) {
            TokenCashManager.getStationTokens(this, new TokenCashManager.onMessageReceived() {
                @Override
                public void showResponse(JSONObject response) {
                    Timber.d(response.toString());
                    showPurchases(adjustTokenPurchases(response));
                    progressBar.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError(String message) {
                    Toast.makeText(TokencashPurchases.this, message, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            initChipRedManager();
            getWolkeTokenSales();
        }
    }

    private void getWolkeTokenSales() {

        // Mostrar fechas en título
        setDatesTitle();

        //Hacer petición para obtener consumos tokencash
        chipRedManager.getTokencashPurchases(offset, SALES_LIMIT, startDate, endDate, "");
    }

    private void showPurchases(JSONObject response) {
        try {
            if (offset == 0) {
                //Limpiar listas
                sales.clear();
                paymentsList.clear();
            }

            //Obtener arreglo de consumos
            JSONArray jsonArray = response.getJSONArray("data");

            //Obtener cada uno de los consumos
            for (int i = 0; i < jsonArray.length(); i++) {
                //Obtener objeto principal
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                //Crear objeto de venta
                final Sale sale = new Sale();
                sale.setCantidad(jsonObject.getDouble("cantidad"));
                sale.setCosto(jsonObject.getDouble("costo"));
                sale.setFechaHora(jsonObject.getString("fecha_hora"));
                sale.setNumDispensario(jsonObject.getInt("numero_dispensario"));
                sale.setPosicionDeCarga(jsonObject.getString("posicion_carga"));
                sale.setPrecioUnitario(jsonObject.getDouble("precio_unitario"));

                //Obtener objeto de datos de estacion
                JSONObject estacion = jsonObject.getJSONObject("estacion");
                sale.setProducto(estacion.getJSONObject("productos").getString("descripcion"));
                sale.setPumper(estacion.getJSONObject("despachadores").getString("nombre"));

                //Crear lista para pagos hecho a la transaccion
                ArrayList<TokenCashPayment> tokenCashPayments = new ArrayList<>();

                //Validar datos
                if (jsonObject.has("token")) {
                    JSONObject saleToken = jsonObject.getJSONObject("token");
                    if (saleToken.has("token") && saleToken.has("transaccion")) {
                        sale.setToken(saleToken.getString("token"));
                        sale.setTokenTransaction(saleToken.getString("transaccion"));
                        sale.setTokenId(saleToken.getString("_id"));
                    }

                    //Validar que existan pago de tokencash
                    if (saleToken.has("payments")) {
                        //Obtener objeto de pagos tokencash
                        JSONArray payments = saleToken.getJSONArray("payments");
                        for (int p = 0; p < payments.length(); p++) {
                            //Obtener elemento del arreglo
                            JSONObject payment = payments.getJSONObject(p);

                            // Obtener detalles
                            JSONObject details = payment.getJSONObject("details");
                            JSONObject to = (details.has("to") ? details.getJSONObject("to") :
                                    details.getJSONObject("details_to"));

                            //Crear objeto de pago tokencash
                            TokenCashPayment tokencashPayment = new TokenCashPayment();
                            tokencashPayment.setAmount(to.getDouble("amount"));
                            tokencashPayment.setCurrency(to.getString("currency"));
                            tokencashPayment.setDateTime(payment.getString("datetime"));

                            tokenCashPayments.add(tokencashPayment);
                        }
                    }
                }

                //Agregar objetos a las listas globales
                sales.add(sale);
                paymentsList.add(tokenCashPayments);
            }

            // Validar si el arreglo esta vacío
            if (jsonArray.length() == 0)
                allDocuments = true;

            if (adapter == null) {
                //Crear adaptador
                adapter = new TokencashPurchaseAdapter(paymentsList, sales, this);

                //Agregar clickListener
                adapter.setPrintButtonListener(v -> {
                    //Obtener posicion del consumo
                    int idx = (int) v.getTag();

                    //Mandar a imprimir el consumo
                    printTokencashPurchase(sales.get(idx), paymentsList.get(idx));
                });

                //Agregar listener para sincronizar pago
                adapter.setSyncButtonListener(v -> {
                    ChipREDConstants.showSnackBarMessage("Obteniendo información...",
                            TokencashPurchases.this);

                    //Obtener posicion del consumo
                    int idx = (int) v.getTag();

                    //Obtener transaccion del token
                    selectedSale = sales.get(idx);
                    String transaction = selectedSale.getTokenTransaction();

                    if (tokenCashScenario.equals("e3")) {
                        TokenCashManager.getTokenPayments(TokencashPurchases.this, transaction,
                                new TokenCashManager.onMessageReceived() {
                                    @Override
                                    public void showResponse(JSONObject response1) {
                                        syncTokenInfo(adjustTokenPayments(response1));
                                    }

                                    @Override
                                    public void onError(String message) {
                                        ChipREDConstants.showSnackBarMessage(message,
                                                TokencashPurchases.this);
                                    }
                                });
                    } else {
                        //Obtener información del token directo de TC
                        chipRedManager.getTokenInfo(transaction);
                    }
                });

                //Definir Adaptador y LayoutManager
                final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                list.setLayoutManager(linearLayoutManager);
                list.setAdapter(adapter);
                list.setVisibility(View.VISIBLE);

                //Definir scroll listener para paginación si no es escenario 3
                final RecyclerView.OnScrollListener scrollChangeListener =
                        new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx,
                                                   int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                Timber.tag(TAG).v("Obteniendo mas consumos tokencash");

                                int visibleItemCount = linearLayoutManager.getChildCount();
                                int totalItemCount = sales.size();
                                int firstVisibleItemPosition =
                                        linearLayoutManager.findFirstVisibleItemPosition();

                                if (visibleItemCount + firstVisibleItemPosition == totalItemCount && !fetchingMoreDocuments && !allDocuments) {
                                    // Pedir más elementos
                                    fetchingMoreDocuments = true;
                                    paginationProgress.setVisibility(View.VISIBLE);

                                    // Aumentar offset
                                    offset += SALES_LIMIT;

                                    // Ejecutar servicio para más documentos
                                    getWolkeTokenSales();
                                }
                            }
                        };

                // Agregar scroll listener si no es escenario 3
                if (!tokenCashScenario.equals("e3"))
                    list.addOnScrollListener(scrollChangeListener);
            }

            //Notificar camibios en lista
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error al procesar la información", Toast.LENGTH_SHORT).show();
            Timber.d("Error JSON -> %s", e.getMessage());
        }
    }

    private void printTokencashPurchase(final Sale sale,
                                        final ArrayList<TokenCashPayment> tokenCashPayments) {
        if (printerIp == null || printerIp.isEmpty()) {
            ChipREDConstants.showSnackBarMessage("No hay una impresora seleccionada", this);
        }

        list.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        //Imprimir recibo
        new Thread(() -> {
            printerManager = null;
            printerManager = new PrinterManager(TokencashPurchases.this, listenerForPurchase);
            printerManager.setIp(printerIp);

            double total = 0;
            for (TokenCashPayment mPayment : tokenCashPayments) {
                total += mPayment.getAmount();
            }

            printerManager.printPurchaseWithTokenCash(sale, tokenCashPayments, "MXN",
                    total, true);
        }).start();
    }

    private void syncTokenInfo(JSONObject response) {
        try {
            //Comprobar que tenga algún pago
            JSONObject data = response.getJSONObject("data");

            //Obtener pagos
            double amount = data.getJSONObject("summary").getDouble("amount");

            //Validar si tiene pagos
            if (amount > 0) {
                //Mostrar progress
                list.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);

                //Enviar servicio para cerrar token
                if (tokenCashScenario.equals("e3")) {
                    TokenCashManager.closeToken(this, selectedSale.getTokenTransaction(),
                            selectedSale.getPumperId(),
                            new TokenCashManager.onMessageReceived() {
                                @Override
                                public void showResponse(JSONObject response) {
                                    getTokenPurchases();
                                }

                                @Override
                                public void onError(String message) {
                                    ChipREDConstants.showSnackBarMessage(message,
                                            TokencashPurchases.this);
                                }
                            });
                } else {
                    chipRedManager.closeToken(selectedSale.getTokenId(), selectedSale.getTokenTransaction(), data.getJSONArray("payments"),
                            data.getJSONObject("summary"));
                }
            } else {
                ChipREDConstants.showSnackBarMessage("No hay pagos para este token",
                        TokencashPurchases.this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error al procesar la información", Toast.LENGTH_SHORT).show();
        }
    }

    private void showPrinterSelector() {
        printersDialogFragment = new PrintersDialogFragment();
        printersDialogFragment.setPrinterDialogInterface(ip -> {
            printerIp = ip;
            printersDialogFragment.dismiss();
        });

        printersDialogFragment.show(getSupportFragmentManager(), "printer_dialog");
    }

    private JSONObject adjustTokenPurchases(JSONObject tokenPurchases) {
        try {
            // Preparar objeto de salida
            JSONObject adjustedObject = new JSONObject();
            JSONArray adjustedArray = new JSONArray();

            // Obtener arreglo de pagos con tokencash
            JSONArray tokens = tokenPurchases.getJSONArray("data");

            // Ajustar cada token
            for (int i = 0; i < tokens.length(); i++) {
                JSONObject tokenSaleObject = tokens.getJSONObject(i);

                // Crear objeto para Token
                JSONObject tokenData = new JSONObject();
                tokenData.put("transaccion", tokenSaleObject.getString("transaction"));
                tokenData.put("estatus", tokenSaleObject.getString("status"));
                tokenData.put("token", tokenSaleObject.getString("token"));
                tokenData.put("payments", tokenSaleObject.getJSONArray("payments"));
                tokenSaleObject.put("token", tokenData);

                // Obtener consumo
                JSONObject sale = tokenSaleObject.getJSONObject("sale");

                // Calcular dato faltante
                int dispSide = sale.getJSONObject("dispenser").getInt("side");
                int dispNumber = dispSide % 2 == 0 ? dispSide / 2 : (dispSide + 1) / 2;

                // Convertir datos de consumo
                tokenSaleObject.put("cantidad", sale.getDouble("quantity"));
                tokenSaleObject.put("numero_dispensario", dispNumber);
                tokenSaleObject.put("fecha_hora", sale.getString("dispatched_at"));
                tokenSaleObject.put("costo", sale.getDouble("amount"));
                tokenSaleObject.put("posicion_carga", String.valueOf(dispSide));
                tokenSaleObject.put("id_despachador_emax",
                        sale.getJSONObject("seller").getString("id"));
                tokenSaleObject.put("precio_unitario", sale.getJSONObject("product").getDouble(
                        "price"));

                // Crear objetos de estacion
                JSONObject estacion = new JSONObject();
                JSONObject productos = new JSONObject();
                productos.put("descripcion", sale.getJSONObject("product").getString("name"));
                estacion.put("productos", productos);
                JSONObject despachadores = new JSONObject();
                despachadores.put("nombre", sale.getJSONObject("seller").getString("name"));
                estacion.put("despachadores", despachadores);

                // Agregar datos de estacion
                tokenSaleObject.put("estacion", estacion);

                // Agregar al arreglo de salida
                adjustedArray.put(tokenSaleObject);
            }

            // Agregar arreglo ajustado a objeto que se retornará
            adjustedObject.put("data", adjustedArray);

            // Retornar objeto
            return adjustedObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Si algo salió mal, retornar objeto vacío
        return new JSONObject();
    }

    private JSONObject adjustTokenPayments(JSONObject response) {
        // Crear nuevo objeto JSON
        JSONObject root = new JSONObject();
        JSONObject data;

        try {
            // Guardar datos en "data"
            data = response;
            root.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return root;
    }

    private void showDateSelectorDialog() {
        // Crear builder
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(this);

        // Definir titulo
        builder.setTitle("Seleccionar fechas");

        // Crear view
        View content = getLayoutInflater().inflate(R.layout.dialog_set_dates_range, null);

        // Definir view
        builder.setView(content);

        // Obtener views
        final MaterialEditText startDateEt = content.findViewById(R.id.start_date);
        final MaterialEditText endDateEt = content.findViewById(R.id.end_date);
        Button cancelButton = content.findViewById(R.id.cancel_button);
        Button submitButton = content.findViewById(R.id.submit_button);

        // Mostrar fechas actuales
        startDateEt.setText(DateUtils.dateObjectToString(startDate));
        endDateEt.setText(DateUtils.dateObjectToString(endDate));

        // Definir datePickers
        View.OnClickListener datePickerListener = view -> {
            DatePickerFragment fragment = DatePickerFragment.newInstance((datePicker, i, i1, i2) -> {
                final String gottenDate =
                        ChipREDConstants.twoDigits(i2) + "/" + DateUtils.monthNumberToString
                                (i1) + "/" + i;

                final MaterialEditText dateEt = (MaterialEditText) view;
                dateEt.setText(gottenDate);

                //Obtener hora actual
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                //Crear instancia de timePicker
                TimePickerDialog timePickerDialog =
                        new TimePickerDialog(TokencashPurchases.this,
                                (view1, hourOfDay, minute1) -> {
                                    String hourStr = String.format(Locale.getDefault(),
                                            "%02d:%02d", hourOfDay, minute1);

                                    String allDate = gottenDate + " " + hourStr;
                                    dateEt.setText(allDate);
                                }, hour, minute,
                                true);

                timePickerDialog.setTitle("Seleccione la hora");
                timePickerDialog.show();
            });

            fragment.show(getSupportFragmentManager(), "datePicker");
        };

        // Asignar datePickers
        startDateEt.setOnClickListener(datePickerListener);
        endDateEt.setOnClickListener(datePickerListener);

        // Crear diálogo
        final Dialog dialog = builder.create();
        dialog.show();

        // Asignar listener de botones
        submitButton.setOnClickListener(v -> {
            // Validar
            boolean valid = startDateEt.validateWith(new METValidator("Fecha no válida") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    return text.length() == 17;
                }
            });
            valid &= endDateEt.validateWith(new METValidator("Fecha no válida") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    return text.length() == 17;
                }
            });

            // Si es valido...
            if (valid) {
                Date startDateObj =
                        DateUtils.dateStringToObject(startDateEt.getText().toString());
                Date endDateObj =
                        DateUtils.dateStringToObject(endDateEt.getText().toString());

                if (startDateObj == null || endDateObj == null) {
                    Toast.makeText(TokencashPurchases.this, "Error al procesar fechas " +
                            "seleccionadas", Toast.LENGTH_SHORT).show();
                } else {
                    // Validar que la fecha final sea mayor
                    if (endDateObj.getTime() > startDateObj.getTime()) {
                        // Definir las nuevas fechas
                        TokencashPurchases.this.startDate.setTime(startDateObj.getTime());
                        TokencashPurchases.this.endDate.setTime(endDateObj.getTime());

                        // Ocultar views
                        progressBar.setVisibility(View.VISIBLE);
                        list.setVisibility(View.GONE);

                        // Mostrar fechas en título
                        setDatesTitle();

                        // Hacer offset cero y hacer falso todos los documentos
                        offset = 0;
                        allDocuments = false;
                        adapter = null;

                        //Obtener ventas
                        getWolkeTokenSales();

                        // Cerrar diálogo
                        dialog.dismiss();
                    } else {
                        startDateEt.setError("Fecha inválida");
                        endDateEt.setError("Fecha inválida");
                    }
                }
            }
        });

        cancelButton.setOnClickListener(v -> dialog.dismiss());
    }

    private void setDatesTitle() {
        String title = getDatesTitle();
        datesRangeTv.setText(title);
        datesRangeTv.setVisibility(View.VISIBLE);
    }

    private String getDatesTitle() {
        // Obtener texto de las fechas
        return DateUtils.dateObjectToString(startDate) + " - " + DateUtils.dateObjectToString(endDate);
    }
}
