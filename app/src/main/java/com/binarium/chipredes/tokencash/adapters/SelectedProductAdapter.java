package com.binarium.chipredes.tokencash.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.binarium.chipredes.R;
import com.binarium.chipredes.tokencash.models.SelectedProduct;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

public class SelectedProductAdapter extends RecyclerView.Adapter<SelectedProductAdapter.SelectedProductViewHolder> {
    private ArrayList<SelectedProduct> selectedProducts;

    public SelectedProductAdapter(ArrayList<SelectedProduct> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    @NonNull
    @Override
    public SelectedProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v =
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tc_selected_products
                        , viewGroup, false);

        return new SelectedProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedProductViewHolder selectedProductViewHolder,
                                 int i) {
        //Obtener producto
        SelectedProduct selectedProduct = selectedProducts.get(i);

        //Mostrar informacion en views
        selectedProductViewHolder.nameTv.setText(selectedProduct.getName());
        selectedProductViewHolder.activeCb.setChecked(selectedProduct.isActive());
        if (selectedProduct.getPercentage() != 0) {
            selectedProductViewHolder.percentageEt.setText(String.valueOf(selectedProduct.getPercentage()));
        }
    }

    @Override
    public int getItemCount() {
        return selectedProducts.size();
    }

    public static class SelectedProductViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTv;
        public CheckBox activeCb;
        public MaterialEditText percentageEt;

        SelectedProductViewHolder(@NonNull View itemView) {
            super(itemView);

            nameTv = itemView.findViewById(R.id.tc_product_name);
            activeCb = itemView.findViewById(R.id.checkBox);
            percentageEt = itemView.findViewById(R.id.tc_product_percentage);
        }
    }
}
