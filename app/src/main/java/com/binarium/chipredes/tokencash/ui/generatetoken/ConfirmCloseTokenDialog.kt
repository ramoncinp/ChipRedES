package com.binarium.chipredes.tokencash.ui.generatetoken

import android.app.AlertDialog
import android.content.Context

fun confirmCloseToken(context: Context, onConfirmed: () -> Unit) {
    val dialogBuilder = AlertDialog.Builder(context)
    dialogBuilder.setTitle("Cerrar token")
    dialogBuilder.setMessage("Ya no se podrán recibir más pagos")
    dialogBuilder.setPositiveButton(
        "Continuar"
    ) { dialog, _ ->
        onConfirmed()
        dialog.dismiss()
    }
    dialogBuilder.setNegativeButton("Regresar") { dialog, _ ->
        dialog.dismiss()
    }

    val dialog = dialogBuilder.create()
    dialog.show()
}
