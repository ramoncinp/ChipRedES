package com.binarium.chipredes.tokencash.ui;

import android.content.Intent;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.PaymentMethodsList;
import com.binarium.chipredes.R;
import com.binarium.chipredes.Sale;

import timber.log.Timber;

public class GenerateToken extends AppCompatActivity {
    //Variables
    private int loadingPosition;

    //Views
    private RelativeLayout progressBarLayout;
    private TextView progressBarText;
    private TextView continueButton;
    private TextView cancelButton;
    private FrameLayout frameLayout;

    //Objetos
    private Sale lastSale;
    private LastSaleFragmentOld lastSaleFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_token);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Obtener la posicion de carga seleccionada
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            loadingPosition = bundle.getInt(ChipREDConstants.SELECTED_LOADING_POSITION);

        //Asignar views a sus respectivos objetos
        progressBarLayout = findViewById(R.id.tokencash_progress_bar_layout);
        progressBarText = findViewById(R.id.tokencash_progress_bar_text);
        cancelButton = findViewById(R.id.cancel_tokencash_payment);
        continueButton = findViewById(R.id.continue_tokencash_payment);
        frameLayout = findViewById(R.id.tokencash_fragment_container);

        //Mostrar en log la posicion de carga seleccionada
        Timber.d("Posicion seleccionada = %s", loadingPosition);

        //Asignar listeners a los botones
        setCancelButtonListener();
        setContinueButtonListener();

        //Crear fragment para mostrar datos de última venta
        setLastSaleFragment();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setLastSaleFragment() {
        lastSaleFragment = new LastSaleFragmentOld();
        lastSaleFragment.setLoadingPosition(loadingPosition);
        lastSaleFragment.setLastSaleInterface(new LastSaleFragmentOld.LastSaleInterface() {
            @Override
            public void returnLastSale(Sale sale) {
                lastSale = sale;
            }

            @Override
            public void requestingData() {
                showProgressBar("Obteniendo última venta");
            }

            @Override
            public void gottenData(boolean continuable) {
                hideProgressBar();
                if (continuable) continueButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void setNextFragment() {
                findViewById(R.id.pending_tokens_cv).setVisibility(View.GONE);
                continueButton.setVisibility(View.VISIBLE);
                setGenerateTokenFragment();
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.tokencash_fragment_container,
                lastSaleFragment).commit();
    }

    private void setGenerateTokenFragment() {
        GenerateTokenFragmentOld generateTokenFragment = new GenerateTokenFragmentOld();
        generateTokenFragment.setGenerateTokenInterface(new GenerateTokenFragmentOld
                .GenerateTokenInterface() {
            @Override
            public void closeFragment(String message) {
                showMessageAndFinish(message);
            }

            @Override
            public void closeFragment() {
                returnToLoadingPositions();
            }

            @Override
            public void requestingData(String text) {
                frameLayout.setVisibility(View.INVISIBLE);
                showProgressBar(text);
            }

            @Override
            public void gottenData() {
                hideProgressBar();
                frameLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void showSnackbarMessage(String text) {
                GenerateToken.this.showSnackbarMessage(text);
            }

            @Override
            public Sale getActivitySale() {
                return lastSale;
            }
        });

        Bundle bundle = new Bundle();
        bundle.putString(ChipREDConstants.PURCHASE_ID_FOR_TOKENCASH, lastSale.getId());
        bundle.putDouble(ChipREDConstants.PURCHASE_AMOUNT_FOR_TOKENCASH, lastSale.getCosto());
        bundle.putInt(ChipREDConstants.PUMP_NUMBER_FOR_TOKEN_CASH, loadingPosition);
        generateTokenFragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.tokencash_fragment_container,
                generateTokenFragment).commit();

        continueButton.setVisibility(View.INVISIBLE);
    }

    private void setContinueButtonListener() {
        continueButton.setOnClickListener(view -> {
            if (lastSaleFragment.getSalePumper() != null) //Venta ya asignada
            {
                lastSaleFragment.askPumperNip();
            } else {
                lastSaleFragment.showNoPumperSelected();
            }
        });

        continueButton.setVisibility(View.INVISIBLE);
    }

    private void setCancelButtonListener() {
        cancelButton.setOnClickListener(view -> returnToLoadingPositions());
    }

    public void showProgressBar(String text) {
        progressBarText.setText(text);
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    public void showMessageAndFinish(String message) {
        GenericDialog genericDialog = new GenericDialog(
                "Error",
                message,
                this::returnToLoadingPositions,
                null,
                this);
        genericDialog.show();
    }

    private void returnToLoadingPositions() {
        Intent resultIntent = new Intent();
        setResult(PaymentMethodsList.REQUEST_TOKENCASH_PAYMENT, resultIntent);
        finish();
    }

    private void showSnackbarMessage(String text) {
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG).show();
    }
}
