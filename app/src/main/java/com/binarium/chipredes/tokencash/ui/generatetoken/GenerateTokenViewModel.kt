package com.binarium.chipredes.tokencash.ui.generatetoken

import android.content.SharedPreferences
import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.local.services.Printer
import com.binarium.chipredes.printer.kt.PrinterManagerKt
import com.binarium.chipredes.printer.domain.usecases.GetLocalPrinterUseCase
import com.binarium.chipredes.tokencash.domain.usecases.CloseTokenUseCase
import com.binarium.chipredes.utils.QRGenerator
import com.binarium.chipredes.utils.network.isWolkeResponseSuccessful
import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.LastSaleData
import com.binarium.chipredes.wolke.models.Pumper
import com.binarium.chipredes.wolke.models.exception.WolkeException
import com.binarium.chipredes.wolke.models.tokencash.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.Response
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class GenerateTokenViewModel @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val closeTokenUseCase: CloseTokenUseCase,
    private val getLocalPrinterUseCase: GetLocalPrinterUseCase,
    private val printerManager: PrinterManagerKt,
    private val wolkeApiService: WolkeApiService,
) : ViewModel() {

    val stationId = sharedPreferences.getString(ChipREDConstants.STATION_ID, "").toString()

    var tokenBitmap: Bitmap? = null
    var lastSaleData: LastSaleData? = null
    var tokenCashConfig: TokenCashConfigurationData? = null
    var tokenPaymentsData: TokenPaymentsData? = null
    var printer: Printer? = null
    var salePumper: Pumper? = null

    private val _generatedToken = MutableLiveData<TokenData>()
    val generatedToken: LiveData<TokenData>
        get() = _generatedToken

    private val _receivedAmount = MutableLiveData<Double>()
    val receivedAmount: LiveData<Double>
        get() = _receivedAmount

    private val _message = MutableLiveData<String>()
    val message: LiveData<String>
        get() = _message

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _isDone = MutableLiveData<Boolean>()
    val isDone: LiveData<Boolean>
        get() = _isDone


    fun startGenerateTokenProcess() {
        viewModelScope.launch {
            _isLoading.value = true
            try {
                fetchTokenConfig()
                generateToken()
                _isLoading.value = false
            } catch (e: WolkeException) {
                Timber.e(e)
                _errorMessage.value = e.message
            } catch (e: Exception) {
                Timber.e(e)
                _errorMessage.value = "Error al generar token"
            }
        }
    }

    @Throws(WolkeException::class)
    private suspend fun fetchTokenConfig() {
        val request = GetTokenCashConfigRequest(numeroEstacion = stationId)
        val response = wolkeApiService.getTokenCashConfig(request)
        if (response.isSuccessful) {
            response.body()?.let { body ->
                if (isWolkeResponseSuccessful(body.response!!)) {
                    tokenCashConfig = response.body()?.data
                } else {
                    throw WolkeException(body.message!!)
                }
            }
        } else {
            throw WolkeException("Error al obtener configuración tokencash")
        }
    }

    private suspend fun generateToken() {
        val request = GenerateTokenRequest(
            numeroEstacion = stationId,
            id = lastSaleData?.id.toString(),
            costo = lastSaleData?.costo!!,
            loadingPosition = lastSaleData?.posicionCarga?.toInt()!!
        )
        val response = wolkeApiService.generateToken(request)
        if (response.isSuccessful) {
            response.body()?.let { body ->
                if (isWolkeResponseSuccessful(body.response!!)) {
                    val token = body.data
                    onReceivedToken(token)
                } else {
                    body.message?.let { _errorMessage.value = it }
                }
            }
        } else {
            _errorMessage.value = "Error al generar token, no responde servidor"
        }
    }

    private fun generateTokenQR(tokenData: TokenData?) {
        val size = 256
        tokenBitmap = QRGenerator.encodeAsBitmap(tokenData?.token.toString(), size, size)
    }

    private fun onReceivedToken(token: TokenData?) {
        generateTokenQR(token)
        token?.let { _generatedToken.value = it }
        getPrinter()
    }

    fun getTokenPayments() {
        viewModelScope.launch {
            _isLoading.value = true
            try {
                val request = GetTokenPaymentsRequest(
                    numeroEstacion = stationId,
                    transaccion = _generatedToken.value?.transaccion
                )
                val response = wolkeApiService.getTokenPayments(request)
                if (response.isSuccessful) {
                    setTokenPayments(response)
                } else {
                    _errorMessage.value = "Error al obtener pagos"
                }
            } catch (e: Exception) {
                Timber.e(e)
                _errorMessage.value = "Error al obtener pagos"
            }
            _isLoading.value = false
        }
    }

    private fun setTokenPayments(response: Response<GetTokenPaymentsResponse>) {
        tokenPaymentsData = response.body()?.data
        val summary = tokenPaymentsData?.summary?.amount
        summary?.let {
            _receivedAmount.value = it
        }
    }

    private fun evaluatePrintTokenTask() {
        val shouldPrintToken = tokenCashConfig?.impresionTokenCobro ?: true
        if (shouldPrintToken) {
            printToken()
        } else {
            _message.value = "Impresión de token desactivada"
        }
    }

    private fun getPrinter() {
        viewModelScope.launch {
            lastSaleData?.posicionCarga?.let { loadingPosition ->
                printer = getLocalPrinterUseCase(loadingPosition)
                if (printer != null) {
                    evaluatePrintTokenTask()
                } else {
                    _errorMessage.postValue("No existe una impresora asignada a esta posición de carga")
                }
            }
        }
    }

    private fun printToken() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                printerManager.ipAddress = printer?.ip.toString()
                _generatedToken.value?.let {
                    printerManager.printToken(it, lastSaleData?.posicionCarga.toString())
                }
            } catch (e: Exception) {
                _message.postValue("Error al imprimir token")
            }
        }
    }

    private suspend fun printReceipt() {
        try {
            printerManager.ipAddress = printer?.ip.toString()
            printerManager.printPurchaseWithTokenCash(
                lastSaleData!!,
                salePumper,
                tokenPaymentsData!!,
                shouldPrintCopy()
            )
        } catch (e: Exception) {
            _message.postValue("Error al imprimir recibo, intente reimprimirlo en la seccion del menu: Consumos Tokencash")
            _errorMessage.postValue("Error al imprimir recibo, intente reimprimirlo en la seccion del menu: Consumos Tokencash")
            Timber.e(e)
        }
    }

    private fun shouldPrintCopy(): Boolean =
        sharedPreferences.getBoolean(ChipREDConstants.RE_PRINT_TOKEN_CASH_PURCHASE, false)

    fun closeToken() {
        viewModelScope.launch(Dispatchers.IO) {
            _isLoading.postValue(true)
            try {
                val transaction = _generatedToken.value?.transaccion.toString()
                tokenPaymentsData?.let { data ->
                    closeTokenUseCase(transaction, data)
                }
                printReceipt()
            } catch (e: Exception) {
                _errorMessage.postValue("Error al cerrar token")
            }
            _isDone.postValue(true)
            _isLoading.postValue(false)
        }
    }
}
