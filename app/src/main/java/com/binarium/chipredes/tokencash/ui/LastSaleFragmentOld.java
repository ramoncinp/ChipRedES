package com.binarium.chipredes.tokencash.ui;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.Pumper;
import com.binarium.chipredes.R;
import com.binarium.chipredes.Sale;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.TwoRowAdapter;
import com.binarium.chipredes.tokencash.repository.TokenCashManager;
import com.binarium.chipredes.tokencash.providers.QRGenerator;
import com.binarium.chipredes.utils.DateUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import timber.log.Timber;

public class LastSaleFragmentOld extends Fragment {
    //Variables
    private int loadingPosition;
    private int pendingTokens = 0;
    private String country;

    //Views
    private ConstraintLayout noSale;
    private ConstraintLayout withSale;
    private ImageView currencyIcon;
    private ListView saleDetailList;

    //Objetos
    private Activity activity;
    private ChipRedManager chipRedManager;
    private TicketlessCoupon ticketlessCoupon;
    private LastSaleInterface lastSaleInterface;
    private Menu menu;
    private ProgressDialog progressDialog;
    private Pumper salePumper;
    private Sale saleObj;

    public LastSaleFragmentOld() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_last_sale_old, container, false);
        noSale = rootView.findViewById(R.id.no_last_sale);
        withSale = rootView.findViewById(R.id.last_sale_layout);
        currencyIcon = rootView.findViewById(R.id.last_sale_currency_icon);
        saleDetailList = rootView.findViewById(R.id.last_sale_detail);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.tc_sale_menu, menu);
        this.menu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.tc_coupon) {
            showTicketlessCoupon();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
        activity.setTitle("Última venta");
        lastSaleInterface.requestingData();

        //Obtener país de la estación
        country = SharedPreferencesManager.getString(getContext(), ChipREDConstants.COUNTRY);

        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(), LastSaleFragmentOld.this)) {
                    return;
                }

                if (webServiceN == ChipRedManager.GET_GENERATED_COUPON) return;

                if (crMessage.contains(ChipREDConstants.NO_LAST_SALE)) {
                    noSale.setVisibility(View.VISIBLE);
                }
                lastSaleInterface.gottenData(false);
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(), LastSaleFragmentOld.this)) {
                    return;
                }

                if (webServiceN == ChipRedManager.GET_GENERATED_COUPON ||
                        webServiceN == ChipRedManager.GET_TOKENCASH_PURCHASES) return;

                GenericDialog genericDialog = new GenericDialog(
                        "Última Venta",
                        errorMessage,
                        () -> getActivity().finish(),
                        null,
                        getActivity());

                genericDialog.setCancelable(false);
                genericDialog.show();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(), LastSaleFragmentOld.this)) {
                    return;
                }

                //Si ya se obtuvieron los datos de la última venta, obtener datos del despachador
                // que hizo la venta
                if (webServiceN == ChipRedManager.GET_LAST_SALE) {
                    try {
                        Timber.d(response.toString(2));
                        //Obtener datos de la ultima venta
                        getSaleFromWolke(response);
                        chipRedManager.getPumpers();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (webServiceN == ChipRedManager.GET_PUMPERS_WS) {
                    //Asignar a la venta su despachador
                    setSalePumper(response);

                    //Obtener variable para saber si se puede generar token
                    boolean canGenerate = SharedPreferencesManager.getBoolean(getContext(),
                            ChipREDConstants.GENERATE_TOKEN, true);

                    //Mostrar información de la venta si no es nula
                    lastSaleInterface.gottenData(canGenerate);

                    //Mostrar layout de informacion de ultima venta
                    withSale.setVisibility(View.VISIBLE);
                } else if (webServiceN == ChipRedManager.GET_GENERATED_COUPON) {
                    //Se obtuvo el cupon de la última venta
                    Timber.d(response.toString());

                    try {
                        //Obtener objeto
                        JSONObject coupon = response.getJSONObject("data");

                        //Crear cupon
                        ticketlessCoupon = new TicketlessCoupon(coupon.getString("cupon"),
                                coupon.getDouble("importe"));

                        //Crear cupon
                        ticketlessCoupon.setCouponQr(QRGenerator.encodeAsBitmap(ticketlessCoupon.getData(), 300));

                        //Mostrar icono del cupon
                        menu.getItem(0).setVisible(true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (webServiceN == ChipRedManager.GET_TOKENCASH_PURCHASES) {
                    parsePendingTokens(response);
                }
            }
        }, activity);

        // Enviar petición a servidor correspondiente para obtener la última venta
        getLastSale();
    }

    private void getLastSale() {
        if (ChipREDConstants.getTokencashScenario(getActivity()).equals("e3")) {
            TokenCashManager.getLastSale(getActivity(), loadingPosition,
                    new TokenCashManager.onMessageReceived() {
                        @Override
                        public void showResponse(JSONObject response) {
                            // Obtener datos
                            getSaleFromTkApi(response);

                            // Obtener despachadores
                            chipRedManager.getPumpers();
                        }

                        @Override
                        public void onError(String message) {
                            GenericDialog genericDialog = new GenericDialog(
                                    "Última Venta",
                                    message,
                                    () -> activity.finish(),
                                    null,
                                    activity);

                            genericDialog.setCancelable(false);
                            genericDialog.show();
                        }
                    });
        } else {
            chipRedManager.getLastSale(loadingPosition);
        }
    }

    public Pumper getSalePumper() {
        return salePumper;
    }

    private void getSaleFromWolke(JSONObject sale) {
        try {
            sale = sale.getJSONObject("data");

            saleObj = new Sale();
            saleObj.setId(sale.getString("id"));
            saleObj.setCantidad(sale.getDouble("cantidad"));
            saleObj.setCosto(sale.getDouble("costo"));
            saleObj.setPrecioUnitario(sale.getDouble("precio_unitario"));
            saleObj.setProducto(sale.getString("combustible"));
            saleObj.setNumProducto(sale.getInt("producto"));
            saleObj.setNumDispensario(sale.getInt("numero_dispensario"));
            saleObj.setPosicionDeCarga(sale.getString("posicion_carga"));

            String fechaHora = sale.getString("fecha_hora");
            saleObj.setFechaHora(fechaHora);
            fechaHora = convertDateFormats(fechaHora);

            saleObj.setFecha(fechaHora.substring(0, fechaHora.indexOf('T')));
            saleObj.setHora(fechaHora.substring(fechaHora.indexOf('T') + 1));
            saleObj.setNumeroTicket(sale.getString("num_ticket"));

            //Asignar id de despachador a la actividad principal
            saleObj.setPumperId(sale.getString("id_despachador_emax"));

            // Mostrar datos obtenidos
            showSaleInfo();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getSaleFromTkApi(JSONObject sale) {
        try {
            // Crear objeto de venta
            saleObj = new Sale();

            // Agregar datos
            saleObj.setId(sale.getString("sale_id"));
            saleObj.setCantidad(sale.getDouble("quantity"));
            saleObj.setCosto(sale.getDouble("amount"));
            saleObj.setPrecioUnitario(sale.getJSONObject("product").getDouble("price"));
            saleObj.setProducto(sale.getJSONObject("product").getString("name"));
            saleObj.setNumProducto(sale.getJSONObject("product").getInt("id"));

            // Obtener numero de dispensario
            int loadingPos = sale.getJSONObject("dispenser").getInt("side");
            int dispNumber = loadingPos % 2 == 0 ? loadingPos / 2 : (loadingPos + 1) / 2;

            // Definir datos
            saleObj.setNumDispensario(dispNumber);
            saleObj.setPosicionDeCarga(String.valueOf(loadingPos));

            // Obtener fecha y convertir
            long dispatchedAt = sale.getJSONObject("dispatched_at").getLong("$date");
            String fechaHora = convertToWolkeDateFormat(new Date(dispatchedAt));

            saleObj.setFechaHora(fechaHora);
            fechaHora = convertDateFormats(fechaHora);

            saleObj.setFecha(fechaHora.substring(0, fechaHora.indexOf('T')));
            saleObj.setHora(fechaHora.substring(fechaHora.indexOf('T') + 1));
            saleObj.setNumeroTicket(sale.getString("sale_id"));

            //Asignar id de despachador a la actividad principal
            saleObj.setPumperId(sale.getJSONObject("seller").getString("id"));

            // Mostrar datos obtenidos
            showSaleInfo();
        } catch (JSONException e) {
            lastSaleInterface.gottenData(false);
            e.printStackTrace();
        }
    }

    public void showSaleInfo() {
        String costo = "";
        String precioUnitario = "";

        if (country.equals("mexico") || country.equals("méxico")) {
            int id = activity.getResources().getIdentifier("ic_attach_money", "drawable",
                    activity.getPackageName());

            costo = ChipREDConstants.MX_AMOUNT_FORMAT.format(saleObj.getCosto()) + " MXN";
            precioUnitario =
                    ChipREDConstants.MX_AMOUNT_FORMAT.format(saleObj.getPrecioUnitario()) +
                            " MXN";
            currencyIcon.setImageResource(id);
        } else if (country.equals("costa rica")) {
            int id = activity.getResources().getIdentifier("ic_colon_crc", "drawable",
                    activity.getPackageName());

            costo = ChipREDConstants.CR_AMOUNT_FORMAT.format(saleObj.getCosto()) + " CRC";
            precioUnitario = ChipREDConstants.CR_AMOUNT_FORMAT.format(saleObj
                    .getPrecioUnitario()) + " CRC";
            currencyIcon.setImageResource(id);
        }

        String cantidad = saleObj.getCantidad() + " L";
        String producto = saleObj.getProducto();

        //Crear lista con los detalles de la venta
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();

        keys.add("Cantidad:");
        values.add(cantidad);

        keys.add("Importe:");
        values.add(costo);

        keys.add("Precio unitario:");
        values.add(precioUnitario);

        keys.add("Producto:");
        values.add(producto);

        keys.add("Fecha:");
        values.add(saleObj.getFecha());

        keys.add("Hora:");
        values.add(saleObj.getHora());

        keys.add("Num. Ticket");
        values.add(saleObj.getNumeroTicket());

        //Obtener tamaño del texto
        int textSize;
        try {
            textSize = getContext().getResources().getInteger(R.integer
                    .normal_screen_text_size);
        } catch (NullPointerException e) {
            textSize = 24;
        }

        //Asignar adaptador con valores de listas al ListView
        saleDetailList.setAdapter(new TwoRowAdapter(activity, keys, values, textSize));

        //Asignar venta a la actividad principal
        lastSaleInterface.returnLastSale(saleObj);

        //Tratar de obtener cupon
        chipRedManager.getGeneratedCoupon(saleObj.getId());
    }

    private String convertDateFormats(String fechaDeEntrada) {
        //Formato de fecha requerida "dd-mm-yyyy hh:MM"
        //Formato de entrada "yyyy-mm-ddThh:MM:ss"

        Date date = null;
        DateFormat dateFormatEntrada = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        try {
            date = dateFormatEntrada.parse(fechaDeEntrada);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String fechaConFormato;
        DateFormat dateFormatSalida = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss", Locale.US);
        fechaConFormato = dateFormatSalida.format(date);

        return fechaConFormato;
    }

    private String convertToWolkeDateFormat(Date date) {
        //Formato de entrada "yyyy-mm-ddThh:MM:ss"
        String fechaConFormato;
        DateFormat dateFormatSalida = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        dateFormatSalida.setTimeZone(TimeZone.getTimeZone("GMT"));
        fechaConFormato = dateFormatSalida.format(date);

        return fechaConFormato;
    }

    private void setSalePumper(JSONObject response) {
        try {
            JSONArray pumpers = response.getJSONObject("data").getJSONArray("despachadores");
            for (int i = 0; i < pumpers.length(); i++) {
                JSONObject pumperJson = pumpers.getJSONObject(i);
                if (saleObj.getPumperId().equals(pumperJson.getString("id_emax"))) {
                    Pumper pumper = new Pumper();
                    pumper.setEmaxId(pumperJson.getString("id_emax"));
                    pumper.setName(pumperJson.getString("nombre"));
                    pumper.setNameInitials(pumperJson.getString("iniciales"));
                    pumper.setNip(pumperJson.getString("nip"));
                    pumper.setTagId(pumperJson.getString("tag"));
                    pumper.setListIndex(i);

                    salePumper = pumper;
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void askPumperNip() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();
        View dialogNip = inflater.inflate(R.layout.dialog_nip, null);
        final EditText etNipD = dialogNip.findViewById(R.id.etNipC);

        builder.setTitle("Ingresar NIP de despachador: " + salePumper.getName());
        builder.setView(dialogNip);
        builder.setPositiveButton("INGRESAR", (dialog, which) -> {
            if (etNipD.getText().toString().equals(salePumper.getNip())) {
                lastSaleInterface.setNextFragment();
            } else {
                Toast.makeText(activity, "No coincide NIP", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("CANCELAR", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = builder.create();
        dialog.show();

        ChipREDConstants.showKeyboard(getActivity());
    }

    public void showNoPumperSelected() {
        GenericDialog genericDialog = new GenericDialog("Error", "No coincide despachador de " +
                "venta con alguno registrado", () -> {
            //nada
        }, null, getContext());

        genericDialog.setCancelable(false);
        genericDialog.setPositiveText("Ok");
        genericDialog.show();
    }

    private void showTicketlessCoupon() {
        //Crear builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //Obtener contenido
        View content = getLayoutInflater().inflate(R.layout.tc_ticketless_coupon, null);

        //Definir layout
        builder.setView(content);

        //Obtener views
        TextView amount = content.findViewById(R.id.coupon_amount);
        TextView couponData = content.findViewById(R.id.coupon_data);
        ImageView couponImage = content.findViewById(R.id.generated_coupon);

        String amountString =
                "$" + ChipREDConstants.MX_AMOUNT_FORMAT.format(ticketlessCoupon.getAmount());

        amount.setText(amountString);
        couponData.append(ticketlessCoupon.getData());

        //Checar si es nulo el código QR
        if (ticketlessCoupon.getCouponQr() == null) {
            //Intentar otra vez
            ticketlessCoupon.setCouponQr(QRGenerator.encodeAsBitmap(ticketlessCoupon.getData(),
                    300));
        }

        if (ticketlessCoupon.getCouponQr() == null) {
            TextView noQr = content.findViewById(R.id.no_ticketless_qr);
            noQr.setVisibility(View.VISIBLE);
            couponImage.setVisibility(View.INVISIBLE);
        } else {
            couponImage.setImageBitmap(ticketlessCoupon.getCouponQr());
        }

        //Agregar boton para regresar
        builder.setNegativeButton("Regresar", (dialogInterface, i) -> dialogInterface.dismiss());

        //Crear dialogo
        Dialog dialog = builder.create();
        dialog.show();
    }

    public void getPendingTokens() {
        pendingTokens = 0;
        Date endDate = DateUtils.getLastTimeOfDay();
        Date startDate = DateUtils.getDeltaDate(new Date(), -1);
        chipRedManager.getTokencashPurchases(
                0,
                15,
                startDate,
                endDate,
                "PE"
        );
    }

    public void parsePendingTokens(JSONObject response) {
        // Obtener arreglo de consumos token
        JSONArray data = response.optJSONArray("data");
        if (data != null) {

            // Iterar en cada consumo
            for (int idx = 0; idx < data.length(); idx++) {

                // Obtener consumo
                final JSONObject tokenPurchase = data.optJSONObject(idx);
                if (tokenPurchase != null) {

                    // Obtener objeto token
                    final JSONObject token = tokenPurchase.optJSONObject("token");
                    if (token != null) {

                        // Obtener estatus de token actual
                        String status = token.optString("estatus");
                        if (status.equals("PE")) {
                            // Si el estado es pendiente, sumar...
                            pendingTokens++;
                        }
                    }
                }
            }
        }

        CardView pendingTokensCv = activity.findViewById(R.id.pending_tokens_cv);
        TextView pendingTokensTv = activity.findViewById(R.id.pending_tokens_tv);
        if (pendingTokens > 0) {
            pendingTokensCv.setVisibility(View.VISIBLE);
            pendingTokensTv.setText(String.valueOf(pendingTokens));
            pendingTokensCv.setOnClickListener(v -> {
                progressDialog = ProgressDialog.show(activity, "", "Cargando...");
                new Handler().postDelayed(() -> {
                    Intent intent = new Intent(activity, TokencashPurchases.class);
                    intent.putExtra(TokencashPurchases.DAYS_BEFORE, -1);
                    startActivity(intent);
                }, 500);
            });
        } else {
            pendingTokensCv.setVisibility(View.GONE);
        }
    }

    public void setLastSaleInterface(LastSaleInterface lastSaleInterface) {
        this.lastSaleInterface = lastSaleInterface;
    }

    public void setLoadingPosition(int loadingPosition) {
        this.loadingPosition = loadingPosition;
    }

    public interface LastSaleInterface {
        void returnLastSale(Sale sale);

        void setNextFragment();

        void requestingData();

        void gottenData(boolean continuable);
    }

    private static class TicketlessCoupon {
        private final double amount;
        private final String data;
        private Bitmap couponQr = null;

        TicketlessCoupon(String data, double amount) {
            this.amount = amount;
            this.data = data;
        }

        public double getAmount() {
            return amount;
        }

        public String getData() {
            return data;
        }

        Bitmap getCouponQr() {
            return couponQr;
        }

        void setCouponQr(Bitmap couponQr) {
            this.couponQr = couponQr;
        }
    }
}
