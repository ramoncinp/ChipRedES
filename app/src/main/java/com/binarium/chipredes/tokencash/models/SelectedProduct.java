package com.binarium.chipredes.tokencash.models;

public class SelectedProduct {
    private String name;
    private double percentage;
    private int productId;
    private boolean active;

    public SelectedProduct(String name, double percentage, boolean active, int productId) {
        this.name = name;
        this.percentage = percentage;
        this.active = active;
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public double getPercentage() {
        return percentage;
    }

    public boolean isActive() {
        return active;
    }

    public int getProductId() {
        return productId;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
