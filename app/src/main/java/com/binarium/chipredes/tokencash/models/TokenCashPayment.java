package com.binarium.chipredes.tokencash.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TokenCashPayment
{
    private String currency;
    private String document;
    private String dateTime;
    private double amount;

    public TokenCashPayment()
    {
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public String getDocument()
    {
        return document;
    }

    public void setDocument(String document)
    {
        this.document = document;
    }

    public String getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(String dateTime)
    {
        //Formato de entrada -> 2018-07-25 11:18:24
        //Formato de salida  -> 11:18 25/07/2018
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat myDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
        try
        {
            Date convertedDate = dateFormat.parse(dateTime);
            this.dateTime = myDateFormat.format(convertedDate);
        }
        catch (ParseException e)
        {
            this.dateTime = dateTime;
        }
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }
}
