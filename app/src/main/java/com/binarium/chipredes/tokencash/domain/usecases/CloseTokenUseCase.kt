package com.binarium.chipredes.tokencash.domain.usecases

import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.tokencash.CloseTokenRequest
import com.binarium.chipredes.wolke.models.tokencash.TokenPaymentsData
import javax.inject.Inject

class CloseTokenUseCase @Inject constructor(
    val wolkeApiService: WolkeApiService
) {
    suspend operator fun invoke(transaction: String, tokenPaymentsData: TokenPaymentsData) {
        val request = buildRequest(transaction, tokenPaymentsData)
        closeToken(request)
    }

    private suspend fun closeToken(closeTokenRequest: CloseTokenRequest) =
        wolkeApiService.closeToken(closeTokenRequest)

    private fun buildRequest(
        transaction: String,
        tokenPaymentsData: TokenPaymentsData
    ): CloseTokenRequest = CloseTokenRequest(
        transaccion = transaction,
        payments = tokenPaymentsData.payments,
        summary = tokenPaymentsData.summary
    )
}
