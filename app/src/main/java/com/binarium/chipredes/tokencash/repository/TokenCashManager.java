package com.binarium.chipredes.tokencash.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class TokenCashManager {
    private static final String TAG = TokenCashManager.class.getSimpleName();
    private static final String URL = "https://tokencash.appspot.com/api";

    // Métodos
    private static final int GET_LAST_SALE = 0;
    private static final int GET_STATION_PRINTERS = 1;
    private static final int UPDATE_PRINTER = 2;
    private static final int REMOVE_PRINTER = 3;
    private static final int CREATE_PRINTER = 4;
    private static final int CREATE_TOKEN = 5;
    private static final int GET_TOKEN_PAYMENTS = 6;
    private static final int CLOSE_TOKEN = 7;
    private static final int GET_STATION_TOKENS = 8;
    private static final int GET_CLOSED_TOKENS = 9;
    private static final int PRINT_CLOSED_TOKENS = 10;

    private static onMessageReceived mListener;

    public static void getLastSale(Context context, int stationSide, onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Agregar parametros
        String url = URL + "/sale?" + "station_number=" + stationId +
                "&dispenser_side=" + stationSide;

        // Enviar peticion
        sendRequest(context, GET_LAST_SALE, url, null, Request.Method.GET);
    }

    public static void getPrinters(Context context, onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Agregar parametros
        String url = URL + "/stations/" + stationId + "/printers";

        // Enviar peticion
        sendRequest(context, GET_STATION_PRINTERS, url, null, Request.Method.GET);
    }

    public static void createPrinter(Context context, JSONObject printer,
                                     onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Agregar parametros
        String url = URL + "/stations/" + stationId + "/printers";

        // Enviar peticion
        sendRequest(context, CREATE_PRINTER, url, printer, Request.Method.POST);
    }

    public static void updatePrinter(Context context, JSONObject printer,
                                     onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        try {
            // Agregar parametros
            String url = URL + "/stations/" + stationId + "/printers/" + printer.getString("id");

            // Ajustar cuerpo de petición
            printer.remove("id");

            // Enviar peticion
            sendRequest(context, UPDATE_PRINTER, url, printer, Request.Method.PATCH);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void removePrinter(Context context, String printerId, onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Agregar parametros
        String url = URL + "/stations/" + stationId + "/printers/" + printerId;

        // Enviar peticion
        sendRequest(context, REMOVE_PRINTER, url, null, Request.Method.DELETE);
    }

    public static void createToken(Context context, String saleId,
                                   onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Agregar parametros
        String url = URL + "/stations/" + stationId + "/token";

        // Crear payload
        JSONObject payload = new JSONObject();
        try {
            payload.put("sale_id", Integer.valueOf(saleId));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Enviar peticion
        sendRequest(context, CREATE_TOKEN, url, payload, Request.Method.POST);
    }

    public static void getTokenPayments(Context context, String transaction,
                                        onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Agregar parametros
        String url = URL + "/stations/" + stationId + "/token/" + transaction + "/payments";

        // Enviar peticion
        sendRequest(context, GET_TOKEN_PAYMENTS, url, null, Request.Method.GET);
    }

    public static void closeToken(Context context, String transaction, String sellerId,
                                  onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Agregar parametros
        String url =
                URL + "/stations/" + stationId + "/sellers/" + sellerId + "/tokens/" + transaction + "/?status=CE";

        // Enviar peticion
        sendRequest(context, CLOSE_TOKEN, url, null, Request.Method.PUT);
    }

    public static void getStationTokens(Context context, onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Agregar parametros
        String url = URL + "/stations/" + stationId + "/tokens?limit=30&sort=-1";

        // Enviar peticion
        sendRequest(context, GET_STATION_TOKENS, url, null, Request.Method.GET);
    }

    public static void getClosedTokens(Context context, String pumperId, onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Agregar parametros
        String url = String.format(Locale.getDefault(), "%s/stations/%s/sellers/%s" +
                "/tokens/?status=CE", URL, stationId, pumperId);

        // Enviar peticion
        sendRequest(context, GET_CLOSED_TOKENS, url, null, Request.Method.GET);
    }

    public static void printClosedTokens(Context context, String pumperId,
                                         onMessageReceived listener) {
        // Asignar listener
        mListener = listener;

        // Obtener id de estación
        String stationId = SharedPreferencesManager.getString(context, ChipREDConstants.STATION_ID);

        // Agregar parametros
        String url = String.format(Locale.getDefault(), "%s/stations/%s/sellers/%s" +
                "/tokencollection?status=IM", URL, stationId, pumperId);

        // Enviar peticion
        sendRequest(context, PRINT_CLOSED_TOKENS, url, null, Request.Method.PUT);
    }

    private static void sendRequest(Context mContext, final int apiMethod, String url,
                                    @Nullable JSONObject data, int httpMethod) {
        // Crear cola de peticiones
        RequestQueue queue = Volley.newRequestQueue(mContext);

        // Crear request
        JsonObjectRequest request = new JsonObjectRequest(httpMethod, url, data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v(TAG, "Respuesta -> " + response.toString());
                        mListener.showResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Respuesta -> " + error.getMessage());
                switch (apiMethod) {
                    case GET_LAST_SALE:
                        mListener.onError("Error al obtener última venta");
                        break;

                    case GET_STATION_PRINTERS:
                        mListener.onError("Error al obtener impresoras");
                        break;

                    case UPDATE_PRINTER:
                        mListener.onError("Error al actualizar impresora");
                        break;

                    case REMOVE_PRINTER:
                        mListener.onError("Error al remover impresora");
                        break;

                    case CREATE_PRINTER:
                        mListener.onError("Error al agregar impresora");
                        break;

                    case CREATE_TOKEN:
                        mListener.onError("Error al crear token de cobro");
                        break;

                    case GET_TOKEN_PAYMENTS:
                        mListener.onError("Error al obtener pagos de token");
                        break;

                    case CLOSE_TOKEN:
                        mListener.onError("Error al cerrar token");
                        break;

                    case GET_STATION_TOKENS:
                        mListener.onError("Error al obtener tokens de la estación");
                        break;

                    case GET_CLOSED_TOKENS:
                        mListener.onError("Error al obtener tokens cerrados por despachador");
                        break;

                    case PRINT_CLOSED_TOKENS:
                        mListener.onError("Error al mandar imprimir los tokens cerrados");
                        break;
                }
            }
        });

        // 20 segundos de timeout
        request.setRetryPolicy(new DefaultRetryPolicy(20000, 0, 0));

        Log.v(TAG, "Iniciando petición");
        Log.v(TAG, "URL -> " + request.getUrl());
        Log.v(TAG, "Body -> " + (data == null ? "null" : data.toString()));

        // Iniciar petición
        queue.add(request);
    }

    public interface onMessageReceived {
        void showResponse(JSONObject response);

        void onError(String message);
    }
}
