package com.binarium.chipredes.tokencash.ui;

import androidx.annotation.NonNull;

import com.binarium.chipredes.tokencash.models.SelectedProduct;
import com.binarium.chipredes.tokencash.adapters.SelectedProductAdapter;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.configstation.StationFileManager;
import com.binarium.chipredes.configstation.StationManager;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TokenCashConfiguration extends AppCompatActivity {
    private static final int SHOW_TICKET_EDITOR_ACTIVITY = 1;
    private int SELECTED_CARD_VIEW_COLOR;
    private int SELECTED_TEXT_VIEW_COLOR;
    private int UNSELECTED_CARD_VIEW_COLOR;
    private int UNSELECTED_TEXT_VIEW_COLOR;

    //Variables
    private JSONObject editables;
    private JSONObject fidelizacion;

    //Views
    private ImageView onOffIcon;
    private ProgressDialog progressDialog;
    private RadioGroup radioGroup;
    private RadioGroup tokenCashScenarioRg;
    private Switch tokenCashSwitch;
    private ProgressBar progressBar;
    private NestedScrollView content;

    //Formulario
    private CardView[] daysCardViews;
    private TextView[] daysTextViews;
    private MaterialEditText percentageEt;
    private MaterialEditText maxAmount;
    private MaterialEditText maxTokenPercentageEt;
    private CheckBox publicoGeneralCb;
    private CheckBox clientePuntosCb;
    private CheckBox clienteDebitoCb;
    private CheckBox clienteCreditoCb;
    private Button sendConfig;
    private Button cancel;
    private RecyclerView selectedProductsList;
    private Switch generateTokenSw;
    private Switch printTokenSw;
    private Switch reprintTokenPurchaseSw;

    //Objetos
    private ArrayList<SelectedProduct> selectedProducts;
    private ChipRedManager chipRedManager;
    private Menu menu;
    private StationManager stationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_token_cash);
        setTitle("Configuración tokencash");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        SELECTED_CARD_VIEW_COLOR = getResources().getColor(R.color.green_ok);
        SELECTED_TEXT_VIEW_COLOR = getResources().getColor(R.color.white);
        UNSELECTED_CARD_VIEW_COLOR = getResources().getColor(R.color.white);
        UNSELECTED_TEXT_VIEW_COLOR = getResources().getColor(R.color.black);

        //Relacionar views
        onOffIcon = findViewById(R.id.on_off_icon);
        tokenCashSwitch = findViewById(R.id.token_cash_switch);
        percentageEt = findViewById(R.id.token_cash_porcentaje_cupon);
        maxAmount = findViewById(R.id.token_cash_monto_maximo_cupon);
        sendConfig = findViewById(R.id.save_button);
        cancel = findViewById(R.id.start_button);
        progressBar = findViewById(R.id.progress_bar);
        content = findViewById(R.id.token_cash_content);
        publicoGeneralCb = findViewById(R.id.token_cash_publico_general_checkbox);
        clientePuntosCb = findViewById(R.id.token_cash_cliente_puntos_checkbox);
        clienteDebitoCb = findViewById(R.id.token_cash_cliente_debito_checkbox);
        clienteCreditoCb = findViewById(R.id.token_cash_cliente_credito_checkbox);
        selectedProductsList = findViewById(R.id.tc_selected_products);
        radioGroup = findViewById(R.id.radio_button_group);
        generateTokenSw = findViewById(R.id.generate_token_sw);
        printTokenSw = findViewById(R.id.print_token_sw);
        reprintTokenPurchaseSw = findViewById(R.id.reprint_token_purchase);
        maxTokenPercentageEt = findViewById(R.id.max_token_percentage_et);
        tokenCashScenarioRg = findViewById(R.id.tokencash_escenario_radio_group);

        //Inicializar views de días de la semana
        initWeekDays();

        //Definir listener de switch
        tokenCashSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    onOffIcon.setImageResource(R.drawable.ic_power_green);
                } else {
                    onOffIcon.setImageResource(R.drawable.ic_power_red);
                }
            }
        });

        sendConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setConfiguration();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //Inicializar estados de switches
        generateTokenSw.setChecked(SharedPreferencesManager.getBoolean(this,
                ChipREDConstants.GENERATE_TOKEN, true));
        reprintTokenPurchaseSw.setChecked(SharedPreferencesManager.getBoolean(this,
                ChipREDConstants.RE_PRINT_TOKEN_CASH_PURCHASE, false));

        //Inicializar el manejador de ChipRED y pedir datos
        initChipRedManager();

        //Inicializar el manejador para sincronizar con Servidor Local
        initStationManager();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.tc_config_menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == R.id.edit_tc_ticket) {
            Intent editTicketActivity = new Intent(this, TicketsEditor.class);
            editTicketActivity.putExtra(TicketsEditor.EDITABLES_PARAM, (editables != null) ?
                    editables.toString() : "");
            startActivityForResult(editTicketActivity, SHOW_TICKET_EDITOR_ACTIVITY);
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == SHOW_TICKET_EDITOR_ACTIVITY) {
            // Validar y obtener los datos editados
            if (data != null) {
                String mData = data.getStringExtra(TicketsEditor.EDITABLES_PARAM);
                if (mData != null && !mData.isEmpty()) {
                    //Definir nuevos datos
                    try {
                        //Asignar a variable global
                        editables = new JSONObject(mData);

                        //Enviar request para actualizar la nube
                        buildTcRequest();
                    } catch (JSONException e) {
                        Toast.makeText(this, "Error al obtener datos editables de ticket",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    private void initStationManager() {
        stationManager = new StationManager(this, new StationManager.OnResponse() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                //Mostrar mensaje
                Toast.makeText(TokenCashConfiguration.this, crMessage, Toast.LENGTH_LONG).show();

                //Mostrar contenido
                content.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                //Mostrar mensaje
                Toast.makeText(TokenCashConfiguration.this, errorMessage, Toast.LENGTH_LONG).show();

                //Mostrar contenido
                content.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                //Evaluar servicio
                if (webServiceN == StationManager.GET_PRODUCTS) {
                    //Mostrar productos
                    setSelectedProductsList(response);

                    //Mostrar contenido
                    content.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    //Mostrar mensaje
                    try {
                        Toast.makeText(TokenCashConfiguration.this, response.getString("message"),
                                Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void buildTcRequest() {
        JSONObject request = new JSONObject();
        try {
            request.put("emitir", tokenCashSwitch.isChecked());
            request.put("productos", getSelectedProducts());
            request.put("tipo_cliente", getSelectedClients());
            request.put("dias", getSelectedDays());
            request.put("monto_maximo", Double.parseDouble(maxAmount.getText().toString()));
            request.put("porcentaje", Integer.parseInt(percentageEt.getText().toString()));
            request.put("tipo_emision", getEmitionType());
            request.put("maximo_porcentaje_cobro", maxTokenPercentageEt.getText().toString());
            request.put("impresion_token_cobro", printTokenSw.isChecked());
            request.put("editables", editables);
            request.put("modo_trabajo", getTokencashScenario());
        } catch (JSONException e) {
            e.printStackTrace();
            ChipREDConstants.showSnackBarMessage("Error al procesar información", this);
            return;
        }

        //Enviar peticion para nueva configuracion
        chipRedManager.configTokenCash(request);

        //Crear objeto JSON para registrar en archivo de estación
        fidelizacion = new JSONObject();
        try {
            fidelizacion.put("tokencash", request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setSelectedDays(JSONArray days) {
        try {
            //Obtener los índices de los días seleccionados
            for (int i = 0; i < days.length(); i++) {
                int dayIdx = days.getInt(i);
                daysCardViews[dayIdx].setCardBackgroundColor(SELECTED_CARD_VIEW_COLOR);
                daysTextViews[dayIdx].setTextColor(SELECTED_TEXT_VIEW_COLOR);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONArray getSelectedDays() {
        JSONArray daysArray = new JSONArray();
        for (int i = 0; i < daysCardViews.length; i++) {
            if (daysCardViews[i].getCardBackgroundColor().getDefaultColor() ==
                    SELECTED_CARD_VIEW_COLOR) {
                //Obtener texto del día correspondiente
                String dayLetter = daysTextViews[i].getText().toString();
                if (dayLetter.equals("D")) {
                    daysArray.put(0);
                } else if (dayLetter.equals("L")) {
                    daysArray.put(1);
                } else if (dayLetter.equals("M")) {
                    daysArray.put(2);
                } else if (dayLetter.equals("Mi")) {
                    daysArray.put(3);
                } else if (dayLetter.equals("J")) {
                    daysArray.put(4);
                } else if (dayLetter.equals("V")) {
                    daysArray.put(5);
                } else if (dayLetter.equals("S")) {
                    daysArray.put(6);
                }
            }
        }

        return daysArray;
    }

    private void setSelectedClients(JSONArray clients) {
        try {
            for (int i = 0; i < clients.length(); i++) {
                switch (clients.getInt(i)) {
                    case 0:
                        publicoGeneralCb.setChecked(true);
                        break;

                    case 1:
                        clientePuntosCb.setChecked(true);
                        break;

                    case 2:
                        clienteDebitoCb.setChecked(true);
                        break;

                    case 3:
                        clienteCreditoCb.setChecked(true);
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONArray getSelectedProducts() {
        //Armar arreglo de productos seleccionados
        JSONArray productsArray = new JSONArray();

        //Obtener elementos del RecyclerView
        for (SelectedProduct selectedProduct : selectedProducts) {
            try {
                JSONObject jsonProduct = new JSONObject();
                jsonProduct.put("descripcion", selectedProduct.getName());
                jsonProduct.put("id", selectedProduct.getProductId());
                jsonProduct.put("porcentaje", selectedProduct.getPercentage());
                jsonProduct.put("activo", selectedProduct.isActive());

                productsArray.put(jsonProduct);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return productsArray;
    }

    private JSONArray getSelectedClients() {
        //Armar arreglo de tipo de clientes
        JSONArray clientsArray = new JSONArray();
        if (publicoGeneralCb.isChecked()) {
            clientsArray.put(0);
        }

        if (clientePuntosCb.isChecked()) {
            clientsArray.put(1);
        }

        if (clienteDebitoCb.isChecked()) {
            clientsArray.put(2);
        }

        if (clienteCreditoCb.isChecked()) {
            clientsArray.put(3);
        }

        return clientsArray;
    }

    private String getTokencashScenario() {
        int checkedRadioButtonId = tokenCashScenarioRg.getCheckedRadioButtonId();
        if (checkedRadioButtonId == R.id.work_mode_e1_cb) {
            return "e1";
        } else if (checkedRadioButtonId == R.id.work_mode_e2_cb) {
            return "e2";
        } else if (checkedRadioButtonId == R.id.work_mode_e3_cb) {
            return "e3";
        }

        //Retornar E1 por default
        return "e1";

    }

    private void setSelectedProductsList(JSONObject response) {
        if (selectedProducts == null) {
            selectedProducts = new ArrayList<>();
        }

        //Aun no hay datos de los productos
        if (selectedProducts.size() == 0) {
            try {
                //Obtener el arreglo de productos
                JSONArray productsArray = response.getJSONObject("data").getJSONArray("productos");

                //Obtener cada producto
                for (int i = 0; i < productsArray.length(); i++) {
                    //Obtener objeto
                    JSONObject mProduct = productsArray.getJSONObject(i);
                    //Obtener atributos
                    String name = "";
                    int productId = 0;

                    if (mProduct.has("descripcion"))
                        name = mProduct.getString("descripcion");

                    if (mProduct.has("id_producto"))
                        productId = mProduct.getInt("id_producto");

                    //Crear objeto
                    SelectedProduct product = new SelectedProduct(name, 0, true,
                            productId);
                    selectedProducts.add(0, product);
                }
            } catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }
        }

        //Crear adaptador
        SelectedProductAdapter selectedProductAdapter =
                new SelectedProductAdapter(selectedProducts);

        selectedProductsList.setAdapter(selectedProductAdapter);
        selectedProductsList.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setConfiguration() {
        String invalidMessage = "";

        METValidator percentageValidator = new METValidator("Porcentaje inválido") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                if (isEmpty) {
                    return false;
                } else {
                    double percentage = Double.parseDouble(text.toString());
                    return (percentage > 0 && percentage <= 5);
                }
            }
        };

        //Validar campos
        boolean isValid1 = percentageEt.validateWith(percentageValidator);

        boolean isValid2 = maxAmount.validateWith(new METValidator("Monto máximo inválido") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                if (isEmpty) {
                    return false;
                } else {
                    double maxAmountValue = Double.parseDouble(text.toString());
                    return (maxAmountValue > 0 && maxAmountValue <= 100);
                }
            }
        });

        boolean isValid3;
        isValid3 = (publicoGeneralCb.isChecked() || clientePuntosCb.isChecked() ||
                clienteDebitoCb.isChecked() || clienteCreditoCb.isChecked());

        if (!isValid3) {
            invalidMessage += "Seleccione al menos un Cliente\n";
        }

        boolean isValid4 = false;
        //Definir listeners a las cardviews
        for (CardView cardView : daysCardViews) {
            if (cardView.getCardBackgroundColor().getDefaultColor() == SELECTED_CARD_VIEW_COLOR) {
                //Hay al menos un día seleccionado
                isValid4 = true;
                break;
            }
        }

        boolean isValid5 = true;
        //Obtener elementos del RecyclerView
        for (int i = 0; i < selectedProductsList.getChildCount(); i++) {
            SelectedProductAdapter.SelectedProductViewHolder selectedProductViewHolder =
                    (SelectedProductAdapter.SelectedProductViewHolder) selectedProductsList.findViewHolderForAdapterPosition(i);

            //Validar que tengan un porcentaje de recompensa
            if (selectedProductViewHolder != null) {
                isValid5 &= selectedProductViewHolder.percentageEt.validateWith(percentageValidator);
                if (isValid5) {
                    //Actualizar objeto
                    selectedProducts.get(i).setActive(selectedProductViewHolder.activeCb.isChecked());
                    selectedProducts.get(i).setPercentage(Double.parseDouble(selectedProductViewHolder.percentageEt.getText().toString()));
                }
            }
        }

        boolean isValid6 = maxTokenPercentageEt.validateWith(new METValidator("Porcentaje máximo " +
                "inválido") {
            @Override
            public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                if (isEmpty) {
                    return false;
                } else {
                    int maxPercentageValue = Integer.parseInt(text.toString());
                    return (maxPercentageValue > 0 && maxPercentageValue <= 100);
                }
            }
        });

        if (!isValid4) {
            invalidMessage += "Seleccione al menos un día de la semana";
        }

        if (!invalidMessage.equals("")) {
            Snackbar.make(findViewById(android.R.id.content), invalidMessage, Snackbar
                    .LENGTH_LONG).show();
        }

        if (isValid1 && isValid2 && isValid3 && isValid4 && isValid5 && isValid6) {
            progressDialog = ProgressDialog.show(this, "", "Cargando...");
            buildTcRequest();
        }
    }

    private int getEmitionType() {
        int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
        if (checkedRadioButtonId == R.id.coupon_print_method) {
            return 0;
        } else if (checkedRadioButtonId == R.id.coupon_ticketless_method) {
            return 1;
        } else if (checkedRadioButtonId == R.id.coupon_both_methods) {
            return 2;
        }

        //Retornar 0 por default (impresion)
        return 0;
    }

    private void setEmitionType(int emitionType) {
        RadioButton print = findViewById(R.id.coupon_print_method);
        RadioButton ticketless = findViewById(R.id.coupon_ticketless_method);
        RadioButton both = findViewById(R.id.coupon_both_methods);

        switch (emitionType) {
            case 0:
                print.setChecked(true);
                break;

            case 1:
                ticketless.setChecked(true);
                break;

            case 2:
                both.setChecked(true);
                break;
        }
    }

    private void setWorkMode(String workMode) {
        RadioButton workModeE1 = findViewById(R.id.work_mode_e1_cb);
        RadioButton workModeE2 = findViewById(R.id.work_mode_e2_cb);
        RadioButton workModeE3 = findViewById(R.id.work_mode_e3_cb);

        switch (workMode) {
            case "e1":
                workModeE1.setChecked(true);
                workModeE2.setChecked(false);
                workModeE3.setChecked(false);
                break;

            case "e2":
                workModeE1.setChecked(false);
                workModeE2.setChecked(true);
                workModeE3.setChecked(false);
                break;

            case "e3":
                workModeE1.setChecked(false);
                workModeE2.setChecked(false);
                workModeE3.setChecked(true);
                break;
        }
    }

    private void initWeekDays() {
        //Inicializar arreglos
        daysCardViews = new CardView[7];
        daysTextViews = new TextView[7];

        //Agregar views a los arreglos
        daysCardViews[0] = findViewById(R.id.domingo_card_view);
        daysCardViews[1] = findViewById(R.id.lunes_card_view);
        daysCardViews[2] = findViewById(R.id.martes_card_view);
        daysCardViews[3] = findViewById(R.id.miercoles_card_view);
        daysCardViews[4] = findViewById(R.id.jueves_card_view);
        daysCardViews[5] = findViewById(R.id.viernes_card_view);
        daysCardViews[6] = findViewById(R.id.sabado_card_view);

        daysTextViews[0] = findViewById(R.id.domingo_text_view);
        daysTextViews[1] = findViewById(R.id.lunes_text_view);
        daysTextViews[2] = findViewById(R.id.martes_text_view);
        daysTextViews[3] = findViewById(R.id.miercoles_text_view);
        daysTextViews[4] = findViewById(R.id.jueves_text_view);
        daysTextViews[5] = findViewById(R.id.viernes_text_view);
        daysTextViews[6] = findViewById(R.id.sabado_text_view);

        //Definir click listener para CardViews
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CardView cardView = (CardView) view;
                if (cardView.getCardBackgroundColor().getDefaultColor() == SELECTED_CARD_VIEW_COLOR) {
                    //Está seleccionado, deseleccionar
                    cardView.setCardBackgroundColor(UNSELECTED_CARD_VIEW_COLOR);
                    TextView dayText = (TextView) cardView.getChildAt(0);
                    dayText.setTextColor(UNSELECTED_TEXT_VIEW_COLOR);
                } else {
                    //Está deseleccionado, seleccionar
                    cardView.setCardBackgroundColor(SELECTED_CARD_VIEW_COLOR);
                    TextView dayText = (TextView) cardView.getChildAt(0);
                    dayText.setTextColor(SELECTED_TEXT_VIEW_COLOR);
                }
            }
        };

        //Definir listeners a las cardviews
        for (CardView cardView : daysCardViews) {
            cardView.setOnClickListener(onClickListener);
        }
    }

    private void initChipRedManager() {
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                if (webServiceN == ChipRedManager.GET_TOKEN_CASH_CONFIG) {
                    ChipREDConstants.showSnackBarMessage(crMessage, TokenCashConfiguration.this);
                    content.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                } else if (webServiceN == ChipRedManager.CONFIG_TOKEN_CASH) {
                    //showDialogResult("Configuración tokencash", crMessage);
                    // Ejecutar función cuando la configuración se guardó correctamente
                    onTokencashConfigured();
                    finish();
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                showDialogResult("Error", errorMessage);
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                //Obtener configuración TC
                getTokenCashConfiguration(response);

                //Pedir productos
                stationManager.getStationProducts();

                //Mostrar elemento del menu
                menu.getItem(0).setVisible(true);
            }
        }, this);

        //Pedir datos de tokencash
        chipRedManager.getTokenCashConfiguration();
    }

    private void getTokenCashConfiguration(JSONObject response) {
        try {
            //Crear variable local
            int percentage;

            //Mostrar mensaje
            String message = response.getString("message");
            ChipREDConstants.showSnackBarMessage(message,
                    TokenCashConfiguration.this);

            //Obtener datos
            JSONObject data = response.getJSONObject("data");

            //Obtener estatus
            boolean emitir = data.getBoolean("emitir");
            if (emitir) {
                tokenCashSwitch.setChecked(true);
            } else {
                tokenCashSwitch.setChecked(false);
            }

            //Buscar opcion ticketless
            if (data.has("tipo_emision")) {
                setEmitionType(data.getInt("tipo_emision"));
            }

            //Obtener porcentaje
            if (data.has("porcentaje")) {
                percentage = data.getInt("porcentaje");
                percentageEt.setText(String.valueOf(percentage));
            }

            //Obtener monto máximo
            double maxAmountValue = data.getDouble("monto_maximo");
            maxAmount.setText(String.valueOf(maxAmountValue));

            //Obtener arreglo de días
            JSONArray days = data.getJSONArray("dias");
            setSelectedDays(days);

            //Obtener arreglo de clientes
            JSONArray clients = data.getJSONArray("tipo_cliente");
            setSelectedClients(clients);

            //Validar que existan restricciones de productos
            if (data.has("productos")) {
                //Inicializar lista
                selectedProducts = new ArrayList<>();

                JSONArray productsArray = data.getJSONArray("productos");
                for (int i = 0; i < productsArray.length(); i++) {
                    JSONObject product = productsArray.getJSONObject(i);
                    SelectedProduct selectedProduct = new SelectedProduct(
                            product.getString("descripcion"),
                            product.getDouble("porcentaje"),
                            product.getBoolean("activo"),
                            product.getInt("id")
                    );

                    selectedProducts.add(selectedProduct);
                }
            }

            //Validar que exista un máximo porcentaje de cobro definido
            if (data.has("maximo_porcentaje_cobro")) {
                maxTokenPercentageEt.setText(String.valueOf(data.getInt("maximo_porcentaje_cobro")));
            } else {
                maxTokenPercentageEt.setText("100");
            }

            //Validar que exista opcion para impresión de token de cobro
            if (data.has("impresion_token_cobro")) {
                printTokenSw.setChecked(data.getBoolean("impresion_token_cobro"));
            } else {
                printTokenSw.setChecked(true);
            }

            //Obtener editables
            if (data.has("editables")) {
                editables = data.getJSONObject("editables");
            }

            //Obtener modo de trabajo
            if (data.has("modo_trabajo")) {
                String workMode = data.getString("modo_trabajo");
                setWorkMode(workMode);
            }

            //No mostrar el teclado al iniciar
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            percentageEt.clearFocus();
        } catch (JSONException e) {
            showDialogResult("Error", "Error al procesar la información");
        }

        //Obtener valor inicial del switch tokencash
        if (tokenCashSwitch.isChecked()) {
            onOffIcon.setImageResource(R.drawable.ic_power_green);
        } else {
            onOffIcon.setImageResource(R.drawable.ic_power_red);
        }
    }

    private void showDialogResult(String title, String message) {
        GenericDialog genericDialog = new GenericDialog(title, message, new
                Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, null, TokenCashConfiguration.this);

        genericDialog.setPositiveText("OK");
        genericDialog.setCancelable(false);
        genericDialog.show();
    }

    private void onTokencashConfigured() {
        //Definir escenario localmente si la respuesta fue exitosa
        ChipREDConstants.setCurrentScenario(TokenCashConfiguration.this, getTokencashScenario());

        //Modificar archivo local .json
        StationFileManager.syncTokenCashInFile(fidelizacion, this);

        //Actualizar servidor local
        stationManager.syncLocalServer();

        //Guardar cambios de switches
        SharedPreferencesManager.putBoolean(this, ChipREDConstants.GENERATE_TOKEN,
                generateTokenSw.isChecked());

        SharedPreferencesManager.putBoolean(this, ChipREDConstants.RE_PRINT_TOKEN_CASH_PURCHASE,
                reprintTokenPurchaseSw.isChecked());
    }
}
