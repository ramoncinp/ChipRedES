package com.binarium.chipredes.tokencash.models;

import java.util.ArrayList;

public class Token
{
    private double purchaseAmount;
    private String purchaseId;
    private String transactionId;
    private String status;
    private String id;
    private String tokenValue;
    private ArrayList<TokenCashPayment> tokenCashPayments;

    public Token()
    {

    }

    public double getPurchaseAmount()
    {
        return purchaseAmount;
    }

    public void setPurchaseAmount(double purchaseAmount)
    {
        this.purchaseAmount = purchaseAmount;
    }

    public String getPurchaseId()
    {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId)
    {
        this.purchaseId = purchaseId;
    }

    public String getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getTokenValue()
    {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue)
    {
        this.tokenValue = tokenValue;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public ArrayList<TokenCashPayment> getTokenCashPayments()
    {
        return tokenCashPayments;
    }

    public void setTokenCashPayments(ArrayList<TokenCashPayment> tokenCashPayments)
    {
        this.tokenCashPayments = tokenCashPayments;
    }
}