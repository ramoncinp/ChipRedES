package com.binarium.chipredes.tokencash.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.binarium.chipredes.R;

import org.json.JSONException;
import org.json.JSONObject;

public class TicketsEditor extends AppCompatActivity
{
    public static final String EDITABLES_PARAM = "editables";

    //Variables
    private JSONObject editables;

    //Views
    private EditText editable1;
    private EditText editable2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tickets_editor);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        setTitle("Editor de tickets");

        //Obtener editText's
        editable1 = findViewById(R.id.editable_1);
        editable2 = findViewById(R.id.editable_2);

        //Obtener editables
        String mEditables = getIntent().getStringExtra(EDITABLES_PARAM);
        if (mEditables != null && !mEditables.isEmpty())
        {
            try
            {
                //Convertir a JSON
                JSONObject mEditableJsonObject = new JSONObject(mEditables);

                //Obtener cada valor
                editable1.setText(mEditableJsonObject.getString("editable_1"));
                editable2.setText(mEditableJsonObject.getString("editable_2"));
            }
            catch (JSONException e)
            {
                Toast.makeText(this, "Error al obtener los valores de los editables",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.tc_edit_ticket_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            finish();
        } else if (itemId == R.id.save_tc_ticket) {
            saveEditables();
            returnResult();
        }

        return true;
    }

    private void saveEditables()
    {
        //Obtener valores
        try
        {
            editables = new JSONObject();
            editables.put("editable_1", editable1.getText().toString());
            editables.put("editable_2", editable2.getText().toString());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void returnResult()
    {
        //Obtener resultados
        String mEditables = editables.toString();

        //Crear intent de regreso
        Intent returnIntent = new Intent();
        returnIntent.putExtra(EDITABLES_PARAM, mEditables);
        setResult(RESULT_OK, returnIntent);

        //Terminar actividad
        finish();
    }
}
