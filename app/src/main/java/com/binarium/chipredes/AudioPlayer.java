package com.binarium.chipredes;

import android.content.Context;
import android.media.MediaPlayer;

public class AudioPlayer
{
    private MediaPlayer mediaPlayer;

    public void stop()
    {
        if (mediaPlayer != null)
        {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void playSuccesSound1 (Context context)
    {
        stop();

        mediaPlayer = MediaPlayer.create(context, R.raw.ok);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                stop();
            }
        });

        mediaPlayer.start();
    }
}
