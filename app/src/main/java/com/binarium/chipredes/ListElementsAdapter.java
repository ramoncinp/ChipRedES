package com.binarium.chipredes;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListElementsAdapter extends RecyclerView.Adapter<ListElementsAdapter.ListElementsViewHolder>
        implements View.OnClickListener
{
    private ArrayList<String> listElementDesc;
    private ArrayList<Integer> imagesReferences;
    private View.OnClickListener listener;

    public ListElementsAdapter(
            ArrayList<String> listElementDesc,
            ArrayList<Integer> imagesReferences)
    {
        this.listElementDesc = listElementDesc;
        this.imagesReferences = imagesReferences;
    }

    @Override
    public int getItemCount()
    {
        return listElementDesc.size();
    }

    @Override
    public ListElementsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.payment_method_card_view,
                viewGroup,
                false);

        ListElementsViewHolder viewHolder = new ListElementsViewHolder(v);

        v.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ListElementsViewHolder listElementsViewHolder, int i)
    {
        listElementsViewHolder.desc.setText(
                listElementDesc.get(i)
        );

        listElementsViewHolder.image.setImageResource(
                imagesReferences.get(i)
        );
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    public static class ListElementsViewHolder extends RecyclerView.ViewHolder
    {
        private TextView desc;
        private ImageView image;

        public ListElementsViewHolder(View itemView)
        {
            super(itemView);

            desc = itemView.findViewById(R.id.payment_method_desc);
            image = itemView.findViewById(R.id.payment_method_image);
        }
    }
}