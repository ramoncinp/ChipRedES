package com.binarium.chipredes.balance.views

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ListView
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.R
import com.binarium.chipredes.TwoRowAdapter
import com.binarium.chipredes.balance.models.PaymentData
import com.binarium.chipredes.balance.utils.PAGO_EMAX_EFECTIVO

class PaymentConfirmDialog(val context: Context, private val paymentData: PaymentData) {

    val keys = arrayListOf<String>()
    val values = arrayListOf<String>()

    init {
        setKeyValues()
    }

    private fun setKeyValues() {
        keys.add("Cliente: ")
        values.add(paymentData.localClientData?.email ?: "")

        keys.add("Cantidad: ")
        values.add(ChipREDConstants.MX_AMOUNT_FORMAT.format(paymentData.amount))

        keys.add("Método de pago: ")
        values.add(paymentData.emaxPaymentType?.description ?: "")

        if (paymentData.emaxPaymentType?.description != PAGO_EMAX_EFECTIVO) {
            if (paymentData.emaxAccount?.isEmpty() == true) {
                keys.add("Terminal: ")
                values.add(paymentData.terminalDesc ?: "")
            } else {
                keys.add("Banco: ")
                values.add(paymentData.emaxAccountDesc ?: "")
            }
        }

        keys.add("Despachador: ")
        values.add(paymentData.pumper?.nombre ?: "")
    }

    fun showDialog(layoutInflater: LayoutInflater, onSubmit: () -> Unit) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Confirmación de abono")

        val content: View = layoutInflater.inflate(R.layout.dialog_payment_confirm, null)
        val paymentConceptsLv = content.findViewById<ListView>(R.id.payment_confirm_items)
        paymentConceptsLv.adapter = TwoRowAdapter(context, keys, values)

        builder.setView(content)
        builder.setPositiveButton("Ok") { dialog, _ ->
            onSubmit()
            dialog.dismiss()
        }
        builder.setNegativeButton("Cancelar") { dialog, _ ->
            dialog.dismiss()
        }

        val dialog = builder.create()
        dialog.setCancelable(false)
        dialog.show()
    }
}