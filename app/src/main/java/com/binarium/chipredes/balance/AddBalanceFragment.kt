package com.binarium.chipredes.balance

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.binarium.chipredes.R
import com.binarium.chipredes.balance.models.EmaxBankEntity
import com.binarium.chipredes.balance.models.EmaxPaymentType
import com.binarium.chipredes.balance.utils.EMAX_PAYMENT_TYPES
import com.binarium.chipredes.balance.utils.PAGO_EMAX_EFECTIVO
import com.binarium.chipredes.balance.views.PaymentConfirmDialog
import com.binarium.chipredes.databinding.FragmentAddBalanceBinding
import com.binarium.chipredes.internationalization.LanguageManager
import com.binarium.chipredes.searchclient.SelectedClientViewModel
import com.binarium.chipredes.utils.showSimpleDialog
import com.binarium.chipredes.utils.validator.validateEmpty
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke.models.Pumper
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddBalanceFragment : Fragment() {

    private val selectedClientViewModel: SelectedClientViewModel by activityViewModels()
    private val viewModel: AddToBalanceViewModel by viewModels()

    private var _binding: FragmentAddBalanceBinding? = null
    private val binding
        get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedClientViewModel.selectedClient.observe(this) {
            onClientSelected(it)
        }

        if (selectedClientViewModel.selectedClient.value == null) {
            navigateToSearchFragment()
        }
    }

    private fun navigateToSearchFragment() {
        findNavController().navigate(
            AddBalanceFragmentDirections.addBalanceToSearchClient()
        )
    }

    private fun onClientSelected(localClientData: LocalClientData) {
        viewModel.onLocalClientSelected(localClientData)
        binding.addBalanceSelectClient.setText(localClientData.toString())
        binding.addBalanceSelectPaymentType.requestFocus()
        binding.addBalanceSelectPaymentType.showDropDown()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentAddBalanceBinding.inflate(layoutInflater, container, false)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.movimientos_cuenta_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.searchLocalClientFragment) {
            navigateToSearchFragment()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initViews() {
        // Define suggestions
        setPaymentMethodsSuggestions()

        // Init pumpers suggestion adapter
        binding.addBalanceSelectPumper.setAdapter(
            ArrayAdapter(
                requireContext(),
                android.R.layout.simple_expandable_list_item_1,
                arrayListOf<Pumper>()
            )
        )

        // Define buttons listeners
        binding.addBalanceSubmitButton.setOnClickListener { onSubmit() }
        binding.addBalanceCancelButton.setOnClickListener { requireActivity().finish() }

        val pumperDataTitle = "Datos del ${
            LanguageManager.getString(
                LanguageManager.DESPACHADOR,
                "Despachador",
                requireContext()
            )
        }"

        binding.pumperInfoText.text = pumperDataTitle

        binding.clearQuery.setOnClickListener {
            viewModel.paymentData.localClientData = null
            binding.addBalanceSelectClient.setText("")
        }
    }

    private fun initObservers() {
        viewModel.initialDataReady.observe(viewLifecycleOwner) {
            setPumpersSuggestionList(viewModel.pumpersList ?: listOf())
            setBanksSuggestions(viewModel.emaxBanksList ?: listOf())
        }

        viewModel.clientError.observe(viewLifecycleOwner) {
            binding.addBalanceSelectClient.error = "Campo obligatorio"
        }

        viewModel.paymentMethodError.observe(viewLifecycleOwner) {
            binding.addBalanceSelectPaymentType.error = "Campo obligatorio"
        }

        viewModel.bankError.observe(viewLifecycleOwner) {
            binding.addBalanceBank.error = "Campo obligatorio"
        }

        viewModel.pumperError.observe(viewLifecycleOwner) {
            binding.addBalanceSelectPumper.error = "Campo obligatorio"
        }

        viewModel.addToBalanceError.observe(viewLifecycleOwner) {
            showSimpleDialog(requireContext(), "Error", it) {}
        }

        viewModel.addToBalanceSuccess.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), "Abono realizado correctamente", Toast.LENGTH_LONG)
                .show()
            requireActivity().finish()
        }

        viewModel.isLoading.observe(viewLifecycleOwner) { loading ->
            binding.addBalanceContent.visibility = if (loading) View.GONE else View.VISIBLE
            binding.addBalanceProgressBar.visibility = if (loading) View.VISIBLE else View.GONE
        }
    }

    private fun setPaymentMethodsSuggestions() {
        // Create adapter for suggestions
        val accountsAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_dropdown_item_1line,
            EMAX_PAYMENT_TYPES
        )
        binding.addBalanceSelectPaymentType.setAdapter(accountsAdapter)
        binding.addBalanceSelectPaymentType.setOnItemClickListener { parent, _, position, _ ->
            val selectedPaymentType = parent.adapter.getItem(position) as EmaxPaymentType
            onPaymentMethodSelected(selectedPaymentType)
        }
    }

    private fun setBanksSuggestions(banks: List<EmaxBankEntity>) {
        // Create adapter for suggestions
        val accountsAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_dropdown_item_1line,
            banks
        )
        binding.addBalanceBank.setAdapter(accountsAdapter)
        binding.addBalanceBank.setOnItemClickListener { parent, _, position, _ ->
            val selectedBank = parent.adapter.getItem(position) as EmaxBankEntity
            onBankEntitySelected(selectedBank)
        }
    }

    private fun setPumpersSuggestionList(pumpers: List<Pumper>) {
        val adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line, pumpers)
        val spinner = binding.addBalanceSelectPumper
        spinner.setAdapter(adapter)

        if (pumpers.isNotEmpty()) {
            spinner.setOnItemClickListener { parent, _, position, _ ->
                val selectedPumper = parent.adapter.getItem(position) as Pumper
                binding.addBalanceSelectPumper.setText(selectedPumper.nombre)
                binding.addBalancePumperPassword.requestFocus()
                viewModel.paymentData.pumper = selectedPumper
            }
        } else {
            Toast.makeText(requireContext(), "No hay despachadores registrados", Toast.LENGTH_LONG)
                .show()
            requireActivity().finish()
        }
    }

    private fun onSubmit() {
        if (validateFields()) {
            getViewsValues()
            showConfirmDialog()
        }
    }

    private fun showConfirmDialog() {
        val paymentConfirmDialog = PaymentConfirmDialog(requireContext(), viewModel.paymentData)
        paymentConfirmDialog.showDialog(layoutInflater) {
            viewModel.addToBalance()
        }
    }

    private fun validateFields(): Boolean {
        var isValid = true
        isValid = isValid.and(binding.addBalanceAmount.validateEmpty())
        isValid = isValid.and(binding.addBalanceReference.validateEmpty())
        isValid = isValid.and(binding.addBalanceFolio.validateEmpty())
        isValid = isValid.and(binding.addBalancePumperPassword.validateEmpty())
        isValid = isValid.and(viewModel.validateData())
        return isValid
    }

    private fun getViewsValues() {
        viewModel.paymentData.amount = binding.addBalanceAmount.text.toString().toDouble()
        viewModel.paymentData.reference = binding.addBalanceReference.text.toString()
        viewModel.paymentData.id = binding.addBalanceFolio.text.toString()
        viewModel.paymentData.pumperNip = binding.addBalancePumperPassword.text.toString()
    }

    private fun onPaymentMethodSelected(selectedPaymentType: EmaxPaymentType) {
        viewModel.paymentData.emaxPaymentType = selectedPaymentType

        if (selectedPaymentType.description == PAGO_EMAX_EFECTIVO) {
            binding.addBalanceBank.visibility = View.GONE
            viewModel.paymentData.emaxAccount = ""
            viewModel.paymentData.terminal = ""
        } else {
            binding.addBalanceBank.visibility = View.VISIBLE
            binding.addBalanceBank.setText("")
            if (viewModel.isBankTerminalMethod(selectedPaymentType)) {
                setBanksSuggestions(viewModel.emaxBankTerminals ?: listOf())
            } else {
                setBanksSuggestions(viewModel.emaxBanksList ?: listOf())
            }
        }
    }

    private fun onBankEntitySelected(bankEntity: EmaxBankEntity) {
        val selectedPaymentType = viewModel.paymentData.emaxPaymentType
        selectedPaymentType?.let {
            viewModel.onBankEntitySelected(it, bankEntity)
        }
    }
}
