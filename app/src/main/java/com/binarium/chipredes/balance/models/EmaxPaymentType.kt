package com.binarium.chipredes.balance.models

data class EmaxPaymentType(
    val description: String,
    val value: Int
) {
    override fun toString(): String {
        return description
    }
}
