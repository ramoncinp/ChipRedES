package com.binarium.chipredes.balance.utils

import com.binarium.chipredes.balance.models.EmaxPaymentType

const val PAGO_EMAX_EFECTIVO = "Efectivo"
const val PAGO_EMAX_TRANSFERENCIA = "Transferencia"
const val PAGO_EMAX_CHEQUE = "Cheque"
const val PAGO_EMAX_TARJETA_CREDITO = "Tarjeta de crédito"
const val PAGO_EMAX_TARJETA_DEBITO = "Tarjeta de débito"

internal val EMAX_PAYMENT_TYPES = listOf(
    EmaxPaymentType(PAGO_EMAX_EFECTIVO, 1),
    EmaxPaymentType(PAGO_EMAX_TRANSFERENCIA, 2),
    EmaxPaymentType(PAGO_EMAX_CHEQUE, 3),
    EmaxPaymentType(PAGO_EMAX_TARJETA_CREDITO, 4),
    EmaxPaymentType(PAGO_EMAX_TARJETA_DEBITO, 10)
)
