package com.binarium.chipredes.balance.models

open class EmaxBankEntity(val id: String, val description: String)
data class BankTerminal(val identifier: String, val name: String) :
    EmaxBankEntity(identifier, name) {
    override fun toString(): String {
        return name
    }
}

data class EmaxBank(val identifier: String, val name: String) : EmaxBankEntity(identifier, name) {
    override fun toString(): String {
        return name
    }
}
