package com.binarium.chipredes.balance.domain.mappers

import com.binarium.chipredes.balance.models.PaymentData
import com.binarium.chipredes.utils.DateUtils
import com.binarium.chipredes.wolke2.models.AddToBalanceTransaction
import com.binarium.chipredes.wolke2.models.Pumper
import java.util.*

internal fun PaymentData.toAddToBalanceTransaction(): AddToBalanceTransaction {
    val paymentPumper = Pumper(
        pumper?.idEmax ?: "",
        pumper?.nombre ?: "",
        pumper?.iniciales ?: ""
    )

    val dateTime = DateUtils.chipRedDateFormat.format(Date())

    return AddToBalanceTransaction(
        localClientData?.idCuenta ?: "",
        amount!!,
        paymentPumper,
        dateTime
    )
}
