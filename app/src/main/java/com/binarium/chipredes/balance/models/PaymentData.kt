package com.binarium.chipredes.balance.models

import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke.models.Pumper

data class PaymentData(
    var localClientData: LocalClientData? = null,
    var amount: Double? = null,
    var pumper: Pumper? = null,
    var pumperNip: String? = null,
    var emaxPaymentType: EmaxPaymentType? = null,
    var reference: String? = null,
    var id: String? = null,
    var date: String? = null,
    var emaxAccount: String? = null,
    var emaxAccountDesc: String? = null,
    var terminal: String? = null,
    var terminalDesc: String? = null,
)
