package com.binarium.chipredes.balance

import android.content.SharedPreferences
import androidx.lifecycle.*
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.balance.domain.mappers.toAddToBalanceTransaction
import com.binarium.chipredes.balance.models.*
import com.binarium.chipredes.balance.utils.PAGO_EMAX_EFECTIVO
import com.binarium.chipredes.balance.utils.PAGO_EMAX_TARJETA_CREDITO
import com.binarium.chipredes.balance.utils.PAGO_EMAX_TARJETA_DEBITO
import com.binarium.chipredes.configstation.service.StationManagerService
import com.binarium.chipredes.emax.EmaxApiService
import com.binarium.chipredes.emax.EmaxDBManager
import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke.models.Pumper
import com.binarium.chipredes.wolke2.Wolke2ApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class AddToBalanceViewModel @Inject constructor(
    private val stationManagerService: StationManagerService,
    private val emaxApiService: EmaxApiService,
    private val emaxDBManager: EmaxDBManager,
    private val wolke2ApiService: Wolke2ApiService,
    private val logger: Logger,
    sharedPreferences: SharedPreferences,
) : ViewModel() {
    val stationId: String =
        sharedPreferences.getString(ChipREDConstants.STATION_MONGO_ID, "").toString()

    // Variables
    val paymentData = PaymentData()
    var pumpersList: List<Pumper>? = null
    var emaxBanksList: List<EmaxBank>? = null
    var emaxBankTerminals: List<BankTerminal>? = null

    private val _initialDataReady = MutableLiveData<Boolean>()
    val initialDataReady: LiveData<Boolean>
        get() = _initialDataReady

    // Errors
    private val _clientError = MutableLiveData<Boolean>()
    val clientError: LiveData<Boolean>
        get() = _clientError

    private val _paymentMethodError = MutableLiveData<Boolean>()
    val paymentMethodError: LiveData<Boolean>
        get() = _paymentMethodError

    private val _bankError = MutableLiveData<Boolean>()
    val bankError: LiveData<Boolean>
        get() = _bankError

    private val _pumperError = MutableLiveData<Boolean>()
    val pumperError: LiveData<Boolean>
        get() = _pumperError

    private val _addToBalanceError = MutableLiveData<String>()
    val addToBalanceError: LiveData<String>
        get() = _addToBalanceError

    private val _addToBalanceSuccess = MutableLiveData<Boolean>()
    val addToBalanceSuccess: LiveData<Boolean>
        get() = _addToBalanceSuccess

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    init {
        getInitialData()
    }

    private fun getInitialData() {
        viewModelScope.launch(Dispatchers.IO) {
            _isLoading.postValue(true)
            getStationPumpers()
            getStationBanks()
            getBankTerminals()
            _initialDataReady.postValue(true)
            _isLoading.postValue(false)
        }
    }

    private suspend fun getStationPumpers() {
        // Buscar cliente
        Timber.i("Fetching station pumpers")

        try {
            // Crear petición
            val pumpersRequest = stationManagerService.getStationPumpers(stationId = stationId)

            // Obtener data
            val pumpers = pumpersRequest.data?.pumpers
            pumpers?.let { list ->
                val sortedList = list.sortedBy { it.nombre }
                pumpersList = sortedList
            }

        } catch (e: Exception) {
            Timber.e("Error fetching station pumpers -> $e")
        }
    }

    private fun getStationBanks() {
        Timber.i("Getting emax accounts")
        try {
            val emaxBanks = emaxDBManager.getBanks()
            emaxBanksList = emaxBanks.sortedBy { it.description }

        } catch (e: Exception) {
            Timber.e("Error trying to get the emax banks")
        }
    }

    private fun getBankTerminals() {
        Timber.i("Getting emax terminals")
        try {
            val terminals = emaxDBManager.getBankTerminals()
            emaxBankTerminals = terminals.sortedBy { it.description }

        } catch (e: Exception) {
            Timber.e("Error trying to get the emax banks")
        }
    }

    fun onLocalClientSelected(localClientData: LocalClientData) {
        paymentData.localClientData = localClientData
        if (localClientData.estatus != "AC") {
            _addToBalanceError.value = "El cliente no ha confirmado su correo electrónico"
        }
    }

    fun validateData(): Boolean {
        var isValid = true

        if (paymentData.localClientData == null) {
            isValid = isValid.and(false)
            _clientError.value = true
        }

        if (paymentData.localClientData?.estatus != "AC") {
            _addToBalanceError.value = "El cliente no ha confirmado su correo electrónico"
            return false
        }

        if (paymentData.emaxPaymentType == null) {
            isValid = isValid.and(false)
            _paymentMethodError.value = true
        } else if (paymentData.emaxPaymentType?.description != PAGO_EMAX_EFECTIVO) {
            if (paymentData.emaxAccount == null) {
                isValid = isValid.and(false)
                _bankError.value = true
            }
        }

        if (paymentData.pumper == null) {
            isValid = isValid.and(false)
            _pumperError.value = true
        }

        return isValid
    }

    fun addToBalance() {
        paymentData.amount?.let {
            if (it <= 0) {
                _addToBalanceError.value = "Cantidad para abono no válida"
                return
            }
        }

        if (paymentData.pumperNip != paymentData.pumper?.nip) {
            _addToBalanceError.value = "NIP de despachador incorrecto"
            return
        }

        viewModelScope.launch {
            _isLoading.value = true
            addToBalancePipeline()
            _isLoading.value = false
        }
    }

    private suspend fun addToBalancePipeline() {
        try {
            if (emaxApiService.testConnection().not()) {
                _addToBalanceError.value = "No responde servidor EMAX"
                return
            }
        } catch (e: Exception) {
            _addToBalanceError.value = "No responde EMAX"
            return
        }

        try {
            val registerDepositResult = emaxApiService.registerDeposit(paymentData)
            if (registerDepositResult == null) {
                _addToBalanceError.value = "Error al abonar saldo en EMAX"
                return
            } else if (registerDepositResult.responseCode != 0) {
                _addToBalanceError.value = registerDepositResult.errorMessage
                registerDepositResult.exceptionMessage?.let { error ->
                    logger.postMessage(
                        error,
                        LogLevel.ERROR,
                        paymentData.localClientData?.id.orEmpty()
                    )
                }

                return
            }
        } catch (e: Exception) {
            _addToBalanceError.value = "Error al abonar saldo en EMAX"
            return
        }

        try {
            val addToChipRedBalanceResult = wolke2ApiService.addToBalance(
                paymentData.toAddToBalanceTransaction()
            )

            if (addToChipRedBalanceResult.isSuccessful.not()) {
                val errorMessage = addToChipRedBalanceResult.errorBody()?.string()
                    ?: "Error al abonar saldo en ChipRED"
                _addToBalanceError.value = errorMessage
                return
            }
        } catch (e: Exception) {
            _addToBalanceError.value = "Error al abonar saldo a ChipRED"
            return
        }

        _addToBalanceSuccess.value = true
    }

    fun onBankEntitySelected(emaxPaymentType: EmaxPaymentType, bankEntity: EmaxBankEntity) {
        if (isBankTerminalMethod(emaxPaymentType)) {
            paymentData.emaxAccount = ""
            paymentData.emaxAccountDesc = ""
            paymentData.terminal = bankEntity.id
            paymentData.terminalDesc = bankEntity.description
        } else {
            paymentData.emaxAccount = bankEntity.id
            paymentData.emaxAccountDesc = bankEntity.description
            paymentData.terminal = ""
            paymentData.terminalDesc = ""
        }
    }

    fun isBankTerminalMethod(paymentMethod: EmaxPaymentType): Boolean =
        (paymentMethod.description == PAGO_EMAX_TARJETA_DEBITO) or
                (paymentMethod.description == PAGO_EMAX_TARJETA_CREDITO)
}
