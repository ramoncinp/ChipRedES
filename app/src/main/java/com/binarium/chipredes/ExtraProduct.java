package com.binarium.chipredes;

public class ExtraProduct
{
    private String categoria;
    private String descripcion;
    private String codigo;
    private String presentacion;

    private String[] mondeda;

    private int existencias;
    private int cantidad;

    private double precio;

    private boolean selected;
    private boolean agotado;

    public ExtraProduct()
    {

    }

    public ExtraProduct(String descripcion, String categoria)
    {
        this.descripcion = descripcion;
        this.categoria = categoria;

        selected = false;
    }

    public String getCategoria()
    {
        return categoria;
    }

    public double getPrecio()
    {
        return precio;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public String getPresentacion()
    {
        return presentacion;
    }

    public String[] getMondeda()
    {
        return mondeda;
    }

    public int getExistencias()
    {
        return existencias;
    }

    public int getCantidad()
    {
        return cantidad;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public boolean isAgotado()
    {
        return agotado;
    }

    public void setAgotado(boolean agotado)
    {
        this.agotado = agotado;
    }

    public void setCategoria(String categoria)
    {
        this.categoria = categoria;
    }

    public void setPrecio(double precio)
    {
        this.precio = precio;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public void setPresentacion(String presentacion)
    {
        this.presentacion = presentacion;
    }

    public void setMondeda(String[] mondeda)
    {
        this.mondeda = mondeda;
    }

    public void setExistencias(int existencias)
    {
        this.existencias = existencias;
    }

    public void setCantidad(int cantidad)
    {
        this.cantidad = cantidad;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }
}
