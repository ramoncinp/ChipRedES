package com.binarium.chipredes.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import com.binarium.chipredes.AddClient;
import com.binarium.chipredes.BuildConfig;
import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.GetBalance;
import com.binarium.chipredes.Integration;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.addstationaccount.AddStationAccount;
import com.binarium.chipredes.balance.AddToBalanceActivity;
import com.binarium.chipredes.billets.ui.validate.ValidateBilletsActivity;
import com.binarium.chipredes.movimientoscuenta.MovimientosCuentaActivity;
import com.google.android.material.navigation.NavigationView;

import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.config.SettingsActivity;
import com.binarium.chipredes.tokencash.ui.TokenCashSummary;
import com.binarium.chipredes.tokencash.ui.TokencashPurchases;
import com.google.firebase.messaging.FirebaseMessaging;

import dagger.hilt.android.AndroidEntryPoint;
import timber.log.Timber;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity implements NavigationView
        .OnNavigationItemSelectedListener
{
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private MainFragment mainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Suscribirse a las notificaciones
        FirebaseMessaging.getInstance().subscribeToTopic("ChipREDES");

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.devices_toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string
                .navigation_drawer_open, R.string.navigation_drawer_close)
        {
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                //hideKeyboard();
                ChipREDConstants.hideKeyboardFromActivity(MainActivity.this);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Visualizar version de app
        View header = navigationView.getHeaderView(0);
        TextView versionTv = header.findViewById(R.id.version_app);
        String version = "v" + BuildConfig.VERSION_NAME;
        versionTv.setText(version);

        //Habilitar o deshabilitar items del menu
        checkMenuItemsToEnable(navigationView.getMenu());
        checkMenuSectionsToEnable(navigationView.getMenu());

        setMainFragment();
        navigationView.setCheckedItem(R.id.main_fragment);

        //Pedir permiso de la camara
        getPermission();
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (resultCode)
        {
            case SettingsActivity.REPALCE_SERVER_ADDR:
                setMainFragment();
                break;

            case SettingsActivity.CHANGE_MENU_ITEMS_VISIBILITY:
                Intent intent = getIntent();
                finish();
                startActivity(intent);
                break;

            default:
                if (mainFragment != null)
                {
                    mainFragment.getStationData();
                }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.main_fragment)
        {
            setMainFragment();
        }
        else if (id == R.id.config)
        {
            askAuth(() ->
            {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivityForResult(intent, SettingsActivity.REPALCE_SERVER_ADDR);
            });
        }
        else if (id == R.id.add_client)
        {
            Intent intent = new Intent(MainActivity.this, AddClient.class);
            intent.putExtra("activityAction", 1);
            startActivity(intent);
        }
        else if (id == R.id.modify_client)
        {
            Intent intent = new Intent(MainActivity.this, AddClient.class);
            intent.putExtra("activityAction", 2);
            startActivity(intent);
        }
        else if (id == R.id.add_account)
        {
            Intent intent = new Intent(MainActivity.this, AddStationAccount.class);
            startActivity(intent);
        }
        else if (id == R.id.add_balance)
        {
            Intent intent = new Intent(MainActivity.this, AddToBalanceActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.get_balance)
        {
            Intent intent = new Intent(MainActivity.this, GetBalance.class);
            startActivity(intent);
            showKeyboard(this);
        }
        else if (id == R.id.get_purchases)
        {
            Intent intent = new Intent(MainActivity.this, MovimientosCuentaActivity.class);
            intent.putExtra(ChipREDConstants.PURCHASE_OR_PAYMENT, 1);
            startActivity(intent);

            //showKeyboard(this);
        }
        else if (id == R.id.get_payments)
        {
            Intent intent = new Intent(MainActivity.this, MovimientosCuentaActivity.class);
            intent.putExtra(ChipREDConstants.PURCHASE_OR_PAYMENT, 2);
            startActivity(intent);

            //showKeyboard(this);
        }
        else if (id == R.id.print_tokens)
        {
            Intent intent = new Intent(MainActivity.this, TokenCashSummary.class);
            startActivity(intent);
        }
        else if (id == R.id.tokencash_purchases)
        {
            Intent intent = new Intent(MainActivity.this, TokencashPurchases.class);
            startActivity(intent);
        }
        else if (id == R.id.validate_billets)
        {
            Intent intent = new Intent(MainActivity.this, ValidateBilletsActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.exit)
        {
            super.onBackPressed();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void setMainFragment()
    {
        mainFragment = new MainFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_fragment_container, mainFragment)
                .commit();
    }

    public void hideKeyboard()
    {
        try
        {
            View kb = this.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) (this.getSystemService(Context
                    .INPUT_METHOD_SERVICE));
            if (imm != null && kb != null)
            {
                imm.hideSoftInputFromWindow(kb.getWindowToken(), 0);
            }
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
    }

    public static void showKeyboard(Context context)
    {
        try
        {
            ((InputMethodManager) (context).getSystemService(Context.INPUT_METHOD_SERVICE))
                    .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager
                            .HIDE_IMPLICIT_ONLY);
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
    }

    public void askAuth(final Runnable action)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_ask_auth, null);
        final EditText nip = view.findViewById(R.id.nip_to_auth);

        //Obtener el Nip Actual
        final String currentNip = SharedPreferencesManager.getString(this, ChipREDConstants
                .APP_PIN, "");

        dialog.setMessage("Ingrese NIP");
        dialog.setView(view).setPositiveButton("Ingresar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String nipToAuth = nip.getText().toString();
                if (currentNip.equals(nipToAuth))
                {
                    action.run();
                }
                else
                {
                    Toast.makeText(MainActivity.this, "NIP incorrecto", Toast.LENGTH_SHORT).show();
                }
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = dialog.create();
        alertDialog.show();

        showKeyboard(this);
    }

    private void checkMenuItemsToEnable(Menu menu)
    {
        //Obtener los items del menu y sus propiedades
        String[][] mMenuItems = ChipREDConstants.MENU_ITEMS;
        for (String[] integrationArray : mMenuItems)
        {
            //Crear instancia de un elemento del menu segun se definio en el arreglo de Strings
            Integration integration = new Integration(integrationArray[0], integrationArray[1],
                    this);

            int resource = this.getResources().getIdentifier(integration.getConstantName(), "id",
                    this.getPackageName());

            menu.findItem(resource).setVisible(integration.isEnabled());
        }
    }

    private void checkMenuSectionsToEnable(Menu menu)
    {
        // Obtener items del menu
        String[][] mIntegrations = ChipREDConstants.INTEGRATIONS;
        for (String[] integrationArray : mIntegrations)
        {
            //Crear instancia de un elemento del menu segun se definio en el arreglo de Strings
            Integration integration = new Integration(integrationArray[0], integrationArray[1],
                    this);

            int resource = this.getResources().getIdentifier(
                    integration.getConstantName(), "id", this.getPackageName());

            menu.findItem(resource).setVisible(integration.isEnabled());
        }
    }

    private void getPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager
                    .PERMISSION_GRANTED)
            {
                //No fue concedido el permiso
                if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA))
                {
                    showMessageOKCancel((dialog, which) -> requestPermissions(
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CODE_ASK_PERMISSIONS));
                    return;
                }

                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
    }

    private void showMessageOKCancel(DialogInterface.OnClickListener okListener)
    {
        new androidx.appcompat.app.AlertDialog.Builder(this).setMessage("Se necesita permiso para" +
                " la cámara para escanear códigos QR")
                .setPositiveButton("OK", okListener).setNegativeButton("Cancel", null).create()
                .show();
    }
}
