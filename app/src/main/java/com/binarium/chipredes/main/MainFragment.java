package com.binarium.chipredes.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.LoadingPosition;
import com.binarium.chipredes.PaymentMethod;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.adapters.LoadingPositionAdapter;
import com.binarium.chipredes.assignvehicleqr.AssignVehicleQR;
import com.binarium.chipredes.comparators.PumpComparator;
import com.binarium.chipredes.configstation.StationFileManager;
import com.binarium.chipredes.configstation.StationManager;
import com.binarium.chipredes.fcm.FcmUtils;
import com.binarium.chipredes.pendingsales.PendingSaleActivity;
import com.binarium.chipredes.utils.DebouncedOnClickListener;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainFragment extends Fragment {
    private static final int TEST_CONNECTION_DELAY = 15000;
    //Variables
    private boolean connectedToInternet = true;
    private boolean hasLocalServer = true;
    private String country;

    //Views
    private Dialog updateDialog;
    private ProgressBar progressBar;
    private RecyclerView loadingPositionList;
    private RelativeLayout noServerLayout;
    private TextView noInternetAccess;
    private TextView noDispensers;

    //Objetos
    private ChipRedManager chipRedManager;
    private StationManager stationManager;
    private Handler internetMonitor;
    private WebSocketClient webSocketClient;
    private final Runnable testConnectionR = new Runnable() {
        @Override
        public void run() {
            if (hasLocalServer)
                chipRedManager.getNetworkState();
        }
    };

    private MainFragmentViewModel viewModel;

    public MainFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(MainFragmentViewModel.class);
        viewModel.fetchRemoteConfig();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        progressBar = rootView.findViewById(R.id.main_progress_bar);
        loadingPositionList = rootView.findViewById(R.id.pc_list);
        noServerLayout = rootView.findViewById(R.id.no_server_layout);
        noInternetAccess = rootView.findViewById(R.id.no_internet_access);
        noDispensers = rootView.findViewById(R.id.no_dispensers);

        country = SharedPreferencesManager.getString(getActivity(), ChipREDConstants.COUNTRY);

        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        evaluatePendingUpdate();
        evaluateConnection();
        evaluateWebSocket();
    }

    @Override
    public void onPause() {
        super.onPause();
        pauseConnectionEvaluation();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            //Ejecutar acciones que requieren contexto
            getActivity().setTitle("ChipRED ES");
            updateDialog = new Dialog(getActivity());

            String getCountry = StationFileManager.getStationCountry(getActivity());
            if (!getCountry.equals("")) {
                //Si fue válido, asignar valor
                country = getCountry;
            }
        }

        //Inicializar manejador de estaciones
        initStationManager();

        //Inicialziar ChipRED
        initChipRedManager();

        //Obtener los datos de estación
        getStationData();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_fragment_menu, menu);
        if (!country.toLowerCase().equals("costa rica"))
            menu.findItem(R.id.assign_vehicle_qr).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.refresh_main_fragment) {
            getStationData();
        } else if (itemId == R.id.assign_vehicle_qr) {
            Intent intent = new Intent(getActivity(), AssignVehicleQR.class);
            intent.putExtra("action", AssignVehicleQR.ASSIGN_QR);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * @param response : Objeto JSON de entrada
     * @param dataKey  : Nombre del arreglo principal que contiene la información
     *                 puede ser "dispensarios" cuando se pide de CRlocal
     *                 y puede ser "data" cuando se recibe actualizacion de socket
     */
    private void parseLoadingPositionsResponse(JSONObject response, String dataKey) {
        ArrayList<LoadingPosition> positions = new ArrayList<>();
        try {
            //Validar si existen los dispensarios
            if (!response.has(dataKey)) {
                //No hay dispensarios registrados
                noDispensers.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                noServerLayout.setVisibility(View.GONE);
                loadingPositionList.setVisibility(View.GONE);

                return;
            } else {
                noDispensers.setVisibility(View.GONE);
            }

            //Obtener la información de los dispensarios
            JSONArray data = response.getJSONArray(dataKey);

            for (int i = 0; i < data.length(); i++) {
                int dispenserNumber;

                JSONObject dispJsonObject = data.getJSONObject(i);
                dispenserNumber = dispJsonObject.getInt("numero_dispensario");

                JSONArray dispSides = dispJsonObject.getJSONArray("lados");
                for (int y = 0; y < dispSides.length(); y++) {
                    JSONObject dispSide = dispSides.getJSONObject(y);
                    LoadingPosition loadingPosition = new LoadingPosition(dispSide.getInt
                            ("lado_estacion"));

                    loadingPosition.setDispenserNumber(dispenserNumber);
                    loadingPosition.setCmIp(dispSide.getString("cm_ip"));
                    loadingPosition.setCmPort(dispSide.getInt("cm_puerto"));
                    loadingPosition.setCmState(dispSide.getString("cm_estado"));
                    loadingPosition.setSide(dispSide.getInt("lado"));
                    loadingPosition.setSideTag(dispSide.getString("lado_tag"));
                    loadingPosition.setState(dispSide.getInt("estado"));
                    if (dispSide.has("estado_r"))
                        loadingPosition.setRealState(dispSide.getInt("estado_r"));
                    if (dispSide.has("lado_estacion_usuario"))
                        loadingPosition.setUserStationSide(dispSide.getInt("lado_estacion_usuario"
                        ));

                    positions.add(loadingPosition);
                }
            }

            //Acomodar posiciones de carga
            Collections.sort(positions, new PumpComparator());

            //Evaluar el servicio de donde viene
            if (dataKey.equals("data")) {
                //Viene de respuesta de websocket
                refreshStates(positions);
            } else {
                //Viene de respuesta de chipred-local
                drawLoadingPositions(positions);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            showNoServerLayout();
            Toast.makeText(getActivity(), "Error al obtener posiciones de carga", Toast
                    .LENGTH_SHORT).show();
        }
    }

    private void drawLoadingPositions(final ArrayList<LoadingPosition> positions) {
        LoadingPositionAdapter loadingPositionAdapter = new LoadingPositionAdapter(getContext(),
                positions, LoadingPositionAdapter.ACCENT_THEME);
        loadingPositionAdapter.setClickListener(new DebouncedOnClickListener(3000) {
            @Override
            public void onDebouncedClick(View v) {
                evaluatePendingUpdate();
                evaluateLoadingPositionSelected(positions.get(loadingPositionList
                        .getChildAdapterPosition(v)));
            }
        });
        loadingPositionAdapter.setLongClickListener(view -> {
            LoadingPosition loadingPosition =
                    positions.get(loadingPositionList.getChildAdapterPosition(view));

            if (loadingPosition.getState() != 0) {
                chipRedManager.liberateLoadingPosition(loadingPosition.getStationSide());
            }

            return false;
        });
        loadingPositionList.setAdapter(loadingPositionAdapter);
        loadingPositionList.setLayoutManager(new GridLayoutManager(getContext(), getResources()
                .getInteger(R.integer.loading_positions_columns_number)));
    }

    private void refreshStates(ArrayList<LoadingPosition> newStates) {
        LoadingPositionAdapter loadingPositionAdapter = (LoadingPositionAdapter)
                loadingPositionList.getAdapter();

        if (loadingPositionAdapter == null) return;

        for (int i = 0; i < newStates.size(); i++) {
            LoadingPosition loadingPosition = loadingPositionAdapter.getItem(i);
            loadingPosition.setState(newStates.get(i).getState());
        }

        loadingPositionAdapter.notifyDataSetChanged();
    }

    private void initChipRedManager() {
        chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(), MainFragment.this))
                    return;

                Log.d("ChipRedManager", "Respuesta en MainFragment -> " + crMessage);
                if (webServiceN != ChipRedManager.LOADING_POS_STATE_CHANGE) {
                    if (webServiceN == ChipRedManager.TEST_WOLKE) {
                        if (noInternetAccess.getVisibility() == View.VISIBLE)
                            noInternetAccess.setVisibility(View.GONE);

                        connectedToInternet = true;
                        internetMonitor.postDelayed(testConnectionR, TEST_CONNECTION_DELAY);
                    } else if (webServiceN == ChipRedManager.NO_URL_REQUEST) {
                        showNoServerLayout();
                    } else if (webServiceN == ChipRedManager.LIBERAR_POSICION) {
                        getStationData();
                    } else {
                        //Si no es una respuesta del websocket
                        if (webServiceN != ChipRedManager.TEST_WEB_SOCKET) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), crMessage, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(), MainFragment.this))
                    return;

                Log.d("ChipRedManager", "Respuesta en MainFragment -> " + errorMessage);
                if (webServiceN != ChipRedManager.LOADING_POS_STATE_CHANGE) {
                    if (webServiceN == ChipRedManager.TEST_WOLKE) {
                        //Mostrar que no hay conexión a internet
                        noInternetAccess.setVisibility(View.VISIBLE);
                        connectedToInternet = false;
                        internetMonitor.postDelayed(testConnectionR, TEST_CONNECTION_DELAY);
                    } else {
                        showNoServerLayout();
                    }
                }
            }

            @Override
            public void showResponse(final JSONObject response, int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(), MainFragment.this))
                    return;

                if (webServiceN != ChipRedManager.LOADING_POS_STATE_CHANGE) {
                    if (webServiceN == ChipRedManager.GET_NETWORK_STATE) {
                        evaluateConnectionState(response);
                    } else {
                        //Mostrar posiciones de carga
                        parseLoadingPositionsResponse(response, ChipREDConstants
                                .LOADING_POSITIONS_FROM_LOCAL);

                        //Procesar datos obtenidos
                        onStationData(response);

                        noServerLayout.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        loadingPositionList.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                parseLoadingPositionsResponse(response, ChipREDConstants
                                        .LOADING_POSITIONS_FROM_WEB_SOCKET);
                            }
                        });
                    }
                }
            }
        }, getActivity());

        //Conectar websocket
        connectWebSocket();
    }

    private void initStationManager() {
        stationManager = new StationManager(getActivity(), new StationManager.OnResponse() {
            @Override
            public void chipRedMessage(String crMessage, int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(), MainFragment.this))
                    return;

                Log.d("ChipRedManager", "Respuesta en MainFragment -> " + crMessage);
            }

            @Override
            public void chipRedError(String errorMessage, int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(), MainFragment.this))
                    return;

                showNoServerLayout();
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(), MainFragment.this))
                    return;

                try {
                    //Obtener documento de estacion
                    JSONObject station = response.getJSONObject("data").getJSONObject(
                            "datos_generales");

                    //Mostrar posiciones de carga
                    parseLoadingPositionsResponse(station, ChipREDConstants
                            .LOADING_POSITIONS_FROM_LOCAL);

                    //Procesar datos obtenidos
                    onStationData(station);

                    //Mostrar views necesarios
                    noServerLayout.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    loadingPositionList.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    showNoServerLayout();
                    Toast.makeText(getActivity(), "Error al obtener los datos de estación",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void getStationData() {
        showProgressBar();

        // Evaluar si la estación es E3 para tokencash
        String workMode = ChipREDConstants.getTokencashScenario(getActivity());
        if (workMode.equals("e1") || workMode.equals("e2")) {
            hasLocalServer = true;
            chipRedManager.getStationData();
        } else {
            hasLocalServer = false;
            stationManager.getGeneralData(null);
        }
    }

    private void showNoServerLayout() {
        noServerLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        loadingPositionList.setVisibility(View.GONE);
    }

    private void evaluatePendingUpdate() {
        String newVersion = SharedPreferencesManager.getString(getActivity(), ChipREDConstants
                .NEW_VERSION_AVAILABLE);

        if (!newVersion.equals("") && !updateDialog.isShowing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            View content = getLayoutInflater().inflate(R.layout.welcome_layout, null);

            TextView title = content.findViewById(R.id.title);
            TextView message = content.findViewById(R.id.text);

            String titleString = "Actualización disponible";
            title.setText(titleString);

            String messageString = "Instale la versión " + newVersion + " para asegurar el buen "
                    + "funcionamiento de " + "" + "" + "la aplicación";
            message.setText(messageString);

            builder.setView(content);
            builder.setPositiveButton("INSTALAR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    goToPlaystore();
                    dialog.dismiss();
                    SharedPreferencesManager.remove(getActivity(),
                            ChipREDConstants.NEW_VERSION_AVAILABLE);
                }
            });
            builder.setNegativeButton("DESPUÉS", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            updateDialog = builder.create();
            updateDialog.setCancelable(false);
            updateDialog.show();
        }
    }

    private void goToPlaystore() {
        if (getActivity() != null) {
            final String appPackageName = getActivity().getPackageName(); // getPackageName() from
            // Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" +
                        appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google" + "" +
                        "" + ".com/store/apps/details?id=" + appPackageName)));
            }
        }
    }

    private void showProgressBar() {
        noServerLayout.setVisibility(View.GONE);
        loadingPositionList.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void evaluateConnection() {
        if (internetMonitor == null) {
            internetMonitor = new Handler();
        }

        testConnectionR.run();
    }

    private void evaluateWebSocket() {
        if (webSocketClient != null) {
            if (webSocketClient.isClosed()) {
                //Refrescar manualmente las posiciones de carga
                getStationData();

                //Intentar reconectarse
                webSocketClient.reconnect();
            }
        } else {
            //Refrescar manualmente las posiciones de carga
            getStationData();
        }
    }

    private void evaluateConnectionState(JSONObject response) {
        try {
            // Obtener valor del estado
            int state = response.getJSONObject("data").getInt("estado");

            if (state == ChipRedManager.NETWORK_OK) {
                if (noInternetAccess.getVisibility() == View.VISIBLE)
                    noInternetAccess.setVisibility(View.GONE);

                connectedToInternet = true;
                internetMonitor.postDelayed(testConnectionR, TEST_CONNECTION_DELAY);
            } else {
                noInternetAccess.setVisibility(View.VISIBLE);
                connectedToInternet = false;
                internetMonitor.postDelayed(testConnectionR, TEST_CONNECTION_DELAY);

                if (state == ChipRedManager.WOLKE_NO_OK) {
                    noInternetAccess.setText(getResources().getString(R.string.no_wolke));
                } else if (state == ChipRedManager.CONTINGENCIA_ACTIVADA) {
                    noInternetAccess.setText(getResources().getString(R.string.contingencia_activada));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void pauseConnectionEvaluation() {
        if (internetMonitor != null) {
            internetMonitor.removeCallbacksAndMessages(null);
        }
    }

    private void evaluateLoadingPositionSelected(LoadingPosition loadingPosition) {
        if (country.toLowerCase().equals("costa rica") && !connectedToInternet) {
            Intent intent = new Intent(getActivity(), PendingSaleActivity.class);
            intent.putExtra("pump", loadingPosition.getStationSide());
            startActivity(intent);
        } else {
            Intent paymentMethodIntent = new Intent(getActivity(), PaymentMethod.class);
            paymentMethodIntent.putExtra("pump", loadingPosition.getStationSide());
            startActivity(paymentMethodIntent);
        }
    }

    private void connectWebSocket() {
        //Obtener direccion de webSocket
        String webSocketUrl = SharedPreferencesManager.getString(getActivity(),
                ChipREDConstants.CR_WEB_SOCKET_IP);

        //Retornar si fue vacía la url
        if (webSocketUrl.isEmpty()) {
            Log.d("WebSocket", "No se inició WebSocket por falta de URL válida");
            return;
        }

        URI uri;
        try {
            uri = new URI(webSocketUrl);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        //Inicializar cliente webSocket
        webSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                Log.d("WebSocket", "Websocket abierto");
            }

            @Override
            public void onMessage(final String message) {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Llegó actualización de posiciones de carga
                            try {
                                //Convertir mensaje a formato JSON
                                JSONObject response = new JSONObject(message);

                                //Ejecutar funcion para interpretar la informacion
                                parseLoadingPositionsResponse(response,
                                        ChipREDConstants.LOADING_POSITIONS_FROM_WEB_SOCKET);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                Log.d("WebSocket", "Websocket cerrado");
            }

            @Override
            public void onError(Exception ex) {
                Log.d("WebSocket", "Error en Websocket");
            }
        };

        //Iniciar conexión
        webSocketClient.connect();
    }

    private void onStationData(JSONObject data) {
        try {
            Context mContext = getActivity();

            // Obtener estacion
            String stationData = data.toString();

            // Obtener número de estacion
            String numeroEstacion = data.getString("id_estacion");

            // Obtener país
            JSONObject address = data.getJSONObject("direccion");
            String pais = address.getString("pais");

            // Obtener cédula o RFC
            String taxCode = data.getString("rfc_cedula");

            // Evaluar diferencias
            if (!stationData.equals(SharedPreferencesManager.getString(mContext,
                    ChipREDConstants.STATION_DATA))) {
                SharedPreferencesManager.putString(mContext, ChipREDConstants.STATION_DATA,
                        stationData);
            }

            if (!numeroEstacion.equals(SharedPreferencesManager.getString(mContext,
                    ChipREDConstants.STATION_ID))) {
                SharedPreferencesManager.putString(mContext, ChipREDConstants.STATION_ID,
                        numeroEstacion);
            }

            if (!pais.equals(SharedPreferencesManager.getString(mContext,
                    ChipREDConstants.COUNTRY))) {
                SharedPreferencesManager.putString(mContext, ChipREDConstants.COUNTRY, pais);
            }

            if (!taxCode.equals(SharedPreferencesManager.getString(mContext,
                    ChipREDConstants.STATION_TAX_CODE))) {
                SharedPreferencesManager.putString(mContext, ChipREDConstants.STATION_TAX_CODE,
                        taxCode);
            }

            // Una vez obtenido el numero de estación, evaluar si se debe suscribir a canal de
            // notificaciones
            if (ChipREDConstants.getTokencashScenario(mContext).equals("e3")) {
                FcmUtils.subscribeToCouponMessaging(mContext);
            } else {
                if (mContext != null)
                    FcmUtils.unsubscribeToCouponMessaging(mContext);
            }

            // Save station data in db
            viewModel.saveStationData(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
