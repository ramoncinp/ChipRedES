package com.binarium.chipredes.main

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.config.domain.FetchRemoteConfigValues
import com.binarium.chipredes.configstation.data.entities.ChipRedStation
import com.binarium.chipredes.db.StationDao
import com.binarium.chipredes.db.mappers.stationEntityFromJson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainFragmentViewModel @Inject constructor(
    private val fetchRemoteConfigValues: FetchRemoteConfigValues,
    private val stationDao: StationDao,
    sharedPreferences: SharedPreferences,
) : ViewModel() {

    private val mongoID = sharedPreferences.getString(
        ChipREDConstants.STATION_MONGO_ID, ""
    ).toString()

    fun saveStationData(data: JSONObject) {
        val station = stationEntityFromJson(mongoID, data)
        station?.let { saveStationInDb(it) }
    }

    private fun saveStationInDb(station: ChipRedStation) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                stationDao.dropDb()
                stationDao.insertStation(station)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun fetchRemoteConfig() {
        viewModelScope.launch {
            fetchRemoteConfigValues()
        }
    }
}
