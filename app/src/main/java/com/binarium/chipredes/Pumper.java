package com.binarium.chipredes;

import org.json.JSONException;
import org.json.JSONObject;

public class Pumper
{
    private String nameInitials;
    private String tagId;
    private String localId;
    private String emaxId;
    private String name;
    private String nip;

    private int listIndex;

    public Pumper()
    {

    }

    public String getNameInitials()
    {
        return nameInitials;
    }

    public void setNameInitials(String nameInitials)
    {
        this.nameInitials = nameInitials;
    }

    public String getTagId()
    {
        return tagId;
    }

    public void setTagId(String tagId)
    {
        this.tagId = tagId;
    }

    public String getLocalId()
    {
        return localId;
    }

    public void setLocalId(String localId)
    {
        this.localId = localId;
    }

    public String getEmaxId()
    {
        return emaxId;
    }

    public void setEmaxId(String emaxId)
    {
        this.emaxId = emaxId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getNip()
    {
        return nip;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public int getListIndex()
    {
        return listIndex;
    }

    public void setListIndex(int listIndex)
    {
        this.listIndex = listIndex;
    }

    public JSONObject toJson()
    {
        try
        {
            JSONObject pumper = new JSONObject();
            pumper.put("id_local", localId);
            pumper.put("id_emax", emaxId);
            pumper.put("nombre", name);
            pumper.put("tag", tagId);
            pumper.put("nip", nip);
            pumper.put("iniciales", nameInitials);

            return pumper;
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    @Override
    public String toString()
    {
        return nameInitials + " - " + name.toUpperCase();
    }
}
