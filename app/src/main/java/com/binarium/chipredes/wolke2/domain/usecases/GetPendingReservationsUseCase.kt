package com.binarium.chipredes.wolke2.domain.usecases

import com.binarium.chipredes.utils.DateUtils
import com.binarium.chipredes.wolke.domain.usecases.GetStationMongoIdUseCase
import com.binarium.chipredes.wolke2.data.Wolke2Repository
import com.binarium.chipredes.wolke2.models.ReservacionSaldo
import java.util.Date
import javax.inject.Inject

class GetPendingReservationsUseCase @Inject constructor(
    private val getStationMongoIdUseCase: GetStationMongoIdUseCase,
    private val wolke2Repository: Wolke2Repository,
) {

    suspend operator fun invoke(
        clientId: String,
        startDate: Date? = DateUtils.getDeltaDate(Date(), -7),
        endDate: Date? = Date()
    ): List<ReservacionSaldo> {

        return wolke2Repository.getPendingReservationsByDate(
            clientId,
            getStationMongoIdUseCase(),
            startDate,
            endDate
        )
    }
}
