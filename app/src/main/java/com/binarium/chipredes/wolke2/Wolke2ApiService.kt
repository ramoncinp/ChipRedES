package com.binarium.chipredes.wolke2

import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.billets.data.model.AdjustablePurchase
import com.binarium.chipredes.wolke2.models.*
import com.binarium.chipredes.wolke2.services.CancelBilletTransaction
import com.binarium.chipredes.wolke2.services.CreateStationAccount
import com.binarium.chipredes.wolke2.services.ReserveBilletTransaction
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

private const val BASE_URL = ChipREDConstants.WOLKE2_BASE_URL

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface Wolke2ApiService {

    @GET("api/clientes/{client_id}/cuentas")
    suspend fun getStationAccounts(@Path("client_id") clientId: String): ClientStationAccounts

    @GET("api/clientes/{client_id}/cuentas/{account_id}/movimientos")
    suspend fun getClientAccountTransactions(
        @Path("client_id") clientId: String,
        @Path("account_id") accountId: String,
        @Query("tipo_movimiento") tipoMovimiento: String,
        @Query("cantidad_movimientos") cantidad: String? = null,
        @Query("fecha_inicial") startDate: String? = null,
        @Query("fecha_final") endDate: String? = null,
    ): List<MovimientoCuenta>

    @GET("api/reservaciones")
    suspend fun getClientPendingReservations(
        @Query("id_estacion") stationId: String,
        @Query("id_cliente") clientId: String? = null,
        @Query("fecha_inicial") startDate: String? = null,
        @Query("fecha_final") endDate: String? = null,
    ): List<ReservacionSaldo>

    @POST("api/clientes/{client_id}/cuentas")
    suspend fun createStationAccount(
        @Path("client_id") clientId: String,
        @Body body: CreateStationAccount,
    ): ClientStationAccount

    @POST("rpc/reservar_saldo")
    suspend fun createBilletTransaction(
        @Body body: ReserveBilletTransaction,
    ): Response<ReserveTransactionResult>

    @POST("rpc/cancelar_reservacion_saldo")
    suspend fun cancelBilletTransaction(
        @Body body: CancelBilletTransaction,
    ): CancelBilletReservationResult

    @POST("rpc/abonar_saldo")
    suspend fun addToBalance(
        @Body body: AddToBalanceTransaction,
    ): Response<AddToBalanceResponse>

    @POST("rpc/conciliar_consumo")
    suspend fun adjustSale(
        @Body body: AdjustablePurchase,
    )

    @POST("api/movil_logging")
    suspend fun postLogMessage(
        @Body body: MovilLogging
    ): Response<Any>
}

object Wolke2Api {
    val retrofitService: Wolke2ApiService by lazy { retrofit.create(Wolke2ApiService::class.java) }
}
