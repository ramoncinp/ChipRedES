package com.binarium.chipredes.wolke2.services

import com.squareup.moshi.Json

data class ReserveBilletTransaction(
    @Json(name = "id_cliente") val clientId: String,
    @Json(name = "id_estacion") val stationId: String,
    @Json(name = "ids_billetes") val billetIds: List<String>,
    @Json(name = "posicion_carga") val loadingPosition: String,
    @Json(name = "url_placas") val urlPlates: String,
    val placas: String,
)
