package com.binarium.chipredes.wolke2.domain.usecases

import com.binarium.chipredes.billets.data.model.AdjustablePurchase
import com.binarium.chipredes.wolke2.Wolke2ApiService
import com.binarium.chipredes.wolke2.models.AdjustSaleResult
import javax.inject.Inject

class AdjustSaleUseCase @Inject constructor(
    private val wolke2ApiService: Wolke2ApiService,
) {
    suspend operator fun invoke(adjustablePurchase: AdjustablePurchase): AdjustSaleResult {
        return try {
            wolke2ApiService.adjustSale(adjustablePurchase)
            AdjustSaleResult.Success(adjustablePurchase)
        } catch (e: Exception) {
            AdjustSaleResult.Error(adjustablePurchase, "Error al ajustar saldo")
        }
    }
}
