package com.binarium.chipredes.wolke2.models

import com.squareup.moshi.Json

data class ClientStationAccounts(
        val data: List<ClientStationAccount>
)

data class ClientStationAccount(
        val _id: MongoId,
        val estacion: StationObject,
        val cliente: ClientObject,
        val tipo: String,
        val saldo: Double,
        @Json(name = "id_cliente_local") val idClienteLocal: String,
        val vehiculo: VehicleObject
)

data class MongoId(
        @Json(name = "\$oid") val id: String
)

data class StationObject(
        @Json(name = "nombre_estacion") var nombreEstacion: String? = "",
        @Json(name = "\$oid") val id: String
)

data class ClientObject(
        @Json(name = "\$oid") val id: String
)

data class VehicleObject(
        val placas: String,
        @Json(name = "tipo_vehiculo") val tipoVehiculo: String,
        @Json(name = "no_tag") val noTag: String,
        val nip: String,
        @Json(name = "consumo_maximo") val consumoMaximo: String,
)