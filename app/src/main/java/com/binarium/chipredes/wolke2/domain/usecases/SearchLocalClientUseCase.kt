package com.binarium.chipredes.wolke2.domain.usecases

import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke.models.SearchStationClientByEmailPost
import com.binarium.chipredes.wolke.models.SearchStationClientByNamePost
import com.binarium.chipredes.wolke.models.SearchStationClientByTaxCodePost
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

class SearchLocalClientUseCase @Inject constructor(
    private val wolkeApiService: WolkeApiService,
) {

    suspend operator fun invoke(query: String, stationId: String): List<LocalClientData> = run {
        val queriedClients = mutableListOf<LocalClientData>()

        try {
            val clientsByEmail = searchByEmail(query, stationId)
            queriedClients.addAll(clientsByEmail)

            val clientsByName = searchByName(query, stationId)
            queriedClients.addAll(clientsByName)

            val clientsByTaxCode = searchByTaxCode(query, stationId)
            queriedClients.addAll(clientsByTaxCode)

        } catch (e: Exception) {
            Timber.e("Error searching client $query -> $e")
        }

        queriedClients.toSet().toList()
    }

    private suspend fun searchByEmail(query: String, stationId: String) = run {
        val request = SearchStationClientByEmailPost(email = query, stationId = stationId)
        val responseByEmail = wolkeApiService.searchStationClientByEmail(request)
        responseByEmail.data?.sortedBy { it.nombre } ?: emptyList()
    }

    private suspend fun searchByName(query: String, stationId: String) = run {
        val request = SearchStationClientByNamePost(name = query, stationId = stationId)
        val responseByEmail = wolkeApiService.searchStationClientByName(request)
        responseByEmail.data?.sortedBy { it.nombre } ?: emptyList()
    }

    private suspend fun searchByTaxCode(query: String, stationId: String) = run {
        val request =
            SearchStationClientByTaxCodePost(taxCode = query.uppercase(), stationId = stationId)
        val responseByEmail = wolkeApiService.searchStationClientByTaxCode(request)
        responseByEmail.data?.sortedBy { it.nombre } ?: emptyList()
    }
}
