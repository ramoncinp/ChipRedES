package com.binarium.chipredes.wolke2.domain.usecases

import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import com.binarium.chipredes.wolke.domain.usecases.GetStationMongoIdUseCase
import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.LocalClientData
import com.binarium.chipredes.wolke.models.SearchStationClientByIdPost
import java.lang.Exception
import javax.inject.Inject

class GetLocalClientUseCase @Inject constructor(
    private val getStationMongoIdUseCase: GetStationMongoIdUseCase,
    private val wolkeApiService: WolkeApiService,
    private val logger: Logger
) {

    suspend operator fun invoke(clientId: String): LocalClientData? {
        try {
            val foundClients = wolkeApiService.searchStationClientById(
                SearchStationClientByIdPost(clientId = clientId, stationId = getStationId())
            )

            return foundClients.data?.let { clientsList ->
                clientsList[0]
            }
        } catch (e: Exception) {
            logger.postMessage(
                message = "Error al obtener datos de cliente (GetLocalClientUseCase): ${e.message}",
                level = LogLevel.ERROR,
                client = clientId
            )
            e.printStackTrace()
        }

        return null
    }

    private fun getStationId() = getStationMongoIdUseCase()
}
