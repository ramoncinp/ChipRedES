package com.binarium.chipredes.wolke2.services

import com.squareup.moshi.Json

data class CancelBilletTransaction(
        @Json(name = "id_reservacion") val reservationId: String,
)