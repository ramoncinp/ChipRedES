package com.binarium.chipredes.wolke2.models

data class CancelBilletReservationResult(
        val message: String,
)