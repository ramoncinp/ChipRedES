package com.binarium.chipredes.wolke2.models

import android.text.Spannable
import android.text.SpannableString
import android.text.SpannedString
import android.text.style.StyleSpan
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.billets.data.model.getTotalAmount
import com.binarium.chipredes.emax.EmaxSaleAuthResult
import com.binarium.chipredes.local.services.StartBilletSalePost
import com.squareup.moshi.Json

data class ReserveTransactionResult(
    @Json(name = "_id") val id: MongoId? = null,
    val estatus: String? = null,
    val cantidad: Double? = null,
    @Json(name = "monto_usado") val montoUsado: Double? = null,
    val combustible: Long? = null,
    val estacion: MongoId? = null,
    @Json(name = "posicion_carga") val posicionCarga: Long? = null,
    @Json(name = "billetes_reservados") val billetesReservados: List<MongoId>? = null,
    @Json(name = "billetes_en_uso") val billetesEnUso: List<String>? = null,
    @Json(name = "folios_billetes") val foliosBilletes: List<String>? = null,
    @Json(name = "url_placas") val urlPlacas: String? = null,
    val placas: String? = null,
    @Json(name = "fecha_hora") val fechaHora: FechaHora? = null,
    val cuenta: MongoId? = null,
    val cliente: MongoId? = null,
    val message: String? = null,
)

data class FechaHora(
    val date: Long? = null,
)

fun ReserveTransactionResult.toStartBilletSalePost(
    emaxAuthResult: EmaxSaleAuthResult,
) = StartBilletSalePost(
    reservationId = id?.id.orEmpty(),
    clientAccountId = cuenta?.id.orEmpty(),
    clientId = cliente?.id.orEmpty(),
    loadingPosition = posicionCarga?.toInt() ?: -1,
    cantidad = cantidad ?: -1.0,
    autorizacion = emaxAuthResult.authNumber
)

fun ReserveTransactionResult.toResultText(): String {
    val billetsQuantity = "${billetesReservados?.size ?: 0} billete(s)"
    val reservedAmount = ChipREDConstants.MX_AMOUNT_FORMAT.format(cantidad ?: 0.0)

    val spannedBilletsQuantity = SpannableString(billetsQuantity).also {
        it.setSpan(
            StyleSpan(android.graphics.Typeface.BOLD),
            0,
            billetsQuantity.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }

    val spannedReservationAmount = SpannableString(reservedAmount).also {
        it.setSpan(
            StyleSpan(android.graphics.Typeface.BOLD),
            0,
            reservedAmount.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }

    return "Preset enviado: $spannedReservationAmount de $spannedBilletsQuantity"
}
