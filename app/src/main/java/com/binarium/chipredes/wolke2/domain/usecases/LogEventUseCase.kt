package com.binarium.chipredes.wolke2.domain.usecases

import com.binarium.chipredes.wolke.domain.usecases.GetStationMongoIdUseCase
import com.binarium.chipredes.wolke2.Wolke2ApiService
import com.binarium.chipredes.wolke2.models.MovilLogging
import timber.log.Timber

class LogEventUseCase(
    private val wolke2ApiService: Wolke2ApiService,
    private val mongoIdUseCase: GetStationMongoIdUseCase,
) {

    suspend operator fun invoke(
        client: String,
        message: String,
        level: String,
        balance: Double = -1.0,
    ) {

        try {
            val stationId = mongoIdUseCase.invoke().ifEmpty { "5f89d4b98c016b3e14421720" }
            val message = MovilLogging(
                estacion = stationId,
                cliente = client.ifEmpty { "5f8ae8e84b47a2d401866963" },
                nivel = level,
                mensaje = message,
                saldo = balance
            )

            val response = sendRequest(message)
            if (!response.isSuccessful) {
                //Retry if not successful
                sendRequest(message)
            }

        } catch (e: Exception) {
            Timber.e("Error al enviar mensaje de log")
        }
    }

    private suspend fun sendRequest(message: MovilLogging) = wolke2ApiService.postLogMessage(message)
}
