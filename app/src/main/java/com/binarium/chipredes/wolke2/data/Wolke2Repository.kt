package com.binarium.chipredes.wolke2.data

import com.binarium.chipredes.utils.DateUtils
import com.binarium.chipredes.wolke2.Wolke2ApiService
import com.binarium.chipredes.wolke2.services.CancelBilletTransaction
import com.binarium.chipredes.wolke2.services.ReserveBilletTransaction
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject

private const val TIPO_VENTA_BILLETE = "VB"
private const val TIPO_DEPOSITO_CUENTA = "DC"
private const val TRANSACTIONS_LIMIT = "1000"

class Wolke2Repository @Inject constructor(
    private val wolke2ApiService: Wolke2ApiService,
) {

    suspend fun getPurchasesByDate(
        clientId: String,
        account: String,
        startDate: Date,
        endDate: Date,
    ) = withContext(Dispatchers.IO) {

        val formattedStartDate = DateUtils.dateToChipredDateString(startDate, false)
        val formattedEndDate = DateUtils.dateToChipredDateString(endDate, false)

        wolke2ApiService.getClientAccountTransactions(
            clientId,
            account,
            TIPO_VENTA_BILLETE,
            startDate = formattedStartDate,
            endDate = formattedEndDate,
            cantidad = TRANSACTIONS_LIMIT
        )
    }

    suspend fun getPendingReservationsByDate(
        clientId: String,
        stationId: String,
        startDate: Date?,
        endDate: Date?
    ) = withContext(Dispatchers.IO) {
        val formattedStartDate = startDate?.let { DateUtils.dateToChipredDateString(startDate, true) }
        val formattedEndDate = endDate?.let { DateUtils.dateToChipredDateString(endDate, true) }

        wolke2ApiService.getClientPendingReservations(
            stationId, clientId, formattedStartDate, formattedEndDate
        )
    }

    suspend fun cancelBilletReservation(reservationId: String) = withContext(Dispatchers.IO) {
        wolke2ApiService.cancelBilletTransaction(
            CancelBilletTransaction(reservationId)
        )
    }

    suspend fun createBilletReservation(data: ReserveBilletTransaction) = withContext(Dispatchers.IO) {
        wolke2ApiService.createBilletTransaction(data)
    }
}
