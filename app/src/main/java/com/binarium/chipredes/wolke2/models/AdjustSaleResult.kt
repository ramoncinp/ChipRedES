package com.binarium.chipredes.wolke2.models

import com.binarium.chipredes.billets.data.model.AdjustablePurchase

sealed class AdjustSaleResult {
    class Error(val sale: AdjustablePurchase, val message: String) : AdjustSaleResult()
    class Success(val sale: AdjustablePurchase) : AdjustSaleResult()
}
