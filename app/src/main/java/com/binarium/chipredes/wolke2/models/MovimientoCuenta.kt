package com.binarium.chipredes.wolke2.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

data class MovimientoCuenta(
        @Json(name = "saldo_anterior") val saldoAnterior: Double,
        @Json(name = "tipo_movimiento") val tipoMovimiento: String,
        @Json(name = "saldo_nuevo") val saldoNuevo: Double,
        @Json(name = "fecha_hora") val fechaHora: String,
        @Json(name = "reservacion_saldo") val reservacionSaldo: ReservacionSaldo?,
        @Json(name = "folios_billetes") val foliosBilletes: List<String>,
        val cantidad: Double,
        val cuenta: Cuenta,
        val estatus: String?,
        val folio: String?,
        val estacion: Estacion,
        val despachador: Despachador?,
        val consumo: Consumo?,
        val cliente: Cliente
)

data class Estacion(
        @Json(name = "nombre_estacion") val nombreEstacion: String,
        @Json(name = "id_estacion") val idEstacion: String
)

data class Despachador(
        val iniciales: String,
        val nombre: String,
        val id: String
)

data class Cuenta(
        val saldo: Double,
        val tipo: String
)

data class Cliente(
        @Json(name = "combustible_billetes") val combustibleBilletes: Int,
        @Json(name = "fecha_alta") val fechaAlta: String,
        @Json(name = "clave_fiscal") val claveFiscal: String,
        val id: String,
        val email: String,
        val nombre: String,
        val estatus: String,
)

@Parcelize
data class ReservacionSaldo(
        @Json(name = "billetes_en_uso") val billetesEnUso: List<Billete>,
        @Json(name = "billetes_reservados") val billetesReservados: List<Billete>,
        @Json(name = "url_placas") val urlPlacas: String,
        @Json(name = "posicion_carga") val posicionCarga: Int,
        @Json(name = "monto_usado") val montoUsado: Double,
        @Json(name = "fecha_hora") val fechaHora: String,
        @Json(name = "folios_billetes") val foliosBilletes: List<String>,
        val placas: String,
        val cantidad: Double,
        val estatus: String,
        val combustible: Int,
        val id: String?
) : Parcelable

@Parcelize
data class Billete(
        @Json(name = "fecha_uso") val fechaUso: String?,
        @Json(name = "monto_uso") val montoUso: Double?,
        @Json(name = "fecha_cancelacion") val fechaCancelacion: String?,
        @Json(name = "fecha_alta") val fechaAlta: String,
        @Json(name = "id_dispositivo") val idDispositivo: String?,
        val estatus: String,
        val origen: String?,
        val combustible: Int,
        val monto: Double,
        val folio: String
) : Parcelable

data class Consumo(
        @Json(name = "num_ticket") val numTicket: String,
        @Json(name = "id_despachador_emax") val idDespachadorEmax: String,
        @Json(name = "tipo_cliente") val tipoCliente: String,
        @Json(name = "posicion_carga") val posicionCarga: String,
        @Json(name = "numero_dispensario") val numeroDispensario: String,
        @Json(name = "precio_unitario") val precioUnitario: Double,
        @Json(name = "fecha_hora") val fechaHora: String,
        @Json(name = "tipo_venta") val tipoVenta: String,
        @Json(name = "numero_estacion") val numeroEstacion: String,
        val cantidad: Double,
        val costo: Double,
        val producto: Producto
)

data class Producto(
        val id: String,
        val descripcion: String,
        val color: String
)