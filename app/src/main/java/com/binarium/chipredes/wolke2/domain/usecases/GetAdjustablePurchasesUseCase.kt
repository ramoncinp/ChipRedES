package com.binarium.chipredes.wolke2.domain.usecases

import com.binarium.chipredes.billets.data.model.AdjustablePurchase
import com.binarium.chipredes.billets.data.model.StationClientInfo
import com.binarium.chipredes.billets.data.model.WolkePurchasesRequest
import com.binarium.chipredes.billets.data.model.toAdjustablePurchase
import com.binarium.chipredes.wolke.domain.usecases.GetStationMongoIdUseCase
import com.binarium.chipredes.wolke.domain.usecases.GetStationNumberUseCase
import com.binarium.chipredes.wolke2.data.Wolke2Repository
import javax.inject.Inject

class GetAdjustablePurchasesUseCase @Inject constructor(
    private val getStationMongoIdUseCase: GetStationMongoIdUseCase,
    private val getStationNumberUseCase: GetStationNumberUseCase,
    private val wolke2Repository: Wolke2Repository,
) {

    suspend operator fun invoke(
        clientChipRedId: String,
        wolkePurchasesRequest: WolkePurchasesRequest,
    ): List<AdjustablePurchase> {
        val purchases = wolkePurchasesRequest.run {
            wolke2Repository.getPurchasesByDate(
                clientId,
                accountId,
                startDate,
                endDate
            )
        }

        val adjustablePurchases = mutableListOf<AdjustablePurchase>()
        purchases.forEach { movimientoCuenta ->
            movimientoCuenta.toAdjustablePurchase(getStationClientInfo(clientChipRedId))?.let {
                adjustablePurchases.add(it)
            }
        }

        return adjustablePurchases.toList().sortedByDescending { it.ticket.toInt() }
    }

    private fun getStationClientInfo(clientChipRedId: String) = StationClientInfo(
        clientChipRedId,
        getStationMongoIdUseCase(),
        getStationNumberUseCase()
    )
}
