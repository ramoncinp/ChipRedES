package com.binarium.chipredes.wolke2.models

data class MovilLogging(
    val estacion: String,
    val cliente: String,
    val nivel: String,
    val mensaje: String,
    val saldo: Double
)
