package com.binarium.chipredes.wolke2.models

import com.squareup.moshi.Json

data class AddToBalanceTransaction(
    @Json(name = "id_cuenta") val accountId: String,
    @Json(name = "cantidad") val amount: Double,
    @Json(name = "despachador") val pumper: Pumper,
    @Json(name = "fecha_hora") val dateTime: String,
)

data class Pumper(
    val id: String,
    @Json(name = "nombre") val name: String,
    @Json(name = "iniciales") val initials: String,
)

data class AddToBalanceResponse(
    val id: MongoId? = null,
    val cantidad: Double? = null,
    val saldoAnterior: Double? = null,
    val saldoNuevo: Double? = null,
    val tipoMovimiento: String? = null,
    val estacion: MongoId? = null,
    val cliente: MongoId? = null,
    val fechaHora: FechaHora? = null,
    val cuenta: MongoId? = null,
    val foliosBilletes: List<String>? = null,
    val estatus: String? = null,
    val folio: Long? = null,
    val despachador: Despachador? = null,
)
