package com.binarium.chipredes.wolke2.services

import com.squareup.moshi.Json

data class CreateStationAccount(
        @Json(name = "id_cliente_local") val clienteLocal: String,
        val estacion: String
)