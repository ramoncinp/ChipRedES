package com.binarium.chipredes.wolke2.domain.usecases

import com.binarium.chipredes.wolke.domain.usecases.GetStationMongoIdUseCase
import com.binarium.chipredes.wolke2.Wolke2ApiService
import com.binarium.chipredes.wolke2.models.ClientStationAccount
import javax.inject.Inject

class GetClientStationAccountUseCase @Inject constructor(
    private val getStationMongoIdUseCase: GetStationMongoIdUseCase,
    private val wolke2ApiService: Wolke2ApiService,
) {

    suspend operator fun invoke(clientId: String): ClientStationAccount? {
        val clientAccounts = wolke2ApiService.getStationAccounts(clientId)
        for (account in clientAccounts.data) {
            if (account.estacion.id == getStationId()) {
                return account
            }
        }
        return null
    }

    private fun getStationId() = getStationMongoIdUseCase()
}
