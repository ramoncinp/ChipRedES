package com.binarium.chipredes.taxreceipts;

import com.binarium.chipredes.ExtraProduct;
import com.binarium.chipredes.Purchase;

import java.util.ArrayList;

public class TaxReceipt extends Purchase
{
    @Override
    public double getAmount()
    {
        return super.getAmount();
    }

    @Override
    public void setAmount(double amount)
    {
        super.setAmount(amount);
    }

    @Override
    public double getVolume()
    {
        return super.getVolume();
    }

    @Override
    public void setVolume(double volume)
    {
        super.setVolume(volume);
    }

    @Override
    public String getTicketNumber()
    {
        return super.getTicketNumber();
    }

    @Override
    public void setTicketNumber(String ticketNumber)
    {
        super.setTicketNumber(ticketNumber);
    }

    @Override
    public String getProductDesc()
    {
        return super.getProductDesc();
    }

    @Override
    public void setProductDesc(String productDesc)
    {
        super.setProductDesc(productDesc);
    }

    @Override
    public String getProductColor()
    {
        return super.getProductColor();
    }

    @Override
    public void setProductColor(String productColor)
    {
        super.setProductColor(productColor);
    }

    @Override
    public String getDateHour()
    {
        return super.getDateHour();
    }

    @Override
    public void setDateHour(String dateHour)
    {
        super.setDateHour(dateHour);
    }

    @Override
    public String getUnitPrice()
    {
        return super.getUnitPrice();
    }

    @Override
    public void setUnitPrice(String unitPrice)
    {
        super.setUnitPrice(unitPrice);
    }

    @Override
    public String getPumper()
    {
        return super.getPumper();
    }

    @Override
    public void setPumper(String pumper)
    {
        super.setPumper(pumper);
    }

    @Override
    public ArrayList<ExtraProduct> getExtraProducts()
    {
        return super.getExtraProducts();
    }

    @Override
    public void setExtraProducts(ArrayList<ExtraProduct> extraProducts)
    {
        super.setExtraProducts(extraProducts);
    }
}
