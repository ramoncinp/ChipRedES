package com.binarium.chipredes.taxreceipts;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.binarium.chipredes.R;

public class TaxReceiptsActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTitle("Consultar facturas");
        setContentView(R.layout.activity_tax_receipts);
    }
}
