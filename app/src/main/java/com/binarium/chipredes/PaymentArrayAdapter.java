package com.binarium.chipredes;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PaymentArrayAdapter extends RecyclerView.Adapter<PaymentArrayAdapter
        .PaymentViewHolder> implements View.OnClickListener
{
    private ArrayList<Payment> payments;
    private Context context;
    private View.OnClickListener listener;

    public PaymentArrayAdapter(ArrayList<Payment> payments, Context context)
    {
        this.payments = payments;
        this.context = context;
    }

    @Override
    public int getItemCount()
    {
        return payments.size();
    }

    @Override
    public PaymentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.payment_layout,
                viewGroup, false);

        PaymentViewHolder pvh = new PaymentViewHolder(v, context, payments);
        v.setOnClickListener(this);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PaymentViewHolder paymentViewHolder, int i)
    {
        Payment payment = payments.get(i);

        String country = SharedPreferencesManager.getString(context, ChipREDConstants.COUNTRY,
                "mexico");
        String quantity;

        if (country.equals("costa rica"))
        {
            paymentViewHolder.currency.setImageResource(R.drawable.ic_colon_crc);
            quantity = ChipREDConstants.CR_AMOUNT_FORMAT.format(payment.getPaymentQuantity()) +
                    " CRC";
        }
        else
        {
            quantity = ChipREDConstants.MX_AMOUNT_FORMAT.format(payment.getPaymentQuantity()) +
                    " MXN";
            paymentViewHolder.currency.setImageResource(R.drawable.ic_payments);
        }

        paymentViewHolder.amount.setText(quantity);

        String dateHour = payment.getDateHour();
        int token = dateHour.indexOf(' ');

        paymentViewHolder.time.setText(dateHour.substring(token + 1));
        paymentViewHolder.date.setText(dateHour.substring(0, token));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null)
        {
            listener.onClick(view);
        }
    }

    public static class PaymentViewHolder extends RecyclerView.ViewHolder
    {
        private TextView amount;
        private TextView date;
        private TextView time;
        private ImageView currency;

        public PaymentViewHolder(View itemView, Context context, ArrayList<Payment> payments)
        {
            super(itemView);

            amount = (TextView) itemView.findViewById(R.id.payment_amount);
            date = (TextView) itemView.findViewById(R.id.payment_date);
            time = (TextView) itemView.findViewById(R.id.payment_time);
            currency = itemView.findViewById(R.id.payment_currency_icon);
        }
    }
}

