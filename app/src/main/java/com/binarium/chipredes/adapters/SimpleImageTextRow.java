package com.binarium.chipredes.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipredes.AddressElementCR;
import com.binarium.chipredes.R;

import java.util.ArrayList;

public class SimpleImageTextRow extends RecyclerView.Adapter<SimpleImageTextRow
        .ImageTextViewHolder> implements View.OnClickListener
{
    private ArrayList<AddressElementCR> values;
    private View.OnClickListener listener;

    public SimpleImageTextRow(ArrayList<AddressElementCR>
            values)
    {
        this.values = values;
    }

    @Override
    public int getItemCount()
    {
        return values.size();
    }

    @Override
    public ImageTextViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_image_text_row,
                parent, false);

        ImageTextViewHolder twoRowViewHolder = new ImageTextViewHolder(v);
        v.setOnClickListener(this);

        return twoRowViewHolder;
    }

    @Override
    public void onBindViewHolder(ImageTextViewHolder holder, int position)
    {
        holder.text.setText(values.get(position).toString());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onClick(v);
        }
    }

    static class ImageTextViewHolder extends RecyclerView.ViewHolder
    {
        private TextView text;

        ImageTextViewHolder(View itemView)
        {
            super(itemView);
            text = itemView.findViewById(R.id.text_tv);
        }
    }
}
