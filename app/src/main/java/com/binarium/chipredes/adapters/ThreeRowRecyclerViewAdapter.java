package com.binarium.chipredes.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipredes.R;

import java.util.ArrayList;

public class ThreeRowRecyclerViewAdapter extends RecyclerView.Adapter<ThreeRowRecyclerViewAdapter
        .ThreeRowViewHolder> implements View.OnClickListener
{
    private Context context;
    private ArrayList<String> columna1, columna2, columna3;
    private View.OnClickListener listener;

    private float textSize = 0;

    public ThreeRowRecyclerViewAdapter(Context context, ArrayList<String> columna1,
                                       ArrayList<String> columna2, ArrayList<String> columna3)
    {
        this.context = context;
        this.columna1 = columna1;
        this.columna2 = columna2;
        this.columna3 = columna3;
    }

    public ThreeRowRecyclerViewAdapter(Context context, ArrayList<String> columna1,
                                       ArrayList<String> columna2, ArrayList<String> columna3,
                                       float textSize)
    {
        this.context = context;
        this.columna1 = columna1;
        this.columna2 = columna2;
        this.columna3 = columna3;
        this.textSize = textSize;
    }

    @Override
    public int getItemCount()
    {
        return columna1.size();
    }

    @Override
    public ThreeRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.three_column_row,
                parent, false);

        ThreeRowViewHolder threeRowViewHolder = new ThreeRowViewHolder(v);
        v.setOnClickListener(this);

        return threeRowViewHolder;
    }

    @Override
    public void onBindViewHolder(ThreeRowViewHolder holder, int position)
    {
        holder.tvColumna1.setText(columna1.get(position));
        holder.tvColumna2.setText(columna2.get(position));
        holder.tvColumna3.setText(columna3.get(position));

        if (textSize != 0)
        {
            holder.tvColumna1.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
            holder.tvColumna2.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
            holder.tvColumna3.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onClick(v);
        }
    }

    static class ThreeRowViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvColumna1, tvColumna2, tvColumna3;

        ThreeRowViewHolder(View itemView)
        {
            super(itemView);

            tvColumna1 = itemView.findViewById(R.id.columna1_fila_tres_columnas);
            tvColumna2 = itemView.findViewById(R.id.columna2_fila_tres_columnas);
            tvColumna3 = itemView.findViewById(R.id.columna3_fila_tres_columnas);
        }
    }
}
