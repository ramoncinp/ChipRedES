package com.binarium.chipredes.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.binarium.chipredes.LoadingPosition;
import com.binarium.chipredes.R;

import java.util.ArrayList;

public class LoadingPositionAdapter extends RecyclerView.Adapter<LoadingPositionAdapter.ViewHolder> implements View.OnClickListener,
        View.OnLongClickListener
{
    public static final int ACCENT_THEME = 0;
    public static final int WHITE_THEME = 1;

    private Context c;
    private ArrayList<LoadingPosition> loadingPositions;
    private int theme;

    private View.OnClickListener clickListener;
    private View.OnLongClickListener longClickListener;

    public LoadingPositionAdapter(Context c, ArrayList<LoadingPosition> loadingPositions, int theme)
    {
        this.c = c;
        this.loadingPositions = loadingPositions;
        this.theme = theme;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = null;

        if (theme == ACCENT_THEME)
        {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.loading_position_model,
                    viewGroup, false);
        }
        else if (theme == WHITE_THEME)
        {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.loading_position_model_white,
                    viewGroup, false);
        }

        if (view != null)
        {
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        LoadingPosition loadingPosition = loadingPositions.get(position);

        int number = loadingPosition.getUserStationSide() == 0 ?
                loadingPosition.getStationSide() :
                loadingPosition.getUserStationSide();

        holder.number.setText(String.valueOf(number));

        if (theme == WHITE_THEME)
        {
            //El adaptador se utiliza para seleccionar impresoras
            if (loadingPosition.isSelected())
            {
                holder.dispenserImage.setImageResource(R.drawable.dispensario_blanco);
                holder.number.setTextColor(c.getResources().getColor(R.color.white));
                ((CardView) holder.itemView).setCardBackgroundColor(c.getResources().getColor(R.color.green_money));
            }
            else
            {
                holder.dispenserImage.setImageResource(R.drawable.dispensario_negro);
                holder.number.setTextColor(c.getResources().getColor(R.color.black));
                ((CardView) holder.itemView).setCardBackgroundColor(c.getResources().getColor(R.color.white));
            }
        }
        else
        {
            //El adaptador se utiliza para las posiciones de carga principales
            if (loadingPosition.getState() == 0)
            {
                //Posicion de carga libre
                ((CardView) holder.itemView).setCardBackgroundColor(c.getResources().getColor(R.color.colorAccent));
            }
            else
            {
                //Posicion de carga ocupada
                ((CardView) holder.itemView).setCardBackgroundColor(c.getResources().getColor(R.color.colorPrimaryDark));
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return loadingPositions.size();
    }

    public ArrayList<LoadingPosition> getLoadingPositions()
    {
        return loadingPositions;
    }

    public LoadingPosition getItem(int idx)
    {
        return loadingPositions.get(idx);
    }

    public void setClickListener(View.OnClickListener clickListener)
    {
        this.clickListener = clickListener;
    }

    public void setLongClickListener(View.OnLongClickListener longClickListener)
    {
        this.longClickListener = longClickListener;
    }

    @Override
    public void onClick(View v)
    {
        if (clickListener != null)
        {
            clickListener.onClick(v);
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if (longClickListener != null)
        {
            longClickListener.onLongClick(view);
        }
        return true;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView number;
        ImageView dispenserImage;

        ViewHolder(View itemView)
        {
            super(itemView);
            number = itemView.findViewById(R.id.station_side_tv);
            dispenserImage = itemView.findViewById(R.id.dispenser_img);
        }
    }
}
