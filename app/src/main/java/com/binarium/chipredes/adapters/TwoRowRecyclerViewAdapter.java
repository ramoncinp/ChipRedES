package com.binarium.chipredes.adapters;

import android.content.Context;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binarium.chipredes.R;

import java.util.ArrayList;

public class TwoRowRecyclerViewAdapter extends RecyclerView.Adapter<TwoRowRecyclerViewAdapter
        .TwoRowViewHolder> implements View.OnClickListener
{
    private Context context;
    private ArrayList<String> keys, values;
    private View.OnClickListener listener;

    private float textSize = 0;
    private boolean boldKeys = true;

    public TwoRowRecyclerViewAdapter(Context context, ArrayList<String> keys, ArrayList<String>
            values, float textSize, boolean boldKeys)
    {
        this.context = context;
        this.keys = keys;
        this.values = values;
        this.textSize = textSize;
        this.boldKeys = boldKeys;
    }

    public TwoRowRecyclerViewAdapter(Context context, ArrayList<String> keys, ArrayList<String>
            values)
    {
        this.context = context;
        this.keys = keys;
        this.values = values;
    }

    @Override
    public int getItemCount()
    {
        return keys.size();
    }

    @Override
    public TwoRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.two_column_row,
                parent, false);

        TwoRowViewHolder twoRowViewHolder = new TwoRowViewHolder(v);
        v.setOnClickListener(this);

        return twoRowViewHolder;
    }

    @Override
    public void onBindViewHolder(TwoRowViewHolder holder, int position)
    {
        holder.keysTv.setText(keys.get(position));
        holder.valuesTv.setText(values.get(position));

        if (textSize != 0)
        {
            holder.keysTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
            holder.valuesTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        }

        if (!boldKeys)
        {
            holder.keysTv.setTypeface(Typeface.DEFAULT);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v)
    {
        if (listener != null)
        {
            listener.onClick(v);
        }
    }

    static class TwoRowViewHolder extends RecyclerView.ViewHolder
    {
        private TextView keysTv;
        private TextView valuesTv;

        TwoRowViewHolder(View itemView)
        {
            super(itemView);

            keysTv = itemView.findViewById(R.id.row_two_column_key);
            valuesTv = itemView.findViewById(R.id.row_two_column_value);

        }
    }
}
