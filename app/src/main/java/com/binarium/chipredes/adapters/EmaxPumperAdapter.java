package com.binarium.chipredes.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binarium.chipredes.R;
import com.binarium.chipredes.emax.model.EmaxPumper;

import java.util.ArrayList;

public class EmaxPumperAdapter extends RecyclerView.Adapter<EmaxPumperAdapter
        .EmaxPumperViewHolder> implements View.OnClickListener {
    private final ArrayList<EmaxPumper> values;
    private View.OnClickListener listener;

    public EmaxPumperAdapter(ArrayList<EmaxPumper> values) {
        this.values = values;
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    @NonNull
    @Override
    public EmaxPumperViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_emax_pumper,
                parent, false);

        EmaxPumperViewHolder twoRowViewHolder = new EmaxPumperViewHolder(v);
        v.setOnClickListener(this);

        return twoRowViewHolder;
    }

    @Override
    public void onBindViewHolder(EmaxPumperViewHolder holder, int position) {
        holder.name.setText(values.get(position).getNombre());
        holder.initials.setText(values.get(position).getIniciales());
        holder.id.setText(values.get(position).getId());
        holder.tag.setText(values.get(position).getTag());
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    static class EmaxPumperViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final TextView initials;
        private final TextView id;
        private final TextView tag;

        EmaxPumperViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name_tv);
            initials = itemView.findViewById(R.id.initials_tv);
            id = itemView.findViewById(R.id.id_tv);
            tag = itemView.findViewById(R.id.tag_tv);
        }
    }
}
