package com.binarium.chipredes.config.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.binarium.chipredes.R
import com.binarium.chipredes.config.data.model.ConfigurableServer
import com.binarium.chipredes.config.data.model.ServerAddressType
import com.binarium.chipredes.config.data.model.ServerTestState
import com.binarium.chipredes.config.utils.getServerAddressToTest
import com.binarium.chipredes.config.utils.setAddress
import com.binarium.chipredes.config.utils.setAddressTypeVisibility
import com.binarium.chipredes.databinding.ConfigServerItemBinding
import com.github.leandroborgesferreira.loadingbutton.customViews.CircularProgressButton

class ConfigurableServerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null,
) : ConstraintLayout(context, attrs) {

    private val binding = ConfigServerItemBinding.inflate(LayoutInflater.from(context), this, true)
    var onTest: (ConfigurableServer) -> Unit = {}

    fun bind(server: ConfigurableServer) {
        with(binding) {
            serverTitleTv.text = server.name.name.replace("_", " ")
            setAddress(server)
            setAddressTypeVisibility(server)
            setAddressInputVisibility(server.addressType, this)
            serverAddressType.setOnCheckedChangeListener { _, radioId ->
                val newAddressType = if (radioId == ipSelectionRb.id) {
                    ServerAddressType.IP
                } else {
                    ServerAddressType.DNS
                }
                server.addressType = newAddressType
                setAddressInputVisibility(newAddressType, this)
            }
            testButton.setOnClickListener {
                testButton.revertAnimation()
                testButton.startAnimation()
                onTest.invoke(getServerToTest(server))
            }
        }
    }

    private fun setAddressInputVisibility(
        addressType: ServerAddressType,
        binding: ConfigServerItemBinding,
    ) {
        with(binding) {
            if (addressType == ServerAddressType.IP) {
                serverAddressType.check(ipSelectionRb.id)
                ipInputGroup.visibility = View.VISIBLE
                dnsInputGroup.visibility = View.GONE
            } else {
                serverAddressType.check(dnsSelectionRb.id)
                ipInputGroup.visibility = View.GONE
                dnsInputGroup.visibility = View.VISIBLE
            }
        }
    }

    private fun getServerToTest(item: ConfigurableServer): ConfigurableServer {
        val address = binding.getServerAddressToTest(item)
        return item.copy(address = address)
    }

    fun bindTestButton(state: ServerTestState) {
        val testButton = binding.testButton
        when (state) {
            ServerTestState.SUCCESS -> testButton.showSuccess()
            ServerTestState.ERROR -> testButton.showError()
            ServerTestState.IDLE -> testButton.revertAnimation()
        }
        testButton.isClickable = true
    }

    private fun CircularProgressButton.showSuccess() {
        val colorResource: Int = ContextCompat.getColor(context, R.color.green_money)
        val drawable = ContextCompat.getDrawable(context, R.drawable.ic_check)
        doneLoadingAnimation(colorResource, drawable?.toBitmap()!!)
    }

    private fun CircularProgressButton.showError() {
        val colorResource: Int = ContextCompat.getColor(context, R.color.colorPrimary)
        val drawable = ContextCompat.getDrawable(context, R.drawable.ic_clear)
        doneLoadingAnimation(colorResource, drawable?.toBitmap()!!)
    }

    fun onDestroy() {
        binding.testButton.dispose()
    }
}
