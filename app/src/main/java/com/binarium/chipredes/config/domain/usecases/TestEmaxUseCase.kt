package com.binarium.chipredes.config.domain.usecases

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.config.data.model.ConfigurableServer
import com.binarium.chipredes.config.data.model.ServerName
import com.binarium.chipredes.config.data.model.TestServerResponse
import com.binarium.chipredes.config.utils.isAddressValid
import com.binarium.chipredes.emax.EmaxApiService
import com.binarium.chipredes.logger.Logger
import com.binarium.chipredes.logger.model.LogLevel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class TestEmaxUseCase @Inject constructor(
    @ApplicationContext private val context: Context,
    private val sharedPreferences: SharedPreferences,
    private val logger: Logger
) {
    private var emaxUrl = ""

    suspend operator fun invoke(server: ConfigurableServer): TestServerResponse {
        if (server.isAddressValid().not()) return TestServerResponse.Error(server)
        formatUrl(server)
        setAddress(server)
        val emaxApiService = createEmaxService()
        return try {
            emaxApiService.testConnection(emaxUrl)
            TestServerResponse.Success(server)
        } catch (e: Exception) {
            logger.postMessage(
                "TestEmax: ${e.message} - $emaxUrl",
                LogLevel.ERROR
            )
            TestServerResponse.Error(server)
        }
    }

    private fun setAddress(server: ConfigurableServer) {
        sharedPreferences.edit {
            putString(getPersistenceKey(server), emaxUrl)
        }
    }

    private fun createEmaxService() = EmaxApiService(context, logger)

    private fun getPersistenceKey(server: ConfigurableServer) =
        if (server.name == ServerName.EMAX) {
            ChipREDConstants.EMAX_URL
        } else {
            ChipREDConstants.EMAX_CLIENT_URL
        }

    private fun formatUrl(server: ConfigurableServer) {
        emaxUrl = server.address.replace("http://", "")
    }
}
