package com.binarium.chipredes.config.domain.usecases

import android.content.SharedPreferences
import androidx.core.content.edit
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.config.data.model.ConfigurableServer
import com.binarium.chipredes.config.data.model.TestServerResponse
import com.binarium.chipredes.config.utils.isAddressValid
import com.binarium.chipredes.local.LocalApiService
import com.binarium.chipredes.wolke.models.TestConnectionPost
import javax.inject.Inject

class TestLocalUseCase @Inject constructor(
    private val localApiService: LocalApiService,
    private val sharedPreferences: SharedPreferences
) {

    suspend operator fun invoke (server: ConfigurableServer): TestServerResponse {
        if (server.isAddressValid().not()) return TestServerResponse.Error(server)
        return try {
            setAddress(server.address)
            localApiService.testConnection(TestConnectionPost())
            TestServerResponse.Success(server)
        } catch (e: Exception) {
            TestServerResponse.Error(server)
        }
    }

    private fun setAddress(addressToTest: String) {
        val localUrl = "$addressToTest/webservice/tablet/"
        sharedPreferences.edit {
            putString(ChipREDConstants.CR_LOCAL_IP, localUrl)
        }
    }
}
