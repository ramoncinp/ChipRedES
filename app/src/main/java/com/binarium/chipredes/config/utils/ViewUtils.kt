package com.binarium.chipredes.config.utils

import android.view.View
import com.binarium.chipredes.config.data.model.ConfigurableServer
import com.binarium.chipredes.config.data.model.ServerAddressType
import com.binarium.chipredes.config.data.model.ServerName
import com.binarium.chipredes.databinding.ConfigServerItemBinding
import java.util.regex.Matcher
import java.util.regex.Pattern

fun ConfigurableServer.isAddressValid() = address.isNotEmpty() && address != "http://"

fun ConfigServerItemBinding.getServerAddressToTest(server: ConfigurableServer): String {
    val url = if (server.addressType == ServerAddressType.DNS) {
        getDnsServerToTest()
    } else {
        getIpServerToTest()
    }

    return "http://$url"
}

fun ConfigServerItemBinding.setAddress(server: ConfigurableServer) {
    val address = if (server.name == ServerName.EMAX || server.name == ServerName.EMAX_CLIENTE) {
        server.address
    } else {
        server.address.getContentFromUrl()
    }
    if (address.isIP()) {
        setIPServerAddress(address)
    } else {
        dnsEt.setText(address)
    }
}

fun ConfigServerItemBinding.setAddressTypeVisibility(server: ConfigurableServer) {
    serverAddressType.visibility = if (server.name == ServerName.EMAX || server.name == ServerName.EMAX_CLIENTE) {
        View.GONE
    } else {
        View.VISIBLE
    }
}

private fun ConfigServerItemBinding.setIPServerAddress(ipAddress: String) {
    val editTexts = arrayOf(ipEtSeg1, ipEtSeg2, ipEtSeg3, ipEtSeg4, ipEtSeg5)
    var idx = 0
    var ip = ipAddress

    while (ip.isNotEmpty()) {
        val pointIdx = ip.indexOf(".")
        ip = if (pointIdx == -1) {
            val portSeparator = ip.indexOf(":")
            if (portSeparator == -1) {
                editTexts[idx].setText(ip)
                ""
            } else {
                val lastSegment = ip.substring(0, portSeparator)
                val port = ip.substring(portSeparator + 1)
                editTexts[idx++].setText(lastSegment)
                if (idx < editTexts.size) editTexts[idx].setText(port)
                ""
            }
        } else {
            val segment = ip.substring(0, pointIdx)
            editTexts[idx++].setText(segment)
            ip.substring(pointIdx + 1)
        }
    }
}

private fun ConfigServerItemBinding.getDnsServerToTest(): String {
    return dnsEt.text.toString()
}

private fun ConfigServerItemBinding.getIpServerToTest(): String {
    val ipSeg1 = ipEtSeg1.text.toString()
    val ipSeg2 = ipEtSeg2.text.toString()
    val ipSeg3 = ipEtSeg3.text.toString()
    val ipSeg4 = ipEtSeg4.text.toString()
    val ipPortSeg = ipEtSeg5.text.toString()

    return "$ipSeg1.$ipSeg2.$ipSeg3.$ipSeg4:$ipPortSeg"
}

fun String.serverAddressToType() = if (isIP()) ServerAddressType.IP else ServerAddressType.DNS

private fun String.isIP(): Boolean {
    val regex = ".*(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}):(\\d{1,5}).*"
    val pattern: Pattern = Pattern.compile(regex)
    val matcher: Matcher = pattern.matcher(this)
    return matcher.matches()
}

private fun countMatches(text: String, match: Char): Int {
    var counter = 0
    for (element in text) {
        if (element == match) {
            counter++
        }
    }
    return counter
}

private fun String.getContentFromUrl(): String {
    if (isEmpty()) return ""
    //Buscar index del primer ":" y sumarle 3 para inicio de direccion, este es el primer limite
    val idx1 = indexOf(":") + 3

    //Buscar index del limite final de la direccion...que sería: "/"
    var idx2 = idx1
    var mChar = 0.toChar()
    while (mChar != '/') {
        idx2++
        mChar = get(idx2)
    }
    return substring(idx1, idx2)
}
