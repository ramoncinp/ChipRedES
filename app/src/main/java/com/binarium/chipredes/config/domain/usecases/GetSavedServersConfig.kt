package com.binarium.chipredes.config.domain.usecases

import android.content.SharedPreferences
import com.binarium.chipredes.ChipREDConstants.*
import com.binarium.chipredes.config.data.model.ConfigurableServer
import com.binarium.chipredes.config.data.model.ServerAddressType
import com.binarium.chipredes.config.data.model.ServerName
import com.binarium.chipredes.config.utils.serverAddressToType
import javax.inject.Inject

class GetSavedServersConfig @Inject constructor(
    private val sharedPreferences: SharedPreferences,
) {

    operator fun invoke(): List<ConfigurableServer> {
        val servers = mutableListOf<ConfigurableServer>()
        servers.add(getLocalServerData())
        servers.add(getWolkeServerData())
        servers.add(getSocketServerData())
        servers.add(getEmaxServerData())
        servers.add(getEmaxClientServerData())
        return servers
    }

    private fun getLocalServerData(): ConfigurableServer {
        val address = sharedPreferences.getString(CR_LOCAL_IP, "").toString()
        return createServer(ServerName.LOCAL, address)
    }

    private fun getWolkeServerData(): ConfigurableServer {
        val address = sharedPreferences.getString(CR_WOLKE_IP, "").toString()
        return createServer(ServerName.WOLKE, address)
    }

    private fun getSocketServerData(): ConfigurableServer {
        val address = sharedPreferences.getString(CR_WEB_SOCKET_IP, "").toString()
        return createServer(ServerName.SOCKET, address)
    }

    private fun getEmaxServerData(): ConfigurableServer {
        val address = sharedPreferences.getString(EMAX_URL, "").toString()
        val serverData = createServer(ServerName.EMAX, address)
        return serverData.copy(addressType = ServerAddressType.DNS)
    }

    private fun getEmaxClientServerData(): ConfigurableServer {
        val address = sharedPreferences.getString(EMAX_CLIENT_URL, "").toString()
        val serverData = createServer(ServerName.EMAX_CLIENTE, address)
        return serverData.copy(addressType = ServerAddressType.DNS)
    }

    private fun createServer(name: ServerName, address: String) = ConfigurableServer(
        name,
        address.serverAddressToType(),
        address
    )
}
