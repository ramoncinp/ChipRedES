package com.binarium.chipredes.config;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.binarium.chipredes.Integration;
import com.binarium.chipredes.R;

import java.util.ArrayList;

public class IntegrationsConfigAdapter extends RecyclerView.Adapter<IntegrationsConfigAdapter
        .IntegrationsViewHolder> implements View.OnClickListener
{
    private ArrayList<Integration> integrations;
    private Context context;
    private View.OnClickListener listener;

    public IntegrationsConfigAdapter(ArrayList<Integration> integrations, Context context)
    {
        this.integrations = integrations;
        this.context = context;
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public IntegrationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.integration_config_element,
                parent, false);

        IntegrationsViewHolder wifiDeviceViewHolder = new IntegrationsViewHolder(v);
        v.setOnClickListener(this);

        return wifiDeviceViewHolder;
    }

    @Override
    public void onBindViewHolder(IntegrationsViewHolder holder, int position)
    {
        final Integration integration = integrations.get(position);

        holder.name.setText(integration.getName());
        holder.stateSwitch.setChecked(integration.isEnabled());
        holder.stateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                integration.setEnabled(isChecked);
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount()
    {
        return integrations.size();
    }

    @Override
    public void onClick(View view)
    {
        if (listener != null) listener.onClick(view);
    }

    public static class IntegrationsViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name;
        private Switch stateSwitch;

        public IntegrationsViewHolder(View itemView)
        {
            super(itemView);

            name = itemView.findViewById(R.id.integration_desc);
            stateSwitch = itemView.findViewById(R.id.integration_state);
        }
    }
}
