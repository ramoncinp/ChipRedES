package com.binarium.chipredes.config;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.R;

class LocalModeManager
{
    // Variables
    private boolean active = false;

    // Objetos
    private Activity activity;
    private LocalModeDialogInterface mInterface;

    // Views
    private CardView powerOnButton;
    private Dialog dialog;
    private LinearLayout contentLayout;
    private ProgressBar progressBar;
    private TextView powerOnText;

    LocalModeManager(Activity activity)
    {
        this.activity = activity;
    }

    void setmInterface(LocalModeDialogInterface localModeDialogInterface)
    {
        mInterface = localModeDialogInterface;
    }

    void showDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        View content = activity.getLayoutInflater().inflate(R.layout.dialog_contingencia_manual,
                null);
        contentLayout = content.findViewById(R.id.content_layout);
        powerOnButton = content.findViewById(R.id.power_on_button);
        powerOnText = content.findViewById(R.id.power_on_state);
        progressBar = content.findViewById(R.id.progress_bar);
        powerOnButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Mostrar progressBar
                contentLayout.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);

                // Cambiar estado
                mInterface.onStateChanged(!active);
            }
        });

        // Definir view
        builder.setView(content);
        builder.setNegativeButton("Regresar", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    void setState(int state)
    {
        if (state == ChipRedManager.NETWORK_OK || state == ChipRedManager.WOLKE_NO_OK)
        {
            active = false;
        }
        else
        {
            active = true;
        }

        // Actualizar views
        powerOnButton.setCardBackgroundColor(ContextCompat.getColor(activity, active
                ? R.color.green_ok : R.color.red));
        powerOnText.setTextColor(ContextCompat.getColor(activity, active
                ? R.color.green_ok : R.color.red));
        powerOnText.setText(active ? "Activado" : "Desactivado");

        // Mostrar views
        progressBar.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
    }

    void showViews()
    {
        progressBar.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
    }

    public interface LocalModeDialogInterface
    {
        void onStateChanged(boolean state);
    }
}
