package com.binarium.chipredes.config.domain

import android.content.SharedPreferences
import com.binarium.chipredes.ChipREDConstants
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import kotlinx.coroutines.tasks.await
import timber.log.Timber
import javax.inject.Inject

private const val MINIMUM_FETCH_INTERVAL = 30L

class FetchRemoteConfigValues @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val remoteConfig: FirebaseRemoteConfig,
) {

    suspend operator fun invoke() {
        try {
            if (shouldFetchRemoteConfig().not()) {
                Timber.v("Not fetching remote config values")
                return
            }
            initConfig()
            remoteConfig.fetchAndActivate().await()
        } catch (e: Exception) {
            e.printStackTrace()
            Timber.e("Error fetching firebase remote config")
        }
    }

    private fun initConfig() {
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(MINIMUM_FETCH_INTERVAL)
            .build()

        remoteConfig.setConfigSettingsAsync(configSettings)
    }

    private fun shouldFetchRemoteConfig(): Boolean {
        val country = sharedPreferences.getString(ChipREDConstants.COUNTRY, "")
        return country == "costa rica"
    }
}
