package com.binarium.chipredes.config.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.binarium.chipredes.config.data.model.ConfigurableServer
import com.binarium.chipredes.config.data.model.ServerName
import com.binarium.chipredes.config.data.model.TestServerResponse
import com.binarium.chipredes.config.domain.toState
import com.binarium.chipredes.databinding.FragmentConfigureServersBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ConfigureServersFragment : Fragment() {

    private var _binding: FragmentConfigureServersBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ConfigureServersViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentConfigureServersBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
        viewModel.getServersConfig()
    }

    private fun initViews() {
        binding.emaxClientSwitch.setOnCheckedChangeListener { _, value ->
            binding.emaxClieTestView.visibility = if (value) View.VISIBLE else View.GONE
            viewModel.setEmaxClientConfig(value)
        }
        binding.emaxClientSwitch.isChecked = viewModel.getEmaxClientConfig()
    }

    private fun initObservers() {
        viewModel.serverResponse.observe(viewLifecycleOwner) {
            onServerResponse(it)
        }

        viewModel.onServersReady.observe(viewLifecycleOwner) {
            if (it) setServersData()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        with(binding) {
            localTestView.onDestroy()
            wolkeTestView.onDestroy()
            socketTestView.onDestroy()
            emaxTestView.onDestroy()
            emaxClieTestView.onDestroy()
        }
    }

    private fun setServersData() {
        with(binding) {
            localTestView.bindServer(viewModel.localServer)
            wolkeTestView.bindServer(viewModel.wolkeServer)
            socketTestView.bindServer(viewModel.socketServer)
            emaxTestView.bindServer(viewModel.emaxServer)
            emaxClieTestView.bindServer(viewModel.emaxClientServer)
        }
    }

    private fun ConfigurableServerView.bindServer(server: ConfigurableServer) {
        bind(server)
        onTest = { viewModel.testServer(it) }
    }

    private fun onServerResponse(serverResponse: TestServerResponse) {
        val serverView = getServerView(serverResponse.server)
        serverView.bindTestButton(serverResponse.toState())
        viewModel.updateServerWithResponse(serverResponse)
    }

    private fun getServerView(server: ConfigurableServer): ConfigurableServerView {
        return when (server.name) {
            ServerName.LOCAL -> binding.localTestView
            ServerName.WOLKE -> binding.wolkeTestView
            ServerName.SOCKET -> binding.socketTestView
            ServerName.EMAX -> binding.emaxTestView
            ServerName.EMAX_CLIENTE -> binding.emaxClieTestView
        }
    }

    fun isContinuable() = viewModel.isContinuable()
}
