package com.binarium.chipredes.config.data.model

sealed class TestServerResponse (val server: ConfigurableServer) {
    class Success(server: ConfigurableServer): TestServerResponse(server)
    class Error(server: ConfigurableServer): TestServerResponse(server)
}
