package com.binarium.chipredes.config.domain.usecases

import android.content.SharedPreferences
import com.binarium.chipredes.ChipREDConstants.EMAX_CLIENT_ENABLED
import javax.inject.Inject

class GetEmaxClientPrefUseCase @Inject constructor(
    private val sharedPreferences: SharedPreferences,
) {

    operator fun invoke(): Boolean {
        return sharedPreferences.getBoolean(EMAX_CLIENT_ENABLED, false)
    }
}
