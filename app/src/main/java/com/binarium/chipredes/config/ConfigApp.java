package com.binarium.chipredes.config;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.config.views.ConfigureServersFragment;
import com.binarium.chipredes.main.MainActivity;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ConfigApp extends AppCompatActivity
{
    private boolean completeProcess = true;
    private int fragmentIdx = 0;
    private int singleAction = 0;

    private NewPinFragment newPinFragment;
    private ConfigureServersFragment configureServersFragment;
    private GetStationReference getStationReference;

    public TextView continueButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_app);
        hideToolBar();

        continueButton = findViewById(R.id.continue_button);
        continueButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                switch (fragmentIdx)
                {
                    case 0:
                        setSecondFragment();
                        fragmentIdx++;
                        break;

                    case 1:
                        if (newPinFragment != null)
                        {
                            String nip = newPinFragment.getNip();
                            if (nip == null)
                            {
                                Toast.makeText(ConfigApp.this,
                                        "Ingrese un NIP válido", Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                if (!newPinFragment.isConfirmation())
                                {
                                    newPinFragment.setFirstNipIntent(newPinFragment.getNip());
                                    newPinFragment.setToConfirm();
                                }
                                else
                                {
                                    if (newPinFragment.getNip().equals(newPinFragment.getFirstNipIntent()))
                                    {
                                        SharedPreferencesManager.putString(ConfigApp.this,
                                                ChipREDConstants.APP_PIN, nip);

                                        if (!completeProcess)
                                        {
                                            Intent intent = new Intent();
                                            setResult(SettingsActivity.REPALCE_NIP, intent);
                                            finish();
                                        }
                                        else
                                        {
                                            setConfigServersFragment();
                                            fragmentIdx++;
                                        }

                                        Toast.makeText(ConfigApp.this,
                                                "NIP guardado",
                                                Toast.LENGTH_LONG).show();
                                    }
                                    else
                                    {
                                        hideKeyboard();
                                        Toast.makeText(ConfigApp.this,
                                                "No coincide confirmación",
                                                Toast.LENGTH_LONG).show();

                                        setSecondFragment();
                                    }
                                }
                            }
                        }
                        break;

                    case 2:
                        if (configureServersFragment != null)
                        {
                            /*if (testConnectionFragment.isContinuable())
                            {
                                if (singleAction == 2)
                                {
                                    Intent intent = new Intent();
                                    setResult(SettingsActivity.REPALCE_SERVER_ADDR, intent);
                                    finish();
                                }
                                else
                                {
                                    Intent intent = new Intent(ConfigApp.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                                    .FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(android.R.anim.fade_in, android.R
                                    .anim.fade_out);
                                }
                            }
                            else
                            {
                                hideKeyboard();
                                Toast.makeText(ConfigApp.this,
                                        "Pruebe todas las conexiónes para continuar",
                                        Toast.LENGTH_LONG).show();
                            }*/
                            final Runnable action;
                            if (!completeProcess)
                            {
                                action = new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        Intent intent = new Intent();
                                        setResult(SettingsActivity.REPALCE_SERVER_ADDR, intent);
                                        finish();
                                    }
                                };
                            }
                            else
                            {
                                action = new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        setFourthFragment();
                                        fragmentIdx++;
                                    }
                                };
                            }

                            if (configureServersFragment.isContinuable())
                            {
                                hideKeyboard();
                                action.run();
                                Toast.makeText(ConfigApp.this,
                                        "Direcciones configuradas",
                                        Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                AlertDialog.Builder builder =
                                        new AlertDialog.Builder(ConfigApp.this);
                                builder.setTitle("Aviso");
                                builder.setMessage("No se configuraron correctamente las " +
                                        "direcciones, " +
                                        "la aplicación podría no funcionar correctamente");
                                builder.setCancelable(false);
                                builder.setPositiveButton("ok",
                                        new DialogInterface.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which)
                                            {
                                                action.run();
                                                dialog.dismiss();
                                            }
                                        });

                                builder.setNegativeButton("Cancelar",
                                        new DialogInterface.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which)
                                            {
                                                dialog.dismiss();
                                            }
                                        });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                        break;

                    case 3:
                        Intent intent = new Intent(ConfigApp.this,
                                MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                                .FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(android.R.anim.fade_in,
                                android.R.anim.fade_out);

                        break;
                }
            }
        });

        //Obtener parámetros
        singleAction = getIntent().getIntExtra("setFragment", 0);
        completeProcess = getIntent().getBooleanExtra("completeProcess", true);

        //Elegir el fragment requerido
        setInitialFragment();
    }

    private void setInitialFragment()
    {
        switch (singleAction)
        {
            case 0:
            default:
                setFirstFragment();
                fragmentIdx = 0;
                break;

            case 1:
                setSecondFragment();
                fragmentIdx = 1;
                break;

            case 2:
                setConfigServersFragment();
                fragmentIdx = 2;
                break;

            case 3:
                setFourthFragment();
                fragmentIdx = 3;
                break;
        }
    }

    private void setFirstFragment()
    {
        TextView title = findViewById(R.id.config_app_title);
        title.setVisibility(View.GONE);

        WelcomeFragment welcomeFragment = new WelcomeFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.content_container, welcomeFragment)
                .commit();
    }

    private void setSecondFragment()
    {
        TextView title = findViewById(R.id.config_app_title);
        title.setVisibility(View.VISIBLE);

        if (!completeProcess)
        {
            title.setText("Cambio de NIP");
        }

        newPinFragment = new NewPinFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                .replace(R.id.content_container, newPinFragment)
                .addToBackStack(null)
                .commit();
    }

    private void setConfigServersFragment()
    {
        TextView title = findViewById(R.id.config_app_title);
        title.setVisibility(View.VISIBLE);

        if (!completeProcess)
        {
            title.setText("Direcciones de servidores");
        }

        Bundle bundle = new Bundle();
        bundle.putBoolean("oldInfo", true);

        configureServersFragment = new ConfigureServersFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                .replace(R.id.content_container, configureServersFragment)
                .addToBackStack(null)
                .commit();
    }

    private void setFourthFragment()
    {
        TextView title = findViewById(R.id.config_app_title);
        title.setVisibility(View.VISIBLE);

        if (!completeProcess)
        {
            title.setText("Datos de estación");
        }

        getStationReference = new GetStationReference();
        getStationReference.setGetStationInterface(new GetStationReference.GetStationInterface()
        {
            @Override
            public void onStationGotten()
            {
                Intent intent = new Intent(ConfigApp.this,
                        MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent
                        .FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
            }
        });

        //Agregar argumentos
        Bundle args = new Bundle();
        args.putInt(GetStationReference.ORIGIN, GetStationReference.FROM_SETUP);
        getStationReference.setArguments(args);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                .replace(R.id.content_container, getStationReference)
                .addToBackStack(null)
                .commit();

        //Ocultar botón de "Continuar"
        continueButton.setVisibility(View.GONE);
    }

    private void hideToolBar()
    {
        Toolbar toolbar = findViewById(R.id.devices_toolbar);

        if (getSupportActionBar() == null) setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        if (!completeProcess)
        {
            finish();
        }
        else
        {
            if (fragmentIdx > 0)
            {
                if (fragmentIdx == 1)
                {
                    TextView title = findViewById(R.id.config_app_title);
                    title.setVisibility(View.GONE);
                }
                fragmentIdx--;
            }
        }
    }

    public void hideKeyboard()
    {
        View view = ConfigApp.this.getCurrentFocus();

        if (view != null)
        {
            InputMethodManager imm = (InputMethodManager) ConfigApp.this.getSystemService(
                    Context.INPUT_METHOD_SERVICE
            );

            if (imm != null) imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
