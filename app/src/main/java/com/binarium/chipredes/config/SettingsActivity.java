package com.binarium.chipredes.config;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import com.binarium.chipredes.billeteapi.ResendConfirmationEmailDialog;
import com.binarium.chipredes.billets.ui.adjustment.SalesAdjustmentActivity;
import com.binarium.chipredes.billets.ui.reservations.PendingReservationsActivity;
import com.binarium.chipredes.configstation.CashConfigFragment;
import com.binarium.chipredes.disppreset.SendPresetConfig;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.Integration;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.assignvehicleqr.AssignVehicleQR;
import com.binarium.chipredes.configstation.ConfigStation;
import com.binarium.chipredes.printer.AssignPrinter;
import com.binarium.chipredes.tokencash.ui.TokenCashConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class SettingsActivity extends AppCompatActivity {
    public static final int REPALCE_NIP = 0;
    public static final int REPALCE_SERVER_ADDR = 1;
    public static final int CHANGE_MENU_ITEMS_VISIBILITY = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Configuraciones");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        // load settings fragment
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new MainPreferenceFragment())
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return true;
    }

    @AndroidEntryPoint
    public static class MainPreferenceFragment extends PreferenceFragmentCompat {
        // Objetos
        private Activity activity;
        private ChipRedManager chipRedManager;
        private LocalModeManager localModeManager;

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.pref_main, rootKey);
            activity = getActivity();
            initChipRedManager();

            final androidx.preference.Preference changeNipPref = findPreference("replace_nip");
            assert changeNipPref != null;
            changeNipPref.setOnPreferenceClickListener(preference -> {
                final Intent intent = new Intent(activity, ConfigApp.class);
                intent.putExtra("setFragment", 1);
                intent.putExtra("completeProcess", false);
                goToPreferenceActivityForResult(intent, REPALCE_NIP);
                return true;
            });

            final androidx.preference.Preference replaceServerAddr = findPreference("config_server");
            assert replaceServerAddr != null;
            replaceServerAddr.setOnPreferenceClickListener(preference -> {
                final Intent intent = new Intent(activity, ConfigApp.class);
                intent.putExtra("setFragment", 2);
                intent.putExtra("completeProcess", false);
                goToPreferenceActivityForResult(intent, REPALCE_SERVER_ADDR);
                return true;
            });

            final androidx.preference.EditTextPreference paymentLimit = findPreference("payment_limit");
            assert paymentLimit != null;
            paymentLimit.setText(SharedPreferencesManager.getString(activity, ChipREDConstants
                    .PAYMENT_LIMIT, ""));
            paymentLimit.setOnPreferenceChangeListener((preference, newValue) -> {
                SharedPreferencesManager.putString(activity,
                        ChipREDConstants.PAYMENT_LIMIT,
                        (String) newValue);

                return false;
            });
            paymentLimit.setOnPreferenceClickListener(preference -> {
                paymentLimit.setText(SharedPreferencesManager.getString(activity,
                        ChipREDConstants.PAYMENT_LIMIT, ""));
                return false;
            });

            final androidx.preference.EditTextPreference cajaMatrixIp = findPreference("set_cm_text");
            assert cajaMatrixIp != null;
            cajaMatrixIp.setOnPreferenceChangeListener((preference, newValue) -> {
                final String newString = (String) newValue;
                if (ChipREDConstants.isAlphaNumeric(newString) && !newString.isEmpty()) {
                    showSnackbarMessage("Cambiando texto de CajaMatrix");
                    chipRedManager.setCmText(newString.toUpperCase());
                } else {
                    showSnackbarMessage("Texto inválido");
                    return false;
                }

                return true;
            });

            final androidx.preference.Preference printerManager = findPreference("set_printer_address");
            assert printerManager != null;
            printerManager.setOnPreferenceClickListener(preference -> {
                final Intent intent = new Intent(activity, AssignPrinter.class);
                startActivity(intent);

                return false;
            });

            final androidx.preference.Preference configIntegrations = findPreference("config_integrations");
            assert configIntegrations != null;
            configIntegrations.setOnPreferenceClickListener(preference -> {
                showIntegrationDialog();
                return false;
            });

            final androidx.preference.Preference configMenuItems = findPreference("config_menu");
            assert configMenuItems != null;
            configMenuItems.setOnPreferenceClickListener(preference -> {
                showMenuItemsDialog();
                return false;
            });

            final androidx.preference.Preference configTokenCash = findPreference("tc_configuration");
            assert configTokenCash != null;
            configTokenCash.setOnPreferenceClickListener(preference -> {
                final Intent intent = new Intent(activity, TokenCashConfiguration.class);
                startActivity(intent);

                return false;
            });

            final androidx.preference.Preference unAssignQr = findPreference("unassign_qr");
            assert unAssignQr != null;
            unAssignQr.setOnPreferenceClickListener(preference -> {
                final Intent intent = new Intent(activity, AssignVehicleQR.class);
                intent.putExtra("action", AssignVehicleQR.UNASSIGN_QR);
                startActivity(intent);

                return false;
            });

            final androidx.preference.Preference configStation = findPreference("config_station");
            assert configStation != null;
            configStation.setOnPreferenceClickListener(preference -> {
                final Intent intent = new Intent(activity, ConfigStation.class);
                startActivity(intent);

                return false;
            });

            final androidx.preference.Preference presetConfig = findPreference("send_preset_conf");
            assert presetConfig != null;
            presetConfig.setOnPreferenceClickListener(preference -> {
                final Intent intent = new Intent(activity, SendPresetConfig.class);
                startActivity(intent);

                return false;
            });

            final androidx.preference.Preference contingenciaManual = findPreference("contingencia_manual");
            assert contingenciaManual != null;
            contingenciaManual.setOnPreferenceClickListener(preference -> {
                // Pedir valor actual de la contingencia
                chipRedManager.getNetworkState();

                // Crear objeto para manejar interfaz
                localModeManager = new LocalModeManager(getActivity());
                localModeManager.showDialog();

                // Definir interfaz
                localModeManager.setmInterface(state -> chipRedManager.setEstadoContingencia(state));
                return false;
            });

            final androidx.preference.Preference resendConfirmationEmail = findPreference("resend_confirmation_email");
            assert resendConfirmationEmail != null;
            resendConfirmationEmail.setOnPreferenceClickListener(preference -> {
                ResendConfirmationEmailDialog dialogFragment = new ResendConfirmationEmailDialog();
                assert getFragmentManager() != null;
                dialogFragment.show(getFragmentManager(), "clientAccountDialog");
                return true;
            });

            final Preference configCashSales = findPreference("config_cash_sales");
            assert configCashSales != null;
            configCashSales.setOnPreferenceClickListener(preference -> {
                CashConfigFragment cashConfigFragment = new CashConfigFragment();
                assert getFragmentManager() != null;
                cashConfigFragment.show(getFragmentManager(), "cashConfigDialog");
                return true;
            });

            final Preference adjustBilletSales = findPreference("sales_adjustment");
            if (adjustBilletSales != null) {
                adjustBilletSales.setOnPreferenceClickListener(preference -> {
                    startActivity(new Intent(requireActivity(), SalesAdjustmentActivity.class));
                    return true;
                });
            }

            final Preference pendingReservations = findPreference("pending_reservations");
            if (pendingReservations != null) {
                pendingReservations.setOnPreferenceClickListener(preference -> {
                    startActivity(new Intent(requireActivity(), PendingReservationsActivity.class));
                    return true;
                });
            }
        }

        private void goToPreferenceActivityForResult(Intent intent, int requestCode) {
            assert activity != null;
            activity.startActivityForResult(intent, requestCode);
        }

        private void initChipRedManager() {
            chipRedManager = new ChipRedManager(new ChipRedManager.OnMessageReceived() {
                @Override
                public void chipRedMessage(String crMessage, int webServiceN) {
                    showSnackbarMessage(crMessage);
                    if (webServiceN == ChipRedManager.GET_NETWORK_STATE) {
                        if (localModeManager != null) localModeManager.setState(0);
                    } else if (webServiceN == ChipRedManager.SET_ESTADO_CONTINGENCIA) {
                        localModeManager.showViews();
                    }
                }

                @Override
                public void chipRedError(String errorMessage, int webServiceN) {
                    showSnackbarMessage(errorMessage);
                    if (webServiceN == ChipRedManager.GET_NETWORK_STATE) {
                        if (localModeManager != null) localModeManager.setState(0);
                    } else if (webServiceN == ChipRedManager.SET_ESTADO_CONTINGENCIA) {
                        localModeManager.showViews();
                    }
                }

                @Override
                public void showResponse(JSONObject response, int webServiceN) {
                    if (webServiceN == ChipRedManager.GET_NETWORK_STATE) {
                        try {
                            int state = response.getJSONObject("data").getInt("estado");
                            if (localModeManager != null) localModeManager.setState(state);
                        } catch (JSONException e) {
                            showSnackbarMessage("Error al obtener estado de la contingencia");
                            if (localModeManager != null) localModeManager.setState(0);
                        }
                    } else if (webServiceN == ChipRedManager.SET_ESTADO_CONTINGENCIA) {
                        // Obtener estado de la contingencia manual
                        chipRedManager.getNetworkState();
                    }
                }
            }, activity);
        }

        private void showSnackbarMessage(String text) {
            try {
                assert activity != null;
                Snackbar.make(activity.findViewById(android.R.id.content), text, Snackbar
                        .LENGTH_LONG).show();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        private void showIntegrationDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            View content = getLayoutInflater().inflate(R.layout.integration_config_list, null);

            //Obtener integraciones y sus valores
            ArrayList<Integration> integrationsList = new ArrayList<>();

            String[][] mIntegrations = ChipREDConstants.INTEGRATIONS;
            for (String[] integrationArray : mIntegrations) {
                Integration integration = new Integration(integrationArray[0], integrationArray[1],
                        activity);
                integrationsList.add(integration);
            }

            IntegrationsConfigAdapter integrationsConfigAdapter = new IntegrationsConfigAdapter
                    (integrationsList, activity);

            RecyclerView recyclerView = content.findViewById(R.id.integration_list);
            recyclerView.setAdapter(integrationsConfigAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(activity));
            recyclerView.setHasFixedSize(true);

            builder.setView(content);
            builder.setPositiveButton("Guardar", (dialog, which) ->
            {
                Intent intent = new Intent();
                assert activity != null;
                activity.setResult(SettingsActivity.CHANGE_MENU_ITEMS_VISIBILITY, intent);
                dialog.dismiss();
            });
            builder.setNegativeButton("Cerrar", (dialog, which) -> dialog.dismiss());

            Dialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }

        private void showMenuItemsDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            View content = getLayoutInflater().inflate(R.layout.integration_config_list, null);

            //Obtener integraciones y sus valores
            ArrayList<Integration> integrationsList = new ArrayList<>();

            String[][] mIntegrations = ChipREDConstants.MENU_ITEMS;
            for (String[] integrationArray : mIntegrations) {
                Integration integration = new Integration(integrationArray[0], integrationArray[1],
                        activity);
                integrationsList.add(integration);
            }

            IntegrationsConfigAdapter integrationsConfigAdapter = new IntegrationsConfigAdapter
                    (integrationsList, activity);

            RecyclerView recyclerView = content.findViewById(R.id.integration_list);
            recyclerView.setAdapter(integrationsConfigAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(activity));
            recyclerView.setHasFixedSize(true);

            builder.setView(content);
            builder.setPositiveButton("Guardar", (dialog, which) ->
            {
                Intent intent = new Intent();
                assert activity != null;
                activity.setResult(SettingsActivity.CHANGE_MENU_ITEMS_VISIBILITY, intent);
            });

            builder.setNegativeButton("Cancelar", (dialog, which) -> dialog.dismiss());

            Dialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == REPALCE_SERVER_ADDR) {
            Intent intent = new Intent();
            setResult(SettingsActivity.REPALCE_SERVER_ADDR, intent);
            finish();
        }
    }
}
