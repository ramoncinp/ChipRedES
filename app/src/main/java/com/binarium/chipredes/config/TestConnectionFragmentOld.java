package com.binarium.chipredes.config;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.ChipRedManager;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.di.NetworkModule;
import com.binarium.chipredes.emax.EmaxProvider;

import org.json.JSONObject;

import com.github.leandroborgesferreira.loadingbutton.customViews.CircularProgressButton;
import timber.log.Timber;

public class TestConnectionFragmentOld extends Fragment {
    private static final String TAG = "TestConnectionFragment";

    // Objetos
    private ChipRedManager chipRedManager;

    // Variables
    private boolean successLocal = false;
    private boolean successWolke = false;
    private boolean successWebSocket = false;

    // Views
    private CircularProgressButton button1;
    private CircularProgressButton button2;
    private CircularProgressButton button3;
    private CircularProgressButton button4;
    private LinearLayout[] ipLayouts;
    private LinearLayout[] etLayouts;
    private TextView warningSign;

    public TestConnectionFragmentOld() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_set_addrs, container, false);
        warningSign = rootView.findViewById(R.id.warning_sign);
        initRadioGroups(rootView);
        initProgressButtons(rootView);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setButtonsAnimations();
        chipRedManager = new ChipRedManager(getChipRedListener(), getActivity());

        assert getArguments() != null;
        boolean oldData = getArguments().getBoolean("oldInfo");
        if (oldData) {
            loadCurrentData();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        button1.dispose();
        button2.dispose();
        button3.dispose();
        button4.dispose();
    }

    private void initProgressButtons(View rootView) {
        button1 = rootView.findViewById(R.id.test_button_1);
        button2 = rootView.findViewById(R.id.test_button_2);
        button3 = rootView.findViewById(R.id.test_button_3);
        button4 = rootView.findViewById(R.id.test_button_4);
    }

    private void initRadioGroups(View rootView) {
        ipLayouts = new LinearLayout[3];
        etLayouts = new LinearLayout[3];

        ipLayouts[0] = rootView.findViewById(R.id.ip_et1);
        ipLayouts[1] = rootView.findViewById(R.id.ip_et2);
        ipLayouts[2] = rootView.findViewById(R.id.ip_et3);

        etLayouts[0] = rootView.findViewById(R.id.url_layout1);
        etLayouts[1] = rootView.findViewById(R.id.url_layout2);
        etLayouts[2] = rootView.findViewById(R.id.url_layout3);

        RadioGroup edSelector1 = rootView.findViewById(R.id.et_selector1);
        RadioGroup edSelector2 = rootView.findViewById(R.id.et_selector2);
        RadioGroup edSelector3 = rootView.findViewById(R.id.et_selector3);

        RadioGroup.OnCheckedChangeListener changeListener = (group, checkedId) -> {
            int layoutIdx = 0;

            if (group.getId() == R.id.et_selector1) {
                layoutIdx = 0;
            } else if (group.getId() == R.id.et_selector2) {
                layoutIdx = 1;
            } else if (group.getId() == R.id.et_selector3) {
                layoutIdx = 2;
            }

            if (checkedId == R.id.ip_type_1 || checkedId == R.id.ip_type_2 || checkedId == R
                    .id.ip_type_3) {
                ipLayouts[layoutIdx].setVisibility(View.VISIBLE);
                etLayouts[layoutIdx].setVisibility(View.GONE);
            } else {
                ipLayouts[layoutIdx].setVisibility(View.GONE);
                etLayouts[layoutIdx].setVisibility(View.VISIBLE);
            }
        };

        edSelector1.setOnCheckedChangeListener(changeListener);
        edSelector2.setOnCheckedChangeListener(changeListener);
        edSelector3.setOnCheckedChangeListener(changeListener);
    }

    private void setButtonsAnimations() {
        button1.setOnClickListener(v -> {
            hideKeyboard();
            button1.revertAnimation();
            button1.startAnimation();

            Handler handler = new Handler();
            handler.postDelayed(() -> {
                String localAddr = getLocalUrl();

                if (localAddr != null) {
                    showWarning("");
                    chipRedManager.setURL(localAddr);
                    chipRedManager.testConnection(0);

                    //Almacenar direccion aunque no se haya comprobado su funcionamiento
                    SharedPreferencesManager.putString(getContext(),
                            ChipREDConstants.CR_LOCAL_IP,
                            localAddr);
                    NetworkModule.INSTANCE.setLocalUrl(localAddr);
                } else {
                    showWarning("Dirección inválida");
                    button1.revertAnimation();
                }
            }, 200);
        });

        button2.setOnClickListener(v -> {
            hideKeyboard();
            button2.revertAnimation();
            button2.startAnimation();

            Handler handler = new Handler();
            handler.postDelayed(() -> {
                String wolkeAddr = getWolkeAddr();

                if (wolkeAddr != null) {
                    showWarning("");
                    chipRedManager.setURL(wolkeAddr);
                    chipRedManager.testConnection(1);

                    //Almacenar direccion aunque no se haya comprobado su funcionamiento
                    SharedPreferencesManager.putString(getContext(),
                            ChipREDConstants.CR_WOLKE_IP,
                            wolkeAddr);
                    NetworkModule.INSTANCE.setWolkeUrl(wolkeAddr);
                } else {
                    showWarning("Dirección inválida");
                    button2.revertAnimation();
                }
            }, 200);
        });

        button3.setOnClickListener(v -> {
            hideKeyboard();
            button3.revertAnimation();
            button3.startAnimation();

            Handler handler = new Handler();
            handler.postDelayed(() -> {
                String socketAddr = getWebSocketUrl();

                if (socketAddr != null) {
                    showWarning("");
                    chipRedManager.setURL(socketAddr);
                    chipRedManager.testConnection(2);

                    //Almacenar direccion aunque no se haya comprobado su funcionamiento
                    SharedPreferencesManager.putString(getContext(), ChipREDConstants
                            .CR_WEB_SOCKET_IP, socketAddr);
                } else {
                    showWarning("Dirección inválida");
                    button3.revertAnimation();
                }
            }, 200);
        });

        button4.setOnClickListener(v -> {
            hideKeyboard();
            button4.revertAnimation();
            button4.startAnimation();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    String emaxUrl = getEmaxUrl();
                    if (!emaxUrl.isEmpty()) {
                        showWarning("");

                        //Almacenar direccion aunque no se haya comprobado su funcionamiento
                        SharedPreferencesManager.putString(getContext(), ChipREDConstants
                                .EMAX_URL, emaxUrl);

                        EmaxProvider.testConnection(getContext(),
                                new EmaxProvider.SimecobConnectionInterface() {
                                    @Override
                                    public void onSuccess(String response) {
                                        setSuccessInButton(4);
                                    }

                                    @Override
                                    public void onError(String response) {
                                        setErrorInButton(4);
                                    }
                                });
                    } else {
                        showWarning("Dirección inválida");
                        button4.revertAnimation();
                    }
                }
            }, 200);
        });
    }

    private String getLocalUrl() {
        String URL;

        EditText[] editTexts = new EditText[6];
        editTexts[0] = getActivity().findViewById(R.id.ip_et_seg1);
        editTexts[1] = getActivity().findViewById(R.id.ip_et_seg2);
        editTexts[2] = getActivity().findViewById(R.id.ip_et_seg3);
        editTexts[3] = getActivity().findViewById(R.id.ip_et_seg4);
        editTexts[4] = getActivity().findViewById(R.id.ip_et_seg5);
        editTexts[5] = getActivity().findViewById(R.id.url_et1);

        URL = decodeFromEditText(editTexts, 0);

        if (URL != null) URL = "http://" + URL + "/webservice/tablet/";

        return URL;
    }

    private String getLocalDefaultUrl() {
        return "http://0.0.0.0:80/webservice/tablet/";
    }

    private String getWolkeAddr() {
        String URL;

        EditText[] editTexts = new EditText[6];
        editTexts[0] = getActivity().findViewById(R.id.ip_et2_seg1);
        editTexts[1] = getActivity().findViewById(R.id.ip_et2_seg2);
        editTexts[2] = getActivity().findViewById(R.id.ip_et2_seg3);
        editTexts[3] = getActivity().findViewById(R.id.ip_et2_seg4);
        editTexts[4] = getActivity().findViewById(R.id.ip_et2_seg5);
        editTexts[5] = getActivity().findViewById(R.id.url_et2);

        URL = decodeFromEditText(editTexts, 1);

        if (URL != null) URL = "http://" + URL + "/webservice_app/";

        return URL;
    }

    private String getWebSocketUrl() {
        String URL;

        EditText[] editTexts = new EditText[6];
        editTexts[0] = getActivity().findViewById(R.id.ip_et3_seg1);
        editTexts[1] = getActivity().findViewById(R.id.ip_et3_seg2);
        editTexts[2] = getActivity().findViewById(R.id.ip_et3_seg3);
        editTexts[3] = getActivity().findViewById(R.id.ip_et3_seg4);
        editTexts[4] = getActivity().findViewById(R.id.ip_et3_seg5);
        editTexts[5] = getActivity().findViewById(R.id.dns_et);

        URL = decodeFromEditText(editTexts, 2);

        if (URL != null) URL = "http://" + URL + "/socket";

        return URL;
    }

    private String getEmaxUrl() {
        EditText emaxUrlEt = getActivity().findViewById(R.id.url_et4);
        return emaxUrlEt.getText().toString();
    }

    private String getWebSocketDefaultUrl() {
        return "http://0.0.0.0:80/socket";
    }

    private void setLocalIp(String local) {
        EditText[] editTexts = new EditText[6];
        editTexts[0] = getActivity().findViewById(R.id.ip_et_seg1);
        editTexts[1] = getActivity().findViewById(R.id.ip_et_seg2);
        editTexts[2] = getActivity().findViewById(R.id.ip_et_seg3);
        editTexts[3] = getActivity().findViewById(R.id.ip_et_seg4);
        editTexts[4] = getActivity().findViewById(R.id.ip_et_seg5);
        editTexts[5] = getActivity().findViewById(R.id.url_et1);
        setEditTextWithIp(editTexts, local);
    }

    private void setWolkeIp(String wolke) {
        EditText[] editTexts = new EditText[6];
        editTexts[0] = getActivity().findViewById(R.id.ip_et2_seg1);
        editTexts[1] = getActivity().findViewById(R.id.ip_et2_seg2);
        editTexts[2] = getActivity().findViewById(R.id.ip_et2_seg3);
        editTexts[3] = getActivity().findViewById(R.id.ip_et2_seg4);
        editTexts[4] = getActivity().findViewById(R.id.ip_et2_seg5);
        editTexts[5] = getActivity().findViewById(R.id.url_et2);
        setEditTextWithIp(editTexts, wolke);
    }

    private void setSocketIp(String socket) {
        EditText[] editTexts = new EditText[6];
        editTexts[0] = getActivity().findViewById(R.id.ip_et3_seg1);
        editTexts[1] = getActivity().findViewById(R.id.ip_et3_seg2);
        editTexts[2] = getActivity().findViewById(R.id.ip_et3_seg3);
        editTexts[3] = getActivity().findViewById(R.id.ip_et3_seg4);
        editTexts[4] = getActivity().findViewById(R.id.ip_et3_seg5);
        editTexts[5] = getActivity().findViewById(R.id.dns_et);
        setEditTextWithIp(editTexts, socket);
    }

    private void setEmaxIp(String emaxUrl) {
        EditText emaxUrlEt = getActivity().findViewById(R.id.url_et4);
        emaxUrlEt.setText(emaxUrl);
    }

    private void setEditTextWithIp(EditText[] editTexts, String ip) {
        int idx = 0;

        // Obtener segmentos separados por puntos
        while (ip.length() > 0) {
            int pointIdx = ip.indexOf(".");
            if (pointIdx == -1) {
                // Buscar separador de puerto
                int portSeparator = ip.indexOf(":");
                if (portSeparator == -1) {
                    // No hay separador de puerto
                    // Queda el último segmento
                    editTexts[idx].setText(ip);

                    // Limpiar cadena
                    ip = "";
                } else {
                    // Sí hay separador de puerto
                    // Obtener último segment
                    String lastSegment = ip.substring(0, portSeparator);
                    String port = ip.substring(portSeparator + 1);

                    // Escribir textos
                    editTexts[idx++].setText(lastSegment);

                    // Validar que el editText del puerto no sea nulo
                    if (idx < editTexts.length)
                        editTexts[idx].setText(port);

                    // Limpiar cadena
                    ip = "";
                }
            } else {
                // Obtener segmento
                String segment = ip.substring(0, pointIdx);

                // Escribirlo en editText e incrementar indice para próxima vuelta
                editTexts[idx++].setText(segment);

                // Cortar cadena
                ip = ip.substring(pointIdx + 1);
            }
        }
    }

    private String decodeFromEditText(EditText[] editTexts, int idx) {
        String URL = "";

        //Si se ingresó IP
        if (ipLayouts[idx].getVisibility() == View.VISIBLE) {
            if (editTexts[0].getText().toString().isEmpty()) return null;
            else URL += editTexts[0].getText().toString();

            URL += ".";
            if (editTexts[1].getText().toString().isEmpty()) return null;
            else URL += editTexts[1].getText().toString();

            URL += ".";
            if (editTexts[2].getText().toString().isEmpty()) return null;
            else URL += editTexts[2].getText().toString();

            URL += ".";
            if (editTexts[3].getText().toString().isEmpty()) return null;
            else URL += editTexts[3].getText().toString();

            // Verificar si se va a definir el puerto
            if (editTexts.length == 6) {
                URL += ":";
                if (editTexts[4].getText().toString().isEmpty()) return null;
                else URL += editTexts[4].getText().toString();
            }
        } else {
            if (editTexts[editTexts.length - 1].getText().toString().isEmpty()) return null;
            else URL = editTexts[editTexts.length - 1].getText().toString();
        }

        return URL;
    }

    private Bitmap convertToBitmap(Drawable drawable) {
        Bitmap mutableBitmap = Bitmap.createBitmap(48, 48, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, 48, 48);
        drawable.draw(canvas);

        return mutableBitmap;
    }

    private void setSuccessInButton(int buttonNumber) {
        int colorResource = getActivity().getResources().getColor(R.color.green_money);
        Drawable drawable = getActivity().getResources().getDrawable(R.drawable.ic_check);
        Bitmap bitmap = convertToBitmap(drawable);

        switch (buttonNumber) {
            case 1:
                button1.doneLoadingAnimation(colorResource, bitmap);
                button1.setClickable(true);
                SharedPreferencesManager.putString(getContext(), ChipREDConstants.CR_LOCAL_IP,
                        getLocalUrl());
                successLocal = true;
                break;

            case 2:
                button2.doneLoadingAnimation(colorResource, bitmap);
                button2.setClickable(true);
                SharedPreferencesManager.putString(getContext(), ChipREDConstants.CR_WOLKE_IP,
                        getWolkeAddr
                                ());
                successWolke = true;
                break;

            case 3:
                button3.doneLoadingAnimation(colorResource, bitmap);
                button3.setClickable(true);
                SharedPreferencesManager.putString(getContext(), ChipREDConstants.CR_WEB_SOCKET_IP,
                        getWebSocketUrl());
                successWebSocket = true;
                break;

            case 4:
                button4.doneLoadingAnimation(colorResource, bitmap);
                button4.setClickable(true);
                SharedPreferencesManager.putString(getContext(), ChipREDConstants.EMAX_URL,
                        getEmaxUrl());
                break;
        }
    }

    private void setErrorInButton(int buttonNumber) {
        int colorResource = getActivity().getResources().getColor(R.color.colorPrimary);
        Drawable drawable = getActivity().getResources().getDrawable(R.drawable.ic_clear);
        Bitmap bitmap = convertToBitmap(drawable);

        switch (buttonNumber) {
            case 1:
                button1.doneLoadingAnimation(colorResource, bitmap);
                button1.setClickable(true);
                successLocal = false;
                break;

            case 2:
                button2.doneLoadingAnimation(colorResource, bitmap);
                button2.setClickable(true);
                successWolke = false;
                break;

            case 3:
                button3.doneLoadingAnimation(colorResource, bitmap);
                button3.setClickable(true);
                successWebSocket = false;
                break;

            case 4:
                button4.doneLoadingAnimation(colorResource, bitmap);
                button4.setClickable(true);
                break;
        }
    }

    private ChipRedManager.OnMessageReceived getChipRedListener() {
        return new ChipRedManager.OnMessageReceived() {
            @Override
            public void chipRedMessage(String crMessage, final int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        TestConnectionFragmentOld.this))
                    return;

                if (webServiceN == 2) {
                    getActivity().runOnUiThread(() -> setSuccessInButton(webServiceN + 1));
                } else {
                    setSuccessInButton(webServiceN + 1);
                }
                Timber.d("Mensaje recibido de ChipRedManager");
            }

            @Override
            public void chipRedError(String errorMessage, final int webServiceN) {
                if (ChipREDConstants.checkNullParentActivity(getActivity(),
                        TestConnectionFragmentOld.this))
                    return;

                if (webServiceN == 2) {
                    getActivity().runOnUiThread(() -> setErrorInButton(webServiceN + 1));
                } else {
                    setErrorInButton(webServiceN + 1);
                }
                Timber.d("Error: %s", errorMessage);
            }

            @Override
            public void showResponse(JSONObject response, int webServiceN) {

            }
        };
    }

    public void hideKeyboard() {
        View view = getActivity().getCurrentFocus();

        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context
                    .INPUT_METHOD_SERVICE);

            if (imm != null) imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    boolean isContinuable() {
        // Evaluar si solamente el test de wolke fue exitoso
        if (successWolke && !successLocal && !successWebSocket) {
            // Evaluar si las direcciones de local y websocket estan vacias
            if (getLocalUrl() == null && getWebSocketUrl() == null) {
                // Almacenar las direcciones default
                SharedPreferencesManager.putString(getContext(), ChipREDConstants.CR_LOCAL_IP,
                        getLocalDefaultUrl());

                SharedPreferencesManager.putString(getContext(), ChipREDConstants.CR_WEB_SOCKET_IP,
                        getWebSocketDefaultUrl());
            }
        }

        return successLocal && successWolke && successWebSocket;
    }

    private void loadCurrentData() {
        String local, wolke, socket, emax;

        local = SharedPreferencesManager.getString(getContext(), ChipREDConstants.CR_LOCAL_IP, "");
        wolke = SharedPreferencesManager.getString(getContext(), ChipREDConstants.CR_WOLKE_IP, "");
        socket = SharedPreferencesManager.getString(getContext(),
                ChipREDConstants.CR_WEB_SOCKET_IP, "");
        emax = SharedPreferencesManager.getString(getContext(), ChipREDConstants.EMAX_URL, "");

        //http://192.168.0.50:9999/webservice/tablet/
        if (!local.equals("")) {
            local = getContentFromUrl(local);
            if (isIP(local)) {
                setLocalIp(local);
            } else {
                RadioButton radioButton = getActivity().findViewById(R.id.text_type_1);
                radioButton.setChecked(true);

                etLayouts[0].setVisibility(View.VISIBLE);
                ipLayouts[0].setVisibility(View.GONE);

                EditText editText = getActivity().findViewById(R.id.url_et1);
                editText.setText(local);
            }
        }

        if (!wolke.equals("")) {
            wolke = getContentFromUrl(wolke);
            if (isIP(wolke)) {
                setWolkeIp(wolke);
            } else {
                RadioButton radioButton = getActivity().findViewById(R.id.text_type_2);
                radioButton.setChecked(true);

                etLayouts[1].setVisibility(View.VISIBLE);
                ipLayouts[1].setVisibility(View.GONE);

                EditText editText = getActivity().findViewById(R.id.url_et2);
                editText.setText(wolke);
            }
        }

        if (!socket.equals("")) {
            socket = getContentFromUrl(socket);
            if (isIP(socket)) {
                setSocketIp(socket);
            } else {
                RadioButton radioButton = getActivity().findViewById(R.id.text_type_3);
                radioButton.setChecked(true);

                etLayouts[2].setVisibility(View.VISIBLE);
                ipLayouts[2].setVisibility(View.GONE);

                EditText editText = getActivity().findViewById(R.id.dns_et);
                editText.setText(socket);
            }
        }

        if (!emax.equals("")) {
            setEmaxIp(emax);
        }
    }

    // Formato de entrada -> ej. http://192.168.0.50:9999/webservice/tablet/
    private String getContentFromUrl(String url) {
        //Buscar index del primer ":" y sumarle 3 para inicio de direccion, este es el primer limite
        int idx1 = url.indexOf(":") + 3;

        //Buscar index del limite final de la direccion...que sería: "/"
        int idx2 = idx1;
        char mChar = 0;
        while (mChar != '/') {
            idx2++;
            mChar = url.charAt(idx2);
        }

        return url.substring(idx1, idx2);
    }

    private boolean isIP(String text) {
        return countMatches(text, '.') >= 3 && text.contains(":");
    }

    private int countMatches(String text, char match) {
        int counter = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == match) {
                counter++;
            }
        }

        return counter;
    }

    private void showWarning(String text) {
        boolean snackOrTextView = getActivity().getResources().getBoolean(R.bool
                .is_warning_text_view);

        if (text.equals("")) {
            warningSign.setText(text);
            return;
        }

        if (snackOrTextView) {
            warningSign.setText(text);
        } else {
            Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
        }
    }
}
