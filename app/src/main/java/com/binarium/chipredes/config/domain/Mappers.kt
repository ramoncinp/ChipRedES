package com.binarium.chipredes.config.domain

import com.binarium.chipredes.config.data.model.ServerTestState
import com.binarium.chipredes.config.data.model.TestServerResponse

fun TestServerResponse.toState() = if (this is TestServerResponse.Success)
    ServerTestState.SUCCESS
else
    ServerTestState.ERROR
