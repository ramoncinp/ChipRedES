package com.binarium.chipredes.config.data.model

enum class ServerName {
    LOCAL,
    WOLKE,
    SOCKET,
    EMAX,
    EMAX_CLIENTE
}

enum class ServerAddressType {
    IP,
    DNS
}

enum class ServerTestState {
    IDLE,
    SUCCESS,
    ERROR
}

data class ConfigurableServer(
    val name: ServerName,
    var addressType: ServerAddressType = ServerAddressType.IP,
    val address: String,
    val testState: ServerTestState = ServerTestState.IDLE
)
