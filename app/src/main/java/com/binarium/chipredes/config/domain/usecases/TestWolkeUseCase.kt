package com.binarium.chipredes.config.domain.usecases

import android.content.SharedPreferences
import androidx.core.content.edit
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.config.data.model.ConfigurableServer
import com.binarium.chipredes.config.data.model.TestServerResponse
import com.binarium.chipredes.config.utils.isAddressValid
import com.binarium.chipredes.wolke.WolkeApiService
import com.binarium.chipredes.wolke.models.TestConnectionPost
import java.lang.Exception
import javax.inject.Inject

class TestWolkeUseCase @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val wolkeApiService: WolkeApiService,
) {

    suspend operator fun invoke(server: ConfigurableServer): TestServerResponse {
        if (server.isAddressValid().not()) return TestServerResponse.Error(server)
        return try {
            setAddress(server.address)
            wolkeApiService.testConnection(TestConnectionPost())
            TestServerResponse.Success(server)
        } catch (e: Exception) {
            e.printStackTrace()
            TestServerResponse.Error(server)
        }
    }

    private fun setAddress(addressToTest: String) {
        val wolkeUrl = "$addressToTest/webservice_app/"
        sharedPreferences.edit {
            putString(ChipREDConstants.CR_WOLKE_IP, wolkeUrl)
        }
    }
}
