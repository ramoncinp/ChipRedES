package com.binarium.chipredes.config;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.binarium.chipredes.R;

public class NewPinFragment extends Fragment {
    private EditText et1;
    private EditText et2;
    private EditText et3;
    private EditText et4;

    private String firstNipIntent;

    private boolean confirmation = false;

    public NewPinFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_pin, container, false);

        et1 = view.findViewById(R.id.et_pin_1);
        et2 = view.findViewById(R.id.et_pin_2);
        et3 = view.findViewById(R.id.et_pin_3);
        et4 = view.findViewById(R.id.et_pin_4);

        et1.addTextChangedListener(new GenericTextWatcher(et1));
        et2.addTextChangedListener(new GenericTextWatcher(et2));
        et3.addTextChangedListener(new GenericTextWatcher(et3));
        et4.addTextChangedListener(new GenericTextWatcher(et4));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showKeyboard(getActivity());
        et1.requestFocus();
    }

    public class GenericTextWatcher implements TextWatcher {
        private final View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int id = view.getId();
            if (id == R.id.et_pin_1) {
                if (text.length() == 1)
                    et2.requestFocus();
            } else if (id == R.id.et_pin_2) {
                if (text.length() == 1)
                    et3.requestFocus();
            } else if (id == R.id.et_pin_3) {
                if (text.length() == 1)
                    et4.requestFocus();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    }

    public String getNip() {
        String finalNip;

        String digit1 = et1.getText().toString();
        String digit2 = et2.getText().toString();
        String digit3 = et3.getText().toString();
        String digit4 = et4.getText().toString();

        if (!digit1.isEmpty() &&
                !digit2.isEmpty() &&
                !digit3.isEmpty() &&
                !digit4.isEmpty()) {
            finalNip = digit1 + digit2 + digit3 + digit4;
            return finalNip;
        } else {
            return null;
        }
    }

    public static void showKeyboard(Context context) {
        try {
            ((InputMethodManager) (context)
                    .getSystemService(Context.INPUT_METHOD_SERVICE))
                    .toggleSoftInput(InputMethodManager.SHOW_FORCED,
                            InputMethodManager.HIDE_IMPLICIT_ONLY);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public boolean isConfirmation() {
        return confirmation;
    }

    public void setToConfirm() {
        TextView textView = getActivity().findViewById(R.id.nip_desc);
        textView.setText("Confirme el NIP ingresado");

        confirmation = true;

        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");

        et1.requestFocus();
        showKeyboard(getActivity());
    }

    public void setFirstNipIntent(String firstNipIntent) {
        this.firstNipIntent = firstNipIntent;
    }

    public String getFirstNipIntent() {
        return firstNipIntent;
    }
}
