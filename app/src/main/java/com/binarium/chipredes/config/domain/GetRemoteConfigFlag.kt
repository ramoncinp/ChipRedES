package com.binarium.chipredes.config.domain

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

class GetRemoteConfigBoolean @Inject constructor(
    private val remoteConfig: FirebaseRemoteConfig,
) {

    operator fun invoke(flagName: String): Boolean = try {
        remoteConfig.getBoolean(flagName)
    } catch (e: Exception) {
        Timber.e("Error trying to get remote config boolean")
        false
    }
}
