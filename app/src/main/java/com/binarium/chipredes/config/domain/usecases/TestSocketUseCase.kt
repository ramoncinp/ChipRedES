package com.binarium.chipredes.config.domain.usecases

import android.content.SharedPreferences
import androidx.core.content.edit
import com.binarium.chipredes.ChipREDConstants
import com.binarium.chipredes.config.data.model.ConfigurableServer
import com.binarium.chipredes.config.data.model.TestServerResponse
import com.binarium.chipredes.config.utils.isAddressValid
import kotlinx.coroutines.*
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.net.URI
import java.net.URISyntaxException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TestSocketUseCase @Inject constructor(
    private val sharedPreferences: SharedPreferences,
) {

    suspend operator fun invoke(server: ConfigurableServer): TestServerResponse {
        if (server.isAddressValid().not()) return TestServerResponse.Error(server)
        val socketUrl = "${server.address}/socket"
        setAddress(socketUrl)
        try {
            val uri = URI(socketUrl)
            testWebSocket(uri)?.let {
                if (it) return TestServerResponse.Success(server)
            }
        } catch (ue: URISyntaxException) {
            ue.printStackTrace()
        }

        return TestServerResponse.Error(server)
    }

    private fun setAddress(addressToTest: String) {
        sharedPreferences.edit {
            putString(ChipREDConstants.CR_WEB_SOCKET_IP, addressToTest)
        }
    }

    private suspend fun testWebSocket(uri: URI): Boolean? = withContext(Dispatchers.IO) {
        val webSocketClient: WebSocketClient = object : WebSocketClient(uri) {
            override fun onOpen(serverHandshake: ServerHandshake) {}

            override fun onMessage(s: String) {}

            override fun onClose(i: Int, s: String, b: Boolean) {}

            override fun onError(e: Exception) {}
        }

        runCatching {
            webSocketClient.connectBlocking(5, TimeUnit.SECONDS)
        }.getOrDefault(null)
    }
}
