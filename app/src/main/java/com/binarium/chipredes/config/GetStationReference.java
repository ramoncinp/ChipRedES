package com.binarium.chipredes.config;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.binarium.chipredes.ChipREDConstants;
import com.binarium.chipredes.GenericDialog;
import com.binarium.chipredes.R;
import com.binarium.chipredes.SharedPreferencesManager;
import com.binarium.chipredes.configstation.ConfigStation;
import com.binarium.chipredes.configstation.StationFileManager;
import com.binarium.chipredes.configstation.StationManager;
import com.binarium.chipredes.fcm.FcmUtils;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;

import org.json.JSONException;
import org.json.JSONObject;

public class GetStationReference extends Fragment implements StationManager.OnResponse {
    //Constantes
    public final static int FROM_SETTINGS = 0;
    public final static int FROM_SETUP = 1;
    public final static int SET_STATION_ELEMENTS = 100;
    public final static int SET_STATION_ELEMENTS_OK = 200;
    public final static String ORIGIN = "origin";

    //Variables
    private String mongoId;

    //Views
    private Button login;
    private Button newStation;
    private LinearLayout form;
    private MaterialEditText username;
    private MaterialEditText password;
    private ProgressBar progressBar;

    //Objetos
    private JSONObject stationData;
    private StationManager stationManager;

    //Interfaz
    private GetStationInterface getStationInterface;

    public GetStationReference() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View content = inflater.inflate(R.layout.fragment_get_station_reference, container, false);
        newStation = content.findViewById(R.id.new_station_button);
        login = content.findViewById(R.id.login);
        username = content.findViewById(R.id.station_username);
        password = content.findViewById(R.id.station_password);
        progressBar = content.findViewById(R.id.progress_bar);
        form = content.findViewById(R.id.login_layout);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                form.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                validateAndSendData();
            }
        });

        //Evaluar la función del fragment
        if (getArguments() != null) {
            Bundle args = getArguments();
            if (args.containsKey(ORIGIN)) {
                int op = args.getInt(ORIGIN);
                if (op == FROM_SETTINGS) {
                    newStation.setVisibility(View.GONE);
                } else {
                    newStation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getActivity(), ConfigStation.class);
                            intent.putExtra(ChipREDConstants.CONFIG_NEW_STATION, true);
                            startActivityForResult(intent, SET_STATION_ELEMENTS);
                        }
                    });
                }
            }
        } else {
            newStation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ConfigStation.class);
                    intent.putExtra(ChipREDConstants.CONFIG_NEW_STATION, true);
                    startActivityForResult(intent, SET_STATION_ELEMENTS);
                }
            });
        }

        // Inflate the layout for this fragment
        return content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        stationManager = new StationManager(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SET_STATION_ELEMENTS && resultCode == SET_STATION_ELEMENTS_OK) {
            getStationInterface.onStationGotten();
        }
    }

    @Override
    public void chipRedMessage(String crMessage, int webServiceN) {
        form.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        ChipREDConstants.showSnackBarMessage(crMessage, getActivity());
    }

    @Override
    public void chipRedError(String errorMessage, int webServiceN) {
        form.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        ChipREDConstants.showSnackBarMessage(errorMessage, getActivity());
    }

    @Override
    public void showResponse(JSONObject response, int webServiceN) {
        switch (webServiceN) {
            case StationManager.GET_GENERAL_DATA:
                confirmGottenStation(response);
                break;

            case StationManager.STATION_LOGIN:
                getStationId(response);
                break;
        }
    }

    private void validateAndSendData() {
        String usernameValue, passwordValue;
        boolean valid;

        if (username.getText() != null) {
            valid = username.validateWith(new METValidator("Usuario inválido") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    if (isEmpty) {
                        return false;
                    } else {
                        String mUsername = text.toString();
                        return mUsername.contains("@");
                    }
                }
            });
        } else {
            valid = false;
        }

        if (password.getText() != null) {
            valid &= password.validateWith(new METValidator("Campo obligatorio") {
                @Override
                public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
                    return !isEmpty;
                }
            });
        } else {
            valid = false;
        }

        if (valid) {
            //Obtener textos
            usernameValue = username.getText().toString().trim();
            passwordValue = password.getText().toString();

            stationManager.stationLogin(usernameValue, passwordValue);
        }
    }

    private void getStationId(JSONObject response) {
        try {
            mongoId =
                    response.getJSONObject("data").getJSONObject("Datos de usuario").getString(
                            "_id_estacion");

            //Obtener el documento de la estación solicitada
            stationManager.getGeneralData(mongoId);
        } catch (JSONException e) {
            Toast.makeText(getContext(), "Error al procesar la información", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void confirmGottenStation(JSONObject response) {
        try {
            //Obtener datos completos de estación
            stationData = response.getJSONObject("data").getJSONObject("datos_generales");

            //Obtener id de estacion
            final String idEstacion = stationData.getString("id_estacion");

            //Definir un mensaje con el nombre de la estación obetnida
            String confirmMessage = "Estación encontrada: " + stationData.getString(
                    "nombre_estacion") + " " + idEstacion;

            GenericDialog genericDialog = new GenericDialog("Inicio de sesión", confirmMessage,
                    () -> {
                        //Guardar información en archivo local
                        StationFileManager.saveStationGeneralData(stationData, getContext());

                        //Guardar el id de mongo
                        SharedPreferencesManager.putString(getContext(),
                                ChipREDConstants.STATION_MONGO_ID, mongoId);

                        //Guardar el id de estacion
                        SharedPreferencesManager.putString(getContext(),
                                ChipREDConstants.STATION_ID, idEstacion);

                        //Guardar el modo de trabajo
                        saveWorkMode();

                        //Sincronizar servidor local
                        stationManager.syncLocalServer();

                        //Reportar que ya se cumplió el proceso en el fragment
                        getStationInterface.onStationGotten();
                    }, new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    form.setVisibility(View.VISIBLE);
                }
            }, getContext());

            genericDialog.setPositiveText("Continuar");
            genericDialog.setNegativeText("Cancelar");
            genericDialog.show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveWorkMode() {
        try {
            // Obtener contexto
            Context context = getActivity();

            // Obtener modo de trabajo
            String workMode =
                    stationData.getJSONObject("fidelizacion").getJSONObject("tokencash").getString("modo_trabajo");

            // Guardar el modo de trabajo
            ChipREDConstants.setCurrentScenario(context, workMode);

            // Des-suscribirse de todos los topics
            if (context != null)
                FcmUtils.unsubscribeToCouponMessaging(context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setGetStationInterface(GetStationInterface getStationInterface) {
        this.getStationInterface = getStationInterface;
    }

    public interface GetStationInterface {
        void onStationGotten();
    }
}
