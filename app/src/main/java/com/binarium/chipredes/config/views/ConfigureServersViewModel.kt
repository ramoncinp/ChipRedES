package com.binarium.chipredes.config.views

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binarium.chipredes.config.data.model.*
import com.binarium.chipredes.config.domain.toState
import com.binarium.chipredes.config.domain.usecases.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ConfigureServersViewModel @Inject constructor(
    private val getSavedServersConfig: GetSavedServersConfig,
    private val getEmaxClientPrefUseCase: GetEmaxClientPrefUseCase,
    private val setEmaxClientPrefUseCase: SetEmaxClientPrefUseCase,
    private val testEmaxUseCase: TestEmaxUseCase,
    private val testLocalUseCase: TestLocalUseCase,
    private val testSocketUseCase: TestSocketUseCase,
    private val testWolkeUseCase: TestWolkeUseCase,
) : ViewModel() {

    private val _onServersReady = MutableLiveData(false)
    val onServersReady: MutableLiveData<Boolean> = _onServersReady

    private val _serverResponse = MutableLiveData<TestServerResponse>()
    val serverResponse: LiveData<TestServerResponse> = _serverResponse

    var localServer = ConfigurableServer(
        ServerName.LOCAL,
        ServerAddressType.IP,
        ""
    )

    var wolkeServer = ConfigurableServer(
        ServerName.WOLKE,
        ServerAddressType.DNS,
        ""
    )

    var socketServer = ConfigurableServer(
        ServerName.SOCKET,
        ServerAddressType.IP,
        ""
    )

    var emaxServer = ConfigurableServer(
        ServerName.EMAX,
        ServerAddressType.DNS,
        ""
    )

    var emaxClientServer = ConfigurableServer(
        ServerName.EMAX_CLIENTE,
        ServerAddressType.DNS,
        ""
    )

    private fun mapServerData(server: ConfigurableServer) {
        when (server.name) {
            ServerName.LOCAL -> localServer = server
            ServerName.WOLKE -> wolkeServer = server
            ServerName.SOCKET -> socketServer = server
            ServerName.EMAX -> emaxServer = server
            ServerName.EMAX_CLIENTE -> emaxClientServer = server
        }
    }

    fun testServer(server: ConfigurableServer) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = when (server.name) {
                ServerName.LOCAL -> testLocalUseCase(server)
                ServerName.WOLKE -> testWolkeUseCase(server)
                ServerName.SOCKET -> testSocketUseCase(server)
                ServerName.EMAX, ServerName.EMAX_CLIENTE -> testEmaxUseCase(server)
            }

            _serverResponse.postValue(response)
        }
    }

    fun updateServerWithResponse(response: TestServerResponse) {
        val updatedServer = response.server.copy(testState = response.toState())
        mapServerData(updatedServer)
    }

    fun getServersConfig() {
        viewModelScope.launch(Dispatchers.IO) {
            getSavedServersConfig().forEach { server ->
                mapServerData(server)
            }
            _onServersReady.postValue(true)
        }
    }

    fun setEmaxClientConfig(isEnabled: Boolean) {
        setEmaxClientPrefUseCase(isEnabled)
    }

    fun getEmaxClientConfig() = getEmaxClientPrefUseCase()

    fun isContinuable() = localServer.testState == ServerTestState.SUCCESS &&
            wolkeServer.testState == ServerTestState.SUCCESS &&
            socketServer.testState == ServerTestState.SUCCESS &&
            emaxServer.testState == ServerTestState.SUCCESS &&
            (getEmaxClientConfig().not() || emaxClientServer.testState == ServerTestState.SUCCESS)
}
