package com.binarium.chipredes.config.domain.usecases

import android.content.SharedPreferences
import androidx.core.content.edit
import com.binarium.chipredes.ChipREDConstants.EMAX_CLIENT_ENABLED
import javax.inject.Inject

class SetEmaxClientPrefUseCase @Inject constructor(
    private val sharedPreferences: SharedPreferences,
) {

    operator fun invoke(isEnabled: Boolean) {
        sharedPreferences.edit {
            putBoolean(EMAX_CLIENT_ENABLED, isEnabled)
        }
    }
}
